package uk.co.wiggle.hybrid.application.datamodel.objectmanagers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNewsfeedCategory;


public class ObjNewsfeedCategoryManager {
	
	List<ObjNewsfeedCategory> mNewsfeedCategories = Collections.synchronizedList(new ArrayList<ObjNewsfeedCategory>());
	
	private static ObjNewsfeedCategoryManager sInstance = null;

	public static ObjNewsfeedCategoryManager getInstance() {
		if (sInstance == null) {
			sInstance = new ObjNewsfeedCategoryManager();
		}
		return sInstance;
	}

	protected ObjNewsfeedCategoryManager() {
		mNewsfeedCategories = Collections.synchronizedList(new ArrayList<ObjNewsfeedCategory>());
	}
	
	
	//only allowing the "set" method as opposed to all of the usual add / set methods as we need to synch content of this manager with
	//our ObjAppUser - see method ObjAppUser.instance().setSubscribedNewsTopics();#
	public void set(Collection<? extends ObjNewsfeedCategory> arg0) {
		mNewsfeedCategories.clear();
		mNewsfeedCategories.addAll(arg0);
		//now that we know our categories - synchronise them with the app user's unsubscribe settings
		ObjAppUser.instance().deserialiseUnsubscribedNewsTopicsFromAppUser();
	}
	

	/**
	 * @param object
	 * @return
	 * @see java.util.List#contains(java.lang.Object)
	 */
	public boolean contains(Object object) {
		return mNewsfeedCategories.contains(object);
	}

	/**
	 * @param object
	 * @return
	 * @see java.util.List#equals(java.lang.Object)
	 */
	public boolean equals(Object object) {
		return mNewsfeedCategories.equals(object);
	}

	/**
	 * @param location
	 * @return
	 * @see java.util.List#get(int)
	 */
	public ObjNewsfeedCategory get(int location) {
		return mNewsfeedCategories.get(location);
	}
	

	/**
	 * @param object
	 * @return
	 * @see java.util.List#indexOf(java.lang.Object)
	 */
	public int indexOf(Object object) {
		return mNewsfeedCategories.indexOf(object);
	}



	/**
	 * @return
	 * @see java.util.List#size()
	 */
	public int size() {
		return mNewsfeedCategories.size();
	}

	public List<ObjNewsfeedCategory> getAll() {
		return mNewsfeedCategories;
	}

	public boolean isEmpty() {
		return mNewsfeedCategories.isEmpty();
	}
	
	public String toString(){
		return mNewsfeedCategories.toString();
	}
	
	
	/** 
	 * 
	 * Convenience methods for retrieving the next children of a category, top level parents etc.
	 * 
	 */
	public ObjNewsfeedCategory getRecordByUniqueId(int intUniqueId){
		
		for(int i=0;i<mNewsfeedCategories.size();i++){
			
			ObjNewsfeedCategory booking = mNewsfeedCategories.get(i);
			
			if(booking.getUniqueId()==intUniqueId){
				return booking;
			}
			
		}

		return null;		
		
	}
	
	
	public List<ObjNewsfeedCategory> getSortedCategories() {
		
		ArrayList<ObjNewsfeedCategory> lstCategories = new ArrayList<ObjNewsfeedCategory>();
		lstCategories.addAll(mNewsfeedCategories);

		//sort list ascending (show nearest / next events starting first)
		SortByTitle sortByTitle = new SortByTitle();
		Collections.sort(lstCategories, sortByTitle);
		return lstCategories;
		

	}


	// Comparators
	class SortByTitle implements Comparator<ObjNewsfeedCategory> {

		@Override
		public int compare(ObjNewsfeedCategory item1, ObjNewsfeedCategory item2) {

			//not the nicest solution to treat item creation dates as string - but as we convert to yyyy-mm-dd this will do and work
			String strTitle1 = item1.getString(ObjNewsfeedCategory.FIELD_TITLE);
			String strTitle2 = item2.getString(ObjNewsfeedCategory.FIELD_TITLE);

			//sort by title alphabetically / ascending
			return strTitle1.compareTo(strTitle2);
			
		}

	}

}
