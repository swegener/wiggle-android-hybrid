package uk.co.wiggle.hybrid.usecase.shop.classicnav.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.usecase.shop.classicnav.activities.HybridActivity;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjTypeaheadResponse;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.extensions.PicassoSSL;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.tagging.AppTagManager;



import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/


public class TextSearchListAdapter extends BaseAdapter{
	
	private String logTag = "TextSearchListAdapter";

	private List<ObjTypeaheadResponse> mTypeheadResponses = new ArrayList<ObjTypeaheadResponse>();
	private int lastPosition;
	
	public TextSearchListAdapter() {
		
	}

	public void setListContent(List<ObjTypeaheadResponse> ObjResortList) {
		mTypeheadResponses = ObjResortList;		
	}

	@Override
	public int getCount() {
		return mTypeheadResponses.size();
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}
	

	@Override
	public View getView(int arg0, View view, ViewGroup arg2) 
	{
		final int position = arg0;
		
		final ObjTypeaheadResponse record = mTypeheadResponses.get(arg0);
		
		Logger.printMessage(logTag, "ObjTypeaheadResponse: " + record.toXML(), Logger.ALL);
		
		LayoutInflater inflater = LayoutInflater.from(ApplicationContextProvider.getContext());
		view = inflater.inflate(R.layout.text_search_item, null);
		
		TextView tvGroupHeader = (TextView) view.findViewById(R.id.textSearch_tvGroupHeader);
		tvGroupHeader.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		
		RelativeLayout layoutTextSearchItem = (RelativeLayout) view.findViewById(R.id.layoutTextSearchItem);
		
		TextView tvName = (TextView) view.findViewById(R.id.textSearch_tvName);
		tvName.setTypeface(AppTypefaces.instance().fontMainRegular);
		
		ImageView ivIcon = (ImageView) view.findViewById(R.id.textSearch_ivIcon);
		String strIcon = record.getString(ObjTypeaheadResponse.FIELD_LOGO);
		if(strIcon!=null && !"".equals(strIcon)){
			String strIconURL = ApplicationContextProvider.getContext().getString(R.string.str_url_wiggle_image_home) 
					  + strIcon;
			PicassoSSL.with(ApplicationContextProvider.getContext()).load(strIconURL).into(ivIcon);
		}else{
			
			//try alternative route for images 
			//This uses the product ID to form a URL like 
			//http://ajax.wiggle.co.uk/images/MainImage/5360092917?w=30&h=30&a=7
			
			String strPDID = record.getString(ObjTypeaheadResponse.FIELD_PDID);
			
			//empty or "0" PDID - do not show an image at all (this is the case for categories at the moment)
			if(strPDID!=null && !"".equals(strPDID) && !"0".equals(strPDID)){
				
				String strAlternativeImageURL = 
						AppUtils.getWiggleURL(ApplicationContextProvider.getContext().getString(R.string.str_url_wiggle_image_home_2)) 
								+ strPDID 
								+ "?w=200&amp;h=200&amp;a=0"; //return image in 200x200 size to ensure resolution is fine on Android
	
				PicassoSSL.with(ApplicationContextProvider.getContext()).load(strAlternativeImageURL).into(ivIcon);
			
			}
			
		}
		
		
		//group header row (outside view holder pattern)
		String strDisplayType = record.getString(ObjTypeaheadResponse.FIELD_DISPLAY_TYPE);
		
		if(ObjTypeaheadResponse.CONST_DISPLAY_TYPE_PARENT.equals(strDisplayType)){
			
			tvGroupHeader.setText(record.getString(ObjTypeaheadResponse.FIELD_NAME));
			tvGroupHeader.setVisibility(View.VISIBLE);
			layoutTextSearchItem.setVisibility(View.GONE);
			
		}else{
					
			tvGroupHeader.setVisibility(View.GONE);
			layoutTextSearchItem.setVisibility(View.VISIBLE);
			
			tvName.setText(record.getString(ObjTypeaheadResponse.FIELD_NAME));
			
			
			//Animation animation = AnimationUtils.loadAnimation(ApplicationContextProvider.getContext(), (position > lastPosition) ? R.anim.listview_anim_up_from_bottom : R.anim.listview_anim_down_from_top);
			//view.startAnimation(animation);
			lastPosition = position;
			
			layoutTextSearchItem.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {
					
					final String strSlug = record.getString(ObjTypeaheadResponse.FIELD_SLUG);

					if(strSlug!=null && !"".equals(strSlug)){
						
						//send GTM Tag based on category header
						String strCategoryHeading = "";
						ObjTypeaheadResponse parentRecord = record.getParentRecord();
						
						if(parentRecord!=null){
							strCategoryHeading = parentRecord.getString(ObjTypeaheadResponse.FIELD_NAME);
						}
						
						//send detailed tag for the selected category
						Map<String, Object> gtmTagsMap = new HashMap<String, Object>();
						gtmTagsMap.put("BrandProductCategory", strCategoryHeading);
						gtmTagsMap.put("keyword", HybridActivity.instance().getSearchTerm());
						gtmTagsMap.put("clickedResult", record.getString(ObjTypeaheadResponse.FIELD_NAME));
						AppTagManager.instance().sendEventTags(ApplicationContextProvider.getContext().getString(R.string.str_gtm_page_name_search), "searchFindAsYouType", gtmTagsMap);
						
						String strTargetUrl = AppUtils.getWiggleURL(ApplicationContextProvider.getContext().getString(R.string.str_url_wiggle_home)) 
								  + "/"
								  + strSlug;
						
						strTargetUrl = AppUtils.appendUtmTag(strTargetUrl, "searchFindAsYouType");
						
						//load URL of selected record
						HybridActivity.instance().loadSearchResultInWebView(strTargetUrl);
					}
					
				}
				
			});
			
		}

		return view;
		
	}
	

	
	@Override
	public Object getItem(int arg0) {
		return mTypeheadResponses.get(arg0);
	}
	

}
