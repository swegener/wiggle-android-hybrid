package uk.co.wiggle.hybrid.usecase.startup.activities;

import java.util.List;

import org.json.JSONObject;

import com.android.volley.Request.Method;
import com.yakivmospan.scytale.Crypto;
import com.yakivmospan.scytale.Options;
import com.yakivmospan.scytale.Store;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler.ON_RESPONSE_ACTION;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjNavigationCategoryManager;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjSalesOrderManager;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjApiRequest;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNavigationCategory;
import uk.co.wiggle.hybrid.application.session.AppSession;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.interfaces.IAppActivityUI;
import uk.co.wiggle.hybrid.interfaces.IAppApiResponse;
import uk.co.wiggle.hybrid.usecase.newsfeed.activities.NewsfeedActivity;
import uk.co.wiggle.hybrid.tagging.AppTagManager;
import uk.co.wiggle.hybrid.usecase.shop.classicnav.activities.HybridActivity;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.activities.TabbedHybridActivity;
import uk.co.wiggle.hybrid.usecase.shop.version3.activities.V3HybridActivity;
import uk.co.wiggle.hybrid.usecase.startup.adapters.StartupMenuListAdapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.view.*;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import javax.crypto.SecretKey;

import static uk.co.wiggle.hybrid.R.id.userLogin_etPassword;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/


public class StartupActivity extends Activity implements View.OnClickListener, IAppActivityUI, IAppApiResponse {
	
	private static final String logTag = "StartupActivity";

	public static String getLogTag(){
		return logTag;
	}
	
    //allow other activities to read an instance of MainActivity so that MainActivity methods can be called
	private static StartupActivity myInstance;

	public static StartupActivity instance(){
		return myInstance;
	}
	
	//Widgets
	LinearLayout lytStartupScreen;
	ScrollView lytLoginScreen;
	LinearLayout login_lytCancel;
	LinearLayout login_lytBack;
	EditText	 login_etEmail;
	EditText	 login_etPassword;
	ProgressBar	 login_pbLoginInProgress;
	TextView	 login_tvLogin;
	Switch		 login_swRememberMyDetails;
	TextView login_tvBack;
	TextView login_tvCancel;
	TextView startup_tvShop;
	TextView startup_tvMyAccount;
	TextView startup_tvTrackMyOrder;
	TextView startup_tvOrderTrackingBadge;
	TextView startup_tvWishlist;
	TextView startup_tvMoreFromWiggle;
	TextView login_tvTitleLogin;
	TextView login_tvRememberMyDetails;
	TextView login_tvForgotPassword;
	TextView login_tvOr;
	TextView login_tvRegister;
	ImageView login_ivBack;
	ImageView login_ivCancel;
	TextView startup_tvAppRedirectActive;
	
	//Dynamic "More from Wiggle" menu with Contentful integration
	AbsListView startup_lvMoreFromWiggle;
	StartupMenuListAdapter startupMenuListAdapter;
	StartupMenuListUpdateTask mStartupMenuListUpdateTask = new StartupMenuListUpdateTask();
	
	
	//Others
	int intLoginScreenCaller = 0;
	Rect rect;
	public static final String SHOW_LOGIN_SCREEN_ONLY = "SHOW_LOGIN_SCREEN_ONLY";
	
   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        setContentView(R.layout.startup_activity);
        
        myInstance = this;
        
		findViews();
		
		if(getIntent().getBooleanExtra(SHOW_LOGIN_SCREEN_ONLY, false)){
			//show login form only
			displayLayout(R.id.lytLoginScreen);
		}else{
			//standard startup
			displayLayout(R.id.lytStartupScreen);	
		}
		
		if(ObjAppUser.instance().isUserLoggedIn()){
			AppRequestHandler.instance().retrieveOrderData(this, ON_RESPONSE_ACTION.RETRIEVE_USER_RELATED_DATA);
		}

    }
   
    @Override
    public void onResume(){
    	
    	super.onResume();
    	

    	if(ObjNavigationCategoryManager.getInstance().getCombinedNavigationTree(StartupActivity.getLogTag())==null){
	        //get navigation tree from local store
	        ObjApiRequest localFileReadRequest = new ObjApiRequest(this, AppRequestHandler.LOCAL_NAVIGATION_FILE, 0, null);
	        AppRequestHandler.instance().getJsonObjectDataFromRawResource(localFileReadRequest);   
    	}

        
    }
   

	  
	@Override
	public void onApiRequestSuccess(ObjApiRequest apiRequest,
			JSONObject apiSuccessDetail) {
		
		if(AppRequestHandler.getAuthenticationEndpoint().equals(apiRequest.getURLEndpoint())){
			
			login_pbLoginInProgress.setVisibility(View.GONE);
			login_tvLogin.setText(getString(R.string.login_tvLogin));
			
			//check for session specific parameters for login
			ObjNavigationCategory androidNativeCategory = AppSession.instance().getAndroidNativeCategory();
			if(androidNativeCategory!=null){
				HybridActivity.instance().onApiRequestSuccess(apiRequest, apiSuccessDetail);
				HybridActivity.instance().showUserAsLoggedInOrOut();
				AppSession.instance().setInAppLogin(false);
				finish();
				return;
			}
			
			if(AppSession.instance().isInAppLogin()){

				if(AppUtils.getWiggleVersion().equals(AppUtils.APP_VERSION.VERSION_1_HYBRID)){
					HybridActivity.instance().handleInAppLogin();
				}else if(AppUtils.getWiggleVersion().equals(AppUtils.APP_VERSION.VERSION_2_TABBED_NAV)){
					TabbedHybridActivity.instance().handleInAppLogin();
				}else if(AppUtils.getWiggleVersion().equals(AppUtils.APP_VERSION.VERSION_3_APP_WIDE_SEARCH)){
					V3HybridActivity.instance().handleInAppLogin();
				}

				finish();
			}else{
				launchHybridActivity(intLoginScreenCaller);
			}
			
		}else if(AppRequestHandler.getOrderTrackingEndpoint().equals(apiRequest.getURLEndpoint())){
			
			updateOrderTrackingBadge();
			
		}

	}

	@Override
	public void onApiRequestError(ObjApiRequest request,
			int intHttpResponseCode, JSONObject apiErrorDetail) {

		if(AppRequestHandler.getAuthenticationEndpoint().equals(request.getURLEndpoint())){
			
			login_pbLoginInProgress.setVisibility(View.GONE);
			login_tvLogin.setText(getString(R.string.login_tvLogin));
			
		}		
		
	}

	@Override
	public void setupCustomActionBar() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCustomFontTypes() {
		
		login_tvBack.setTypeface(AppTypefaces.instance().fontMainRegular);
		login_tvCancel.setTypeface(AppTypefaces.instance().fontMainRegular);
		startup_tvShop.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		startup_tvMyAccount.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		startup_tvTrackMyOrder.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		startup_tvOrderTrackingBadge.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		startup_tvWishlist.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		startup_tvMoreFromWiggle.setTypeface(AppTypefaces.instance().fontMainRegular);
		login_tvTitleLogin.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		login_etEmail.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		login_etPassword.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		login_tvRememberMyDetails.setTypeface(AppTypefaces.instance().fontMainRegular);
		login_swRememberMyDetails.setTypeface(AppTypefaces.instance().fontMainRegular);
		login_tvForgotPassword.setTypeface(AppTypefaces.instance().fontMainRegular);
		login_tvLogin.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		login_tvOr.setTypeface(AppTypefaces.instance().fontMainRegular);
		login_tvRegister.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		
	}

	@Override
	public void findViews() {

		startup_tvAppRedirectActive = (TextView) findViewById(R.id.startup_tvAppRedirectActive);
		if(!"".equals(AppUtils.REDIRECT_APP_TO_TEST_SERVER)){
			startup_tvAppRedirectActive.setText(AppUtils.REDIRECT_APP_TO_TEST_SERVER);
			startup_tvAppRedirectActive.setTypeface(AppTypefaces.instance().fontMainRegular);
			startup_tvAppRedirectActive.setVisibility(View.VISIBLE);
		}else{
			startup_tvAppRedirectActive.setVisibility(View.GONE);
		}

		lytStartupScreen = (LinearLayout) findViewById(R.id.lytStartupScreen);
		lytLoginScreen = (ScrollView) findViewById(R.id.lytLoginScreen);
		
		login_lytCancel = (LinearLayout) findViewById(R.id.login_lytCancel);
		login_ivCancel = (ImageView) findViewById(R.id.login_ivCancel);
		login_lytBack = (LinearLayout) findViewById(R.id.login_lytBack);
		login_ivBack = (ImageView) findViewById(R.id.login_ivBack);
		
		login_etEmail = (EditText) findViewById(R.id.login_etEmail);
		login_etPassword = (EditText) findViewById(R.id.login_etPassword);
		addImeOptionGo(login_etPassword);
		
		login_pbLoginInProgress = (ProgressBar) findViewById(R.id.login_pbLoginInProgress);
		login_tvLogin = (TextView) findViewById(R.id.login_tvLogin);
		login_swRememberMyDetails = (Switch) findViewById(R.id.login_swRememberMyDetails);
		
		login_tvBack = (TextView) findViewById(R.id.login_tvBack);
		login_tvCancel = (TextView) findViewById(R.id.login_tvCancel);
		startup_tvShop = (TextView) findViewById(R.id.startup_tvShop);
		startup_tvMyAccount = (TextView) findViewById(R.id.startup_tvMyAccount);
		startup_tvTrackMyOrder = (TextView) findViewById(R.id.startup_tvTrackMyOrder);
		startup_tvOrderTrackingBadge = (TextView) findViewById(R.id.startup_tvOrderTrackingBadge);
		startup_tvWishlist = (TextView) findViewById(R.id.startup_tvWishlist);
		startup_tvMoreFromWiggle = (TextView) findViewById(R.id.startup_tvMoreFromWiggle);
		login_tvTitleLogin = (TextView) findViewById(R.id.login_tvTitleLogin);
		login_tvRememberMyDetails = (TextView) findViewById(R.id.login_tvRememberMyDetails);
		login_tvForgotPassword = (TextView) findViewById(R.id.login_tvForgotPassword);
		login_tvOr = (TextView) findViewById(R.id.login_tvOr);
		login_tvRegister = (TextView) findViewById(R.id.login_tvRegister);

		//add changing background to image views
		setDependantImageViewListener(login_lytBack, login_ivBack, getResources().getColor(R.color.white));
		setDependantImageViewListener(login_lytCancel, login_ivCancel, getResources().getColor(R.color.orangeButton));
		
		//An empty password address indicates a user has never logged in yet or logged out from the app before (in the latter case we would keep the email for ease of use)
		//Set the "remember my details" switch to on.
		String strUserPassword = ObjAppUser.instance().getString(ObjAppUser.FIELD_PASSWORD);
		if(strUserPassword==null || "".equals(strUserPassword)){
			login_swRememberMyDetails.setChecked(true);
		}else{
			//if we have an email address for this user - set the switch in accordance with the "remember my credentials" field
			login_swRememberMyDetails.setChecked(ObjAppUser.instance().getBoolean(ObjAppUser.FIELD_REMEMBER_CREDENTIALS));
		}

		//Dynamic "More from Wiggle" menu
		startup_lvMoreFromWiggle = (AbsListView) findViewById(R.id.startup_lvMoreFromWiggle);
		setupMoreFromWiggleListView();
		
		//standard methods to style textviews
		setCustomFontTypes();
		
	}

	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		int id = v.getId();
		
		switch (id) {
		
			/* Widgets starting app internal web view */
			case R.id.startup_btnShop:
				launchHybridActivity(id);
				break;
				
			case R.id.login_tvForgotPassword:
				launchHybridActivity(id);
				break;
				
			case R.id.login_btnRegister:
				launchHybridActivity(id);
				break;
				
			/* Widgets requiring login */
			case R.id.startup_lytMyAccount:
				intLoginScreenCaller = id;
				displayLayout(R.id.lytLoginScreen);
				break;
				
			case R.id.startup_lytTrackMyOrder:
				intLoginScreenCaller = id;
				displayLayout(R.id.lytLoginScreen);
				break;
				
			case R.id.startup_lytWishlist:
				intLoginScreenCaller = id;
				displayLayout(R.id.lytLoginScreen);
				break;
				
			/* Back navigation */
			case R.id.login_lytCancel:
				AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_signin), "signinCancel");
				//clear login specific session variables
				AppSession.instance().setAndroidNativeCategory(null);
				onBackPressed();
				break;
				
			case R.id.login_lytBack:
				if(lytStartupScreen.getVisibility()==View.VISIBLE){
					AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_home), "homeBack");
				}else if(lytLoginScreen.getVisibility()==View.VISIBLE){
					//clear login specific session variables
					AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_signin), "signinBack");
				}
				onBackPressed();
				break;

			/* Bespoke methods */
			case R.id.login_btnLogin:
				AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_signin), "signinSignin");
				submitLogin();
				break;

				
				
		default:
			break;
			
		}
		
	}
	
	
	private void startNewsfeedActivity(){
		
		Intent intent = new Intent(this, NewsfeedActivity.class);
		//intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION); //this makes sure activities do not slide in from right to left when started, but simply appear
		intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT); //if we open this screen through a deep-link it might just need to be re-ordered to the front
		startActivityForResult(intent, 0);
		
		
	}

	public void launchHybridActivity(int intViewId){
		
		Intent intent = new Intent(this, HybridActivity.class);
		//intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION); //this makes sure activities do not slide in from right to left when started, but simply appear
		intent.putExtra(HybridActivity.WEB_VIEW_CALLER, intViewId);
		startActivityForResult(intent, 0);
		
		
		finish();
		
		//if it was displayed - close WelcomeActivity
		if(WelcomeActivity.instance()!=null) {
			WelcomeActivity.instance().finish();
			AppSession.instance().setAllowBackNavigationToWelcomeScreen(false);
		}

		
	}

	/* used by Contentful-configured items on StartupScreen */
	public void launchHybridActivity(String strURLPattern, boolean blnFinishStartupActivity){

		Intent intent = new Intent(this, HybridActivity.class);
		//intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION); //this makes sure activities do not slide in from right to left when started, but simply appear
		intent.putExtra(HybridActivity.LOAD_TARGET_URL_FROM_STARTUP_MENU, strURLPattern);
		intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT); //if we open this screen through a deep-link it might just need to be re-ordered to the front
		startActivityForResult(intent, 0);

		if(blnFinishStartupActivity) {
			finish();
		}

		//if it was displayed - close WelcomeActivity
		//Exception: if we opened StartupActivity from country selection screen .. allow user to go back there
		if(WelcomeActivity.instance()!=null) {
			WelcomeActivity.instance().finish();
			AppSession.instance().setAllowBackNavigationToWelcomeScreen(false);
		}


	}

	
	public void openUrlInExternalBrowser(String strTargetURL){
		
		Intent externalBrowserIntent = new Intent(Intent.ACTION_VIEW);
		externalBrowserIntent.setData(Uri.parse(strTargetURL));
		startActivity(externalBrowserIntent);
		
	}
	
	private void displayLayout(int layoutId){
		
		//before showing login screen - check if this is needed at all ... 
		if(layoutId==R.id.lytLoginScreen && getIntent().getBooleanExtra(SHOW_LOGIN_SCREEN_ONLY, false) == false){
			//user already logged in? Just start target activity and rely on silent log in to make things work
			if(ObjAppUser.instance().isUserLoggedIn()){
				launchHybridActivity(intLoginScreenCaller);
				return;
			}
		}
		
		lytStartupScreen.setVisibility(View.GONE);
		lytLoginScreen.setVisibility(View.GONE);
		
		//set passed view visible
		View layoutView = findViewById(layoutId);
		layoutView.setVisibility(View.VISIBLE);
		
		if(R.id.lytLoginScreen==layoutId){
			login_lytBack.setVisibility(View.VISIBLE);
			login_lytCancel.setVisibility(View.VISIBLE);
		}else{
			//if we opened this from country selection screen .. allow to go back there
			if(AppSession.instance().isAllowedBackNavigationToWelcomeScreen()){
				login_lytBack.setVisibility(View.VISIBLE);
			}else{
				
				login_lytBack.setVisibility(View.INVISIBLE);
			}
			
			login_lytCancel.setVisibility(View.INVISIBLE);
		}
		
		
		

		//set email address for login screen
		if(layoutId==lytLoginScreen.getId()){
			
			login_etEmail.setText(ObjAppUser.instance().getString(ObjAppUser.FIELD_EMAIL));
			//this will read the user's encrypted password (stored in FIELD_PASSWORD)
			// and populate the EditText with the original password (to avoid re-encryption of already encrypted string)
			login_etPassword.setText(ObjAppUser.instance().getPassword());
			
		}
		

	}
	
	@Override
	public void onBackPressed(){
		
		if(lytLoginScreen.getVisibility()==View.VISIBLE){
			
			//clear other session variables specific to login
			AppSession.instance().setAndroidNativeCategory(null);

			//Login screen: user presses "cancel", "back button" or screen is closed when login successful
			AppSession.instance().setInAppLogin(false);
			
			
			//if this activity was called to show the login form only .. 
			if(getIntent().getBooleanExtra(SHOW_LOGIN_SCREEN_ONLY, false)){
				finish();
				return;
			}
			
			displayLayout(R.id.lytStartupScreen);
			return;
		}
		
		//reset session variable (will only be set to true if WelcomeActivity calls StartupActivity
		AppSession.instance().setAllowBackNavigationToWelcomeScreen(false);
		//clear navigation tree in case user changes country (could lead to french menu being displayed to non-FR market user)
		ObjNavigationCategoryManager.getInstance().clearNavigationTrees();
		//clear order manager
		ObjSalesOrderManager.getInstance().clear();
		

		
		AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_general), "generalBack");

		//Fix: if "<Back" button is not displayed: make sure "Welcome" screen is closed as well in case it is in the back stack
		if(login_lytBack.getVisibility()!=View.VISIBLE){
			if(WelcomeActivity.instance()!=null){
				WelcomeActivity.instance().finishActivity();
			}
		}
		
		super.onBackPressed();
	}
	
	private void submitLogin(){
		
		ObjAppUser loginUser = isLoginValid();
		if(loginUser==null){
			return;
		}
		
		submitEmailLogin(loginUser);
		
	}

	private ObjAppUser isLoginValid(){
		
		View invalidView = null;
		ObjAppUser loginUser = new ObjAppUser(); //temporary user to pass login information to request
		
		//save setting of "Remember my credentials" switch
		ObjAppUser.instance().setField(ObjAppUser.FIELD_REMEMBER_CREDENTIALS, AppUtils.getParameterFromSwitch(login_swRememberMyDetails));
		
		//Password validation
		String strPasswordValidationResult = AppUtils.validatePassword(login_etPassword);
		if(strPasswordValidationResult!=null){
			invalidView = login_etPassword;
			AppUtils.flagWidgetDataStatus(login_etPassword, false, strPasswordValidationResult);	
		}else{
			AppUtils.flagWidgetDataStatus(login_etPassword, true, strPasswordValidationResult);

			//We are using a temporary user object for request (loginUser)
			//We are encrypting the password the user typed in
			Crypto crypto = new Crypto(Options.TRANSFORMATION_SYMMETRIC);
			Store store = new Store(ApplicationContextProvider.getContext());
			SecretKey key = store.getSymmetricKey("pwkey", null);
			String strEncryptedPassword = crypto.encrypt(login_etPassword.getText().toString(), key);
			loginUser.setField(ObjAppUser.FIELD_PASSWORD, strEncryptedPassword);

			//only persist password if user wanted to save his credentials (we are saving the encrypted password only)
			if(ObjAppUser.instance().getBoolean(ObjAppUser.FIELD_REMEMBER_CREDENTIALS)){
				ObjAppUser.instance().setField(ObjAppUser.FIELD_PASSWORD, strEncryptedPassword);
			}
			
		}		
		
		//Email Mandatory
		String strEmailValidationResult = AppUtils.validateEmail(login_etEmail);
		if(strEmailValidationResult!=null){
			invalidView = login_etEmail;
			AppUtils.flagWidgetDataStatus(login_etEmail, false, strEmailValidationResult);	
		}else{
			AppUtils.flagWidgetDataStatus(login_etEmail, true, strEmailValidationResult);
			//temporary user object for requst
			loginUser.setField(ObjAppUser.FIELD_EMAIL, login_etEmail.getText().toString());	
			//persist email address with application's app user object
			ObjAppUser.instance().setField(ObjAppUser.FIELD_EMAIL, login_etEmail.getText().toString());
		}
			
		if(invalidView==null){
			return loginUser;
		}else{
			invalidView.requestFocus();
			return null;
		}				
		
		
	}
	
	private void submitEmailLogin(ObjAppUser loginUser){
		
		login_pbLoginInProgress.setVisibility(View.VISIBLE);
		login_tvLogin.setText(getString(R.string.login_btnLogin_inProgress));
		
		new ObjApiRequest(this, 
				null, 
				AppRequestHandler.getAuthenticationEndpoint(),
				Method.POST, 
				loginUser, //temporary login user as opposed to application user
				null, 
				ON_RESPONSE_ACTION.RETRIEVE_USER_RELATED_DATA)
		.submit();
		
	}
	

	public void setDependantImageViewListener(final View clickableParent, final ImageView dependantImageView, final int intActiveColour){
		
		//final int intActiveColor = Color.parseColor("#f2951f"); //change colour to orange             
		
		clickableParent.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {

				switch (event.getAction()) {

					case MotionEvent.ACTION_DOWN:

						// Construct a rect of the view's bounds
						rect = new Rect(clickableParent.getLeft(), clickableParent.getTop(), clickableParent.getRight(), clickableParent.getBottom());

						dependantImageView.setColorFilter(intActiveColour);
						break;

					case MotionEvent.ACTION_UP:
					case MotionEvent.ACTION_CANCEL:

						dependantImageView.setColorFilter(null);
						break;

					case MotionEvent.ACTION_MOVE:
						if (!rect.contains(dependantImageView.getLeft() + (int) event.getX(), dependantImageView.getTop() + (int) event.getY())) {
							dependantImageView.setColorFilter(null);
						} else {
							dependantImageView.setColorFilter(intActiveColour);
						}
						break;

					default:
						break;

				}

				return false; // returning false as we still want onClick to be triggered when suitable
			}
		});
		
	}
	
	private void addImeOptionGo(final EditText editText){
		
		editText.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

				if (actionId == EditorInfo.IME_ACTION_GO) {

					if (!"".equals(editText.getText().toString())) {

						submitLogin();

					}

					return true;
				}
				return false;
			}


		});
		
	}
	
	private void updateOrderTrackingBadge(){
		
		int intBadgeCounter = ObjSalesOrderManager.getInstance().getOrdersWithNewInformation().size();
		
		if(intBadgeCounter>0){
			startup_tvOrderTrackingBadge.setText(String.valueOf(intBadgeCounter));
			startup_tvOrderTrackingBadge.setVisibility(View.VISIBLE);
		}else{
			startup_tvOrderTrackingBadge.setVisibility(View.GONE);
		}
		
		if(HybridActivity.instance()!=null){
			HybridActivity.instance().updateOrderTrackingBadge();
		}

		
	}

	private void setupMoreFromWiggleListView(){

		startupMenuListAdapter = new StartupMenuListAdapter(this);
		startup_lvMoreFromWiggle.setAdapter(startupMenuListAdapter);
		updateMoreFromMiggleMenu();

	}

	public void updateMoreFromMiggleMenu(){

		if(startupMenuListAdapter==null){
			return;
		}

		runOnUiThread(mStartupMenuListUpdateTask);

	}



	private class StartupMenuListUpdateTask implements Runnable {

		@Override
		public void run() {

			//Get the list of menu items configured for the "More From Wiggle" menu
			//These are items from top_level_navigation.json as well as remote contentful items
			List<ObjNavigationCategory> moreFromWiggleMenuItems = ObjNavigationCategoryManager.getInstance().getStartupMoreFromWiggleItems();

			startupMenuListAdapter.setListContent(moreFromWiggleMenuItems);
			startupMenuListAdapter.notifyDataSetChanged();

			//start with "More from Wiggle" as invisible
			if(moreFromWiggleMenuItems.size()==0) {
				startup_tvMoreFromWiggle.setVisibility(View.GONE);
			}else{
				startup_tvMoreFromWiggle.setVisibility(View.VISIBLE);
			}

		}

	}

	public void startNativeScreen(ObjNavigationCategory menuItem){

		//tag "menu_item_target" defines which screen to start ..
		String strMenuItemTargetScreen = menuItem.getString(ObjNavigationCategory.FIELD_MENU_ITEM_TARGET);

		if(NewsfeedActivity.getLogTag().equals(strMenuItemTargetScreen)){
			startNewsfeedActivity();
		}

		//if it was displayed - close WelcomeActivity
		if(WelcomeActivity.instance()!=null) {
			WelcomeActivity.instance().finish();
			AppSession.instance().setAllowBackNavigationToWelcomeScreen(false);
		}

	}



}
