package uk.co.wiggle.hybrid.application;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Logger.LogLevel;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;
import com.salesforce.marketingcloud.MCLogListener;
import com.salesforce.marketingcloud.MarketingCloudConfig;
import com.salesforce.marketingcloud.MarketingCloudSdk;
import com.salesforce.marketingcloud.UrlHandler;
import com.salesforce.marketingcloud.messages.iam.InAppMessage;
import com.salesforce.marketingcloud.messages.iam.InAppMessageManager;
import com.salesforce.marketingcloud.notifications.NotificationCustomizationOptions;
import com.salesforce.marketingcloud.notifications.NotificationManager;
import com.salesforce.marketingcloud.notifications.NotificationMessage;
import com.yakivmospan.scytale.Store;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjNavigationCategoryManager;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjHoehle;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.tagging.AppTagManager;
import uk.co.wiggle.hybrid.usecase.shop.version3.activities.V3HybridActivity;
import uk.co.wiggle.hybrid.usecase.startup.activities.WelcomeActivity;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import androidx.core.app.NotificationCompat;
import androidx.security.crypto.EncryptedSharedPreferences;
import androidx.security.crypto.MasterKeys;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Random;

import javax.crypto.SecretKey;

import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

import org.jetbrains.annotations.NotNull;


/**
 * This class can be used to access the application context outside the typical
 * context scope (e.g. in Utility classes etc)
 * 
 * Please note: for this to work the application name in the Manifest must be
 * defined as follows:
 * 
 * <application ...
 * android:name="uk.co.wiggle.hybrid.application.ApplicationContextProvider"
 * ... >
 * 
 * @author AndroMedia
 * 
 */

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class ApplicationContextProvider extends MultiDexApplication {

	/**
	 * Keeps a reference of the application context
	 */
	private static final String logTag = "ApplicationContextProvider";

	private static Context sContext;
	private static ApplicationContextProvider mApplication;
	private Tracker tracker; //used for Google Analytics
	private FirebaseAnalytics mFirebaseAnalytics; //Analytics instance for app-wide use

	//variables will be initialised in the onCreate function
	private String masterKeyAlias;
	private static SharedPreferences encryptedSharedPreferences;

	public static SharedPreferences getEncryptedSharedPreferences(){
		return encryptedSharedPreferences;
	}

	@Override
	public void onCreate() {
		super.onCreate();

		sContext = getApplicationContext();
		mApplication = this;


		// listen for connectivity changes
		//registerReceiver(new ConnectivityChangeReceiver(), new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

		//start Google Analytics
		startGoogleAnalytics();

        //initialise Firebase Analytics
        initialiseFirebaseAnalytics();

        //Firebase push integration
		getFirebaseMessagingToken();

        //start Microsoft App Center
		startMicrosoftAppCenter();

        //Initialise Facebook SDK - and Facebook Analytics
		FacebookSdk.sdkInitialize(getApplicationContext());
		AppEventsLogger.activateApp(this);

		//Create encrypted shared preferences store for sensitive information
		//(1) create or retrieve masterkey from Android keystore
		//    The masterkey is used to encrypt data encryption keys
		try {
			masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC);
			//(2) Get instance of EncryptedSharedPreferences class
			// as part of the params we pass the storage name, reference to
			// masterKey, context and the encryption schemes used to
			// encrypt SharedPreferences keys and values respectively.
			encryptedSharedPreferences = EncryptedSharedPreferences.create(
					"sec_shared_prefs",
					masterKeyAlias,
					this,
					EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
					EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
					);

		} catch (GeneralSecurityException e) {
			Logger.printMessage(logTag, "MasterKeys.getOrCreate() failed: " + e.getMessage(), Logger.ERROR);
		} catch (IOException e) {
			e.printStackTrace();
		}

		//Create a secret key for the user password and save it
		// Create and save a secret key on this device on app startup
		Store store = new Store(getApplicationContext());
		if (!store.hasKey("pwkey")) {
			SecretKey key = store.generateSymmetricKey("pwkey", null);
		}


		// Initialize logging _before_ initializing the SDK to avoid losing valuable debugging information.
		MarketingCloudSdk.setLogLevel(MCLogListener.VERBOSE);
		MarketingCloudSdk.setLogListener(new MCLogListener() {
			@Override
			public void out(int intLogLevel, @NotNull String strLogTag, @NotNull String strMessage, @org.jetbrains.annotations.Nullable Throwable throwable) {
				Logger.printMessage("SALESFORCE LOG", strLogTag + ": " + strMessage, Logger.INFO);
			}
		});

		ObjHoehle h = new ObjHoehle();


		//Initialise SalesForce marketing cloud
		//Comment: If you initialize the FirebaseApp manually, you must initialize Firebase before initializing the SDK.
		MarketingCloudSdk.init(this,
				MarketingCloudConfig.builder()
						//Mobile Push App "Wiggle App Development"
						/*
						.setApplicationId("8448f500-e325-4e61-a8fd-ca20b9433305") //{MC_APP_ID}
						.setAccessToken("eml6TQqJ62rvQgJ1p2d630OJ") //{MC_ACCESS_TOKEN}
						.setMarketingCloudServerUrl("https://mctq60-m630tq7zl7z1r5967p7m0.device.marketingcloudapis.com/") //{MC_APP_SERVER_URL}
						*/

						//Mobile Push App "Wiggle App Production
						.setApplicationId(h.getValue(ObjHoehle.FIELD_BB)) //{MC_APP_ID}
						.setAccessToken(h.getValue(ObjHoehle.FIELD_BC)) //{MC_ACCESS_TOKEN}
						.setMarketingCloudServerUrl(h.getValue(ObjHoehle.FIELD_BD)) //{MC_APP_SERVER_URL}

						.setSenderId(h.getValue(ObjHoehle.FIELD_BE)) //{FCM_SENDER_ID_FOR_MC_APP}
						.setAnalyticsEnabled(true)
						//The following makes sure that OpenDirect notifications (notifications with a URL) are handled:
						.setNotificationCustomizationOptions(
							NotificationCustomizationOptions.create(R.drawable.ic_notification,
									new NotificationManager.NotificationLaunchIntentProvider() {
										@Nullable
										@Override
										public PendingIntent getNotificationPendingIntent(@NonNull Context context,
																						  @NonNull NotificationMessage notificationMessage) {
											int requestCode = new Random().nextInt();
											String url = notificationMessage.url();
											PendingIntent pendingIntent;
											if (TextUtils.isEmpty(url)) {
												pendingIntent = PendingIntent.getActivity(
														context,
														requestCode,
														new Intent(context, V3HybridActivity.class),
														PendingIntent.FLAG_UPDATE_CURRENT
												);
											} else {
												pendingIntent = PendingIntent.getActivity(
														context,
														requestCode,
														new Intent(Intent.ACTION_VIEW, Uri.parse(url)),
														PendingIntent.FLAG_UPDATE_CURRENT
												);
											}
											return NotificationManager.redirectIntentForAnalytics(context, pendingIntent, notificationMessage, true);
										}
									}, new NotificationManager.NotificationChannelIdProvider() {
										@NonNull @Override public String getNotificationChannelId(@NonNull Context context,
																								  @NonNull NotificationMessage notificationMessage) {
											if (TextUtils.isEmpty(notificationMessage.url())) {
												return NotificationManager.createDefaultNotificationChannel(context);
											} else {
												//URL notifications should be posted on the default channel too
												return NotificationManager.createDefaultNotificationChannel(context);
											}
										}
									}))
						//handle URLs passed for buttons on an in-App message
						.setUrlHandler(new UrlHandler() {
							@Nullable
							@Override
							public PendingIntent handleUrl(@NonNull Context context, @NonNull String url, @NonNull String s1) {

								int requestCode = new Random().nextInt();
								Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
								return PendingIntent.getActivity(getApplicationContext(),requestCode,intent,
										PendingIntent.FLAG_UPDATE_CURRENT);

							}
						})
						.build(this),
				initializationStatus -> Log.i("SALESFORCE INIT", initializationStatus.toString()));



	}

	/**
	 * Returns an instance of this class. Built, so we can use getTracker() from other classes
	 *
	 * @return ApplicationContextProvider
	 */
	public static ApplicationContextProvider instance() {
		return mApplication;
	}


	/**
	 * Returns the application context
	 *
	 * @return application context
	 */
	public static Context getContext() {
		return sContext;
	}


	public void startGoogleAnalytics() {
		
		GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
	    analytics.setLocalDispatchPeriod(1800);

	    // Please note that UA-473815-42 is only an app tracker. If global trackers etc are required add accordingly
	    // See https://developers.google.com/analytics/devguides/collection/android/v4/advanced
	    // For general overview see https://developers.google.com/analytics/devguides/collection/android/v4/
	    // Please also mind the obvious setup on Google Analytics: http://collectiveidea.com/blog/archives/2013/11/06/google-analytics-demographics-interest-reports-not-working-heres-what-you-need-to-know/
	    //tracker = analytics.newTracker("UA-473815-42");  //replaced with XML file below - see https://developers.google.com/android/reference/com/google/android/gms/analytics/Tracker
	    tracker = analytics.newTracker(R.xml.app_tracker_config);
	    tracker.enableExceptionReporting(true);
	    tracker.enableAdvertisingIdCollection(true);
	    tracker.enableAutoActivityTracking(true);

	    // Set the log level to verbose.
	    GoogleAnalytics.getInstance(this).getLogger().setLogLevel(LogLevel.ERROR);
	}
	
	public synchronized Tracker getTracker() {
	    if (tracker == null) {
	        startGoogleAnalytics();
	    }
	    return tracker;
	}

	private int getSmallIcon()
	{
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
		{
			return R.drawable.ic_notification;
		}
		else
		{
			return R.mipmap.ic_launcher;
		}
	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		MultiDex.install(this);
	}

	public String getAnalyticsTrackerCID(){

		if(tracker!=null) {
			return tracker.get("&cid");
		}else{
			return "";
		}

	}

	private void initialiseFirebaseAnalytics(){
		// Obtain the FirebaseAnalytics instance.
		mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
	}
	public FirebaseAnalytics getAppFirebaseAnalytics(){
		return this.mFirebaseAnalytics;
	}

	private void startMicrosoftAppCenter(){
		AppCenter.start(this, "f8f8eced-1b0d-458b-8a62-a231a0366af8",
				Analytics.class, Crashes.class);
	}

	private void getFirebaseMessagingToken(){

		FirebaseMessaging.getInstance().getToken()
				.addOnCompleteListener(new OnCompleteListener<String>() {
					@Override
					public void onComplete(@NonNull Task<String> task) {
						if (!task.isSuccessful()) {
							Logger.printMessage("ApplicationContextProvider", "Fetching FCM registration token failed: " + task.getException(), Logger.INFO);
							return;
						}

						// Get new FCM registration token
						String token = task.getResult();
						Logger.printMessage("ApplicationContextProvider", "Firebase messaging token: " + token, Logger.INFO);
						//Regsiter this token with SalesForce marketing cloud
						MarketingCloudSdk.requestSdk(new MarketingCloudSdk.WhenReadyListener() {
							@Override
							public void ready(MarketingCloudSdk marketingCloudSdk) {
								String strToken = marketingCloudSdk.getPushMessageManager().getPushToken();
								if(strToken==null || "".equals(strToken)){
									marketingCloudSdk.getPushMessageManager().setPushToken(token);
									Logger.printMessage("ApplicationContextProvider", "Setting push token " + token, Logger.INFO);
								}else{
									Logger.printMessage("ApplicationContextProvider", "Push token in Marketing cloud: " + strToken, Logger.INFO);
								}

							}
						});

					}
				});

	}
}
