package uk.co.wiggle.hybrid.application.datamodel.objects;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Chain Reaction Cycles / Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Chain Reaction Cycles / Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class ObjGeneric extends CustomObject {

	public static final String OBJECT_NAME = "generic_object";

	public static final String FIELD_NAME = "name";

	private static List<String> sFieldList;
	static {

		sFieldList = Collections.synchronizedList(new ArrayList<String>());

		//fields used for categories taken from top_level_navigation.json
		sFieldList.add(FIELD_NAME);

	}

	private static ConcurrentHashMap<String, Integer> sFieldTypes;
	static {
		sFieldTypes = new ConcurrentHashMap<String, Integer>();

		sFieldTypes.put(FIELD_NAME, FIELD_TYPE_STRING);

	}

	public ObjGeneric() {
	}

	public ObjGeneric(Bundle bundle) {
		super(bundle);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ObjGeneric)) {
			return false;
		}

		ObjGeneric other = (ObjGeneric) o;
		String otherURI = other.getString(FIELD_NAME);
		String thisURI = this.getString(FIELD_NAME);
		return otherURI.equals(thisURI);
	}

	@Override
	public String getObjectName() {
		return OBJECT_NAME;
	}

	@Override
	public List<String> getFields() {
		return sFieldList;
	}

	@Override
	public Class<?> getSubclass() {
		return ObjGeneric.class;
	}

	@Override
	public Integer getFieldType(String fieldName) {
		return sFieldTypes.get(fieldName);
	}


}