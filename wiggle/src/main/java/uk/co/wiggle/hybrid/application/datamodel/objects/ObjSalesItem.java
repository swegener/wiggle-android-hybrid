package uk.co.wiggle.hybrid.application.datamodel.objects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.utils.DateUtils;


import android.content.Context;
import android.os.Bundle;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class ObjSalesItem extends CustomObject {

	public static final String OBJECT_NAME = "ObjSalesItem";
	
	private ObjSalesOrder mParentRecord;

	//fields used for categories taken from top_level_navigation.json
	public static final String FIELD_PRODUCT_ID = "ProductId";
	public static final String FIELD_PRODUCT_NAME = "ProductName";
	public static final String FIELD_PRODUCT_DESCRIPTION_ID = "ProductDescriptionId";
	public static final String FIELD_MANUFACTURER = "Manufacturer";
	public static final String FIELD_PRODUCT_COLOUR = "ProductColour";
	public static final String FIELD_PRODUCT_SIZE = "ProductSize";
	public static final String FIELD_PRODUCT_URL = "ProductUrl";
	public static final String FIELD_PRODUCT_RETURN_URL = "ProductReturnUrl";
	public static final String FIELD_PRODUCT_PRICE = "ProductPrice";
	public static final String FIELD_PRODUCT_IMAGE_PATH = "ProductImagePath";
	public static final String FIELD_LOCAL_PRODUCT_PRICE_FORMATTED = "LocalProductPriceFormatted";
	public static final String FIELD_LOCAL_LINE_TOTAL_FORMATTED = "LocalLineTotalFormatted";
	public static final String FIELD_QUANTITY_ORDERED = "QtyOrdered"; //should not really be part of the ObjSalesItem - but is for now ... 
	public static final String FIELD_JSON_ARRAY_INDEX = "json_array_index"; //used internally for sorting only .. 
	public static final String FIELD_ORDER_ID_GROUP = "order_id_group"; //used internally for listview group headers
	
	private static List<String> sFieldList;
	static {

		sFieldList = Collections.synchronizedList(new ArrayList<String>());
		
		sFieldList.add(FIELD_ID);
		sFieldList.add(FIELD_PRODUCT_ID);
		sFieldList.add(FIELD_PRODUCT_NAME);
		sFieldList.add(FIELD_PRODUCT_DESCRIPTION_ID);
		sFieldList.add(FIELD_MANUFACTURER);
		sFieldList.add(FIELD_PRODUCT_COLOUR);
		sFieldList.add(FIELD_PRODUCT_SIZE);
		sFieldList.add(FIELD_PRODUCT_URL);
		sFieldList.add(FIELD_PRODUCT_RETURN_URL);
		sFieldList.add(FIELD_PRODUCT_PRICE);
		sFieldList.add(FIELD_PRODUCT_IMAGE_PATH);
		sFieldList.add(FIELD_LOCAL_PRODUCT_PRICE_FORMATTED);
		sFieldList.add(FIELD_LOCAL_LINE_TOTAL_FORMATTED);
		sFieldList.add(FIELD_QUANTITY_ORDERED);
		sFieldList.add(FIELD_JSON_ARRAY_INDEX);
		sFieldList.add(FIELD_ORDER_ID_GROUP);
		
	}

	private static ConcurrentHashMap<String, Integer> sFieldTypes;
	static {
		sFieldTypes = new ConcurrentHashMap<String, Integer>();
		
		sFieldTypes.put(FIELD_ID, FIELD_TYPE_INTEGER);
		sFieldTypes.put(FIELD_PRODUCT_ID, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PRODUCT_NAME, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PRODUCT_DESCRIPTION_ID, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_MANUFACTURER, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PRODUCT_COLOUR, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PRODUCT_SIZE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PRODUCT_URL, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PRODUCT_RETURN_URL, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PRODUCT_PRICE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PRODUCT_IMAGE_PATH, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_LOCAL_PRODUCT_PRICE_FORMATTED, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_LOCAL_LINE_TOTAL_FORMATTED, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_QUANTITY_ORDERED, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_JSON_ARRAY_INDEX, FIELD_TYPE_INTEGER);
		sFieldTypes.put(FIELD_ORDER_ID_GROUP, FIELD_TYPE_STRING);
		
	}

	public ObjSalesItem() {
	}

	public ObjSalesItem(Bundle bundle) {
		super(bundle);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ObjSalesItem)) {
			return false;
		}

		ObjSalesItem other = (ObjSalesItem) o;
		String otherId = other.getString(ObjSalesItem.FIELD_PRODUCT_ID);
		String thisId = this.getString(ObjSalesItem.FIELD_PRODUCT_ID);
		return otherId.equals(thisId);
	}

	@Override
	public String getObjectName() {
		return OBJECT_NAME;
	}

	@Override
	public List<String> getFields() {
		return sFieldList;
	}

	@Override
	public Class<?> getSubclass() {
		return ObjSalesItem.class;
	}

	@Override
	public Integer getFieldType(String fieldName) {
		return sFieldTypes.get(fieldName);
	}
	
	//handler for children of this object
	public void setParentRecord(ObjSalesOrder parent){
		this.mParentRecord = parent;
	}
	
	public ObjSalesOrder getParentRecord(){
		return mParentRecord;
	}
	
	public String getImagePath(String strRequestedImageSize){
		
		//check if this string starts with a domain .. 
		//This is highly specific, so may work for some items but not for others
		//Image path is given e.g. as 
		// "//www.wigglestatic.com/product-media/5360108141/dhb-Bib-Shorts-Lycra-Cycling-Shorts-Black-Grey-NU0381.jpg"
		// so we just prefix this with https:
		//We also suffix the path with parameter e.g. 
		//   ?w=200&amp;h=200&amp;a=0" (this returns the image in 200x200 size)
		//to retrieve the image in a desired size
				
		String strImagePath = getString(FIELD_PRODUCT_IMAGE_PATH);
		String strDisplayImage = strImagePath + strRequestedImageSize;  //return image in axb size to ensure resolution is fine on Android (we also do the same in the text search result)
		
		if(strImagePath==null){
			strDisplayImage = "";
		}else{
			
			if(strDisplayImage.startsWith("//")){
				strDisplayImage = "https:" + strDisplayImage;
			}
			
		}
		
		return strDisplayImage;
		
	}

	public String getProductBrandAndName() {
		
		String strProductName = getString(FIELD_PRODUCT_NAME);
		String strProductBrand = getString(FIELD_MANUFACTURER);
		if(strProductBrand!=null){
			strProductName = strProductBrand + " " + strProductName;
		}
		
		return strProductName;
		
	}
	
	public String getProductVarietyString(){
		
		
		String strVarietyString = "";
		
		String strProductColour = getString(FIELD_PRODUCT_COLOUR);
		if(!"".equals(strProductColour) && strProductColour!=null){
			strVarietyString = strProductColour;
		}
		
		String strProductSize = getString(FIELD_PRODUCT_SIZE);
		if(!"".equals(strProductSize) && strProductSize!=null){
			if(!"".equals(strVarietyString)){
				strVarietyString = strVarietyString + "; ";
			}
			strVarietyString = strVarietyString + strProductSize;
		}
		
		return strVarietyString;
		
	}

	public String getOrderListHeaderString() {
		
		Context context = ApplicationContextProvider.getContext();
		
		String strOrderListHeader = getString(FIELD_ORDER_ID_GROUP);
		
		if("".equals(strOrderListHeader) || strOrderListHeader==null){
			strOrderListHeader = "";
		}else{
			
			//get parent Order ID and order date
			strOrderListHeader = String.format(
					context.getString(R.string.str_order_list_header_group),
					getParentRecord().getString(ObjSalesOrder.FIELD_ORDER_ID),
					DateUtils.getOrderTrackingDate(getParentRecord().getDateFromJson(ObjSalesOrder.FIELD_ORDER_DATE))
					);
			
		}
		
		return strOrderListHeader;
		
		
		
		
	}

	public String getAdditionalInfo(){

		String strProductSize = getString(FIELD_PRODUCT_SIZE);

		// returns a combination of size and colour if available
		String strProductColour = getString(FIELD_PRODUCT_COLOUR);
		if(strProductColour!=null && !"".equals(strProductColour)){
			if(strProductSize!=null && !"".equals(strProductSize)){
				return strProductSize + ", " + strProductColour;
			}else{
				return strProductColour;
			}
		}

		return strProductSize;


	}
	

	

}