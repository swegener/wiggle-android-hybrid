package uk.co.wiggle.hybrid.application.datamodel.objectmanagers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import uk.co.wiggle.hybrid.api.parsers.JsonObjectParser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjTypeaheadResponse;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjUserSearch;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.application.persistence.DataStore;
import uk.co.wiggle.hybrid.application.utils.DateUtils;


public class ObjRecentSearchesManager {

	private static final String logTag = "ObjRecentSearchesManager";
	
	List<ObjUserSearch> mRecentSearches = Collections.synchronizedList(new ArrayList<ObjUserSearch>());
	
	private static ObjRecentSearchesManager sInstance = null;

	public static ObjRecentSearchesManager getInstance() {
		if (sInstance == null) {
			sInstance = new ObjRecentSearchesManager();
		}
		return sInstance;
	}

	protected ObjRecentSearchesManager() {
		mRecentSearches = Collections.synchronizedList(new ArrayList<ObjUserSearch>());
	}
	
	
	/**
	 * @param location
	 * @param object
	 * @see List#add(int, Object)
	 */
	public void add(int location, ObjUserSearch object) {
		mRecentSearches.add(location, object);
	}
	
	
	/**
	 * @param object
	 * @return
	 * @see List#add(Object)
	 */
	public boolean add(ObjUserSearch object) {
		return mRecentSearches.add(object);
	}

	/**
	 * @param arg0
	 * @return
	 * @see List#addAll(Collection)
	 */
	public boolean addAll(Collection<? extends ObjUserSearch> arg0) {
		return mRecentSearches.addAll(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 * @see List#addAll(int, Collection)
	 */
	public boolean addAll(int arg0, Collection<? extends ObjUserSearch> arg1) {
		return mRecentSearches.addAll(arg0, arg1);
	}

	/**
	 * 
	 * @see List#clear()
	 */
	public void clear() {
		mRecentSearches.clear();
	}

	/**
	 * @param object
	 * @return
	 * @see List#contains(Object)
	 */
	public boolean contains(Object object) {
		return mRecentSearches.contains(object);
	}

	/**
	 * @param object
	 * @return
	 * @see List#equals(Object)
	 */
	public boolean equals(Object object) {
		return mRecentSearches.equals(object);
	}

	/**
	 * @param location
	 * @return
	 * @see List#get(int)
	 */
	public ObjUserSearch get(int location) {
		return mRecentSearches.get(location);
	}
	
	public void set(Collection<? extends ObjUserSearch> arg0) {
		mRecentSearches.clear();
		mRecentSearches.addAll(arg0);
	}
	
	/**
	 * @param object
	 * @return
	 * @see List#indexOf(Object)
	 */
	public int indexOf(Object object) {
		return mRecentSearches.indexOf(object);
	}

	/**
	 * @param object
	 * @return
	 * @see List#remove(Object)
	 */
	public boolean remove(Object object) {
		return mRecentSearches.remove(object);
	}
	
	public ObjUserSearch remove(int index) {
		return mRecentSearches.remove(index);
	}

	/**
	 * @param
	 * @return
	 * @see List#removeAll(Collection)
	 */
	public void removeAll() {
		mRecentSearches.clear();
	}

	/**
	 * @return
	 * @see List#size()
	 */
	public int size() {
		return mRecentSearches.size();
	}

	public List<ObjUserSearch> getAll() {
		return mRecentSearches;
	}

	public boolean isEmpty() {
		return mRecentSearches.isEmpty();
	}
	
	public String toString(){
		return mRecentSearches.toString();
	}
	
	
	/** 
	 * 
	 * Convenience methods for easy access to data in this manager
	 * 
	 */

	public List<ObjUserSearch> getOrderedRecentSearches(){


		List<ObjUserSearch> lstRecentSearches = getAll();

		//sort events
		SortRecentSearches sortLocations = new SortRecentSearches();
		Collections.sort(lstRecentSearches, sortLocations);


		return lstRecentSearches;

	}


	//returns ObjTypeaheadResponse records (converted ObjUserSearch for use on FragmentShopNavigation)
	public List<ObjTypeaheadResponse> getRecentSearchesForDisplay(){


		List<ObjUserSearch> lstRecentSearches = getAll();

		//sort recent searches
		SortRecentSearches sortRecentSearches = new SortRecentSearches();
		Collections.sort(lstRecentSearches, sortRecentSearches);

		//in order to work on FragmentShopNavigation, recent searches need to be returned
		// in a certain format as ObjTypeaheadResponse
		List<ObjTypeaheadResponse> displayList = new ArrayList<ObjTypeaheadResponse>();
		for(int i=0;i<lstRecentSearches.size();i++){
			ObjUserSearch search = lstRecentSearches.get(i);
			if(search!=null) {
				ObjTypeaheadResponse convertedRecentSearch = search.convertToTypeaheadResponse();
				displayList.add(convertedRecentSearch);
			}
		}


		return displayList;

	}

	
	//can be used to sort individual locations by "created" (i.e. user selected) date
	class SortRecentSearches implements Comparator<ObjUserSearch> {

		@Override
		public int compare(ObjUserSearch location1, ObjUserSearch location2) {

			//Order by date descending
			Date search1LastUsedDate = location1.getLastSelectedDate();
			Date search2LastUsedDate = location2.getLastSelectedDate();


			if(search1LastUsedDate.equals(search2LastUsedDate)){

				//TODO: same event dates? What sort order do we use?
				/*
				int item1OrderIndex = item1.getInt(ObjSalesItem.FIELD_JSON_ARRAY_INDEX);
				int item2OrderIndex = item1.getInt(ObjSalesItem.FIELD_JSON_ARRAY_INDEX);
				
				return item1OrderIndex - item2OrderIndex;
				*/
			}

			//this sorts by date descending - newest orders are on top of list
			return search2LastUsedDate.compareTo(search1LastUsedDate);
			
		}

	}

	public void addIfMissing(ObjUserSearch selectedSearch){

		ObjUserSearch existingSearch = null;

		int index = mRecentSearches.indexOf(selectedSearch);
		if(index>-1) {
			existingSearch = mRecentSearches.get(index);
		}

		if(existingSearch!=null){
			existingSearch.setField(ObjUserSearch.FIELD_SELECTED_DATE, DateUtils.dateToInternalString(new Date()));
		}else{
			//we did not find this search in our list? Add it ..
			mRecentSearches.add(selectedSearch);
		}


		int intMaxSearchesToKeepInHistory = 10;

		//Also we only keep the most recent 10 recent searched - so .. anything above that knocks the last recent search off the list
		if(mRecentSearches.size()>intMaxSearchesToKeepInHistory){
			List<ObjUserSearch> orderedSearches = getOrderedRecentSearches();
			//bottom element of the list is the one that can go as it is the oldest one
			ObjUserSearch oldestSearch = orderedSearches.get(orderedSearches.size()-1);
			//remove this item from our manager
			mRecentSearches.remove(oldestSearch);
		}

		//persist recent searches list
		persistData();

	}

	public void persistData(){


		JSONArray recentSearchesArray = new JSONArray();

		try {

			for (int i = 0; i < mRecentSearches.size(); i++) {
				ObjUserSearch mySearch = mRecentSearches.get(i);
				JSONObject searchJson = mySearch.createJSONfromObject(mySearch, new ObjUserSearch());
				if(searchJson!=null){
					recentSearchesArray.put(searchJson);
				}
			}

			//store json array String
			String strRecentSearchJson = recentSearchesArray.toString();

			Logger.printMessage("recent search", strRecentSearchJson, Logger.ERROR);
			DataStore.saveStringToSharedPreferences(logTag, "persistedData", strRecentSearchJson);

		}catch(JSONException jsonException){
			Logger.printMessage(logTag, "persistData() failed: " + jsonException.getMessage(), Logger.WARN);
		}

	}


	public void loadPersistedData(){

		//Get data from shared preferences
		String strRecentSearchJson = DataStore.getStringFromSharedPreferences(logTag, "persistedData");
		if(strRecentSearchJson!=null && !"".equals(strRecentSearchJson)){

			try {
				JsonObjectParser.instance().parseRecentSearchesJsonArray(strRecentSearchJson);
			} catch (JSONException e) {
				Logger.printMessage(logTag, "loadPersistedData(): " + e.getMessage(), Logger.DEBUG);
			}

		}

	}

	public void clearPersistedData(){

		clear();
		DataStore.saveStringToSharedPreferences(logTag, "persistedData", "");

	}


}
