package uk.co.wiggle.hybrid.application.datamodel.objects;

import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class ObjMegamenuItem extends CustomObject {

	public static final String OBJECT_NAME = "MegamenuItem";

	public static final String FIELD_URL = "Url";
	public static final String FIELD_TEXT = "Text";
	public static final String FIELD_DATA_MAP = "DataMap"; //the unique key per country code
	public static final String FIELD_HAS_CHILDREN = "HasChildren";
	public static final String FIELD_COUNTRY_CODE = "CountryCode"; //from api call

	private static List<String> sFieldList;
	static {
		sFieldList = Collections.synchronizedList(new ArrayList<String>());
		sFieldList.add(FIELD_URL);
		sFieldList.add(FIELD_TEXT);
		sFieldList.add(FIELD_DATA_MAP);
		sFieldList.add(FIELD_HAS_CHILDREN);
		sFieldList.add(FIELD_COUNTRY_CODE);
	}

	private static ConcurrentHashMap<String, Integer> sFieldTypes;
	static {
		sFieldTypes = new ConcurrentHashMap<String, Integer>();

		sFieldTypes.put(FIELD_URL, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_TEXT, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_DATA_MAP, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_HAS_CHILDREN, FIELD_TYPE_BOOLEAN);
		sFieldTypes.put(FIELD_COUNTRY_CODE, FIELD_TYPE_INTEGER);

	}

	public ObjMegamenuItem() {

	}

	public ObjMegamenuItem(Bundle bundle) {
		super(bundle);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ObjMegamenuItem)) {
			return false;
		}

		ObjMegamenuItem other = (ObjMegamenuItem) o;
		int otherCountryCode = other.getInt(ObjMegamenuItem.FIELD_COUNTRY_CODE);
		int thisCountryCode = this.getInt(ObjMegamenuItem.FIELD_COUNTRY_CODE);
		String otherDataMapId = other.getString(ObjMegamenuItem.FIELD_DATA_MAP);
		String thisDataMapId = this.getString(ObjMegamenuItem.FIELD_DATA_MAP);
		//Country code and data map Id must match
		return (otherCountryCode == thisCountryCode) && otherDataMapId.equals(thisDataMapId);
	}
	
	@Override
	public String getObjectName() {
		return OBJECT_NAME;
	}
	
	@Override
	public List<String> getFields() {
		return sFieldList;
	}
	
	@Override
	public Class<?> getSubclass() {
		return ObjMegamenuItem.class;
	}
	
	@Override
	public Integer getFieldType(String fieldName) {
		return sFieldTypes.get(fieldName);
	}
	
	
	public static String createJSONfromName(ObjMegamenuItem object , CustomObject targetObject) throws JSONException{
		
		JSONArray mainArray = new JSONArray();
		List<String> fields = targetObject.getFields();
			
		JSONObject jsonObject = new JSONObject();
			
			// first do the flat fields
			for (String strFieldName : fields) {
				
				if(object.get(strFieldName) != null){
					
					int intValueFieldType = targetObject.getFieldType(strFieldName);
					
					switch (intValueFieldType) {
					
					case CustomObject.FIELD_TYPE_OBJECT:
						break;
					
					case CustomObject.FIELD_TYPE_DATETIME:
						break;
					
					default:
						jsonObject.put(strFieldName, object.get(strFieldName));
						break;
					}
					
					
				}
				
			}
			
			mainArray.put(jsonObject);
			
		
		return mainArray.toString();
		
	}
	

		
}

