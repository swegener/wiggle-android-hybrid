package uk.co.wiggle.hybrid.application.datamodel.objects;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class ObjectTypeHelper {
	public static Boolean parseBoolean(String value) {
		value = value.trim();
		if (value.equals("0")) {
			return false;
		} else if (value.equals("1")) {
			return true;
		} else if (value.equals("true")) {
			return true;
		} else if (value.equals("false")) {
			return false;
		} else {
			return Boolean.parseBoolean(value);
		}
	}
	
	public static Boolean parseBoolean(int value) {
		
		return value==1; //only 1 is true, everything else is deemed to be false 
		
	}	
}
