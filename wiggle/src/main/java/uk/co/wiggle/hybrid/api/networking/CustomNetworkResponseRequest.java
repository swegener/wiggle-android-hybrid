package uk.co.wiggle.hybrid.api.networking;

/**
 * @author AndroMedia
 * 
 * Custom JSON Object request for use with Volley. This supports passing Header and 
 * Authentication information to a web API and enables us to send parameters with POST requests.
 * 
 */

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import uk.co.wiggle.hybrid.application.logging.Logger;

public class CustomNetworkResponseRequest extends Request<NetworkResponse> {
	
	private String logTag = "CustomNetworkResponseRequest";

    private Listener<NetworkResponse> listener;
    private Map<String, String> params;
    private Map<String, String> headerParams;
    private CustomHttpParams  mMultisetParams;
    private Map<String, File>   multipartFileList = new HashMap<String,File>(); //the "Jobs" section supports the upload of a CV file
    private Priority mPriority = Priority.IMMEDIATE; //all volley requests need to be executed immediately - if you want to change this for a given request simply call DuibzzleJsonRequest.setPriority(Priority.xxx);
    

    public CustomNetworkResponseRequest(int method, 
    								 String url, 
    								 Map<String, String> params, 
    								 Map<String, String> headerParams, 
    								 CustomHttpParams multisetParams, 
    								 Map<String, File> multipartFileList, 
    								 Listener<NetworkResponse> reponseListener, 
    								 ErrorListener errorListener) {
        super(method, url, errorListener);
        this.listener = reponseListener;
        this.params = params;
        this.headerParams = headerParams;
        this.mMultisetParams = multisetParams;
        Logger.printMessage(logTag, "Method: " + method + " URL: " + url, Logger.INFO);
    }
    
    /** AndroMedia: Please note that this method is ONLY called for POST and PUT by Volley. Any GET parameters need to be appended to the URL **/
    @Override
    protected Map<String, String> getParams()
            throws AuthFailureError {
    	Logger.printMessage(logTag, "Params: " + params.toString(), Logger.INFO);
        return params;
    };
    
    @Override
    public Map<String, String> getHeaders() 
    		throws AuthFailureError {
    	Logger.printMessage(logTag, "Header: " + headerParams.toString(), Logger.INFO);
		headerParams.put(AppRequestHandler.HEADER_ACCEPT_ENCODING, AppRequestHandler.ENCODING_GZIP);
    	return headerParams;
    }

    @Override
    protected Response<NetworkResponse> parseNetworkResponse(NetworkResponse response) {
        
    	try {
        	//return the raw response so that we can parse its data stream
            return Response.success(response, HttpHeaderParser.parseCacheHeaders(response));
        } catch (Exception e) {
            return Response.error(new ParseError(e));
        }
        
        
    }

    @Override
    protected void deliverResponse(NetworkResponse response) {
        // TODO Auto-generated method stub
        listener.onResponse(response);
    }
    
    /**
     *  AndroMedia: in case the server returns an error, valid JSON  
     */
    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError){
    	
    	if(volleyError.networkResponse != null && volleyError.networkResponse.data != null){
    		
            VolleyError error = new VolleyError(new String(volleyError.networkResponse.data));
            volleyError = error;
            
        }

    	return volleyError;
    }
    
    
    /**
     * 
     * @author Andromedia
     * 
     * The Dubizzle Kombi API listing endpoint supports sending a list of identical tags (categories).
     * This method in Combination with class DubizzleHttpParams extends the volley library to support this.
     * 
     * Please note that this method is ONLY called for POST and PUT by Volley. Any GET parameters need to be appended to the URL
     *
     */    
    /* De-activated as this is not used within the Dubizzle API - activating this overwrites the standard parameters returned by "getParams"
    @Override
    public byte[] getBody() throws AuthFailureError {
        Logger.printMessage(logTag, "Body params:" + mMultisetParams.toString(), Logger.INFO);
        return mMultisetParams.encodeParameters(getParamsEncoding());
    }   
    */
    	
    
    public void addMultipartFile(String param,File file) {
        multipartFileList.put(param,file);
    }

    public Map<String,File> getMultipartFiles() {
        return multipartFileList;
    }
    
    @Override
    public Priority getPriority() {
        return mPriority;
    }

    public void setPriority(Priority priority) {
        mPriority = priority;
    }
    
    

}
