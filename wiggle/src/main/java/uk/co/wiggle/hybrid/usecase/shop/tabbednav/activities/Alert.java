package uk.co.wiggle.hybrid.usecase.shop.tabbednav.activities;


import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;


import java.util.Date;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjApiRequest;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjGeneric;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.application.utils.DateUtils;
import uk.co.wiggle.hybrid.interfaces.IAppActivityUI;


public class Alert extends Activity implements View.OnClickListener, IAppActivityUI {

	RelativeLayout alert_lytAlertCanvas;

	TextView alert_alertTitle;
	TextView alert_alertDetail;
	ImageView alert_closeButton;

	LinearLayout alert_lytReviewPrompt;
	TextView reviewPrompt_tvPositive;
	TextView reviewPrompt_tvNegative;

	LinearLayout alert_lytRateUs;
	TextView rateUs_tvPositive;
	TextView rateUs_tvNeutral;
	TextView rateUs_tvNegative;

	LinearLayout alert_lytFeedback;
	EditText feedback_etVerbatim;
	ProgressBar feedback_pbSubmitInProgress;
	TextView feedback_tvSubmit;
	RelativeLayout feedback_btnSubmit;

	LinearLayout alert_lytUpgradeRequired;

	TextView alert_tvCancel;
	TextView alert_tvSubmit;
	RelativeLayout alert_topButtons;
	
	//allow other activities to read an instance of MainActivity so that MainActivity methods can be called
	private static Alert myInstance;

	public static Alert instance(){
		return myInstance;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		try{
			super.onCreate(savedInstanceState);
		}catch(IllegalStateException e){
			return;
		}
		
		// No title in window.
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		//Fullscreen Activity
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);


		setContentView(R.layout.generic_dialog);
		
		//AppUtils.setRandomBlurredBackground(this, 180);
		View viewToBlur = findViewById(android.R.id.content).getRootView();
		//AppUtils.setBlurredBackground(this, viewToBlur, 180, R.drawable.scenery08);
		//View viewToBlur = findViewById(R.id.alert_lytAlertCanvas);
		//AppUtils.setBlurredBackground(this, viewToBlur, 180, R.drawable.scenery04);
		
		myInstance = this;
		
		findViews();
		
		setCustomFontTypes();

		displayLayout(R.id.alert_lytReviewPrompt);

		//store the date of this ..
		String strSystemDate = DateUtils.dateToInternalString(new Date());
		ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_APP_REVIEW_PROMPT_DISPLAY_DATE, strSystemDate);


	}
	
	public void displayLayout(int intLayoutId){

		alert_lytReviewPrompt.setVisibility(View.GONE);
		alert_lytRateUs.setVisibility(View.GONE);
		alert_lytFeedback.setVisibility(View.GONE);
		alert_lytUpgradeRequired.setVisibility(View.GONE);

		alert_lytAlertCanvas.setVisibility(View.VISIBLE);

		//set alert title and text accordingly
		if(intLayoutId==R.id.alert_lytReviewPrompt){
			alert_alertTitle.setText(R.string.str_app_review_prompt_title);
			alert_alertDetail.setText(R.string.str_app_review_prompt_message);
			alert_closeButton.setVisibility(View.GONE);
			alert_topButtons.setVisibility(View.GONE);
		}else if(intLayoutId==R.id.alert_lytRateUs){
			alert_alertTitle.setText(R.string.str_rate_us_dialog_title);
			alert_alertDetail.setText(R.string.str_rate_us_dialog_message);
			alert_closeButton.setVisibility(View.GONE);
			alert_topButtons.setVisibility(View.GONE);
		}else if(intLayoutId==R.id.alert_lytFeedback){
			alert_alertTitle.setText(R.string.str_feedback_dialog_title);
			alert_alertDetail.setText(R.string.str_feedback_dialog_message);
			//hide submit button initially
			feedback_btnSubmit.setVisibility(View.GONE);
			alert_tvSubmit.setVisibility(View.GONE);
			alert_closeButton.setVisibility(View.GONE);
			alert_topButtons.setVisibility(View.VISIBLE);

		}

		View layout = findViewById(intLayoutId);
		layout.setVisibility(View.VISIBLE);
		
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

			case R.id.alert_tvSubmit:
			case R.id.feedback_btnSubmit:

				//We don't do any processing but want to react on the request code
				//So simply set the result equal to request code. This will trigger onActivityResult on the calling activity.
				//setResult(intActionForOKButton);
				//alertDisappear();
				//closeAlert();
				submitFeedback();
				break;
			case R.id.reviewPrompt_tvPositive:
				displayLayout(R.id.alert_lytRateUs);
				break;

			case R.id.reviewPrompt_tvNegative:
				ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_APP_REVIEW_PROMPT_RESPONSE, ObjAppUser.CONST_THUMBS_DOWN);
				displayLayout(R.id.alert_lytFeedback);
				break;

			case R.id.rateUs_tvPositive:
				rateApp();
				ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_APP_REVIEW_PROMPT_RESPONSE, ObjAppUser.CONST_RATE_US_PROMPT_ACCEPTED);
				closeAlert();
				break;

			case R.id.rateUs_tvNeutral:
				ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_APP_REVIEW_PROMPT_RESPONSE, ObjAppUser.CONST_RATE_US_PROMPT_LATER);
				closeAlert();
				break;

			case R.id.rateUs_tvNegative:
				ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_APP_REVIEW_PROMPT_RESPONSE, ObjAppUser.CONST_RATE_US_PROMPT_DECLINED);
				closeAlert();
				break;

			case R.id.alert_tvCancel:
			case R.id.alert_closeButton:
				closeAlert();
				break;

			default:
				break;

		}
		
	}	
	
	@Override
	public void onBackPressed() {

		super.onBackPressed();
		//closeAlert();
		
	}
	
	public void closeAlert(){

		hideVirtualKeyboard();
		
		myInstance = null;
		finish();
		overridePendingTransition(0, R.anim.fade_out);
		
	}

	protected void hideVirtualKeyboard(){

		InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		im.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

	}
	
    public void findViews() {

		alert_lytAlertCanvas = (RelativeLayout) findViewById(R.id.alert_lytAlertCanvas);

		alert_alertTitle = (TextView) findViewById(R.id.alert_alertTitle);
		alert_alertDetail = (TextView) findViewById(R.id.alert_alertDetail);
		alert_closeButton = (ImageView) findViewById(R.id.alert_closeButton);

		alert_lytReviewPrompt = (LinearLayout) findViewById(R.id.alert_lytReviewPrompt);
		reviewPrompt_tvPositive = (TextView) findViewById(R.id.reviewPrompt_tvPositive);
		reviewPrompt_tvNegative = (TextView) findViewById(R.id.reviewPrompt_tvNegative);

		alert_lytRateUs = (LinearLayout) findViewById(R.id.alert_lytRateUs);
		rateUs_tvPositive = (TextView) findViewById(R.id.rateUs_tvPositive);
		rateUs_tvNeutral = (TextView) findViewById(R.id.rateUs_tvNeutral);
		rateUs_tvNegative = (TextView) findViewById(R.id.rateUs_tvNegative);

		alert_lytFeedback = (LinearLayout) findViewById(R.id.alert_lytFeedback);
		alert_lytUpgradeRequired = (LinearLayout) findViewById(R.id.alert_lytUpgradeRequired);
		feedback_etVerbatim = (EditText) findViewById(R.id.feedback_etVerbatim);
		feedback_pbSubmitInProgress = (ProgressBar) findViewById(R.id.feedback_pbSubmitInProgress);
		feedback_tvSubmit = (TextView) findViewById(R.id.feedback_tvSubmit);
		feedback_btnSubmit = (RelativeLayout) findViewById(R.id.feedback_btnSubmit);

		alert_tvCancel = (TextView) findViewById(R.id.alert_tvCancel);
		alert_tvSubmit = (TextView) findViewById(R.id.alert_tvSubmit);
		alert_topButtons = (RelativeLayout) findViewById(R.id.alert_topButtons);
        
        //react to enter button on softkeyboard
		feedback_etVerbatim.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                switch (actionId) {

                    case EditorInfo.IME_ACTION_DONE:
						hideVirtualKeyboard();
                        return true;

                    default:
                        return false;
                }
            }

        });

		feedback_etVerbatim.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

				if (charSequence.length() >0) {
					feedback_btnSubmit.setVisibility(View.VISIBLE);
					alert_tvSubmit.setVisibility(View.VISIBLE);
				}else{
					feedback_btnSubmit.setVisibility(View.GONE);
					alert_tvSubmit.setVisibility(View.GONE);
				}

			}

			@Override
			public void afterTextChanged(Editable editable) {

			}
		});
        
        
    }

	@Override
	public void setCustomFontTypes(){
		
		//Alert
		alert_alertTitle.setTypeface(AppTypefaces.instance().fontMainBold);
		alert_alertDetail.setTypeface(AppTypefaces.instance().fontMainRegular);
		reviewPrompt_tvPositive.setTypeface(AppTypefaces.instance().fontMainRegular);
		reviewPrompt_tvNegative.setTypeface(AppTypefaces.instance().fontMainRegular);
		rateUs_tvPositive.setTypeface(AppTypefaces.instance().fontMainRegular);
		rateUs_tvNeutral.setTypeface(AppTypefaces.instance().fontMainRegular);
		rateUs_tvNegative.setTypeface(AppTypefaces.instance().fontMainRegular);
		feedback_etVerbatim.setTypeface(AppTypefaces.instance().fontMainRegular);
		feedback_tvSubmit.setTypeface(AppTypefaces.instance().fontMainRegular);
		alert_tvCancel.setTypeface(AppTypefaces.instance().fontMainBold);
		alert_tvSubmit.setTypeface(AppTypefaces.instance().fontMainBold);

	}
	
	@Override
	public void setupCustomActionBar(){
		
		//not applicable to alert
		
	}
	
	
	@Override
	public void onStop(){
		super.onStop();
	}	
	
	@Override
	public void onResume(){
		super.onResume();
	}	


	private void submitFeedback(){

		hideVirtualKeyboard(); //if not already hidden

		//sending in progress? Do nothing and wait for result
		if(feedback_pbSubmitInProgress.getVisibility()==View.VISIBLE){
			return;
		}

		feedback_pbSubmitInProgress.setVisibility(View.VISIBLE);
		feedback_etVerbatim.setEnabled(false);
		feedback_tvSubmit.setText(getString(R.string.main_tvPleaseWait));
		alert_tvSubmit.setText(getString(R.string.main_tvPleaseWait));

		String strFeedback = feedback_etVerbatim.getText().toString();

		//Using our generic object to pass the feedback to AppRequestHandler
		ObjGeneric mailObject = new ObjGeneric();
		mailObject.setField(ObjGeneric.FIELD_NAME, strFeedback);

		new ObjApiRequest(this,
				null,
				AppRequestHandler.ENDPOINT_MAILGUN,
				Request.Method.POST,
				mailObject,
				null,
				AppRequestHandler.ON_RESPONSE_ACTION.NO_ACTION)
				.submit();

	}

	public void showFeedbackEmailError(){

		AppUtils.showCustomToast(getString(R.string.feedback_email_submission_error), false);
		feedback_pbSubmitInProgress.setVisibility(View.GONE);
		feedback_etVerbatim.setEnabled(true);
		feedback_tvSubmit.setText(getString(R.string.feedback_tvSubmit));


	}

	public void showFeedbackEmailSuccess(){

		AppUtils.showCustomToast(getString(R.string.feedback_email_submission_success), false);
		feedback_pbSubmitInProgress.setVisibility(View.GONE);
		feedback_etVerbatim.setEnabled(true);
		feedback_tvSubmit.setText(getString(R.string.feedback_tvSubmit));
		feedback_etVerbatim.setText("");
		closeAlert();

	}

	/*
	* Start with rating the app
	* Determine if the Play Store is installed on the device
	*
	* */
	public void rateApp()
	{
		try
		{
			Intent rateIntent = rateIntentForUrl("market://details");
			startActivity(rateIntent);
		}
		catch (ActivityNotFoundException e)
		{
			Intent rateIntent = rateIntentForUrl("https://play.google.com/store/apps/details");
			startActivity(rateIntent);
		}
	}

	private Intent rateIntentForUrl(String url)
	{
		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url, getPackageName())));
		int flags = Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK;
		if (Build.VERSION.SDK_INT >= 21)
		{
			flags |= Intent.FLAG_ACTIVITY_NEW_DOCUMENT;
		}
		else
		{
			//noinspection deprecation
			flags |= Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
		}
		intent.addFlags(flags);
		return intent;
	}

}