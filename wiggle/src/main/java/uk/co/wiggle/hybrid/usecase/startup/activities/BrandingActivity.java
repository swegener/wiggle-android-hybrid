package uk.co.wiggle.hybrid.usecase.startup.activities;

import android.annotation.SuppressLint;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.animations.AppAnimations;

public class BrandingActivity extends Activity {

    RelativeLayout welcome_lytBranding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_branding);

        welcome_lytBranding = findViewById(R.id.welcome_lytBranding);
        welcome_lytBranding.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    welcome_lytBranding.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    welcome_lytBranding.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                showBrandingForDefinedTime(100, 700);
            }
        });

    }



    private void showBrandingForDefinedTime(int fadeInTimeInMs, int intTotalDisplayTimeInMs){


        AppAnimations.getInstance().startFadeInAnimationWithDurationInMs(fadeInTimeInMs, welcome_lytBranding);
        //AppAnimations.getInstance().doCircleReveal(welcome_lytBranding, AppAnimations.CENTER, AppAnimations.CENTER,1 );

        //A manged branding screen
        new CountDownTimer(intTotalDisplayTimeInMs, 100) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                launchWelcomeActivity();
            }

        }.start();

    }

    private void launchWelcomeActivity(){

        Intent intent = new Intent(this, WelcomeActivity.class);
        //intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION); //this makes sure activities do not slide in from right to left when started, but simply appear
        startActivityForResult(intent, 0);

        //finish this activity
        finish();

    }

}