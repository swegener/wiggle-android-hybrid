package uk.co.wiggle.hybrid.interfaces;


import uk.co.wiggle.hybrid.application.datamodel.objects.CustomObject;

/**
 * @author Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 *
 *  All Data Capture fragments should implement this interface
 *
 * @license The code of this Application is licensed to Wiggle UK Ltd.
 * The license and its ownership rights can not be transferred to a Third Party.
 *
 * @android If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 **/


public interface IDataCaptureForm {

	public boolean  isFormDataValid();

	public boolean collectFormData(CustomObject dataObject);

	public void clearFormData();

}
