package uk.co.wiggle.hybrid.usecase.shop.tabbednav.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjTypeaheadResponse;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.extensions.PicassoSSL;
import uk.co.wiggle.hybrid.tagging.AppTagManager;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.activities.TabbedHybridActivity;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.fragments.FragmentShopNavigation;


/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/


public class TabbedTextSearchListAdapter extends BaseAdapter{
	
	private String logTag = "TabbedTextSearchListAdapter";

	private List<ObjTypeaheadResponse> mTypeheadResponses = new ArrayList<ObjTypeaheadResponse>();
	private int lastPosition;
	
	public TabbedTextSearchListAdapter() {
		
	}

	public void setListContent(List<ObjTypeaheadResponse> ObjResortList) {
		mTypeheadResponses = ObjResortList;		
	}

	@Override
	public int getCount() {
		return mTypeheadResponses.size();
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}
	

	@Override
	public View getView(int arg0, View view, ViewGroup arg2) 
	{
		final int position = arg0;
		
		final ObjTypeaheadResponse record = mTypeheadResponses.get(arg0);
		
		Logger.printMessage(logTag, "ObjTypeaheadResponse: " + record.toXML(), Logger.ALL);
		
		LayoutInflater inflater = LayoutInflater.from(ApplicationContextProvider.getContext());

		//add dummy footer view for dummy item ...
		if("DummyFooter".equals(record.getString(ObjTypeaheadResponse.FIELD_DISPLAY_TYPE))){
			view = inflater.inflate(R.layout.shop_text_search_footer_item, null);
			return view;
		}else {
			//otherwise: inflate record layout
			view = inflater.inflate(R.layout.text_search_item_tabbed_nav, null);
		}
		
		TextView tvGroupHeader = (TextView) view.findViewById(R.id.textSearch_tvGroupHeader);
		tvGroupHeader.setTypeface(AppTypefaces.instance().fontMainBold);
		
		RelativeLayout layoutTextSearchItem = (RelativeLayout) view.findViewById(R.id.layoutTextSearchItem);
		
		TextView tvName = (TextView) view.findViewById(R.id.textSearch_tvName);
		tvName.setTypeface(AppTypefaces.instance().fontMainRegular);

		TextView textSearch_tvButton = (TextView) view.findViewById(R.id.textSearch_tvButton);
		textSearch_tvButton.setTypeface(AppTypefaces.instance().fontMainRegular);
		textSearch_tvButton.setVisibility(View.GONE);

		View textSearch_vSeparator = view.findViewById(R.id.textSearch_vSeparator);
		textSearch_vSeparator.setVisibility(View.VISIBLE);
		
		ImageView ivIcon = (ImageView) view.findViewById(R.id.textSearch_ivIcon);
		//Wiggle requirement - hide icons. We leave the code below just in case they want this enabled again
		if(AppUtils.getWiggleVersion().getVersionCode()>AppUtils.APP_VERSION.VERSION_1_HYBRID.getVersionCode()){
			ivIcon.setVisibility(View.GONE);
		}else {


			String strIcon = record.getString(ObjTypeaheadResponse.FIELD_LOGO);
			if (strIcon != null && !"".equals(strIcon)) {
				String strIconURL = ApplicationContextProvider.getContext().getString(R.string.str_url_wiggle_image_home)
						+ strIcon;
				PicassoSSL.with(ApplicationContextProvider.getContext()).load(strIconURL).into(ivIcon);
			} else {

				//try alternative route for images
				//This uses the product ID to form a URL like
				//http://ajax.wiggle.co.uk/images/MainImage/5360092917?w=30&h=30&a=7

				String strPDID = record.getString(ObjTypeaheadResponse.FIELD_PDID);

				//empty or "0" PDID - do not show an image at all (this is the case for categories at the moment)
				if (strPDID != null && !"".equals(strPDID) && !"0".equals(strPDID)) {

					String strAlternativeImageURL =
							AppUtils.getWiggleURL(ApplicationContextProvider.getContext().getString(R.string.str_url_wiggle_image_home_2))
									+ strPDID
									+ "?w=200&amp;h=200&amp;a=0"; //return image in 200x200 size to ensure resolution is fine on Android

					PicassoSSL.with(ApplicationContextProvider.getContext()).load(strAlternativeImageURL).into(ivIcon);

				}

			}

		}
		

		//group header row (outside view holder pattern)
		String strDisplayType = record.getString(ObjTypeaheadResponse.FIELD_DISPLAY_TYPE);
		
		if(ObjTypeaheadResponse.CONST_DISPLAY_TYPE_PARENT.equals(strDisplayType)) {

			String strTitle = record.getString(ObjTypeaheadResponse.FIELD_NAME);
			tvGroupHeader.setText(strTitle);
			tvGroupHeader.setVisibility(View.VISIBLE);
			layoutTextSearchItem.setVisibility(View.GONE);

			//No separator on "Your recent searches" item ..
			if(ApplicationContextProvider.getContext().getString(R.string.tab_shop_your_recent_searches)
					.equals(strTitle)){
				textSearch_vSeparator.setVisibility(View.GONE);
			}else{
				textSearch_vSeparator.setVisibility(View.VISIBLE);
			}

		}else if(ObjTypeaheadResponse.CONST_DISPLAY_TYPE_BUTTON.equals(strDisplayType)){

			textSearch_tvButton.setText(record.getString(ObjTypeaheadResponse.FIELD_NAME));
			textSearch_tvButton.setVisibility(View.VISIBLE);
			layoutTextSearchItem.setVisibility(View.GONE);
			tvGroupHeader.setVisibility(View.GONE);

			textSearch_tvButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					FragmentShopNavigation.instance().clearRecentSearches();
				}
			});

		}else{
					
			tvGroupHeader.setVisibility(View.GONE);
			layoutTextSearchItem.setVisibility(View.VISIBLE);
			textSearch_tvButton.setVisibility(View.GONE);
			
			tvName.setText(record.getString(ObjTypeaheadResponse.FIELD_NAME));
			
			
			//Animation animation = AnimationUtils.loadAnimation(ApplicationContextProvider.getContext(), (position > lastPosition) ? R.anim.listview_anim_up_from_bottom : R.anim.listview_anim_down_from_top);
			//view.startAnimation(animation);
			lastPosition = position;
			
			layoutTextSearchItem.setOnClickListener(new OnClickListener(){

				@Override
				public void onClick(View v) {

					final String strItemType = record.getString(ObjTypeaheadResponse.FIELD_CATEGORY_TYPE);

					//Is this a saved search item? Fire off Ajax search request ..
					if(ObjTypeaheadResponse.CONST_RESULT_TYPE_SAVED_SEARCH.equals(strItemType)){
						String strSearchTerm = record.getString(ObjTypeaheadResponse.FIELD_NAME);
						FragmentShopNavigation.instance().useRecentSearch(strSearchTerm);
						return;
					}

					//Any other item type: open URL
					final String strSlug = record.getString(ObjTypeaheadResponse.FIELD_SLUG);

					if(strSlug!=null && !"".equals(strSlug)){
						
						//send GTM Tag based on category header
						String strCategoryHeading = "";
						String strCategoryType = "";
						ObjTypeaheadResponse parentRecord = record.getParentRecord();
						
						if(parentRecord!=null){
							strCategoryHeading = parentRecord.getString(ObjTypeaheadResponse.FIELD_NAME);
							strCategoryType = parentRecord.getString(ObjTypeaheadResponse.FIELD_CATEGORY_TYPE);
						}
						
						//Version 2 tagging - depending on parent Category
						//A) Products
						String strResultName = record.getString(ObjTypeaheadResponse.FIELD_NAME);
						if(ObjTypeaheadResponse.CONST_RESULT_TYPE_PRODUCT.equals(strCategoryType)) {
							AppTagManager.instance().sendV2EventTags("linkclick", "App - search", "Products",strResultName);
						}else if(ObjTypeaheadResponse.CONST_RESULT_TYPE_CATEGORY.equals(strCategoryType)) {
							AppTagManager.instance().sendV2EventTags("linkclick","App - search","Categories",strResultName);
						}else if(ObjTypeaheadResponse.CONST_RESULT_TYPE_HELP.equals(strCategoryType)) {
							AppTagManager.instance().sendV2EventTags("linkclick","App - search","Customer Services",strResultName);
						}



						String strTargetUrl = AppUtils.getWiggleURL(ApplicationContextProvider.getContext().getString(R.string.str_url_wiggle_home)) 
								  + "/"
								  + strSlug;
						
						strTargetUrl = AppUtils.appendUtmTag(strTargetUrl, "searchFindAsYouType");
						
						//load URL of selected record
						TabbedHybridActivity.instance().loadSearchResultInWebView(strTargetUrl);
					}
					
				}
				
			});
			
		}

		return view;
		
	}
	

	
	@Override
	public Object getItem(int arg0) {
		return mTypeheadResponses.get(arg0);
	}
	

}
