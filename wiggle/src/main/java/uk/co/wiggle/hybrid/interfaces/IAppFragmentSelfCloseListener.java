package uk.co.wiggle.hybrid.interfaces;

/**
 * 
 * @author AndroMedia
 * 
 * There are situations where fragments are no longer useful (e.g. app was in background for a while and 
 * brought back to front. Parent Activity has long been destroyed and is newly created. However the last 
 * state of the Activity might be lost / destroyed (at least if instance state was not saved correctly)
 * 
 * In this case it can help to close a fragment that relied on such data. This interface enables that.
 *
 */
public interface IAppFragmentSelfCloseListener {

	public void onFragmentSelfClose(String tag);
	
}
