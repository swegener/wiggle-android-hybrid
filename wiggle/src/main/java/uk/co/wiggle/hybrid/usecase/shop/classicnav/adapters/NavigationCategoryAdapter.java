package uk.co.wiggle.hybrid.usecase.shop.classicnav.adapters;

import java.util.ArrayList;
import java.util.List;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.usecase.shop.classicnav.activities.HybridActivity;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjSalesOrderManager;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNavigationCategory;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.usecase.shop.classicnav.fragments.NavigationDrawerFragment;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;


import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class NavigationCategoryAdapter extends BaseAdapter {

	private String logTag = "NavigationCategoryAdapter";

	private List<ObjNavigationCategory> mCategories = new ArrayList<ObjNavigationCategory>(); // the original list of categories

	private List<ObjNavigationCategory> mCompleteBreadcrumb = new ArrayList<ObjNavigationCategory>();
	private NavigationDrawerFragment mContext;
	private int lastPosition;
	private boolean blnInterruptAnimation;

	public NavigationCategoryAdapter(NavigationDrawerFragment context) {
		mContext = context;
	}

	public void setListContent(List<ObjNavigationCategory> listItems,
			List<ObjNavigationCategory> completeBreadcrumb) {
		mCategories = listItems;
		mCompleteBreadcrumb = completeBreadcrumb;
	}

	@Override
	public int getCount() {
		
		if (mCategories == null){
			return 0;
		}else{
			return mCategories.size();
		}
		
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}


	@Override
	public View getView(int arg0, View view, ViewGroup arg2) {
		final int position = arg0;

		final ObjNavigationCategory category = (ObjNavigationCategory) mCategories.get(arg0);

		Logger.printMessage(logTag, "CATEGORY: " + category.toXML(), Logger.INFO);

		LayoutInflater inflater = LayoutInflater.from(mContext.getActivity());
		view = inflater.inflate(R.layout.navigation_item_row, null);

		TextView tvCategoryName = (TextView) view.findViewById(R.id.navigationItem_tvCategoryName);
		tvCategoryName.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		
		TextView navigationItem_tvCounterBadge = (TextView) view.findViewById(R.id.navigationItem_tvCounterBadge);
		navigationItem_tvCounterBadge.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		
		//set counter where required
		String strMenuItemTarget = category.getString(ObjNavigationCategory.FIELD_MENU_ITEM_TARGET);
		int intBadgeCounter = 0;
		
		if("OrderTracking".equals(strMenuItemTarget)){
			intBadgeCounter = ObjSalesOrderManager.getInstance().getOrdersWithNewInformation().size();
			//flag this view as order tracking menu item (we need this to update the notification badge later)
			view.setTag(strMenuItemTarget);
		}
		
		if(intBadgeCounter>0){
			navigationItem_tvCounterBadge.setText(String.valueOf(intBadgeCounter));
			navigationItem_tvCounterBadge.setVisibility(View.VISIBLE);
		}else{
			navigationItem_tvCounterBadge.setVisibility(View.GONE);
		}

		ImageView ivLevel1Image = (ImageView) view.findViewById(R.id.navigationItem_ivLevel1Image);

		View vLevel3Indent = (View) view.findViewById(R.id.navigationItem_vLevel3Indent);

		View vLevel4Indent = (View) view.findViewById(R.id.navigationItem_vLevel4Indent);
			
		ImageView navigationItem_ivMore = (ImageView) view.findViewById(R.id.navigationItem_ivMore);
			
		TextView navigationItem_tvSectionHeader = (TextView) view.findViewById(R.id.navigationItem_tvSectionHeader);
		navigationItem_tvSectionHeader.setTypeface(AppTypefaces.instance().fontMainRegular);

		// by default: display category name only
		String strCategoryName = category.getDisplayName();
		tvCategoryName.setText(strCategoryName);
		navigationItem_tvSectionHeader.setText(strCategoryName);
		
		//if this is a section header - show this in dedicated TextView instead of category name
		if(ObjNavigationCategory.CONST_CATEGORY_TYPE_HEADER.equals(category.getString(ObjNavigationCategory.FIELD_TYPE))){
			navigationItem_tvSectionHeader.setVisibility(View.VISIBLE);
			tvCategoryName.setVisibility(View.GONE);
		}else{
			navigationItem_tvSectionHeader.setVisibility(View.GONE);
			tvCategoryName.setVisibility(View.VISIBLE);
		}
		
/**
		intImageResource = intImageResource = category.getCategoryImage();
		

		if (intImageResource > 0) {
			ivLevel1Image.setImageResource(intImageResource);
			ivLevel1Image.setVisibility(View.VISIBLE);
		} else {
			ivLevel1Image.setVisibility(View.GONE);
		}

**/
		// add indent depending on breadcrumb level (but only if text filter is
		// not active)
		if (mCompleteBreadcrumb != null) {
			if (mCompleteBreadcrumb.size() >= 3) {
				vLevel3Indent.setVisibility(View.VISIBLE);
			} else {
				vLevel3Indent.setVisibility(View.GONE);
			}

			if (mCompleteBreadcrumb.size() >= 4) {
				vLevel4Indent.setVisibility(View.VISIBLE);
			} else {
				vLevel4Indent.setVisibility(View.GONE);
			}
		}
		
		//hide more-info item for link categories
		if(ObjNavigationCategory.CONST_CATEGORY_TYPE_LINK.equals(category.getString(ObjNavigationCategory.FIELD_TYPE)) 
				|| ObjNavigationCategory.CONST_CATEGORY_TYPE_HEADER.equals(category.getString(ObjNavigationCategory.FIELD_TYPE))
				|| ObjNavigationCategory.CONST_CATEGORY_TYPE_MENU_ITEM.equals(category.getString(ObjNavigationCategory.FIELD_TYPE))
				|| category.isConfirmedDeadEnd()
				){
			navigationItem_ivMore.setVisibility(View.GONE);
		}else{
			navigationItem_ivMore.setVisibility(View.VISIBLE);
		}


		Animation animation = AnimationUtils.loadAnimation(mContext.getActivity(), 
				(position > lastPosition) ? R.anim.listview_anim_up_from_bottom
						: R.anim.listview_anim_down_from_top);
		if (!blnInterruptAnimation) {
			view.startAnimation(animation);
		}
		lastPosition = position;
		
		// short click - use this search and execute it
		view.setOnClickListener(null);
		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				
				//do nothing if this is only a heading ... 
				if(ObjNavigationCategory.CONST_CATEGORY_TYPE_HEADER.equals(category.getString(ObjNavigationCategory.FIELD_TYPE))){
					return;
				}

				//Save a reference to this view in category (needed to manage progress bar on clicks etc)
				//Note: this will retain the view in memory as long as category lives - ok here as only used for a small list
				category.setAdapterView(view);

				HybridActivity.instance().handleMenuCategoryClick(category, false);
				
			}

		});

		return view;

	}

	@Override
	public Object getItem(int arg0) {
		return mCategories.get(arg0);
	}


	public void setInterruptListviewAnimation(boolean blnInterruptAnimation) {
		this.blnInterruptAnimation = blnInterruptAnimation;
	}

}
