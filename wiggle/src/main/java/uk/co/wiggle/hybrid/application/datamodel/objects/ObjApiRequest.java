package uk.co.wiggle.hybrid.application.datamodel.objects;

import java.util.List;

import com.android.volley.Request.Method;
import com.android.volley.Request.Priority;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler.ON_RESPONSE_ACTION;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;

import android.content.Context;

import org.json.JSONObject;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class ObjApiRequest extends CustomObject{
	public static final String OBJECT_NAME = "apiRequest";

	Context 			cxRequestContext; 											//the source of the request
	String 				strURI;														//only where applicable: a request specific URI that needs to be added to the base URL
	String  			strURLEndpoint; 											//the resource Endpoint
	CustomObject 		apiObject; 													//any in-app CustomObject that might be useful in the pre- or post-processing of the request (not necessarily the one carrying the above URI)
	ON_RESPONSE_ACTION  eOnResponseAction = ON_RESPONSE_ACTION.NO_ACTION; 			//a hint that can be used to trigger certain actions once the response was received
	int 				intVolleyHTTPMethod = Method.GET; 							//GET, POST, PUT ...
	String				strGETQuery; 												//for GET requests - the query parameters need to be passed
	ObjApiRequest		failedRequest;
	Priority			requestPriority;											//the request priority
	
	//Response control fields
	boolean				blnResponseError;
	String				strResponseMessage;
    JSONObject          apiResponse;

    //additional info
	CustomObject		infoObject;	//any in-app CustomObject that might be useful in the pre- or post-processing of the request (not necessarily the same as apiObject)
	
	//a basic constructor e.g. for Authentication and retrieving meta data such as categories / cities
	public ObjApiRequest(Context context, String strURLEndpoint, int intVolleyHTTPMethod, ObjApiRequest failedRequest){
		
		this.cxRequestContext = context;
		this.strURLEndpoint = strURLEndpoint;
		this.intVolleyHTTPMethod = intVolleyHTTPMethod;
		this.failedRequest = failedRequest; //used for re-submission in case of 412 errors 

	}
	
	//this constructor can be used by all endpoints - parameters not needed can be NULL
	public ObjApiRequest(Context context, String strURI, String strURLEndpoint, int intVolleyHTTPMethod, CustomObject apiObject, String strGETQueryString, ON_RESPONSE_ACTION onResponseAction){
		
		this.cxRequestContext = context;
		this.strURI = strURI;
		this.strURLEndpoint = strURLEndpoint;
		this.intVolleyHTTPMethod = intVolleyHTTPMethod;
		this.apiObject = apiObject;
		this.eOnResponseAction = onResponseAction;
		this.strGETQuery = strGETQueryString;	
		
	}
	
	//by default we are sending all api requests with "IMMEDIATE" priority - if changes to this are needed use this call
	public ObjApiRequest(Context context, String strURI, String strURLEndpoint, int intVolleyHTTPMethod, CustomObject apiObject, String strGETQueryString, ON_RESPONSE_ACTION onResponseAction, Priority requestPriority){
		
		this.cxRequestContext = context;
		this.strURI = strURI;
		this.strURLEndpoint = strURLEndpoint;
		this.intVolleyHTTPMethod = intVolleyHTTPMethod;
		this.apiObject = apiObject;
		this.eOnResponseAction = onResponseAction;
		this.strGETQuery = strGETQueryString;		
		this.requestPriority = requestPriority;

	}	
	

	public Context getContext() {
		return cxRequestContext;
	}


	public void setContext(Context cxRequestContext) {
		this.cxRequestContext = cxRequestContext;
	}

	public String getRequestURI() {
		return strURI;
	}


	public void setRequestURI(String strURI) {
		this.strURI = strURI;
	}

	public String getURLEndpoint() {
		return strURLEndpoint;
	}


	public void setURLEndpoint(String strURLEndpoint) {
		this.strURLEndpoint = strURLEndpoint;
	}

	public CustomObject getApiObject() {
		return apiObject;
	}

	public void setApiObject(CustomObject apiObject) {
		this.apiObject = apiObject;
	}


	public ON_RESPONSE_ACTION getOnResponseAction() {
		return eOnResponseAction;
	}

	public void setOnResponseAction(ON_RESPONSE_ACTION eOnResponseAction) {
		this.eOnResponseAction = eOnResponseAction;
	}


	public int getVolleyHTTPMethod() {
		return intVolleyHTTPMethod;
	}

	public void setVolleyHTTPMethod(int intVolleyHTTPMethod) {
		this.intVolleyHTTPMethod = intVolleyHTTPMethod;
	}

	public String getQueryString() {
		return strGETQuery;
	}


	public void setQueryString(String strQueryString) {
		this.strGETQuery = strQueryString;
	}

	public String getObjectName() {
		return OBJECT_NAME;
	}
	
	public ObjApiRequest getFailedRequest(){
		return failedRequest;
	}
	
	public Priority getRequestPriority(){
		return requestPriority;
	}
	
	/**
	 *  This method submits an api request directly to the server.
	 *  Please note the difference to submitAfterHeadRequest().
	 *  @return
	 */
	public ObjApiRequest submit(){
		
		AppRequestHandler.instance().executeHTTPRequest(this);
		return this;
		
	}
	
	
	/**
	 *  For network intensive request endpoints (and in the current absence of ETag information) api requests 
	 *  using this method will only be passed to the server if a first HEAD request indicates that the server data 
	 *  has changed and local data needs refreshing 
	 *  
	 *  @return
	 */
	public ObjApiRequest submitAfterHeadRequest(){
		
		AppRequestHandler.instance().executeHeadRequest(this);
		return this;		
		
	}
	

	@Override
	public List<String> getFields() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Class<?> getSubclass() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getFieldType(String fieldName) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	public boolean isResponseError(){
		return blnResponseError;
	}
	
	public void setResponseError(boolean blnResponseError){
		this.blnResponseError = blnResponseError;
	}
	
	public String getResponseMessage(){
		return strResponseMessage;
	}
	
	public void setResponseMessage(String strResponseMessage){
		this.strResponseMessage = strResponseMessage;
	}

	public JSONObject getApiResponse(){
	    return this.apiResponse;
    }

    public void setApiResponse(JSONObject apiResponse){
	    this.apiResponse = apiResponse;
    }

    public void setInfoObject(CustomObject infoObject){
		this.infoObject = infoObject;
	}

	public CustomObject getInfoObject(){
		return this.infoObject;
	}

}
