package uk.co.wiggle.hybrid.application.datamodel.objects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjNewsfeedCategoryManager;
import uk.co.wiggle.hybrid.api.parsers.JsonObjectParser;
import uk.co.wiggle.hybrid.application.helpers.AppCountryHelper;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.logging.Logger;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;

import com.yakivmospan.scytale.Crypto;
import com.yakivmospan.scytale.Options;
import com.yakivmospan.scytale.Store;

import javax.crypto.SecretKey;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class ObjAppUser extends CustomObject {
	
	public static final String OBJECT_NAME = "appUser";
	
	static ObjAppUser mAppUser;
	
	private ObjCustomerName mCustomerName = new ObjCustomerName();
	
	private ArrayList<ObjNewsfeedCategory> mUnsubscribedNewsfeeds = new ArrayList<ObjNewsfeedCategory>(); //stores newsfeed topics the user has opted out of

	//Note: api fields "Success" and "Reason" are extracted in JsonObjectParser and linked to the ObjApiRequest
	//via ObjApiRequest.setResponseError() and ObjApiRequest.setResponseMessage()
	public static final String FIELD_CUSTOMER_ID = "CustomerId";
	public static final String FIELD_ENCRYPTED_CUSTOMER_ID = "EncryptedCustomerId";
	public static final String FIELD_ACCESS_TOKEN = "Token";
	public static final String FIELD_DISCOUNT = "Discount";
	public static final String JSON_OBJECT_CUSTOMER_NAME = "CustomerName"; // => saved to mCustomerName
	
	//These fields belong to nested Json object CustomerName - we store them redundantly as flat fields against the user for ease of use
	public static final String FIELD_TITLE = "Title";
	public static final String FIELD_FORENAME = "FirstName";
	public static final String FIELD_SURNAME = "Surname";
	
	//fields not returned by API, but kept locally
	public static final String FIELD_USERNAME = "username";
	public static final String FIELD_EMAIL = "email";
	public static final String FIELD_PASSWORD = "password";
	public static final String FIELD_COUNTRY = "country"; //TODO: remove if unused
	public static final String FIELD_REMEMBER_CREDENTIALS = "remember_credentials";
	public static final String FIELD_TEST_SERVER_URL = "test_server_url";

	//Internationalisation parameters for Customer cookie:
	public static final String FIELD_CURRENT_WIGGLE_STORE = "store"; //top level domain of the current Wiggle store (e.g. www.wiggle.co.uk for UK)
	public static final String FIELD_PARAMETER_CURRENCY = "curr"; //Currency  configured in Wiggle's Delivery / Currency menu
	public static final String FIELD_PARAMETER_DELIVERY_DESTINATION = "dest"; //Delivery destination configured in Wiggle's Delivery / Currency menu
	public static final String FIELD_PARAMETER_LANGUAGE = "language"; //Language configured in Wiggle's Delivery / Currency menu
		
	//Storage for subscribed newsfeed categories (topics)
	public static final String JSON_ARRAY_UNSUBSCRIBED_NEWS_TOPICS = "unsubscribed_topics"; //internal use: json array of numbers [1, 3, 5]
	
	//internal field to track if we already displayed the news preferences hint
	public static final String FIELD_NEWS_PREFERENCES_HINT_ALREADY_SEEN = "news_pref_hint_seen";
	//Internal field to track if this user has agreed to leave a trustpilot review
	//We are storing the app version the user has done this in so we can ask him in a later release.
	public static final String FIELD_POST_ORDER_TRUSTPILOTREVIEW_ACCEPTED = "post_order_review";
	//internal field to track if we already displayed the events homescreen to the user
	public static final String FIELD_EVENTS_INTRO_ALREADY_SEEN = "events_intro_seen";
	//internal field to track if we already displayed the "swipe list item to add event to calendar" message
	public static final String FIELD_EVENTS_SWIPE_HINT_ALREADY_SEEN = "events_swipe_hint_seen";
	//internal field to track if the user has submitted a Legal & Insurance claim, but it needs re-sending as initial submission failed
	public static final String FIELD_LEGAL_CLAIM_OUTSTANDING = "legal_claim_outstanding";

	//CRC - stores the user's feedback to our app review prompt
	public static final String FIELD_APP_REVIEW_PROMPT_RESPONSE = "app_review_prompt_response";
	public static final String FIELD_APP_REVIEW_PROMPT_DISPLAY_DATE = "app_review_prompt_display_date";
	public static final String CONST_RATE_US_PROMPT_ACCEPTED = "ACCEPTED";
	public static final String CONST_RATE_US_PROMPT_LATER = "LATER";
	public static final String CONST_RATE_US_PROMPT_DECLINED = "DECLINED";
	public static final String CONST_THUMBS_DOWN= "THUMBS_DOWN";

	private static List<String> sFieldList;
	static {
		sFieldList = Collections.synchronizedList(new ArrayList<String>());
		sFieldList.add(FIELD_ID);
		sFieldList.add(FIELD_CUSTOMER_ID);
		sFieldList.add(FIELD_ENCRYPTED_CUSTOMER_ID);
		sFieldList.add(FIELD_ACCESS_TOKEN);
		sFieldList.add(FIELD_DISCOUNT);
		sFieldList.add(JSON_OBJECT_CUSTOMER_NAME);
		sFieldList.add(FIELD_TITLE);
		sFieldList.add(FIELD_FORENAME);
		sFieldList.add(FIELD_SURNAME);
		sFieldList.add(FIELD_USERNAME);
		sFieldList.add(FIELD_EMAIL);
		sFieldList.add(FIELD_PASSWORD);
		sFieldList.add(FIELD_REMEMBER_CREDENTIALS);
		sFieldList.add(FIELD_COUNTRY);
		sFieldList.add(FIELD_CURRENT_WIGGLE_STORE);
		sFieldList.add(FIELD_PARAMETER_CURRENCY);
		sFieldList.add(FIELD_PARAMETER_DELIVERY_DESTINATION);
		sFieldList.add(FIELD_PARAMETER_LANGUAGE);
		sFieldList.add(JSON_ARRAY_UNSUBSCRIBED_NEWS_TOPICS);
		sFieldList.add(FIELD_NEWS_PREFERENCES_HINT_ALREADY_SEEN);
		sFieldList.add(FIELD_POST_ORDER_TRUSTPILOTREVIEW_ACCEPTED);
		sFieldList.add(FIELD_EVENTS_INTRO_ALREADY_SEEN);
		sFieldList.add(FIELD_EVENTS_SWIPE_HINT_ALREADY_SEEN);
		sFieldList.add(FIELD_LEGAL_CLAIM_OUTSTANDING);
		sFieldList.add(FIELD_TEST_SERVER_URL);
		sFieldList.add(FIELD_APP_REVIEW_PROMPT_RESPONSE);
		sFieldList.add(FIELD_APP_REVIEW_PROMPT_DISPLAY_DATE);
		
	}

	private static ConcurrentHashMap<String, Integer> sFieldTypes;
	static {
		sFieldTypes = new ConcurrentHashMap<String, Integer>();
		
		sFieldTypes.put(FIELD_ID, FIELD_TYPE_INTEGER);
		sFieldTypes.put(FIELD_CUSTOMER_ID, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_ENCRYPTED_CUSTOMER_ID, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_ACCESS_TOKEN, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_DISCOUNT, FIELD_TYPE_STRING);
		sFieldTypes.put(JSON_OBJECT_CUSTOMER_NAME, FIELD_TYPE_OBJECT);
		sFieldTypes.put(FIELD_TITLE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_FORENAME, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_SURNAME, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_USERNAME, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_EMAIL, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PASSWORD, FIELD_TYPE_STRING);	
		sFieldTypes.put(FIELD_REMEMBER_CREDENTIALS, FIELD_TYPE_BOOLEAN);
		sFieldTypes.put(FIELD_COUNTRY, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_CURRENT_WIGGLE_STORE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PARAMETER_CURRENCY, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PARAMETER_DELIVERY_DESTINATION, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PARAMETER_LANGUAGE, FIELD_TYPE_STRING);
		sFieldTypes.put(JSON_ARRAY_UNSUBSCRIBED_NEWS_TOPICS, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_NEWS_PREFERENCES_HINT_ALREADY_SEEN, FIELD_TYPE_BOOLEAN);
		sFieldTypes.put(FIELD_POST_ORDER_TRUSTPILOTREVIEW_ACCEPTED,  FIELD_TYPE_INTEGER);
		sFieldTypes.put(FIELD_EVENTS_INTRO_ALREADY_SEEN, FIELD_TYPE_BOOLEAN);
		sFieldTypes.put(FIELD_EVENTS_SWIPE_HINT_ALREADY_SEEN, FIELD_TYPE_BOOLEAN);
		sFieldTypes.put(FIELD_LEGAL_CLAIM_OUTSTANDING, FIELD_TYPE_BOOLEAN);
		sFieldTypes.put(FIELD_TEST_SERVER_URL, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_APP_REVIEW_PROMPT_RESPONSE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_APP_REVIEW_PROMPT_DISPLAY_DATE, FIELD_TYPE_STRING);
		
	}
	
	public ObjAppUser() {		
		
	}
	
	public ObjAppUser(Bundle bundle) {
		super(bundle);
	}
	
	public static ObjAppUser instance(){
		
		if(mAppUser==null){
			
			//just in case app was in background and user variables where garbage collected .. re-instate App user from shared preferences
			mAppUser = getAppUserFromSharedPreferences();
			
			//still null? create new empty ObjAppUser
			if(mAppUser==null){
				mAppUser = new ObjAppUser();	
			}
			
		}
		
		return mAppUser;
		
	}
	
	public static void setAppUser(ObjAppUser appUser) {
		mAppUser = appUser;		
	}
		
	//only use this to verify if up user is empty
	public static ObjAppUser getAppUser(){
		return mAppUser;
	}

	@Override
	public String getObjectName() {
		return OBJECT_NAME;
	}
	
	@Override
	public List<String> getFields() {
		return sFieldList;
	}
	
	@Override
	public Class<?> getSubclass() {
		return ObjAppUser.class;
	}
	
	@Override
	public Integer getFieldType(String fieldName) {
		return sFieldTypes.get(fieldName);
	}
	
	
	public void updateWith(ObjAppUser incomingUser){
		

		for(String strFieldName : sFieldList){
			
			if(incomingUser.containsKey(strFieldName)){
				
				int intFieldType = getFieldType(strFieldName);
				
				switch (intFieldType) {

				case FIELD_TYPE_BOOLEAN:
					setField(strFieldName, incomingUser.getBoolean(strFieldName));
					break;
					
				case FIELD_TYPE_STRING:
					setField(strFieldName, incomingUser.getString(strFieldName));
					break;
					
				case FIELD_TYPE_INTEGER:
					setField(strFieldName, incomingUser.getInt(strFieldName));
					break;
					
				default:
					break;
				}
				
			}
			
		}	
		
		
	}
	
	public static String createJSONfromUser(ObjAppUser object , CustomObject targetObject) throws JSONException{
		
		JSONArray mainArray = new JSONArray();
		List<String> fields = targetObject.getFields();
			
		JSONObject jsonObject = new JSONObject();
			
			// first do the flat fields
			for (String strFieldName : fields) {

				if(object.get(strFieldName) != null){
					
					int intValueFieldType = targetObject.getFieldType(strFieldName);
					
					switch (intValueFieldType) {
					
					case CustomObject.FIELD_TYPE_OBJECT:
						break;
					
					case CustomObject.FIELD_TYPE_DATETIME:
						break;
					
					default:
						jsonObject.put(strFieldName, object.get(strFieldName));
						break;
					}
					
					
				}

			}
			
			mainArray.put(jsonObject);
			
		
		return mainArray.toString();
		
	}
	

	//handler for children of this object	
	public ObjCustomerName getCustomerName() {
		return mCustomerName;
	}
	
	public void setCustomerName(ObjCustomerName customerName){
		mCustomerName = customerName;
	}

	public boolean isUserLoggedIn(){
		
		/**
		String strUserAccessToken = mAppUser.getString(FIELD_ACCESS_TOKEN);
    	String strFacebookAccessToken = mAppUser.getString(FIELD_FACEBOOK_ACCESS_TOKEN);
    	
    	if(strUserAccessToken.equals("") && strFacebookAccessToken.equals("")){
    		return false;
    	}else{
    		return true;
    	}
    	**/
		
		//a logged in user has at least one custom app configuration downloaded .. 
		//TODO: make this more clever (use API access token etc)
		if("".equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_ACCESS_TOKEN))){
			return false;
		}else{
			return true;
		}
    	
		
	}
	
	
	
	public void updateAppUser(ObjAppUser incomingUser) {
		
		if(mAppUser == null){
			mAppUser = incomingUser;			
		}else{
			mAppUser.updateWith(incomingUser); //not replacing the user with an incoming user, but only updating information that was actually delivered
		}
		
		saveToSharedPreferences();
		
		
	}
	
	public void updateAppUserThroughTransientUser(String strFieldName, String strValue){
		
		//When changing fields on the appUser object we do this through a transient user. By calling updateAppUser the changes are written to appUser and shared preferences
		ObjAppUser transientUser = new ObjAppUser();
		transientUser.setField(strFieldName, strValue);
		updateAppUser(transientUser);
		
	}	
	
	
	public void saveToSharedPreferences(){
		
		SharedPreferences sharedPreferences = ApplicationContextProvider.getContext().getSharedPreferences(OBJECT_NAME, Context.MODE_PRIVATE);
	    Editor editor = sharedPreferences.edit();
	    
    	try {

    		String json = createJSONfromUser(mAppUser, new ObjAppUser());
    		AppUtils.showCustomToast("Save AppUser: " + json.toString(), true);
			editor.putString(OBJECT_NAME, json);
			
		} catch (JSONException e) {
			Logger.printMessage(OBJECT_NAME, e.getMessage(), Logger.ERROR);
		}
	    
	    //editor.apply(); //asynchronous
    	editor.commit(); //changed to synchronous to avoid timing issues (we are not writing huge amounts of data so that should be ok)
		
	}
	
	public static ObjAppUser getAppUserFromSharedPreferences() {
		
		ObjAppUser storedUser = null;
		
		SharedPreferences sharedPreferences;
	    
	    sharedPreferences = ApplicationContextProvider.getContext().getSharedPreferences(OBJECT_NAME, Context.MODE_PRIVATE);
	    
	    if(!sharedPreferences.getString(OBJECT_NAME, "").equals("")){				    
		    
		    String json = sharedPreferences.getString(OBJECT_NAME, "");
		    AppUtils.showCustomToast("Get AppUser: " + json.toString(), true);
		    try {
		    	storedUser = JsonObjectParser.instance().createUserFromJSON(json);
			} catch (JSONException e) {
				Logger.printMessage(OBJECT_NAME, e.getMessage(), Logger.ERROR);
			}
		    
	    }
	    
	    return storedUser;
	    
	}	
	
	public void clearCountrySpecificFields(){
		
    	setField(ObjAppUser.FIELD_COUNTRY, "");
    	setField(ObjAppUser.FIELD_CURRENT_WIGGLE_STORE, "");
    	setField(ObjAppUser.FIELD_PARAMETER_CURRENCY, "");
    	setField(ObjAppUser.FIELD_PARAMETER_DELIVERY_DESTINATION, "");
    	setField(ObjAppUser.FIELD_PARAMETER_LANGUAGE, "");
    	
    	saveToSharedPreferences();
    	
    	
	}
	
	/* Unsubscribed newsfeeds */
	
	// Returns the unsubscribed news category list as ArrayList
	public ArrayList<ObjNewsfeedCategory> getUnsubscribedNewsTopics(){
		return mUnsubscribedNewsfeeds;
	}
	
	//Once we have defined our Categories in ObjNewsfeedCategoryManager - we can get the
	//json list of category unsubscribes from the app user and turn it into mUnsubscribedNewsfeeds
	public void deserialiseUnsubscribedNewsTopicsFromAppUser(){
		

		//get the flat json array from the app user, loop through it and build an ArrayList of ObjNewfeedCategory
		JSONArray unsubscribedFeeds = getUnsubscribedTopicsFromAppUser();
		if(unsubscribedFeeds==null){
			return;
		}
		
		ArrayList<ObjNewsfeedCategory> lstUnsubscribedTopics = new ArrayList<ObjNewsfeedCategory>();
		
		for(int i=0;i<unsubscribedFeeds.length();i++){
			
			int intCategoryId = -1;
			try {
				intCategoryId = unsubscribedFeeds.getInt(i);
			} catch (JSONException e) {
				Logger.printMessage(OBJECT_NAME, "Conversion of " + JSON_ARRAY_UNSUBSCRIBED_NEWS_TOPICS + "failed in setUnsubscribedNewsTopics", Logger.WARN);
			}
			
			ObjNewsfeedCategory unsubscribedCategory = ObjNewsfeedCategoryManager.getInstance().getRecordByUniqueId(intCategoryId);
			lstUnsubscribedTopics.add(unsubscribedCategory);
			
		}
		
		mUnsubscribedNewsfeeds.clear();
		mUnsubscribedNewsfeeds.addAll(lstUnsubscribedTopics);
		
	}
	
	//used from newsfeed settings screen - unsubscribe from a topic. Maintain mUnsubscribedNewsfeeds and ObjAppUser flat field's JsonArray
	public ArrayList<ObjNewsfeedCategory> unsubscribeFromTopic(ObjNewsfeedCategory topic){
		
		if(!mUnsubscribedNewsfeeds.contains(topic)){
			
			mUnsubscribedNewsfeeds.add(topic);
			writeUnsubscribedTopicsToAppUser();
			
		}
		
		return mUnsubscribedNewsfeeds;
		
	}
	
	//used from newsfeed settings screen - subscribe to a topic. Maintain mUnsubscribedNewsfeeds and ObjAppUser flat field's JsonArray
	public ArrayList<ObjNewsfeedCategory> subscribeToTopic(ObjNewsfeedCategory topic){
		
		if(mUnsubscribedNewsfeeds.contains(topic)){
			
			mUnsubscribedNewsfeeds.remove(topic);
			writeUnsubscribedTopicsToAppUser();
			
		}
		
		return mUnsubscribedNewsfeeds;
		
	}
	
	//turn mUnsubscribedNewsfeeds from ArrayList<ObjNewsfeedCategory> into a json list of IDs [1, 4, 6] that we can save with app user
	private void writeUnsubscribedTopicsToAppUser(){
		
		JSONArray unsubscribedTopics = new JSONArray();
		
		for(int i=0;i<mUnsubscribedNewsfeeds.size();i++){
			
			ObjNewsfeedCategory topic = mUnsubscribedNewsfeeds.get(i);
			int intTopicId = topic.getUniqueId();
			unsubscribedTopics.put(intTopicId);
			
		}
		
		//convert json array to String and save with ObjAppUser
		String strAdjustedTopics = unsubscribedTopics.toString();
		updateAppUserThroughTransientUser(JSON_ARRAY_UNSUBSCRIBED_NEWS_TOPICS, strAdjustedTopics);
		
		
	}
	
	// read app user's flat field - turn the String into a JSONArray
	public JSONArray getUnsubscribedTopicsFromAppUser(){
		
		String strUnsubscribedNewsfeeds = getString(JSON_ARRAY_UNSUBSCRIBED_NEWS_TOPICS);
		
		if(strUnsubscribedNewsfeeds==null || "".equals(strUnsubscribedNewsfeeds)){
			return null;
		}
		
		try {
			
			JSONArray unsubscribedFeeds = new JSONArray(strUnsubscribedNewsfeeds);
			return unsubscribedFeeds;
			
		} catch (JSONException e) {
			Logger.printMessage(OBJECT_NAME, "Conversion of " + JSON_ARRAY_UNSUBSCRIBED_NEWS_TOPICS + "failed in getUnsubscribedTopics", Logger.WARN);
		}
		
		return null;

	}
	
	public boolean isSubscribedToNewsTopic(ObjNewsfeedCategory topic){
		
		if(mUnsubscribedNewsfeeds.contains(topic)){
			return false;
		}else{
			return true;
		}
		
	}

	public String getPassword(){

		//plain text version
		String strPasswordEncrypted = getString(ObjAppUser.FIELD_PASSWORD);

		//Decrypt the encrypted password
		Crypto crypto = new Crypto(Options.TRANSFORMATION_SYMMETRIC);
		Store store = new Store(ApplicationContextProvider.getContext());
		SecretKey key = store.getSymmetricKey("pwkey", null);

		String decryptedPassword = "";
		try {
			decryptedPassword = crypto.decrypt(strPasswordEncrypted, key);
		}catch(Exception e){
			AppUtils.showCustomToast("Password decryption failed", true);
			//Should only happen for users who have not had their password encrypted yet
			//(We could force encryption by returning empty string and clearing "remember credentials" field for existing users?)
			//We went for the more pragmatic approach for existing users:
			//simply use stored plaintext password saved with versions <=2.0.0
			//when login screen is shown next, this will get decrypted on Sign in
			return strPasswordEncrypted;
		}

		//Exception handling (prevent NPE in AppRequestHandler) - if password decryption returns null - return stored plaintext password
		if(decryptedPassword==null){
			Logger.printMessage("ObjAppUser.getPassword()", "Password decryption returned null - using stored plain text password", Logger.DEBUG);
			decryptedPassword = strPasswordEncrypted;
		}

		return decryptedPassword;

	}

	public String getFullName(){

		return getString(FIELD_FORENAME) + " "  + getString(FIELD_SURNAME);
	}

	public int getDeliveryDestination(){

		//Delivery destination: check if webview saw the WiggleCustomer2 cookie for this customer
		//If so, use it, otherwise default to chosen country
		String strDeliveryDestination = getString(FIELD_PARAMETER_DELIVERY_DESTINATION);

		if(strDeliveryDestination!=null && !"".equals(strDeliveryDestination)){
			try{
				int intDeliveryDestination = Integer.valueOf(strDeliveryDestination);
				return intDeliveryDestination;
			}catch(Exception e){
				//non numeric value .. log and continue
				Logger.printMessage(OBJECT_NAME, "Delivery destination non-numeric, using country code", Logger.INFO);
			}
		}

		String strCountryCode = getString(ObjAppUser.FIELD_COUNTRY);
		AppCountryHelper.Country myCountry =  AppCountryHelper.instance().getCountryByInternalCode(strCountryCode);
		int intCountryId = AppCountryHelper.instance().getDefaultCountry().getmCountryId();
		if(myCountry!=null) {
			intCountryId = myCountry.getmCountryId();
		}

		return intCountryId;

	}

}

