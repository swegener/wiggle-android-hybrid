package uk.co.wiggle.hybrid.api.networking;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;

import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Request.Method;
import com.android.volley.Request.Priority;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjApiRequest;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjGeneric;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjHoehle;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjMegamenuItem;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNavigationCategory;
import uk.co.wiggle.hybrid.api.parsers.AppXMLPullParser;
import uk.co.wiggle.hybrid.api.parsers.JsonObjectParser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjTFA;
import uk.co.wiggle.hybrid.application.helpers.AppCountryHelper;
import uk.co.wiggle.hybrid.application.session.AppSession;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.persistence.DataStore;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.usecase.shop.classicnav.activities.HybridActivity;
import uk.co.wiggle.hybrid.usecase.newsfeed.activities.NewsfeedActivity;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.activities.Alert;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.activities.TabbedHybridActivity;
import uk.co.wiggle.hybrid.usecase.shop.version3.activities.V3HybridActivity;
import uk.co.wiggle.hybrid.usecase.startup.activities.StartupActivity;
import uk.co.wiggle.hybrid.usecase.startup.activities.WelcomeActivity;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/


public class AppRequestHandler {
	
	private String logTag = "AppRequestHandler";
	
	private static AppRequestHandler mInstance = null;
	private static ObjHoehle h;

	//Base API details (user signup, login, download configuration)
	public final static String BASE_URL_PRODUCTION = ApplicationContextProvider.getContext().getString(R.string.str_base_url_prod_server); //prod server
	public final static String SERVER_API_DIRECTOY = "/api";
	//public final static String SERVER_API_VERSION = "/v1";
	public final static String SERVER_API_VERSION = "";
	public final static String BASE_URL_STAGING = ApplicationContextProvider.getContext().getString(R.string.str_base_url_staging_server); //staging server
	
	private static final String DEFAULT_PAGINATE_BY = "25";
	
	//Resource endpoints
	//public final static String ENDPOINT_AUTHENTICATE = "/session/authenticatemobile";
	//replaced through obfuscated version ..
	public static String getAuthenticationEndpoint(){
		ObjHoehle h = new ObjHoehle();
		String strEndpoint = h.getValue(ObjHoehle.FIELD_BA);
		return strEndpoint;
	}

	public final static String ENDPOINT_CATEGORIES = "/api/list";
	public final static String ENDPOINT_ORDER_TRACKING = "/orders/recent"; //order tracking endpoint
	public final static String ENDPOINT_ORDER_TRACKING_TEST = "https://andromedia.biz/download/wiggle/order_tracking_test_response.json"; //order tracking endpoint for testing purposes (can be edited manually)
	public final static String ENDPOINT_TYPEAHEAD = ApplicationContextProvider.getContext().getString(R.string.str_url_typeahead);
	public final static String LOCAL_NAVIGATION_FILE = "local_data"; //no API endpoint - used to read a local file containing navigation menu entries
	public final static String URL_NEWSFEED_CONFIG_FILE = "http://www.wigglestatic.com/presentation/app/config/config-brands.json"; //no API endpoint - but pointing to the newsfeed config-brand.json file
	public final static String URL_NEWSFEED_SPORTS_CONFIG_FILE = "http://www.wigglestatic.com/presentation/app/config/config.json"; //no API endpoint - but pointing to the newsfeed config-brand.json file
	public final static String ENDPOINT_NEWSFEED_DUMMY = "NEWSFEED_RSS_TYPE_1";
	//Mailgun (REST api used for user's app feedback)
	public static String ENDPOINT_MAILGUN = "";
	public static String ENDPOINT_UPGRADE_CHECK = "https://azuuksprdmob01str.z33.web.core.windows.net/wiggle/version.json";
	public static String ENDPOINT_MEGAMENU = "/megamenu";
	//Two-Factor Authentication
	public static String ENDPOINT_VERIFY_MFA = "/session/verifymfa";
	//Refresh token validation (silent login from 3.3.5)
	public static String ENDPOINT_SILENT_LOGIN = "/session/refreshtoken";
	public final static String JSON_FIELD_REFRESH_TOKEN = "RefreshToken";
	public final static String ERROR_TEXT_REFRESH_TOKEN_INCORRECT = "NotFound";
	
	//handled error codes
	public final static int ERROR_CODE_BAD_REQUEST = 400;
	public final static int ERROR_CODE_UNAUTHORIZED = 401;
	public final static int ERROR_CODE_RESOURCE_NOT_FOUND = 404;
	public final static int ERROR_CODE_CONFLICT = 409;
	public final static int ERROR_CODE_INVALID_TOKEN = 412;
	public final static int ERROR_CODE_INTERNAL_SERVER_ERROR = 500;
	public final static int ERROR_CODE_METHOD_NOT_IMPLEMENTED = 501;
	public final static int ERROR_CODE_SERVER_UNAVAILABLE = 503;
	
	//error strings returned by server (HTTP 200 responses with "Success:false Reason:errorMessage)
	public final static String ERROR_TEXT_LOGIN_INCORRECT = "PasswordIncorrect";
	public final static String ERROR_TEXT_LOGIN_MFA_REQUIRED = "MfaRequired";
	public final static String ERROR_TEXT_LOGIN_ACCOUNT_LOCKED_GENERAL = "AccountLocked";
	public final static String ERROR_TEXT_LOGIN_ACCOUNT_LOCKED_MFA = "AccountLockedMfa"; //also seen as AccountLockedMFA?
	public final static String ERROR_TEXT_LOGIN_MULTIFACTOR_FAILED = "MultiFactorFailed";

	public static final String ENCODING_UTF_8 = "UTF-8";
	public static final String ENCODING_LATIN_I = "ISO-8859-1";

	
	//configuration of volley retry policy and timeouts
	private final static int VOLLEY_RETRY_POLICY_TIMEOUT_IN_SECONDS = 10; //10 seconds (decrease with caution - some requests take longer than 10 seconds!)
	private final static int VOLLEY_RETRY_POLICY_MAX_RETRIES = 1; //if a request fails, Volley will try one more time (0 will mean no retries)
	private final static float VOLLEY_RETRIY_POLICY_BACKOFF_MULTIPLIER = 1f; //multiplier for exponential backoff (left at non-exponential by setting it to 1f)
	
	
	public static final String HEADER_ENCODING = "Content-Encoding";
    public static final String HEADER_ACCEPT_ENCODING = "Accept-Encoding";
    public static final String ENCODING_GZIP = "gzip";
	
	private static String API_ACCESS_TOKEN = "";
	
	public static String getApiAccessToken(){
		return API_ACCESS_TOKEN;
	}

	//Refresh token handling (stored securely)
	public String getRefreshToken(){
		//(4) retrieve text from encrypted shared preferences held in ApplicationContextProvider
		String strRefreshToken = "";
		SharedPreferences encryptedSharedPreferences = ApplicationContextProvider.getEncryptedSharedPreferences();
		if(encryptedSharedPreferences != null){
			strRefreshToken = encryptedSharedPreferences.getString("rt", "");
			Logger.printMessage(logTag, "Encrypted Shared Preferences ok - refresh token retrieved", Logger.INFO);
		}else{
			Logger.printMessage(logTag,"Encrypted Shared Preferences missing - no refresh token stored", Logger.ERROR);
		}
		return strRefreshToken;
	}

	public void setRefreshToken(String strRefreshToken) {
		//(3) Store text in encrypted shared preferences held in ApplicationContextProvider
		//data will be encrypted as its stored
		SharedPreferences encryptedSharedPreferences = ApplicationContextProvider.getEncryptedSharedPreferences();
		if(encryptedSharedPreferences != null){
			encryptedSharedPreferences.edit().putString("rt", strRefreshToken).commit(); //using synchronous commit() as opposed to apply() to avoid timing issues (we are not writing huge amounts of data so that should be ok)
			Logger.printMessage(logTag, "Encrypted Shared Preferences ok - refresh token stored", Logger.INFO);
		}else{
			Logger.printMessage(logTag,"Encrypted Shared Preferences missing - refresh token cannot be saved", Logger.ERROR);
		}

	}

	public void clearRefreshToken(){
		SharedPreferences encryptedSharedPreferences = ApplicationContextProvider.getEncryptedSharedPreferences();
		if(encryptedSharedPreferences != null){
			encryptedSharedPreferences.edit().remove("rt").commit();
		}
	}

	public void setApiAccessToken(String strApiAccessToken){
		//not applicable for this app
		//API_ACCESS_TOKEN = strApiAccessToken;
		//ObjAppUser.instance().setField(ObjAppUser.FIELD_ACCESS_TOKEN, strApiAccessToken);		
	}
	
	//Array list of endpoints where caching is used - the only purpose of this is to prevent unnecessary disk-reads
	//TODO: adjust where needed
	private ArrayList<String> cacheFiles = new ArrayList<String>(Arrays.asList(
			ENDPOINT_NEWSFEED_DUMMY, 
			URL_NEWSFEED_CONFIG_FILE, 
			URL_NEWSFEED_SPORTS_CONFIG_FILE,
			getOrderTrackingEndpoint()
			));
	
	private ArrayList<String> LIST_ETAG_LABEL = new ArrayList<String>(Arrays.asList("Etag", "ETag")); //add as required
	

	
	public String getServerDNS(){
		
		String strServerDNS = AppUtils.getWiggleURL(BASE_URL_PRODUCTION);
		
		String.format(strServerDNS, AppSession.instance().getCurrentLocale().getCountry());
		
		if(AppUtils.isProductionVersionOfApp()==false){
			strServerDNS = AppUtils.getWiggleURL(BASE_URL_STAGING);
		}
		
		return strServerDNS;
		
	}
	
	private String getServerAddress(ObjApiRequest apiRequest){
		
		String strServerAddress = getServerDNS() + SERVER_API_DIRECTOY + SERVER_API_VERSION;
		
		return strServerAddress;
		
	}
	
	private String getApiBaseURL(ObjApiRequest apiRequest){

		//Mailgun - use configured url
		if(ENDPOINT_MAILGUN.equals(apiRequest.getURLEndpoint())){
			return ENDPOINT_MAILGUN;
		}
		
		String strBaseURL = getServerAddress(apiRequest);
		
		return strBaseURL;
		
	}
	

	
	
	RequestQueue requestQueue;
	
	public RequestQueue getRequestQueue(){
		return requestQueue;
	}
	
	public enum ON_RESPONSE_ACTION{
		NO_ACTION,
		START_WEBVIEW_ACTIVITY,
		OPEN_RETURN_URL,
		REFRESH_NAVIGATION_DRAWER,
		RETRIEVE_ORDERS_AFTER_AUTHENTICATION,
		SUBMIT_ORDER_TRACKING_REQUEST,
		RETRIEVE_USER_RELATED_DATA,
		SHOW_UPGRADE_REQUIRED_DIALOG
	}
	
	//this will ensure that requestQueue remains a singleton as long as this object is alive
	public static AppRequestHandler instance(){
        
        if (mInstance == null){
            mInstance = new AppRequestHandler();
			h = new ObjHoehle();

			ENDPOINT_MAILGUN = h.getValue(ObjHoehle.FIELD_AV);

        }
        
        return mInstance;
        
	}
	

	private AppRequestHandler(){

		//Introducing certificate pinning
		SSLSocketFactory sf = null;
		try{
			sf = getPinnedSSLCertificates();
		}catch(Exception e){
			AppUtils.showCustomToast("Certificate pinning not working!!", true);
		}

		//We want control over the thread pool size - hence we are not using the Volley-default method
		//to set up a request queue.
		//If there is no such requirement using the below call to set up the request queue works perfectly well.
		//requestQueue = Volley.newRequestQueue(ApplicationContextProvider.getContext());
		
		//We are using the following modification as we want to catch 302 (not found, redirect) errors from the server
		//This happens if a user is not authenticated when e.g. visiting his recent orders. We need to intercept this and 
		//silently submit a login request here.
		requestQueue = Volley.newRequestQueue(ApplicationContextProvider.getContext(), new HurlStack(null, sf) {
		    @Override
		    protected HttpURLConnection createConnection(URL url) throws IOException {
		        HttpURLConnection connection = super.createConnection(url);
		        connection.setInstanceFollowRedirects(false);

		        return connection;
		    }
		});
		
		/*
		// Instantiate the cache
		Cache cache = new DiskBasedCache(ApplicationContextProvider.getContext().getCacheDir(), 1024 * 1024); // 1MB max
        
		// Set up the network to use HttpURLConnection as the HTTP client.
		Network network = new BasicNetwork(new HurlStack());
		
		//Custom threadpool size
		int intCustomThreadpoolSize = 10; //allow up to 10 requests in parallel
		
		requestQueue = new RequestQueue(cache, network, intCustomThreadpoolSize);
		*/
		
		// Start the queue
		requestQueue.start();
        
		
	}
	
	
	/**
	 *  This method is used to submit HEAD requests to the API.
	 *  In the current absence of ETag information, we retrieve the last modified Header for endpoints that are network intensive such as
	 *  Cities, Buildings, Categories
	 *
	 *  @param apiRequest
	 *
	 */
	public void executeHeadRequest(final ObjApiRequest apiRequest){
		
		if(apiRequest==null){
			Logger.printMessage(logTag, "No request details supplied", Logger.ERROR);
			return;
		}
		

		//no network? Don't bother submitting requests - get data from cache
		if(isConnectedToNetwork()==false){
			if(ENDPOINT_NEWSFEED_DUMMY.equals(apiRequest.getURLEndpoint())
					|| 
					URL_NEWSFEED_CONFIG_FILE.equals(apiRequest.getURLEndpoint())
					|| 
					URL_NEWSFEED_SPORTS_CONFIG_FILE.equals(apiRequest.getURLEndpoint())
					|| 
					getOrderTrackingEndpoint().equals(apiRequest.getURLEndpoint())
					){
				
				boolean blnCachedDataRetrieved = getDataFromLocalCache(apiRequest);
				
				//for RSS Newsfeeds: special handling here: if we don't have any cached data for a request, notify NewsfeedActivity
				if(ENDPOINT_NEWSFEED_DUMMY.equals(apiRequest.getURLEndpoint()) && blnCachedDataRetrieved==false){
					onResponseParsingEnd(apiRequest, false);
				}
				
				return;
			}
		}
		
		
		//Note: do not fail these requests on missing network connection - let Volley retry policy handle this
		
		//build request URL depending on resource endpoint, request method etc
		String strTargetURL = buildRequestURL(apiRequest);
		
		//Default request method to "HEAD". If the header information indicates that the server data was changed, a second request will be
		//submitted using the passed ObjapiRequest
		ConcurrentHashMap<String,String> headerParams = getHeaderParams(apiRequest);
		ConcurrentHashMap <String,String> requestParams = getSimpleRequestParams(apiRequest); //standard 1:1 parameter list for POST and PUT only (Volley restriction)
		CustomHttpParams multisetRequestParams = getMultisetRequestParams(apiRequest); //1:n parameter list for POST and PUT only (Volley restriction) not used for any Dubizzle requests yet
		
		//for multipart file uploads (caution: Volley is not really good at processing big files ...)
		Map<String, File> multipartFileList = getMultipartFiles(apiRequest);
		
		addNetworkResponseRequestToQueue(apiRequest, strTargetURL, requestParams, headerParams, multisetRequestParams, multipartFileList, true);
		
	}
    
	/**
	 *  This method is used to submit actual requests for data to the Api.
	 *
	 *  @param apiRequest
	 */
	public void executeHTTPRequest(final ObjApiRequest apiRequest){
		
		
		if(apiRequest==null){
			Logger.printMessage(logTag, "No request details supplied", Logger.ERROR);
			return;
		}

		//for certain endpoints: we store the data received earlier in a file to save server calls. If the data is available and up to date, do not submit a server call
		//Disabled - as otherwise order tracking requests keeps reading from cache (we save the previously retrieved order tracking file for comparison
		//if(getDataFromLocalCache(apiRequest)){
		//	return;
		//}
		
		//after we have retrieved cached data - check if we are ok to submit a network request: do we have a network at all?
		//If we have no network connection - do not submit requests
		if(isConnectedToNetwork()==false){
			
			//for this app - we are only using cached data when there is no network
			getDataFromLocalCache(apiRequest);
			
			
			forwardApiError(apiRequest, 0, null);
			
			AppUtils.showCustomToast("No network!", true);
			
//			MainActivity.instance().showNetworkStatusMessage(apiRequest.getContext().getString(R.string.str_undo_bar_message_no_network)
//															,apiRequest.getContext().getString(R.string.str_undo_bar_button_text_hide)
//															, false);
			return;
		}
        
		
		//build request URL depending on resource endpoint, request method etc
		String strTargetURL = buildRequestURL(apiRequest);

		//display the URL in case we are on a test server
		if(!"".equals(AppUtils.REDIRECT_APP_TO_TEST_SERVER) || !AppUtils.isProductionVersionOfApp()){
			AppUtils.showCustomToast("Requesting:\n" + strTargetURL, false);
		}
		
		ConcurrentHashMap<String,String> headerParams = getHeaderParams(apiRequest);
		//Logger.e("header params", "header params: " + headerParams.toString());
		ConcurrentHashMap <String,String> requestParams = getSimpleRequestParams(apiRequest); //standard 1:1 parameter list for POST and PUT only (Volley restriction)
		CustomHttpParams multisetRequestParams = getMultisetRequestParams(apiRequest); //1:n parameter list for POST and PUT only (Volley restriction)
		
		//for multipart file uploads (caution: Volley is not really good at processing big files ...)
		Map<String, File> multipartFileList = getMultipartFiles(apiRequest);
		

		/* RSS Newsfeed - read as string requests */
		if(ENDPOINT_NEWSFEED_DUMMY.equals(apiRequest.getURLEndpoint())){
			addStringGETRequestToQueue(apiRequest, strTargetURL);
			return;
		}

		/* Mailgun - POST request expects parameters not in Json, but as  x-www-form-urlencoded */
		if(ENDPOINT_MAILGUN.equals(apiRequest.getURLEndpoint())){
			addStringPOSTRequestToQueue(apiRequest, strTargetURL, headerParams, requestParams);
			return;
		}
		
		/*
		 * For some apps: all POST requests require JSON data to be posted
		 * 
		 * Example: POST login 
		 * {"email": "stephan@andromedia.biz", "password":"password"}
		 * 
		 * This requires a CustomJsonPostRequest to be submitted (addJSONPostRequestToQueue)
		 * 
		 * For other apps we had standard Post body parameters 
		 * 
		 * email=stephan@andromedia.biz&password=password
		 * 
		 * For this case, a CustomNetworkResponseRequest can be submitted (addNetworkResponseRequestToQueue)
		 * 
		 */
		
		if(Method.POST==apiRequest.getVolleyHTTPMethod()){
			
			//Custom-built json body - for this app this is simply the request parameter list printed as JSON string
			JSONObject jsonPostBody = getJSONBodyToPost(apiRequest, requestParams);
			addJSONPostRequestToQueue(apiRequest, jsonPostBody, strTargetURL, requestParams, headerParams);
			
			
		}else{
			//default: add Network response request
			//addGZipRequestToQueue(apiRequest, strTargetURL, requestParams, headerParams, multisetRequestParams, multipartFileList, false);
			addNetworkResponseRequestToQueue(apiRequest, strTargetURL, requestParams, headerParams, multisetRequestParams, multipartFileList, false);
		}
		
	}
	

	
	/*
	 *
	 *  For certain endpoints: we store the data received earlier in a file to save server calls. If the data is available and up to date, do not submit a server call
	 *
	 */
	private boolean getDataFromLocalCache(ObjApiRequest apiRequest){
		
		boolean blnRetrievedDataFromCache = false;
		
		//exclusion of certain endpoints as they are dealt with in DataStore.readCachedDataImmediately()
		if("any_endpoint_for_immediate_reading".equals(apiRequest.getURLEndpoint())
           ){
			//return false;
		}
		
		if(ENDPOINT_NEWSFEED_DUMMY.equals(apiRequest.getURLEndpoint())){
			blnRetrievedDataFromCache = getStringDataFromCache(apiRequest);
		}else{
		
			blnRetrievedDataFromCache = getJsonObjectDataFromCache(apiRequest);
		
		}
		
		return blnRetrievedDataFromCache;
		
	}
	
	
	/*
	 *
	 *  This method is used to build the target URL for api requests.
	 *
	 */
	private String buildRequestURL(ObjApiRequest apiRequest){
		
		String strTargetURL;
		
		//a few exceptions (fixed URLs independent of market):
		if(URL_NEWSFEED_CONFIG_FILE.equals(apiRequest.getURLEndpoint()) 
			|| URL_NEWSFEED_SPORTS_CONFIG_FILE.equals(apiRequest.getURLEndpoint())
			|| ENDPOINT_ORDER_TRACKING_TEST.equals(apiRequest.getURLEndpoint())
			|| ENDPOINT_MAILGUN.equals(apiRequest.getURLEndpoint())
				|| ENDPOINT_UPGRADE_CHECK.equals(apiRequest.getURLEndpoint())
			){
			return apiRequest.getURLEndpoint();
		}
		
		if(ENDPOINT_NEWSFEED_DUMMY.equals(apiRequest.getURLEndpoint())){
			return apiRequest.getRequestURI();
		}
		
		
		//Default - append request endpoint
		strTargetURL = getApiBaseURL(apiRequest) + apiRequest.getURLEndpoint();
		
		//some requests require a URI to be appended with object IDs embedded in the URL
		if(apiRequest.getRequestURI()!=null && !apiRequest.getRequestURI().equals("")){
			strTargetURL = getApiBaseURL(apiRequest) + apiRequest.getRequestURI();
		}
		
		//categories need to be amended with the corresponding API path.. 
		if(ENDPOINT_CATEGORIES.equals(apiRequest.getURLEndpoint())){
			
			ObjNavigationCategory category = (ObjNavigationCategory) apiRequest.getApiObject();
			
			//for locally sourced categories - use the following logic
			if(ObjNavigationCategory.CONST_CATEGORY_TYPE_CATEGORIES.equals(category.getString(ObjNavigationCategory.FIELD_TYPE))
					||
					ObjNavigationCategory.CONST_CATEGORY_TYPE_END_CATEGORY_OPEN_URL.equals(category.getString(ObjNavigationCategory.FIELD_TYPE))
					){
				strTargetURL = AppUtils.getWiggleURL(BASE_URL_PRODUCTION);
				strTargetURL = strTargetURL + apiRequest.getURLEndpoint();
				String strAPIUrl = category.getString(ObjNavigationCategory.FIELD_API_URL);
				strTargetURL = strTargetURL + "/" + strAPIUrl;
			}else if(ObjNavigationCategory.CONST_CATEGORY_TYPE_WIGGLE_API.equals(category.getString(ObjNavigationCategory.FIELD_TYPE))){
				//for Wiggle's api categories - simply use the api URL string provided
				strTargetURL = category.getString(ObjNavigationCategory.FIELD_REFINEMENT_API_URL);
			}
			
			//Important: for Chinese / Japanese URLs we need URL encoding
			//to turn e.g. http://www.wiggle.cn/api/list/cycle/自行车/
			//into http://www.wiggle.cn/api/list/cycle/%E8%87%AA%E8%A1%8C%E8%BD%A6/ 
			try{
				URL url = new URL(strTargetURL);
				URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
				URL urlEncoded = new URL(uri.toASCIIString()); //this is where the correct "encoding" happens. Thank you, http://stackoverflow.com/questions/4831301/invalid-uri-with-chinese-characters-java
				strTargetURL = urlEncoded.toString();
			} catch (MalformedURLException e1) {
				Logger.printMessage(logTag, "" + e1.getMessage(), Logger.ERROR);
			} catch (URISyntaxException e2) {
				Logger.printMessage(logTag, "" + e2.getMessage(), Logger.ERROR);
			}
			
		}else if(ENDPOINT_TYPEAHEAD.equals(apiRequest.getURLEndpoint())){
			
			//Exception for typeahead URL
			strTargetURL = AppUtils.getWiggleURL(ENDPOINT_TYPEAHEAD);
			
		}else if(ENDPOINT_MEGAMENU.equals(apiRequest.getURLEndpoint())) {

			ObjNavigationCategory callingCategory = (ObjNavigationCategory) apiRequest.getApiObject();

			//for locally sourced categories - use the following logic
			String strDataMap = callingCategory.getString(ObjNavigationCategory.FIELD_DATA_MAP);

			int intCountryId = ObjAppUser.instance().getDeliveryDestination();

			//build request URL
			strTargetURL = strTargetURL + "/" + intCountryId + "/" + strDataMap;
			AppUtils.showCustomToast("Megamenu requested: " + strTargetURL, true);

			//Important: for Chinese / Japanese URLs we need URL encoding
			//to turn e.g. http://www.wiggle.cn/api/list/cycle/自行车/
			//into http://www.wiggle.cn/api/list/cycle/%E8%87%AA%E8%A1%8C%E8%BD%A6/
			try {
				URL url = new URL(strTargetURL);
				URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
				URL urlEncoded = new URL(uri.toASCIIString()); //this is where the correct "encoding" happens. Thank you, http://stackoverflow.com/questions/4831301/invalid-uri-with-chinese-characters-java
				strTargetURL = urlEncoded.toString();
			} catch (MalformedURLException e1) {
				Logger.printMessage(logTag, "" + e1.getMessage(), Logger.ERROR);
			} catch (URISyntaxException e2) {
				Logger.printMessage(logTag, "" + e2.getMessage(), Logger.ERROR);
			}

		}



				//for GET methods - add query string
		if(apiRequest.getVolleyHTTPMethod()==Method.GET && apiRequest.getQueryString()!=null){
			
			String strQueryString = apiRequest.getQueryString();
    		
			//add ? to core-URL
    		if(!strTargetURL.endsWith("?")){
    			strTargetURL += "?";
    		}
    		
    		strTargetURL = strTargetURL + strQueryString;
    		
            
		}else{
			
			//no GET request - add slash
			if(!strTargetURL.endsWith("/")){
				strTargetURL += "/";
			}
		}
		
		return strTargetURL;
	}
	
	
	/**
	 * POST application json data to server.
	 * @param apiRequest
	 * @param jsonRequest
	 * @param strTargetURL
	 * @param requestParams
	 * @param headerParams
	 */
	private void addJSONPostRequestToQueue(final ObjApiRequest apiRequest,JSONObject jsonRequest, String strTargetURL, Map<String,String> requestParams, Map<String,String> headerParams){
		
		CustomJSONPostRequest jsonPostRequest = new CustomJSONPostRequest(
				
                                	apiRequest.getVolleyHTTPMethod()
                                   , strTargetURL
                                   , jsonRequest
                                   , requestParams
                                   , headerParams
                                   , new Listener<JSONObject>() {
                                    
	                                    @Override
	                                    public void onResponse(final JSONObject response) {

	                                        //parse response in Background thread as this will block main thread otherwise
	                                        new Thread(new Runnable() {
	                                            public void run() {
                                                try {
                                                    handleJsonObjectResponse(apiRequest, response, false);
                                                    Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - response processed", Logger.INFO);
                                                } catch (JSONException e) {
                                                    Logger.printMessage(logTag, apiRequest.getURLEndpoint() + ": " + e.toString(), Logger.ERROR);
                                                    Logger.e("error",e.getMessage());
                                                    AppUtils.showCustomToast(apiRequest.getURLEndpoint() + ": " + e.toString(), true);
                                                }
	                                            }
	                                        }).start();
	                                    }
                                	}
                                 , new ErrorListener() {
                                    
	                                    @Override
	                                    public void onErrorResponse(VolleyError error) {
	                                        // TODO Auto-generated method stub
	                                        handleErrorResponse(apiRequest, error);
	                                        
	                                    }
                                });
		
		
		//set request priority if delivered
		Priority specifiedPriority = apiRequest.getRequestPriority();
		if(specifiedPriority!=null){
			jsonPostRequest.setPriority(specifiedPriority);
		}
		
		
		//Setting a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions - let Volley retry the request
		jsonPostRequest.setRetryPolicy(new DefaultRetryPolicy(
                                                              (int) TimeUnit.SECONDS.toMillis(VOLLEY_RETRY_POLICY_TIMEOUT_IN_SECONDS),
                                                              VOLLEY_RETRY_POLICY_MAX_RETRIES,
                                                              VOLLEY_RETRIY_POLICY_BACKOFF_MULTIPLIER));
		
		
		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - add request to Volley queue", Logger.INFO);
        requestQueue.add(jsonPostRequest);
	}
	
	
    
    private Response.Listener<NetworkResponse> createNetworkResponseSuccessListener(final ObjApiRequest apiRequest, final boolean blnHeadRequestOnly) {
        return new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(final NetworkResponse response) {
                
        		if(Logger.CURRENT_LOGING_LEVEL==Logger.ALL){
        			String strJSONResponse = response.data.toString();
        			Logger.printMessage(logTag, apiRequest.getURLEndpoint() + "\n" + strJSONResponse, Logger.INFO);
        		}
        		
        		
        		//Some requests are only submitted to verify HTTP headers. Handle those separately.
        		if(blnHeadRequestOnly){
        			//Logger.e("response headers",response.headers.toString());
        			getHttpResponseHeaderInfo(apiRequest, response.headers);
        			//stop after parsing header as we will not have a response body
        			return;
        		}else{
        			//if we had a successful response for an actual data request - save ETag if available for later use
        			//Logger.e(" else response headers",response.headers.toString());
        			saveETagIfAvailable(apiRequest, response.headers);
        		}
        		
        		
        		
        		//parse response body in Background thread as this will block mainthread otherwise
        		new Thread(new Runnable() {
        	        public void run() {
        	        	
        	        	
	        		//for this app - only handle Json Object response - exception: RSS newsfeed requests (they are XML)
        	        if(ENDPOINT_NEWSFEED_DUMMY.equals(apiRequest.getURLEndpoint())){
        	        	handleNetworkResponse(apiRequest, response);
        	        }else{
        	        	convertToJsonObjectResponse(apiRequest, response);
        	        }
	    		        	        
        	        }
        	    }).start();
                
            }
        };
    }
    
    
    
    private Response.ErrorListener createNetworkResponseErrorListener(final ObjApiRequest apiRequest) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                
            	handleErrorResponse(apiRequest, error);
            }
        };
    }
    
    
	private void addNetworkResponseRequestToQueue(final ObjApiRequest apiRequest, String strTargetURL, Map<String,String> requestParams, Map<String,String> headerParams, CustomHttpParams multisetRequestParams, Map<String, File> multipartFileList, boolean blnHeadRequestOnly){
		
		int intRequestMethod = apiRequest.getVolleyHTTPMethod();
		
		//for HEAD requests only - set method accordingly
		if(blnHeadRequestOnly){
			intRequestMethod = Method.HEAD;
			Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - add HEAD request to Volley queue", Logger.INFO);
		}else{
			Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - add full request to Volley queue", Logger.INFO);
		}
		
		
		CustomNetworkResponseRequest volleyRequest = new CustomNetworkResponseRequest(
                                                                                          intRequestMethod
                                                                                          , strTargetURL
                                                                                          , requestParams
                                                                                          , headerParams
                                                                                          , multisetRequestParams
                                                                                          , multipartFileList
                                                                                          , createNetworkResponseSuccessListener(apiRequest, blnHeadRequestOnly)
                                                                                          , createNetworkResponseErrorListener(apiRequest)
                                                                                          );
		
		
		//set request priority if delivered
		Priority specifiedPriority = apiRequest.getRequestPriority();
		if(specifiedPriority!=null){
			volleyRequest.setPriority(specifiedPriority);
		}
        
		//Setting a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions - let Volley retry the request
		volleyRequest.setRetryPolicy(new DefaultRetryPolicy(
                                                            (int) TimeUnit.SECONDS.toMillis(VOLLEY_RETRY_POLICY_TIMEOUT_IN_SECONDS),
                                                            VOLLEY_RETRY_POLICY_MAX_RETRIES,
                                                            VOLLEY_RETRIY_POLICY_BACKOFF_MULTIPLIER));
        
		//Important: a head request returns a null response. We run in danger of caching these responses - and later requests to the same URL
		//would be returned with no data from the cache. As a result: NEVER cache head-only requests!
		volleyRequest.setShouldCache(false);
		
		requestQueue.add(volleyRequest);
		
	}  
	

	private void addStringGETRequestToQueue(final ObjApiRequest apiRequest, String strTargetURL){
		
		int intRequestMethod = apiRequest.getVolleyHTTPMethod();
		
		
		StringRequest volleyRequest = new StringRequest(Request.Method.GET, strTargetURL, 
			    new Response.Listener<String>() 
			    {
			        @Override
			        public void onResponse(String response) {
			            handleStringResponse(apiRequest, response, false);
			        }
			    }, 
			    new Response.ErrorListener() 
			    {
			         @Override
			         public void onErrorResponse(VolleyError error) {                                
			         // handle error response
			        	 //String request is only used for newsfeed - just notify activity that request has been processed
			        	 onResponseParsingEnd(apiRequest, false);
			       }
			    }
			);
		
		
        
		//Setting a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions - let Volley retry the request
		volleyRequest.setRetryPolicy(new DefaultRetryPolicy(
                                                            (int) TimeUnit.SECONDS.toMillis(VOLLEY_RETRY_POLICY_TIMEOUT_IN_SECONDS),
                                                            VOLLEY_RETRY_POLICY_MAX_RETRIES,
                                                            VOLLEY_RETRIY_POLICY_BACKOFF_MULTIPLIER));
        
		requestQueue.add(volleyRequest);

	}



	private void addStringPOSTRequestToQueue(final ObjApiRequest apiRequest, String strTargetURL
			, Map<String, String> headerParams, Map<String, String> requestParams){

		int intRequestMethod = apiRequest.getVolleyHTTPMethod();


		CustomStringPostRequest volleyRequest = new CustomStringPostRequest(
				intRequestMethod
			  , strTargetURL
				, requestParams
				, headerParams
				,
				new Response.Listener<String>()
				{
					@Override
					public void onResponse(String response) {
						handleStringResponse(apiRequest, response, false);
					}
				},
				new Response.ErrorListener()
				{
					@Override
					public void onErrorResponse(VolleyError error) {
						// handle error response
						handleErrorResponse(apiRequest, error);
					}
				}
		);



		//Setting a retry policy in case of SocketTimeout & ConnectionTimeout Exceptions - let Volley retry the request
		volleyRequest.setRetryPolicy(new DefaultRetryPolicy(
				(int) TimeUnit.SECONDS.toMillis(VOLLEY_RETRY_POLICY_TIMEOUT_IN_SECONDS),
				VOLLEY_RETRY_POLICY_MAX_RETRIES,
				VOLLEY_RETRIY_POLICY_BACKOFF_MULTIPLIER));

		requestQueue.add(volleyRequest);

	}


	private void convertToJsonObjectResponse(ObjApiRequest apiRequest, NetworkResponse response){
    	
    	byte[] data = null;
    	
    	if(isGzipped(response)){
    		
    		try {
                data = decompressResponse(response.data);
                
            } catch (IOException e1) {
                Logger.printMessage(logTag, "gzip decode error:" + e1, Logger.ERROR);
                return;
            }
    		
    	}else{
    		data = response.data;
    	}
    	
 	   	try {
            String jsonString = new String(data, HttpHeaderParser.parseCharset(response.headers));
            if(jsonString==null || "".equals(jsonString)){
            	return;
            }
            handleJsonObjectResponse(apiRequest, new JSONObject(jsonString), false);
        } catch (UnsupportedEncodingException e) {
            Logger.printMessage(logTag, apiRequest.getURLEndpoint() + ": " + e.toString(), Logger.ERROR);
            return;
        } catch (JSONException je) {
            Logger.printMessage(logTag, apiRequest.getURLEndpoint() + ": " + je.toString(), Logger.ERROR);
            AppUtils.showCustomToast(apiRequest.getURLEndpoint() + ": " + je.toString(), true);
            return;
        }
    	
        
    	
    	
    }
    
    
    
    private void getHttpResponseHeaderInfo(final ObjApiRequest apiRequest, Map<String, String> responseHeader){
    	
    	//get ETag
    	String strServerETag = null;
    	for(int i=0;i<LIST_ETAG_LABEL.size();i++){
    		String strEtagLabel = LIST_ETAG_LABEL.get(i);
    		strServerETag = responseHeader.get(strEtagLabel);
    		if(strServerETag!=null){
    			break;
    		}
    	}
    	
    	//get Last Modified date
    	String strLastModified = responseHeader.get("Last-Modified");
        
    	//get last modified date
        long lastModifiedDate = 0;
        
        if (strLastModified != null) {
        	//this returns 0 in case of exceptions ...
        	lastModifiedDate = HttpHeaderParser.parseDateAsEpoch(strLastModified);
        }
        
        //ask Data store if our cached data is out of date (based on ETag or last modified date)
		String strCacheFileName = getCacheFileName(apiRequest);
		
		final String strUserCountry = ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY);
        boolean blnCacheOutOfDate = DataStore.isLocalDataOutdated(strUserCountry, strCacheFileName, lastModifiedDate, strServerETag);
        
        if(blnCacheOutOfDate){
        	Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - no cache or out of date. Submit Server request", Logger.INFO);
        }else{
        	Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - cache usable - get Data from Cache", Logger.INFO);
        }
        
        //Now re-submit the ORIGINAL api request (usually a GET) to retrieve the data either from cache or network
        apiRequest.submit();
    	
    }
    
    private void saveETagIfAvailable(final ObjApiRequest apiRequest, Map<String, String> responseHeader){
    	
    	//get ETag
    	String strServerETag = null;
    	for(int i=0;i<LIST_ETAG_LABEL.size();i++){
    		String strEtagLabel = LIST_ETAG_LABEL.get(i);
    		strServerETag = responseHeader.get(strEtagLabel);
    		if(strServerETag!=null){
    			break;
    		}
    	}
    	
    	
    	if(strServerETag!=null && !"".equals(strServerETag)){
    		
    		String strCacheFileName = getCacheFileName(apiRequest);
    		
    		DataStore.saveStringToSharedPreferences(DataStore.SHARED_PREF_ETAG_STORE, strCacheFileName, strServerETag);
    	}
    	
    }
    
    
    
	private ConcurrentHashMap<String, String> getHeaderParams(ObjApiRequest apiRequest) {
		
		
		ConcurrentHashMap<String,String> headerParams = new ConcurrentHashMap<String, String>();
		
		if(getOrderTrackingEndpoint().equals(apiRequest.getURLEndpoint())) {
			
			/*
			    
			    Request format for Order tracking is ... 
			  
				GET https://www.wiggle.co.uk/api/orders/recent HTTP/1.1
				Host: www.wiggle.co.uk
				Content-Type: application/json
				Cookie:SecureLogin=24D3A2FA1950F1EC5C234DEF3273F90655A0899871F7B6562434D32B5145D1873DE09FCBA56624ED71CB1BA0C7CE2A55B7469978F106E58F33742701A8EF08A631AE1FAC5F80F2CE2575C8BC4062A2494EB0D1ED8C037F6C84020D385B9D894F37EAFE918BD288E35031C22D868321B170A220CE

			*/

			//the Order tracking endpoint requires a cookie "SecureLogin" to be sent along with it
			String strSecureLoginCookie = "SecureLogin=" + ObjAppUser.instance().getString(ObjAppUser.FIELD_ACCESS_TOKEN);
			headerParams.put("Cookie", strSecureLoginCookie);

			//add some additional tokens as required
			//String strUniqueDeviceID = Secure.getString(apiRequest.getContext().getContentResolver(), Secure.ANDROID_ID);
			//headerParams.put("IMEI", strUniqueDeviceID);


		}else if(ENDPOINT_MAILGUN.equals(apiRequest.getURLEndpoint())){

			//Mailgun requires url encoding of POST parameters
			//NOTE: this is also done in CustomStringPostRequest.getBodyContentType(), but does not seem to be sufficient on its own
			headerParams.put("Content-Type", "application/x-www-form-urlencoded");

			//Mailgun requires a specific header
			ObjHoehle h = new ObjHoehle();

			String strMailgunApiUsernamePassword = h.getValue(ObjHoehle.FIELD_AU);
			//we need to create a Base64 string from the username:password pair combination
			//Plain text:
			// Authorization: Basic api:key-b744e07a1a1fc541e182bde8a5361479
			//Converted to:
			// Authorization: Basic YXBpOmtleS1iNzQ0ZTA3YTFhMWZjNTQxZTE4MmJkZThhNTM2MTQ3OQ==

			try {
				byte[] data = strMailgunApiUsernamePassword.getBytes("UTF-8");
				String strBase64 = Base64.encodeToString(data, Base64.DEFAULT);
				//headerParams.put("Authorization", "Basic " + strBase64) - masked:
				headerParams.put(h.getValue(ObjHoehle.FIELD_AQ), h.getValue(ObjHoehle.FIELD_AR) + " " + strBase64);
			} catch (UnsupportedEncodingException e) {
				Logger.printMessage(logTag, "getHeaderParams() for Mailgun failed", Logger.ERROR);
			}


		}else{
			
			//for all other API-requests, use access-token as returned by Authorisation procedure
			//X-Auth-Token: access token
			String strToken = getApiAccessToken();
			//headerParams.put("X-Auth-Token", strToken);

			ObjHoehle h = new ObjHoehle();

			//header parameters for API requests
			String strUserAgentKey = h.getValue(ObjHoehle.FIELD_AX);
			String strUserAgentValue = h.getValue(ObjHoehle.FIELD_AW) + AppUtils.getAppVersionNumber();
			headerParams.put(strUserAgentKey, strUserAgentValue);

			//Limit exposure of Auth token for now: send it with auth endpoints only
			if(getAuthenticationEndpoint().equals(apiRequest.getURLEndpoint())
				|| ENDPOINT_VERIFY_MFA.equals(apiRequest.getURLEndpoint())
					|| ENDPOINT_SILENT_LOGIN.equals(apiRequest.getURLEndpoint())
				) {
				String strAuthTokenKey = h.getValue(ObjHoehle.FIELD_AY);
				String strAuthTokenValue = h.getValue(ObjHoehle.FIELD_AZ);
				headerParams.put(strAuthTokenKey, strAuthTokenValue);
			}

		}
		
        
		
        return headerParams;
		
	}
	
    
	/*
	 * Use this method if you only need to supply simple, non-repetitive request parameters
	 * Please note: Volley only uses parameter lists for PUT and POST requests so this method can only be used for those operations.
	 * Any GET request parameters need to be passed to volley as part of the URL.
	 *
	 */
	private ConcurrentHashMap <String, String> getSimpleRequestParams(ObjApiRequest apiRequest){
		
		ConcurrentHashMap <String,String> requestParams = new ConcurrentHashMap <String, String>();
		

		//Many Api requests do not need request parameters apart from the request header.
		//For the following endpoints, request parameters need to be defined.
		if(getAuthenticationEndpoint().equals(apiRequest.getURLEndpoint())){
			
			ObjAppUser user = (ObjAppUser) apiRequest.getApiObject();
			
			requestParams.put(ObjAppUser.FIELD_USERNAME, user.getString(ObjAppUser.FIELD_EMAIL)); 
			requestParams.put(ObjAppUser.FIELD_PASSWORD, user.getPassword());

		}else if(ENDPOINT_VERIFY_MFA.equals(apiRequest.getURLEndpoint())){

			/*
				Parameters:
				LoginVerificationId
				plus either
				TOTP or RecoveryCode
				depending if this is a login verification or account recovery
			 */

			ObjTFA tfaObject = (ObjTFA) apiRequest.getApiObject();
			String strLoginVerificationId = tfaObject.getString(ObjTFA.FIELD_LOGIN_VERIFICATION_ID);
			String strTOTP = tfaObject.getString(ObjTFA.FIELD_LOGIN_TOTP);
			String strRecoveryCode = tfaObject.getString(ObjTFA.FIELD_LOGIN_RECOVERY_CODE);

			//Copy the tfaObject to the 'infoObject' as during login the apiObject will be overwritten with ObjAppUser
			apiRequest.setInfoObject(tfaObject);

			requestParams.put(ObjTFA.FIELD_LOGIN_VERIFICATION_ID, strLoginVerificationId);

			if(strTOTP != null && !"".equals(strTOTP)) {
				requestParams.put(ObjTFA.FIELD_LOGIN_TOTP, strTOTP);
			}

			if(strRecoveryCode != null && !"".equals(strRecoveryCode)){
				requestParams.put(ObjTFA.FIELD_LOGIN_RECOVERY_CODE, strRecoveryCode);
			}


		}else if(ENDPOINT_SILENT_LOGIN.equals(apiRequest.getURLEndpoint())){

			//pass refresh token (read from secure shared preferences)
			requestParams.put(JSON_FIELD_REFRESH_TOKEN, getRefreshToken());

		}else if(ENDPOINT_MAILGUN.equals(apiRequest.getURLEndpoint())){

			ObjHoehle h = new ObjHoehle();

			//construct mailgun parameters
			String strFrom = h.getValue(ObjHoehle.FIELD_AS);
			String strTo = h.getValue(ObjHoehle.FIELD_AT);
			String strSubject = "Wiggle App Feedback";


			String strFeedbackMessage = ((ObjGeneric) apiRequest.getApiObject()).getString(ObjGeneric.FIELD_NAME);

			//Extra information
			String strAppVersion = "App Version: Android " + AppUtils.getAppVersionNumber();
			String strDevice = "Device: " +  AppUtils.getDeviceName();
			String strCustomerId = "Customer ID: " + ObjAppUser.instance().getString(ObjAppUser.FIELD_CUSTOMER_ID);


			//Default - get language part of locale (e.g. en_GB -> retrieve "en")
			String strDeviceLanguage = "Language: " + Locale.getDefault().getLanguage();

			//String strLanguage = "Language: " + ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_LANGUAGE);

			String strBody = "App Name: Wiggle (Android)" +
					"\n" +
					strAppVersion +
					"\n" +
					strDevice +
					"\n" +
					strCustomerId +
					"\n" +
					strDeviceLanguage +
					"\n\n" +
					"FEEDBACK MESSAGE:" +
					"\n" +
					"=================" +
					"\n" +
					strFeedbackMessage
					;

			requestParams.put("from", strFrom);
			requestParams.put("to", strTo);
			requestParams.put("subject", strSubject);

			//TODO: check if we need URI encoding of long text e.g.
			// strMessage = Uri.encode(strMessage);
			requestParams.put("text", strBody);
			requestParams.put("attachment", "");

		}
		
		return requestParams;
		
	}
	
	
	/*
	 * Use this method if you need to supply a list of values for a given request parameter
	 * Please note: Volley only uses parameter lists for PUT and POST requests so this method can only be used for those operations.
	 * Any GET request parameters need to be passed to volley as part of the URL.
	 *
	 */
	private CustomHttpParams getMultisetRequestParams(ObjApiRequest apiRequest){
		
		CustomHttpParams requestMultisetParams = new CustomHttpParams();
		
		if(apiRequest.getURLEndpoint().equals("test-ignore-this")){
			
			//Example for using this method - add key-List<String> pairs for single or multiple values per key as follows:
			//Case 1: standard name-value pair
			requestMultisetParams.put("name_1", Arrays.asList("value"));
			
			//Case 2: a name-List<String> pair
			List<String> parameterList = Arrays.asList(
                                                       "value1",
                                                       "value2,"
                                                       );
			
			requestMultisetParams.put("name_2", parameterList);
			
		}
        
		
		return requestMultisetParams;
		
	}
	
	/*
	 * Use this method if you need to supply files to an API endpoint (e.g. upload a file)
	 * Please note: Volley is not good at handling large files. This may need improving over time.
	 *
	 */
	private Map<String, File> getMultipartFiles(ObjApiRequest apiRequest){
		
		Map<String, File> multipartFileList = new HashMap<String,File>();
		

		return multipartFileList;
		
	}
	
	public byte[] getBytesFromBitmap(String filepath) {
		
		File file = new File(filepath);
		FileInputStream fis = null;
		try {
		    fis = new FileInputStream(file);
        } catch (FileNotFoundException e) {
		    e.printStackTrace();
		}
        
		Bitmap bm = BitmapFactory.decodeStream(fis);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.JPEG, 100 , baos);
		byte[] b = baos.toByteArray();
		
		return b;
	}
	
	private byte[] readBytes(File file){
		int size = (int) file.length();
	    byte[] bytes = new byte[size];
	    try {
	        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
	        buf.read(bytes, 0, bytes.length);
	        buf.close();
	    } catch (FileNotFoundException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    } catch (IOException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }
	    return bytes;
	}
	
	
	private void handleJsonObjectResponse(final ObjApiRequest apiRequest, final JSONObject jsonPayload, boolean isCachedData) throws JSONException{
		
		/*
		 *  An API request can return a successful response in the format
		 *     {"data":  {  ... "data":[{}, {},..] }
		 *  or an Error response in the format
		 *     {"error": ".." "message": {...} }
		 *
		 *  Each response is checked for errors first. Parsing of a successful response will only be triggered if there are no errors
		 *
		 *  Please note that an error response is handled in "handleErrorResponse". And erroneous request does not reach the handleServerResponse method
		 */
        
		
		/*
		 *  For all requests: Extract server return codes
		 */
		
		JsonObjectParser.instance().setRequestApiReturnCodes(apiRequest, jsonPayload);
		
		if(apiRequest.isResponseError()){

			if (apiRequest.getContext() instanceof TabbedHybridActivity) {

				TabbedHybridActivity.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								handleBespokeApiErrorResponse(apiRequest); //for this app - server returns HTTP 200 with Success / Reason strings
							}
						}));

			}else if (apiRequest.getContext() instanceof V3HybridActivity) {

				V3HybridActivity.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								handleBespokeApiErrorResponse(apiRequest); //for this app - server returns HTTP 200 with Success / Reason strings
							}
						}));

			}else{
				handleBespokeApiErrorResponse(apiRequest); //for this app - server returns HTTP 200 with Success / Reason strings
			}
			return;
		}
		
		/*
		 *  Parse successful responses
		 */
		if(getAuthenticationEndpoint().equals(apiRequest.getURLEndpoint())
				) {

			//A standard first authorization request - once authorization successful, submit other meta data requests.
			String strApiAccessToken = JsonObjectParser.instance().handleAuthenticationResponse(apiRequest, jsonPayload);
			setApiAccessToken(strApiAccessToken);

			//if this is a re-authentication - get the original api request that failed with error 412 and re-execute it
			if (apiRequest.getFailedRequest() != null) {
				ObjApiRequest requestToResubmit = apiRequest.getFailedRequest();
				requestToResubmit.submit();
				return;
			}


			//save login date as last app refresh date
			AppUtils.saveLastAuthenticationTimestamp();

		}else if(ENDPOINT_VERIFY_MFA.equals(apiRequest.getURLEndpoint())){

			//The verify MFA endpoint has the same response structure as the "non Two-Factor" login
			//So we are handling it the same way (except failed request handling)
			String strApiAccessToken = JsonObjectParser.instance().handleAuthenticationResponse(apiRequest, jsonPayload);
			setApiAccessToken(strApiAccessToken);

			//save login date as last app refresh date
			AppUtils.saveLastAuthenticationTimestamp();

		}else if(ENDPOINT_SILENT_LOGIN.equals(apiRequest.getURLEndpoint())){

			//The verify Silent login (Refresh token) endpoint has the same response structure as the MFA / "non Two-Factor" login
			//So we are handling it the same way (except failed request handling)
			String strApiAccessToken = JsonObjectParser.instance().handleAuthenticationResponse(apiRequest, jsonPayload);
			setApiAccessToken(strApiAccessToken);

			//save login date as last app refresh date
			AppUtils.saveLastAuthenticationTimestamp();

			
		}else if(ENDPOINT_CATEGORIES.equals(apiRequest.getURLEndpoint())
				){
			
			JsonObjectParser.instance().handleCategoryResponse(apiRequest, jsonPayload, isCachedData);
	
			
		}else if(LOCAL_NAVIGATION_FILE.equals(apiRequest.getURLEndpoint())){
			
			JsonObjectParser.instance().parseLocalNavigationJson(apiRequest, jsonPayload);
			
		}else if(ENDPOINT_TYPEAHEAD.equals(apiRequest.getURLEndpoint())){
			
			JsonObjectParser.instance().parseTypeaheadResponse(apiRequest, jsonPayload);
			
		}else if(URL_NEWSFEED_CONFIG_FILE.equals(apiRequest.getURLEndpoint())){
			
			JsonObjectParser.instance().parseNewsfeedConfigFile(apiRequest, jsonPayload);
			
		}else if(URL_NEWSFEED_SPORTS_CONFIG_FILE.equals(apiRequest.getURLEndpoint())){
			
			JsonObjectParser.instance().parseNewsfeedSportsConfigFile(apiRequest, jsonPayload);
			
		}else if(getOrderTrackingEndpoint().equals(apiRequest.getURLEndpoint())){
			
			JsonObjectParser.instance().parseOrderTrackingResponse(apiRequest, jsonPayload, isCachedData);
			
		}else if(ENDPOINT_UPGRADE_CHECK.equals(apiRequest.getURLEndpoint())){
			JsonObjectParser.instance().parseUpgradeCheckResponse(apiRequest, jsonPayload);

		}else if(ENDPOINT_MEGAMENU.equals(apiRequest.getURLEndpoint())){
			JsonObjectParser.instance().parseMegamenuResponse(apiRequest, jsonPayload);
		}
		
	}
	
	
	
    
	
	
    
    
	
	private void handleErrorResponse(final ObjApiRequest apiRequest, final VolleyError volleyError){
		
		Logger.printMessage(logTag, "Volley error: " + volleyError.getMessage()+"", Logger.ERROR);
		
		//for RSS Newsffeds: special handling here - network response requests are used for Head only requests. If a head 
		//only request results in an error, just notify NewsfeedActivity of this. 
		if(ENDPOINT_NEWSFEED_DUMMY.equals(apiRequest.getURLEndpoint())){
			onResponseParsingEnd(apiRequest, false);
			return;
		}else if(ENDPOINT_MAILGUN.equals(apiRequest.getURLEndpoint())){
			//App feedback email - display error
			forwardApiError(apiRequest, 0, null);
			return;
		}else if(ENDPOINT_UPGRADE_CHECK.equals(apiRequest.getURLEndpoint())){
			//handle error silently - ignore and continue
			forwardApiError(apiRequest, 0, null);
			return;
		}else if(ENDPOINT_MEGAMENU.equals(apiRequest.getURLEndpoint())){
			forwardApiError(apiRequest, 0, null);
		}

		
		if(volleyError instanceof NetworkError || volleyError instanceof NoConnectionError ){
			

			if(volleyError instanceof NoConnectionError){
				
				//fallback if internet connection fails during request processing
				//MainActivity.instance().sendATITag(MainActivity.ATIPageAlertNoConnection, null, MainActivity.ATIActionPageView);
				//AppTagManager.getInstance().sendDubizzleTags(MainActivity.ATIPageAlertNoConnection, null, MainActivity.ATIActionPageView);

				//hide potential "searching" alert
				//MainActivity.instance().hideDubizzleAlert();
				
				if(!isConnectedToNetwork()){
					
					//AppUtils.showCustomToast("No network! Leads will be stored uploaded as soon as network is available.", false);
//					MainActivity.instance().showNetworkStatusMessage(apiRequest.getContext().getString(R.string.str_undo_bar_message_no_network)
//                                                                 ,apiRequest.getContext().getString(R.string.str_undo_bar_button_text_retry)
//                                                                 , false);
				}
				
			}
			
			
			
			return;
			
            
		}else if(volleyError instanceof TimeoutError){
			
			//Logger.e("error","empty" + "inside time out");
            //			MainActivity.instance().sendATITag(MainActivity.ATIPageAlertNoConnection, null, MainActivity.ATIActionPageView);
			
            //    		MainActivity.instance().alertUser(
            //  				  R.drawable.alert_flash_icon
            //  				, ""
            //  				, apiRequest.getContext().getString(R.string.alert_alertTitleConnectionLost)
            //  				, apiRequest.getContext().getString(R.string.alert_alertDetailConnectionLost)
            //  				, Alert.OK_BUTTON_NO_ACTION);
			
			
//			MainActivity.instance().showNetworkStatusMessage(apiRequest.getContext().getString(R.string.str_undo_bar_message_no_network)
//                                                             ,apiRequest.getContext().getString(R.string.str_undo_bar_button_text_retry)
//                                                             , false);
			
			if(AppRequestHandler.getAuthenticationEndpoint().equals(apiRequest.getURLEndpoint()) ||
					AppRequestHandler.ENDPOINT_CATEGORIES.equals(apiRequest.getURLEndpoint())){

				//for LeadR - we are only using cached data when there is no network
				getDataFromLocalCache(apiRequest);
			}
			
			forwardApiError(apiRequest, 0, null);
    		
			return;
			
		}else if(volleyError instanceof VolleyError){
			
			//This happens when a user tries to navigate a non-french Wiggle shop with french menu settings
			//Reason:
			//top_level_navigationl.json contains URL parts that don't exist in other markets.
			//Example: apiurl="cyclisme"
			//This then builds the URL 
			//http://www.wiggle.co.uk/api/list/cyclisme/ for the UK shop - a URL that does not exist
			//Volley returns:  Unexpected response code 500
			//TODO: for later versions - for french devices only offer french shop, otherwise use english top_level_navigation.json entries to get child elements
			if(ENDPOINT_CATEGORIES.equals(apiRequest.getURLEndpoint())){
				AppUtils.showCustomToast(
						"The Wiggle store in " 
						+ AppUtils.getCountryName() 
						+ " can only be navigated if your device language is set to English."
						, true);
				return;
			}
			

		}
		
		
		//OrderTracking - check if user was not authenticated
		//only needed for "live" order tracking endpoint, not for test endpoint
		if(ENDPOINT_ORDER_TRACKING.equals(apiRequest.getURLEndpoint())){

			//if(("" + volleyError.getMessage()).contains("/Secure/MyAccount/LogOn?ReturnUrl")){
			//URL pattern changed from LogOn to logon ... lowercase to make this future-proof
			if(("" + volleyError.getMessage().toLowerCase()).contains("myaccount/logon")){

				AppUtils.showCustomToast("Submitting login..", true);

				/*
				Remove email/password login and replace with MFA silent login ..
				new ObjApiRequest(apiRequest.getContext(), 
						null, 
						AppRequestHandler.getAuthenticationEndpoint(),
						Method.POST, 
						ObjAppUser.instance(), //use saved app user details
						null,
						ON_RESPONSE_ACTION.RETRIEVE_ORDERS_AFTER_AUTHENTICATION
						)
				.submit();
				 */

				//Silent login using the refresh token instead of email / password
				new ObjApiRequest(apiRequest.getContext(),
						null,
						AppRequestHandler.ENDPOINT_SILENT_LOGIN,
						Request.Method.POST,
						ObjAppUser.instance(), //use saved app user details
						null,
						AppRequestHandler.ON_RESPONSE_ACTION.RETRIEVE_ORDERS_AFTER_AUTHENTICATION)
						.submit();
			}
		
		}
		

		if(volleyError==null || volleyError.getMessage()==null){
			AppUtils.showCustomToast("Unknown error - please contact support", true);
			Logger.printMessage(logTag, apiRequest.getURLEndpoint() + ": " + "Unknown error", Logger.INFO);
			return;
		}
		
		
		String strVolleyResponse = volleyError.getMessage().toString();
		
		try{
			
	        JSONObject jsonErrorDetail = new JSONObject(strVolleyResponse);
	        
			JsonObjectParser.instance().setRequestApiReturnCodes(apiRequest, jsonErrorDetail);
	        
	        int intHTTPStatusCode = JsonObjectParser.getErrorCode(apiRequest, jsonErrorDetail, volleyError);
	        
	        //important to handle token expiry first...
	        if(intHTTPStatusCode==ERROR_CODE_INVALID_TOKEN){
	        	
	        	//Access token expired - remove cached access token and re-authenticate
	        	//DataStore.deleteCacheFile(apiRequest.getCountryCode(), ENDPOINT_LOGIN_EMAIL); //remove cached access token
	        	AppSession.instance().performLogout();
	        	

	        	// not submitting the original request because we could run into an infinite loop of 412s and resubmissions
	        	new ObjApiRequest(apiRequest.getContext(), null, AppRequestHandler.getAuthenticationEndpoint(), Method.POST, null, null, ON_RESPONSE_ACTION.NO_ACTION).submit();
	        	//re-try authentication and pass original api request so that it can be re-submitted after authentication
	        	//new ObjApiRequest(apiRequest.getContext(), DubizzleRequestHandler.ENDPOINT_AUTHENTICATE, Method.POST, apiRequest).submit();
	        	return;
	        	
	        }else if(intHTTPStatusCode==ERROR_CODE_RESOURCE_NOT_FOUND){
	        	

	        	
	        	
	        }else if(intHTTPStatusCode==ERROR_CODE_METHOD_NOT_IMPLEMENTED){
	        	

	        	
	        }else if(intHTTPStatusCode==ERROR_CODE_UNAUTHORIZED){
                

				//AppUtils.showCustomToast(ApplicationContextProvider.getContext().getString(R.string.str_api_error_message_unauthorized_user_feedback), true);
				forwardApiError(apiRequest, intHTTPStatusCode, null);
				return;

    			
    			
	        }else if(intHTTPStatusCode==ERROR_CODE_BAD_REQUEST){
    			
	        	if(getAuthenticationEndpoint().equals(apiRequest.getURLEndpoint())){
	        		//TODO: should be using server message - check if we get here at all
	        		AppUtils.showCustomToast("Invalid login - this message needs to be changed.", false);
	        		return;
	        	}
	        	
    			AppUtils.showCustomToast("Server error " + intHTTPStatusCode, true);
    			return;
	        	
	        	
	        }else if(intHTTPStatusCode==ERROR_CODE_INTERNAL_SERVER_ERROR
                     || intHTTPStatusCode==ERROR_CODE_SERVER_UNAVAILABLE){
                
    			AppUtils.showCustomToast("Server error " + intHTTPStatusCode, true);
    			
    			
    			/**
        		MainActivity.instance().alertUser(
                                                  R.drawable.alert_flash_icon
                                                  , ""
                                                  , apiRequest.getContext().getString(R.string.alert_alertTitle_internalServerError)
                                                  , apiRequest.getContext().getString(R.string.alert_alertDetail_internalServerError)
                                                  , Alert.OK_BUTTON_NO_ACTION);
        		
    			**/
    			
    			return;
                
    		}else{
    			
    			AppUtils.showCustomToast("Server error " + intHTTPStatusCode, true);
    			
    		}
            
	        
	        return;
	        
	    } catch(JSONException e){
			//catchall for networking error messages
	    	AppUtils.showCustomToast("Server message: " + strVolleyResponse, true);
	        e.printStackTrace();
	        
	    }
		
        
	}
	
	
    
	
	private boolean getJsonObjectDataFromCache(final ObjApiRequest apiRequest){
		
		//do not do anything if this is not a defined cache file
		if(!cacheFiles.contains(apiRequest.getURLEndpoint())){
			return false;
		}
		
		final String logTag = "Cache performance";
		
		String strCacheFileName = getCacheFileName(apiRequest);
		
		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " reading file start", Logger.INFO);
		final String strUserCountry = ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY);
		final String strCachedData = DataStore.readFileAsString(strUserCountry, strCacheFileName, getEndpointEncoding(apiRequest.getURLEndpoint()));
		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " reading file end", Logger.INFO);
		
		if(strCachedData==null || strCachedData.equals("")){
			return false; //data in cache not existing or out of date - a web request needs to be submitted
		}else{
			
			final String strThreadCacheFileName = strCacheFileName; //final string for thread - just a copy of strCacheFileName
			
			//parse cached data similar to what we receive from a web request
			//parse response in Background thread as this will block mainthread otherwise
    		new Thread(new Runnable() {
    	        public void run() {
	        	try {
                    
					Logger.printMessage(logTag, apiRequest.getURLEndpoint() + "Build JSON from String - start", Logger.INFO);
					JSONObject cacheObject = new JSONObject(strCachedData);
					Logger.printMessage(logTag, apiRequest.getURLEndpoint() + "Build JSON from String - end", Logger.INFO);
					
					Logger.printMessage(logTag, apiRequest.getURLEndpoint() + "Parse JSON - start", Logger.INFO);
					handleJsonObjectResponse(apiRequest, cacheObject, true);
					AppUtils.showCustomToast("Read from cache: " + apiRequest.getURLEndpoint(), true);
					Logger.printMessage(logTag, apiRequest.getURLEndpoint() + "Parse JSON - end", Logger.INFO);
					
                } catch (JSONException e) {
                    Logger.printMessage(logTag, "Error getting Data from cache: " + e.toString(), Logger.ERROR);
                    //if anything goes wrong with retrieving the data from the cache:
                    //delete cache file and simply submit original api-request to the API again
                    DataStore.deleteCacheFile(strUserCountry, strThreadCacheFileName);
                    apiRequest.submit();
                }
    	        }
    	    }).start();
    		
    		return true; //data in cache being processed ...
            
		}
		
	}
	
	
    
	public boolean getJsonObjectDataFromRawResource(final ObjApiRequest apiRequest){
		
		int intResorceJsonFileID = 0; 
		
		//get device language
		String strLanguage = Locale.getDefault().getLanguage();
		
		//Non-out-of-the-box Locales ... 
		Locale spanishLocale = new Locale("es", "ES");
		Locale swedishLocale = new Locale("sv", "SE");
		Locale dutchLocale = new Locale("nl", "NL");
		
		//a raw file containing a demo configuration for the app
		if(LOCAL_NAVIGATION_FILE.equals(apiRequest.getURLEndpoint())){
			
			//country France selected (and locale not french - this would lead to a mix of english and french menu items) - use french translation file
			//We need to use the French file as a basis for the FR shop as some of the api URL entries (e.g. "apiurl": "cyclisme") are specific for the french market and won't work with the standard English menu
			
			//so ... if this is a non-French device, but the user selected Country France --> show french tree (in French)
			
			/**
			 * 
			 * An English device could select UK, JP, CN, DE shop .. so we need english navigation tree files for JP, CN, DE to display English text
			 * but use the JP, CN, DE URLs. Plus beyond level 2 the api would return local language sub-menus which is an odd user experience. So: 
			 * We assume that if a user visits the shop of the target country, he is proficient with the language there. 
			**/
			if(!Locale.FRENCH.getLanguage().equals(strLanguage) && apiRequest.getContext().getString(R.string.str_locale_country_iso_code_fr).equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY))){
				intResorceJsonFileID = R.raw.top_level_navigation_country_france_non_french_locale;
			}else if(!spanishLocale.getLanguage().equals(strLanguage) && apiRequest.getContext().getString(R.string.str_locale_country_iso_code_es).equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY))){
					intResorceJsonFileID = R.raw.top_level_navigation_country_spain_non_spanish_locale;
			}else if(!Locale.ITALIAN.getLanguage().equals(strLanguage) && apiRequest.getContext().getString(R.string.str_locale_country_iso_code_it).equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY))){
				intResorceJsonFileID = R.raw.top_level_navigation_country_italy_non_italian_locale;
			}else if(!Locale.CHINESE.getLanguage().equals(strLanguage) && apiRequest.getContext().getString(R.string.str_locale_country_iso_code_cn).equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY))){
				intResorceJsonFileID = R.raw.top_level_navigation_country_china_non_chinese_locale;
			}else if(!Locale.JAPANESE.getLanguage().equals(strLanguage) && apiRequest.getContext().getString(R.string.str_locale_country_iso_code_jp).equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY))){
				intResorceJsonFileID = R.raw.top_level_navigation_country_japan_non_japanese_locale;
			}else if(!Locale.ENGLISH.getLanguage().equals(strLanguage) && apiRequest.getContext().getString(R.string.str_locale_country_iso_code_uk).equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY))){
				intResorceJsonFileID = R.raw.top_level_navigation_english_non_english_locale;
			}else if(!Locale.ENGLISH.getLanguage().equals(strLanguage) && apiRequest.getContext().getString(R.string.str_locale_country_iso_code_us).equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY))){
				intResorceJsonFileID = R.raw.top_level_navigation_english_non_english_locale;
			}else if(!Locale.ENGLISH.getLanguage().equals(strLanguage) && apiRequest.getContext().getString(R.string.str_locale_country_iso_code_au).equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY))){
				intResorceJsonFileID = R.raw.top_level_navigation_english_non_english_locale;
			}else if(!Locale.ENGLISH.getLanguage().equals(strLanguage) && apiRequest.getContext().getString(R.string.str_locale_country_iso_code_nz).equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY))){
				intResorceJsonFileID = R.raw.top_level_navigation_english_non_english_locale;
			}else if(!Locale.GERMAN.getLanguage().equals(strLanguage) && apiRequest.getContext().getString(R.string.str_locale_country_iso_code_de).equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY))){
				intResorceJsonFileID = R.raw.top_level_navigation_country_germany_non_german_locale;
			}else if(!swedishLocale.getLanguage().equals(strLanguage) && apiRequest.getContext().getString(R.string.str_locale_country_iso_code_se).equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY))){
				intResorceJsonFileID = R.raw.top_level_navigation_country_sweden_non_swedish_locale;
			}else if(!dutchLocale.getLanguage().equals(strLanguage) && apiRequest.getContext().getString(R.string.str_locale_country_iso_code_nl).equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY))){
				intResorceJsonFileID = R.raw.top_level_navigation_country_dutch_non_dutch_locale;
			
			}else{
				
				//.. otherwise show the tree based on the language locale
				intResorceJsonFileID = R.raw.top_level_navigation;
			}

			
		}
		
		String strJsonData = "";
		
		try{
            
			InputStream inputStream = apiRequest.getContext().getResources().openRawResource(intResorceJsonFileID);
            
	        if ( inputStream != null ) {
	        	
	        	
	        	String strCharacterset = ENCODING_LATIN_I; //default for European languages
	        	String strUserCountry = ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY);
	        	
	        	//user selected China or Japan or device langue is Chinese / Japanese? Read raw file as UTF-8 encoded
	        	if(apiRequest.getContext().getString(R.string.str_locale_country_iso_code_cn).equals(strUserCountry)
	        			|| apiRequest.getContext().getString(R.string.str_locale_country_iso_code_jp).equals(strUserCountry)
	        			){
	        		strCharacterset = ENCODING_UTF_8;
	        	}
	        	
	        	InputStreamReader inputStreamReader = new InputStreamReader(inputStream, strCharacterset);
	            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	            String buffer = "";
	            StringBuilder stringBuilder = new StringBuilder();
                
	            while ( (buffer = bufferedReader.readLine()) != null ) {
	                stringBuilder.append(buffer);
	            }
                
	            inputStream.close();
	            strJsonData = stringBuilder.toString();
	        }		
	        
		}catch (IOException e) {
	    	Logger.printMessage(logTag, "Can not read raw resource for " + intResorceJsonFileID + " due to " + e.toString(), Logger.WARN);
	    	return false;
	    }
		
		final String strFinalJsonData = strJsonData;
		
		if(strFinalJsonData==null || "".equals(strFinalJsonData)){
			return false; //data in cache not existing or out of date - a web request needs to be submitted
		}else{ 
			//parse cached data similar to what we receive from a web request
			//parse response in Background thread as this will block main thread otherwise
    		new Thread(new Runnable() {
    	        public void run() {
    	        	try {
                        
    					Logger.printMessage(logTag, apiRequest.getURLEndpoint() + "Build JSON from String - start", Logger.INFO);
    					JSONObject jsonObject = new JSONObject(strFinalJsonData);
    					Logger.printMessage(logTag, apiRequest.getURLEndpoint() + "Build JSON from String - end", Logger.INFO);
    					
    					Logger.printMessage(logTag, apiRequest.getURLEndpoint() + "Parse JSON - start", Logger.INFO);
    					handleJsonObjectResponse(apiRequest, jsonObject, true);
    					Logger.printMessage(logTag, apiRequest.getURLEndpoint() + "Parse JSON - end", Logger.INFO);
    					
                    } catch (JSONException e) {
                        Logger.printMessage(logTag, "Error getting Data from raw resource: " + e.toString(), Logger.ERROR);
                    }
    	        }
    	    }).start();

    		return true; 
            
		}		
		
	}	
	
	public boolean isConnectedToNetwork(){
		
		ConnectivityManager connManager = (ConnectivityManager)ApplicationContextProvider.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		
		NetworkInfo info = connManager.getActiveNetworkInfo();
		
		if(info==null){
			return false;
		}else{
			return info.isConnected();
		}
        
		
	}
	
	public boolean cancelAllVolleyRequests(){
		
		
		requestQueue.cancelAll(new RequestQueue.RequestFilter() {
		    @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });	
		
		return false;
		
	}
	
	/**
	 * check if the server response in gzipped
	 * @param response
	 * @return
	 */
	private boolean isGzipped(NetworkResponse response) {
        Map<String, String> headers = response.headers;
        return headers != null && !headers.isEmpty() && headers.containsKey(HEADER_ENCODING) &&
        headers.get(HEADER_ENCODING).contains(ENCODING_GZIP);
    }
    
	/**
	 * decode gzipped response into byte[]
	 * @param compressed
	 * @return
	 * @throws IOException
	 */
    private byte[] decompressResponse(byte [] compressed) throws IOException {
        ByteArrayOutputStream baos = null;
        try {
            int size;
            ByteArrayInputStream memstream = new ByteArrayInputStream(compressed);
            GZIPInputStream gzip = new GZIPInputStream(memstream);
            final int buffSize = 8192;
            byte[] tempBuffer = new byte[buffSize];
            baos = new ByteArrayOutputStream();
            while ((size = gzip.read(tempBuffer, 0, buffSize)) != -1) {
                baos.write(tempBuffer, 0, size);
            }
            return baos.toByteArray();
        } finally {
            if (baos != null) {
                baos.close();
            }
        }
        
        
    }
    
    

	
	private JSONObject getJSONBodyToPost(ObjApiRequest apiRequest, ConcurrentHashMap <String,String> requestParams) {
		
		//http://stackoverflow.com/questions/7193599/how-can-i-turn-a-jsonarray-into-a-jsonobject
		//http://stackoverflow.com/questions/23220695/google-volley-how-to-send-a-post-request-with-json-data
		
		/**
		JSONObject jsonBody = new JSONObject();
		
		if(ENDPOINT_MATCH_PHONE_CONTACTS.equals(strURLEndpoint)){
			
			JSONArray phoneNumberArray = new JSONArray();
			JSONObject onePhoneContact = new JSONObject();
			JSONArray contactsToMatch = new JSONArray();
			
			try {
				
				phoneNumberArray.put("1234");
				phoneNumberArray.put("2345");
				
				onePhoneContact.put("numbers", phoneNumberArray);
				onePhoneContact.put("unique_id", "stephan");
				
				contactsToMatch.put(onePhoneContact);
				
				jsonBody.put("contacts", contactsToMatch);
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		Logger.printMessage("JsonTest", jsonBody.toString(), Logger.INFO);
		return jsonBody;
		**/
		
		//Conversion from jsonReqeuestBody to JSONObject may fail if there are e.g. spaces within a string
		//So first step: loop through all params and embed them in escaped quotes.
		for(String key : requestParams.keySet()){
			
			String value = requestParams.get(key);
			requestParams.put(key, "\"" + value + "\"");
			
		}
		
		//For Sunbed: the json Body to post simply are the request parameters printed as JSON object
		JSONObject jsonRequestBody = null;
		try {
			jsonRequestBody = new JSONObject(requestParams.toString());
		} catch (JSONException e1) {
			Logger.printMessage(logTag, "Error in getJSONBodyToPost() " + e1.getMessage(), Logger.ERROR);
		}  
				
		
		//Logger.printMessage("JsonTest", jsonRequestBody.toString(), Logger.INFO);
		return jsonRequestBody;
		
		
	}    
	
	
	

	private void forwardApiError(final ObjApiRequest apiRequest, final int intHTTPStatusCode, final JSONObject volleyError){
		
		//Reset progress bar on  buttons ... 
		if(AppRequestHandler.getAuthenticationEndpoint().equals(apiRequest.getURLEndpoint()) ||
				AppRequestHandler.ENDPOINT_CATEGORIES.equals(apiRequest.getURLEndpoint()) ||
				AppRequestHandler.ENDPOINT_UPGRADE_CHECK.equals(apiRequest.getURLEndpoint()) ||
				AppRequestHandler.ENDPOINT_MEGAMENU.equals(apiRequest.getURLEndpoint()) ||
				AppRequestHandler.ENDPOINT_VERIFY_MFA.equals(apiRequest.getURLEndpoint()) ||
				AppRequestHandler.ENDPOINT_SILENT_LOGIN.equals(apiRequest.getURLEndpoint())
			){
			
			if (apiRequest.getContext() instanceof StartupActivity) {

				StartupActivity.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								if (StartupActivity.instance() != null) {
									StartupActivity.instance().onApiRequestError(apiRequest, 0, null);
								}
							}
						}));

			}else if (apiRequest.getContext() instanceof WelcomeActivity) {

				WelcomeActivity.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								if(WelcomeActivity.instance()!=null){
									WelcomeActivity.instance().onApiRequestError(apiRequest, 0, null);
								}
							}
						}));

			}else if (apiRequest.getContext() instanceof HybridActivity) {
				
				HybridActivity.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								if(HybridActivity.instance()!=null){
									HybridActivity.instance().onApiRequestError(apiRequest, 0, null);
								}
							}
						}));
				
			}else if (apiRequest.getContext() instanceof TabbedHybridActivity) {

				TabbedHybridActivity.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								if(TabbedHybridActivity.instance()!=null){
									TabbedHybridActivity.instance().onApiRequestError(apiRequest, 0, null);
								}
							}
						}));


			}else if (apiRequest.getContext() instanceof V3HybridActivity) {

				V3HybridActivity.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								if(V3HybridActivity.instance()!=null){
									V3HybridActivity.instance().onApiRequestError(apiRequest, 0, null);
								}
							}
						}));


			}
			
		}else if(apiRequest.getContext() instanceof Alert){

			if(Alert.instance()!=null){

				Alert.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								if(Alert.instance()!=null){
									Alert.instance().showFeedbackEmailError();
								}
							}
						}));
			}

		}
		
		
	}
	
	private void handleBespokeApiErrorResponse(ObjApiRequest apiRequest){

    	//Silent login expected errors (handling separately as error message 'NotFound' is quite general ..
		if(AppRequestHandler.ENDPOINT_SILENT_LOGIN.equals(apiRequest.getURLEndpoint())){
			String strError = apiRequest.getResponseMessage();
			if(AppRequestHandler.ERROR_TEXT_REFRESH_TOKEN_INCORRECT.equals(strError)) {
				forwardApiError(apiRequest, 0, null);
				return;
			}
		}

    	//Login endpoint expected errors
		if(ERROR_TEXT_LOGIN_INCORRECT.equals(apiRequest.getResponseMessage())
			|| ERROR_TEXT_LOGIN_MFA_REQUIRED.equals(apiRequest.getResponseMessage())
				|| ERROR_TEXT_LOGIN_ACCOUNT_LOCKED_GENERAL.equals(apiRequest.getResponseMessage())
				|| ERROR_TEXT_LOGIN_ACCOUNT_LOCKED_MFA.equals(apiRequest.getResponseMessage())
				|| ERROR_TEXT_LOGIN_MULTIFACTOR_FAILED.equals(apiRequest.getResponseMessage())
			){
    		//AppUtils.showCustomToast(ApplicationContextProvider.getContext().getString(R.string.str_api_error_message_unauthorized_user_feedback), true);
    		forwardApiError(apiRequest, 0, null);
    		return;
    	}
		
		
	}
	

    private void handleNetworkResponse(ObjApiRequest apiRequest, NetworkResponse response){
        
        
        byte[] data = null;
        
   		if(isGzipped(response)){
   			
   			try {
   				data = decompressResponse(response.data);
                
   	 	   	} catch (IOException e1) {
                // TODO Auto-generated catch block
   	 	   		Logger.printMessage("gzip decode error", e1.toString(), Logger.ERROR);
                return;
   	 	   	}
   		}else{
   			data = response.data;
   		}
        
        try {
            handleByteStreamResponse(apiRequest, data, false);
        	Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - response processed", Logger.INFO);
        } catch (Exception e) {
            Logger.printMessage(logTag, apiRequest.getURLEndpoint() 
            		+ "\n" + apiRequest.getRequestURI()
            		+ "\n" + "Parse error: " 
            		+ e.getMessage(), Logger.ERROR);
            
            onResponseParsingEnd(apiRequest, false);
            
        }
		
    }
    

	private void handleByteStreamResponse(final ObjApiRequest apiRequest, byte[] responseByteStream, boolean isCachedData) throws XmlPullParserException, IOException {
		
		/*
		 *
		 *  Each response is checked for errors first. Parsing of a successful response will only be triggered if there are no errors
		 *
		 *  Please note that an error response is handled in "handleErrorResponse". And erroneous request does not reach the handleServerResponse method
		 */
        
		
		/*
		 *  ON SUCCESS FROM RESPONSE
		 */
		
		
		if("any bytestream endpoint".equals(apiRequest.getURLEndpoint())){
									
		}	
		
	}
	

	private void handleStringResponse(final ObjApiRequest apiRequest, String strResponse, boolean isCachedData) {
		
		/*
		 *
		 *  Each response is checked for errors first. Parsing of a successful response will only be triggered if there are no errors
		 *
		 *  Please note that an error response is handled in "handleErrorResponse". And erroneous request does not reach the handleServerResponse method
		 */
        
		
		/*
		 *  ON SUCCESS FROM RESPONSE
		 */
		
		
		if(ENDPOINT_NEWSFEED_DUMMY.equals(apiRequest.getURLEndpoint())){
			
			//Some RSS feeds are well formed - so we parse the byte stream directly.
			//Other RSS feeds start with trailing spaces - we detect this by catching the XMLPullParserException
			//"Unexpected token" - and convert the response to a string for separate handling (i.e. remove trailing spaces)
			//so it can be parsed ok.
			
			//For Gore Apparel Atom feed - convert String response to UTF-8. Volley returns response in whatever encoding
			//So for this feed this leads to characters like � instead of � .. please note that displaying of embedded image URLs
			//gets affected by this if the URL contains special characters - some images (very few) dont display if encoding is done for entire response
			String strConvertedResponse = strResponse;
			/*
			if(!isCachedData & "http://inspiration.goreapparel.com/feed/atom/".equals(apiRequest.getURLEndpoint())){
				try{
					//strConvertedResponse = new String(strResponse.getBytes("ISO-8859-1"), ENCODING_UTF_8);
					//strConvertedResponse = new String(strResponse.getBytes(), ENCODING_UTF_8);
					strConvertedResponse = new String(strResponse.getBytes(), "ISO-8859-1");
				}catch (UnsupportedEncodingException e) {
					Logger.printMessage(logTag, "handleStringResponse: Volley response decoding failed" + apiRequest.getRequestURI(), Logger.WARN);
				}
			}
			*/
			
			try{
				strConvertedResponse = new String(strResponse.getBytes(), getEndpointEncoding(apiRequest.getURLEndpoint()));
			}catch (UnsupportedEncodingException e) {
				Logger.printMessage(logTag, "handleStringResponse: Volley response decoding failed" + apiRequest.getRequestURI(), Logger.WARN);
			}
		
			
			try{
				Logger.printMessage(logTag, "Received String (XML) response for: " + apiRequest.getRequestURI(), Logger.INFO);
				AppXMLPullParser.instance().parseRSSTypeNewsfeed(apiRequest, strConvertedResponse, isCachedData, false);
				onResponseParsingEnd(apiRequest, true);
			
			}catch(XmlPullParserException xe){
				
				Logger.printMessage(logTag, "handleStringResponse: XmlPullParserException for endpoint\n" + apiRequest.getRequestURI() + "\n" + xe.getMessage(), Logger.ERROR);
				onResponseParsingEnd(apiRequest, false);
				
			}catch(IOException ie){
				
				Logger.printMessage(logTag, "handleStringResponse: IOException" + ie.getMessage(), Logger.ERROR);
				onResponseParsingEnd(apiRequest, false);
				
			}
				
						
		}else if(ENDPOINT_MAILGUN.equals(apiRequest.getURLEndpoint())){

			if(apiRequest.getContext() instanceof  Alert){

				if(Alert.instance()!=null){
					Alert.instance().showFeedbackEmailSuccess();
				}

			}

		}
		
	}
	
	
	private void onResponseParsingEnd(final ObjApiRequest apiRequest, final boolean blnResponseProcessedSuccessfully){
		
		if(ENDPOINT_NEWSFEED_DUMMY.equals(apiRequest.getURLEndpoint())){
		
			//no luck with parsing attempt - remove request from request list
			NewsfeedActivity.instance().runOnUiThread(
					new Thread(new Runnable() {
						public void run() {
							if(NewsfeedActivity.instance()!=null){
								
								if(blnResponseProcessedSuccessfully){
									NewsfeedActivity.instance().onApiRequestSuccess(apiRequest, null);
								}else{
									NewsfeedActivity.instance().onApiRequestError(apiRequest, -1, null);
								}
								
							}
						}
					}));
		
		}

	}




	private boolean getStringDataFromCache(final ObjApiRequest apiRequest){
		
		//do not do anything if this is not a defined cache file
		if(!cacheFiles.contains(apiRequest.getURLEndpoint())){
			return false;
		}
		
		final String logTag = "Cache performance";
		
		String strCacheFileName = getCacheFileName(apiRequest);
		
		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " reading file start", Logger.INFO);
		final String strUserCountry = ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY);
		final String strCachedData = DataStore.readFileAsString(strUserCountry, strCacheFileName, getEndpointEncoding(apiRequest.getURLEndpoint()));
		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " reading file end", Logger.INFO);
		
		if(strCachedData==null || strCachedData.equals("")){
			return false; //data in cache not existing or out of date - a web request needs to be submitted
		}else{
			
			//parse cached data similar to what we receive from a web request
			//parse response in Background thread as this will block mainthread otherwise
    		new Thread(new Runnable() {
    	        public void run() {
	        	

					Logger.printMessage(logTag, apiRequest.getURLEndpoint() + "Parse String - start", Logger.INFO);
    	        	handleStringResponse(apiRequest, strCachedData, true);
    	        	//AppUtils.showCustomToast("Read from cache: " + apiRequest.getRequestURI(), true);
					Logger.printMessage(logTag, apiRequest.getURLEndpoint() + "Parse String - end", Logger.INFO);
					
    	        }
    	    }).start();
    		
    		return true; //data in cache being processed ...
            
		}
		
	}
	
	private String getCacheFileName(ObjApiRequest apiRequest){
		
		String strCacheFileName = apiRequest.getURLEndpoint();
		
		//special URI based file name for RSS feeds
		if(ENDPOINT_NEWSFEED_DUMMY.equals(apiRequest.getURLEndpoint())){
			strCacheFileName = apiRequest.getRequestURI(); //get cache file for individual category URI
		}
		
		return strCacheFileName;
		
	}
	
	public String getEndpointEncoding(String strURLEndpoint){
		
		/**
		 * 
		 * We need to work around a few issues around response encoding. We have found that most Newsfeeds return the character set used 
		 * correctly in the content header as follows:
		 * 
		 * Content-Type: application/rss+xml; charset=utf-8
		 * 
		 * However a few feeds dont follow that exact pattern:
		 * 
		 * http://www.wigglehighfive.com/feed/
		 * http://feeds2.feedburner.com/blogs/MaBf
		 * http://inspiration.goreapparel.com/feed/atom/
		 * deliver 
		 * Content-Type: ... charset=UTF-8 (Charset in upper case - this should be detected fine though?)
		 * 
		 * http://www.garmin.blogs.com/ukpr/fitness.xml
		 * http://www.garmin.blogs.com/ukpr/outdoor.xml
		 * Content-Type: text/xml (no charset specified)
		 * 
		 * We don't seem to have a problem with 
		 * http://feeds2.feedburner.com/blogs/MaBf though. The other endpoints showed spurious characters - the response from 
		 * the web usually is parsed ok, but cashed responses caused data to show incorrectly. 
		 * 
		 * We are not using exact science here to fix this, but as a workaround we force encode some endpoints to Latin I as 
		 * non UTF-8 characters were used in them. This fixed the issue in combination with storing cached data unchanged in terms 
		 * of encoding. If this issue persists, please look into overwriting the Volley String request and improve the detection / setting
		 * of the encoding there. An example can be found in .. 
		 * http://stackoverflow.com/questions/19267616/why-does-volleys-response-string-use-an-encoding-different-from-that-in-the-res
		 *  
		 **/
		
		if("http://inspiration.goreapparel.com/feed/atom/".equals(strURLEndpoint)
				||
				"http://www.wigglehighfive.com/feed/".equals(strURLEndpoint)
				||
				"http://www.garmin.blogs.com/ukpr/fitness.xml".equals(strURLEndpoint)
				|| 
				"http://www.garmin.blogs.com/ukpr/outdoor.xml".equals(strURLEndpoint)
				||
				ENDPOINT_CATEGORIES.equals(strURLEndpoint)
				){
			return ENCODING_LATIN_I;
		}else{
			return ENCODING_UTF_8; //default to UTF-8
		}
		
	}
	
	public void retrieveOrderData(Context context, ON_RESPONSE_ACTION responseAction){
		
		//Create a dummy request. This is used to query our cache for order tracking data
		ObjApiRequest orderRequest = new ObjApiRequest(context, 
				null, 
				AppRequestHandler.getOrderTrackingEndpoint(), 
				Method.GET, 
				null,
				null, 
				responseAction);
		
		//For test purposes - delete the cached data file
		//DataStore.deleteCacheFile(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY), ENDPOINT_ORDER_TRACKING);
		
		
		//We are retrieving the order tracking data from cache first. During parsing the data, a request to the order endpoint will be submitted
		boolean blnCachedDataFound = getDataFromLocalCache(orderRequest);
		
		//But: in case there is no cached data for orders (e.g. first time call to endpoint)? We manually submit a http request to the order endpoint
		if(!blnCachedDataFound){
			orderRequest.setOnResponseAction(ON_RESPONSE_ACTION.RETRIEVE_ORDERS_AFTER_AUTHENTICATION);
			orderRequest.submit();
		}
			

	}
	
	public static String getOrderTrackingEndpoint(){
		
		
		//for testing, use this endpoint:
		//Go to http://andromedia.biz/download/wiggle/order_tracking_test_response.json
		//and edit order status values etc at will
		//return ENDPOINT_ORDER_TRACKING_TEST;
		
		//for live app, use this endpoint
		return ENDPOINT_ORDER_TRACKING;
		
	}

	private SSLSocketFactory getPinnedSSLCertificates() throws Exception {

    	//if we connect to a test server - ignore any certificate pinning as othwerwise we will
		//need to keep adding changing test-server certificates to the below list
		if(!"".equals(AppUtils.REDIRECT_APP_TO_TEST_SERVER)){
			return null;
		}

    	//Read all the expected certificates from the raw directory and assign an alias to each
		Certificate certProd = readKnownCertificate(R.raw.wiggle);
		String strCertProdAlias = "ca_prod";

		Certificate certTest = readKnownCertificate(R.raw.wiggle_tst5);
        String strCertTst5Alias = "ca_tst5";

        //The typeahead response endpoint needs trusting too
		Certificate certAjax = readKnownCertificate(R.raw.wiggle_ajax);
		String strCertAjaxAlias = "ca_ajax";
		//.. add new certificates here (cert and alias)

		// Create a KeyStore containing our trusted CAs
		String keyStoreType = KeyStore.getDefaultType();
		KeyStore trusted = KeyStore.getInstance(keyStoreType);
		trusted.load(null, null);

		//Add all certificates to the trust store
		trusted.setCertificateEntry(strCertProdAlias, certProd); //trust this cert
		trusted.setCertificateEntry(strCertTst5Alias, certTest); //trust this cert
		trusted.setCertificateEntry(strCertAjaxAlias, certAjax); //trust this cert
        //.. trust new certificates here:


		// Create a TrustManager that trusts the CAs in our KeyStore
		String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
		tmf.init(trusted);

		// Create an SSLContext that uses our TrustManager
		SSLContext context = SSLContext.getInstance("TLS");
		context.init(null, tmf.getTrustManagers(), null);

		SSLSocketFactory sf = context.getSocketFactory();
		return sf;

	}

	private Certificate readKnownCertificate(int intRawResourceId) throws Exception {

		CertificateFactory cf = CertificateFactory.getInstance("X.509");

		//Trust & Pin the Wiggle test site certificate (tst5.wiggle.co.uk)
		// Generate the certificate using the certificate file under res/raw/cert.cer
		InputStream caInput = new BufferedInputStream(ApplicationContextProvider.getContext().getResources().openRawResource(intRawResourceId));
		Certificate ca = cf.generateCertificate(caInput);
		caInput.close();

		return ca;

	}
	
}