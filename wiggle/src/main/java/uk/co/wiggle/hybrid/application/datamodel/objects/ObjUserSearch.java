package uk.co.wiggle.hybrid.application.datamodel.objects;

import android.os.Bundle;


import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.application.utils.DateUtils;


/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class ObjUserSearch extends CustomObject {

	public static final String OBJECT_NAME = "ObjUserSearch";


	//used for user searches triggers from e.g. FragmentShopNavigation
	//and displayed in "recent searches" widgets
	public static final String FIELD_SEARCH_STRING= "search_string"; //search Term
	public static final String FIELD_SELECTED_DATE = "selected_date";//last time this search term was used


	private static List<String> sFieldList;
	static {

		sFieldList = Collections.synchronizedList(new ArrayList<String>());

		sFieldList.add(FIELD_SEARCH_STRING);
		sFieldList.add(FIELD_SELECTED_DATE);


	}

	private static ConcurrentHashMap<String, Integer> sFieldTypes;
	static {
		sFieldTypes = new ConcurrentHashMap<String, Integer>();

		sFieldTypes.put(FIELD_SEARCH_STRING, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_SELECTED_DATE, FIELD_TYPE_STRING);

	}

	public ObjUserSearch() {
	}

	public ObjUserSearch(String strSearchTerm){
		setField(FIELD_SEARCH_STRING, strSearchTerm);
		setField(FIELD_SELECTED_DATE, DateUtils.dateToInternalString(new Date()));
	}

	public ObjUserSearch(Bundle bundle) {
		super(bundle);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ObjUserSearch)) {
			return false;
		}

		//this comparison only works for recent search locations - as only those will have a Google Place Id
		//it won't work for Wiggle Contentful location information.
		ObjUserSearch other = (ObjUserSearch) o;
		String strThisSearch = getString(FIELD_SEARCH_STRING);
		String strOtherSearch = other.getString(FIELD_SEARCH_STRING);
		return strThisSearch.equals(strOtherSearch);
	}

	@Override
	public String getObjectName() {
		return OBJECT_NAME;
	}

	@Override
	public List<String> getFields() {
		return sFieldList;
	}

	@Override
	public Class<?> getSubclass() {
		return ObjUserSearch.class;
	}

	@Override
	public Integer getFieldType(String fieldName) {
		return sFieldTypes.get(fieldName);
	}

	public Date getLastSelectedDate(){

		Date createdDate = DateUtils.getDateFromInternalString(getString(FIELD_SELECTED_DATE));
		return createdDate;

	}

	public ObjTypeaheadResponse convertToTypeaheadResponse(){

		ObjTypeaheadResponse typeaheadResponse = new ObjTypeaheadResponse();
		typeaheadResponse.setField(ObjTypeaheadResponse.FIELD_NAME, getString(FIELD_SEARCH_STRING));
		typeaheadResponse.setField(ObjTypeaheadResponse.FIELD_DISPLAY_TYPE, ObjTypeaheadResponse.CONST_DISPLAY_TYPE_CHILD);
		typeaheadResponse.setField(ObjTypeaheadResponse.FIELD_CATEGORY_TYPE, ObjTypeaheadResponse.CONST_RESULT_TYPE_SAVED_SEARCH);

		return typeaheadResponse;

	}


}