package uk.co.wiggle.hybrid.usecase.newsfeed.adapters;

import java.util.ArrayList;
import java.util.List;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.usecase.newsfeed.activities.NewsfeedActivity;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNewsfeedCategory;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.extensions.PicassoSSL;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;


import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class NewsfeedCategoryAdapter extends BaseAdapter{
	
	private String logTag = "NewsfeedCategoryAdapter";

	private List<ObjNewsfeedCategory> mCategories = new ArrayList<ObjNewsfeedCategory>();
	private NewsfeedActivity mContext;
	private int mSelectedGridItem = 0;
	private int NUMBER_OF_ITEMS_IN_ROW = 3;

	public NewsfeedCategoryAdapter(NewsfeedActivity context) {
		mContext = context;
	}

	public void setListContent(List<ObjNewsfeedCategory> listItems) {
		mCategories = listItems;
	}

	@Override
	public int getCount() {
		//return mItemList.size();
		//the size of the list view does no longer equal to the number of items in the list, but 
		//to the number of rows displayed. Each row displays NUMBER_OF_ITEMS_IN_ROW markers --> (items / NUMBER_OF_ITEMS_IN_ROW items displayed in each row)
		//this is used to e.g. calculate the scrollbar etc
		if ( (mCategories.size() % NUMBER_OF_ITEMS_IN_ROW) == 0){
			return (int) (mCategories.size() / NUMBER_OF_ITEMS_IN_ROW);
		}else{
			return (int) 1 + (mCategories.size() / NUMBER_OF_ITEMS_IN_ROW);
		}
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}
	

	@Override
	public View getView(int arg0, View view, ViewGroup arg2) 
	{
		
		final int position = arg0;
		
		//get NUMBER_OF_ITEMS_IN_ROW items per list row
		ObjNewsfeedCategory item1 = null;
		ObjNewsfeedCategory item2 = null;
		ObjNewsfeedCategory item3 = null;
		
		if(getItemCount()>arg0*NUMBER_OF_ITEMS_IN_ROW){
			item1 = (ObjNewsfeedCategory) getItem(arg0 * NUMBER_OF_ITEMS_IN_ROW);
			Logger.printMessage(logTag, "CATEGORY: " + item1.toXML(), Logger.INFO);
		}	
		
		if(getItemCount()>arg0*NUMBER_OF_ITEMS_IN_ROW+1){
			item2 = (ObjNewsfeedCategory) getItem(arg0 * NUMBER_OF_ITEMS_IN_ROW+1);
			Logger.printMessage(logTag, "CATEGORY: " + item2.toXML(), Logger.INFO);
		}		
		
		if(getItemCount()>arg0*NUMBER_OF_ITEMS_IN_ROW+2){
			item3= (ObjNewsfeedCategory) getItem(arg0 * NUMBER_OF_ITEMS_IN_ROW+2);
			Logger.printMessage(logTag, "CATEGORY: " + item3.toXML(), Logger.INFO);
		}		
		
		
		//Inflate row layout
		LayoutInflater inflater = LayoutInflater.from(mContext);
		view = inflater.inflate(R.layout.newsfeed_settings_category_row, null);
		
		//find views
		LinearLayout categoryRow_lytTopicOne = (LinearLayout) view.findViewById(R.id.categoryRow_lytTopicOne);
		ImageView categoryRow_ivBackgroundTopicOne = (ImageView) view.findViewById(R.id.categoryRow_ivBackgroundTopicOne);
		RelativeLayout categoryRow_lytForegroundTopicOne = (RelativeLayout) view.findViewById(R.id.categoryRow_lytForegroundTopicOne);
		ImageView categoryRow_ivTopicOneStar = (ImageView) view.findViewById(R.id.categoryRow_ivTopicOneStar);
		TextView categoryRow_tvTopicOneName = (TextView) view.findViewById(R.id.categoryRow_tvTopicOneName);
		categoryRow_tvTopicOneName.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		
		LinearLayout categoryRow_lytTopicTwo = (LinearLayout) view.findViewById(R.id.categoryRow_lytTopicTwo);
		ImageView categoryRow_ivBackgroundTopicTwo = (ImageView) view.findViewById(R.id.categoryRow_ivBackgroundTopicTwo);
		RelativeLayout categoryRow_lytForegroundTopicTwo = (RelativeLayout) view.findViewById(R.id.categoryRow_lytForegroundTopicTwo);
		ImageView categoryRow_ivTopicTwoStar = (ImageView) view.findViewById(R.id.categoryRow_ivTopicTwoStar);
		TextView categoryRow_tvTopicTwoName = (TextView) view.findViewById(R.id.categoryRow_tvTopicTwoName);
		categoryRow_tvTopicTwoName.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		
		LinearLayout categoryRow_lytTopicThree = (LinearLayout) view.findViewById(R.id.categoryRow_lytTopicThree);
		ImageView categoryRow_ivBackgroundTopicThree = (ImageView) view.findViewById(R.id.categoryRow_ivBackgroundTopicThree);
		RelativeLayout categoryRow_lytForegroundTopicThree = (RelativeLayout) view.findViewById(R.id.categoryRow_lytForegroundTopicThree);
		ImageView categoryRow_ivTopicThreeStar = (ImageView) view.findViewById(R.id.categoryRow_ivTopicThreeStar);
		TextView categoryRow_tvTopicThreeName = (TextView) view.findViewById(R.id.categoryRow_tvTopicThreeName);
		categoryRow_tvTopicThreeName.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		
		

		//handle grid items individually
		if(item1!=null){
			
			//this background image should really be on the server - but for some reason we have this locally
			String strBackgroundImage = item1.getString(ObjNewsfeedCategory.FIELD_IMAGE_URL); //image file name e.g. cycle_road.png
			int intResourceId = AppUtils.getDrawableByName(strBackgroundImage);
			PicassoSSL.with(mContext).load(intResourceId).into(categoryRow_ivBackgroundTopicOne);
			categoryRow_tvTopicOneName.setText(item1.getString(ObjNewsfeedCategory.FIELD_TITLE));
			categoryRow_lytTopicOne.setVisibility(View.VISIBLE);
			indicateNewsTopicSubscriptionStatus(item1, categoryRow_lytForegroundTopicOne, categoryRow_ivTopicOneStar);
			setSubscriptionClickListener(item1, categoryRow_lytForegroundTopicOne, categoryRow_ivTopicOneStar);
			
		}else{
			categoryRow_lytTopicOne.setVisibility(View.INVISIBLE);
		}
		
		if(item2!=null){
			
			String strBackgroundImage = item2.getString(ObjNewsfeedCategory.FIELD_IMAGE_URL); //image file name e.g. cycle_road.png
			int intResourceId = AppUtils.getDrawableByName(strBackgroundImage);
			PicassoSSL.with(mContext).load(intResourceId).into(categoryRow_ivBackgroundTopicTwo);
			categoryRow_tvTopicTwoName.setText(item2.getString(ObjNewsfeedCategory.FIELD_TITLE));
			categoryRow_lytTopicTwo.setVisibility(View.VISIBLE);
			indicateNewsTopicSubscriptionStatus(item2, categoryRow_lytForegroundTopicTwo, categoryRow_ivTopicTwoStar);
			setSubscriptionClickListener(item2, categoryRow_lytForegroundTopicTwo, categoryRow_ivTopicTwoStar);
			
			
		}else{
			categoryRow_lytTopicTwo.setVisibility(View.INVISIBLE);
		}
		
		
		if(item3!=null){
			
			String strBackgroundImage = item3.getString(ObjNewsfeedCategory.FIELD_IMAGE_URL); //image file name e.g. cycle_road.png
			int intResourceId = AppUtils.getDrawableByName(strBackgroundImage);
			PicassoSSL.with(mContext).load(intResourceId).into(categoryRow_ivBackgroundTopicThree);
			categoryRow_tvTopicThreeName.setText(item3.getString(ObjNewsfeedCategory.FIELD_TITLE));
			categoryRow_lytTopicThree.setVisibility(View.VISIBLE);
			indicateNewsTopicSubscriptionStatus(item3, categoryRow_lytForegroundTopicThree, categoryRow_ivTopicThreeStar);
			setSubscriptionClickListener(item3, categoryRow_lytForegroundTopicThree, categoryRow_ivTopicThreeStar);
			
			
		}else{
			categoryRow_lytTopicThree.setVisibility(View.INVISIBLE);
		}
		
		
		return view;
	}
	
	
	private void setSubscriptionClickListener(final ObjNewsfeedCategory category, final View foregroundView, final ImageView starImageView){
		
		foregroundView.setOnClickListener(null);
		foregroundView.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View view) {
						
						NewsfeedActivity.instance().setNewsTopicSubscribedOnOff(category, foregroundView, starImageView);
						
					}

				});     	
				
	}
	
	

	//Convenience methods
	public void indicateNewsTopicSubscriptionStatus(ObjNewsfeedCategory category, View foregroundView, ImageView starImageView){
		
		//Check if user subscribed to this topic
		if(ObjAppUser.instance().isSubscribedToNewsTopic(category)){
			
			starImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.sd_news_topic_activated));
			AppUtils.setSDKDependentBackground(foregroundView, R.drawable.sd_button_clear_to_opaque);	
			//foregroundView.setSelected(false);
			//starImageView.setSelected(false);
		}else{
			
			starImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.sd_news_topic_deactivated));
			AppUtils.setSDKDependentBackground(foregroundView, R.drawable.sd_button_opaque_to_clear);	
			//foregroundView.setSelected(true);
			//starImageView.setSelected(true);
		}
		
	}

	private int getItemCount(){
		return mCategories.size();
	}
	
	
	@Override
	public Object getItem(int arg0) {
		return mCategories.get(arg0);
	}
	

	


}
