package uk.co.wiggle.hybrid.application.datamodel.objects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;


import android.os.Bundle;


/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class ObjTypeaheadResponse extends CustomObject {

	public static final String OBJECT_NAME = "typeahead_response";
	
	private ObjTypeaheadResponse mParentRecord;

	public static final String FIELD_LOGO = "logo"; 
	public static final String FIELD_NAME = "Name";
	public static final String FIELD_PDID = "pdid";
	public static final String FIELD_SLUG = "Slug";
	public static final String FIELD_DISPLAY_TYPE = "display_type"; //not returened by API but set internally
	public static final String FIELD_CATEGORY_TYPE = "Type"; //for parent categories: unique 'type' of this category (Brand, Product, Help, Featured)
	
	public static final String CONST_DISPLAY_TYPE_PARENT = "parent";
	public static final String CONST_DISPLAY_TYPE_CHILD = "child";
	public static final String CONST_DISPLAY_TYPE_BUTTON = "button";
	
	public static final String CONST_RESULT_TYPE_BRAND = "Brand"; //For brand results
	public static final String CONST_RESULT_TYPE_PRODUCT = "Product"; //For product results
	public static final String CONST_RESULT_TYPE_HELP = "Help"; //For Customer Service
	public static final String CONST_RESULT_TYPE_CONTENT = "Content"; //For featured products
	public static final String CONST_RESULT_TYPE_CATEGORY = "Category"; //For categories
	public static final String CONST_RESULT_TYPE_SAVED_SEARCH = "Ajax"; //Not returned by API, but created locally in app. Used for "saved search" feature.

	
	
	private static List<String> sFieldList;
	static {

		sFieldList = Collections.synchronizedList(new ArrayList<String>());
		
		//fields used for categories taken from top_level_navigation.json
		sFieldList.add(FIELD_LOGO);
		sFieldList.add(FIELD_NAME);
		sFieldList.add(FIELD_PDID);
		sFieldList.add(FIELD_SLUG);
		sFieldList.add(FIELD_DISPLAY_TYPE);
		sFieldList.add(FIELD_CATEGORY_TYPE);
				
	}

	private static ConcurrentHashMap<String, Integer> sFieldTypes;
	static {
		sFieldTypes = new ConcurrentHashMap<String, Integer>();
		
		sFieldTypes.put(FIELD_LOGO, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_NAME, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PDID, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_SLUG, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_DISPLAY_TYPE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_CATEGORY_TYPE, FIELD_TYPE_STRING);
				
	}

	public ObjTypeaheadResponse() {
	}

	public ObjTypeaheadResponse(Bundle bundle) {
		super(bundle);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ObjTypeaheadResponse)) {
			return false;
		}

		ObjTypeaheadResponse other = (ObjTypeaheadResponse) o;
		String otherURI = other.getString(FIELD_NAME) + other.getString(FIELD_CATEGORY_TYPE) + other.getString(FIELD_SLUG);
		String thisURI = this.getString(FIELD_NAME) + this.getString(FIELD_CATEGORY_TYPE) + this.getString(FIELD_SLUG);
		return otherURI.equals(thisURI);
	}

	@Override
	public String getObjectName() {
		return OBJECT_NAME;
	}

	@Override
	public List<String> getFields() {
		return sFieldList;
	}

	@Override
	public Class<?> getSubclass() {
		return ObjTypeaheadResponse.class;
	}

	@Override
	public Integer getFieldType(String fieldName) {
		return sFieldTypes.get(fieldName);
	}

	//handler for children of this object
	public void setParentRecord(ObjTypeaheadResponse parent){
		this.mParentRecord = parent;
	}
	
	public ObjTypeaheadResponse getParentRecord(){
		return mParentRecord;
	}

	//Convenience methods
	public boolean isRecordOfType(String strCategoryType){

		if(mParentRecord==null){
			return strCategoryType.equals(getString(FIELD_CATEGORY_TYPE)); //in case this is a parent record
		}else{
			return strCategoryType.equals(mParentRecord.getString(FIELD_CATEGORY_TYPE)); //in case this is a child record
		}

	}

}