package uk.co.wiggle.hybrid.usecase.shop.classicnav.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.usecase.shop.classicnav.activities.HybridActivity;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNavigationCategory;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.usecase.shop.classicnav.fragments.NavigationDrawerFragment;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.tagging.AppTagManager;




import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class NavigationHeaderAdapter extends BaseAdapter{
	
	private String logTag = "NavigationHeaderAdapter";

	private List<ObjNavigationCategory> mCategories = new ArrayList<ObjNavigationCategory>();
	public List<ObjNavigationCategory> mCompleteBreadcrumb = new ArrayList<ObjNavigationCategory>();
	private NavigationDrawerFragment mContext;

	public NavigationHeaderAdapter(NavigationDrawerFragment context) {
		mContext = context;
	}

	public void setListContent(List<ObjNavigationCategory> listItems, List<ObjNavigationCategory> completeBreadcrumb) {
		mCategories = listItems;
		mCompleteBreadcrumb = completeBreadcrumb;
	}

	@Override
	public int getCount() {
		return mCategories.size();
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}
	

	@Override
	public View getView(int arg0, View view, ViewGroup arg2) 
	{
		final int position = arg0;
		final int POSITION_OF_BACK_TO_ITEM = 0;
		
		final ObjNavigationCategory category = (ObjNavigationCategory) mCategories.get(arg0);
		final String strRecordURI= category.getString(ObjNavigationCategory.FIELD_URI);
		
		Logger.printMessage(logTag, "CATEGORY: " + category.toXML(), Logger.INFO);

		LayoutInflater inflater = LayoutInflater.from(mContext.getActivity());
		
		//first position is the "back" position
		if(position==POSITION_OF_BACK_TO_ITEM){
			view = inflater.inflate(R.layout.navigation_item_header_back_to, null);
		}else{
			//the current section the user is browsing
			view = inflater.inflate(R.layout.navigation_item_row, null);
		}
		
		final TextView tvCategoryName = (TextView) view.findViewById(R.id.navigationItem_tvCategoryName);
		String strCategoryName = category.getDisplayName(); 
		tvCategoryName.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		
		if(position==POSITION_OF_BACK_TO_ITEM){
			
			String strLanguage = Locale.getDefault().getLanguage();
			
			//For some languages: "Back" only as opposed to "Back to Category"
			if(Locale.JAPANESE.equals(strLanguage)
					|| Locale.CHINESE.equals(strLanguage)
					){
				//No string formatting for Japanese & Chinese
				tvCategoryName.setText(mContext.getString(R.string.str_navigate_back_to));
			}else{
				tvCategoryName.setText(String.format(mContext.getString(R.string.str_navigate_back_to), strCategoryName));
			}
		
		}else{
			
			tvCategoryName.setText(String.format(mContext.getString(R.string.str_all_of_this_category), strCategoryName));
			
			//if there are no more children - set text to regular to indicate this ... 
			if(category.getAllChildren()==null || category.getAllChildren().size()==0){
				tvCategoryName.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
			}
			
			//add indent depending on breadcrumb level
			View vLevel3Indent = view.findViewById(R.id.navigationItem_vLevel3Indent);
			View vLevel4Indent = view.findViewById(R.id.navigationItem_vLevel4Indent);
			if(mCompleteBreadcrumb.size()>3){
				vLevel3Indent.setVisibility(View.VISIBLE);
			}else{
				vLevel3Indent.setVisibility(View.GONE);
			}
			
			if(mCompleteBreadcrumb.size()>4){
				vLevel4Indent.setVisibility(View.VISIBLE);
			}else{
				vLevel4Indent.setVisibility(View.GONE);
			}		
			
			//hide more-info item for (All) category
			ImageView navigationItem_ivMore = (ImageView) view.findViewById(R.id.navigationItem_ivMore);
			navigationItem_ivMore.setVisibility(View.GONE);
			
			
			/**
			int intImageResource = category.getCategoryImage();
			
			ImageView ivLevel1Image = (ImageView) view.findViewById(R.id.navigationItem_ivLevel1Image);
			if(intImageResource>0){
				ivLevel1Image.setImageResource(intImageResource);
				ivLevel1Image.setVisibility(View.VISIBLE);
			}else{
				ivLevel1Image.setVisibility(View.INVISIBLE);
			}
			
			**/	
			
		}
		
		//short click - use this search and execute it
		view.setOnClickListener(null);
		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				
				//Navigate to URL straight away if "all of" item was clicked
				if(position!=POSITION_OF_BACK_TO_ITEM){
					
					//send GTM Tag
					Map<String, Object> gtmTagsMap = new HashMap<String, Object>();
					gtmTagsMap.put("shopDept", tvCategoryName.getText().toString());
					AppTagManager.instance().sendEventTags(mContext.getString(R.string.str_gtm_page_name_shop), "shopShop");
					//make sure GTM event is also sent to UTM Tracking
					category.setField(ObjNavigationCategory.FIELD_GOOGLE_TAG_MANAGER_ID, "shopShop");
					HybridActivity.instance().setCurrentNavigationCategory(category, true);
					
				}else {
					
					/** back navigation - don't send GTM tag - we only want to send a GTM tag if a user navigates to a category.
					 This is the case either when pressing the "All" header item or selecting a category that (confirmed) does not have any more children
					 
					//send GTM Tag
					Map<String, Object> gtmTagsMap = new HashMap<String, Object>();
					gtmTagsMap.put("shopDept", tvCategoryName.getText().toString());
					AppTagManager.instance().sendEventTags(mContext.getString(R.string.str_gtm_page_name_shop), "shopShop");
					
					**/
					
					//continue browsing menu... 
					HybridActivity.instance().setCurrentNavigationCategory(category, false);
				}
				
			}

		});     				
		
		
		return view;
	}
	

	
	@Override
	public Object getItem(int arg0) {
		return mCategories.get(arg0);
	}
	

	


}
