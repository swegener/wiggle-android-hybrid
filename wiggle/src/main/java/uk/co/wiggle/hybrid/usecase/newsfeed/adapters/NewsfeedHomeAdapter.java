package uk.co.wiggle.hybrid.usecase.newsfeed.adapters;

import java.util.ArrayList;
import java.util.List;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.usecase.newsfeed.activities.NewsfeedActivity;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNewsfeed;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNewsfeedRSSItem;
import uk.co.wiggle.hybrid.application.utils.DateUtils;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.extensions.PicassoSSL;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;



import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LevelListDrawable;
import android.os.AsyncTask;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class NewsfeedHomeAdapter extends BaseAdapter {

	private String logTag = "NewsfeedHomeAdapter";

	private List<ObjNewsfeedRSSItem> mNewsfeeds = new ArrayList<ObjNewsfeedRSSItem>(); // the original list of categories

	private NewsfeedActivity mContext;
	
	public NewsfeedHomeAdapter(NewsfeedActivity context) {
		mContext = context;
	}

	public void setListContent(List<ObjNewsfeedRSSItem> listItems) {
		mNewsfeeds = listItems;
	}

	@Override
	public int getCount() {
		
		if (mNewsfeeds == null){
			return 0;
			
		}else{
			return mNewsfeeds.size();
		}
		
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	private class ViewHolder {
		
		TextView newsfeedRow_tvTitle;
		TextView newsfeedRow_tvContent;
		ImageView newsfeedRow_ivNewsImage;
		ImageView newsfeedRow_ivBrandImage;
		TextView newsfeedRow_tvReadMore;
		TextView newsfeedRow_tvDate;
		
	}		

	@Override
	public View getView(int arg0, View view, ViewGroup arg2) {
		final int position = arg0;

		final ObjNewsfeedRSSItem newsItem = (ObjNewsfeedRSSItem) mNewsfeeds.get(arg0);

		Logger.printMessage(logTag, "NEWSFEED RSS ITEM: " + newsItem.toXML(), Logger.INFO);

		final ViewHolder holder;
			
		if(view==null){
			
			LayoutInflater inflater = LayoutInflater.from(mContext);
			view = inflater.inflate(R.layout.newsfeed_home_list_row, null);
			
			holder = new ViewHolder();
			
			holder.newsfeedRow_tvTitle = (TextView) view.findViewById(R.id.newsfeedRow_tvTitle);
			holder.newsfeedRow_tvTitle.setTypeface(AppTypefaces.instance().fontMainBold);
			
			
			holder.newsfeedRow_tvContent = (TextView) view.findViewById(R.id.newsfeedRow_tvContent);
			holder.newsfeedRow_tvContent.setTypeface(AppTypefaces.instance().fontMainRegular);
			
			holder.newsfeedRow_ivNewsImage = (ImageView) view.findViewById(R.id.newsfeedRow_ivNewsImage);
			
			holder.newsfeedRow_ivBrandImage = (ImageView) view.findViewById(R.id.newsfeedRow_ivBrandImage);
			
			holder.newsfeedRow_tvReadMore = (TextView) view.findViewById(R.id.newsfeedRow_tvReadMore);
			holder.newsfeedRow_tvReadMore.setTypeface(AppTypefaces.instance().fontMainRegular);
			
			
			holder.newsfeedRow_tvDate = (TextView) view.findViewById(R.id.newsfeedRow_tvDate);
			holder.newsfeedRow_tvDate.setTypeface(AppTypefaces.instance().fontMainRegular);
			
			view.setTag(holder);
			
		}else{
			holder = (ViewHolder)view.getTag();		
		}

		String strTitle = newsItem.getString(ObjNewsfeedRSSItem.FIELD_TITLE);
		Spanned htmlTitle = Html.fromHtml(strTitle);
		holder.newsfeedRow_tvTitle.setText(htmlTitle);
		
		
		
		//Use this if you want to display html - if <img ../> tags are embedded this leads to the display of green squares
		//for the images though which is ugly, so instead of using a simple Html.fromHtml(text) we remove all image tags from 
		//the text
		Spanned htmlContent = Html.fromHtml(newsItem.getContent().replaceAll("<img.+?>", ""));
		
		//Use the below if you want to display embedded images in the text view as well ... this downloads images in <img ..> 
		//tags async and shows them 
		//Spanned htmlContent = getHtmlWithAsyncImageDownload(newsItem, newsfeedRow_tvContent);
		String strTrimmedContent = htmlContent.toString().trim();
		holder.newsfeedRow_tvContent.setText(strTrimmedContent);
		//turn numbers, URLs in text into clickable hyperlinks: that is not useful here - our HTML already contains linked elements. 
		//We want the user to be able to use those.
		//Linkify.addLinks(newsfeedRow_tvContent, Linkify.ALL);
		//So instead of the above we use the following which makes html tags clickable that are otherwise only presented, but dont react to user input
		//holder.newsfeedRow_tvContent.setMovementMethod(LinkMovementMethod.getInstance());
		
		
		String strAdditionalImageUrl = newsItem.getAdditionalImageUrl();
		if(strAdditionalImageUrl!=null && !"".equals(strAdditionalImageUrl)){
			holder.newsfeedRow_ivNewsImage.setVisibility(View.VISIBLE);
			PicassoSSL.with(mContext).load(strAdditionalImageUrl).into(holder.newsfeedRow_ivNewsImage);
		}else{
			holder.newsfeedRow_ivNewsImage.setVisibility(View.GONE);
		}
		
		
		ObjNewsfeed parentFeed = newsItem.getParentChannel().getParentNewsfeed();
		String strBrandImageUrl = parentFeed.getString(ObjNewsfeed.FIELD_IMAGE_URL);
		PicassoSSL.with(mContext).load(strBrandImageUrl).into(holder.newsfeedRow_ivBrandImage);
		
		String strGUIDate = DateUtils.getTimeAgoFromRSSDate(newsItem.getPublishDate());
		holder.newsfeedRow_tvDate.setText(strGUIDate);
		
		//for below listner: use newsfeedRow_tvReadMore to set item listener on "Read more >" control only as there is hyperlink-URLs embedded in text sometimes. 
		view.setOnClickListener(null);
		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				
				NewsfeedActivity.instance().showRssDetail(newsItem);
				//AppUtils.showCustomToast("Loading into webview: " + newsItem.getString(ObjNewsfeedRSSItem.FIELD_LINK), true);
				
			}

		});

		return view;

	}

	@Override
	public Object getItem(int arg0) {
		return mNewsfeeds.get(arg0);
	}


	private Spanned getHtmlWithAsyncImageDownload(final ObjNewsfeedRSSItem newsItem, final TextView targetTextView){
		
		Spanned htmlString = Html.fromHtml(newsItem.getContent(), new Html.ImageGetter() {
            @Override
            public Drawable getDrawable(String source) {
                LevelListDrawable d = new LevelListDrawable();
                Drawable empty = mContext.getResources().getDrawable(R.mipmap.ic_launcher);
                d.addLevel(0, 0, empty);
                d.setBounds(0, 0, empty.getIntrinsicWidth(), empty.getIntrinsicHeight());
                new ImageGetterAsyncTask(mContext, source, d).execute(targetTextView);

                return d;
            }
        }, null);
		
		return htmlString;
		
	}
	
	
	class ImageGetterAsyncTask extends AsyncTask<TextView, Void, Bitmap> {


	    private LevelListDrawable levelListDrawable;
	    private Context context;
	    private String source;
	    private TextView t;

	    public ImageGetterAsyncTask(Context context, String source, LevelListDrawable levelListDrawable) {
	        this.context = context;
	        this.source = source;
	        this.levelListDrawable = levelListDrawable;
	    }

	    @Override
	    protected Bitmap doInBackground(TextView... params) {
	        t = params[0];
	        try {
	            Logger.printMessage(logTag, "Downloading the image from: " + source, Logger.INFO);
	            return PicassoSSL.with(context).load(source).get();
	        } catch (Exception e) {
	            return null;
	        }
	    }

	    @Override
	    protected void onPostExecute(final Bitmap bitmap) {
	        try {
	            Drawable d = new BitmapDrawable(mContext.getResources(), bitmap);
	            Point size = new Point();
	            mContext.getWindowManager().getDefaultDisplay().getSize(size);
	            // Lets calculate the ratio according to the screen width in px
	            int multiplier = size.x / bitmap.getWidth();
	            Logger.printMessage(logTag, "multiplier: " + multiplier, Logger.ALL);
	            levelListDrawable.addLevel(1, 1, d);
	            // Set bounds width  and height according to the bitmap resized size
	            levelListDrawable.setBounds(0, 0, bitmap.getWidth() * multiplier, bitmap.getHeight() * multiplier);
	            levelListDrawable.setLevel(1);
	            t.setText(t.getText()); // invalidate() doesn't work correctly...
	        } catch (Exception e) { /* Like a null bitmap, etc. */ }
	    }
	}
	
}
