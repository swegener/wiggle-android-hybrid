package uk.co.wiggle.hybrid.application.session;

import java.util.Date;
import java.util.Locale;

import org.json.JSONObject;

import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjSalesOrderManager;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNavigationCategory;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.persistence.DataStore;

import android.location.Location;
import android.net.Uri;
import android.os.Build;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class AppSession {
	
	String logTag = "AppSession";
	
	private static AppSession mSession = null;
	
	//TODO: adjust in case object managers are needed
	//public ObjNavigationManager mNavigationManager;
	
	Date dtLastResortRefresh;
	Location mLastUserLocation;
    
	
	public AppSession(){
		
		//TODO: adjust
		//mNavigationManager = ObjNavigationManager.getInstance();

		//Things to do when a new session is started

		
	}
	
	public static AppSession instance(){
		
		if(mSession==null){
			mSession = new AppSession();
		}
		
		return mSession;
		
	}

	public void performLogout(){
		
		//MainActivity.instance().stopLocationService();
		clearUserSpecificData();
		 
	}
	
	public void clearUserSpecificData(){

		//clear "refresh token" that is used to perform silent re-authentication
		AppRequestHandler.instance().setRefreshToken("");

		//clear user specific fields
		ObjAppUser.instance().setField(ObjAppUser.FIELD_ID, -1);
		ObjAppUser.instance().setField(ObjAppUser.FIELD_TITLE, "");
		ObjAppUser.instance().setField(ObjAppUser.FIELD_FORENAME, "");
		ObjAppUser.instance().setField(ObjAppUser.FIELD_SURNAME, "");
		ObjAppUser.instance().setField(ObjAppUser.FIELD_CUSTOMER_ID, "");
		ObjAppUser.instance().setField(ObjAppUser.FIELD_ACCESS_TOKEN, "");
		ObjAppUser.instance().setField(ObjAppUser.FIELD_DISCOUNT, "");


		//** Logon credentials - new handling - START ** /
		//ObjAppUser.instance().setField(ObjAppUser.FIELD_REMEMBER_CREDENTIALS, 0);
		//If user did not choose to have his credentials remembered - clear login information, otherwise keep it!
		if(ObjAppUser.instance().getBoolean(ObjAppUser.FIELD_REMEMBER_CREDENTIALS)==false){
			ObjAppUser.instance().setField(ObjAppUser.FIELD_EMAIL, ""); //handle email same as password
			ObjAppUser.instance().setField(ObjAppUser.FIELD_PASSWORD, "");
		}else{
            //user decided to remember credentials - leave email, but remove password in any case to protect account
			//Removed on Wiggle's Request - password should also be remembered and presented to the user
			//ObjAppUser.instance().setField(ObjAppUser.FIELD_PASSWORD, "");
        }

        //once the above is done - reset "remember credentials" so the user can make a new decision next time he logs in
		//ObjAppUser.instance().setField(ObjAppUser.FIELD_REMEMBER_CREDENTIALS, 0); //removed (version 54)
		//** Logon credentials - new handling - END **/

		ObjAppUser.instance().saveToSharedPreferences();
		
		//clear all managers
		clearObjectManagers();
		
		//Delete other user specific cache files
		String strUserCountry = ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY);
		DataStore.deleteCacheFile(strUserCountry, AppRequestHandler.ENDPOINT_CATEGORIES);
		DataStore.deleteCacheFile(strUserCountry, AppRequestHandler.getOrderTrackingEndpoint());

	    //clear image cache
	    //TODO: for later version of Picasso - call Picasso's cache evictAll() method

	}
	
	public void clearObjectManagers(){
		
		//Clear order manager
		ObjSalesOrderManager.getInstance().clear();

	}
	
    public boolean isAccessTokenOutdated(){
    	
    	Date now = new Date();
    	
    	if(now.getTime() - dtLastResortRefresh.getTime()> 30 * 60 * 1000){
    		return true;
    	}else{
    		return false;
    	}
    	
    }
    
    public void recordAccessTokenRefresh(Date dtLastRefresh){
    	
    	dtLastResortRefresh = dtLastRefresh;
    	
    }
    
    public Locale getCurrentLocale(){
    	
    	Locale currentLocale;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			currentLocale = ApplicationContextProvider.getContext().getResources().getConfiguration().getLocales().get(0);
		} else {
			currentLocale = ApplicationContextProvider.getContext().getResources().getConfiguration().locale;
		}

		return currentLocale;
    	
    }
    
    /* Other Session variables */
    //Used to indicate if it was WelcomeActivity's continue button who opened StartupActivity
    //This displays the back-button on the StartupActivity then
    private boolean blnAllowBackNavigationToWelcomeScreen;
    
    public void setAllowBackNavigationToWelcomeScreen(boolean blnAllow){
    	this.blnAllowBackNavigationToWelcomeScreen = blnAllow;
    }
    
    public boolean isAllowedBackNavigationToWelcomeScreen(){
    	return this.blnAllowBackNavigationToWelcomeScreen;
    }
    
    //Used to store if Login screen was called from within the HybridActivity or not
    //If true, no second instance of Hybrid activity will be started
    private boolean blnInAppLogin;
    	
    public void setInAppLogin(boolean blnValue){
    	this.blnInAppLogin = blnValue;
    }
    
    public boolean isInAppLogin(){
    	return this.blnInAppLogin;
    }
    
    //Used to store if Login screen was shown after the user pressing a menu item that leads to an Android Native screen
    private ObjNavigationCategory androidNativeCategory;
    
    public void setAndroidNativeCategory(ObjNavigationCategory category){
    	androidNativeCategory = category;
    }
    
    public ObjNavigationCategory getAndroidNativeCategory(){
    	return androidNativeCategory;
    }

    /* 
     * The following is used by the Order Tracking use case.
     * We hold on to the response from the order tracking endpoint until the user decides to open the 
     * order tracking screen. Only when the order tracking screen is opened, we cache the response so we use it as a baseline 
     * for future comparisons of new order information and already viewed order information
     */
    private JSONObject mOrderTrackingPayload;
    private String mOrderTrackingEndpoint;
    
    public void setOrderTrackingPayload(JSONObject jsonPayload, String strURLEndpoint){
    	mOrderTrackingPayload = jsonPayload;
    	mOrderTrackingEndpoint = strURLEndpoint;
    }
    
    public void acceptOrderTrackingPayloadIntoCache(){
    	if(mOrderTrackingPayload!=null){
    		DataStore.writeJSONResponseToFile(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY), mOrderTrackingEndpoint, mOrderTrackingPayload);
    		mOrderTrackingPayload = null;
    		mOrderTrackingEndpoint = null;
    	}
    }

	public void setLastLocation(Location location){
		mLastUserLocation = location;
	}

	public Location getLastLocation(){
		return mLastUserLocation;
	}

}
