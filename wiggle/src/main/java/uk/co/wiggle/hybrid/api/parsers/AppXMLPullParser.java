package uk.co.wiggle.hybrid.api.parsers;

/**
 * 
 * @author AndroMedia
 * 
 * This class is used to parsed RSS Newsfeed XML information
 *
 */

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;

import org.json.JSONException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import uk.co.wiggle.hybrid.application.datamodel.objects.ObjApiRequest;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNewsfeed;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNewsfeedRSSChannel;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNewsfeedRSSItem;
import uk.co.wiggle.hybrid.application.persistence.DataStore;


public class AppXMLPullParser {
	
	private static final String logTag = "AppXMLPullParser";

	private static AppXMLPullParser mInstance = null;
	
    public static AppXMLPullParser instance(){
	  
	  if (mInstance == null){
		  mInstance = new AppXMLPullParser();
	  }
	  
	  return mInstance;
    }
  
	
	   /**
		 * Parse the xml network response (RSS Feed)
		 * 	 * 
		 * @param apiRequest
		 * @param strResponse
		 * @param isCachedData
		 * @return void
	     * @throws XmlPullParserException 
		 * @throws JSONException
		 */
		public void parseRSSTypeNewsfeed(final ObjApiRequest apiRequest, final String strResponseData, boolean isCachedData, boolean blnSkipXMLTag) throws XmlPullParserException, IOException {

			//get underlying newsfeed
			ObjNewsfeed requestedFeed = (ObjNewsfeed) apiRequest.getApiObject();
			
			ArrayList<ObjNewsfeedRSSChannel> rssChannels = new ArrayList<ObjNewsfeedRSSChannel>();
			
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
	        XmlPullParser xpp = factory.newPullParser();
	        

	        /*Show xml document content as string
	        String theString = IOUtils.toString(inputStream, AppRequestHandler.ENCODING_UTF_8); 
	        AppUtils.showCustomToast(theString, false);
	        */
	        
	        /* A bit of exception handling for some RSS problems */
	        String strDataToParse = strResponseData;
	        
	        //Some XML data has leading spaces - trim them off as this confuses our XML Pull parser and leads to errors
	        if(strDataToParse.startsWith(" ")){
	        	strDataToParse = strDataToParse.trim();
	        }
	        
	        //manual handling for feeds that start with <rss as oposed to <xml (http://www.garmin.blogs.com/ukpr/fitness.xml)
			//Just make sure the parse does not skip the first tag ( parameter blnSkipXmlTag false)
			if(strDataToParse.startsWith("<rss")){
	        	blnSkipXMLTag = true;
	        }
	        
	        Reader stringReader = new StringReader(strDataToParse);
	        xpp.setInput(stringReader);


        	//Document starts with <?xml ...  > <rss ... >
        	//We are not interested in reading the first tag (<?xml>), so move directly to the next tag <rss>
	        //There are feeds that start with <rss immediately - dont skip for those as this will lead to parse errors.
        	if(!blnSkipXMLTag){
        		xpp.nextTag();
        	}
	        
	        
	        //get rss tag and parse it. If the rss tag cant be found, then our XML is invalid or we are dealing with an Atom type feed
	        try{
	        	xpp.require(XmlPullParser.START_TAG, null, "rss");
	        }catch (XmlPullParserException xe){
	        	
	        	/** 
	        	 * if the require rss-tag throws an exception, check if we deal with an Atom feed
	        	 * https://en.wikipedia.org/wiki/Atom_(standard)
	        	 * <?xml.. >
	        	 *   <feed>
	        	 *      <entry>
	        	 *      </entry>
	        	 *   <feed>
	        	 *   
	        	 *   If parsing this as Atom feed fails, propagate the error up
	        	 *   
	        	 **/
	        	parseAtomTypeNewsfeed(apiRequest, strResponseData, blnSkipXMLTag, false);
	        	return;
	        	
	        }
	        
	        
	       
	        
	        while (xpp.next() != XmlPullParser.END_TAG) {
	        	
	            if (xpp.getEventType() != XmlPullParser.START_TAG) {
	                continue;
	            }
	            
	            String name = xpp.getName();
	            
	            ObjNewsfeedRSSChannel rssChannel = null;
	            
	            // parse channels
	            if (name.equals("channel")) {
	            	rssChannel = (ObjNewsfeedRSSChannel) parseRssChannel(xpp, "channel", new ObjNewsfeedRSSChannel());
	            	rssChannel.setParentNewsfeed(requestedFeed);
		    		rssChannels.add(rssChannel);
	            } else {
	                skip(xpp);
	            }
	            
	        }  
        
	        //add channels to api object (from ObjNewsfeedManager) - so we can access the channels from anywhere in the app
			requestedFeed.setChannels(rssChannels);
			
			//we made it to the end of the parsing without Exceptions - save data to cache
			if(!isCachedData){
				String strUserCountry = ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY);
				//write the original string response data to a cache file for later use
				DataStore.writeStringResponseToFile(strUserCountry, apiRequest.getRequestURI(), strResponseData);
			}
		    
			
		}
		

		private ObjNewsfeedRSSChannel parseRssChannel(XmlPullParser xpp, String strXMLObjectTag, ObjNewsfeedRSSChannel rssChannel) throws XmlPullParserException, IOException{
			
			//get "channel" tag
			xpp.require(XmlPullParser.START_TAG, null, strXMLObjectTag);
			
			while (xpp.next() != XmlPullParser.END_TAG) {
		        if (xpp.getEventType() != XmlPullParser.START_TAG) {
		            continue;
		        }
		        
		        String xmlTagName = xpp.getName();
		        
		        //check if this is an embedded item object - if yes, parse it
		        if("item".equals(xmlTagName)){
		        	
		        	ObjNewsfeedRSSItem rssItem = parseRssItem(xpp, "item", new ObjNewsfeedRSSItem());
		        	rssItem.setParentChannel(rssChannel);
		        	rssChannel.addRssItem(rssItem);
		        
		        //read flat fields for channel
		        }else if(rssChannel.getFields().contains(xmlTagName)){
		        	xpp.require(XmlPullParser.START_TAG, null, xmlTagName);
		        	String strXmlValue = "";
		            if (xpp.next() == XmlPullParser.TEXT) {
		            	strXmlValue = xpp.getText();
		            	rssChannel.setField(xmlTagName, strXmlValue);
		                xpp.nextTag();
		            }
		       
		        } else {
		            skip(xpp);
		        }
		    }
			
		    return rssChannel;
			
		}
		
		private ObjNewsfeedRSSItem parseRssItem(XmlPullParser xpp, String strXMLObjectTag, ObjNewsfeedRSSItem rssItem) throws XmlPullParserException, IOException{

			
			//get "item" tag
			xpp.require(XmlPullParser.START_TAG, null, strXMLObjectTag);
			
			while (xpp.next() != XmlPullParser.END_TAG) {
		        if (xpp.getEventType() != XmlPullParser.START_TAG) {
		            continue;
		        }
		        
		        String xmlTagName = xpp.getName();
		        
		       
		        
		        if(rssItem.getFields().contains(xmlTagName)){
		        	xpp.require(XmlPullParser.START_TAG, null, xmlTagName);
		        	String strXmlValue = "";
		            if (xpp.next() == XmlPullParser.TEXT) {
		            	strXmlValue = xpp.getText();
		            	rssItem.setField(xmlTagName, strXmlValue);
		            	//after processing information from this tag: move parser to next XML tag
			            xpp.nextTag();
		            }
		            
		            //special handling for some fields
		            if(ObjNewsfeedRSSItem.FIELD_MEDIA_CONTENT.equals(xmlTagName)
		            		|| ObjNewsfeedRSSItem.FIELD_ENCLOSURE.equals(xmlTagName)
		            		){
		            	//get XML property "url"
		            	//Example for Wiggle High 5: <media:content url="http://www.wigglehighfive.com/wp-content/uploads/2016/04/IMG_5676_F-300x92.jpg" width="300" height="92" medium="image" type="image/jpeg" />
		            	String strUrlProperty = xpp.getAttributeValue(null, "url");
		            	strXmlValue = strUrlProperty;
		            	rssItem.setField(xmlTagName, strXmlValue);
		            	//xpp.nextTag(); //don't move parser here as we only re-fine the information we processed above
		            }
		            
		            
		        } else {
		            skip(xpp);
		        }
		    }

		    return rssItem;
			
		}
		
		
		
		private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
			
		    if (parser.getEventType() != XmlPullParser.START_TAG) {
		        throw new IllegalStateException();
		    }
		    int depth = 1;
		    while (depth != 0) {
		        switch (parser.next()) {
		        case XmlPullParser.END_TAG:
		            depth--;
		            break;
		        case XmlPullParser.START_TAG:
		            depth++;
		            break;
		        }
		    }
		 }
		
		
	   /**
		 * Parse the xml network response (Atom feed)
		 * 	 * 
		 * @param apiRequest
		 * @param strResponse
		 * @param isCachedData
		 * @return void
	     * @throws XmlPullParserException 
		 * @throws JSONException
		 */
		public void parseAtomTypeNewsfeed(final ObjApiRequest apiRequest, final String strResponseData, boolean isCachedData, boolean blnSkipXMLTag) throws XmlPullParserException, IOException {

			//get underlying newsfeed
			ObjNewsfeed requestedFeed = (ObjNewsfeed) apiRequest.getApiObject();
			
			ArrayList<ObjNewsfeedRSSChannel> rssChannels = new ArrayList<ObjNewsfeedRSSChannel>();
			
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
	        XmlPullParser xpp = factory.newPullParser();
	        
	        
	        //xpp.setInput(inputStream, null);
	        
	        //Some XML newsreader documents start with a byte order mark. The Appache commons BOMInputStream
	        //can handle this, thus the following conversion is needed.
	        //Thank you, http://stackoverflow.com/questions/15254089/kxmlparser-throws-unexpected-token-exception-at-the-start-of-rss-pasing
	        //InputStream bomSafeInputStream = new BOMInputStream(inputStream, false, ByteOrderMark.UTF_8); 
	        //InputStream bomSafeInputStream = new BOMInputStream(inputStream, false, ByteOrderMark.UTF_8, ByteOrderMark.UTF_16BE, ByteOrderMark.UTF_16LE, ByteOrderMark.UTF_32BE, ByteOrderMark.UTF_32LE);
	        //xpp.setInput(bomSafeInputStream, null);
	        
	        /*Show xml document content as string
	        String theString = IOUtils.toString(inputStream, AppRequestHandler.ENCODING_UTF_8); 
	        AppUtils.showCustomToast(theString, false);
	        */
	        
	        /* A bit of exception handling for some RSS problems */
	        String strDataToParse = strResponseData;
	        
	        //Some XML data has leading spaces - trim them off as this confuses our XML Pull parser and leads to errors
	        if(strDataToParse.startsWith(" ")){
	        	strDataToParse = strDataToParse.trim();
	        }
	        
	        //manual handling for feeds that start with <rss as oposed to <xml (http://www.garmin.blogs.com/ukpr/fitness.xml)
			//Just make sure the parse does not skip the first tag ( parameter blnSkipXmlTag false)
			if(strDataToParse.startsWith("<feed")){
	        	blnSkipXMLTag = true;
	        }
	        
	        Reader stringReader = new StringReader(strDataToParse);
	        xpp.setInput(stringReader);


        	//Document starts with <?xml ...  > <rss ... >
        	//We are not interested in reading the first tag (<?xml>), so move directly to the next tag <rss>
	        //There are feeds that start with <rss immediately - dont skip for those as this will lead to parse errors.
        	if(!blnSkipXMLTag){
        		xpp.nextTag();
        	}
	        
	        
	        //get feed tag and parse it
        	ObjNewsfeedRSSChannel atomFeed = (ObjNewsfeedRSSChannel) parseAtomFeed(xpp, "feed", new ObjNewsfeedRSSChannel());
        	atomFeed.setParentNewsfeed(requestedFeed);
        	
        	//add channels to api object (from ObjNewsfeedManager) - so we can access the channels from anywhere in the app
			requestedFeed.setAtomFeed(atomFeed);
			
			//we made it to the end of the parsing without Exceptions - save data to cache
			if(!isCachedData){
				String strUserCountry = ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY);
				//write the original string response data to a cache file for later use
				DataStore.writeStringResponseToFile(strUserCountry, apiRequest.getRequestURI(), strResponseData);
			}
		    
			
		}
		
		private ObjNewsfeedRSSChannel parseAtomFeed(XmlPullParser xpp, String strXMLObjectTag, ObjNewsfeedRSSChannel atomFeed) throws XmlPullParserException, IOException{
			
			//get "feed" tag
			xpp.require(XmlPullParser.START_TAG, null, strXMLObjectTag);
			
			while (xpp.next() != XmlPullParser.END_TAG) {
		        if (xpp.getEventType() != XmlPullParser.START_TAG) {
		            continue;
		        }
		        
		        String xmlTagName = xpp.getName();
		        
		        //ignore "id" tag - this field has a different meaning in our app
		        if("id".equals(xmlTagName)){
		        	skip(xpp);
		        	continue;
		        }
		        

		        //check if this is an embedded item object - if yes, parse it
		        if("entry".equals(xmlTagName)){
		        	
		        	ObjNewsfeedRSSItem atomEntry = parseAtomEntry(xpp, "entry", new ObjNewsfeedRSSItem());
		        	atomEntry.setParentChannel(atomFeed);
		        	atomFeed.addRssItem(atomEntry);
		        
		        //read flat fields for channel
		        }else if(atomFeed.getFields().contains(xmlTagName)){
		        	xpp.require(XmlPullParser.START_TAG, null, xmlTagName);
		        	String strXmlValue = "";
		            if (xpp.next() == XmlPullParser.TEXT) {
		            	strXmlValue = xpp.getText();
		            	atomFeed.setField(xmlTagName, strXmlValue);
		                xpp.nextTag();
		            }
		       
		        } else {
		            skip(xpp);
		        }
		    }
			
		    return atomFeed;
			
		}
				
				
		private ObjNewsfeedRSSItem parseAtomEntry(XmlPullParser xpp, String strXMLObjectTag, ObjNewsfeedRSSItem atomEntry) throws XmlPullParserException, IOException{

			
			//get "entry" tag
			xpp.require(XmlPullParser.START_TAG, null, strXMLObjectTag);
			
			while (xpp.next() != XmlPullParser.END_TAG) {
		        if (xpp.getEventType() != XmlPullParser.START_TAG) {
		            continue;
		        }
		        
		        String xmlTagName = xpp.getName();
		        
		        //ignore "id" tag - this field has a different meaning in our app
		        //We also skip a number of other tags we dont use or that would make parsing more complex than necessary
		        if("id".equals(xmlTagName)
		        		|| "author".equals(xmlTagName)
		        		|| "category".equals(xmlTagName)
		        		){
		        	skip(xpp);
		        	continue;
		        }
		        
		        //some tags have data in attributes
		        //<link rel="alternate" type="text/html" href="http://inspiration.goreapparel.com/running-with-the-moroccans/" />
		        if("link".equals(xmlTagName)){
		        	
		        	String strRel = xpp.getAttributeValue(null, "rel");
		        	
		        	//if rel="alternate", then this is our link to the article (there are various "link" tags in an atom entry
		        	if("alternate".equals(strRel)){
		        		String strLink = xpp.getAttributeValue(null, "href");
		        		atomEntry.setField(xmlTagName, strLink);
		        	}
		        	
		        	xpp.nextTag();
		        	continue;
		        }
		        
		       if(atomEntry.getFields().contains(xmlTagName)){
		        	
		        	xpp.require(XmlPullParser.START_TAG, null, xmlTagName);
		        	String strXmlValue = "";
		            if (xpp.next() == XmlPullParser.TEXT) {
		            	strXmlValue = xpp.getText();
		            	atomEntry.setField(xmlTagName, strXmlValue);
		            	//after processing information from this tag: move parser to next XML tag
			            xpp.nextTag();
		            }
		            
		            //Post-processing of certain values ... 
		            //the image is hidden in the Atom <summary> tag in embedded in ![CDATA[ html .. 
			        if("summary".equals(xmlTagName)){
			        	
			        	//String strSummary = new String(strXmlValue.getBytes(), AppRequestHandler.ENCODING_LATIN_I); //helps to display some images with special characters in path for Gore Apparel
			        	String strSummary = strXmlValue;
			        	//convert summary to UTF-8
			        	String strImageUrl = "";
			        	if(strSummary.contains("src=")){
			        		strImageUrl = strSummary.substring(strSummary.indexOf("src=")+5); //+5 as we want to move cursor after src="
			        		//find second " .. 
			        		strImageUrl = strImageUrl.substring(0, strImageUrl.indexOf("\""));
			        	}
			        	atomEntry.setField(ObjNewsfeedRSSItem.FIELD_IMAGE_SRC, strImageUrl);
			        	
			        }
		            
		        } else {
		            skip(xpp);
		        }
		    }

		    return atomEntry;
			
		}

}
