package uk.co.wiggle.hybrid.application.datamodel.objects;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.animations.AppAnimations;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.utils.DateUtils;
import uk.co.wiggle.hybrid.tagging.AppTagManager;
import uk.co.wiggle.hybrid.usecase.newsfeed.activities.NewsfeedActivity;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.fragments.FragmentShopNavigation;


import android.animation.ObjectAnimator;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;



/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class ObjNavigationCategory extends CustomObject {

	public static final String OBJECT_NAME = "category";

	private List<ObjNavigationCategory> mChildren = new ArrayList<ObjNavigationCategory>();
	private List<String> mAvailability = new ArrayList<String>();

	//store the parent object if any
	private ObjNavigationCategory mParent;

	private boolean blnIsAllOfCategory = false;

	public void setIsAllOfCategory(){
		blnIsAllOfCategory = true;
	}

	public boolean isAllOfCategory(){
		return blnIsAllOfCategory;
	}

	//fields used for categories taken from top_level_navigation.json
	public static final String FIELD_TYPE = "type"; // link, submenu, refinement
	public static final String FIELD_TITLE = "title";
	public static final String FIELD_NAME = "name";
	public static final String FIELD_LINK = "link";
	public static final String FIELD_API_URL = "apiurl";
	public static final String FIELD_AVAILABILITY = "availability";
	public static final String FIELD_GOOGLE_TAG_MANAGER_ID = "GTMID"; //the GTMID of the HybridActivity navigation drawer
	public static final String FIELD_STARTUP_GOOGLE_TAG_MANAGER_ID = "startupGTMID"; //the GTMID of the StartupActivity menu items
	public static final String FIELD_OPEN_IN_EXTERNAL_BROWSER = "external";
	public static final String JSON_LIST_CHILDREN = "items"; //category children in top_level_navigation.json
	public static final String FIELD_MENU_ITEM_TARGET = "menu_item_target";
	public static final String FIELD_MENU_ITEM_IMAGE = "menu_item_image";
	public static final String FIELD_INTERNAL_UNIQUE_ID = "uniqueId"; //not returned from server but used internally for raw json files


	//fields only used by child categories (refinements) from web API
	public static final String FIELD_REFINEMENT_API_URL = "ApiUrl";
	public static final String FIELD_REFINEMENT_NAME = "Name";
	public static final String FIELD_REFINEMENT_URL = "Url";
	public static final String JSON_LIST_REFINEMENTS = "Refinements"; //category children in web API response

	//fields only used by child categories from megamenu API
	public static final String FIELD_URL = "Url";
	public static final String FIELD_TEXT = "Text";
	public static final String FIELD_DATA_MAP = "DataMap"; //the unique key per country code
	public static final String FIELD_HAS_CHILDREN = "HasChildren";

	//fields only used by contentful menu items
	public static final String FIELD_START_DATE = "dateStart";
	public static final String FIELD_END_DATE = "dateEnd";
	public static final String FIELD_ORDER_ID = "order";
	public static final String FIELD_SHOW_ON_STARTUP = "visibleOnIntro";
	public static final String FIELD_SHOW_ON_NAV_DRAWER = "visibleOnMain";
	public static final String FIELD_SOURCE = "source";
	public static final String FIELD_IMAGE_ASSET_ID = "asset_id";

	//constants for category types
	public static final String CONST_CATEGORY_TYPE_LINK = "link";
	public static final String CONST_CATEGORY_TYPE_SUBMENU = "submenu";
	public static final String CONST_CATEGORY_TYPE_CATEGORIES = "categories";
	public static final String CONST_CATEGORY_TYPE_END_CATEGORY_OPEN_URL = "end_category_open_url"; //the last category in the raw json file that will open a URL (used for Outdoor & Gym new categories without API support)
	public static final String CONST_CATEGORY_TYPE_HEADER = "header";
	public static final String CONST_CATEGORY_TYPE_MENU_ITEM = "menu_item";
	public static final String CONST_CATEGORY_TYPE_WIGGLE_API = "wiggle_api";
	public static final String CONST_CATEGORY_TYPE_MEGAMENU_API = "megamenu_api";

	//source of this menu item
	public static final String CONST_CATEGORY_SOURCE_CONTENTFUL = "contentful_menu_item"; //a menu item configured via contentful
	public static final String CONST_CATEGORY_SOURCE_LOCAL = "local_menu_item"; //a menu item created from the local raw-json file

	//Wiggle version 2 - Tabbed navigation
	//Level 1 - Wiggle Home (root), Level 2 - Shop by Department,
	public static final int CONST_NAV_BROWSER_HIDDEN_LEVELS = 2;
	// Level 3 - Parent Category (Cycle, Run, Swim, Tri)
	//Level 4 is the first API level
	public static final int CONST_NAV_BROWSER_LEVEL_1 = 3; //corresponds to level 1 on the GUI, but level 4in the Wiggle nav tree
	public static final int CONST_NAV_BROWSER_LEVEL_2 = 4; //corresponds to level 2 on the GUI, but level 5 in the Wiggle nav tree
	public static final int CONST_NAV_BROWSER_LEVEL_3 = 5; //corresponds to level 3 on the GUI, but level 6 in the Wiggle nav tree

	private boolean blnChildrenRetrievedFromServer;

	public boolean isChildrenRetrievedFromServer() {
		return blnChildrenRetrievedFromServer;
	}

	public void setChildrenRetrievedFromServer(boolean blnRetrieved) {
		this.blnChildrenRetrievedFromServer = blnRetrieved;
	}

	//store a reference to the adapter view
	private View adapterView;

	public void setAdapterView(View v) {
		adapterView = v;
	}

	public View getAdapterView() {
		return adapterView;
	}

	private static List<String> sFieldList;

	static {

		sFieldList = Collections.synchronizedList(new ArrayList<String>());

		//fields used for categories taken from top_level_navigation.json
		sFieldList.add(FIELD_TYPE);
		sFieldList.add(FIELD_TITLE);
		sFieldList.add(FIELD_NAME);
		sFieldList.add(FIELD_LINK);
		sFieldList.add(FIELD_API_URL);
		sFieldList.add(FIELD_AVAILABILITY);
		sFieldList.add(FIELD_GOOGLE_TAG_MANAGER_ID);
		sFieldList.add(FIELD_OPEN_IN_EXTERNAL_BROWSER);
		sFieldList.add(JSON_LIST_CHILDREN);
		sFieldList.add(FIELD_MENU_ITEM_TARGET);
		sFieldList.add(FIELD_MENU_ITEM_IMAGE);

		//fields only used by child categories (refinements) from web API
		sFieldList.add(FIELD_REFINEMENT_API_URL);
		sFieldList.add(FIELD_REFINEMENT_NAME);
		sFieldList.add(FIELD_REFINEMENT_URL);
		sFieldList.add(JSON_LIST_REFINEMENTS);

		//fields only used by child categories from megamenu API
		sFieldList.add(FIELD_URL);
		sFieldList.add(FIELD_TEXT);
		sFieldList.add(FIELD_DATA_MAP);
		sFieldList.add(FIELD_HAS_CHILDREN);

		//fields only used by contentful menu items
		sFieldList.add(FIELD_START_DATE);
		sFieldList.add(FIELD_END_DATE);
		sFieldList.add(FIELD_ORDER_ID);
		sFieldList.add(FIELD_SHOW_ON_STARTUP);
		sFieldList.add(FIELD_SHOW_ON_NAV_DRAWER);
		sFieldList.add(FIELD_SOURCE);
		sFieldList.add(FIELD_IMAGE_ASSET_ID);

		//Internal fields
		sFieldList.add(FIELD_INTERNAL_UNIQUE_ID);


	}

	private static ConcurrentHashMap<String, Integer> sFieldTypes;

	static {
		sFieldTypes = new ConcurrentHashMap<String, Integer>();

		//fields used for categories taken from top_level_navigation.json
		sFieldTypes.put(FIELD_TYPE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_TITLE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_NAME, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_LINK, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_API_URL, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AVAILABILITY, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_GOOGLE_TAG_MANAGER_ID, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_OPEN_IN_EXTERNAL_BROWSER, FIELD_TYPE_BOOLEAN);
		sFieldTypes.put(JSON_LIST_CHILDREN, FIELD_TYPE_OBJECT);
		sFieldTypes.put(FIELD_MENU_ITEM_TARGET, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_MENU_ITEM_IMAGE, FIELD_TYPE_STRING);

		//fields only used by child categories (refinements) from web API
		sFieldTypes.put(FIELD_REFINEMENT_API_URL, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_REFINEMENT_NAME, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_REFINEMENT_URL, FIELD_TYPE_STRING);
		sFieldTypes.put(JSON_LIST_REFINEMENTS, FIELD_TYPE_OBJECT);

		//fields only used by child categories from megamenu API
		sFieldTypes.put(FIELD_URL, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_TEXT, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_DATA_MAP, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_HAS_CHILDREN, FIELD_TYPE_BOOLEAN);

		//fields only used by contentful menu items
		sFieldTypes.put(FIELD_START_DATE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_END_DATE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_ORDER_ID, FIELD_TYPE_DOUBLE);
		sFieldTypes.put(FIELD_SHOW_ON_STARTUP, FIELD_TYPE_BOOLEAN);
		sFieldTypes.put(FIELD_SHOW_ON_NAV_DRAWER, FIELD_TYPE_BOOLEAN);
		sFieldTypes.put(FIELD_SOURCE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_IMAGE_ASSET_ID, FIELD_TYPE_STRING);

		//internal fields
		sFieldTypes.put(FIELD_INTERNAL_UNIQUE_ID, FIELD_TYPE_STRING);
	}

	public ObjNavigationCategory() {
	}

	public ObjNavigationCategory(Bundle bundle) {
		super(bundle);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ObjNavigationCategory)) {
			return false;
		}

		ObjNavigationCategory other = (ObjNavigationCategory) o;
		String otherURI = other.getString(FIELD_REFINEMENT_URL);
		String thisURI = this.getString(FIELD_REFINEMENT_URL);

		String otherGTMID = other.getString(FIELD_GOOGLE_TAG_MANAGER_ID);
		String thisGTMID = this.getString(FIELD_GOOGLE_TAG_MANAGER_ID);

		String otherName = other.getDisplayName();
		String thisName = other.getDisplayName();

		String otherUniqueId = other.getString(FIELD_INTERNAL_UNIQUE_ID);
		String thisUniqueId = this.getString(FIELD_INTERNAL_UNIQUE_ID);

		String strOtherCategoryKey = otherURI + otherGTMID + otherName + otherUniqueId;
		String strThisCategoryKey = thisURI + thisGTMID + thisName + thisUniqueId;

		return strOtherCategoryKey.equals(strThisCategoryKey);
	}

	@Override
	public String getObjectName() {
		return OBJECT_NAME;
	}

	@Override
	public List<String> getFields() {
		return sFieldList;
	}

	@Override
	public Class<?> getSubclass() {
		return ObjNavigationCategory.class;
	}

	@Override
	public Integer getFieldType(String fieldName) {
		return sFieldTypes.get(fieldName);
	}

	// handler for children of this object
	public List<ObjNavigationCategory> getAllChildren() {
		return mChildren;
	}

	public List<ObjNavigationCategory> getChildrenForCurrentCountry() {

		List<ObjNavigationCategory> mApplicableChildren = new ArrayList<ObjNavigationCategory>();
		String strCountry = ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY);

		for (int i = 0; i < mChildren.size(); i++) {
			ObjNavigationCategory child = mChildren.get(i);
			if (child.isAvailable(strCountry)) {
				mApplicableChildren.add(child);
			}
		}

		return mApplicableChildren;
	}

	public void addChild(ObjNavigationCategory child) {
		mChildren.add(child);
	}

	public void addChildrenAtPosition(Collection<? extends ObjNavigationCategory> lstChildren, int intPosition) {
		mChildren.addAll(intPosition, lstChildren);
	}

	public boolean setChildren(Collection<? extends ObjNavigationCategory> arg0) {
		mChildren.clear();
		return mChildren.addAll(arg0);
	}

	public List<String> getAvailability() {
		return mAvailability;
	}

	public void addAvailability(String strAvailability) {
		mAvailability.add(strAvailability);
	}

	public boolean setAvailability(Collection<? extends String> arg0) {
		mAvailability.clear();
		return mAvailability.addAll(arg0);
	}

	public ObjNavigationCategory getParent() {
		return mParent;
	}

	public void setParent(ObjNavigationCategory parent) {
		this.mParent = parent;
	}
	
	/* Other convenience methods */

	public boolean isAvailable(String strRequiredAvailability) {

		for (int i = 0; i < mAvailability.size(); i++) {
			String strActualAvailability = mAvailability.get(i);
			if (strRequiredAvailability.equals(strActualAvailability)) {
				return true;
			}
		}

		return false;
	}

	public String getDisplayName() {

		String strDisplayName = "";

		if (CONST_CATEGORY_TYPE_CATEGORIES.equals(getString(FIELD_TYPE))
				||
				CONST_CATEGORY_TYPE_END_CATEGORY_OPEN_URL.equals(getString(FIELD_TYPE))
				) {
			strDisplayName = getString(FIELD_NAME);
		} else if (CONST_CATEGORY_TYPE_WIGGLE_API.equals(getString(FIELD_TYPE))) {
			strDisplayName = getString(FIELD_REFINEMENT_NAME);
		} else if (CONST_CATEGORY_TYPE_MEGAMENU_API.equals(getString(FIELD_TYPE))){
			strDisplayName = getString(FIELD_TEXT);
		} else {
			strDisplayName = getString(FIELD_TITLE);
		}

		//Exception for local categories: display names can be string resources ...
		if(strDisplayName.startsWith("@")){
			String strPropertyName = strDisplayName.substring(1);
			String strResource = ApplicationContextProvider.getContext().getResources()
					.getString(ApplicationContextProvider.getContext().getResources()
							.getIdentifier(strPropertyName, "string", ApplicationContextProvider.getContext().getPackageName()));
			strDisplayName = strResource;
		}

		return strDisplayName;

	}

	public String getTargetUrl() {

		String strType = getString(FIELD_TYPE);

		if (CONST_CATEGORY_TYPE_LINK.equals(strType)) {
			return getString(FIELD_LINK);
		} else if (CONST_CATEGORY_TYPE_CATEGORIES.equals(strType)
				||
				CONST_CATEGORY_TYPE_END_CATEGORY_OPEN_URL.equals(strType)
				) {
			String strWiggleHome = AppUtils.getWiggleURL(ApplicationContextProvider.getContext().getString(R.string.str_url_wiggle_home));
			String strURL = strWiggleHome + "/" + getString(FIELD_API_URL);
			return strURL;
		} else if (CONST_CATEGORY_TYPE_WIGGLE_API.equals(strType)) {
			return getString(FIELD_REFINEMENT_URL);
		} else if (CONST_CATEGORY_TYPE_MEGAMENU_API.equals(strType)) {

			//Megamenu returns complete URLs - but also just some paths in some cases (e.g. "/new") in URL field
			String strMegamenuUrlPath = getString(FIELD_URL);
			if(strMegamenuUrlPath.startsWith("/")) {
				String strWiggleHome = AppUtils.getWiggleURL(ApplicationContextProvider.getContext().getString(R.string.str_url_wiggle_home_https));
				String strURL = strWiggleHome + strMegamenuUrlPath;
				return strURL;
			}else {
				return getString(FIELD_URL);
			}
		} else {
			return "";
		}

	}

	public List<ObjNavigationCategory> getBreadcrumb() {

		List<ObjNavigationCategory> breadcrumb = new ArrayList<ObjNavigationCategory>();
		ObjNavigationCategory currentCategory = this; //starting point of breadcrumb

		breadcrumb.add(currentCategory);

		while ((currentCategory.getParent() != null)) {

			//add items starting from child categories to parent 
			//the very first entry will be the "All" item, the second one the "back to" item
			currentCategory = currentCategory.getParent();
			breadcrumb.add(currentCategory);

		}


		return breadcrumb;

	}

	public String getBreadcrumbString(){

		String strBreadcrumb = "";

		List<ObjNavigationCategory> lstBreadcrumb = getBreadcrumb();
		for(int i=lstBreadcrumb.size()-1;i>=0;i--){
			ObjNavigationCategory breadcrumbCategory = lstBreadcrumb.get(i);
			if(!"".equals(strBreadcrumb)){
				strBreadcrumb = strBreadcrumb + ">";
			}
			strBreadcrumb  = strBreadcrumb + breadcrumbCategory.getDisplayName();
		}

		return strBreadcrumb;
	}

	public boolean isConfirmedDeadEnd() {

		//megamenu api tells us if there are children ..
		if (CONST_CATEGORY_TYPE_MEGAMENU_API.equals(getString(FIELD_TYPE))){
			return !getBoolean(FIELD_HAS_CHILDREN);
		}

		//otherwise inspect child records
		if (mChildren.size() > 0) {
			return false;
		}

		return isChildrenRetrievedFromServer();

	}

	public boolean between(Date startDate, Date endDate) {

		Date now = new Date();

		if (now.equals(startDate) || now.equals(endDate)) {
			return true;
		} else if (now.after(startDate) && now.before(endDate)) {
			return true;
		} else {
			return false;
		}

	}


	public String getImagePath(String strFieldName) {

		//check if this string starts with a domain ..
		//This is highly specific, so may work for categories but not for others
		//Image path is given e.g. as
		// "//images.contentful.com/qm3myhm7mg7k/65tItPUpeoyk6WwoQ44SSY/e0906772e9a38f14cd42c103f0524192/iphone3x.png"
		// so we just prefix this with http:

		String strImagePath = getString(strFieldName);

		if (strImagePath == null) {
			strImagePath = "";
		} else {

			if (strImagePath.startsWith("//")) {
				strImagePath = "http:" + strImagePath;
			}

		}

		return strImagePath;

	}

	public int getCategoryLevel(){

		int iLevel = 0;

		ObjNavigationCategory parentCategory = this;

		while(parentCategory!=null){
			parentCategory = parentCategory.getParent();
			iLevel++;
		}

		//All-of-category needs special handling ..
		//"All of" category is an artificial copy of parent to a lower level
		//The "All of" category thus has one parent less
		if(isAllOfCategory()){
			iLevel = iLevel + 1;
		}


		return iLevel;

	}

	public ObjNavigationCategory getParentForLevel(int iDesiredLevel){

		int iLevel = 0;

		ObjNavigationCategory parentCategory = this;

		while(parentCategory!=null){
			parentCategory = parentCategory.getParent();
			iLevel++;
			if(iLevel==getCategoryLevel() - iDesiredLevel){
				return parentCategory;
			}
		}
		return null;

	}

	public void setHighlighted(boolean blnHighlighted){

		if(AppUtils.getWiggleVersion().equals(AppUtils.APP_VERSION.VERSION_3_APP_WIDE_SEARCH)) {
			LinearLayout navBrowserCategory_button = (LinearLayout) getAdapterView().findViewById(R.id.navBrowserCategory_button);
			navBrowserCategory_button.setSelected(blnHighlighted);
		}

		//Do no longer do this (Wiggle design decision)
		/*
		if(getAdapterView()!=null && getCategoryLevel()>=CONST_NAV_BROWSER_LEVEL_1) {
			TextView navBrowserCategory_tvLevelOneButton = (TextView) getAdapterView().findViewById(R.id.navBrowserCategory_tvLevelOneButton);
			navBrowserCategory_tvLevelOneButton.setSelected(blnHighlighted);
			TextView navBrowserCategory_tvLevelChildButton = (TextView) getAdapterView().findViewById(R.id.navBrowserCategory_tvLevelChildButton);
			navBrowserCategory_tvLevelChildButton.setSelected(blnHighlighted);
		}
		*/
	}




	public void toggleChildVisibility(){


		//Get child container from category adapter view
		View parentView = getAdapterView();
		final LinearLayout navigationItem_lytChildren = (LinearLayout) parentView.findViewById(R.id.navigationItem_lytChildren);
        final ImageView navigationItem_ivMore = (ImageView) parentView.findViewById(R.id.navigationItem_ivMore);

		//We made it to here? Toggle visibility ..
		if (navigationItem_lytChildren.getVisibility() == View.INVISIBLE
				|| navigationItem_lytChildren.getVisibility() == View.GONE
				) {

			ObjectAnimator.ofFloat(navigationItem_ivMore, "rotation", 0, -180).start();
            //AppAnimations.getInstance().doCircleReveal(navigationItem_lytChildren, AppAnimations.CENTER, AppAnimations.MIN, 2);
			AppAnimations.getInstance().expand(navigationItem_lytChildren);


		} else {

            ObjectAnimator.ofFloat(navigationItem_ivMore, "rotation", -180, 0).start();
			//AppAnimations.getInstance().doCircleCollapse(navigationItem_lytChildren, AppAnimations.CENTER, AppAnimations.MIN, 2);
			AppAnimations.getInstance().collapse(navigationItem_lytChildren);

		}

	}

	public int countChildViews(){

		//counts the children on the sub-layout displaying this category's children
		View parentView = getAdapterView();

		//no adapterView? .. report this back
		if(parentView==null){
			return -1;
		}

		final LinearLayout navigationItem_lytChildren = (LinearLayout) parentView.findViewById(R.id.navigationItem_lytChildren);
		if(navigationItem_lytChildren!=null) {
			return navigationItem_lytChildren.getChildCount();
		}

		return 0;

	}


	public ObjNavigationCategory createCopy(ObjNavigationCategory newObject){

		//creates a copy of the current CustomObject so a new object can be created that is NOT
		//the same as this object, but a new object with identical values

		List<String> fields = newObject.getFields();

		// first do the flat fields
		for (String strFieldName : fields) {

			if(this.get(strFieldName) != null){

				int intValueFieldType = newObject.getFieldType(strFieldName);

				switch (intValueFieldType) {

					case CustomObject.FIELD_TYPE_BOOLEAN:
						newObject.setField(strFieldName, this.getBoolean(strFieldName));
						break;

					case CustomObject.FIELD_TYPE_DOUBLE:
						newObject.setField(strFieldName, this.getDouble(strFieldName));
						break;

					case CustomObject.FIELD_TYPE_FLOAT:
						newObject.setField(strFieldName, this.getFloat(strFieldName));
						break;

					case CustomObject.FIELD_TYPE_INTEGER:
						newObject.setField(strFieldName, this.getInt(strFieldName));
						break;

					case CustomObject.FIELD_TYPE_LONG:
						newObject.setField(strFieldName, this.getLong(strFieldName));
						break;

					case CustomObject.FIELD_TYPE_STRING:
						newObject.setField(strFieldName, this.getString(strFieldName));
						break;

					case CustomObject.FIELD_TYPE_OBJECT:
						break;

					case CustomObject.FIELD_TYPE_DATETIME:
						break;

					default:
						break;

				}


			}

		}

		//add all children
		newObject.setChildren(this.getAllChildren());
		newObject.setAvailability(this.getAvailability());
		newObject.setParent(this.getParent());

		return newObject;

	}


	public String getVersion2GATag() {

		//default - return display name. TODO: return English string
		String strTag = getDisplayName();

		String strMenuItemTarget = getString(FIELD_MENU_ITEM_TARGET);

		if(NewsfeedActivity.getLogTag().equals(strMenuItemTarget)){
			return "News";
		}

		String strGTMID = getString(FIELD_GOOGLE_TAG_MANAGER_ID);

		if("shopBuyerGuides".equals(strGTMID)){
			return "Buyers Guides"; //TODO: confirm this
		}else if("shopSale".equals(strGTMID)){
			return "Sale";
		}else if("shopNew".equals(strGTMID)){
			return "New";
		}

		//we get to here? return default value
		return strTag;

	}

	public String getVersion2GATagLevel(){

		/* Bespoke method only to be used for GA Tagging */
		int intCategoryLevel = getCategoryLevel();

        switch (intCategoryLevel) {

			case 2: //2: for some top level local categories such as "Sale", "News", report "Tier 1" for those as they are displayed on the same level as items of level CONST_NAV_BROWSER_LEVEL_1
			case CONST_NAV_BROWSER_LEVEL_1: //3: for high level wiggle categories under "Shop by department" (Cycle, Run, Swim)
            	return "Tier 1";
			case CONST_NAV_BROWSER_LEVEL_2:
				return "Tier 2";
			case CONST_NAV_BROWSER_LEVEL_3:
				return "Tier 3";

			default:
				return "";

		}

	}

	public List<ObjNavigationCategory> getSortedChildren() {

		ArrayList<ObjNavigationCategory> lstCategories = new ArrayList<ObjNavigationCategory>();
		lstCategories.addAll(mChildren);

		//sort list ascending
		SortByDisplayName sortByDisplayName = new SortByDisplayName();
		Collections.sort(lstCategories, sortByDisplayName);
		return lstCategories;


	}

	class SortByDisplayName implements Comparator<ObjNavigationCategory > {

		@Override
		public int compare(ObjNavigationCategory  navCategory1, ObjNavigationCategory  navCategory2) {


			String strDisplayName1 = navCategory1.getDisplayName();
			String strDisplayName2 = navCategory2.getDisplayName();

			//Sort by display name ascending
			return strDisplayName1.compareTo(strDisplayName2);


		}

	}

	public void showMoreChildrenIndicator(View vShowMoreChildren){

		if(hasNoChildren()){
			vShowMoreChildren.setVisibility(View.GONE);
		}else{
			vShowMoreChildren.setVisibility(View.VISIBLE);
		}
	}

	public boolean isStaticCategory(){

		if(CONST_CATEGORY_TYPE_LINK.equals(getString(ObjNavigationCategory.FIELD_TYPE))
				|| CONST_CATEGORY_TYPE_HEADER.equals(getString(ObjNavigationCategory.FIELD_TYPE))
				|| CONST_CATEGORY_TYPE_MENU_ITEM.equals(getString(ObjNavigationCategory.FIELD_TYPE))
		)
		{
			return true;
		}else{
			return false;
		}

	}

	public boolean hasNoChildren(){

		if(CONST_CATEGORY_TYPE_LINK.equals(getString(ObjNavigationCategory.FIELD_TYPE))
				|| CONST_CATEGORY_TYPE_HEADER.equals(getString(ObjNavigationCategory.FIELD_TYPE))
				|| CONST_CATEGORY_TYPE_MENU_ITEM.equals(getString(ObjNavigationCategory.FIELD_TYPE))
				|| isConfirmedDeadEnd()
		)
		{
			return true;
		}else{
			return false;
		}

	}

}