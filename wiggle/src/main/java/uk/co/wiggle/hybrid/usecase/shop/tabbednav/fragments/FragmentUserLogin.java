package uk.co.wiggle.hybrid.usecase.shop.tabbednav.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.SwitchCompat;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.yakivmospan.scytale.Crypto;
import com.yakivmospan.scytale.Options;
import com.yakivmospan.scytale.Store;

import javax.crypto.SecretKey;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjApiRequest;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjHoehle;
import uk.co.wiggle.hybrid.application.helpers.AppGlobalConstants;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentSelfCloseListener;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentUI;
import uk.co.wiggle.hybrid.tagging.AppTagManager;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.activities.TabbedHybridActivity;

/**
 * @author Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 *
 * @license The code of this Application is licensed to Wiggle UK Ltd.
 * The license and its ownership rights can not be transferred to a Third Party.
 *
 * @android If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 **/


public class FragmentUserLogin extends Fragment implements IAppFragmentUI {

    private static final String logTag = "FragmentUserLogin";

    public static String getLogTag(){
        return logTag;
    }


    EditText userLogin_etEmail;
    EditText userLogin_etPassword;
    TextView userLogin_tvRememberMyDetails;
    SwitchCompat userLogin_swRememberMyDetails;
    TextView userLogin_tvForgotPassword;
    ProgressBar userLogin_pbLoginInProgress;
    TextView userLogin_tvLogin;
    TextView userLogin_tvOr;
    TextView userLogin_tvRegister;
    ImageView userLogin_ivClearPassword;
    ImageView userLogin_ivClearEmail;


    IAppFragmentSelfCloseListener selfCloseListener;


    String strCaller = "";
    private static FragmentUserLogin myInstance;

    public static FragmentUserLogin instance(){
        return myInstance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myInstance = this;


        /** Getting the arguments to the Bundle object */
        Bundle data = getArguments();

        // Get the extras
        if (!data.containsKey(AppGlobalConstants.CALLING_ACTIVITY)) {
            throw new IllegalArgumentException("The " + AppGlobalConstants.CALLING_ACTIVITY + " must be passed as extra for the intent. "
                    + "The content of the extras is " + data);
        }

        strCaller = data.getString(AppGlobalConstants.CALLING_ACTIVITY);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.my_account_login, container, false);


        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

       updateFragmentContent(view);

    }


    @Override
    public void updateFragmentContent(View view) {


        findViews(view);

        setCustomFontTypes();


    }



    public void findViews(View view) {

        userLogin_etEmail = (EditText) view.findViewById(R.id.userLogin_etEmail);
        userLogin_etPassword = (EditText) view.findViewById(R.id.userLogin_etPassword);
        userLogin_tvRememberMyDetails = (TextView) view.findViewById(R.id.userLogin_tvRememberMyDetails);
        userLogin_swRememberMyDetails = (SwitchCompat) view.findViewById(R.id.userLogin_swRememberMyDetails);
        //add check change listener so we can send Analytics events
        userLogin_swRememberMyDetails.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                AppTagManager.instance().sendV2EventTags("swipeclick","App - signin","Main","Remember my details");
            }
        });


        userLogin_tvForgotPassword = (TextView) view.findViewById(R.id.userLogin_tvForgotPassword);
        userLogin_pbLoginInProgress = (ProgressBar) view.findViewById(R.id.userLogin_pbLoginInProgress);
        userLogin_tvLogin = (TextView) view.findViewById(R.id.userLogin_tvLogin);
        userLogin_tvOr = (TextView) view.findViewById(R.id.userLogin_tvOr);
        userLogin_tvRegister = (TextView) view.findViewById(R.id.userLogin_tvRegister);
        userLogin_ivClearPassword = (ImageView) view.findViewById(R.id.userLogin_ivClearPassword);
        userLogin_ivClearEmail = (ImageView) view.findViewById(R.id.userLogin_ivClearEmail);

        updateWidgets();

    }

    public void updateWidgets(){

        userLogin_etEmail.setText(ObjAppUser.instance().getString(ObjAppUser.FIELD_EMAIL));
        //this will read the user's encrypted password (stored in FIELD_PASSWORD)
        // and populate the EditText with the original password (to avoid re-encryption of already encrypted string)
        userLogin_etPassword.setText(ObjAppUser.instance().getPassword());

    }


    private void setCustomFontTypes(){

        userLogin_etEmail.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLogin_etPassword.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLogin_tvRememberMyDetails.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLogin_swRememberMyDetails.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLogin_tvForgotPassword.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLogin_tvLogin.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLogin_tvOr.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLogin_tvRegister.setTypeface(AppTypefaces.instance().fontMainRegular);

    }



    @Override
    public void onStart() {
        super.onStart();

        setTitleBarText();

    }

    @Override
    public void onResume() {

        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);


    }



    //Check if this is working - this is the new version of deprecated
    //public void onAttach(Activity activity)
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            selfCloseListener = (IAppFragmentSelfCloseListener) context;
        } catch (ClassCastException e) {
            throw new RuntimeException(getActivity().getClass().getName() + " must implement IAppFragmentSelfCloseListener to use this fragment", e);
        }

    }


    public void onClickEvent(int intClickedViewId){

       switch (intClickedViewId) {

            case R.id.userLogin_ivClearEmail:
                userLogin_etEmail.setText("");
                break;

           case R.id.userLogin_ivClearPassword:
               userLogin_etPassword.setText("");
               break;

            case R.id.userLogin_btnLogin:
                AppTagManager.instance().sendV2EventTags("linkclick","App - signin","Main","Sign In");
                submitLogin();
                break;

            //go to target web forms for non-native user cases
            case R.id.userLogin_btnRegister:
                AppTagManager.instance().sendV2EventTags("linkclick","App - signin","Main","Register");
                goToWebForm(intClickedViewId);
                break;
           case R.id.userLogin_tvForgotPassword:
                AppTagManager.instance().sendV2EventTags("linkclick","App - signin","Main","Forgot Password");
                goToWebForm(intClickedViewId);
                break;


            default:
                break;

        }

    }




    public void onLoginResponseReceived(){

        userLogin_pbLoginInProgress.setVisibility(View.GONE);
        userLogin_tvLogin.setText(getString(R.string.login_tvLogin));
    }


    private void submitLogin(){

        //Check if a "reserved" login was used to access hidden app features
        if(isReservedLogin()){
            return;
        }

        ObjAppUser loginUser = isLoginValid();
        if(loginUser==null){
            return;
        }

        submitEmailLogin(loginUser);


    }


    private ObjAppUser isLoginValid(){

        View invalidView = null;
        ObjAppUser loginUser = new ObjAppUser(); //temporary user to pass login information to request

        //save setting of "Remember my credentials" switch
        String strRememberCredentials = AppUtils.getParameterFromSwitch(userLogin_swRememberMyDetails);
        ObjAppUser.instance().setField(ObjAppUser.FIELD_REMEMBER_CREDENTIALS, strRememberCredentials);

        //Password validation
        String strPasswordValidationResult = AppUtils.validatePassword(userLogin_etPassword);
        if(strPasswordValidationResult!=null){
            invalidView = userLogin_etPassword;
            AppUtils.flagWidgetDataStatus(userLogin_etPassword, false, strPasswordValidationResult);
        }else{
            AppUtils.flagWidgetDataStatus(userLogin_etPassword, true, strPasswordValidationResult);

            //We are using a temporary user object for request (loginUser)
            //We are encrypting the password the user typed in
            Crypto crypto = new Crypto(Options.TRANSFORMATION_SYMMETRIC);
            Store store = new Store(ApplicationContextProvider.getContext());
            SecretKey key = store.getSymmetricKey("pwkey", null);
            String strEncryptedPassword = crypto.encrypt(userLogin_etPassword.getText().toString(), key);
            loginUser.setField(ObjAppUser.FIELD_PASSWORD, strEncryptedPassword);

            //only persist password if user wanted to save his credentials (we are saving the encrypted password only)
            if(ObjAppUser.instance().getBoolean(ObjAppUser.FIELD_REMEMBER_CREDENTIALS)){
                ObjAppUser.instance().setField(ObjAppUser.FIELD_PASSWORD, strEncryptedPassword);
            }

        }

        //Email Mandatory
        String strEmailValidationResult = AppUtils.validateEmail(userLogin_etEmail);
        if(strEmailValidationResult!=null){
            invalidView = userLogin_etEmail;
            AppUtils.flagWidgetDataStatus(userLogin_etEmail, false, strEmailValidationResult);
        }else{
            AppUtils.flagWidgetDataStatus(userLogin_etEmail, true, strEmailValidationResult);
            //temporary user object for requst
            loginUser.setField(ObjAppUser.FIELD_EMAIL, userLogin_etEmail.getText().toString());
            //only persist email if user wanted to save his credentials
            if(ObjAppUser.instance().getBoolean(ObjAppUser.FIELD_REMEMBER_CREDENTIALS)) {
                ObjAppUser.instance().setField(ObjAppUser.FIELD_EMAIL, userLogin_etEmail.getText().toString());
            }
        }

        if(invalidView==null){
            return loginUser;
        }else{
            invalidView.requestFocus();
            return null;
        }


    }


    private void submitEmailLogin(ObjAppUser loginUser){

        userLogin_pbLoginInProgress.setVisibility(View.VISIBLE);
        userLogin_tvLogin.setText(getString(R.string.login_btnLogin_inProgress));

        new ObjApiRequest(getActivity(),
                null,
                AppRequestHandler.getAuthenticationEndpoint(),
                Request.Method.POST,
                loginUser, //temporary login user as opposed to application user
                null,
                AppRequestHandler.ON_RESPONSE_ACTION.RETRIEVE_USER_RELATED_DATA
        ).submit();

    }



    private void goToWebForm(int intPressedViewId){

        TabbedHybridActivity.instance().loadUrlForViewInWebViewFragment(intPressedViewId);

    }

    @Override
    public void onHiddenChanged(boolean hidden){

        if(!hidden){
            setTitleBarText();
        }
    }

    private void setTitleBarText(){

        if(TabbedHybridActivity.instance()!=null){
            Bundle data = getArguments();

            // Get the extras
            if (data.containsKey(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE)) {

                String strTitle = data.getString(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE);
                TabbedHybridActivity.instance().setBreadcrumbText(strTitle);

            }

        }


    }

    private boolean isReservedLogin(){

        String strEmail = userLogin_etEmail.getText().toString();

        ObjHoehle h = new ObjHoehle();
        String strLoginToTestServer = h.getValue(ObjHoehle.FIELD_AM);
        String strClearTestServer = h.getValue(ObjHoehle.FIELD_AN);

        if("contentfulpreview".equals(userLogin_etEmail.getText().toString())
                ||
                "classicnavigation".equals(userLogin_etEmail.getText().toString())
                ){

            ObjAppUser.instance().setField(ObjAppUser.FIELD_FORENAME, strEmail);
            ObjAppUser.instance().setField(ObjAppUser.FIELD_EMAIL, strEmail);
            ObjAppUser.instance().setField(ObjAppUser.FIELD_ACCESS_TOKEN, strEmail); //to make user appeared "logged in"
            ObjAppUser.instance().saveToSharedPreferences();

            TabbedHybridActivity.instance().restartApp();

            return true;

        }else if(strLoginToTestServer.toLowerCase().equals(userLogin_etEmail.getText().toString().toLowerCase())){

            //start dialog to define test server
            showTestServerConfigDialog();
            return true;

        }else if(strClearTestServer.toLowerCase().equals(userLogin_etEmail.getText().toString().toLowerCase())){

            //disconnect from test server - revert to live server
            String strTestServer = ObjAppUser.instance().getString(ObjAppUser.FIELD_TEST_SERVER_URL);
            AppUtils.showCustomToast("Restarting to disconnect from " + strTestServer, false);
            ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_TEST_SERVER_URL, "");
            TabbedHybridActivity.instance().restartApp();
            return true;

        }

        return false;
    }

    private void showTestServerConfigDialog(){

        ObjHoehle h = new ObjHoehle();
        String strDialogTitle = h.getValue(ObjHoehle.FIELD_AP);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(strDialogTitle);

        // Set up the input
        final EditText input = new EditText(getActivity());
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        String strTestServerExample = h.getValue(ObjHoehle.FIELD_AO);
        input.setText(strTestServerExample);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton(getString(R.string.str_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strTestServer = input.getText().toString();
                AppUtils.showCustomToast("Restarting to connect to " + strTestServer, false);
                ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_TEST_SERVER_URL, strTestServer);
                TabbedHybridActivity.instance().restartApp();
            }
        });
        builder.setNegativeButton(getString(R.string.str_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }
}


