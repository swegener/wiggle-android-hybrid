package uk.co.wiggle.hybrid.usecase.shop.version3.activities;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Browser;
import android.provider.Settings;

import com.android.volley.Request;
import com.google.android.material.appbar.AppBarLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request.Method;
import com.salesforce.marketingcloud.MarketingCloudSdk;
import com.salesforce.marketingcloud.messages.iam.InAppMessage;
import com.salesforce.marketingcloud.messages.iam.InAppMessageManager;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler.ON_RESPONSE_ACTION;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.animations.AppAnimations;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjNavigationCategoryManager;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjRecentSearchesManager;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjSalesOrderManager;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjApiRequest;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNavigationCategory;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesOrder;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjTFA;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjTypeaheadResponse;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjUserSearch;
import uk.co.wiggle.hybrid.application.fragments.Dialog;
import uk.co.wiggle.hybrid.application.fragments.Dialog.DialogActionListener;
import uk.co.wiggle.hybrid.application.helpers.AppFragmentHelper;
import uk.co.wiggle.hybrid.application.helpers.AppGlobalConstants;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.application.persistence.DataStore;
import uk.co.wiggle.hybrid.application.session.AppSession;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.application.utils.DateUtils;
import uk.co.wiggle.hybrid.interfaces.IAppActivityUI;
import uk.co.wiggle.hybrid.interfaces.IAppApiResponse;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentSelfCloseListener;
import uk.co.wiggle.hybrid.tagging.AppTagManager;
import uk.co.wiggle.hybrid.usecase.newsfeed.activities.NewsfeedActivity;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.activities.Alert;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.fragments.FragmentMyAccount;
import uk.co.wiggle.hybrid.usecase.shop.version3.adapters.V3FragmentBackStackAdapter;
import uk.co.wiggle.hybrid.usecase.shop.version3.adapters.V3TabbedTextSearchListAdapter;
import uk.co.wiggle.hybrid.usecase.shop.version3.fragments.V3FragmentOrderTracking;
import uk.co.wiggle.hybrid.usecase.shop.version3.fragments.V3FragmentShopNavigation;
import uk.co.wiggle.hybrid.usecase.shop.version3.fragments.V3FragmentUserLoginMFA;
import uk.co.wiggle.hybrid.usecase.shop.version3.fragments.V3FragmentWebShop;
import uk.co.wiggle.hybrid.usecase.startup.activities.UpdateRequiredActivity;
import uk.co.wiggle.hybrid.usecase.startup.activities.WelcomeActivity;

import static android.os.Build.VERSION_CODES.LOLLIPOP;


/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/


@SuppressLint("NewApi") public class V3HybridActivity extends AppCompatActivity
        implements IAppActivityUI, IAppApiResponse, OnClickListener,
		DialogActionListener, IAppFragmentSelfCloseListener, AppBarLayout.OnOffsetChangedListener {


	//allow other activities to read an instance of MainActivity so that MainActivity methods can be called
	private static V3HybridActivity myInstance;

	public static V3HybridActivity instance() {
		return myInstance;
	}


	private static final String logTag = "V3HybridActivity";

	public static String getLogTag() {
		return logTag;
	}

	/* Constants used to pass information from other screens */
	public static String WEB_VIEW_CALLER = "WEB_VIEW_CALLER";
	public static String LOAD_TARGET_URL_FROM_NATIVE_SCREEN = "LOAD_TARGET_URL_FROM_NATIVE_SCREEN";
	public static String BREADCRUMB_TEXT = "BREADCRUMB_TEXT";


	//Widgets
	AppBarLayout main_lytTopElements;
	boolean blnAppBarFullyExpanded = false;

	TextView main_tvBreadcrumb;
	TextView main_tvInfo;
	RelativeLayout lytPleaseWaitDark;
	ImageView pleaseWaitDark_ivPleaseWait;
	TextView pleaseWaitDark_tvPleaseWait;
	ProgressBar main_pbLoadProgress;
	View main_headerDivider;
	LinearLayout nativeSearchBar;
	RelativeLayout navBar_btnBack;

	//App-wide search
	EditText nativeSearch_etQueryString;
	ImageView nativeSearch_ivTextSearchClear;
	RelativeLayout nativeSearch_lytResult;
	AbsListView nativeSearch_lvSearchResult;
	LinearLayout nativeSearch_dynamicHeader;
	TextView textSearch_tvGroupHeader;
	TextView nativeSearch_tvCancel;
	TextView nativeSearch_tvCloseScreen;
	TextView tvBreadcrumb;

	//Native search
	V3TabbedTextSearchListAdapter mTextSearchListAdapter;
	UpdateTextSearchListviewTask mUpdateTextSearchListviewTask;

	//Fragment back stack (for debugging)
	RelativeLayout navBar_btnBackStack;
	LinearLayout lytFragmentBackStack;
	AbsListView fragmentBackStack_lvFragments;
	V3FragmentBackStackAdapter mFragmentBackStackAdapter;
	UpdateFragmentDebugListTask mUpdateFragmentDebugListTask;

	//SalesForce listener for in-app messages
	InAppMessageManager.EventListener myInAppListener;


	//defines the category order in which we want our typeahead search result returned
	ArrayList<String> mTypeadheadSortOrder = new ArrayList<String>(
			Arrays.asList(ObjTypeaheadResponse.CONST_RESULT_TYPE_PRODUCT,
					ObjTypeaheadResponse.CONST_RESULT_TYPE_BRAND,
					ObjTypeaheadResponse.CONST_RESULT_TYPE_HELP,
					ObjTypeaheadResponse.CONST_RESULT_TYPE_CONTENT)
	);

	List<ObjTypeaheadResponse> mLastKnownTypeaheadResponses = new ArrayList<ObjTypeaheadResponse>();
	String mDeviceLanguage;
	int intMinCharsForTextWatcher; //character minimum for text watcher


	//Used for order tracking caching
	boolean blnAcceptResponseIntoCacheOnSwipeRefresh = false;

	public void setAcceptOrderTrackingResponseOnSwipeRefresh(boolean blnValue) {
		blnAcceptResponseIntoCacheOnSwipeRefresh = blnValue;
	}

	//Used in case of country changes in MyAccount - if this boolean is set and the fragment
	//manager back stack is cleared, a fresh "home" fragment will be added
	boolean blnAddHomeAfterBackstackCleared = false;

	public void setAddHomeFragmentOnBackStackClearedListener() {
		blnAddHomeAfterBackstackCleared = true;
		showPleaseWait(getString(R.string.main_tvPleaseWait));
	}


	//Tabbed navigation bar
	LinearLayout hybridAcitivtyNavigationTab;
	RelativeLayout navigation_Home;
	ImageView navigation_ivHome;
	RelativeLayout navigation_Search;
	ImageView navigation_ivSearch;
	RelativeLayout navigation_Basket;
	ImageView navigation_ivBasket;
	RelativeLayout navigation_Wishlist;
	ImageView navigation_ivWishlist;
	RelativeLayout navigation_Account;
	ImageView navigation_ivAccount;
	TextView navigation_tvAccountBadge;
	TextView navBar_tvBasketCounter;
	TextView navBar_tvAccountNewInfoAlert;

	//Fragment container and manager
	FrameLayout main_fragmentContainer;
	FragmentManager fm;

	FrameLayout main_navigationFragmentContainer;


	//Last URL user tried before login was required
	String strLastUserRequestedURL;

	//Reference to search menu item
	MenuItem actionBarSearchMenuItem;

	//Used to store the actual FragmentWebShop instance that called the login screen
	Fragment mLoginScreenCaller = null;

	public void setLoginScreenCaller(Fragment callingFragment) {
		mLoginScreenCaller = callingFragment;
	}

	//URL used to navigate to unsupported stores in external browser
	String strUnsupportedStoreHost = "";

	//used to force clearing history when user returns to "Home" after deep-navigation
	boolean blnForceClearHistory = false;

	public void setForceClearHistory(boolean blnValue) {
		blnForceClearHistory = blnValue;
	}


	boolean blnIsDisplayedToUser = false;

	//For deep links
	String strDeepLinkTargetFragmentName = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addSalesForceInAppMessageListener();

		//No title window
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		overridePendingTransition(AppUtils.getActivityOpenAnimation(), android.R.anim.fade_out);

		setContentView(R.layout.v3_hybrid_activity);

		myInstance = this;

		//perform any upgrade relevant code
		executeCodeForVersionUpgrade();

		setupCustomActionBar();

		initialiseSupportFragmentManager();

		findViews();

		//retriever order data if user is logged in
		if (ObjAppUser.instance().isUserLoggedIn()) {
			AppRequestHandler.instance().retrieveOrderData(this, ON_RESPONSE_ACTION.SUBMIT_ORDER_TRACKING_REQUEST);
		}

		//Activity was opened by user or any other mechanism
		//Add Home webview & initialise tab
		setNavigationTabSelected(navigation_Home);

		//Check for deep or app linking
		boolean blnIsDeepOrAppLink = scanForDeepLinking() || scanForAppLinking();

		if (!blnIsDeepOrAppLink) {
			clearWebViewHistory();
			//no deep or app link (i.e. standard start of app via homescreen) - show home page
			addWebShopFragment(true, AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home_https)));
		}

		AppTagManager.instance().sendVersion2ScreenView("App - Shop");

		checkForRequiredAppUpgrade();

	}


	public void updateShoppingCartCounter(String strShoppingCartCounter) {

		//prevent NPE
		if (navBar_tvBasketCounter == null) {
			return;
		}

		if (strShoppingCartCounter != null && !"".equals(strShoppingCartCounter) && Integer.parseInt(strShoppingCartCounter) > 0) {
			navBar_tvBasketCounter.setText(strShoppingCartCounter);
			navBar_tvBasketCounter.setVisibility(View.VISIBLE);
		} else {
			navBar_tvBasketCounter.setText(strShoppingCartCounter);
			navBar_tvBasketCounter.setVisibility(View.GONE);
		}

	}


	public void handleNetworkStatusChange(boolean isConnected) {

		// TODO Implement as required

	}

	public void restartApp() {

		/**
		 *  This kills the app process - not very fond of this solution.
		 *  Handler delays restarting the app as we may have to wait for shared preferences to be saved (async)
		 *
		 **/
		appRestartHandler.postDelayed(appRestartRunnable, 800);
	}

	private Handler appRestartHandler = new Handler();

	private Runnable appRestartRunnable = new Runnable() {
		public void run() {
			killAndRestartApp();
		}
	};

	private void killAndRestartApp() {

		/*
			Some native activities use HybridActivity to show content e.g.
				Legal & Insurance (insurance quote)
				Events - event details
				News - some news links

			Just killing and restarting the app with a default intent leads to old back
			stack being shown. We hence restart the app as a completely new task and clear
			the back stack.

		 */
		Intent restartIntent = new Intent(this, WelcomeActivity.class);
		restartIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);


		//With API level 16, we can finish all activities in the current task (even those above the current activity) by just calling ..
		finishAffinity();


		AlarmManager alm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
		alm.set(AlarmManager.RTC, System.currentTimeMillis() + 500, PendingIntent.getActivity(this, 0, restartIntent, 0));
		android.os.Process.killProcess(android.os.Process.myPid());

	}


	@Override
	public void onSaveInstanceState(Bundle savedState) {

		super.onSaveInstanceState(savedState);

	}


	@Override
	public void onRestoreInstanceState(Bundle savedState) {

		super.onRestoreInstanceState(savedState);


	}


	public void showUserAsLoggedInOrOut() {

		//If my Account fragment is shown - update it
		if (FragmentMyAccount.instance() != null
				&& FragmentMyAccount.instance().isAdded()
				&& FragmentMyAccount.instance().getView() != null //make sure onViewCreated was executed
		) {

			FragmentMyAccount.instance().showUserAsLoggedInOrOut();

		}

		//If Navigation fragment is shown - update it
		if (V3FragmentShopNavigation.instance() != null
				&& V3FragmentShopNavigation.instance().isAdded()
				&& V3FragmentShopNavigation.instance().getView() != null //make sure onViewCreated was executed
		) {

			V3FragmentShopNavigation.instance().setAccountWidgetVisibility();

		}

		updateOrderTrackingBadge();

	}

	public void handleInAppLogin() {

		//hide search bar after login
		toggleSearchBar(true);

		showUserAsLoggedInOrOut();
		AppSession.instance().setInAppLogin(false);
		if (mLoginScreenCaller != null) {
			if (mLoginScreenCaller instanceof V3FragmentWebShop) {

				V3FragmentWebShop webShopOrView = ((V3FragmentWebShop) mLoginScreenCaller);

				if (webShopOrView.isWebViewFragment()) {

					V3FragmentWebShop webViewFragment = (V3FragmentWebShop) fm.findFragmentByTag(V3FragmentWebShop.CONST_WEB_VIEW_FRAGMENT);
					if (webViewFragment != null
							&& webViewFragment.isAdded()
					) {
						bringFragmentToTop(webViewFragment.CONST_WEB_VIEW_FRAGMENT);
						webViewFragment.navigateToReturnUrl();
					}

				} else if (webShopOrView.isWebShopFragment()) {

					V3FragmentWebShop webShopFragment = (V3FragmentWebShop) fm.findFragmentByTag(V3FragmentWebShop.CONST_WEB_SHOP_FRAGMENT);
					if (webShopFragment != null
							&& webShopFragment.isAdded()
					) {
						bringFragmentToTop(webShopFragment.CONST_WEB_SHOP_FRAGMENT);
						webShopFragment.navigateToReturnUrl();
					}

				}

			}
			mLoginScreenCaller = null;
		}

	}


	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub

		int id = view.getId();

		switch (id) {

			case R.id.navBar_btnBack:
				onBackPressed();
				break;
			case R.id.navBar_btnMenu:
				toggleAppMenu();
				break;
			case R.id.navBar_btnSearch:
				toggleSearchBar(false);
				break;
			case R.id.navBar_btnShoppingCart:
				navigation_Basket.performClick();
				break;
			case R.id.navBar_ivLogo:
				navigateToHomepage();
				break;
			case R.id.nativeSearch_ivResortTextSearchClear:
				V3FragmentShopNavigation.instance().onClickEvent(id);
				break;
			case R.id.orderDetail_btnTrackOrder:
			case R.id.orderDetail_btnContactUs:
			case R.id.orderDetail_tvReturns:
			case R.id.orderDetail_tvCancellations:
				V3FragmentOrderTracking.instance().onClickEvent(id);
				break;

			case R.id.navigation_Home:
			case R.id.navigation_Search:
			case R.id.navigation_Basket:
			case R.id.navigation_Wishlist:
			case R.id.navigation_Account:
				onNavigationTabPressed(id, true);
				break;

			//handle click events from MyAccount fragment
			case R.id.myAccount_btnLogin:
			case R.id.myAccount_btnRegister:
			case R.id.myAccount_lytTrackMyOrder:
			case R.id.myAccount_tvDetails:
			case R.id.myAccount_tvCurrency:
			case R.id.myAccount_tvDeliveryDestination:
			case R.id.myAccount_tvHelp:
			case R.id.myAccount_tvContactUs:
			case R.id.myAccount_tvAppFeedback:
			case R.id.myAccount_tvChangeCountry:
			case R.id.myAccount_btnLogout:
				FragmentMyAccount.instance().onClickEvent(id);
				break;

			//handle click events from UserLoginMFA fragment
			case R.id.userLoginMFA_btnLogin:
			case R.id.userLoginMFA_btnRegister:
			case R.id.userLoginMFA_cbRememberMyDetails:
			case R.id.userLoginMFA_tvForgotPassword:
			case R.id.userLoginMFA_tvRecoverMyPassword:
			case R.id.userLoginMFA_ivClearEmail:
			case R.id.userLoginMFA_ivClearPassword:
			case R.id.userLoginMFA_tsv_btnLogin:
			case R.id.userLoginMFA_tsv_tvEnterRevoveryCode:
			case R.id.userLoginMFA_recover_btnSubmit:
			case R.id.userLoginMFA_recovered_btnCopy:
			case R.id.userLoginMFA_recovered_cbUserConfirmation:
			case R.id.userLoginMFA_recovered_btnContinue:
			case R.id.userLoginMFA_locked_btnHome:
			case R.id.userLoginMFA_tsv_tvLoginHelp:
			case R.id.userLoginMFA_recover_tvLoginHelp:
				V3FragmentUserLoginMFA.instance().onClickEvent(id);
				break;
			//App wide search
			case R.id.nativeSearch_tvCancel:
			case R.id.nativeSearch_tvCloseScreen:
				onBackPressed();
				break;
			case R.id.nativeSearch_ivTextSearchClear:
				onKeywordTextCleared();
				break;
			//click events from navigation Fragment
			case R.id.navigation_itemAllCategories:
			case R.id.navigation_itemL1Account:
			case R.id.navigation_tvClose:
			case R.id.navigation_btnLogin:
			case R.id.navigation_btnLogout:
			case R.id.navigation_btnRegister:
			case R.id.navigation_itemL2Wishlist:
			case R.id.navigation_itemL2ManageAccount:
			case R.id.navigation_itemL2MyOrders:
			case R.id.navigation_itemL2Currency:
			case R.id.navigation_itemL2DeliveryDestination:
			case R.id.navigation_itemL2Help:
				V3FragmentShopNavigation.instance().onClickEvent(id);
				break;

			case R.id.main_tvBreadcrumb:
				showAppReviewPromptActivity(true);
				//getSessionConfirmationNumber();
				break;

			case R.id.fragmentBackStack_btnClose:
			case R.id.navBar_btnBackStack:
				toggleBackStackList();
				break;
			case R.id.fragmentBackStack_btnRefresh:
				updateFragmentDebugList();
				break;

			default:
				break;
		}

		//on each forward click in our activity - check if we need to show our contextual back-button
		showContextualBackButton();
	}

	@Override
	public void onApiRequestSuccess(ObjApiRequest apiRequest,
									JSONObject apiSuccessDetail) {

		if (AppRequestHandler.getAuthenticationEndpoint().equals(apiRequest.getURLEndpoint())
                || AppRequestHandler.ENDPOINT_VERIFY_MFA.equals(apiRequest.getURLEndpoint())
				|| AppRequestHandler.ENDPOINT_SILENT_LOGIN.equals(apiRequest.getURLEndpoint())
                ) {

			//Close FragmentUserLoginMFA if displayed (only the case on explicit login, not silent log in)
			if (V3FragmentUserLoginMFA.instance() != null
					&& V3FragmentUserLoginMFA.instance().isAdded()) {

				hideVirtualKeyboard();
				V3FragmentUserLoginMFA.instance().onLoginSuccessResponse(apiRequest, apiSuccessDetail);

				//Check if this is an "Account Recovery" usecase
				if(apiRequest.getInfoObject() != null){
					ObjTFA tfaObject = (ObjTFA) apiRequest.getInfoObject();
					if(tfaObject.isAccountRecovery()){
						V3FragmentUserLoginMFA.instance().handleAccountRecovery(apiRequest);
						//important - let this continue to handleInAppLogin()
						//so we show user correctly as logged in / out now
						handleInAppLogin();
						return;
					}
				}

				executePostMFALoginSuccessActions();

			}

			handleInAppLogin();


		} else if (AppRequestHandler.getOrderTrackingEndpoint().equals(apiRequest.getURLEndpoint())) {

			//update order counter if any
			updateOrderTrackingBadge();

			if (ON_RESPONSE_ACTION.RETRIEVE_USER_RELATED_DATA.equals(apiRequest.getOnResponseAction())
			) {

				if (FragmentMyAccount.instance() != null
						&& FragmentMyAccount.instance().isAdded()) {
					FragmentMyAccount.instance().updateOrderTrackingBadge();
				}

			} else {

				//update entire list
				updateOrderTrackingList();

			}

			if (blnAcceptResponseIntoCacheOnSwipeRefresh) {
				blnAcceptResponseIntoCacheOnSwipeRefresh = false;
				AppSession.instance().acceptOrderTrackingPayloadIntoCache();
			}

		} else if (AppRequestHandler.ENDPOINT_UPGRADE_CHECK.equals(apiRequest.getURLEndpoint())) {

			if (AppRequestHandler.ON_RESPONSE_ACTION.SHOW_UPGRADE_REQUIRED_DIALOG.equals(apiRequest.getOnResponseAction())) {
				showUpgradeRequiredMessage();
			}

		}

	}

	public void executePostMFALoginSuccessActions(){

		hideVirtualKeyboard();
		//successful login via MFA or non-MFA enabled account
		//check if login screen needs to redirect elsewhere:
		int intLoginScreenCaller = V3FragmentUserLoginMFA.instance().getLoginScreenCaller();
		//close FragmentUserLogin
		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, V3FragmentUserLoginMFA.instance(), V3FragmentUserLoginMFA.getLogTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.REMOVE, R.anim.bottom_view_slide_out, false);
		performAccountAction(intLoginScreenCaller);
		//set title to visible fragment after removing login fragment
		setTitleForVisibleFragment();

	}

	private void updateOrderTrackingList() {

		if (V3FragmentOrderTracking.instance() != null
				&& V3FragmentOrderTracking.instance().isAdded()
		) {
			V3FragmentOrderTracking.instance().initialiseNoDataText();
			V3FragmentOrderTracking.instance().updateOrderTrackingList();

		}

	}

	@Override
	public void onApiRequestError(ObjApiRequest request,
								  int intHttpResponseCode, JSONObject apiErrorDetail) {

		if (AppRequestHandler.getAuthenticationEndpoint().equals(request.getURLEndpoint())
            || AppRequestHandler.ENDPOINT_VERIFY_MFA.equals(request.getURLEndpoint())
				|| AppRequestHandler.ENDPOINT_SILENT_LOGIN.equals(request.getURLEndpoint())
			) {

			//Notify FragmentUserLoginMFA if displayed (explicit login)
			if (V3FragmentUserLoginMFA.instance() != null
						&& V3FragmentUserLoginMFA.instance().isAdded()
						&& V3FragmentUserLoginMFA.instance().isVisible()) {

					V3FragmentUserLoginMFA.instance().onLoginErrorResponse(request, intHttpResponseCode, apiErrorDetail);

			} else {

				//in case of in-app silent login:
				//any problems with login - show login screen
				AppUtils.showCustomToast("Please log in manually..", true);
				showLoginScreen();

			}

		}

	}

	@Override
	public void setupCustomActionBar() {
		// TODO Auto-generated method stub


	}

	@Override
	public void setCustomFontTypes() {
		// TODO Auto-generated method stub

		main_tvBreadcrumb.setTypeface(AppTypefaces.instance().fontMainBold);
		main_tvInfo.setTypeface(AppTypefaces.instance().fontMainBold);
		pleaseWaitDark_tvPleaseWait.setTypeface(AppTypefaces.instance().fontMainRegular);


		navBar_tvBasketCounter.setTypeface(AppTypefaces.instance().fontMainRegular);
		navigation_tvAccountBadge.setTypeface(AppTypefaces.instance().fontMainRegular);
		navBar_tvAccountNewInfoAlert.setTypeface(AppTypefaces.instance().fontMainRegular);

		//App wide search
		nativeSearch_etQueryString.setTypeface(AppTypefaces.instance().fontMainRegular);
		nativeSearch_etQueryString.setHintTextColor(getResources().getColor(R.color.greyButton));
		textSearch_tvGroupHeader.setTypeface(AppTypefaces.instance().fontMainBold);
		nativeSearch_tvCancel.setTypeface(AppTypefaces.instance().fontMainRegular);
		nativeSearch_tvCloseScreen.setTypeface(AppTypefaces.instance().fontMainRegular);


	}

	@Override
	public void findViews() {

		main_lytTopElements = (AppBarLayout) findViewById(R.id.main_lytTopElements);
		//set a listener so we can track if this is expanded or not
		main_lytTopElements.addOnOffsetChangedListener(this);


		main_tvBreadcrumb = (TextView) findViewById(R.id.main_tvBreadcrumb);
		main_tvInfo = (TextView) findViewById(R.id.main_tvInfo);
		lytPleaseWaitDark = (RelativeLayout) findViewById(R.id.lytPleaseWaitDark);
		pleaseWaitDark_ivPleaseWait = (ImageView) findViewById(R.id.pleaseWaitDark_ivPleaseWait);
		pleaseWaitDark_tvPleaseWait = (TextView) findViewById(R.id.pleaseWaitDark_tvPleaseWait);
		main_pbLoadProgress = (ProgressBar) findViewById(R.id.main_pbLoadProgress);
		main_headerDivider = findViewById(R.id.main_headerDivider);
		nativeSearchBar = findViewById(R.id.nativeSearchBar);
		navBar_btnBack = findViewById(R.id.navBar_btnBack);


		//Tabbed navigation bar
		hybridAcitivtyNavigationTab = (LinearLayout) findViewById(R.id.hybridAcitivtyNavigationTab);
		navigation_Home = (RelativeLayout) findViewById(R.id.navigation_Home);
		navigation_ivHome = (ImageView) findViewById(R.id.navigation_ivHome);
		navigation_Search = (RelativeLayout) findViewById(R.id.navigation_Search);
		navigation_ivSearch = (ImageView) findViewById(R.id.navigation_ivSearch);
		navigation_Basket = (RelativeLayout) findViewById(R.id.navigation_Basket);
		navigation_ivBasket = (ImageView) findViewById(R.id.navigation_ivBasket);
		navigation_Wishlist = (RelativeLayout) findViewById(R.id.navigation_Wishlist);
		navigation_ivWishlist = (ImageView) findViewById(R.id.navigation_ivWishlist);
		navigation_Account = (RelativeLayout) findViewById(R.id.navigation_Account);
		navigation_ivAccount = (ImageView) findViewById(R.id.navigation_ivAccount);
		navBar_tvBasketCounter = (TextView) findViewById(R.id.navBar_tvBasketCounter);
		navigation_tvAccountBadge = (TextView) findViewById(R.id.navigation_tvAccountBadge);
		navBar_tvAccountNewInfoAlert = (TextView) findViewById(R.id.navBar_tvAccountNewInfoAlert);

		//fragment container
		main_fragmentContainer = (FrameLayout) findViewById(R.id.main_fragmentContainer);
		main_navigationFragmentContainer = (FrameLayout) findViewById(R.id.main_navigationFragmentContainer);

		//App-wide search
		nativeSearch_etQueryString = (EditText) findViewById(R.id.nativeSearch_etQueryString);
		nativeSearch_ivTextSearchClear = (ImageView) findViewById(R.id.nativeSearch_ivTextSearchClear);
		nativeSearch_lytResult = (RelativeLayout) findViewById(R.id.nativeSearch_lytResult);
		nativeSearch_lvSearchResult = (AbsListView) findViewById(R.id.nativeSearch_lvSearchResult);
		nativeSearch_dynamicHeader = (LinearLayout) findViewById(R.id.nativeSearch_dynamicHeader);
		textSearch_tvGroupHeader = (TextView) findViewById(R.id.textSearch_tvGroupHeader);
		nativeSearch_tvCancel = (TextView) findViewById(R.id.nativeSearch_tvCancel);
		nativeSearch_tvCloseScreen = (TextView) findViewById(R.id.nativeSearch_tvCloseScreen);
		tvBreadcrumb = (TextView) findViewById(R.id.tvBreadcrumb);
		setupNativeSearchWidgets();

		//Fragment back stack (debugging)
		navBar_btnBackStack = (RelativeLayout) findViewById(R.id.navBar_btnBackStack);
		lytFragmentBackStack = (LinearLayout) findViewById(R.id.lytFragmentBackStack);
		fragmentBackStack_lvFragments = (AbsListView) findViewById(R.id.fragmentBackStack_lvFragments);

		if (AppUtils.showFragmentDebugList()) {
			navBar_btnBackStack.setVisibility(View.VISIBLE);
			lytFragmentBackStack.setVisibility(View.VISIBLE);
		} else {
			navBar_btnBackStack.setVisibility(View.GONE);
			lytFragmentBackStack.setVisibility(View.GONE);
		}

		updateWidgets();

	}

	private void updateWidgets() {

		showUserAsLoggedInOrOut();

		setCustomFontTypes();

		showAdditionalInfo();

		mTextSearchListAdapter = new V3TabbedTextSearchListAdapter();
		nativeSearch_lvSearchResult.setAdapter(mTextSearchListAdapter);
		setListViewScrollAdapter();

		//load recent searches on search screen
		ObjRecentSearchesManager.getInstance().loadPersistedData();

		//show search bar
		nativeSearchBar.setVisibility(View.VISIBLE);

		//fragment list for debugging
		mFragmentBackStackAdapter = new V3FragmentBackStackAdapter();
		fragmentBackStack_lvFragments.setAdapter(mFragmentBackStackAdapter);


	}


	@Override
	public void onResume() {

		super.onResume();

		//load local navigation tree
		loadLocalNavigationTreeIfMissing();

		myInstance = this; //repeated after onCreate - prevent NPE after app in background

	}

	@Override
	protected void onPause() {
		super.onPause();
		blnIsDisplayedToUser = false;
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);

		if (hasFocus) {


		}

	}


	@Override
	public void onBackPressed() {

		navigationCanGoBack(true);

		//After any back-press - we might need to show/hide the contextual back button
		showContextualBackButton();


	}

	private boolean navigationCanGoBack(boolean blnBackWasPressed) {


		AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_general), "generalBack");

		//Check if app search is shown
		if (nativeSearch_lytResult.getVisibility() == View.VISIBLE) {
			if (blnBackWasPressed) {
				toggleSearchBar(true);
			}
			return false;
		}

		//Check if navigation screen is shown
		if (main_navigationFragmentContainer.getVisibility() == View.VISIBLE) {
			if (blnBackWasPressed) {
				hideNavigationScreenIfDisplayed();
			}
			return false;
		}

		//special handling of back-press depending on fragment shown
		if (handleFragmentBackNavigation(blnBackWasPressed)) {
			return true;
		}

		//If we get to here - close app
		if (blnBackWasPressed) {

			/**
			 alertUser(
			 0,
			 "",
			 getString(R.string.str_place_an_ad_dialog_title),
			 getString(R.string.str_place_an_ad_dialog_message),
			 Alert.OK_GO_BACK);
			 **/
			AppUtils.showCustomToast("Nothing more to go back to...", true);


			//clear order manager
			ObjSalesOrderManager.getInstance().clear();

			//clear any session parameters
			AppSession.instance().setAndroidNativeCategory(null);
			AppSession.instance().setInAppLogin(false);

			//clear SalesForce in-App message listener
			MarketingCloudSdk.requestSdk(new MarketingCloudSdk.WhenReadyListener() {
				@Override
				public void ready(MarketingCloudSdk marketingCloudSdk) {
					Logger.printMessage(logTag, "Removing myInAppListener", Logger.INFO);
					myInAppListener = null;
					marketingCloudSdk.getInAppMessageManager().setInAppMessageListener(myInAppListener);
					finish();
				}
			});

			//Moved to SalesForce SDK as we need to wait for this ...
			//finish();
		}

		return false;

	}


	public void loadUrlInExternalBrowser(String strTargetUrl) {

		Bundle bundle = new Bundle();

		/* You can add additional header information as follows - but only do this if it can't be achieved in webview.setSettings()  */
		//bundle.putString(key, value);

		Intent externalBrowserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(strTargetUrl));
		externalBrowserIntent.putExtra(Browser.EXTRA_HEADERS, bundle);
		startActivity(externalBrowserIntent);

	}

	public void loadUrlInWebViewFragment(String strTargetUrl) {

		hideNavigationScreenIfDisplayed();

		/* Use this method if a URL needs to be opened in the WebView fragment */

		addWebViewFragment(strTargetUrl, true);

	}

	public void loadUrlForViewInWebViewFragment(int intCallingViewId) {

		hideNavigationScreenIfDisplayed();

		/* Use this method if a URL linked to a specific calling view
			needs to be opened in the WebView fragment */

		addWebViewFragment(intCallingViewId, false);

	}

	public void loadUrlInWebShopFragment(String strTargetUrl) {

		hideNavigationScreenIfDisplayed();

		/* Use this method if a URL needs to be opened in the WebShop fragment */

		addWebShopFragment(false, strTargetUrl);

	}

	public void loadUrlInWebShopFragment(int intCallingViewId) {

		hideNavigationScreenIfDisplayed();

		/* Use this method if a URL needs to be opened in the WebShop fragment */

		addWebShopFragment(false, intCallingViewId);

	}

	public void loadLocalNavigationTreeIfMissing() {

		if (ObjNavigationCategoryManager.getInstance().getLocalNavigationTree() == null) { //indicates app was in background and paused - or just starting now


			//get navigation tree from local store and make sure navigation drawer is updated afterwards
			ObjApiRequest localFileReadRequest =
					new ObjApiRequest(this,
							null,
							AppRequestHandler.LOCAL_NAVIGATION_FILE,
							Method.GET,
							null,
							null,
							ON_RESPONSE_ACTION.REFRESH_NAVIGATION_DRAWER);
			AppRequestHandler.instance().getJsonObjectDataFromRawResource(localFileReadRequest);


		}

	}

	public void loadSearchResultInWebView(String strTargetUrl) {

		hideVirtualKeyboard();
		toggleSearchBar(true);
		loadUrlInWebShopFragment(strTargetUrl);

	}

	public void openCategoryUrl(ObjNavigationCategory category, String strGTMEvent) {

		//send GTM Tag - report "linkclick" as another screen is opened (webview or native screen)
		String strCategoryTag = category.getVersion2GATag();
		String strCategoryTier = category.getVersion2GATagLevel();
		AppTagManager.instance().sendV2EventTags("linkclick", "App - Navigation", strCategoryTier, strCategoryTag);


		//get URL depending on category source / type
		String strCategoryUrl = category.getTargetUrl();

		//build Wiggle shop URL
		String strTargetUrl = AppUtils.getWiggleURL(strCategoryUrl);

		//additional UMT tagging for all categories with a Google Tag Manager event .. amend UTM parameters to URL
		if (strGTMEvent != null && !"".equals(strGTMEvent)) {
			strTargetUrl = AppUtils.appendUtmTag(strTargetUrl, strGTMEvent);
		}


		if (category.getBoolean(ObjNavigationCategory.FIELD_OPEN_IN_EXTERNAL_BROWSER)) {
			loadUrlInExternalBrowser(strTargetUrl);
		} else {
			//load URL in web shop (not web view)
			loadUrlInWebShopFragment(strTargetUrl);
		}

	}

	public void startNewsfeedActivity(ObjNavigationCategory category, String strGTMEvent) {

		//send GTM Tag - report "linkclick" as another screen is opened (webview or native screen)
		if (category != null) {
			String strCategoryTag = category.getVersion2GATag();
			String strCategoryTier = category.getVersion2GATagLevel();
			AppTagManager.instance().sendV2EventTags("linkclick", "App - Navigation", strCategoryTier, strCategoryTag);
		}

		Intent intent = new Intent(this, NewsfeedActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		//intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION); //this makes sure activities do not slide in from right to left when started, but simply appear
		startActivityForResult(intent, 0);


	}


	public void performLogout() {

		AppSession.instance().clearUserSpecificData();
		showUserAsLoggedInOrOut();
		clearLoginCookieOnWebview(); //clear login cookie
		//APP-238: requirement now is to keep the Basket cookie (we need to keep all cookies in this case) after logout
		//So, disable the following. We would prefer to leave this in place and have the Wiggle server handle the basket cookie correctly after logout / relogin
		//clearAllCookiesAfterLogout(); //APP-190 - clear Shopping basket and other cookies after logout
		navigateToHomepage();

	}

	public void navigateToHomepage() {

		toggleSearchBar(true);
		//webViewAutoScrollToTopAndShowHideSearchbar(null, true); --> gets sorted out by V3FragmentWebshop (when webview cant go back)

		//Whenever we go back to home page - check if FragmentWebView is shown and remove if required
		Fragment webViewFragment = fm.findFragmentByTag(V3FragmentWebShop.CONST_WEB_VIEW_FRAGMENT);
		if (webViewFragment != null
				&& webViewFragment.isAdded()) {
			AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, webViewFragment, webViewFragment.getTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.REMOVE, 0, false);
		}

		//Also remove other fragments (events, order tracking)
		//leaving them hidden in the back stack is not always working: we came across an Android bug:
		//fragments had the flag mHidden = true but were visible to the user!
		Fragment orderTrackingFragment = fm.findFragmentByTag(V3FragmentOrderTracking.getLogTag());
		if (orderTrackingFragment != null
				&& orderTrackingFragment.isAdded()) {
			AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, orderTrackingFragment, orderTrackingFragment.getTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.REMOVE, 0, false);
		}

		clearWebViewHistory();
		loadUrlInWebShopFragment(AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home)));
	}

	public void clearLoginCookieOnWebview() {

		if (V3FragmentWebShop.instance() != null
				&& V3FragmentWebShop.instance().isAdded()
		) {
			V3FragmentWebShop.instance().addAdditionalHttpHeaders();
		}
	}


	@SuppressWarnings("deprecation")
	public static void clearAllCookiesAfterLogout() {

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			CookieManager.getInstance().removeAllCookies(null);
			CookieManager.getInstance().flush();
		} else {
			CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(ApplicationContextProvider.getContext());
			cookieSyncMngr.startSync();
			CookieManager cookieManager = CookieManager.getInstance();
			cookieManager.removeAllCookie();
			cookieManager.removeSessionCookie();
			cookieSyncMngr.stopSync();
			cookieSyncMngr.sync();
		}
	}


	public void showLoginScreen() {

		AppSession.instance().setInAppLogin(true);

		//addUserLoginFragment(false);
		addUserLoginFragmentMFA
				(false);

	}


	public void showLoginScreenWithForwardAction(int intClickedViewId) {

		AppSession.instance().setInAppLogin(true);

		addUserLoginFragmentMFA(false, intClickedViewId);


	}


	private void addNavigationFragment(boolean blnSupressAnimation) {

		AppTagManager.instance().sendVersion2ScreenView("App - Navigation");

		//show search bar
		nativeSearchBar.setVisibility(View.VISIBLE);
		showNavigationScreen();

		//Fragment already in back stack? Just show it again and hide all the others
		/*
		if(bringFragmentToTop(V3FragmentShopNavigation.getLogTag())){
			return;
		}
		*/

		Fragment fragment = fm.findFragmentByTag(V3FragmentShopNavigation.getLogTag());
		if (fragment != null && fragment.isAdded()) {
			return;
		}


		int intAppearAnimation = 0;

		//if this is the initial fragment: no animation as otherwise activity starts with an ugly delay
		if (blnSupressAnimation) {
			intAppearAnimation = 0;
		}


		V3FragmentShopNavigation navigationFragment = new V3FragmentShopNavigation();
		setFragmentArguments(navigationFragment, getString(R.string.shop_navigationFragment_breadcrumb_text));
		AppFragmentHelper.handleFragment(this, fm, main_navigationFragmentContainer, navigationFragment, V3FragmentShopNavigation.getLogTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.ADD, intAppearAnimation, false);


	}

	public void hideVirtualKeyboard() {

		InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		im.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

	}


	public void showUnsupportedStoreDialog(String strTargetHost) {

		strUnsupportedStoreHost = strTargetHost;

		DialogFragment clearFormDialog = new Dialog(this,
				getString(R.string.str_unsupported_store_dialog_title),
				String.format(
						getString(R.string.str_unsupported_store_dialog_message),
						AppUtils.getCountryNameFromHost(strTargetHost)
				),
				String.format(
						getString(R.string.str_unsupported_store_dialog_action_confirm),
						strTargetHost
				),
				getString(R.string.str_unsupported_store_dialog_action_cancel)
		);


		clearFormDialog.setCancelable(false); //make sure dialog can't be cancelled via back button or tapping outside it

		clearFormDialog.show(fm, Dialog.DIALOG_GO_TO_UNSUPPORTED_STORE);

	}


	@Override
	public void onDialogPositiveClick(DialogFragment dialog) {

		String strDialogTag = dialog.getTag();

		if (Dialog.DIALOG_GO_TO_UNSUPPORTED_STORE.equals(strDialogTag)) {

			loadUrlInExternalBrowser("http://" + strUnsupportedStoreHost);
			strUnsupportedStoreHost = "";
			finish(); //close Wiggle app (if not doing this, do mWebView.clearHistory(); to avoid issues with back-navigation)

		} else if (Dialog.DIALOG_LEAVE_APP_REVIEW_AFTER_ORDER.equals(strDialogTag)) {

			String strStoreUrl = AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home));

			Uri uri = Uri.parse(strStoreUrl);
			String strHost = uri.getHost();

			//Example for ES ..
			//https://www.trustpilot.com/review/www.wiggle.es
			//For UK:
			//https://www.trustpilot.com/review/www.wiggle.co.uk
			loadUrlInExternalBrowser("https://www.trustpilot.com/review/" + strHost);

			ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_POST_ORDER_TRUSTPILOTREVIEW_ACCEPTED, String.valueOf(AppUtils.getAppVersionInt()));

			/* Dialogs from FragmentEvents */
		} else if (Dialog.DIALOG_EVENT_LIST_SWIPE_TO_REVEAL_HINT.equals(strDialogTag)) {
			ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_EVENTS_SWIPE_HINT_ALREADY_SEEN, "1");
		} else if (Dialog.DIALOG_EVENT_SCREEN_LOCATION_REQUIRED.equals(strDialogTag)) {
			// Show location settings screen ..
			Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);
		} else if (Dialog.DIALOG_EVENT_SCREEN_UPDATE_GOOGLE_PLAY_SERVICES.equals(strDialogTag)) {

			//Go to Google Play and suggest Google Play Services
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse("market://details?id=com.google.android.gms"));
			startActivity(intent);

		}

	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {

		String strDialogTag = dialog.getTag();

		if (Dialog.DIALOG_GO_TO_UNSUPPORTED_STORE.equals(strDialogTag)) {

			strUnsupportedStoreHost = "";
			clearWebViewHistory(); //avoid issues with back-navigation

		} else if (Dialog.DIALOG_LEAVE_APP_REVIEW_AFTER_ORDER.equals(strDialogTag)) {

			//make sure user is not asked for review multiple times in the same session
			ObjSalesOrderManager.getInstance().resetJustDeliveredOrders();

		}

	}

	private void clearWebViewHistory() {

		V3FragmentWebShop webShopFragment = (V3FragmentWebShop) fm.findFragmentByTag(V3FragmentWebShop.CONST_WEB_SHOP_FRAGMENT);
		if (webShopFragment != null
				&& webShopFragment.isAdded()
		) {
			webShopFragment.setForceClearHistory();
		}
	}


	@Override
	public void onFragmentSelfClose(String tag) {

		//remove fragment by tag where applicable
		Fragment fragmentToRemove = fm.findFragmentByTag(tag);

		// remove fragment by tag ...
//		Fragment fragmentToRemove = fm.findFragmentByTag(tag);
//		FragmentTransaction transaction = fm.beginTransaction();
//		transaction.remove(fragmentToRemove);
//		transaction.commitAllowingStateLoss();

		//or just pop the entire back stack if dealing with multiple fragments
		//The idea is to display a "virgin" parent activity
		fm.popBackStack();


	}

	private void executeCodeForVersionUpgrade() {

		final String VERSION_UPGRADE_LOG = "VERSION_UPGRADE_LOG";
		final String LAST_SUCCESSFUL_UPGRADE_TARGET_VERSION = "LAST_SUCCESSFUL_UPGRADE_TARGET_VERSION";

		String strLastSucessfulUpgradeTargetVersion = DataStore.getStringFromSharedPreferences(VERSION_UPGRADE_LOG, LAST_SUCCESSFUL_UPGRADE_TARGET_VERSION);

		//Version spefic code executed after version upgrades if required
		//First introduced with version 1.02 - any version before will return "" from shared preferences
		if (strLastSucessfulUpgradeTargetVersion.equals("")) {

			//Version 1.02 upgrade - we need to remove all Cookies as previous versions incorrectly duplicated cookie WiggleCustomer2
			//Without clearing all cookies, the app will show a "too_many_redirects" error again during checkout
			clearAllCookies();

			//Job done? All upgrade relevant code executed .. log target version number in shared preferences
			String strAppVersion = AppUtils.getAppVersionNumber();
			DataStore.saveStringToSharedPreferences(VERSION_UPGRADE_LOG, LAST_SUCCESSFUL_UPGRADE_TARGET_VERSION, strAppVersion);

		} else if (strLastSucessfulUpgradeTargetVersion.equals("1.03")) {

			//do some upgrade work if required .. TODO: add version numbers and handling as required

		}


	}

	private void clearAllCookies() {

		CookieManager cookieManager = CookieManager.getInstance();

		if (Build.VERSION.SDK_INT >= LOLLIPOP) {
			cookieManager.removeAllCookies(new ValueCallback<Boolean>() {
				// a callback which is executed when the cookies have been removed
				@Override
				public void onReceiveValue(Boolean aBoolean) {
					Logger.printMessage(logTag, "clearAllCookies - removedAllCookies: " + aBoolean, Logger.INFO);
				}
			});
		} else {
			cookieManager.removeAllCookie();
		}

	}


	public void showReviewOrderDialog(ObjSalesOrder salesOrder) {

		//get current app version number:
		int intCurrentAppVersion = AppUtils.getAppVersionInt();

		//get version number the user last agreed to leave feedback on an order
		int intLastFeedbackInVersion = ObjAppUser.instance().getInt(ObjAppUser.FIELD_POST_ORDER_TRUSTPILOTREVIEW_ACCEPTED);

		//let at least one version pass before we ask user to submit review again. E.g. if user submitted review
		//in version 16, we wont ask again before version 18. If user never submitted review, intCurrentAppVersion would be 
		//zero and we ask him in any case. 
		/** Enable this if you would like to ask users for further feedback in the future
		 if(intLastFeedbackInVersion == 0
		 || intCurrentAppVersion - intLastFeedbackInVersion <=1){ // 1 = skip 1 version for feedback
		 return;
		 }
		 **/

		//if user already accepted the trustpilot review dialog - dont ask him again.
		if (intLastFeedbackInVersion > 0) {
			//Already asked for Feedback? Return .. 
			return;
		}


		DialogFragment reviewOrderDialog = new Dialog(this,
				"", //getString(R.string.str_leave_order_review_dialog_title),
//				String.format(
//						getString(R.string.str_leave_order_review_dialog_message),
//						salesOrder.getString(ObjSalesOrder.FIELD_ORDER_ID)
//						),
				getString(R.string.str_leave_order_review_dialog_message_new),
				getString(R.string.str_leave_order_review_dialog_action_confirm),
				getString(R.string.str_leave_order_review_dialog_action_cancel)
		);


		reviewOrderDialog.show(fm, Dialog.DIALOG_LEAVE_APP_REVIEW_AFTER_ORDER);

	}


	public void updateOrderTrackingBadge() {

		int intUpdatedOrders = ObjSalesOrderManager.getInstance().getOrdersWithNewInformation().size();

		if (intUpdatedOrders > 0) {
			//Wiggle requirement - on "My Account" navigation tab, only show badge, but no text
			navigation_tvAccountBadge.setText("");
			//navigation_tvAccountBadge.setText(String.valueOf(intUpdatedOrders));
			navigation_tvAccountBadge.setVisibility(View.VISIBLE);

			//New info alert next to menu burger
			navBar_tvAccountNewInfoAlert.setText(""); //just show orange dot
			navBar_tvAccountNewInfoAlert.setVisibility(View.VISIBLE);
		} else {
			navigation_tvAccountBadge.setVisibility(View.GONE);
			navBar_tvAccountNewInfoAlert.setVisibility(View.GONE);
		}

		if (V3FragmentShopNavigation.instance() != null
				&& V3FragmentShopNavigation.instance().isAdded()
				&& V3FragmentShopNavigation.instance().getView() != null //make sure onViewCreated was executed
		) {
			V3FragmentShopNavigation.instance().updateOrderTrackingBadge();
		}


	}


	private boolean scanForDeepLinking() {

		/*

		   Deep links can point to this app currently with the following configuration:
		   1) wiggle://native  ->   opens native app screen
		      Current supported deep links are:
		      		   •	wiggle://native/news -> NewsfeedActivity
		      		   •	wiggle://native/events -> EventsActivity
		      		   •	wiggle://native/delivery -> Order tracking screen in HybridActivity
		   2) wiggle://web -> opens Hybrid activity and opens URL slug after /web
		      For instance:
		      			•	wiggle://web/swim  -->  opens www.wiggle.<topleveldomain>/swim

		 */
		Intent intent = getIntent();
		String action = intent.getAction();
		Uri deepLinkUri = intent.getData();

		if (deepLinkUri == null) {
			//nothing to do
			return false;
		} else {
			return onDeepLinkDetected(deepLinkUri);
		}

	}

	private boolean onDeepLinkDetected(Uri deepLinkUri) {

		/* 1. Handle dedicated wiggle://native deep links
		 *
		 *     They can look just like "wiggle://native/news"
		 *     or have parameters appended to it. e.g in app message deep links may
		 *    look like "wiggle://native/news" in the console, but
		 *     arrive here as wiggle://native/delivery?ampExternalOpen=true&ampAction=click
		 *     Thus using startsWith (could use Uri.getHost etc ... but doesnt matter here)
		 * */
		if (deepLinkUri.toString().startsWith("wiggle://native/news")) {

			AppUtils.showCustomToast("Open Newsfeed Activity for " + deepLinkUri.toString(), true);
			closeNativeScreensOnDeepLink();

			//In order to preserve navigation - and keep user in the app: always add shop fragment
			addWebShopFragment(true, null);

			startNewsfeedActivity(null, "deepLinkNews"); //TODO: adjust GTM tag
			//Deep link identified
			return true;

		} else if (deepLinkUri.toString().startsWith("wiggle://native/delivery")) {

			AppUtils.showCustomToast("Open Order tracking screen for " + deepLinkUri.toString(), true);
			//Close any activities that would appear on top of our Hybrid Activity
			closeNativeScreensOnDeepLink();

			//In order to preserve navigation - and keep user in the app: always add shop fragment
			addWebShopFragment(true, null);

			if (blnIsDisplayedToUser) {
				addOrderTrackingFragment(true);
			} else {
				//Activity is not shown or started by deep link? Wait for onStart to open deep link
				strDeepLinkTargetFragmentName = V3FragmentOrderTracking.getLogTag();
			}
			//Deep link identified
			return true;


		}

		/* 2. Handle dedicated wiggle://web deep links */
		if ("wiggle".equals(deepLinkUri.getScheme()) && "web".equals(deepLinkUri.getHost())) {

			String strTargetUrl = AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home)) + deepLinkUri.getPath().toString();
			AppUtils.showCustomToast("Open hybrid activity for " + deepLinkUri.toString(), true);
			//Close any activities that would appear on top of our Hybrid Activity
			closeNativeScreensOnDeepLink(); //false - do not close HybridActivity as we open URL here
			openDeepLinkInWebView(strTargetUrl);
			//Deep link identified
			return true;

		}

		/* 3. Handle other URL patterns */
		/* Not needed for now
		if(!blnDeepLinkIdentified
				&& ("wiggle".equals(deepLinkUri.getScheme()) && "sportives".equals(deepLinkUri.getHost()))
				||
				("wiggle".equals(deepLinkUri.getScheme()) && "events".equals(deepLinkUri.getHost()))
				){

			if(AppUtils.isNativeEventScreenEnabled()) {
				AppUtils.showCustomToast("Open Events Activity for " + deepLinkUri.toString(), true);
				blnDeepLinkIdentified = true;
				closeNativeScreensOnDeepLink();
				startEventsActivity(null, "deepLinkEvents");
				finish(); //finish Hybrid Activity when detail screen is opened to avoid multiple instances of HybridActiviy in back stack
				return;
			}

		}
		*/

		/* 4. Check if this in general is a wiggle-hosted URL - just open it in Hybrid Activity */
		if (AppUtils.isTrustedWiggleHost(deepLinkUri.getHost().toString()) //removed in version 2.1.1 - we rely on App-Linking to auto-verify trusted hosts
		) {

		    //In the push message configuration, a user can specify a fully qualified URL (https:/www.wiggle.co.uk/hello)
            //We want to remove the 'host' and concatenate the current Wiggle store host with the path /hello
            //e.g. for Spain this should turn into https:/www.wiggle.es/hello
            String strPath = deepLinkUri.getPath();
            Uri targetUri = deepLinkUri;
            if("https".equals(deepLinkUri.getScheme())) {
                String strUrl = AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home_https)) + strPath;
                targetUri = Uri.parse(strUrl);
            }else if("http".equals(deepLinkUri.getScheme())) {
                String strUrl = AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home))+ strPath;
                targetUri = Uri.parse(strUrl);
            }


		    //Close any activities that would appear on top of our Hybrid Activity
			closeNativeScreensOnDeepLink(); //false - do not close HybridActivity as we open URL here
			openDeepLinkInWebView(targetUri.toString());
			//Deep link identified
			return true;
		}


		//no handled deep link found? Do nothing
		return false;

	}

	private void openDeepLinkInWebView(String strUrl) {

		loadUrlInWebShopFragment(strUrl);

	}

	private void closeNativeScreensOnDeepLink() {


		/*
		   Closing any native screens that currently exist in the activity stack
		    makes sure that deep-linking does not introduce duplicate activities to the stack
		   and all deep-link intents are correctly processed (e.g. when HybridActivity is open
		   a deep-link needs to take you to the correct target page)
		 */

		if (NewsfeedActivity.instance() != null) {
			NewsfeedActivity.instance().finish();
		}

		//TODO: add further activities here when implemented

	}


	@Override
	public void onNewIntent(Intent intent) {

		//check new intents as well for deep links
		//If StartupActivity has already been started and deep links are pressed elsewhere they may not work otherwise
		super.onNewIntent(intent);
		setIntent(intent);
		scanForDeepLinking();

	}


	@Override
	public void onStart() {

		super.onStart();

		blnIsDisplayedToUser = true;

		goToDeepLinkFragment();


	}


	public void showHttpErrorDialog(String strUrl) {

		DialogFragment dialog = new Dialog(this,
				getString(R.string.hybridActivity_webview_load_error_title),
				getString(R.string.hybridActivity_webview_load_error_message),
				getString(R.string.hybridActivity_webview_load_error_ok_button).toUpperCase(),
				"" // no cancel button
		);

		//Crashes have been reported for this error dialog.
		//If there is an issue - just ignore and proceed
		try {
			dialog.show(fm, Dialog.DIALOG_WEBVIEW_LOAD_ERROR);
		} catch (Exception e) {
			Logger.printMessage(logTag, "showHttpErrorDialog: error showing dialog", Logger.ERROR);
		}


	}


	private void onNavigationTabPressed(int intTabId, boolean blnExplicitUserAction) {

		hideNavigationScreenIfDisplayed();
		toggleSearchBar(true);

		//Whenever tab is pressed - check if FragmentWebView is shown and remove if required
		Fragment webViewFragment = fm.findFragmentByTag(V3FragmentWebShop.CONST_WEB_VIEW_FRAGMENT);
		if (webViewFragment != null
				&& webViewFragment.isAdded()) {
			AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, webViewFragment, webViewFragment.getTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.REMOVE, 0, false);
		}

		//properly remove account fragments if required
		removeMyAccountFragments();


		View tabView = findViewById(intTabId);

		setNavigationTabSelected(tabView);

		resetCoordinatorLayout();

		performTabAction(tabView, intTabId);

	}

	private void setNavigationTabSelected(View tabView) {

		//highlight current tab
		deselectAllNavigationTabs();

		ImageView tabImage = (ImageView) tabView.findViewWithTag("tabImage");
		tabView.setSelected(true);
		tabImage.setColorFilter(getResources().getColor(R.color.orangeButton));

		setBreadcrumbText(tabView.getTag().toString());

		AppAnimations.getInstance().pulseAnimation(tabImage, R.anim.pulse_navtab);

	}

	private void performTabAction(View tabView, int intTabId) {

		//legacy tracking - same for all tabs
		AppTagManager.instance().sendEventTags("App - Thumb Nav", "ThumbNav");

		//when clicking a tab - always clear any fragments that might be displayed
		//removeAllFragmentsToIndex(0); //this REMOVES all fragments, so last user interaction is lost when fragment next added

		//String strTabTag = tabView.getTag().toString();
		switch (intTabId) {

			case R.id.navigation_Home:
				AppTagManager.instance().sendV2EventTags("linkclick", "App - Thumb Nav", "Main", "Home Page");
				addWebViewFragment(R.id.navigation_Home, false);
				break;

			case R.id.navigation_Search:
				AppTagManager.instance().sendV2EventTags("linkclick", "App - Thumb Nav", "Main", "Navigation");
				addNavigationFragment(false);
				break;

			case R.id.navigation_Basket:
				AppTagManager.instance().sendV2EventTags("linkclick", "App - Thumb Nav", "Main", "Basket");
				String strBasketUrl = AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_shopping_basket), "shopBasket");
				//addWebViewFragment(strBasketUrl, false);
				loadUrlInWebShopFragment(strBasketUrl);
				break;

			case R.id.navigation_Wishlist:
				AppTagManager.instance().sendV2EventTags("linkclick", "App - Thumb Nav", "Main", "Wishlist");
				//loadTargetURL(R.id.navigation_Wishlist);
				addWebViewFragment(R.id.navigation_Wishlist, false);
				break;

			case R.id.navigation_Account:
				AppTagManager.instance().sendV2EventTags("linkclick", "App - Thumb Nav", "Main", "My Account");
				//loadTargetURL(R.id.navigation_Account);
				addMyAccountFragment(false);
				break;

			default:
				break;

		}

	}

	private void deselectAllNavigationTabs() {

		navigation_Home.setSelected(false);
		navigation_ivHome.setColorFilter(null);
		navigation_Search.setSelected(false);
		navigation_ivSearch.setColorFilter(null);
		navigation_Basket.setSelected(false);
		navigation_ivBasket.setColorFilter(null);
		navigation_Wishlist.setSelected(false);
		navigation_ivWishlist.setColorFilter(null);
		navigation_Account.setSelected(false);
		navigation_ivAccount.setColorFilter(null);

	}


	private void initialiseSupportFragmentManager() {

		fm = getSupportFragmentManager();

		//clear any 'old' fragments if they still hang around
		//This is useful when activity gets destroyed - however: may cause issues when saving / recreating instance state
		removeAllFragmentsToIndex(0);

		//add onBackStackListener so we can update the breadcrumb etc depending on the fragment currently shown
		fm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {

			@Override
			public void onBackStackChanged() {

				Fragment activeFragment = fm.findFragmentById(R.id.main_fragmentContainer);
				updateUIWidgetsForFragment(activeFragment);

				if (fm.getBackStackEntryCount() == 0) {
					//main_fragmentContainer.setVisibility(View.GONE);
					//no more fragments - add "Home" fragment automatically (this is used when user changes country in account options)
					if (blnAddHomeAfterBackstackCleared) {
						blnAddHomeAfterBackstackCleared = false;
						//Recreate home fragment - but wait a while as fragment manager needs to finish handling outstanding transactions
						recreateHomeFragmentWithDelay();
					}
				}

			}
		});

	}

	public void updateUIWidgetsForFragment(Fragment fragment) {


		String strFragmentBreadcrumbTitle = AppFragmentHelper.getFragmentBreadcrumbTitle(fragment);
		if (strFragmentBreadcrumbTitle != null && !"".equals(strFragmentBreadcrumbTitle)) {


			//Due to the use of hidden fragments this does not always work
			//Hence let navTabs and fragments do the breadcrumb setting
			//setBreadcrumbText(strFragmentBreadcrumbTitle);
			setTitleForVisibleFragment(); //show title for visible fragment instead

			main_tvBreadcrumb.setVisibility(View.GONE); //2019.2 no longer used
		} else {
			main_tvBreadcrumb.setVisibility(View.GONE);
		}

		showContextualBackButton();

	}


	private void addMyAccountFragment(boolean blnSupressAnimation) {

		AppTagManager.instance().sendVersion2ScreenView("App - My Account");


		//Fragment already in back stack? Just show it again and hide all the others
		if (bringFragmentToTop(FragmentMyAccount.getLogTag())) {
			return;
		}

		int intAppearAnimation = 0; //removed (workaround for Shop-tab screens showing when fragments are changed on top of Shop-tab)

		//if this is the initial fragment: no animation as otherwise activity starts with an ugly delay
		if (blnSupressAnimation) {
			intAppearAnimation = 0;
		}

		FragmentMyAccount myAccountFragment = new FragmentMyAccount();
		setFragmentArguments(myAccountFragment, getString(R.string.shop_myAccount_breadcrumb_text));
		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, myAccountFragment, FragmentMyAccount.getLogTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.ADD, intAppearAnimation, false);


	}


	private void addWebShopFragment(boolean blnSupressAnimation, String strOptionalURL) {

		//Web SHOP Fragment already in back stack? Just show it again and hide all the others
		Fragment webShopFragment = fm.findFragmentByTag(V3FragmentWebShop.CONST_WEB_SHOP_FRAGMENT);
		if (webShopFragment != null
				&& ((V3FragmentWebShop) webShopFragment).isWebShopFragment()
				&& webShopFragment.isAdded()
		) {
			if (bringFragmentToTop(V3FragmentWebShop.CONST_WEB_SHOP_FRAGMENT)) {

				if (strOptionalURL != null && !"".equals(strOptionalURL)) {
					((V3FragmentWebShop) webShopFragment).loadUrlInWebView(strOptionalURL);
				}
				return;
			}
		}

		/*
		if (bringFragmentToTop(FragmentWebShop.CONST_WEB_SHOP_FRAGMENT)) {
			return;
		}
		*/


		int intAppearAnimation = R.anim.activity_right_slide_in;

		//if this is the initial fragment: no animation as otherwise activity starts with an ugly delay
		if (blnSupressAnimation) {
			intAppearAnimation = 0;
		}

		V3FragmentWebShop newWebShopFragment = new V3FragmentWebShop();
		setWebShopFragmentArguments(newWebShopFragment, "Web Shop Fragment", V3FragmentWebShop.CONST_WEB_SHOP_FRAGMENT, strOptionalURL);
		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, newWebShopFragment, V3FragmentWebShop.CONST_WEB_SHOP_FRAGMENT, AppFragmentHelper.FRAGMENT_TRANSACTION.ADD, intAppearAnimation, false);

	}

	private void addWebShopFragment(boolean blnSupressAnimation, int intClickedViewId) {

		//Web SHOP Fragment already in back stack? Just show it again and hide all the others
		Fragment webShopFragment = fm.findFragmentByTag(V3FragmentWebShop.CONST_WEB_SHOP_FRAGMENT);
		if (webShopFragment != null
				&& ((V3FragmentWebShop) webShopFragment).isWebShopFragment()
				&& webShopFragment.isAdded()
		) {
			if (bringFragmentToTop(V3FragmentWebShop.CONST_WEB_SHOP_FRAGMENT)) {

				((V3FragmentWebShop) webShopFragment).loadTargetURL(intClickedViewId);
				return;
			}
		}

		/*
		if (bringFragmentToTop(FragmentWebShop.CONST_WEB_SHOP_FRAGMENT)) {
			return;
		}
		*/


		int intAppearAnimation = R.anim.activity_right_slide_in;

		//if this is the initial fragment: no animation as otherwise activity starts with an ugly delay
		if (blnSupressAnimation) {
			intAppearAnimation = 0;
		}

		V3FragmentWebShop newWebShopFragment = new V3FragmentWebShop();
		setWebShopFragmentArguments(newWebShopFragment, "Web Shop Fragment", V3FragmentWebShop.CONST_WEB_SHOP_FRAGMENT, intClickedViewId);
		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, newWebShopFragment, V3FragmentWebShop.CONST_WEB_SHOP_FRAGMENT, AppFragmentHelper.FRAGMENT_TRANSACTION.ADD, intAppearAnimation, false);

	}


	//only called in exceptions if the user changes the Wiggle shop region in the settings screen
	public void removeWebShopFragment() {

		//Web SHOP Fragment already in back stack? Just show it again and hide all the others
		Fragment webShopFragment = fm.findFragmentByTag(V3FragmentWebShop.CONST_WEB_SHOP_FRAGMENT);
		if (webShopFragment != null
				&& webShopFragment.isAdded()
		) {
			removeSingleFragment(webShopFragment);
		}

	}

	public void addUserLoginFragmentMFA(boolean blnSupressAnimation) {

		AppTagManager.instance().sendVersion2ScreenView("App - signin");


		if (bringFragmentToTop(V3FragmentUserLoginMFA.getLogTag())) {
			return;
		}

		int intAppearAnimation = R.anim.activity_right_slide_in;

		//if this is the initial fragment: no animation as otherwise activity starts with an ugly delay
		if (blnSupressAnimation) {
			intAppearAnimation = 0;
		}

		V3FragmentUserLoginMFA userLoginFragment = new V3FragmentUserLoginMFA();
		setFragmentArguments(userLoginFragment, getString(R.string.shop_userLogin_breadcrumb_text));
		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, userLoginFragment, V3FragmentUserLoginMFA.getLogTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.ADD, intAppearAnimation, false);


	}


	public void addUserLoginFragmentMFA(boolean blnSupressAnimation, int intClickedViewId) {

		AppTagManager.instance().sendVersion2ScreenView("App - signin");

		hideNavigationScreenIfDisplayed();


		if (bringFragmentToTop(V3FragmentUserLoginMFA.getLogTag())) {
			return;
		}

		int intAppearAnimation = R.anim.activity_right_slide_in;

		//if this is the initial fragment: no animation as otherwise activity starts with an ugly delay
		if (blnSupressAnimation) {
			intAppearAnimation = 0;
		}

		V3FragmentUserLoginMFA userLoginFragment = new V3FragmentUserLoginMFA();
		setFragmentArguments(userLoginFragment, getString(R.string.shop_userLogin_breadcrumb_text));

		//add extra argument for calling view
		userLoginFragment.getArguments().putInt(V3FragmentUserLoginMFA.LOGIN_SCREEN_CALLER, intClickedViewId);

		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, userLoginFragment, V3FragmentUserLoginMFA.getLogTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.ADD, intAppearAnimation, false);


	}


	private void addWebViewFragment(int intCallingView, boolean blnSupressAnimation) {

		//Fragment already in back stack? Just show it again and hide all the others
		/*
		Fragment webViewFragment = fm.findFragmentByTag(V3FragmentWebShop.CONST_WEB_VIEW_FRAGMENT);
		if(webViewFragment!=null
				&& webViewFragment.isAdded()
		){
			if (bringFragmentToTop(V3FragmentWebShop.CONST_WEB_VIEW_FRAGMENT)) {

				((V3FragmentWebShop) webViewFragment).loadTargetURL(intCallingView);
				return;
			}
		}
		*/

		//This fragment always goes to the top of the stack
		//And always will get removed when navigated away from it
		//It's purpose is to load Wiggle shop URLs from various callers
		//whereas the WebShopFragment is used for browing on the "Shop" tab and always remains in the
		//background retaining its last state

		int intAppearAnimation = 0; //removed (workaround for Shop-tab screens showing when fragments are changed on top of Shop-tab);

		//if this is the initial fragment: no animation as otherwise activity starts with an ugly delay
		if (blnSupressAnimation) {
			intAppearAnimation = 0;
		}

		V3FragmentWebShop newWebViewFragment = new V3FragmentWebShop();
		setWebShopFragmentArguments(newWebViewFragment, "Web View Fragment", V3FragmentWebShop.CONST_WEB_VIEW_FRAGMENT, intCallingView);
		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, newWebViewFragment, V3FragmentWebShop.CONST_WEB_VIEW_FRAGMENT, AppFragmentHelper.FRAGMENT_TRANSACTION.ADD, intAppearAnimation, false);

	}

	private void addWebViewFragment(String strTargetUrl, boolean blnSupressAnimation) {

		//Fragment already in back stack? Just show it again and hide all the others
		/*
		Fragment webViewFragment = fm.findFragmentByTag(V3FragmentWebShop.CONST_WEB_VIEW_FRAGMENT);
		if(webViewFragment!=null
				&& webViewFragment.isAdded()
		){
			if (bringFragmentToTop(V3FragmentWebShop.CONST_WEB_VIEW_FRAGMENT)) {

				((V3FragmentWebShop) webViewFragment).loadUrlInWebView(strTargetUrl);
				return;
			}
		}
		*/

		//This fragment always goes to the top of the stack
		//And always will get removed when navigated away from it
		//It's purpose is to load Wiggle shop URLs from various callers
		//whereas the WebShopFragment is used for browing on the "Shop" tab and always remains in the
		//background retaining its last state

		int intAppearAnimation = 0; //removed (workaround for Shop-tab screens showing when fragments are changed on top of Shop-tab);

		//if this is the initial fragment: no animation as otherwise activity starts with an ugly delay
		if (blnSupressAnimation) {
			intAppearAnimation = 0;
		}

		V3FragmentWebShop newWebViewFragment = new V3FragmentWebShop();
		setWebShopFragmentArguments(newWebViewFragment, getString(R.string.shop_home_breadcrumb_text), V3FragmentWebShop.CONST_WEB_VIEW_FRAGMENT, strTargetUrl);
		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, newWebViewFragment, V3FragmentWebShop.CONST_WEB_VIEW_FRAGMENT, AppFragmentHelper.FRAGMENT_TRANSACTION.ADD, intAppearAnimation, false);

	}


	public void addOrderTrackingFragment(boolean blnSupressAnimation) {

		AppTagManager.instance().sendVersion2ScreenView("App - Track My Order - My Orders");

		hideNavigationScreenIfDisplayed();


		//Fragment already in back stack? Just show it again and hide all the others
		if (bringFragmentToTop(V3FragmentOrderTracking.getLogTag())) {
			//Make sure order list is shown
			V3FragmentOrderTracking orderTrackingFragment = (V3FragmentOrderTracking) fm.findFragmentByTag(V3FragmentOrderTracking.getLogTag());
			if (orderTrackingFragment != null && orderTrackingFragment.isAdded()) {
				orderTrackingFragment.orderTrackingScreenCanGoBack(true);
			}
			return;
		}

		int intAppearAnimation = R.anim.activity_right_slide_in;

		//if this is the initial fragment: no animation as otherwise activity starts with an ugly delay
		if (blnSupressAnimation) {
			intAppearAnimation = 0;
		}

		V3FragmentOrderTracking orderTrackingFragment = new V3FragmentOrderTracking();
		setFragmentArguments(orderTrackingFragment, getString(R.string.myAccount_orderTracking_breadcrumb_text));
		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, orderTrackingFragment, V3FragmentOrderTracking.getLogTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.ADD, intAppearAnimation, false);


	}


	private void setFragmentArguments(Fragment anyFragmentOnThisActivity, String strBreadcrumbTitle) {

        /* common method used to populate
            CALLING_ACTIVITY
            FRAGMENT_BREADCRUMB_TITLE
            for any fragment hosted by this Activity
         */
		Bundle arguments = new Bundle();
		arguments.putString(AppGlobalConstants.CALLING_ACTIVITY, V3HybridActivity.getLogTag());
		arguments.putString(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE, strBreadcrumbTitle);
		anyFragmentOnThisActivity.setArguments(arguments);

	}

	//specific implementation to setFragmentArguments for WebShopFragment
	private void setWebShopFragmentArguments(
			V3FragmentWebShop webShopFragment
			, String strBreadcrumbTitle
			, String strFragmentUsage
			, int intWebViewCaller
	) {


		Bundle arguments = new Bundle();
		arguments.putString(AppGlobalConstants.CALLING_ACTIVITY, V3HybridActivity.getLogTag());
		arguments.putString(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE, strBreadcrumbTitle);
		arguments.putString(V3FragmentWebShop.CONST_FRAGMENT_USAGE, strFragmentUsage);
		arguments.putInt(V3FragmentWebShop.WEB_VIEW_CALLER, intWebViewCaller);
		webShopFragment.setArguments(arguments);

	}

	private void setWebShopFragmentArguments(
			V3FragmentWebShop webShopFragment
			, String strBreadcrumbTitle
			, String strFragmentUsage
			, String strTargerUrl
	) {


		Bundle arguments = new Bundle();
		arguments.putString(AppGlobalConstants.CALLING_ACTIVITY, V3HybridActivity.getLogTag());
		arguments.putString(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE, strBreadcrumbTitle);
		arguments.putString(V3FragmentWebShop.CONST_FRAGMENT_USAGE, strFragmentUsage);
		arguments.putString(V3FragmentWebShop.LOAD_TARGET_URL, strTargerUrl);
		webShopFragment.setArguments(arguments);

	}

	private boolean handleFragmentBackNavigation(boolean blnBackWasPressed) {


		if (fm.getBackStackEntryCount() == 0) {
			return false;
		}

		//Check if FragmentWebView (i.e. FragmentWebShop with webView usage)
		// is the current fragment - if so, then remove it
		Fragment currentFragment = fm.findFragmentById(R.id.main_fragmentContainer);
		if (currentFragment instanceof V3FragmentWebShop
				&& ((V3FragmentWebShop) currentFragment).isWebViewFragment()
				//ignore if the fragment is hidden
				&& currentFragment.isVisible()
		) {

			//TODO:
			if (((V3FragmentWebShop) currentFragment).webViewCanGoBack(blnBackWasPressed)) {
				//go back
				return true;
			} else {
				//Close webView fragment
				if (blnBackWasPressed) {
					AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, currentFragment, currentFragment.getTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.REMOVE, 0, false);
				}
				return true;
			}

		}

		Fragment visibleFragment = getTopVisibleFragment();

		//No fragments visible? Nothing to handle
		if (visibleFragment == null) {
			return false;
		}


		//special handling for some fragments if required .. (some fragments have a "canGoBack" option)
		if (visibleFragment instanceof V3FragmentWebShop) {

			if (((V3FragmentWebShop) visibleFragment).webViewCanGoBack(blnBackWasPressed)) {
				return true;
			} else {
				//We can't go back on the web shop fragment? Close app
				return false;
			}

		} else if (visibleFragment instanceof V3FragmentUserLoginMFA) {

			if (blnBackWasPressed) {
				AppSession.instance().setInAppLogin(false);
			}
			if (((V3FragmentUserLoginMFA) visibleFragment).canGoBack(blnBackWasPressed)) {
				return true;
			}

		}


		if (visibleFragment instanceof V3FragmentOrderTracking) {

			//allow back navigation on order tracking screen
			if (((V3FragmentOrderTracking) visibleFragment).orderTrackingScreenCanGoBack(blnBackWasPressed)) {
				return true;
			}

		}


		//default handling - remove fragment on back pressed
		if (visibleFragment != null) {
			if (blnBackWasPressed) {
				AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, visibleFragment, visibleFragment.getTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.REMOVE, 0, false);
			}
		}

		return true;

	}


	public int getActiveTab() {

		int lstNavTabs[] = {
				R.id.navigation_Home,
				R.id.navigation_Search,
				R.id.navigation_Basket,
				R.id.navigation_Wishlist,
				R.id.navigation_Account
		};


		for (int idView : lstNavTabs) {
			View tabView = findViewById(idView);
			if (tabView.isSelected()) {
				return idView;
			}
		}

		return -1;

	}

	public void removeAllFragmentsToIndex(int entryIndex) {

		// This removes all fragments at once leading to an empty fragment stack
		// Use with care as this of course looses any interaction the user has last done on the fragment

		if (fm == null) {
			return;
		}

		if (fm.getBackStackEntryCount() <= entryIndex) {
			return;
		}

		FragmentManager.BackStackEntry entry = fm.getBackStackEntryAt(entryIndex);

		if (entry != null) {
			fm.popBackStackImmediate(entry.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}


	}


	private Fragment getTopVisibleFragment() {


		//gets the "topmost" fragment in the back stack that is visible - this one is displayed to the user

		if (fm == null || fm.getFragments() == null) {
			return null;
		}

		Fragment topVisibleFragment = null;

		//On this Activity, we show / hide fragments in order to keep their state rather than replacing them.
		//Show/Hide operations do not change the backstack - and the "active" fragment on top of the stack may not be the one
		//that you are seeing on the screen. So - in order to find the fragment that is currently displayed, we
		//loop through all fragments and identify the one that is currently visible
		for (Fragment myFragment : fm.getFragments()) {

			//Exclude navigation fragment as this is handled in a separate fragment container
			if (V3FragmentShopNavigation.getLogTag().equals(myFragment.getTag())) {
				continue;
			}

			if (myFragment != null && myFragment.isVisible()) {
				topVisibleFragment = myFragment;
			}
		}

		//no fragments or all fragments hidden?
		return topVisibleFragment;
	}


	public boolean bringFragmentToTop(String tag) {

		Fragment fragment = fm.findFragmentByTag(tag);

		if (fragment != null && fragment.isAdded()) {

			FragmentTransaction fragmentTransaction = fm.beginTransaction();

			fragmentTransaction.setCustomAnimations(R.anim.activity_left_slide_in, R.anim.activity_left_slide_out, 0, 0);


			for (Fragment f : fm.getFragments()) {

				if (f == null) {
					continue;
				}

				if (f.equals(fragment)) {

					fragmentTransaction.show(f);
					//update UI Widgets for this fragment
					updateUIWidgetsForFragment(fragment);
				} else {

					//hide other fragments ..

					//exclude WebShop fragment as this is our 'home' fragment which always needs displaying
					if (f instanceof V3FragmentWebShop
							&& ((V3FragmentWebShop) f).isWebShopFragment()) {
						continue;
					}

					//exclude navigation fragment as that is in a separate container
					//Handling of NPEs that were caused due to memory clear-ups
					String v3FragmentshopNavigationLogTag = V3FragmentShopNavigation.getLogTag();
					if (v3FragmentshopNavigationLogTag == null) {
						v3FragmentshopNavigationLogTag = "V3FragmentShopNavigation";
					}
					if (!v3FragmentshopNavigationLogTag.equals(f.getTag())) {
						fragmentTransaction.hide(f);
					}

				}
			}

			try {
				fragmentTransaction.commit();
			} catch (Exception e) {
				Logger.printMessage(logTag, "bringFragmentToTop: fragmentTransaction.commit()", Logger.ERROR);
			}
			return true;
		}

		return false;
	}

	private void hideAllFragments() {

		if (fm == null || fm.getFragments() == null) {
			return;
		}

		FragmentTransaction fragmentTransaction = fm.beginTransaction();

		for (Fragment f : fm.getFragments()) {
			if (f == null) {
				continue;
			}

			if (f.isVisible()) {
				fragmentTransaction.hide(f);
			}
		}

		fragmentTransaction.commit();

	}


	public void setBreadcrumbText(String strUrlOrText) {

		//Hide progress bar if displayed
		main_pbLoadProgress.setVisibility(View.GONE);
		showHeaderDividerForShopFragment();
		resetCoordinatorLayout();

		main_tvBreadcrumb.setText(strUrlOrText);

		if (strUrlOrText == null || "".equals(strUrlOrText)) {
			main_lytTopElements.setVisibility(View.GONE);
		} else {
			main_lytTopElements.setVisibility(View.VISIBLE);
		}

	}

	public void hideSingleFragment(Fragment fragment) {

		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, fragment, fragment.getTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.HIDE, R.anim.activity_left_slide_out, false);

	}

	public void removeSingleFragment(Fragment fragment) {

		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, fragment, fragment.getTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.REMOVE, R.anim.activity_left_slide_out, false);

	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

	}

	public void setTitleForVisibleFragment() {

		//Exception - do nothing when we are on "Home" tab and user is navigating deep into the website
		//TODO: this is very hacky .. please adjust
		if (R.id.navigation_Home == V3HybridActivity.instance().getActiveTab()) {
			if ("    ".equals(main_tvBreadcrumb.getText())) {
				return;
			}
		}


		Fragment visibleFragment = getTopVisibleFragment();

		if (visibleFragment != null) {

			//We shop fragment does not have arguments for use on back pressed - set screen title to tab title
			if (visibleFragment instanceof V3FragmentWebShop) {

				String strBreadcrumb = "";

				//check if there was a caller for this fragment
				if (((V3FragmentWebShop) visibleFragment).getWebViewCaller() > 0) {
					strBreadcrumb = ((V3FragmentWebShop) visibleFragment).getBreadcrumbText();
				}

				//Breadcrumb for caller available? Use it ..
				if (!"".equals(strBreadcrumb)) {
					setBreadcrumbText(strBreadcrumb);
				} else {
					//default: show tab title as breadcrumb for FragmentWebShop
					int intActiveTab = getActiveTab();
					View activeTab = findViewById(intActiveTab);
					setBreadcrumbText(activeTab.getTag().toString());
				}

				return;

			}

			//other fragments: set screen title to fragment breadcrumb extra
			Bundle data = visibleFragment.getArguments();

			// Get the extras
			if (data.containsKey(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE)) {

				String strTitle = data.getString(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE);
				setBreadcrumbText(strTitle);

			}
		}

	}

	private void recreateHomeFragmentWithDelay() {


		new Handler().postDelayed(new Runnable() {
			public void run() {
				loadUrlForViewInWebViewFragment(R.id.navigation_Home);
				hidePleaseWait();
			}
		}, 500);

	}

	public void showPleaseWait(String strMessage) {

		lytPleaseWaitDark.setVisibility(View.VISIBLE);
		AppAnimations.getInstance().pulseAnimation(pleaseWaitDark_ivPleaseWait, R.anim.pulse);
		pleaseWaitDark_tvPleaseWait.setText(strMessage);

	}

	public void hidePleaseWait() {

		lytPleaseWaitDark.setVisibility(View.INVISIBLE);
	}

	private void goToDeepLinkFragment() {

		/*
		    We need to wait for onStart for deep links to be opened - deep link fires way earlier than onStart
		    and at that point the system is still to add the WebShopFragment for the home screen
		    This leads to deep links not being displayed in the foreground.
		 */

		if (strDeepLinkTargetFragmentName == null) {
			return;
		}

		if (V3FragmentOrderTracking.getLogTag().equals(strDeepLinkTargetFragmentName)) {
			addOrderTrackingFragment(false);
		}

		strDeepLinkTargetFragmentName = null;

	}

	public void setProgressBar(int intProgress) {

		Fragment visibleFragment = getTopVisibleFragment();

		//only show progress bar when WebShop fragment is the top visible fragment
		if (visibleFragment instanceof V3FragmentWebShop) {
			main_pbLoadProgress.setVisibility(View.VISIBLE);
		} else {
			main_pbLoadProgress.setVisibility(View.GONE);
		}

		main_pbLoadProgress.setProgress(intProgress);

		if (intProgress == 100) {
			//Page loaded .. hide progress bar
			main_pbLoadProgress.setVisibility(View.GONE);
			main_headerDivider.setVisibility(View.VISIBLE);
		}

	}

	private void showHeaderDividerForShopFragment() {

		Fragment myFragment = getTopVisibleFragment();
		if (myFragment instanceof V3FragmentWebShop) {
			main_headerDivider.setVisibility(View.VISIBLE);
		} else {
			main_headerDivider.setVisibility(View.INVISIBLE);
		}

	}


	public void resetCoordinatorLayout() {

		Fragment topVisibleFragment = getTopVisibleFragment();

		//reset coordinator layout on all views but webShop / webView
		if (!(topVisibleFragment instanceof V3FragmentWebShop)) {
			main_lytTopElements.setExpanded(true);
		}

	}

	private void removeMyAccountFragments() {

		//only do the following if we currently are on the MyAccount tab before switching tabs
		if (getActiveTab() != R.id.navigation_Account) {
			return;
		}

		if (V3FragmentOrderTracking.instance() != null
				&& V3FragmentOrderTracking.instance().isAdded()) {
			removeSingleFragment(V3FragmentOrderTracking.instance());
		}

		if (V3FragmentUserLoginMFA.instance() != null
				&& V3FragmentUserLoginMFA.instance().isAdded()) {
			removeSingleFragment(V3FragmentUserLoginMFA.instance());
		}

		if (FragmentMyAccount.instance() != null
				&& FragmentMyAccount.instance().isAdded()) {
			removeSingleFragment(FragmentMyAccount.instance());
		}

	}



	private void showAdditionalInfo() {

		String strAdditionalInfo = "";

		//Check if app is redirected to a test server
		strAdditionalInfo = AppUtils.REDIRECT_APP_TO_TEST_SERVER;

		main_tvInfo.setText(strAdditionalInfo);

		if (!"".equals(strAdditionalInfo)) {
			main_tvInfo.setVisibility(View.VISIBLE);
		} else {
			main_tvInfo.setVisibility(View.GONE);
		}

	}

	private boolean scanForAppLinking() {

		//This method is to handle auto-verified deep links (http links mapped via App-Linking)

		Intent appLinkIntent = getIntent();
		String appLinkAction = appLinkIntent.getAction();
		Uri appLinkUri = appLinkIntent.getData();

		if (appLinkUri == null) {
			//nothing to do
			return false;
		} else {
			return onDeepLinkDetected(appLinkUri);
		}


	}

	public void showAppReviewPromptActivity(boolean blnForceShow) {

		//AppUtils.showCustomToast(ObjAppUser.instance().toXML(), false);

		//check if the user already was presented with this dialog - and if so, what their response was
		String strPreviousAppReviewResponse = ObjAppUser.instance().getString(ObjAppUser.FIELD_APP_REVIEW_PROMPT_RESPONSE);

		//if user has already seen this dialog and responded to it ... check if there is a point showing it again:
		if (blnForceShow == false) {
			if (ObjAppUser.CONST_RATE_US_PROMPT_ACCEPTED.equals(strPreviousAppReviewResponse)
					|| ObjAppUser.CONST_RATE_US_PROMPT_DECLINED.equals(strPreviousAppReviewResponse)
					|| ObjAppUser.CONST_THUMBS_DOWN.equals(strPreviousAppReviewResponse)
			) {
				//Review (likely) given or declined to review the app - or did not like the app previously
				return;
			}
		}


		// Exceptions have been reported when showing this alert ..
		// In case an exception is thrown, just ignore and continue
		try {
			Intent intent = new Intent(this, Alert.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			//intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION); //this makes sure activities do not slide in from right to left when started, but simply appear
			startActivityForResult(intent, 0);
		} catch (Exception e) {
			Logger.printMessage(logTag, "showAppReviewPromptActivity: Error when showing Alert", Logger.ERROR);
		}


		//store the date of this ..
		String strSystemDate = DateUtils.dateToInternalString(new Date());
		ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_APP_REVIEW_PROMPT_DISPLAY_DATE, strSystemDate);


	}

	public void performAccountAction(int intClickedViewId) {


		switch (intClickedViewId) {

			case R.id.navigation_itemL2ManageAccount:
			case R.id.navigation_itemL2Wishlist:
			case R.id.navigation_itemL2Currency:
			case R.id.navigation_itemL2DeliveryDestination:
			case R.id.navigation_itemL2Help:

				//Previously:
				// loadUrlForViewInWebViewFragment(intClickedViewId);
				//now:
				loadUrlInWebShopFragment(intClickedViewId);

				break;
			case R.id.navigation_itemL2MyOrders:
				addOrderTrackingFragment(false);
				break;
			default:
				break;

		}

	}

	public void showSearchTextView() {
		nativeSearchBar.setVisibility(View.VISIBLE);
	}

	public void toggleSearchBar(boolean blnForceHide) {

		//if(blnForceHide || nativeSearchBar.getVisibility()==View.VISIBLE){
		if (blnForceHide || blnAppBarFullyExpanded) {

			//nativeSearchBar.setVisibility(View.GONE);
			webViewAutoScrollToTopAndShowHideSearchbar(null, false, true);
			nativeSearch_lytResult.setVisibility(View.GONE);
			nativeSearch_ivTextSearchClear.setVisibility(View.GONE);
			nativeSearch_etQueryString.setText("");
			nativeSearch_etQueryString.clearFocus();
			hideVirtualKeyboard();

		} else {
			//nativeSearchBar.setVisibility(View.VISIBLE);
			webViewAutoScrollToTopAndShowHideSearchbar(null, true, true);
			nativeSearch_lytResult.setVisibility(View.VISIBLE);
			nativeSearch_ivTextSearchClear.setVisibility(View.GONE);
			showVirtualKeyboard();
		}
	}

	private void displayLayout(View viewToShow) {

		nativeSearch_lytResult.setVisibility(View.GONE);

		//Show navigation browser
		if (viewToShow.getId() == nativeSearch_lytResult.getId()) {

			AppTagManager.instance().sendVersion2ScreenView("App - search");

			//start with clean view every time the search screen is shown after the nav screen
			mLastKnownTypeaheadResponses = null;
			updateQueryResult(null);

			//nativeSearch_tvCancel.setVisibility(View.VISIBLE);
			//let user do this as he may wish to browse recent searches only:
			//nativeSearch_etQueryString.requestFocus();
		}

		viewToShow.setVisibility(View.VISIBLE);

	}

	/* App-wide search */

	private void setupNativeSearchWidgets() {

		//get device language (used to reduce number of minimum characters required on typeahead search for Chinese language)
		mDeviceLanguage = Locale.getDefault().getLanguage();

		//If this device is on Chinese language - reduce number of minimum characters required for a search to 1
		if (Locale.CHINESE.getLanguage().equals(mDeviceLanguage)
				|| getString(R.string.str_locale_country_iso_code_cn).equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY))
		) {

			//For Chinese we only require 1 character minimum for our text watcher
			intMinCharsForTextWatcher = 0;

		} else {

			//all other languages: we require 3 characters minimum for our text watcher
			intMinCharsForTextWatcher = 2;

		}

		nativeSearch_etQueryString.addTextChangedListener(mTypeaheadTextWatcher);

		nativeSearch_etQueryString.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {

				if (hasFocus) {

					//User opens search view by clicking into the EditText
					AppTagManager.instance().sendVersion2ScreenView("App - search");
					AppTagManager.instance().sendV2EventTags("linkclick", "App - search", "Search Box", "Initiate Search");

					//nativeSearch_etQueryString.setText(""); //leave text as user might still need previous search result - user can press "x" to clear text
					//whenever EditText gets the focus - display search layout
					displayLayout(nativeSearch_lytResult);
					updateQueryResult(null);

					showContextualBackButton();
				}

			}

		});

		//handle "Go" (Enter) click on keyboard
		nativeSearch_etQueryString.setOnEditorActionListener(new TextView.OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

				if (actionId == EditorInfo.IME_ACTION_GO) {

					//Event tag for executing a search
					AppTagManager.instance().sendV2EventTags("formSubmit", "App - search", "Search Box", "Search");

					//if user presses enter whilst typing in a search string - build query string
					//http://www.wiggle.co.uk?s=texttheuserentered
					//and open in webview

					String strUserKeyword = nativeSearch_etQueryString.getText().toString();

					//upper case this for API
					String strQueryKeyword = strUserKeyword.toUpperCase();

					//execute search in browser
					excecuteSearchForKeyword(strQueryKeyword);

					//user executed this search deliberately - so store it for later use
					saveAsRecentSearch(strUserKeyword);


					return true;
				}
				return false;
			}


		});


	}


	/* Native search handling */


	private final TextWatcher mTypeaheadTextWatcher = new TextWatcher() {

		public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

		}

		public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

			//do nothing if text was deleted
			if (count <= before) { //<= as sometimes counts get equal even though no change perceived for user
				return;
			}


			if (charSequence.length() > 0) {

				onKeywordTextEntered();


				if (charSequence.length() > intMinCharsForTextWatcher) {


					String strQueryKeyword = charSequence.toString().toUpperCase();

					submitAjaxRequest(strQueryKeyword);

				}

			}

		}

		public void afterTextChanged(Editable s) {

			if (s.length() == 0) {
				//clear last search and display empty list
				mLastKnownTypeaheadResponses = null;
				updateQueryResult(null);
			}
		}

	};


	private void onKeywordTextEntered() {

		nativeSearch_ivTextSearchClear.setVisibility(View.VISIBLE);

	}

	private void onKeywordTextCleared() {

		nativeSearch_ivTextSearchClear.setVisibility(View.GONE);
		nativeSearch_etQueryString.setText("");

		//clear last search and display empty list
		mLastKnownTypeaheadResponses = null;
		updateQueryResult(null);

		showVirtualKeyboard();

	}

	public void updateQueryResult(List<ObjTypeaheadResponse> lstTypeaheadResponse) {

		mUpdateTextSearchListviewTask = new UpdateTextSearchListviewTask(lstTypeaheadResponse);
		runOnUiThread(mUpdateTextSearchListviewTask);

	}


	private class UpdateTextSearchListviewTask implements Runnable {

		private List<ObjTypeaheadResponse> resultList;
		private List<ObjTypeaheadResponse> displayList = new ArrayList<ObjTypeaheadResponse>();

		public UpdateTextSearchListviewTask(List<ObjTypeaheadResponse> resultList) {
			this.resultList = resultList;

			if (this.resultList == null) {
				this.resultList = mLastKnownTypeaheadResponses;
			}
			//save this .. used in combination with saved searches
			if (resultList != null) {
				mLastKnownTypeaheadResponses = resultList;
			}
		}


		@Override
		public void run() {


			if (resultList != null && resultList.size() > 0) {

				//loop through result list and pick items in the desired order
				for (int i = 0; i < mTypeadheadSortOrder.size(); i++) {
					String strType = mTypeadheadSortOrder.get(i);
					for (int j = 0; j < resultList.size(); j++) {
						ObjTypeaheadResponse result = resultList.get(j);
						if (result.isRecordOfType(strType)) {
							displayList.add(result);
						}
					}
				}

				//fallback - any results that we did not know about - add them to the end of the list
				for (int i = 0; i < resultList.size(); i++) {
					ObjTypeaheadResponse result = resultList.get(i);
					if (!displayList.contains(result)) {
						displayList.add(result);
					}
				}

			}


			//always add recent searches to list
			displayList.addAll(getRecentSearches());

			//show display list (combination of search result and recent searches) if not empty
			if (displayList.size() > 0) {

				//Add dummy item that is used to make the list scrollable above the keyboard
				ObjTypeaheadResponse dummyFooter = new ObjTypeaheadResponse();
				dummyFooter.setField(ObjTypeaheadResponse.FIELD_DISPLAY_TYPE, "DummyFooter");
				displayList.add(dummyFooter);

				mTextSearchListAdapter.setListContent(displayList);

				mTextSearchListAdapter.notifyDataSetChanged();
				nativeSearch_lvSearchResult.setSelection(0); //scroll back to top on each new search result

				nativeSearch_lvSearchResult.setVisibility(View.VISIBLE);


			} else {

				nativeSearch_lvSearchResult.setVisibility(View.GONE);
				textSearch_tvGroupHeader.setText("");

			}


		}

	}


	private void setListViewScrollAdapter() {


		nativeSearch_lvSearchResult.setOnScrollListener(new AbsListView.OnScrollListener() {

			@Override
			public void onScroll(AbsListView listView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

				if (nativeSearch_lvSearchResult != null && nativeSearch_lvSearchResult.getCount() > 0) {

					int intFirstVisibleItem = nativeSearch_lvSearchResult.getFirstVisiblePosition();

					ObjTypeaheadResponse response = (ObjTypeaheadResponse) nativeSearch_lvSearchResult.getItemAtPosition(intFirstVisibleItem);

					if (response.getParentRecord() != null) {
						response = response.getParentRecord();
					}

					String strDisplayType = response.getString(ObjTypeaheadResponse.FIELD_DISPLAY_TYPE);
					if (ObjTypeaheadResponse.CONST_DISPLAY_TYPE_PARENT.equals(strDisplayType)) {

						String strGroupHeader = response.getString(ObjTypeaheadResponse.FIELD_NAME);
						nativeSearch_dynamicHeader.setVisibility(View.VISIBLE);
						textSearch_tvGroupHeader.setText(strGroupHeader);

					}


				} else {

					nativeSearch_dynamicHeader.setVisibility(View.GONE);

				}


			}

			@Override
			public void onScrollStateChanged(AbsListView arg0, int arg1) {
				// TODO Auto-generated method stub

			}

		});

	}

	private void showVirtualKeyboard() {

		nativeSearch_etQueryString.requestFocus();
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(nativeSearch_etQueryString, InputMethodManager.SHOW_IMPLICIT);

	}

	public String getSearchTerm() {

		String strSearchTerm = nativeSearch_etQueryString.getText().toString().toUpperCase();
		return strSearchTerm;

	}


	public List<ObjTypeaheadResponse> getRecentSearches() {

		//a dummy list of typeahead responses ..
		List<ObjTypeaheadResponse> lstRecentSearches = new ArrayList<ObjTypeaheadResponse>();
		List<ObjTypeaheadResponse> displayList = new ArrayList<ObjTypeaheadResponse>();


		//get the list of saved searches
		lstRecentSearches = ObjRecentSearchesManager.getInstance().getRecentSearchesForDisplay();

		if (lstRecentSearches.size() > 0) {

			//Title
			ObjTypeaheadResponse dummyRecentSearchHeading = new ObjTypeaheadResponse();
			dummyRecentSearchHeading.setField(ObjTypeaheadResponse.FIELD_NAME, getString(R.string.tab_shop_your_recent_searches));
			dummyRecentSearchHeading.setField(ObjTypeaheadResponse.FIELD_DISPLAY_TYPE, ObjTypeaheadResponse.CONST_DISPLAY_TYPE_PARENT);
			displayList.add(dummyRecentSearchHeading);

			//Recent search items
			displayList.addAll(lstRecentSearches);

			//Clear Recent Searches button
			ObjTypeaheadResponse dummyClearSearchesItem = new ObjTypeaheadResponse();
			dummyClearSearchesItem.setField(ObjTypeaheadResponse.FIELD_NAME, getString(R.string.tab_shop_clear_recent_searches));
			dummyClearSearchesItem.setField(ObjTypeaheadResponse.FIELD_DISPLAY_TYPE, ObjTypeaheadResponse.CONST_DISPLAY_TYPE_BUTTON);
			displayList.add(dummyClearSearchesItem);

		}

		return displayList;

	}


	public void useRecentSearch(String strKeyword) {

		//Report keyword entered by user (translation not really an option here)
		AppTagManager.instance().sendV2EventTags("linkclick", "App - search", "Recent Search History", strKeyword);


		//setting the text automatically will trigger the ajax search to update the search result list
		nativeSearch_etQueryString.setText(""); //clear keyword first
		nativeSearch_etQueryString.setText(strKeyword); //use stored keyword

		//open search in web shop browser
		excecuteSearchForKeyword(strKeyword);

		//store the fact that this search was re-used
		ObjUserSearch search = new ObjUserSearch(strKeyword);
		//persist in manager (addIfMissing adjusts the manager and persists its data)
		ObjRecentSearchesManager.getInstance().addIfMissing(search);

	}


	private void submitAjaxRequest(String strKeyword) {

		try {

			String strGETQueryString = "s=" + URLEncoder.encode(strKeyword, "UTF-8");

			new ObjApiRequest(
					V3HybridActivity.instance(),
					null,
					AppRequestHandler.ENDPOINT_TYPEAHEAD,
					Method.GET,
					null,
					strGETQueryString,
					ON_RESPONSE_ACTION.NO_ACTION
			).submit();

		} catch (UnsupportedEncodingException e) {
			Logger.printMessage(logTag, "" + e.getMessage(), Logger.ERROR);
		}

	}

	private void saveAsRecentSearch(String strSearchTerm) {

		//Do not store empty search terms
		if (strSearchTerm == null || "".equals(strSearchTerm)) {
			return;
		}

		ObjUserSearch recentSearch = new ObjUserSearch(strSearchTerm);

		ObjRecentSearchesManager.getInstance().addIfMissing(recentSearch);

		updateQueryResult(null);

	}

	private void excecuteSearchForKeyword(String strQueryKeyword) {

		if (strQueryKeyword == null || "".equals(strQueryKeyword)) {
			V3HybridActivity.instance().hideVirtualKeyboard();
			return;
		}

		try {

			String strQuery = URLEncoder.encode(strQueryKeyword, "UTF-8");
			String strTargetURL = AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home)) + "?s=" + strQuery;
			loadSearchResultInWebView(strTargetURL);

		} catch (UnsupportedEncodingException e) {
			Logger.printMessage(logTag, "" + e.getMessage(), Logger.ERROR);
		}

		V3HybridActivity.instance().hideVirtualKeyboard();

	}

	public void clearRecentSearches() {

		ObjRecentSearchesManager.getInstance().clearPersistedData();
		updateQueryResult(null);


	}

	public void showContextualBackButton() {

		//We basically execute onBackPressed - but without pressing the back button to find out
		//if there is back navigation available

		//Do this with slight delay to ensure fragment transactions were completed
		new Handler().postDelayed(new Runnable() {
			public void run() {

				boolean blnBackNavigationAvailable = navigationCanGoBack(false);
				if (blnBackNavigationAvailable) {
					navBar_btnBack.setVisibility(View.VISIBLE);
				} else {
					navBar_btnBack.setVisibility(View.GONE);
				}

				updateFragmentDebugList();

			}
		}, 100);


	}

	private void toggleBackStackList() {

		if (lytFragmentBackStack.getVisibility() == View.VISIBLE) {
			lytFragmentBackStack.setVisibility(View.GONE);
		} else {
			lytFragmentBackStack.setVisibility(View.VISIBLE);
			updateFragmentDebugList();
		}

	}


	public void updateFragmentDebugList() {

		if (AppUtils.showFragmentDebugList()) {

			mUpdateFragmentDebugListTask = new UpdateFragmentDebugListTask();
			runOnUiThread(mUpdateFragmentDebugListTask);

		}

	}


	private class UpdateFragmentDebugListTask implements Runnable {


		@Override
		public void run() {

			List<Fragment> fragmentList = fm.getFragments();
			mFragmentBackStackAdapter.setListContent(fragmentList);
			mFragmentBackStackAdapter.notifyDataSetChanged();
		}

	}

	@Override
	public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
		if (offset == 0) {
			// App bar is fully expanded
			blnAppBarFullyExpanded = true;
		} else {
			// App bar is not fully expanded or collapsed
			blnAppBarFullyExpanded = false;

		}
	}

	public void webViewAutoScrollToTopAndShowHideSearchbar(WebView webView, boolean blnExpand, boolean blnAnimate) {

		//scroll to the end of NestedScrollView
		main_lytTopElements.setExpanded(blnExpand, blnAnimate);

		if (webView != null) {
			webView.post(new Runnable() {
				@Override
				public void run() {
					webView.scrollTo(0, 0);
				}
			});
		}

	}

	public boolean hideNavigationScreenIfDisplayed() {

		if (main_navigationFragmentContainer.getVisibility() == View.VISIBLE) {

			//Exception: search result is shown on top of our visible navigation: just close the search
			if (nativeSearch_lytResult.getVisibility() == View.VISIBLE) {
				toggleSearchBar(true);
				return false;
			}

			//main_navigationFragmentContainer.setVisibility(View.INVISIBLE);
			fadeOutAnimation(main_navigationFragmentContainer, 300);
			showHeaderDividerForShopFragment();
			return true;
		}

		return false;

	}

	public void showNavigationScreen() {

		fadeInAnimation(main_navigationFragmentContainer, 300);
		//main_navigationFragmentContainer.setVisibility(View.VISIBLE);
		showHeaderDividerForShopFragment();
		//navigation_Search.performClick();

	}

	private void toggleAppMenu() {

		if (main_navigationFragmentContainer.getVisibility() == View.VISIBLE) {
			hideNavigationScreenIfDisplayed();
		} else {
			toggleSearchBar(true);
			addNavigationFragment(false);
		}

	}

	private void fadeOutAnimation(View viewToFadeOut, long duration) {

		Animation fadeOut = AnimationUtils.loadAnimation(this, R.anim.activity_left_slide_out);
		fadeOut.setDuration(duration);
		fadeOut.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {

				viewToFadeOut.setVisibility(View.INVISIBLE);
				//show contextual back button again after animation ends if required ...
				if (viewToFadeOut == main_navigationFragmentContainer) {
					showContextualBackButton();
				}
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}
		});
		viewToFadeOut.startAnimation(fadeOut);
		/*
		ObjectAnimator fadeOut = ObjectAnimator.ofFloat(viewToFadeOut, "alpha", 1f, 0f);
		fadeOut.setDuration(duration);
		fadeOut.addListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator animation) {
				// We wanna set the view to GONE, after it's fade out. so it actually disappear from the layout & don't take up space.
				viewToFadeOut.setVisibility(View.INVISIBLE);
			}
		});
		fadeOut.start();

		 */
	}

	private void fadeInAnimation(View viewToFadeIn, long duration) {

		Animation fadeIn = AnimationUtils.loadAnimation(this, R.anim.activity_left_slide_in);
		fadeIn.setDuration(duration);
		fadeIn.setAnimationListener(new Animation.AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
				viewToFadeIn.setVisibility(View.VISIBLE);
			}

			@Override
			public void onAnimationEnd(Animation animation) {

			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}
		});
		viewToFadeIn.startAnimation(fadeIn);
		/*
		ObjectAnimator fadeIn = ObjectAnimator.ofFloat(viewToFadeIn, "alpha", 0f, 1f);
		fadeIn.setDuration(duration);

		fadeIn.addListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationStart(Animator animation) {
				// We wanna set the view to VISIBLE, but with alpha 0. So it appear invisible in the layout.
				viewToFadeIn.setVisibility(View.VISIBLE);
				viewToFadeIn.setAlpha(0);
			}
		});
		fadeIn.start();

		 */
	}


	private void checkForRequiredAppUpgrade() {

		//Check if the user needs to upgrade their app
		new ObjApiRequest(this,
				null,
				AppRequestHandler.ENDPOINT_UPGRADE_CHECK,
				Request.Method.GET,
				null, //temporary login user as opposed to application user
				null,
				AppRequestHandler.ON_RESPONSE_ACTION.NO_ACTION)
				.submit();
	}

	private void showUpgradeRequiredMessage() {

		Intent intent = new Intent(this, UpdateRequiredActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		//intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION); //this makes sure activities do not slide in from right to left when started, but simply appear
		startActivityForResult(intent, 0);

	}


	private void addSalesForceInAppMessageListener() {

	    /*
	        We are not using any of these for now, but added for future purposes

	        The way the SalesForce in-App message works is as follows:
	        - App in foreground: silent push is sent to device salesforce SDK is notified that
	           there is an in-app message on the server
	        - App in background coming to foreground: salesforce SDK synch checks for new in-App
	           messages and finds that there is one waiting on the server
	        - in both scenarios, the app is already open when the SDK learns that there is
	           a new in-App message. The only configurable trigger to display the message in
	           Journey Builder is "when app is opened", so the in-App message will only show
	           the next time the app is being opened rather than immediately when the SDK finds
	           out that there is a new message waiting
	        - App is opened again: SalesForce SDK recognises the launcher-Activity and adds
	           the modal activity on top of that with a small delay it seems so that any remaining
	           UI can render. Our WElcomeActivity is that launcher activity
	         - The below listeners are called eventually when V3HybridActivity is loaded


	     */

		myInAppListener = new InAppMessageManager.EventListener() {
			@Override
			public boolean shouldShowMessage(@NonNull InAppMessage inAppMessage) {
				Logger.printMessage(logTag, "This is a SalesForce InAppMessage! (should Show)", Logger.INFO);
				return true; //return true to display the message, false to suppress the mesage
			}

			@Override
			public void didShowMessage(@NonNull InAppMessage inAppMessage) {
				Logger.printMessage(logTag, "This is a SalesForce InAppMessage! (did Show)", Logger.INFO);
			}

			@Override
			public void didCloseMessage(@NonNull InAppMessage inAppMessage) {
				Logger.printMessage(logTag, "This is a SalesForce InAppMessage! (did Close)", Logger.INFO);

			}
		};

		//React to display of in-app message
		//Regsiter this token with SalesForce marketing cloud
		MarketingCloudSdk.requestSdk(new MarketingCloudSdk.WhenReadyListener() {
			@Override
			public void ready(MarketingCloudSdk marketingCloudSdk) {
				Logger.printMessage(logTag, "Setting myInAppListener", Logger.INFO);
				marketingCloudSdk.getInAppMessageManager().setInAppMessageListener(myInAppListener);
			}
		});

	}
}
