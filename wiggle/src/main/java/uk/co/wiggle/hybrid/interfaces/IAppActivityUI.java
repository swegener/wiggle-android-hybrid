package uk.co.wiggle.hybrid.interfaces;

/**
 * 
 * @author AndroMedia
 * 
 * All Activities should implement this interface due to use of custom font types and custom action bar
 *
 */
public interface IAppActivityUI {
	
	
	public void setupCustomActionBar();

	public void setCustomFontTypes();
	
	public void findViews();
		
}
