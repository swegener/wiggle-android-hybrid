package uk.co.wiggle.hybrid.application.utils;


import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.content.Context;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.datamodel.objects.CustomObject;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.logging.Logger;

/**
 * 
 * @author AndroMedia
 * 
 * This class handles Date conversions
 *
 */

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class DateUtils {

    private static String logTag = "DateUtils";

    public static final String getGUITimeOnly(String strInternalDate){

        Date startDate = new Date();

        SimpleDateFormat sourceFormat = new SimpleDateFormat(ApplicationContextProvider.getContext().getString(R.string.str_server_date_format));
        try {
            startDate = sourceFormat.parse(strInternalDate);
            SimpleDateFormat targetFormat = new SimpleDateFormat(("HH:mm"));
            return targetFormat.format(startDate);
        } catch (ParseException e) {
            Logger.printMessage(logTag, "Date conversion failed: " + e.getMessage(), Logger.ERROR);
        }

        return "";

    }

    public static final String getGUITimeOnly(Date date){

        SimpleDateFormat targetFormat = new SimpleDateFormat(("HH:mm"));
        return targetFormat.format(date);

    }

    public static final String getGUIDate(Date date){

        Context context = ApplicationContextProvider.getContext();
        return new SimpleDateFormat(context.getString(R.string.str_gui_date_format)).format(date);

    }

    public static final String getHttpParameterDate(Date date){

        return new SimpleDateFormat("yyyy-MM-dd").format(date);

    }
    
    public static final String getOrderTrackingDate(Date date){

    	Context context = ApplicationContextProvider.getContext();

    	/*
    	//final String strUserCountry = ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY);
    	String strLanguage = Locale.getDefault().getLanguage();
    	
    	//if(context.getString(R.string.str_locale_country_iso_code_cn).equals(strUserCountry)
    	//		&& Locale.CHINESE.getLanguage().equals(strLanguage)){
    	//Add chinese date format (see basecamp https://basecamp.com/2987293/projects/12726842/todos/270415660 for Spec)
        if(Locale.CHINESE.getLanguage().equals(strLanguage)){
            DateFormat calendarDateFormat = DateFormat.getDateInstance(DateFormat.LONG, Locale.CHINA);
            String strCalendarDateFormatted = calendarDateFormat.format(date);
            SimpleDateFormat weekdayFormat = new SimpleDateFormat("EEEE");
            String strWeekday = weekdayFormat.format(date);
            return strCalendarDateFormatted + strWeekday;
    	}else{
    		SimpleDateFormat targetFormat = new SimpleDateFormat(context.getString(R.string.str_order_tracking_date_format));
    		return targetFormat.format(date);
    	}
    	*/

        //Use device's preferred date format (Version 2019.2)
        Format dateFormat = android.text.format.DateFormat.getDateFormat(context);
        String pattern = ((SimpleDateFormat) dateFormat).toLocalizedPattern();
        SimpleDateFormat targetFormat = new SimpleDateFormat(pattern);
        return targetFormat.format(date);
    	

    }

    public static final Date getDateFromInternalString(String strInternalDate){

        Date resultDate = new Date();

        SimpleDateFormat sourceFormat = new SimpleDateFormat(ApplicationContextProvider.getContext().getString(R.string.str_server_date_format));
        try {
            resultDate = sourceFormat.parse(strInternalDate);
            return resultDate;
        } catch (ParseException e) {
            Logger.printMessage(logTag, "Date conversion failed: " + e.getMessage(), Logger.ERROR);
        }

        //something went wrong - return now
        return new Date();
    }

    public static final String dateToInternalString(Date date){

        Context context = ApplicationContextProvider.getContext();
        return new  SimpleDateFormat(context.getString(R.string.str_server_date_format)).format(date);

    }



    public static final boolean isDateToday(String strDateToCheck){

        Calendar c1 = Calendar.getInstance(); // today

        Calendar c2 = Calendar.getInstance();
        c2.setTime(getDateFromInternalString(strDateToCheck)); // date to check

        if (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)
                && c1.get(Calendar.DAY_OF_YEAR) == c2.get(Calendar.DAY_OF_YEAR)) {
            //this is today, so return true
            return true;
        }else{
            return false;
        }

    }

    public static final String getDefaultGUIDate(String strInternalDate){
    	
    	Context context = ApplicationContextProvider.getContext();
    	
    	String strGUIDate = getGUIDate(strInternalDate, context.getString(R.string.str_server_date_format), context.getString(R.string.str_gui_date_time_format));
    	return strGUIDate;
    	
    }
 
    public static final String getGUIDateFromRSSDate(String strRSSDate){
    	
    	Context context = ApplicationContextProvider.getContext();
    	
    	Date myDate = new Date();
    	//This is the standard date format and should apply to most RSS feeds
    	SimpleDateFormat sourceFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH); //TODO: adjust this locale to current market
    	String strTargetDateFormat = context.getString(R.string.str_gui_date_time_format);
    	SimpleDateFormat targetFormat = new SimpleDateFormat(strTargetDateFormat);
    	
    	 try {
    		 //first try - work with RSS standard format. If this does not work - use alternative known formats
    		 myDate = sourceFormat.parse(strRSSDate);
             return targetFormat.format(myDate);
             
         } catch (ParseException e) {
        	 
        	 //Adidas newsfeed: they use a UK date format (e.g. 01/11/2015 19:15:00)
        	 SimpleDateFormat adidasFormat = new SimpleDateFormat("dd/mm/yyyy HH:mm:ss", Locale.ENGLISH);
        	 try {
        		 myDate = adidasFormat.parse(strRSSDate);
                 return targetFormat.format(myDate);
        	 } catch (ParseException eAdidas) {
        		 Logger.printMessage(logTag, "Adidas Date conversion failed: " + eAdidas.getMessage(), Logger.WARN);
        		 
        		 //Some feeds have got date only format ... 
        		 SimpleDateFormat dayOnlyFormat = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
        		 try {
            		 myDate = dayOnlyFormat.parse(strRSSDate);
                     return targetFormat.format(myDate);
            	 } catch (ParseException eDateOnly) {
            		 
            		 Logger.printMessage(logTag, "Date only conversion failed: " + eAdidas.getMessage(), Logger.WARN);
            		 
            		 //Some feeds have a TZ format (yyyy-MM-dd'T'HH:mm:ss'Z' for now - might need to add yyyy-MM-dd'T'HH:mm:ss.SSS'Z' later)
            		 SimpleDateFormat tzFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
            		 try {
                		 myDate = tzFormat.parse(strRSSDate);
                         return targetFormat.format(myDate);
                	 } catch (ParseException eTzDate) {
                		 Logger.printMessage(logTag, "getGUIDateFromRSSDate: All date conversions failed: " + eTzDate.getMessage(), Logger.ERROR);
                	 }
            		 
            	 }
        		 
        		 
        	 }
        	 
        	
         }
    	
    	return "";
    	
    }
    
    public static final Date getDateFromRSSDate(String strRSSDate){
    	
    	Context context = ApplicationContextProvider.getContext();
    	
    	Date myDate = new Date();
    	//This is the standard date format and should apply to most RSS feeds
    	SimpleDateFormat sourceFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);
    	
    	 try {
    		 //first try - work with RSS standard format. If this does not work - use alternative known formats
    		 myDate = sourceFormat.parse(strRSSDate);
             return myDate;
             
         } catch (ParseException e) {
        	 
        	 //Adidas newsfeed: they use a UK date format (e.g. 01/11/2015 19:15:00)
        	 SimpleDateFormat adidasFormat = new SimpleDateFormat("dd/mm/yyyy HH:mm:ss", Locale.ENGLISH);
        	 try {
        		 myDate = adidasFormat.parse(strRSSDate);
                 return myDate;
        	 } catch (ParseException eAdidas) {
        		 Logger.printMessage(logTag, "Adidas Date conversion failed: " + eAdidas.getMessage(), Logger.WARN);
        		 
        		 //Some feeds have got date only format ...
        		 SimpleDateFormat dayOnlyFormat = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
            	 try {
            		 myDate = dayOnlyFormat.parse(strRSSDate);
                     return myDate;
            	 } catch (ParseException eDateOnly) {
            		
            		 Logger.printMessage(logTag, "Day only conversion failed: " + eAdidas.getMessage(), Logger.WARN); 
            		 
            		 //Some feeds have a TZ format (yyyy-MM-dd'T'HH:mm:ss'Z' for now - might need to add yyyy-MM-dd'T'HH:mm:ss.SSS'Z' later)
            		 SimpleDateFormat tzFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
            		 try {
                		 myDate = tzFormat.parse(strRSSDate);
                         return myDate;
                	 } catch (ParseException eTzDate) {
                		 Logger.printMessage(logTag, "getDateFromRSSDate: All date conversions failed: " + eTzDate.getMessage(), Logger.ERROR);
                	 }
            		 
            	 }
            	 
        		 
        	 }
        	
         }
    	
    	return myDate;
    	
    }
    
    public static final String getTimeAgoFromRSSDate(String strRSSDate){
    	
    	Date rssDate = getDateFromRSSDate(strRSSDate);
    	//CharSequence timeAgoCharSequence = android.text.format.DateUtils.getRelativeTimeSpanString(rssDate.getTime());
    	//the following returns "10 seconds ago" for anything < 1 minute
    	CharSequence timeAgoCharSequence = android.text.format.DateUtils.getRelativeTimeSpanString(rssDate.getTime(), new Date().getTime(), android.text.format.DateUtils.SECOND_IN_MILLIS );
        return timeAgoCharSequence.toString();
    	
    }

    public static final String getGUIDate(String strInternalDate, String strSourceDateFormat, String strTargetDateFormat){

        Date myDate = new Date();

        SimpleDateFormat sourceFormat = new SimpleDateFormat(strSourceDateFormat);
        try {
        	myDate = sourceFormat.parse(strInternalDate);
            SimpleDateFormat targetFormat = new SimpleDateFormat(strTargetDateFormat);
            return targetFormat.format(myDate);
        } catch (ParseException e) {
            Logger.printMessage(logTag, "Date conversion failed: " + e.getMessage(), Logger.ERROR);
        }

        return "";

    }

    public static Date truncateDate(Date inDate){

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(inDate);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();

    }

    public static Date getDateFromContentfulString(CustomObject wiggleObject, String strFieldName){

        String strContentfulSourceDateString = wiggleObject.getString(strFieldName);

        Date myDate = new Date(); //TODO: defaults to today - check with Wiggle what to do in case of exceptions

        //Json date format from Contentful API can be various ...
        //For events: 2016-10-01T00:00:00+00:00
        //For menu items: 2016-10-01T00:00+00:00 (but format also seen on some events)
        SimpleDateFormat contentfulDateFormat1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZZ", Locale.ENGLISH); //TODO: adjust this locale to current market
        SimpleDateFormat contentfulDateFormat2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZZZZZ", Locale.ENGLISH); //TODO: adjust this locale to current market

        //try to parse date with different formats and see which one matches
        //Try format 1 ..
        try {
            myDate = contentfulDateFormat1.parse(strContentfulSourceDateString);
        } catch (ParseException eTzDate1) {
            Logger.printMessage(logTag, "getDateFromJson: date conversion 1 failed - trying 2: " + eTzDate1.getMessage(), Logger.ERROR);

            //Try format 2 ...
            try {
                myDate = contentfulDateFormat2.parse(strContentfulSourceDateString);
            } catch (ParseException eTzDate2) {
                Logger.printMessage(logTag, "getDateFromJson: date conversion 1 failed - trying 2: " + eTzDate2.getMessage(), Logger.ERROR);
            }

        }

        return myDate;

    }

    public static String getMinutesAndSecondsFromTimestamp(long timestamp){

        return (new SimpleDateFormat("mm:ss")).format(new Date(timestamp));

    }


}
