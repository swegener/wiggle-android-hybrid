package uk.co.wiggle.hybrid.usecase.shop.version3.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesItem;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesOrder;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.extensions.PicassoSSL;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.fragments.FragmentWebShop;
import uk.co.wiggle.hybrid.usecase.shop.version3.fragments.V3FragmentOrderTracking;
import uk.co.wiggle.hybrid.usecase.shop.version3.fragments.V3FragmentWebShop;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class V3FragmentBackStackAdapter extends BaseAdapter {

	private String logTag = "V3FragmentBackStackAdapter";

	private List<Fragment> mFragments = new ArrayList<Fragment>();

	public V3FragmentBackStackAdapter() {

	}


	public void setListContent(List<Fragment> fragments) {
		this.mFragments = fragments;
	}

	@Override
	public int getCount() {
		
		if (mFragments == null){
			return 0;
			
		}else{
			return mFragments.size();
		}
		
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	private class ViewHolder {
		
		LinearLayout lytFragmentBackStackRow;
		TextView fragmentRow_tvName;
		TextView fragmentRow_tvTag;
		TextView fragmentRow_tvVisibility;

	}		

	@Override
	public View getView(int arg0, View view, ViewGroup arg2) {
		final int position = arg0;

		final Fragment fragment = (Fragment) mFragments.get(arg0);

		//Logger.printMessage(logTag, "FRAGMENT: " + fragment.getTag().toString(), Logger.INFO);

		final ViewHolder holder;
			
		if(view==null){
			
			LayoutInflater inflater = LayoutInflater.from(ApplicationContextProvider.getContext());
			view = inflater.inflate(R.layout.fragment_backstack_row, null);
			
			holder = new ViewHolder();
			
			holder.lytFragmentBackStackRow = (LinearLayout) view.findViewById(R.id.lytFragmentBackStackRow);
			holder.fragmentRow_tvName = (TextView) view.findViewById(R.id.fragmentRow_tvName);
			holder.fragmentRow_tvName.setTypeface(AppTypefaces.instance().fontMainRegular);

			holder.fragmentRow_tvTag = (TextView) view.findViewById(R.id.fragmentRow_tvTag);
			holder.fragmentRow_tvTag.setTypeface(AppTypefaces.instance().fontMainRegular);

			holder.fragmentRow_tvVisibility = (TextView) view.findViewById(R.id.fragmentRow_tvVisibility);
			holder.fragmentRow_tvVisibility.setTypeface(AppTypefaces.instance().fontMainRegular);

			view.setTag(holder);
			
		}else{
			holder = (ViewHolder)view.getTag();		
		}


		//TODO: adjust
		String strFragmentPosition = "(" + position + ")";
		String strFragmentName = fragment.getClass().getSimpleName();
		if(fragment.getTag()!=null){
			strFragmentName = fragment.getTag().toString();
		}
		strFragmentName = fragment.getTag().toString();

		String strVisibility = "hidden";
		if(fragment.isVisible()){
			strVisibility = "visible";
		}

		String strAdded = "notAdded";
		if(fragment.isAdded()){
			strAdded = "isAdded";
		}

		String strFragmentDebugInfo = strFragmentPosition + " " + strFragmentName + "; " + strVisibility + "; " + strAdded;

		if(fragment instanceof V3FragmentWebShop){
			boolean blnCanGoBack = ((V3FragmentWebShop)fragment).webViewCanGoBack(false);
			String strCanGoBack = "canGoBack:" + blnCanGoBack;
			strFragmentDebugInfo = strFragmentDebugInfo + "; " + strCanGoBack;
		}

		holder.fragmentRow_tvName.setText(strFragmentDebugInfo);

		return view;

	}

	@Override
	public Object getItem(int arg0) {
		return mFragments.get(arg0);
	}
	
}
