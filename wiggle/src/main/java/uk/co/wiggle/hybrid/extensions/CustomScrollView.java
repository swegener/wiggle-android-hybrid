package uk.co.wiggle.hybrid.extensions;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

/**
 *
 * @author AndroMedia
 *
    This class is used as a replacement for the standard ScrollView.
    If a screen has a scrollview that embeds scrollable elements such as view pagers or maps,
    using standard ScrollView will lead to an odd user experience when a user is swiping through the ViewPager-pages
    or moving embedded maps as any slight movement upwards will trigger list scrolling as opposed to page-flipping
    in the viewpager or moving around in a map.
    This class addresses this issue.
 */

public class CustomScrollView extends ScrollView{

	private float xDistance, yDistance, lastX, lastY;

    // If built programmatically
    public CustomScrollView(Context context) {
        super(context);
        // init();
    }

    // This example uses this method since being built from XML
    public CustomScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // init();
    }

    // Build from XML layout
    public CustomScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // init();
    }
    

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        switch (ev.getAction()) {
        case MotionEvent.ACTION_DOWN:
            xDistance = yDistance = 0f;
            lastX = ev.getX();
            lastY = ev.getY();
            break;
        case MotionEvent.ACTION_MOVE:
            final float curX = ev.getX();
            final float curY = ev.getY();
            xDistance += Math.abs(curX - lastX);
            yDistance += Math.abs(curY - lastY);
            lastX = curX;
            lastY = curY;
            if (xDistance > yDistance)
                return false;
        }

        return super.onInterceptTouchEvent(ev);

    }
	
}
