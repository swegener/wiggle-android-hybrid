package uk.co.wiggle.hybrid.application.helpers;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;

/**
 *
 * @author   Created by AndroMedia IT Ltd on behalf of Chain Reaction Cycles / Wiggle UK Ltd
 *
 * @license  The code of this Application is licensed to Chain Reaction Cycles / Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 *
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class AppCountryHelper {

    private static AppCountryHelper mInstance = null;

    public static class Country {

        private int mCountryId; //the numeric country code for the Wiggle Megamenu
        private String mCountryName; //the country display name
        private String mCountryCode; //the "Wiggle internal" key for the country
        private boolean mDefaultCountry; //default country flag (displays on top of selection list)


        public Country(int intCountryId,
                       String strCountryName,
                       String strCountryCode
        ){
            this.mCountryId = intCountryId;
            this.mCountryName = strCountryName;
            this.mCountryCode = strCountryCode;
        }

        public int getmCountryId(){
            return mCountryId;
        }

        public String getCountryName(){
            return mCountryName;
        }

        public String getCountryCode(){
            return mCountryCode;
        }

        public void setDefaultCountry(){
            this.mDefaultCountry = true;
            defaultCountry = this;
        }

        public boolean isDefaultCountry(){
            return this.mDefaultCountry;
        }


    }

    private static ArrayList<Country> appCountries = new ArrayList<Country>();
    private static Country defaultCountry;

    public static AppCountryHelper instance(){

        if(mInstance==null){
            mInstance = new AppCountryHelper();
        }

        return mInstance;
    }


    public AppCountryHelper(){

        initialiseCountryList();

    }

    private void initialiseCountryList(){

        Context context = ApplicationContextProvider.getContext();

        //reset country list on device language change
        String strLastUserLanguage = ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_LANGUAGE);
        String strDeviceLanguage = Locale.getDefault().getLanguage();
        if (strLastUserLanguage != null && !"".equals(strLastUserLanguage)) {
            if (!strLastUserLanguage.equals(strDeviceLanguage)) {
                appCountries.clear();
            }
        }

        //prevent duplicate list creation
        if(appCountries.size()>0){
            return;
        }

        Country uk = new Country(1,"United Kingdom","UK");
        addCountry(uk);

        Country be = new Country(3,"Belgium","BE");
        addCountry(be);

        Country lu = new Country(4,"Luxembourg","LU");
        addCountry(lu);

        Country nl = new Country(5,"Netherlands","NL");
        addCountry(nl);

        Country ie = new Country(6,"Irish Republic","IE");
        addCountry(ie);

        Country at = new Country(7,"Austria","AT");
        addCountry(at);

        Country dk = new Country(8,"Denmark","DK");
        addCountry(uk);

        Country de = new Country(9,"Germany","DE");
        addCountry(de);

        Country it = new Country(10,"Italy","IT");
        addCountry(it);

        Country fr = new Country(11,"France","FR");
        addCountry(fr);

        Country pt = new Country(12,"Portugal","PT");
        addCountry(pt);

        Country es = new Country(13,"Spain","ES");
        addCountry(es);

        Country se = new Country(14,"Sweden","SE");
        addCountry(se);

        Country cz = new Country(15,"Czech Republic","CZ");
        addCountry(cz);

        Country sk = new Country(16,"Slovakia","SK");
        addCountry(sk);

        Country pl = new Country(17,"Poland","PL");
        addCountry(pl);

        Country us = new Country(18,"USA","US");
        addCountry(us);

        Country ca = new Country(19,"Canada","CA");
        addCountry(ca);

        Country lt = new Country(20,"Lithuania","LT");
        addCountry(lt);

        Country ro = new Country(23,"Romania","RO");
        addCountry(ro);

        Country ru = new Country(24,"Russia","RU");
        addCountry(ru);

        Country au = new Country(27,"Australia","AU");
        addCountry(au);

        Country bh = new Country(28,"Bahrain","BH");
        addCountry(bh);

        Country bn = new Country(29,"Brunei Darussalam","BN");
        addCountry(bn);

        Country cn = new Country(31,"China","CN");
        addCountry(cn);

        Country hk = new Country(32,"Hong Kong","HK");
        addCountry(hk);

        Country jp = new Country(33,"Japan","JP");
        addCountry(jp);

        Country my = new Country(34,"Malaysia","MY");
        addCountry(my);

        Country nz = new Country(35,"New Zealand","NZ");
        addCountry(nz);

        Country nzit = new Country(36,"New Zealand Island Territories","NZ");
        addCountry(nzit);

        Country sg = new Country(37,"Singapore","SG");
        addCountry(sg);

        Country th = new Country(38,"Thailand","TH");
        addCountry(th);

        Country eg = new Country(40,"Egypt","EG");
        addCountry(eg);

        Country sa = new Country(43,"Saudi Arabia","SA");
        addCountry(sa);

        Country ae = new Country(45,"United Arab Emirates","AE");
        addCountry(ae);

        Country mc = new Country(47,"Monaco","MC");
        addCountry(mc);

        Country no = new Country(48,"Norway","NO");
        addCountry(no);

        Country sar = new Country(49,"Sardinia","IT");
        addCountry(sar);

        Country gr = new Country(50,"Greece","GR");
        addCountry(gr);

        Country sci = new Country(51,"Sicily","IT");
        addCountry(sci);

        Country ch = new Country(52,"Switzerland","CH");
        addCountry(ch);

        Country ee = new Country(53,"Estonia","EE");
        addCountry(ee);

        Country gi = new Country(54,"Gibraltar","GI");
        addCountry(gi);

        Country hu = new Country(55,"Hungary","HU");
        addCountry(hu);

        Country si = new Country(56,"Slovenia","SI");
        addCountry(si);

        Country va = new Country(57,"Vatican City State","VA");
        addCountry(va);

        Country fi = new Country(58,"Finland","FI");
        addCountry(fi);

        Country bai = new Country(62,"Spain - Balearic Islands","ES");
        addCountry(bai);

        Country cor = new Country(63,"Corsica","FR");
        addCountry(cor);

        Country lv = new Country(64,"Latvia","LV");
        addCountry(lv);

        Country mt = new Country(66,"Malta","MT");
        addCountry(mt);

        Country sm = new Country(67,"San Marino","SM");
        addCountry(sm);

        Country tr = new Country(68,"Turkey","TR");
        addCountry(tr);

        Country tw = new Country(69,"Taiwan","TW");
        addCountry(tw);

        Country ph = new Country(70,"Philippines","PH");
        addCountry(ph);

        Country br = new Country(71,"Brazil","BR");
        addCountry(br);

        Country je = new Country(74,"Jersey","JE");
        addCountry(je);

        Country gg = new Country(75,"Guernsey","GG");
        addCountry(gg);

        Country cy = new Country(76,"Cyprus","CY");
        addCountry(cy);

        Country kr = new Country(77,"Korea","KR");
        addCountry(kr);

        Country il = new Country(79,"Israel","IL");
        addCountry(il);

        Country hr = new Country(80,"Croatia","HR");
        addCountry(hr);

        Country bg = new Country(81,"Bulgaria","BG");
        addCountry(bg);

        Country jo = new Country(85,"Jordan","JO");
        addCountry(jo);

        Country kw = new Country(87,"Kuwait","KW");
        addCountry(kw);

        Country qa = new Country(88,"Qatar","QA");
        addCountry(qa);

        Country ic = new Country(96,"Spain - Canary Islands","IC");
        addCountry(ic);

        Country is = new Country(97,"Iceland","IS");
        addCountry(is);

        Country ua = new Country(99,"Ukraine","UA");
        addCountry(ua);

        Country mo = new Country(101,"Macao","MO");
        addCountry(mo);

        Country by = new Country(102,"Belarus","BY");
        addCountry(by);

        Country kz = new Country(108,"Kazakhstan","KZ");
        addCountry(kz);


        //declare the UK as default country
        Country defaultCountry = getCountryById(1);
        defaultCountry.setDefaultCountry();
        addCountry(defaultCountry);

    }

    private void addCountry(Country country){

       appCountries.add(country);

    }

    public void reset(){
        mInstance = null;
        appCountries.clear();
    }

    public ArrayList<Country> getAll(){

        //return appCountries;

        //Sort countries by name
        SortCountries sortCountries = new SortCountries();
        Collections.sort(appCountries, sortCountries);

        return appCountries;

    }


    public Country getCountryByInternalCode(String strInternalCountryCode){

        for(int i=0;i<appCountries.size();i++){
            Country myCountry = appCountries.get(i);
            if(strInternalCountryCode.equals(myCountry.getCountryCode())){
                return myCountry;
            }
        }

        return defaultCountry;
    }

    public Country getCountryById(int intCountryId){

        for(int i=0;i<appCountries.size();i++){
            Country myCountry = appCountries.get(i);
            if(intCountryId == myCountry.getmCountryId()){
                return myCountry;
            }
        }

        return defaultCountry;
    }





    //can be used to sort by country name
    //Use
    //SortCountriesByName sortCountriesByName = new SortCountriesByName();
    //Collections.sort(countryList, sortCountriesByName);
    class SortCountries implements Comparator<Country> {

        @Override
        public int compare(Country country1, Country country2) {

            //default country on top
            int intDefaultFlagResult = Boolean.compare(country2.isDefaultCountry(), country1.isDefaultCountry());
            if(intDefaultFlagResult==0){
                return country1.getCountryName().compareTo(country2.getCountryName());
            }else {
                return intDefaultFlagResult;
            }

        }

    }

    public Country getDefaultCountry(){

        return defaultCountry;

    }

}
