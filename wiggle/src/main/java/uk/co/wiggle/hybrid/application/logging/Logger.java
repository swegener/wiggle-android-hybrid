package uk.co.wiggle.hybrid.application.logging;

import android.util.Log;

/**
 * 
 * @author AndroMedia IT Ltd
 *
 * This class can be used for logging. It uses the android default logger for logging.
 * You can enable/disbale the logging globally via {@code ENABLE_CONSOLE_DEBUG}
 * 
 * Usage example 
 * 
 *  Logger.printMessage(TAG, "Print Message", Logger.DEBUG);
 * TAG is generally a used as a static string in each class containing the class name. This is to identify
 * from which class the log was generated.
 * 
 * {@code ALL, INFO, DEBUG, WARN, ERROR } are the different logging levels provided by this class. Debug logs
 * are compiled in but stripped at runtime. Error, Warning and Info logs are always kept.
 * 
 * 
 */

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class Logger {
    /**
     * Flag to enable/disable logging
     */
    public static final boolean ENABLE_CONSOLE_DEBUG = false;

    /*
     * Debug logs are compiled in but stripped at runtime. Error, warning and info logs are always kept.
     */

    public static final int ALL = 1;
    public static final int INFO = 6;
    public static final int DEBUG = 11;
    public static final int WARN = 16;
    public static final int ERROR = 21;
    // public static final int FATAL = 26;

    public static final int CURRENT_LOGING_LEVEL = 21;

    /**
     * Method used to print logs to DDMS Logcat
     * 
     * @param TAG
     *            String to identify the log. Recomended use of class name as tag
     * @param msg
     *            Actual error message to print
     * @param logingLevel
     *            Sets the loging level of log. Can contain values {@code ALL, INFO, DEBUG, WARN, ERROR }
     */
    public static void printMessage(String TAG, String msg, int logingLevel) {
        if (ENABLE_CONSOLE_DEBUG) {
            if (logingLevel >= CURRENT_LOGING_LEVEL) {
            	if(msg == null){
            		;//Logger.e(TAG, "message is null.");
            		return;
            	}
                switch (logingLevel) {
                case INFO:
                    Log.i(TAG, msg);
                    break;
                case DEBUG:
                    Log.d(TAG, msg);
                    break;
                case WARN:
                    Log.w(TAG, msg);
                    break;
                case ERROR:
                	Log.e(TAG, msg);
                    break;

                }

            }
        }
    }
    
    public static void e(String TAG, String msg) {
    	if (ENABLE_CONSOLE_DEBUG) {
    		Log.e(TAG, msg);
    	}
    }
    
    public static void i(String TAG, String msg) {
    	if (ENABLE_CONSOLE_DEBUG) {
    		Log.i(TAG, msg);
    	}
    }
    
    public static void w(String TAG, String msg) {
    	if (ENABLE_CONSOLE_DEBUG) {
    		Log.w(TAG, msg);
    	}
    }
    
    public static void d(String TAG, String msg) {
    	if (ENABLE_CONSOLE_DEBUG) {
    		Log.d(TAG, msg);
    	}
    }
}
