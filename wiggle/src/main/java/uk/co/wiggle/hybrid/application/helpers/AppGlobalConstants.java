package uk.co.wiggle.hybrid.application.helpers;

/**
 * @author Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 *
 * @license The code of this Application is licensed to Wiggle UK Ltd.
 * The license and its ownership rights can not be transferred to a Third Party.
 *
 * @android If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 **/

public class AppGlobalConstants {

    public static final String CALLING_ACTIVITY = "CALLING_ACTIVITY";
    public static final String FRAGMENT_BREADCRUMB_TITLE = "FRAGMENT_BREADCRUMB_TITLE";
    public static final String CALLING_VIEW = "CALLING_VIEW";
    public static final String GOOGLE_DOCS_VIEWER = "https://docs.google.com/viewer?url=";
    public static final String CONTENTFUL_LOCALE_EN_GB = "en-GB";
    public static final String INSURANCE_PRODUCT_QUOTELINE_URL = "INSURANCE_PRODUCT_QUOTELINE_URL";

}
