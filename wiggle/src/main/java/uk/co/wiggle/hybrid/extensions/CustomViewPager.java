package uk.co.wiggle.hybrid.extensions;



import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import uk.co.wiggle.hybrid.application.logging.Logger;

/**
 * 
 * @author AndroMedia
 * 
 * This class is used to overcome a specific problem with the Android support library.
 * On some devices, multi-touches lead to error
 * java.lang.IllegalArgumentException: pointerIndex out of range
 * 
 * We work around this issue by overwriting the standard view pager and catching exceptions of its onInterceptTouchEvent method
 * 
 * 
 *
 */

public class CustomViewPager extends ViewPager {
	
	private static final String logTag = "CustomViewPager";
	
	public CustomViewPager(Context context) {
		super(context);
	}
	
	public CustomViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/*public boolean onInterceptTouchEvent(final MotionEvent event) {
		
		try {
	        return super.onInterceptTouchEvent(event);
	    }catch (Exception e) {
	    	//The standard view pager's onInterceptTouchEvent can lead to exceptions (pointerIndex out of range)
	    	//By catching view pager's exceptions we hope to work around this. 
	    	Logger.printMessage(logTag, "" + e.getMessage(), Logger.WARN);
	    }
		
		return false;
	
	}*/
	
	
	/**
	 * adding these two methods in a hope to resolve 
	 * illegalArgumentException (pointer out of index) usually caused by the
	 * viewpager in the photoviewer
	 * reference:  https://github.com/chrisbanes/PhotoView/issues/31 (azibug's solution)
	 */
	
	@Override
    public boolean onTouchEvent(MotionEvent ev) {
        try {
            return super.onTouchEvent(ev);
        } catch (IllegalArgumentException ex) {
        	Logger.printMessage(logTag, "" + ex.getMessage(), Logger.WARN);
            ex.printStackTrace();
        }catch(Exception e){
        	Logger.printMessage(logTag, "" + e.getMessage(), Logger.WARN);
        }
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        try {
            return super.onInterceptTouchEvent(ev);
        } catch (IllegalArgumentException ex) {
        	Logger.printMessage(logTag, "" + ex.getMessage(), Logger.WARN);
            ex.printStackTrace();
        }catch(Exception e){
        	Logger.printMessage(logTag, "" + e.getMessage(), Logger.WARN);
        }
        
        return false;
    }
	

}
