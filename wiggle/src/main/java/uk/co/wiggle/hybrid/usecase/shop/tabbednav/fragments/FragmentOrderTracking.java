package uk.co.wiggle.hybrid.usecase.shop.tabbednav.fragments;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.List;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjSalesOrderManager;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesItem;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesOrder;
import uk.co.wiggle.hybrid.application.helpers.AppGlobalConstants;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.application.session.AppSession;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.application.utils.DateUtils;
import uk.co.wiggle.hybrid.extensions.PicassoSSL;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentSelfCloseListener;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentUI;
import uk.co.wiggle.hybrid.tagging.AppTagManager;
import uk.co.wiggle.hybrid.usecase.ordertracking.adapters.OrderTrackingListAdapter;
import uk.co.wiggle.hybrid.usecase.ordertracking.helpers.OrderTrackingUtils;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.activities.TabbedHybridActivity;



/**
 * @author Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 *
 * @license The code of this Application is licensed to Wiggle UK Ltd.
 * The license and its ownership rights can not be transferred to a Third Party.
 *
 * @android If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 **/


public class FragmentOrderTracking extends Fragment implements IAppFragmentUI {

    private static final String logTag = "FragmentOrderTracking";

    public static String getLogTag(){
        return logTag;
    }


    IAppFragmentSelfCloseListener selfCloseListener;


    String strCaller = "";
    private static FragmentOrderTracking myInstance;

    public static FragmentOrderTracking instance(){
        return myInstance;
    }

    RelativeLayout lytOrderTracking;
    RelativeLayout orderTracking_orderList;
    SwipeRefreshLayout orderList_swipeRefreshLayout;
    AbsListView orderList_lvOrderHistory;
    TextView orderList_tvNoData;
    LinearLayout orderList_lytGroupHeader;
    TextView orderList_tvGroupHeader;
    ScrollView lytOrderDetail;
    TextView orderList_tvTitle;
    OrderTrackingListAdapter orderTrackingAdapter;
    private OrderTrackingTask mOrderTrackingTask = new OrderTrackingTask();


    //Order Detail
    TextView orderDetail_tvTitle;
    TextView orderDetail_tvDeliveryStatusText;
    TextView orderDetail_tvDeliveryStatusDate;
    ImageView orderDetail_ivProductImage;
    TextView orderDetail_tvProductBrandAndName;
    TextView orderDetail_tvProductVariety;
    TextView orderDetail_tvProductPrice;
    TextView orderDetail_lblDeliveryAddress;
    TextView orderDetail_tvDeliveryAddress;
    TextView orderDetail_lblOrderNumber;
    TextView orderDetail_tvOrderNumber;
    TextView orderDetail_tvMoreDetails;
    LinearLayout lytOrderTrackingGraph;


    boolean blnSuppressScrollToStart = false;

    private ObjSalesItem mActiveSalesItem;

    public ObjSalesItem getActiveSalesItem(){
        return mActiveSalesItem;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myInstance = this;


        /** Getting the arguments to the Bundle object */
        Bundle data = getArguments();

        // Get the extras
        if (!data.containsKey(AppGlobalConstants.CALLING_ACTIVITY)) {
            throw new IllegalArgumentException("The " + AppGlobalConstants.CALLING_ACTIVITY + " must be passed as extra for the intent. "
                    + "The content of the extras is " + data);
        }

        strCaller = data.getString(AppGlobalConstants.CALLING_ACTIVITY);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.order_tracking_layout, container, false);


        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

       updateFragmentContent(view);

    }


    @Override
    public void updateFragmentContent(View view) {


        findViews(view);

        setCustomFontTypes();


    }



    public void findViews(View view) {

        //Order tracking
        lytOrderTracking = (RelativeLayout) view.findViewById(R.id.lytOrderTracking);
        orderTracking_orderList = (RelativeLayout) view.findViewById(R.id.orderTracking_orderList);
        orderList_swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.orderList_swipeRefreshLayout);
        setupOrderSwipeToRefreshLayout();



        orderList_lvOrderHistory = (AbsListView) view.findViewById(R.id.orderList_lvOrderHistory);
        orderList_lytGroupHeader = (LinearLayout) view.findViewById(R.id.orderList_lytGroupHeader);
        orderList_tvGroupHeader = (TextView) view.findViewById(R.id.orderList_tvGroupHeader);

        //we need to make sure that swipe refresh is only possible if user is at the top of the listview. Otherwise swipe refresh is triggered
        //every time the user scrolls up from anywhere in the listview.
        //Thank you, http://nlopez.io/swiperefreshlayout-with-listview-done-right/
        orderList_lvOrderHistory.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                //this makes sure we dont swipe to refresh when scrolling up
                int topRowVerticalPosition =
                        (orderList_lvOrderHistory == null || orderList_lvOrderHistory.getChildCount() == 0) ? 0 : orderList_lvOrderHistory.getChildAt(0).getTop();
                orderList_swipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);

                //this handles the Order group header
                if(orderList_lvOrderHistory!=null &&
                        ObjSalesOrderManager.getInstance().getAllOrderedItems().size()>0
                        ){

                    int intFirstVisibleItem = orderList_lvOrderHistory.getFirstVisiblePosition();

                    ObjSalesItem currentItem = (ObjSalesItem) orderList_lvOrderHistory.getItemAtPosition(intFirstVisibleItem);
                    if(currentItem!=null){ //prevent NPE
                        String strOrderGroupListHeader = String.format(
                                getString(R.string.str_order_list_header_group),
                                currentItem.getParentRecord().getString(ObjSalesOrder.FIELD_ORDER_ID),
                                DateUtils.getOrderTrackingDate(currentItem.getParentRecord().getDateFromJson(ObjSalesOrder.FIELD_ORDER_DATE))
                        );
                        orderList_tvGroupHeader.setText(strOrderGroupListHeader);
                        if(AppUtils.showListviewHeaders()){
                            orderList_lytGroupHeader.setVisibility(View.VISIBLE);
                        }
                    }


                }else{

                    orderList_lytGroupHeader.setVisibility(View.GONE);

                }

            }


        });


        orderList_tvNoData = (TextView) view.findViewById(R.id.orderList_tvNoData);
        lytOrderDetail = (ScrollView) view.findViewById(R.id.lytOrderDetail);
        orderList_tvTitle = (TextView) view.findViewById(R.id.orderList_tvTitle);
        setupOrderTrackingList();

        //Order Details
        orderDetail_tvTitle = (TextView) view.findViewById(R.id.orderDetail_tvTitle);
        orderDetail_tvDeliveryStatusText = (TextView) view.findViewById(R.id.orderDetail_tvDeliveryStatusText);
        orderDetail_tvDeliveryStatusDate = (TextView) view.findViewById(R.id.orderDetail_tvDeliveryStatusDate);
        orderDetail_ivProductImage = (ImageView) view.findViewById(R.id.orderDetail_ivProductImage);
        orderDetail_tvProductBrandAndName = (TextView) view.findViewById(R.id.orderDetail_tvProductBrandAndName);
        orderDetail_tvProductVariety = (TextView) view.findViewById(R.id.orderDetail_tvProductVariety);
        orderDetail_tvProductPrice = (TextView) view.findViewById(R.id.orderDetail_tvProductPrice);
        orderDetail_lblDeliveryAddress = (TextView) view.findViewById(R.id.orderDetail_lblDeliveryAddress);
        orderDetail_tvDeliveryAddress = (TextView) view.findViewById(R.id.orderDetail_tvDeliveryAddress);
        orderDetail_lblOrderNumber = (TextView) view.findViewById(R.id.orderDetail_lblOrderNumber);
        orderDetail_tvOrderNumber = (TextView) view.findViewById(R.id.orderDetail_tvOrderNumber);
        orderDetail_tvMoreDetails = (TextView) view.findViewById(R.id.orderDetail_tvMoreDetails);
        lytOrderTrackingGraph = (LinearLayout) view.findViewById(R.id.lytOrderTrackingGraph);

        updateWidgets();

    }

    public void updateWidgets(){



    }

    public void initialiseNoDataText(){

        orderList_tvNoData.setText(R.string.orderList_tvNoData);

    }

    private void setCustomFontTypes(){

        orderList_tvGroupHeader.setTypeface(AppTypefaces.instance().fontMainRegular);
        orderList_tvNoData.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
        orderList_tvTitle.setTypeface(AppTypefaces.instance().fontMainRegularItalic);

    }



    @Override
    public void onStart() {
        super.onStart();

        setTitleBarText();

    }

    @Override
    public void onResume() {

        retrieveOrderHistory();

        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();

        //special handling for Order tracking screen:
        // if user decides to view this screen (i.e it is visible), replace the cached version of the order tracking json
        // with the latest json payload received from the API.
        //We do this when the screen is either closed or sent to background after viewing i.e. the user is finished checking his orders
        //Doing this when this screen is opened leads to subtle problems
        AppSession.instance().acceptOrderTrackingPayloadIntoCache();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);


    }



    //Check if this is working - this is the new version of deprecated
    //public void onAttach(Activity activity)
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            selfCloseListener = (IAppFragmentSelfCloseListener) context;
        } catch (ClassCastException e) {
            throw new RuntimeException(getActivity().getClass().getName() + " must implement IAppFragmentSelfCloseListener to use this fragment", e);
        }

    }


    public void onClickEvent(int intClickedViewId){

       switch (intClickedViewId) {

           case R.id.orderDetail_btnMoreDetails:
               openOrderDetailUrl();
               break;

            default:
                break;

        }

    }



    private void setupOrderTrackingList(){

        orderTrackingAdapter = new OrderTrackingListAdapter();
        orderList_lvOrderHistory.setAdapter(orderTrackingAdapter);

    }

    public void updateOrderTrackingList(){

        if(lytOrderTracking.getVisibility()==View.GONE){
            displayLayout(lytOrderTracking);
        }

        if(orderTrackingAdapter==null){
            return;
        }

        getActivity().runOnUiThread(mOrderTrackingTask);
    }


    private void displayLayout(View viewToShow){

        lytOrderTracking.setVisibility(View.GONE);
        lytOrderDetail.setVisibility(View.GONE);

        viewToShow.setVisibility(View.VISIBLE);

    }

    private class OrderTrackingTask implements Runnable {

        @Override
        public void run() {


            List<ObjSalesItem> orderedItemsList = ObjSalesOrderManager.getInstance().getAllOrderedItems();

            if(orderedItemsList.size()>0){

                orderTrackingAdapter.setListContent(orderedItemsList);
                orderTrackingAdapter.notifyDataSetChanged();

                orderList_lvOrderHistory.setVisibility(View.VISIBLE);
                orderList_tvNoData.setVisibility(View.GONE);

                if(AppUtils.showListviewHeaders()){
                    orderList_lytGroupHeader.setVisibility(View.VISIBLE);
                }

                checkForNewDeliveredOrder();

                //check if we need to show a badge in the navigation browser
                TabbedHybridActivity.instance().updateOrderTrackingBadge();

                if(!blnSuppressScrollToStart){
                    scrollToStartOfList(orderList_lvOrderHistory);
                }else{
                    blnSuppressScrollToStart = false; //reset
                }

            }else{

                orderList_lvOrderHistory.setVisibility(View.GONE);
                orderList_tvNoData.setVisibility(View.VISIBLE);

                orderList_lytGroupHeader.setVisibility(View.GONE);

            }

            orderList_swipeRefreshLayout.setRefreshing(false);

        }

    }


    private void checkForNewDeliveredOrder(){

        //If there as at least one order that changed to status "Delivered" since last refresh - show Trustpilot review prompt
        if(ObjSalesOrderManager.getInstance().getJustDeliveredOrderList().size()>0){
            ObjSalesOrder salesOrder = ObjSalesOrderManager.getInstance().getJustDeliveredOrderList().get(0);
            TabbedHybridActivity.instance().showReviewOrderDialog(salesOrder);
        }

    }


    /**
     * setUp swipeRefreshLayout to refresh current fragment data
     */
    private void setupOrderSwipeToRefreshLayout(){

        orderList_swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                //clear order manager as we want to update it
                ObjSalesOrderManager.getInstance().clear();

                //Order screen: when user pulls to refresh, the response needs accepting into the cache as soon as data arrives as user
                //views the result immediately.
                TabbedHybridActivity.instance().setAcceptOrderTrackingResponseOnSwipeRefresh(true);

                retrieveOrderHistory(); //refresh everything including news config
            }
        });


        orderList_swipeRefreshLayout.setColorSchemeResources(
                R.color.orangeButton,
                R.color.backgroundGreyDark,
                R.color.orangeButton,
                R.color.backgroundGreyDark
        );

    }

    public void onOrderedItemClick(ObjSalesItem salesItem){

        mActiveSalesItem = salesItem;

        AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_my_orders), "myordersItemStatus");

        ObjSalesOrder parentOrder = salesItem.getParentRecord();
        //AppUtils.showCustomToast("Clicked on order Id: " + parentOrder.getString(ObjSalesOrder.FIELD_ORDER_ID), true);

        showOrderDetail(parentOrder, salesItem);

    }

    private void showOrderDetail(ObjSalesOrder order, ObjSalesItem salesItem){

        AppTagManager.instance().sendVersion2ScreenView("App - Track My Order - Item Status");

        //flag this order as "viewed" (resets "updated" label)
        order.setField(ObjSalesOrder.FIELD_ORDER_NEW_INFORMATION, false);
        updateAllOrderTrackingBadges(); //update order counter badges on NavTab and MyAccount screen

        String strProductImageUrl = salesItem.getImagePath("?w=600&amp;h=600&amp;a=0");
        if(strProductImageUrl!=null && !"".equals(strProductImageUrl)){
            orderDetail_ivProductImage.setVisibility(View.VISIBLE);
            PicassoSSL.with(getContext()).load(strProductImageUrl).into(orderDetail_ivProductImage);
        }else{
            orderDetail_ivProductImage.setVisibility(View.GONE);
        }

        String strDispatchStatus = order.getDisplayDeliveryStatus();
        orderDetail_tvDeliveryStatusText.setText(strDispatchStatus);

        String strDispatchDate = order.getDisplayDeliveryDate();
        orderDetail_tvDeliveryStatusDate.setText(strDispatchDate);

        String strProductName = salesItem.getProductBrandAndName();
        orderDetail_tvProductBrandAndName.setText(strProductName);

        String strProductVariety = salesItem.getProductVarietyString();
        orderDetail_tvProductVariety.setText(strProductVariety);

        String strLocalProductPrice = salesItem.getString(ObjSalesItem.FIELD_LOCAL_LINE_TOTAL_FORMATTED);
        orderDetail_tvProductPrice.setText(strLocalProductPrice);

        String strFormattedAddress = order.getFormattedAddress();
        orderDetail_tvDeliveryAddress.setText(strFormattedAddress);

        String strOrderNumber = salesItem.getParentRecord().getString(ObjSalesOrder.FIELD_ORDER_ID);
        orderDetail_tvOrderNumber.setText(strOrderNumber);

        lytOrderDetail.setVisibility(View.VISIBLE);
        lytOrderDetail.scrollTo(0, 0); //scroll to start of list (as might have been used earlier)

        OrderTrackingUtils.showOrderStatus(lytOrderTrackingGraph, order);


    }

    private void openOrderDetailUrl(){

        TabbedHybridActivity.instance().loadUrlInWebViewFragment(mActiveSalesItem.getParentRecord().getString(ObjSalesOrder.FIELD_DETAILS_URL));

    }


    private void scrollToStartOfList(final AbsListView listView){

        //do nothing if list not initialised (crash-fix: NPE)
        if(listView==null){
            return;
        }

        //needed to move this statment to a runnable as this does not work immediately after notifydatasetchanged
        //newsfeedHome_lvListView.setSelection(0);
        listView.post(new Runnable() {

            @Override
            public void run() {
                listView.setSelection(0);
            }
        });


    }


    private void retrieveOrderHistory(){

        orderList_tvNoData.setText(R.string.orderList_tvNoData_pleaseWait);

        updateOrderTrackingList();

        if(ObjSalesOrderManager.getInstance().size()==0){
            orderList_swipeRefreshLayout.setRefreshing(true);
            AppRequestHandler.instance().retrieveOrderData(TabbedHybridActivity.instance(), AppRequestHandler.ON_RESPONSE_ACTION.SUBMIT_ORDER_TRACKING_REQUEST);
        }

    }


    public boolean orderTrackingScreenCanGoBack(){


        if(lytOrderDetail.getVisibility()==View.VISIBLE){

            orderTrackingScreenGoBack();
            return true;
        }else{

            return false;
        }


    }

    private void orderTrackingScreenGoBack(){

        lytOrderTracking.setVisibility(View.VISIBLE);
        lytOrderDetail.setVisibility(View.GONE);
    }

    private void updateAllOrderTrackingBadges(){

        //update notification badge on My Account tab
        TabbedHybridActivity.instance().updateOrderTrackingBadge();

        //update order tracking counter on My Account screen
        if(FragmentMyAccount.instance()!=null
                && FragmentMyAccount.instance().isAdded()){
            FragmentMyAccount.instance().updateOrderTrackingBadge();
        }

        //update "new" status icon on order tracking list
        updateOrderTrackingList();

    }

    @Override
    public void onHiddenChanged(boolean hidden){

        if(!hidden){
            setTitleBarText();
        }
    }

    private void setTitleBarText(){

        if(TabbedHybridActivity.instance()!=null){
            Bundle data = getArguments();

            // Get the extras
            if (data.containsKey(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE)) {

                String strTitle = data.getString(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE);
                TabbedHybridActivity.instance().setBreadcrumbText(strTitle);

            }

        }


    }
}


