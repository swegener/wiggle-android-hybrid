package uk.co.wiggle.hybrid.application.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import uk.co.wiggle.hybrid.application.logging.Logger;

public class ConnectivityChangeReceiver 
               extends BroadcastReceiver {
	
   private static final String logTag = "ConnectivityChangeReceiver";
   

   @Override
   public void onReceive(Context context, Intent intent) {

      //notify user about connection status
      ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
      NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
      
      String strMessage = "";
      
      //prevent NPEs
      if(networkInfo==null){
          //report "no network"
    	  //TODO: add required actions if no network
    	  return;
      }
      
      boolean isConnected = networkInfo.isConnected();
      
      //network available after it was unavailable? Auto-submit outstanding claim ..
      if(isConnected){

          //add action when network is back online ..

      }

   }
   
   private void debugIntent(Intent intent, String tag) {

      Logger.printMessage(logTag, "action: " + intent.getAction(), Logger.INFO);
      Logger.printMessage(logTag, "component: " + intent.getComponent(), Logger.INFO);
      
      Bundle extras = intent.getExtras();
      if (extras != null) {
         for (String key: extras.keySet()) {
        	Logger.printMessage(logTag, "key [" + key + "]: " +
                    extras.get(key), Logger.INFO);
         }
      }
      else {
    	  Logger.printMessage(logTag, "no extras", Logger.INFO);
      }
   }

}