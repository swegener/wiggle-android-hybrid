package uk.co.wiggle.hybrid.application.helpers;


import uk.co.wiggle.hybrid.application.ApplicationContextProvider;

import android.content.Context;
import android.graphics.Typeface;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class AppTypefaces {

	//Fonts
	//Main Typeface is Gotham Book - different varieties are taken from various sources
	public static String TYPEFACE_MAIN_REGULAR = "fonts/roboto-ttf/Roboto-Regular.ttf";
	public static String TYPEFACE_MAIN_BOLD = "fonts/roboto-ttf/Roboto-Bold.ttf";
	public static String TYPEFACE_MAIN_REGULAR_ITALIC = "fonts/roboto-ttf/Roboto-Italic.ttf";
	
	//Google Material Icon Font
	public static String GOOGLE_MATERIAL_ICONS_FONT = "fonts/google-material-icons-font/MaterialIcons-Regular.ttf";
	
	//Typefaces
	public Typeface fontMainRegular;
	public Typeface fontMainBold;
	public Typeface fontMainRegularItalic;
	public Typeface fontGoogleMaterial;
	
	private static AppTypefaces mInstance = null;
	
	public static AppTypefaces instance() {

		if (mInstance == null) {
			mInstance = new AppTypefaces();
		}

		return mInstance;

	}

	public AppTypefaces() {

		loadTypefaces();

	}
	
	
	private void loadTypefaces(){
		
		Context context = ApplicationContextProvider.getContext();
		
		//for English version, use the following typefaces
		fontMainRegular = Typeface.createFromAsset(context.getAssets(), TYPEFACE_MAIN_REGULAR);
		fontMainBold = Typeface.createFromAsset(context.getAssets(), TYPEFACE_MAIN_BOLD);
		fontMainRegularItalic = Typeface.createFromAsset(context.getAssets(), TYPEFACE_MAIN_REGULAR_ITALIC);		
		fontGoogleMaterial = Typeface.createFromAsset(context.getAssets(), GOOGLE_MATERIAL_ICONS_FONT);

	}	
	

}
