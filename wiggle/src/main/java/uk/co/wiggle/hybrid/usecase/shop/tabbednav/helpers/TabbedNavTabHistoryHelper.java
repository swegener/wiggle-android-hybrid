package uk.co.wiggle.hybrid.usecase.shop.tabbednav.helpers;

import java.util.ArrayList;

import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;

/**
 *
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 *
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 *
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/
public class TabbedNavTabHistoryHelper {

    private static TabbedNavTabHistoryHelper mInstance = null;
    private ArrayList<HistoryEntry> mHistory = new ArrayList<HistoryEntry>();



    public static TabbedNavTabHistoryHelper instance() {

        if (mInstance == null) {
            mInstance = new TabbedNavTabHistoryHelper();
        }

        return mInstance;

    }

    //Data object holding tab history (tab Id and a URL)
    public class HistoryEntry {

        int mTabId;
        String mTabName;
        String mLastUrl;

        public int getTabId(){
            return mTabId;
        }

        public String getTabName(){
            return mTabName;
        }

        public String getLastUrl(){
            return mLastUrl;
        }

        HistoryEntry(int intTabId, String strTabName, String strLastUrl){
            mTabId = intTabId;
            mTabName = strTabName;
            mLastUrl = strLastUrl;
        }

    }

    public void clearHistory(){
        mHistory.clear();
    }

    private int findTabInHistory(int intTabId){

        for(int i=0;i<mHistory.size();i++){
            HistoryEntry existingEntry = mHistory.get(i);
            if(existingEntry.getTabId()==intTabId){
                return i;
            }
        }
        return -1;
    }

    public void addHistoryEntry(int intTabId, String strTabName, String strUrl){

        HistoryEntry entry = new HistoryEntry(intTabId, strTabName, strUrl);

        //tab already in history? remove it to avoid double navigation to the same tab
        int intExistingIndex = findTabInHistory(entry.getTabId());
        if(intExistingIndex>-1){
            mHistory.remove(intExistingIndex);
        }

        //always make most recent tab top of the list
        mHistory.add(0, entry);
    }

    public HistoryEntry getBackNavigationEntry(){

        if(mHistory.size()>0) {
            HistoryEntry lastEntry = mHistory.get(0);
            //return separate object to caller
            HistoryEntry returnEntry = new HistoryEntry(lastEntry.mTabId, lastEntry.mTabName, lastEntry.mLastUrl);
            //delete last history entry
            mHistory.remove(0);
            return returnEntry;
        }else{
            return null;
        }

    }

    public void removeTabFromHistory(int intTabId){
        int intExistingIndex = findTabInHistory(intTabId);
        if(intExistingIndex>-1){
            mHistory.remove(intExistingIndex);
        }
    }

}
