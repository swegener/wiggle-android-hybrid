package uk.co.wiggle.hybrid.usecase.shop.tabbednav.fragments;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjSalesOrderManager;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.helpers.AppGlobalConstants;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentSelfCloseListener;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentUI;
import uk.co.wiggle.hybrid.tagging.AppTagManager;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.activities.TabbedHybridActivity;

/**
 * @author Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 *
 * @license The code of this Application is licensed to Wiggle UK Ltd.
 * The license and its ownership rights can not be transferred to a Third Party.
 *
 * @android If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 **/


public class FragmentMyAccount extends Fragment implements IAppFragmentUI {

    private static final String logTag = "FragmentMyAccount";

    public static String getLogTag(){
        return logTag;
    }

    //Account Home
    ScrollView lytMyAccount;
    LinearLayout myAccountHome;
    TextView myAccountHome_tvHeading;
    TextView myAccountHome_tvUsername;
    LinearLayout myAccount_loggedOutActions;
    ProgressBar myAccount_pbLoginInProgress;
    TextView myAccount_tvLogin;
    TextView myAccount_tvRegister;
    LinearLayout myAccount_loggedInOptions;
    TextView myAccount_tvTrackMyOrder;
    TextView myAccount_tvOrderCounter;
    TextView myAccount_tvDetails;
    TextView myAccount_tvCurrency;
    TextView myAccount_tvDeliveryDestination;
    TextView myAccount_tvHelp;
    TextView myAccount_tvContactUs;
    TextView myAccount_tvAppFeedback;
    RelativeLayout myAccount_btnLogout;
    TextView myAccount_tvLogout;
    TextView myAccount_tvChangeCountry;
    TextView myAccountHome_tvAppVersion;
    LinearLayout myAccount_lytAppFeedback;

    IAppFragmentSelfCloseListener selfCloseListener;


    String strCaller = "";
    private static FragmentMyAccount myInstance;

    public static FragmentMyAccount instance(){
        return myInstance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myInstance = this;


        /** Getting the arguments to the Bundle object */
        Bundle data = getArguments();

        // Get the extras
        if (!data.containsKey(AppGlobalConstants.CALLING_ACTIVITY)) {
            throw new IllegalArgumentException("The " + AppGlobalConstants.CALLING_ACTIVITY + " must be passed as extra for the intent. "
                    + "The content of the extras is " + data);
        }

        strCaller = data.getString(AppGlobalConstants.CALLING_ACTIVITY);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.my_account, container, false);


        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

       updateFragmentContent(view);

    }


    @Override
    public void updateFragmentContent(View view) {


        findViews(view);

        setCustomFontTypes();


    }



    public void findViews(View view) {

        lytMyAccount = (ScrollView) view.findViewById(R.id.lytMyAccount);
        myAccountHome = (LinearLayout) view.findViewById(R.id.myAccountHome);
        myAccountHome_tvHeading = (TextView) view.findViewById(R.id.myAccountHome_tvHeading);
        myAccountHome_tvUsername = (TextView) view.findViewById(R.id.myAccountHome_tvUsername);
        myAccount_loggedOutActions = (LinearLayout) view.findViewById(R.id.myAccount_loggedOutActions);
        myAccount_pbLoginInProgress = (ProgressBar) view.findViewById(R.id.myAccount_pbLoginInProgress);
        myAccount_tvLogin = (TextView) view.findViewById(R.id.myAccount_tvLogin);
        myAccount_tvRegister = (TextView) view.findViewById(R.id.myAccount_tvRegister);
        myAccount_loggedInOptions = (LinearLayout) view.findViewById(R.id.myAccount_loggedInOptions);
        myAccount_tvTrackMyOrder = (TextView) view.findViewById(R.id.myAccount_tvTrackMyOrder);
        myAccount_tvOrderCounter = (TextView) view.findViewById(R.id.myAccount_tvOrderCounter);
        myAccount_tvDetails = (TextView) view.findViewById(R.id.myAccount_tvDetails);
        myAccount_tvCurrency = (TextView) view.findViewById(R.id.myAccount_tvCurrency);
        myAccount_tvDeliveryDestination = (TextView) view.findViewById(R.id.myAccount_tvDeliveryDestination);
        myAccount_tvHelp = (TextView) view.findViewById(R.id.myAccount_tvHelp);
        myAccount_tvContactUs = (TextView) view.findViewById(R.id.myAccount_tvContactUs);
        myAccount_tvAppFeedback = (TextView) view.findViewById(R.id.myAccount_tvAppFeedback);
        myAccount_lytAppFeedback = (LinearLayout) view.findViewById(R.id.myAccount_lytAppFeedback);
        myAccount_btnLogout = (RelativeLayout) view.findViewById(R.id.myAccount_btnLogout);
        myAccount_tvLogout = (TextView) view.findViewById(R.id.myAccount_tvLogout);
        myAccount_tvChangeCountry = (TextView) view.findViewById(R.id.myAccount_tvChangeCountry);
        myAccountHome_tvAppVersion = (TextView) view.findViewById(R.id.myAccountHome_tvAppVersion);
        String strAppVersion = String.format(getString(R.string.myAccountHome_tvAppVersion)
                                                ,AppUtils.getAppVersionNumber()
                                            );
        myAccountHome_tvAppVersion.setText(strAppVersion);

        updateWidgets();

    }

    public void updateWidgets(){

        displayLayout(myAccountHome);

        showUserAsLoggedInOrOut();

        //Show app feedback option for UK only
        /*
        if(getString(R.string.str_locale_country_iso_code_uk)
                .equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY))){
            myAccount_lytAppFeedback.setVisibility(View.VISIBLE);
        }else{
            myAccount_lytAppFeedback.setVisibility(View.GONE);
        }
        */
        //APP-220: do not show feedback option on Android (missing survey monkey links for android)
        myAccount_lytAppFeedback.setVisibility(View.GONE);

    }


    private void setCustomFontTypes(){

        myAccountHome_tvHeading.setTypeface(AppTypefaces.instance().fontMainRegular);
        myAccountHome_tvUsername.setTypeface(AppTypefaces.instance().fontMainBold);
        myAccount_tvLogin.setTypeface(AppTypefaces.instance().fontMainRegular);
        myAccount_tvRegister.setTypeface(AppTypefaces.instance().fontMainRegular);
        myAccount_tvTrackMyOrder.setTypeface(AppTypefaces.instance().fontMainRegular);
        myAccount_tvOrderCounter.setTypeface(AppTypefaces.instance().fontMainRegular);
        myAccount_tvDetails.setTypeface(AppTypefaces.instance().fontMainRegular);
        myAccount_tvCurrency.setTypeface(AppTypefaces.instance().fontMainRegular);
        myAccount_tvDeliveryDestination.setTypeface(AppTypefaces.instance().fontMainRegular);
        myAccount_tvHelp.setTypeface(AppTypefaces.instance().fontMainRegular);
        myAccount_tvContactUs.setTypeface(AppTypefaces.instance().fontMainRegular);
        myAccount_tvAppFeedback.setTypeface(AppTypefaces.instance().fontMainRegular);
        myAccount_tvLogout.setTypeface(AppTypefaces.instance().fontMainRegular);
        myAccount_tvChangeCountry.setTypeface(AppTypefaces.instance().fontMainRegular);
        myAccountHome_tvAppVersion.setTypeface(AppTypefaces.instance().fontMainRegular);

    }



    @Override
    public void onStart() {
        super.onStart();

        setTitleBarText();


    }

    @Override
    public void onResume() {

        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);


    }



    //Check if this is working - this is the new version of deprecated
    //public void onAttach(Activity activity)
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            selfCloseListener = (IAppFragmentSelfCloseListener) context;
        } catch (ClassCastException e) {
            throw new RuntimeException(getActivity().getClass().getName() + " must implement IAppFragmentSelfCloseListener to use this fragment", e);
        }

    }

    public void onClickEvent(int intClickedViewId){

        switch (intClickedViewId) {

            case R.id.myAccount_btnLogin:
                AppTagManager.instance().sendV2EventTags("linkclick","App - My Account","Main","Sign In");
                //displayLayout(lytLoginScreen);
                TabbedHybridActivity.instance().addUserLoginFragment(false);
                break;

            case R.id.myAccount_btnLogout:
                AppTagManager.instance().sendV2EventTags("linkclick","App - My Account","Main","Log Out");
                doLogOut();
                break;

            case R.id.myAccount_lytTrackMyOrder:
                AppTagManager.instance().sendV2EventTags("linkclick","App - My Account","Main","Track my order");
                TabbedHybridActivity.instance().addOrderTrackingFragment(false);
                break;

            case R.id.myAccount_tvChangeCountry:
                AppTagManager.instance().sendV2EventTags("linkclick","App - My Account","Main","Change Country");
                ObjAppUser.instance().clearCountrySpecificFields();
                /*No need to remove user specific data when only the country is changing - we keep him logged in and everything
                if(ObjAppUser.instance().isUserLoggedIn()){
                    AppSession.instance().clearUserSpecificData();
                }
                */
                TabbedHybridActivity.instance().restartApp();
                break;

            //go to target web forms for non-native use cases
            case R.id.myAccount_btnRegister:
                AppTagManager.instance().sendV2EventTags("linkclick","App - My Account","Main","Register");
                goToWebForm(intClickedViewId);
                break;
            case R.id.myAccount_tvDetails:
                AppTagManager.instance().sendV2EventTags("linkclick","App - My Account","Main","My account details");
                goToWebForm(intClickedViewId);
                break;
            case R.id.myAccount_tvCurrency:
                AppTagManager.instance().sendV2EventTags("linkclick","App - My Account","Locale","Currency");
                goToWebForm(intClickedViewId);
                break;
            case R.id.myAccount_tvDeliveryDestination:
                AppTagManager.instance().sendV2EventTags("linkclick","App - My Account","Locale","Delivery Destination");
                goToWebForm(intClickedViewId);
                break;
            case R.id.myAccount_tvHelp:
                AppTagManager.instance().sendV2EventTags("linkclick","App - My Account","Customer Services","Help");
                goToWebForm(intClickedViewId);
                break;
            case R.id.myAccount_tvContactUs:
                AppTagManager.instance().sendV2EventTags("linkclick","App - My Account","Customer Services","Contact Us");
                goToWebForm(intClickedViewId);
                break;

            case R.id.myAccount_tvAppFeedback:
                AppTagManager.instance().sendV2EventTags("linkclick","App - My Account","Customer Services","Give feedback about the app");
                openSurveyUrl();
                break;

            default:
                break;

        }

    }

    private void displayLayout(View viewToShow){

        myAccountHome.setVisibility(View.GONE);

        viewToShow.setVisibility(View.VISIBLE);

    }

    public void showUserAsLoggedInOrOut(){

        if(ObjAppUser.instance().isUserLoggedIn()){
            myAccount_btnLogout.setVisibility(View.VISIBLE);
            myAccount_loggedInOptions.setVisibility(View.VISIBLE);
            myAccount_loggedOutActions.setVisibility(View.GONE);
            myAccountHome_tvHeading.setVisibility(View.GONE);
            showUserName();
            updateOrderTrackingBadge();
        }else{
            myAccount_btnLogout.setVisibility(View.GONE);
            myAccount_loggedInOptions.setVisibility(View.GONE);
            myAccount_loggedOutActions.setVisibility(View.VISIBLE);
            myAccountHome_tvHeading.setVisibility(View.VISIBLE);
            myAccountHome_tvUsername.setVisibility(View.GONE);
            //hide order tracking badge
            myAccount_tvOrderCounter.setVisibility(View.GONE);
        }


        scrollToTop();



    }

    private void doLogOut(){

        //check if we have to restart the app after a reserved login was used
        boolean blnReservedLogin = isReservedLogin(); //check before ObjAppUser is cleared

        TabbedHybridActivity.instance().performLogout();
        showUserAsLoggedInOrOut();

        //restart app if required
        if(blnReservedLogin){
            TabbedHybridActivity.instance().restartApp();
        }

    }


    private boolean isReservedLogin(){

        String strEmail = ObjAppUser.instance().getString(ObjAppUser.FIELD_EMAIL);

        if("contentfulpreview".equals(strEmail)){
            ObjAppUser.instance().setField(ObjAppUser.FIELD_EMAIL, ""); //clear email field
            ObjAppUser.instance().saveToSharedPreferences();
            return true;
        }

        return false;
    }

    private void showUserName(){

        myAccountHome_tvUsername.setText(String.format(getString(R.string.myAccountHome_tvUsername), ObjAppUser.instance().getString(ObjAppUser.FIELD_FORENAME)));
        myAccountHome_tvUsername.setVisibility(View.VISIBLE);
    }

    public void updateOrderTrackingBadge(){

        int intUpdatedOrders = ObjSalesOrderManager.getInstance().getOrdersWithNewInformation().size();

        if(intUpdatedOrders>0){
            myAccount_tvOrderCounter.setText(String.valueOf(intUpdatedOrders));
            myAccount_tvOrderCounter.setVisibility(View.VISIBLE);
        }else{
            myAccount_tvOrderCounter.setVisibility(View.GONE);
        }

        //also update counter on navigation tabs if required
        TabbedHybridActivity.instance().updateOrderTrackingBadge();

    }

    private void goToWebForm(int intPressedViewId){

        TabbedHybridActivity.instance().loadUrlForViewInWebViewFragment(intPressedViewId);

    }

    private void openSurveyUrl(){

        TabbedHybridActivity.instance().loadUrlInExternalBrowser(getString(R.string.str_url_app_feedback));

    }

    @Override
    public void onHiddenChanged(boolean hidden){

        if(!hidden){
            setTitleBarText();
        }
    }

    private void setTitleBarText(){

        if(TabbedHybridActivity.instance()!=null){
            Bundle data = getArguments();

            // Get the extras
            if (data.containsKey(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE)) {

                String strTitle = data.getString(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE);
                TabbedHybridActivity.instance().setBreadcrumbText(strTitle);

            }

        }


    }

    private void scrollToTop(){

        //scroll to top whenever layouts are changed from logged in to logged out
        lytMyAccount.post(new Runnable()
        {
            public void run()
            {
                lytMyAccount.fullScroll(ScrollView.FOCUS_UP);
            }
        });
    }

}


