package uk.co.wiggle.hybrid.api.networking;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import uk.co.wiggle.hybrid.application.logging.Logger;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class CustomJSONPostRequest extends JsonObjectRequest {

	private String logTag = "CustomJSONPostRequest";

	private Map<String, String> headerParams;
	private Listener<JSONObject> listener;
	private Map<String, String> params;
	private Priority mPriority = Priority.IMMEDIATE; // all volley requests need
														// to be executed
														// immediately - if you
														// want to change this
														// for a given request
														// simply call
														// DubizzleJsonObjectRequest.setPriority(Priority.xxx);

	public CustomJSONPostRequest(int method, String url,
			JSONObject jsonRequest, Map<String, String> params,
			Map<String, String> headerParams, Listener<JSONObject> listener,
			ErrorListener errorListener) {
		super(method, url, jsonRequest,listener, errorListener);
		// TODO Auto-generated constructor stub
		this.listener = listener;
		this.headerParams = headerParams;
		this.params = params;
		Logger.printMessage(logTag, "Json Body: " + jsonRequest.toString(), Logger.INFO);
	}

	@Override
	protected Map<String, String> getParams() throws AuthFailureError {
		Logger.printMessage(logTag, "Params: " + params.toString(), Logger.INFO);
		return params;
	};

	@Override
	public Map<String, String> getHeaders() throws AuthFailureError {
		Logger.printMessage(logTag, "Header: " + headerParams.toString(),
				Logger.INFO);
		;//Logger.e("headers", headerParams.toString());
		return headerParams;
	}

	@Override
	protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
		// TODO Auto-generated method stub
		try {
			String jsonString = new String(response.data,
					HttpHeaderParser.parseCharset(response.headers));
			return Response.success(new JSONObject(jsonString),
					HttpHeaderParser.parseCacheHeaders(response));
		} catch (UnsupportedEncodingException e) {
			return Response.error(new ParseError(e));
		} catch (JSONException je) {
			return Response.error(new ParseError(je));
		}
	}
	
	@Override
    public String getBodyContentType() {
        return "application/json";
    }

	@Override
	protected void deliverResponse(JSONObject response) {
		// TODO Auto-generated method stub
		// super.deliverResponse(response);
		listener.onResponse(response);
	}

	@Override
	protected VolleyError parseNetworkError(VolleyError volleyError) {
		// TODO Auto-generated method stub
		if (volleyError.networkResponse != null
				&& volleyError.networkResponse.data != null) {

			VolleyError error = new VolleyError(new String(
					volleyError.networkResponse.data));
			volleyError = error;

		}

		return volleyError;
	}

	@Override
	public Priority getPriority() {
		return mPriority;
	}

	public void setPriority(Priority priority) {
		mPriority = priority;
	}

}
