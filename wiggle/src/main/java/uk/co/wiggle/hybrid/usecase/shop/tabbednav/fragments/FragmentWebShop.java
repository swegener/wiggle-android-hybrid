package uk.co.wiggle.hybrid.usecase.shop.tabbednav.fragments;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjNavigationCategoryManager;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjApiRequest;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.helpers.AppGlobalConstants;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.application.session.AppSession;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.extensions.NestedWebView;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentSelfCloseListener;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentUI;
import uk.co.wiggle.hybrid.tagging.AppTagManager;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.activities.TabbedHybridActivity;


/**
 * @author Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 *
 * @license The code of this Application is licensed to Wiggle UK Ltd.
 * The license and its ownership rights can not be transferred to a Third Party.
 *
 * @android If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 **/

@SuppressLint("NewApi")
public class FragmentWebShop extends Fragment implements IAppFragmentUI {

    private static final String logTag = "FragmentWebShop";

    IAppFragmentSelfCloseListener selfCloseListener;


    String strCaller = "";
    int mWebViewCaller = 0;

    public int getWebViewCaller(){
        return mWebViewCaller;
    }

    String mPassedUrl = "";


    private static FragmentWebShop myInstance;

    public static FragmentWebShop instance(){
        return myInstance;
    }

    private static FragmentWebShop myWebShopInstance;
    public static FragmentWebShop getWebShopInstance(){
        return myWebShopInstance;
    }

    private static FragmentWebShop myWebViewInstance;
    public static FragmentWebShop getWebViewInstance(){
        return myWebViewInstance;
    }

    //This fragment is used for two purposes
    //a) The "Shop" fragment that is always in the background
    //b) The "WebView" fragment which is used for sporadic URL calls that need to be displayed on top of the view hierarchy
    public static final String CONST_FRAGMENT_USAGE = "CONST_FRAGMENT_USAGE"; //deliver either CONST_WEB_SHOP_FRAGMENT or CONST_WEB_VIEW_FRAGMENT here
    public static final String CONST_WEB_SHOP_FRAGMENT = "webShop";
    public static final String CONST_WEB_VIEW_FRAGMENT = "webView";

    String strFragmentUsage = "";
    public boolean isWebShopFragment(){
        return CONST_WEB_SHOP_FRAGMENT.equals(strFragmentUsage);
    }

    public boolean isWebViewFragment(){
        return CONST_WEB_VIEW_FRAGMENT.equals(strFragmentUsage);
    }

    /* Web View Elements */
    private NestedWebView mWebView;
    private TextView tvErrorMessage;
    private RelativeLayout lytPleaseWait;
    private ImageView ivPleaseWait;
    ProgressBar main_pbLoadProgress;
    TextView main_tvBreadcrumb;
    TextView hybridActivity_tvCurrentLink;

    public static String WEB_VIEW_CALLER = "WEB_VIEW_CALLER";
    public static String LOAD_TARGET_URL = "LOAD_TARGET_URL";
    public static String BREADCRUMB_TEXT = "BREADCRUMB_TEXT";

    //Last URL user tried before login was required
    String strLastUserRequestedURL;

    //URL used to navigate to unsupported stores in external browser
    String strUnsupportedStoreHost = "";

    //used to force clearing history when user returns to "Home" after deep-navigation
    boolean blnForceClearHistory = false;

    public void setForceClearHistory(){
        //set this boolean from other screens - history is only cleared successfully at the end of a page load,
        //so calling webview.clearhistory() directly does not always work. This boolean ensures the webview
        //calls clearHistory() at the right time.
        blnForceClearHistory = true;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myInstance = this;


        /** Getting the arguments to the Bundle object */
        Bundle data = getArguments();

        // Get the extras
        if (!data.containsKey(AppGlobalConstants.CALLING_ACTIVITY)) {
            throw new IllegalArgumentException("The " + AppGlobalConstants.CALLING_ACTIVITY + " must be passed as extra for the intent. "
                    + "The content of the extras is " + data);
        }

        if (!data.containsKey(CONST_FRAGMENT_USAGE)) {
            throw new IllegalArgumentException("The " + CONST_FRAGMENT_USAGE + " must be passed as extra for the intent. "
                    + "The content of the extras is " + data);
        }

        strCaller = data.getString(AppGlobalConstants.CALLING_ACTIVITY);
        strFragmentUsage = data.getString(CONST_FRAGMENT_USAGE);

        if(CONST_WEB_VIEW_FRAGMENT.equals(strFragmentUsage)){
            myWebViewInstance = this;
        }else{
            myWebShopInstance = this;
        }

        mWebViewCaller = data.getInt(WEB_VIEW_CALLER, 0);
        mPassedUrl = data.getString(LOAD_TARGET_URL, "");

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_web_shop, container, false);


        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

       updateFragmentContent(view);

    }


    @Override
    public void updateFragmentContent(View view) {


        findViews(view);

        setCustomFontTypes();


    }



    public void findViews(View view) {


        mWebView = (NestedWebView) view.findViewById(R.id.main_webView);
        main_pbLoadProgress = (ProgressBar) view.findViewById(R.id.main_pbLoadProgress);
        main_tvBreadcrumb = (TextView) view.findViewById(R.id.main_tvBreadcrumb);
        tvErrorMessage = (TextView) view.findViewById(R.id.main_tvErrorMessage);
        lytPleaseWait = (RelativeLayout) view.findViewById(R.id.main_lytPleaseWait);
        ivPleaseWait = (ImageView) view.findViewById(R.id.main_ivPleaseWait);

        hybridActivity_tvCurrentLink = (TextView) view.findViewById(R.id.hybridActivity_tvCurrentLink);
        hybridActivity_tvCurrentLink.setVisibility(View.GONE);

        if(!"".equals(AppUtils.REDIRECT_APP_TO_TEST_SERVER) || !AppUtils.isProductionVersionOfApp()){
            hybridActivity_tvCurrentLink.setVisibility(View.VISIBLE);
        }


        updateWidgets();

    }

    public void updateWidgets(){

        setupWebClients();

    }


    private void setCustomFontTypes(){

        main_tvBreadcrumb.setTypeface(AppTypefaces.instance().fontMainRegular);
        hybridActivity_tvCurrentLink.setTypeface(AppTypefaces.instance().fontMainRegular);

    }



    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {

        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);


    }



    //Check if this is working - this is the new version of deprecated
    //public void onAttach(Activity activity)
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            selfCloseListener = (IAppFragmentSelfCloseListener) context;
        } catch (ClassCastException e) {
            throw new RuntimeException(getActivity().getClass().getName() + " must implement IAppFragmentSelfCloseListener to use this fragment", e);
        }

    }



    private void setupWebClients(){

        mWebView.setWebViewClient(genericWebClient);
        //mWebView.setWebChromeClient(new CustomWebChromeClient()); //chrome client handles file choosers etc ...
        //Version 1.3 - Wiggle requires a progress bar to be shown .. so set web chrome client as well
        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {

                /*
                main_pbLoadProgress.setVisibility(View.VISIBLE);
                main_pbLoadProgress.setProgress(progress);

                if(progress == 100){
                    //Page loaded .. hide progress bar
                    main_pbLoadProgress.setVisibility(View.VISIBLE);
                }
                */

                TabbedHybridActivity.instance().setProgressBar(progress);

            }
        });


        mWebView.getSettings().setJavaScriptEnabled(true); //yes, that is a security issue ... but given that we only load known and no external URLs this is in control

        //enable local storage (used by Web chat)
        mWebView.getSettings().setDomStorageEnabled(true);

        String strUserAgentString = mWebView.getSettings().getUserAgentString();

        //enable debugging through Chrome browser
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mWebView.setWebContentsDebuggingEnabled(true);
        }

        //Back navigation on some Wiggle pages (e.g. assist insurance product quote use case)
        //result in ERR_CACHE_MISS, so add this workaround.
        //Android Lollipop Chrome WebView is the likely cause for this behaviour
        //See https://stackoverflow.com/questions/25664146/android-4-4-giving-err-cache-miss-error-in-onreceivederror-for-webview-back
        if (Build.VERSION.SDK_INT == 19) {

            //CAUTION: this can lead to outdated cache being used, so watch this one for
            //any odd user experience reports.
            //mWebView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK); // uses cached resources even if they're expired. This is no good!

            //Important!! Disabled the above LOAD_CACHE_ELSE_NETWORK as this lead to the following bug:
            //A user (not logged in) adds an item to the basket and presses "Checkout".
            //A request is fired to the /checkout endpoint. Wiggle recognise the user as logged out
            //and suggest he visits the "Login" URL which presents the native login screen.
            //The native login screen is shown and the user submits his login.
            //The secure/checkout request is fired, BUT THE RESPONSE is taken from the cache.
            //This of course shows the user as still "logged out" and results in a request to login again.
            //This then happens indefinitely.
            //So instead, we are working around ERR_CACHE_MISS by not using our cache at all.
            //This is far from ideal, so check for any user reports reporting odd behaviour.
            mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE); // Dont use cache at all

        }

        String strCustomUserAgent = strUserAgentString + String.format(getString(R.string.str_browser_header_user_agent_value), AppUtils.getAppVersionNumber());
        //AppUtils.showCustomToast(strCustomUserAgent, true); //test only
        mWebView.getSettings().setUserAgentString(strCustomUserAgent);

        //speed up the webview
        tweakWebviewPerformance();


        //Javascript interface - TODO: enable and adjust implementation if required
        //mWebView.addJavascriptInterface(new CustomJavaScriptInterface(), "HTMLOUT");
        //mWebView.addJavascriptInterface(new IJavascriptHandler(), "cpjs");

        //clear cache to clear any old access tokens that might float around
        clearCache(getActivity(), 0);

        loadTargetURL(mWebViewCaller);

    }

    public void loadTargetURL(int intWebViewCaller){

        //no caller Id passed - get it from this class
        if(intWebViewCaller==0){
            intWebViewCaller = mWebViewCaller;
        }

        //Check if URL was passed ..
        String strTargetUrl = mPassedUrl;
        if(strTargetUrl==null || "".equals(strTargetUrl)) {
            strTargetUrl = AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_wiggle_home), "homeShop");
        }


        switch (intWebViewCaller) {

            case R.id.navigation_Home:
                TabbedHybridActivity.instance().setBreadcrumbText(getString(R.string.shop_Home_breadcrumb_text));
                break;

            case R.id.startup_btnShop:
                AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_home), "homeShop");
                strTargetUrl = AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_wiggle_home), "homeShop");
                break;

            case R.id.login_tvForgotPassword:
            case R.id.userLogin_tvForgotPassword:
                AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_signin), "signinFgtPwd");
                strTargetUrl = AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_forgot_password), "signinFgtPwd");
                break;

            case R.id.myAccount_btnRegister:
            case R.id.userLogin_btnRegister:
            case R.id.login_btnRegister:
                AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_signin), "signinRegister");
                strTargetUrl = AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_registration), "signinRegister");
                TabbedHybridActivity.instance().setBreadcrumbText(getString(R.string.login_tvRegister));
                break;

            case R.id.myAccount_tvDetails:
                AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_home), "homeMyAccount");
                strTargetUrl = AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_my_account), "homeMyAccount");
                TabbedHybridActivity.instance().setBreadcrumbText(getString(R.string.myAccount_tvDetails));
                break;

            case R.id.startup_lytTrackMyOrder:
                AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_home), "homeTrackOrder");
                //Replaced by a native screen in version 1.06
                //strTargetUrl = AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_my_orders), "homeTrackOrder");
                TabbedHybridActivity.instance().addOrderTrackingFragment(false);
                break;

            case R.id.navigation_Wishlist:
                AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_home), "homeWishlist");
                strTargetUrl = AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_my_wishlist), "homeWishlist");
                break;


            case R.id.myAccount_tvCurrency:
                AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_home), "myAccountCurrency");
                strTargetUrl = AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_currency), "myAccountCurrency");
                TabbedHybridActivity.instance().setBreadcrumbText(getString(R.string.myAccount_tvCurrency));
                break;

            case R.id.myAccount_tvDeliveryDestination:
                AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_home), "myAccountDeliveryDestination");
                strTargetUrl = AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_currency), "myAccountDeliveryDestination");
                TabbedHybridActivity.instance().setBreadcrumbText(getString(R.string.myAccount_tvDeliveryDestination));
                break;

            case R.id.myAccount_tvHelp:
                AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_home), "myAccountHelp");
                strTargetUrl = AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_help), "myAccountHelp");
                TabbedHybridActivity.instance().setBreadcrumbText(getString(R.string.myAccount_tvHelp));
                break;

            case R.id.myAccount_tvContactUs:
                AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_home), "myAccountContactUs");
                strTargetUrl = AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_contact_us), "myAccountContactUs");
                TabbedHybridActivity.instance().setBreadcrumbText(getString(R.string.myAccount_tvContactUs));
                break;

            default:
                break;

        }


        //no website? do nothing ..
        if(strTargetUrl==null || "".equals(strTargetUrl)){
            TabbedHybridActivity.instance().hideSingleFragment(this);
            return;
        }else{
            if(!strTargetUrl.startsWith("https://") && !strTargetUrl.startsWith("http://")){
                strTargetUrl = "http://" + strTargetUrl;
            }
        }


        //TODO: this may not be needed for Wiggle as there is no interception of html buttons ... only enable first leg when problems become apparent
        if(1==2 && Build.VERSION.SDK_INT == 19) {

            TabbedHybridActivity.instance().loadUrlInExternalBrowser(strTargetUrl);


        }else{

            //all other Android versions - load URL in webview
            loadUrlInWebView(strTargetUrl);

        }


    }


    //user defined WebViewClient class in order to let Android OS handle clicks on certain links
    WebViewClient genericWebClient = new WebViewClient(){

        String pendingUrl = null;



        /* Please note - shouldOverrideUrlLoading is not reliable called on http redirects ... for this purpose onPageStarted was added below */
        @Override
        public boolean shouldOverrideUrlLoading(WebView  view, String  url){

            boolean blnValue = isOverrideUrlLoading(view, url);
            return blnValue;
            //view.loadUrl(url, addAdditionalHttpHeaders());


        }

        //in case we want an external app to be opened ...
        private void openURLInExternalApp(String url){
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
        }

        private boolean isOverrideUrlLoading(WebView  view, String  url){

            //check if we are connected to the internet - if not, show error and abort page load
            if(!AppUtils.isNetworkConnected()){
                TabbedHybridActivity.instance().showHttpErrorDialog(url);
                view.stopLoading();
                return true;
            }

            //can't make webview visible when Javascript is called, so using a trick with a view that hides the webview from the user
            showPleaseWait(false);

            //URLs that are allowed to open in external apps
            if(url.contains("callto:")
                    || url.contains("tel:")
                    || url.contains("geo:")
                    || url.contains("mailto:")){

                openURLInExternalApp(url);
                return true;

            }

            // We want to intercept any attempt to show the web login screen with our own
            //Lower casing this as Wiggle introduced different cased URL patterns in the past ...
            if(url.toLowerCase().contains("myaccount/logon")
                    || url.toLowerCase().contains("account/logon")
                    || url.toLowerCase().contains("checkout/logon")
                    ){


                //the URL contains a parameter "returnUrl"
                /** we don't need this as we store the URL originally requested in strLastUserRequestedURL
                 Uri uri = Uri.parse(url);
                 String strReturnURLFragment = uri.getQueryParameter("returnUrl");
                 String strReturnURL = uri.getScheme() + "://" + uri.getHost() + "/" +  strReturnURLFragment;
                 strLastUserRequestedURL = strReturnURL;
                 **/

                handleLoginRequired(url);
                return true; //don't open URL
            }


            //Legal & Insurance - check if we need to display pdf documents (such as Terms & Conditions)
            if(AppUtils.isUrlPdfDocument(url)){

                //open PDF externally
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse(url), "application/pdf");
                try{
                    view.getContext().startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    //user does not have a pdf viewer installed - open pdf with google docs
                    mWebView.loadUrl(AppGlobalConstants.GOOGLE_DOCS_VIEWER + url);
                }

                return true; //dont open URL in webview
            }

            //in all other cases, open URL in this webview
            return false;

        }


        //here some defined URLs can be intercepted
        @Override
        public void onLoadResource(WebView  view, String  url){


            //in all standard cases, simply load URL
            super.onLoadResource(view, url);
        }


        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {

            //For debugging or on test server
            hybridActivity_tvCurrentLink.setText(url);

            //Set breadcrumb text (currently only for Insurance quote Velosure pages)
            setBreadcrumbText(url);

            if (pendingUrl == null) {
                pendingUrl = url;
            }

            //** on redirects and when not clicking on hyperlinks, shouldOverrideUrlLoading would have been missed - so handling it here.
            isOverrideUrlLoading(view, url);

        }

        //HTTP 500 server problem - needed to read html content of web-view before it is displayed to the user
        @Override
        public void onPageFinished(WebView webView, String url) {

            //prevent NPE - page may end loading when this fragment is no longer attached - do nothing in this case
            if(!isAdded()){
                return;
            }

            //Special handling:
            //If we are on the "Home" tab, but the user browses deeper, replace the nav bar title with an empty string
            if(R.id.navigation_Home==TabbedHybridActivity.instance().getActiveTab()){
                if(mWebView.canGoBack()){
                    TabbedHybridActivity.instance().setBreadcrumbText("    ");
                }else{
                    TabbedHybridActivity.instance().setBreadcrumbText(getString(R.string.shop_Home_breadcrumb_text));
                }
            }

            if (!url.equals(pendingUrl)) {

                Logger.printMessage(logTag, "Detected HTTP redirect " + pendingUrl + "->" + url, Logger.INFO);
                //Add code reacting to page redirects here if needed ...
                pendingUrl = null;

            }

            /** .. example use of javascript manipulation .. only enable if required
             *
             * If the following is enabled, then you also need to enable mWebView.addJavascriptInterface(new CustomJavaScriptInterface(), "HTMLOUT"); above
             *

             String strJavascript = ""
             + "javascript:(function onSelectFieldChange(sel) {"
             + "var value = sel.value;"
             + "javascript:window.cpjs.sendToAndroid(value);"
             + "}"
             + "javascript:document.getElementById('langId').onchange = 'onSelectFieldChange(this)';"
             + "javascript:window.HTMLOUT.showHTML" +
             "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');"
             ;



             mWebView.loadUrl(strJavascript, addAdditionalHttpHeaders());

             **/


            //set Activity title to title of current web-page (useful for the user to know where he is when browsing deeper into Wiggle)
            extractPageTitle(webView.getTitle(), url); //de-activated on request in version 1.01

            updateShoppingCartCounter();

            //Version 1.02
            //check if the WiggleCustomer2 cookie indicates that a user has logged in and the delivery destination / currency / language of his account
            //does not match the information we have locally
            //TODO: should be merged with checkForUserInternationalSettingChange as both methods do the same thing
            listenForCustomerCookie(AppUtils.getWiggleURL(getString(R.string.str_http_header_domain)), url);

            //Wiggle users can change their location within the web-browser (Currency & Delivery options), so we need to listen for that.
            //Note: as this method contains mWebView.clearHistory() it needs to remain in onPageFinished - as otherwise not all of the preceding history is removed (Gotta love Android :-) )
            checkForUserInternationalSettingChange(url);

            if(blnForceClearHistory){
                mWebView.clearHistory();
                blnForceClearHistory = false;
            }

            hidePleaseWait();

            //If this is an order confirmation page - ask user to rate the app!
            if(url.toLowerCase().contains("orderthankyou")){
                TabbedHybridActivity.instance().showAppReviewPromptActivity(false);
            }

        }


        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url)
        {
            //use this method to intercept AJAX calls
            return null;
        }


        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);

            view.loadUrl("about:blank");
        }


        @TargetApi(Build.VERSION_CODES.M)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
            //Removed due to Google policy: Redirect to deprecated method, so you can use it in all SDK versions
            //onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
            if(req!=null && req.getUrl()!=null) {
                //showHttpErrorDialog(req.getUrl().toString());
            }
        }


        //With Android M we can finally react to Http errors and display a nice user message
        @Override
        public void onReceivedHttpError (WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
            //Toast.makeText(view.getContext(), "HTTP error "+errorResponse.getStatusCode(), Toast.LENGTH_LONG).show();

				/* From google docs:

					Notify the host application that an HTTP error has been received from the server while loading
					a resource. HTTP errors have status codes >= 400. This callback will be called for any resource
					 (iframe, image, etc), not just for the main page. Thus, it is recommended to perform minimum
					 required work in this callback. Note that the content of the server response may not be provided
					 within the errorResponse parameter.

					 So without further logic, even a website that loads, but individual requests have errors would
					 display a multitude of error messages... TODO: filter out relevant errors and showHttpErrorDialog() accordingly

				 */

            if(request!=null && request.getUrl()!=null) {
                //showHttpErrorDialog(request.getUrl().toString());
            }
        }


    };

    private void showPleaseWait(boolean blnConfirmShowPleaseWait){

        if(!blnConfirmShowPleaseWait){
            return;
        }

        if(lytPleaseWait!=null) {
            lytPleaseWait.setVisibility(View.VISIBLE);
        }
        if(ivPleaseWait!=null) {
            AppUtils.startFloatingLogoAnimation(getActivity(), ivPleaseWait);
        }

    }



    public void hidePleaseWait(){

        if(ivPleaseWait!=null) {
            AppUtils.stopFloatingLogoAnimation(getActivity(), ivPleaseWait);
        }
        if(lytPleaseWait!=null) {
            lytPleaseWait.setVisibility(View.GONE);
        }

    }

    //HTTP 500 server problem - needed to read html content of web-view before it is displayed to the user
    class CustomJavaScriptInterface {

        //Annotation @JavascriptInterface necessary for any method you want to make available to web page code
        //Without this annotation, the method will not be executed. NB: the method MUST also be public
        @JavascriptInterface
        public void showHTML(String html) {
            String htmlString = html;

            if(htmlString==null){
                //return and wait for valid html download to complete
                return;
            }else{
                AppUtils.showCustomToast(htmlString, false);
            }

            if(htmlString.contains("500 - Internal Server Error")
                    || htmlString.contains("Internal Server Error")
                    ){
                //reload URL
                showPleaseWait(false);

                loadTargetURL(0);
                Logger.printMessage(logTag, "500 error - reloading resource", Logger.ERROR);
            }else{
                //can't make webview visible at this state, so using a trick with a view that hides the webview from the user
                Logger.printMessage(logTag, "Webpage ok - display to user", Logger.INFO);
                hidePleaseWait();
            }
        }

    }

    //methods allowing to clear cache of paa webview
    static int clearCacheFolder(final File dir, final int numDays) {

        int deletedFiles = 0;
        if (dir!= null && dir.isDirectory()) {
            try {
                for (File child:dir.listFiles()) {

                    //first delete subdirectories recursively
                    if (child.isDirectory()) {
                        deletedFiles += clearCacheFolder(child, numDays);
                    }

                    //then delete the files and subdirectories in this dir
                    //only empty directories can be deleted, so subdirs have been done first

                    if (child.lastModified() < new Date().getTime() - numDays * android.text.format.DateUtils.DAY_IN_MILLIS) {
                        if (child.delete()) {
                            deletedFiles++;
                        }
                    }
                }
            }
            catch(Exception e) {
                Logger.printMessage(logTag, "Failed to clean the cache " + e.getMessage(), Logger.ERROR);

            }
        }
        return deletedFiles;
    }

    /*
     * Delete the files older than numDays days from the application cache
     * 0 means all files.
     */
    public static void clearCache(final Context context, final int numDays) {
        int numDeletedFiles = clearCacheFolder(context.getCacheDir(), numDays);
        Logger.printMessage(logTag, "Deleted " + numDeletedFiles + " cache files", Logger.INFO);
    }

    public boolean webViewCanGoBack(){

        //in case please wait screen is shown - always hide it
        hidePleaseWait();

        if(mWebView==null){
            return false;
        }

        if(mWebView.canGoBack()) {
            webViewGoBack();
            return true; //can go back = true
        }else{
            return false; //cant go back = false -> continue with onBackPressed on Activity level
        }

    }

    public void webViewGoBack(){

        mWebView.goBack();

    }


    final class IJavascriptHandler {

        IJavascriptHandler() {
        }

        // This annotation is required in Jelly Bean and later:
        @JavascriptInterface
        public void sendToAndroid(String text) {
            // this is called from JS with passed value
            AppUtils.showCustomToast(text, true);
        }
    }

    private void extractPageTitle(String strPageTitle, String strUrl){



        //don't do it for URLs that we navigated to from category browsing etc. We only want page titles for deep-browsing via website links.
        if(strUrl.equals(strLastUserRequestedURL)){
            return;
        }

        //don't do it for the top level domain URL
        if(strUrl.equals(AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home)) + "/")){
            setBreadcrumbText(getString(R.string.shop_Home_breadcrumb_text));
            return;
        }

        //no page title - return
        if(strPageTitle==null || "".equals(strPageTitle)){
            return;
        }




        if(strPageTitle.contains("|")){

            //Page title is something like Wiggle | Shoes | Womens shoes ..  so get last token
            strPageTitle = strPageTitle.substring(strPageTitle.lastIndexOf("|"));
            if(strPageTitle!=null){
                strPageTitle = strPageTitle.replace("|", "");
                strPageTitle = strPageTitle.trim();
            }

        }

        setBreadcrumbText(strPageTitle);

    }

    private void checkForUserInternationalSettingChange(String strURL){

        //Skip this method if we are looking at a dedicated test server
        if(!"".equals(AppUtils.REDIRECT_APP_TO_TEST_SERVER)){
            return;
        }

        Uri uri = Uri.parse(strURL);
        String strNewHost = uri.getHost();

        String strCurrentStoreHost = ObjAppUser.instance().getString(ObjAppUser.FIELD_CURRENT_WIGGLE_STORE);

        //remove any subdomains from new host and current host e.g. "guides.wiggle.co.uk" are still
        //on the same domain as "www.wiggle.co.uk"  - so we just compare "wiggle.co.uk"
        strNewHost = strNewHost.substring(strNewHost.indexOf(".")+1);
        strCurrentStoreHost = strCurrentStoreHost.substring(strCurrentStoreHost.indexOf(".")+1);

        //check if country domain changed
        if(!strNewHost.equals(strCurrentStoreHost)){

            //no need to show this dialog if an image was tapped to show a higher resolution image
            //TODO: remove the hard-coding and replace with generic mechanism ..
            //Note: www.wigglestatic.com is the image host - tapping on a product image to enlarge it would be classed as country domain change
            //      There also are embedded youtube videos - so make that domains not containing "wiggle" are not classed as country domain change
            if(!strNewHost.contains("wiggle") || strNewHost.startsWith("www.wigglestatic.")){
                return;
            }

            if(!AppUtils.isAppSupportingWiggleStore(strNewHost)){
                TabbedHybridActivity.instance().showUnsupportedStoreDialog(strNewHost);
                //loadUrlInWebView("http://" + strCurrentStoreHost); //revert to current store
                loadTargetURL(mWebViewCaller); //simply reload same page (should be Delivery Country or Currency page only)
                return;
            }

            adjustAppUserToNewDomain(uri);


            //Remove all fragments that are displayed - the app is reset to "virgin" state after country change
            TabbedHybridActivity.instance().setAddHomeFragmentOnBackStackClearedListener();
            TabbedHybridActivity.instance().removeAllFragmentsToIndex(0);


            ObjNavigationCategoryManager.getInstance().setLocalNavigationTree(null);


            //get new country domain
            String strNewUserCountry = AppUtils.getCountryByTopLevelDomain(strNewHost);
            ObjAppUser.instance().setField(ObjAppUser.FIELD_COUNTRY, strNewUserCountry); //update current object
            ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_COUNTRY, strNewUserCountry); //persist change
            mWebView.clearHistory(); //do not allow user to navigate back after country change as this would reset country when user revisits preceding pages from previous country

            //reload local navigation tree
            TabbedHybridActivity.instance().loadLocalNavigationTreeIfMissing();

            //Go to "Home" tab so that everything is re-initiated
            TabbedHybridActivity.instance().navigateToHomeTab();


        }else{

            boolean blnRefreshPage = false;

            //check if only currency or delivery destination changed
            String strCurrency = uri.getQueryParameter(ObjAppUser.FIELD_PARAMETER_CURRENCY);
            if(strCurrency!=null && !"".equals(strCurrency)){
                if(!strCurrency.equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_CURRENCY))){
                    ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_PARAMETER_CURRENCY, strCurrency);
                    blnRefreshPage = true;
                }
            }

            String strDeliveryDestination = uri.getQueryParameter(ObjAppUser.FIELD_PARAMETER_DELIVERY_DESTINATION);
            if(strDeliveryDestination!=null && !"".equals(strDeliveryDestination)){
                if(!strDeliveryDestination.equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_DELIVERY_DESTINATION))){
                    ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_PARAMETER_DELIVERY_DESTINATION, strDeliveryDestination);
                    blnRefreshPage = true;
                }
            }

            String strLanguage = uri.getQueryParameter(ObjAppUser.FIELD_PARAMETER_LANGUAGE);
            if(strLanguage!=null && !"".equals(strLanguage)){
                if(!strLanguage.equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_LANGUAGE))){
                    ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_PARAMETER_LANGUAGE, strLanguage);
                    blnRefreshPage = true;
                }
            }

            if(blnRefreshPage){
                showPleaseWait(false);
                loadUrlInWebView(strURL);
            }

        }

    }



    private void adjustAppUserToNewDomain(Uri uri){

        //Format of location change within the app:
        //https://www.wiggle.es/?curr=EUR&dest=1
        //ObjAppUser.instance().setField(ObjAppUser.FIELD_IN_APP_SELECTED_WIGGLE_STORE, strHost);
        ObjAppUser newCountryUser = new ObjAppUser();

        String strHost = uri.getHost();
        newCountryUser.setField(ObjAppUser.FIELD_CURRENT_WIGGLE_STORE, strHost);

        String strCountry = AppUtils.getCountryByTopLevelDomain(strHost);
        newCountryUser.setField(ObjAppUser.FIELD_COUNTRY, strCountry);

        String strCurrency = uri.getQueryParameter(ObjAppUser.FIELD_PARAMETER_CURRENCY);
        newCountryUser.setField(ObjAppUser.FIELD_PARAMETER_CURRENCY, strCurrency);

        String strDeliveryDestination = uri.getQueryParameter(ObjAppUser.FIELD_PARAMETER_DELIVERY_DESTINATION);
        newCountryUser.setField(ObjAppUser.FIELD_PARAMETER_DELIVERY_DESTINATION, strDeliveryDestination);

        //unfortunately this query parameter is missing .. String strLanguage = uri.getQueryParameter(ObjAppUser.FIELD_PARAMETER_LANGUAGE);
        //so using the target domain to manually set the language. TODO: improve
        String strLanguage = AppUtils.getLanguageCodeForStore(strHost);
        newCountryUser.setField(ObjAppUser.FIELD_PARAMETER_LANGUAGE, strLanguage);

        //update app user and persist changes
        ObjAppUser.instance().updateAppUser(newCountryUser);

    }

    private String getReturnURL(String strURL){

        //if login screen is displayed Wiggle return the original request URL in parameter "returnUrl"
        //e.g. https://www.wiggle.co.uk/secure/checkout/logon?returnUrl=%2Fsecure%2Fcheckout%2...
        //Use that return URL to redirect the user after login.
        Uri uri = Uri.parse(strURL);
        String strReturnURLParameter = uri.getQueryParameter("returnUrl");

        if(strReturnURLParameter==null){
            strReturnURLParameter = uri.getQueryParameter("returnurl"); //some endpoints return the parameter name in lower case ...
        }

        String strReturnURL = AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home_https)) + strReturnURLParameter;
        return strReturnURL;
    }


    public Map<String, String> addAdditionalHttpHeaders(){

        Map<String, String> headers = new HashMap<String, String>();

		/* Any additional Http headers can be added here. Before doing this check if this can be done on the webview.setSettings() though */
        //map.put(key, value);


		/* Add Wiggle specific request cookies - this is done using CookieManager.setCookie(domain, cookieNameAndValue) */

        //The Customer cookie
        String strCustomerCookieName = getString(R.string.str_http_cookie_name_customer);
        String strCustomerCookieValue = String.format(getString(R.string.str_http_cookie_name_customer_pattern)
                , ObjAppUser.instance().getString(ObjAppUser.FIELD_CUSTOMER_ID)
                , ObjAppUser.instance().getString(ObjAppUser.FIELD_DISCOUNT)
                , ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_CURRENCY)
                , ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_DELIVERY_DESTINATION)
                , ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_LANGUAGE)
        );

        //Domain can be one the user selected within the mobile site - or one chosen in the native app
        String strDomain = ObjAppUser.instance().getString(ObjAppUser.FIELD_CURRENT_WIGGLE_STORE);
        if(strDomain==null || "".equals(strDomain)){
            strDomain = AppUtils.getWiggleURL(getString(R.string.str_http_header_domain));
        }




		/*
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		c.add(Calendar.DATE, 1);
	    String strExpires = org.apache.http.impl.cookie.DateUtils.formatDate(c.getTime()); //formats date correctly to RFC2616 standard
	    */

        //The session cookie
        String strSessionCookieName = getString(R.string.str_http_cookie_name_session);
        String strSessionCookieValue = ObjAppUser.instance().getString(ObjAppUser.FIELD_ACCESS_TOKEN);


        String strCustomerCookieHeaderString = strCustomerCookieName + "=" + strCustomerCookieValue;
        String strSessionCookieHeaderString = strSessionCookieName + "=" +  strSessionCookieValue;

        //set/overwrite existing browser cookie
        //Note - we are only adding very basic cookie information here (name=value). If required set a fully qualified cookie e.g. "cookieName=cookieValue;domain=domain.com;path=/;Expires=Thu, 2 Aug 2021 20:47:11 UTC;"
        CookieManager cookieManager = CookieManager.getInstance();

		/*
		 * Version 1.02 - we do no longer add a self-made WiggleCustomer2 cookie as this later leads to THIS cookie and the actual server cookie being returned
		 * This then led to two WiggleCustomer2 settings - which becomes a problem if the settings between them differ (e.g. one says delivery destination = UK, other one says delivery destination is another country)
		 * So - commmented this out:

		cookieManager.setCookie(strDomain, strCustomerCookieHeaderString);

		*/
        cookieManager.setCookie(strDomain, strSessionCookieHeaderString);

        return headers;

    }

    private void tweakWebviewPerformance(){

        /**
         *  Android's webview can perform poorly at times .. the below uses various strategies to optimise it.
         *  Most times, setting android:hardwareAccelerated="true" in Manifest does the trick.
         *  At other times this needs disabling (enable LAYER_TYPE_SOFTWARE below).
         *  If this manifest setting does not help use a combination of the strategies below.
         *
         */

        //mWebView.getSettings().setRenderPriority(RenderPriority.HIGH); //deprecated in version 18 and no longer recommended
        //mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE); //only use when cache definitely not beneficial

        /**
         if (Build.VERSION.SDK_INT >= 11){
         mWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
         }
         **/

    }


    public String getCookieValueByName(String strDomain,String strCookieName){

        /**
         *  The cookie is a string with semi-colon separated name value pairs e.g.
         *  Cookie: nomobilewarn=true; WiggleCustomer2=xyz; SecureLogin=abc
         *
         *  We simply read this string for a given domain and split it after each semi-colon to get an individual cookie e.g.
         *
         *     WiggleCustomer2=xyz
         *
         *  We check if that string contains our cookie name (WiggleCustomer2) - if yes, then our Cookie value then is the value after the equals to sign.
         *
         */
        CookieManager cookieManager = CookieManager.getInstance();
        String strCookieValue = null;
        String strAllDomainCookies = cookieManager.getCookie(strDomain);

        if(strAllDomainCookies==null){
            return "";
        }

        String[] temp=strAllDomainCookies.split(";");
        for (String strTemp : temp ){
            if(strTemp.contains(strCookieName)){
                strCookieValue = strTemp.replace(strCookieName + "=", ""); //Wiggle speciality - remove "WiggleCustomer2=" (cookiename=) from strTemp
                return strCookieValue;
            }
        }

        return strCookieValue;
    }

	/*
	 *  In parameter:
	 *  1 - the domain that is being searched for the WiggleCustomer2 cookie
	 *  2 - the URL we are intercepting as we need to reload it with adjusted international settings
	 */

    public void listenForCustomerCookie(String strDomain, String strURL){

        class WiggleCustomer2Cookie{

            private String mCurrency;
            private String mDestination;
            private String mLanguage;
            private String mSiteDomainName;

            public WiggleCustomer2Cookie(String strCurrency, String strDestination, String strLanguage, String strSiteDomainName){
                mCurrency = strCurrency;
                mDestination = strDestination;
                mLanguage = strLanguage;
                mSiteDomainName = strSiteDomainName;
            }

            public String getCurrency(){
                return mCurrency;
            }

            public String getDestination(){
                return mDestination;
            }

            public String getLanguage(){
                return mLanguage;
            }

            public String getSiteDomain(){
                return mSiteDomainName;
            }

            public String toString(){
                String strCookie = "Currency: " + getCurrency() + "; Language: " + getLanguage() + "; Destination: " + getDestination() + "; Domain: " + getSiteDomain();
                return strCookie;
            }

        }



        /**
         *  The cookie is a string with semi-colon separated name value pairs e.g.
         *  Cookie: nomobilewarn=true; WiggleCustomer2=xyz; SecureLogin=abc
         *
         *  We simply read this string for a given domain and split it after each semi-colon to get an individual cookie e.g.
         *
         *     WiggleCustomer2=xyz
         *
         *  After certain url calls, a customer cookie is returned (opening shopping basket for instance) It can be used to track changes
         *  of the user delivery destination, currency and language. Example (relevant information shown only):
         *
         *  Cookie 2: WiggleCustomer2=CID=6204751425& .... &Cur=GBP&Dest=10&Language=en&SiteDomainName=wiggle.co.uk&ListLayout=Grid&ActualDiscountType=;
         *
         *  So we will wait for this Cookie to make an appearance and compare the values for
         *    Cur, Dest, Language, SiteDomainName
         *  with our local user and adjust the local user settings where appropriate.
         *
         *
         */
        CookieManager cookieManager = CookieManager.getInstance();
        String strAllDomainCookies = cookieManager.getCookie(strDomain);

        if(strAllDomainCookies==null){
            return; //no cookies for that domain found? Do nothing
        }


        WiggleCustomer2Cookie customerCookie = null;

        String[] temp=strAllDomainCookies.split(";");
        for (String strTemp : temp ){

            //create a WiggleCustomer2Cookie object for each time we find the WiggleCustomer2 cookie
            if(strTemp.contains("WiggleCustomer2")){

                String strParameterCurrency = "";
                String strParameterDestination = "";
                String strParameterLanguage = "";
                String strParameterSiteDomainName = "";

                String[] parameterNameValuePairs = strTemp.split("&");
                for(String strParameterNameValuePair : parameterNameValuePairs){

                    //get currency
                    if(strParameterNameValuePair.startsWith("Cur=")){
                        strParameterCurrency = strParameterNameValuePair.replace("Cur=", "");
                    }
                    //get Destination
                    if(strParameterNameValuePair.startsWith("Dest=")){
                        strParameterDestination = strParameterNameValuePair.replace("Dest=", "");
                    }
                    //get Language
                    if(strParameterNameValuePair.startsWith("Language=")){
                        strParameterLanguage = strParameterNameValuePair.replace("Language=", "");
                    }
                    //get SiteDomainName
                    if(strParameterNameValuePair.startsWith("SiteDomainName=")){
                        strParameterSiteDomainName = strParameterNameValuePair.replace("SiteDomainName=", "");
                    }

                }


                //create WiggleCustomer2 representation object so we can work with it
                customerCookie = new WiggleCustomer2Cookie(strParameterCurrency, strParameterDestination, strParameterLanguage, strParameterSiteDomainName);

            }

        }

        if(customerCookie!=null){

            boolean blnRefreshPage = false;

            String strCurrency = customerCookie.getCurrency();
            if(strCurrency!=null){
                if(!strCurrency.equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_CURRENCY))){
                    ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_PARAMETER_CURRENCY, strCurrency);
                    blnRefreshPage = true;
                }
            }


            String strDestination = customerCookie.getDestination();
            if(strDestination!=null){
                if(!strDestination.equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_DELIVERY_DESTINATION))){
                    ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_PARAMETER_DELIVERY_DESTINATION, strDestination);
                    blnRefreshPage = true;
                }
            }

            String strLanguage = customerCookie.getLanguage();
            if(strLanguage!=null){
                if(!strLanguage.equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_LANGUAGE))){
                    ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_PARAMETER_LANGUAGE, strLanguage);
                    blnRefreshPage = true;
                }
            }

            String strSiteDomain = customerCookie.getSiteDomain();
            if(strSiteDomain!=null){
                //TODO: we currently trap domain changes with other method .. check if this cookie parameter can make things easier for us by tracking it
            }


            if(blnRefreshPage){
                AppUtils.showCustomToast("Customer Cookie found! " + customerCookie.toString(), true);
                showPleaseWait(false);
                //blnForceClearHistory = true; //TODO: check for side effects. Remove if this causes issues.
                loadUrlInWebView(strURL);
            }

        }

    }

    private void updateShoppingCartCounter(){

        String strShoppingCartCookie = getCookieValueByName(AppUtils.getWiggleURL(getString(R.string.str_http_header_domain)), getString(R.string.str_http_cookie_wiggle_shopping_basket));
        //Structure of basket cookie
        //BasketId2=..&Count=..&AddtionalInfo=...
        String strShoppingCartCounter = AppUtils.getKeyValueFromNVPString(strShoppingCartCookie, "Count", "&", "=");

        TabbedHybridActivity.instance().updateShoppingCartCounter(strShoppingCartCounter);

    }


    public void loadUrlInWebView(String strTargetUrl){

        //just in case an endpoint comes back with "login required" - save last user visited URL
        strLastUserRequestedURL = strTargetUrl;
        mPassedUrl = strTargetUrl;

        if(mWebView!=null) {
            mWebView.loadUrl(strTargetUrl, addAdditionalHttpHeaders());
        }else{
            //problems with webview - remove fragment so user can try again
            TabbedHybridActivity.instance().removeSingleFragment(this);
        }

    }


    private void handleLoginRequired(String strURL){

        strLastUserRequestedURL = getReturnURL(strURL);

        if(ObjAppUser.instance().getBoolean(ObjAppUser.FIELD_REMEMBER_CREDENTIALS)){
            submitSilentLogin();
        }else{

            //Samsung fix - if in-app login already in progress, return
            if(AppSession.instance().isInAppLogin()){
                return;
            }

            hidePleaseWait();
            //store login screen caller for use on login response
            TabbedHybridActivity.instance().setLoginScreenCaller(myInstance);
            //show login screen
            TabbedHybridActivity.instance().showLoginScreen();
        }

    }



    private void submitSilentLogin(){

        AppUtils.showCustomToast("Submitting silent login ..", true);

        showPleaseWait(false);

        TabbedHybridActivity.instance().setLoginScreenCaller(myInstance);

        new ObjApiRequest(TabbedHybridActivity.instance(),
                null,
                AppRequestHandler.getAuthenticationEndpoint(),
                Request.Method.POST,
                ObjAppUser.instance(), //use saved app use details
                null,
                AppRequestHandler.ON_RESPONSE_ACTION.NO_ACTION)
                .submit();

    }



    private void setBreadcrumbText(String strUrlOrText){

        main_tvBreadcrumb.setText(strUrlOrText);
        main_tvBreadcrumb.setVisibility(View.GONE); //breadcrumb on this fragment always hidden in current version

    }


    public void openOrderDetailUrl(){

        showPleaseWait(true);
        blnForceClearHistory = true;
        loadTargetURL(R.id.orderDetail_btnMoreDetails);

    }




    public void navigateToReturnUrl(){

        if(strLastUserRequestedURL!=null){

            loadUrlInWebView(strLastUserRequestedURL);
            strLastUserRequestedURL = null;

        }

    }

    public String getBreadcrumbText(){

        switch (mWebViewCaller) {

            case R.id.navigation_Home:
                return getString(R.string.shop_Home_breadcrumb_text);

            case R.id.login_tvForgotPassword:
            case R.id.userLogin_tvForgotPassword:
                return getString(R.string.login_tvForgotPassword);

            case R.id.myAccount_btnRegister:
            case R.id.userLogin_btnRegister:
            case R.id.login_btnRegister:
                return getString(R.string.login_tvRegister);

            case R.id.myAccount_tvDetails:
                return getString(R.string.myAccount_tvDetails);

            case R.id.myAccount_tvCurrency:
                return getString(R.string.myAccount_tvCurrency);

            case R.id.myAccount_tvDeliveryDestination:
                return getString(R.string.myAccount_tvDeliveryDestination);

            case R.id.myAccount_tvHelp:
                return getString(R.string.myAccount_tvHelp);

            case R.id.myAccount_tvContactUs:
                return getString(R.string.myAccount_tvContactUs);

            default:
                return "";

        }

    }

}


