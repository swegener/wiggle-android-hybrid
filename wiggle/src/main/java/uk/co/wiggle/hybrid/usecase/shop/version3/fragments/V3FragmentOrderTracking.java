package uk.co.wiggle.hybrid.usecase.shop.version3.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjSalesOrderManager;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesItem;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesOrder;
import uk.co.wiggle.hybrid.application.helpers.AppGlobalConstants;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.application.session.AppSession;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.extensions.PicassoSSL;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentSelfCloseListener;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentUI;
import uk.co.wiggle.hybrid.tagging.AppTagManager;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.fragments.FragmentMyAccount;
import uk.co.wiggle.hybrid.usecase.shop.version3.activities.V3HybridActivity;
import uk.co.wiggle.hybrid.usecase.shop.version3.adapters.V3OrderTrackingListAdapter;


/**
 * @author Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 *
 * @license The code of this Application is licensed to Wiggle UK Ltd.
 * The license and its ownership rights can not be transferred to a Third Party.
 *
 * @android If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 **/


public class V3FragmentOrderTracking extends Fragment implements IAppFragmentUI {

    private static final String logTag = "V3FragmentOrderTracking";

    public static String getLogTag(){
        return logTag;
    }


    IAppFragmentSelfCloseListener selfCloseListener;


    String strCaller = "";
    private static V3FragmentOrderTracking myInstance;

    public static V3FragmentOrderTracking instance(){
        return myInstance;
    }

    RelativeLayout lytOrderTracking;
    RelativeLayout orderTracking_orderList;
    SwipeRefreshLayout orderList_swipeRefreshLayout;
    ListView orderList_lvOrderHistory;
    TextView orderList_tvNoData;
    ScrollView lytOrderDetail;
    V3OrderTrackingListAdapter orderTrackingAdapter;
    private OrderTrackingTask mOrderTrackingTask = new OrderTrackingTask();


    //Order Detail
    TextView orderDetail_tvTitle;
    TextView orderDetail_tvTitleEnd;
    RelativeLayout orderDetail_lytSummary;
    TextView orderDetail_tvOrderStatus;
    TextView orderDetail_tvOrderSubStatus;
    LinearLayout orderDetail_orderInfoTable;
    TextView orderDetail_tvReturns;
    TextView orderDetail_tvCancellations;
    LinearLayout orderDetail_productList;
    TextView orderDetail_lblDeliveryAddress;
    TextView orderDetail_tvDeliveryAddress;
    TextView orderDetail_lblContactDetails;
    TextView orderDetail_tvContactDetails;
    TextView orderDetail_lblOrderTotal;
    TextView orderDetail_tvSubtotalKey;
    TextView orderDetail_tvSubtotalValue;
    TextView orderDetail_tvDeliveryCostKey;
    TextView orderDetail_tvDeliveryCostValue;
    TextView orderDetail_tvSavingsKey;
    TextView orderDetail_tvSavingsValue;
    TextView orderDetail_tvOrderTotalKey;
    TextView orderDetail_tvOrderTotalValue;
    LinearLayout orderDetail_lytFAQContainer;
    TextView orderDetail_lblHelp;
    TextView orderDetail_lblWiggle;

    boolean blnSuppressScrollToStart = false;

    private ObjSalesOrder mActiveOrder;

    public ObjSalesOrder getActiveSalesOrder(){
        return mActiveOrder;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myInstance = this;


        /** Getting the arguments to the Bundle object */
        Bundle data = getArguments();

        // Get the extras
        if (!data.containsKey(AppGlobalConstants.CALLING_ACTIVITY)) {
            throw new IllegalArgumentException("The " + AppGlobalConstants.CALLING_ACTIVITY + " must be passed as extra for the intent. "
                    + "The content of the extras is " + data);
        }

        strCaller = data.getString(AppGlobalConstants.CALLING_ACTIVITY);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.v3_order_tracking_layout, container, false);


        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

       updateFragmentContent(view);

    }


    @Override
    public void updateFragmentContent(View view) {


        findViews(view);

        setCustomFontTypes();


    }



    public void findViews(View view) {

        //Order tracking
        lytOrderTracking = (RelativeLayout) view.findViewById(R.id.lytOrderTracking);
        orderTracking_orderList = (RelativeLayout) view.findViewById(R.id.orderTracking_orderList);
        orderList_swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.orderList_swipeRefreshLayout);
        setupOrderSwipeToRefreshLayout();



        orderList_lvOrderHistory = (ListView) view.findViewById(R.id.orderList_lvOrderHistory);

        //we need to make sure that swipe refresh is only possible if user is at the top of the listview. Otherwise swipe refresh is triggered
        //every time the user scrolls up from anywhere in the listview.
        //Thank you, http://nlopez.io/swiperefreshlayout-with-listview-done-right/
        orderList_lvOrderHistory.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                //this makes sure we dont swipe to refresh when scrolling up
                int topRowVerticalPosition =
                        (orderList_lvOrderHistory == null || orderList_lvOrderHistory.getChildCount() == 0) ? 0 : orderList_lvOrderHistory.getChildAt(0).getTop();
                orderList_swipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);

            }


        });


        View header = getLayoutInflater().inflate(R.layout.v3_generic_listview_header, null);
        TextView genericList_tvTitle = (TextView) header.findViewById(R.id.genericList_tvTitle);
        genericList_tvTitle.setTypeface(AppTypefaces.instance().fontMainBold);
        genericList_tvTitle.setText(getString(R.string.str_navigation_my_orders));
        orderList_lvOrderHistory.addHeaderView(header);

        orderList_tvNoData = (TextView) view.findViewById(R.id.orderList_tvNoData);
        lytOrderDetail = (ScrollView) view.findViewById(R.id.lytOrderDetail);

        setupOrderTrackingList();

        //Order Details
        orderDetail_tvTitle = (TextView) view.findViewById(R.id.orderDetail_tvTitle);
        orderDetail_tvTitleEnd = (TextView) view.findViewById(R.id.orderDetail_tvTitleEnd);
        orderDetail_lytSummary = (RelativeLayout) view.findViewById(R.id.orderDetail_lytSummary);
        orderDetail_tvOrderStatus = (TextView) view.findViewById(R.id.orderDetail_tvOrderStatus);
        orderDetail_tvOrderSubStatus = (TextView) view.findViewById(R.id.orderDetail_tvOrderSubStatus);
        orderDetail_orderInfoTable = (LinearLayout) view.findViewById(R.id.orderDetail_orderInfoTable);
        orderDetail_tvReturns = (TextView) view.findViewById(R.id.orderDetail_tvReturns);
        orderDetail_tvCancellations = (TextView) view.findViewById(R.id.orderDetail_tvCancellations);
        orderDetail_productList = (LinearLayout) view.findViewById(R.id.orderDetail_productList);
        orderDetail_lblDeliveryAddress = (TextView) view.findViewById(R.id.orderDetail_lblDeliveryAddress);
        orderDetail_tvDeliveryAddress = (TextView) view.findViewById(R.id.orderDetail_tvDeliveryAddress);
        orderDetail_lblContactDetails = (TextView) view.findViewById(R.id.orderDetail_lblContactDetails);
        orderDetail_tvContactDetails = (TextView) view.findViewById(R.id.orderDetail_tvContactDetails);
        orderDetail_lblOrderTotal = (TextView) view.findViewById(R.id.orderDetail_lblOrderTotal);
        orderDetail_tvSubtotalKey = (TextView) view.findViewById(R.id.orderDetail_tvSubtotalKey);
        orderDetail_tvSubtotalValue = (TextView) view.findViewById(R.id.orderDetail_tvSubtotalValue);
        orderDetail_tvDeliveryCostKey = (TextView) view.findViewById(R.id.orderDetail_tvDeliveryCostKey);
        orderDetail_tvDeliveryCostValue = (TextView) view.findViewById(R.id.orderDetail_tvDeliveryCostValue);
        orderDetail_tvSavingsKey = (TextView) view.findViewById(R.id.orderDetail_tvSavingsKey);
        orderDetail_tvSavingsValue = (TextView) view.findViewById(R.id.orderDetail_tvSavingsValue);
        orderDetail_tvOrderTotalKey = (TextView) view.findViewById(R.id.orderDetail_tvOrderTotalKey);
        orderDetail_tvOrderTotalValue = (TextView) view.findViewById(R.id.orderDetail_tvOrderTotalValue);
        orderDetail_lytFAQContainer = (LinearLayout) view.findViewById(R.id.orderDetail_lytFAQContainer);
        orderDetail_lblHelp = (TextView) view.findViewById(R.id.orderDetail_lblHelp);
        orderDetail_lblWiggle = (TextView) view.findViewById(R.id.orderDetail_lblWiggle);

        updateWidgets();

    }

    public void updateWidgets(){



    }

    public void initialiseNoDataText(){

        orderList_tvNoData.setText(R.string.orderList_tvNoData);

    }

    private void setCustomFontTypes(){

        orderList_tvNoData.setTypeface(AppTypefaces.instance().fontMainRegularItalic);

        orderDetail_tvTitle.setTypeface(AppTypefaces.instance().fontMainBold);
        orderDetail_tvTitleEnd.setTypeface(AppTypefaces.instance().fontMainRegular);
        orderDetail_tvOrderStatus.setTypeface(AppTypefaces.instance().fontMainBold);
        orderDetail_tvOrderSubStatus.setTypeface(AppTypefaces.instance().fontMainRegular);

        orderDetail_tvReturns.setTypeface(AppTypefaces.instance().fontMainRegular);
        orderDetail_tvCancellations.setTypeface(AppTypefaces.instance().fontMainRegular);

        orderDetail_lblDeliveryAddress.setTypeface(AppTypefaces.instance().fontMainBold);
        orderDetail_tvDeliveryAddress.setTypeface(AppTypefaces.instance().fontMainRegular);

        orderDetail_lblContactDetails.setTypeface(AppTypefaces.instance().fontMainBold);
        orderDetail_tvContactDetails.setTypeface(AppTypefaces.instance().fontMainRegular);

        orderDetail_lblOrderTotal.setTypeface(AppTypefaces.instance().fontMainBold);
        orderDetail_tvSubtotalKey.setTypeface(AppTypefaces.instance().fontMainRegular);
        orderDetail_tvSubtotalValue.setTypeface(AppTypefaces.instance().fontMainRegular);
        orderDetail_tvDeliveryCostKey.setTypeface(AppTypefaces.instance().fontMainRegular);
        orderDetail_tvDeliveryCostValue.setTypeface(AppTypefaces.instance().fontMainRegular);
        orderDetail_tvSavingsKey.setTypeface(AppTypefaces.instance().fontMainRegular);
        orderDetail_tvSavingsValue.setTypeface(AppTypefaces.instance().fontMainRegular);
        orderDetail_tvOrderTotalKey.setTypeface(AppTypefaces.instance().fontMainBold);
        orderDetail_tvOrderTotalValue.setTypeface(AppTypefaces.instance().fontMainBold);
        orderDetail_lblHelp.setTypeface(AppTypefaces.instance().fontMainBold);
        orderDetail_lblWiggle.setTypeface(AppTypefaces.instance().fontMainRegular);


    }



    @Override
    public void onStart() {
        super.onStart();

        setTitleBarText();

    }

    @Override
    public void onResume() {

        retrieveOrderHistory();

        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();

        //special handling for Order tracking screen:
        // if user decides to view this screen (i.e it is visible), replace the cached version of the order tracking json
        // with the latest json payload received from the API.
        //We do this when the screen is either closed or sent to background after viewing i.e. the user is finished checking his orders
        //Doing this when this screen is opened leads to subtle problems
        AppSession.instance().acceptOrderTrackingPayloadIntoCache();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);


    }



    //Check if this is working - this is the new version of deprecated
    //public void onAttach(Activity activity)
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            selfCloseListener = (IAppFragmentSelfCloseListener) context;
        } catch (ClassCastException e) {
            throw new RuntimeException(getActivity().getClass().getName() + " must implement IAppFragmentSelfCloseListener to use this fragment", e);
        }

    }


    public void onClickEvent(int intClickedViewId){

       switch (intClickedViewId) {

           case R.id.orderDetail_btnTrackOrder:
               openOrderDetailUrl();
               break;

           case R.id.orderDetail_btnContactUs:
               String strTargetUrl = AppUtils.getWiggleURL(getString(R.string.str_url_contact_us));
               //append order Id
               strTargetUrl = strTargetUrl + "?OrderId=" + mActiveOrder.getString(ObjSalesOrder.FIELD_ORDER_ID);
               V3HybridActivity.instance().setBreadcrumbText(getString(R.string.myAccount_tvContactUs));
               V3HybridActivity.instance().loadUrlInWebViewFragment(strTargetUrl);
               break;
           case R.id.orderDetail_tvReturns:
           case R.id.orderDetail_tvCancellations:
                   goToWebForm(intClickedViewId);
               break;

            default:
                break;

        }

    }

    private void goToWebForm(int intPressedViewId){

        V3HybridActivity.instance().loadUrlForViewInWebViewFragment(intPressedViewId);

    }



    private void setupOrderTrackingList(){

        orderTrackingAdapter = new V3OrderTrackingListAdapter();
        orderList_lvOrderHistory.setAdapter(orderTrackingAdapter);

    }

    public void updateOrderTrackingList(){

        if(lytOrderTracking.getVisibility()==View.GONE){
            displayLayout(lytOrderTracking);
        }

        if(orderTrackingAdapter==null){
            return;
        }

        getActivity().runOnUiThread(mOrderTrackingTask);
    }


    private void displayLayout(View viewToShow){

        lytOrderTracking.setVisibility(View.GONE);
        lytOrderDetail.setVisibility(View.GONE);

        viewToShow.setVisibility(View.VISIBLE);

    }

    private class OrderTrackingTask implements Runnable {

        @Override
        public void run() {


            List<ObjSalesOrder> orderList = ObjSalesOrderManager.getInstance().getAll();

            if(orderList.size()>0){

                orderTrackingAdapter.setListContent(orderList);
                orderTrackingAdapter.notifyDataSetChanged();

                orderList_lvOrderHistory.setVisibility(View.VISIBLE);
                orderList_tvNoData.setVisibility(View.GONE);

                checkForNewDeliveredOrder();

                //check if we need to show a badge in the navigation browser
                V3HybridActivity.instance().updateOrderTrackingBadge();

                if(!blnSuppressScrollToStart){
                    scrollToStartOfList(orderList_lvOrderHistory);
                }else{
                    blnSuppressScrollToStart = false; //reset
                }

            }else{

                orderList_lvOrderHistory.setVisibility(View.GONE);
                orderList_tvNoData.setVisibility(View.VISIBLE);

            }

            orderList_swipeRefreshLayout.setRefreshing(false);

        }

    }


    private void checkForNewDeliveredOrder(){

        //If there as at least one order that changed to status "Delivered" since last refresh - show Trustpilot review prompt
        if(ObjSalesOrderManager.getInstance().getJustDeliveredOrderList().size()>0){
            ObjSalesOrder salesOrder = ObjSalesOrderManager.getInstance().getJustDeliveredOrderList().get(0);
            V3HybridActivity.instance().showReviewOrderDialog(salesOrder);
        }

    }


    /**
     * setUp swipeRefreshLayout to refresh current fragment data
     */
    private void setupOrderSwipeToRefreshLayout(){

        orderList_swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                //clear order manager as we want to update it
                ObjSalesOrderManager.getInstance().clear();

                //Order screen: when user pulls to refresh, the response needs accepting into the cache as soon as data arrives as user
                //views the result immediately.
                V3HybridActivity.instance().setAcceptOrderTrackingResponseOnSwipeRefresh(true);

                retrieveOrderHistory(); //refresh everything including news config
            }
        });


        orderList_swipeRefreshLayout.setColorSchemeResources(
                R.color.orangeButton,
                R.color.backgroundGreyDark,
                R.color.orangeButton,
                R.color.backgroundGreyDark
        );

    }

    public void onOrderedItemClick(ObjSalesOrder order){

        this.mActiveOrder = order;

        AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_my_orders), "myordersItemStatus");

        //AppUtils.showCustomToast("Clicked on order Id: " + parentOrder.getString(ObjSalesOrder.FIELD_ORDER_ID), true);

        showOrderDetail(order);

    }

    private void showOrderDetail(ObjSalesOrder order){



        AppTagManager.instance().sendVersion2ScreenView("App - Track My Order - Item Status");

        //flag this order as "viewed" (resets "updated" label)
        order.setField(ObjSalesOrder.FIELD_ORDER_NEW_INFORMATION, false);
        updateAllOrderTrackingBadges(); //update order counter badges on NavTab and MyAccount screen

        String strOrderNumber = "#" + order.getString(ObjSalesOrder.FIELD_ORDER_ID);
        orderDetail_tvTitleEnd.setText(strOrderNumber);

        String strOrderStatus = order.getDisplayOrderStatus();
        orderDetail_tvOrderStatus.setText(strOrderStatus);

        String strDeliveryStatusDate = order.getString(ObjSalesOrder.FIELD_STATUS_DETAILS);
        orderDetail_tvOrderSubStatus.setText(strDeliveryStatusDate);
        setOrderStatusColour(orderDetail_lytSummary, order);

        buildOrderInfoTable();

        buildOrderProductList();


        String strFormattedAddress = order.getFormattedAddress();
        orderDetail_tvDeliveryAddress.setText(strFormattedAddress);

        String strCustomerName = ObjAppUser.instance().getFullName();
        orderDetail_tvContactDetails.setText(strCustomerName);

        String strOrderSubTotal = order.getString(ObjSalesOrder.FIELD_TOTAL_FORMATTED);
        orderDetail_tvSubtotalValue.setText(strOrderSubTotal);


        orderDetail_tvDeliveryCostValue.setText("---");

        orderDetail_tvSavingsValue.setText("---");

        String strOrderTotal = order.getString(ObjSalesOrder.FIELD_TOTAL_FORMATTED);
        orderDetail_tvOrderTotalValue.setText(strOrderTotal);

        buildCustomerHelpWidget();


        lytOrderDetail.setVisibility(View.VISIBLE);
        lytOrderDetail.scrollTo(0, 0); //scroll to start of list (as might have been used earlier)


    }

    private void openOrderDetailUrl(){

        V3HybridActivity.instance().loadUrlInWebViewFragment(mActiveOrder.getString(ObjSalesOrder.FIELD_DETAILS_URL));

    }


    private void setOrderStatusColour(View view, ObjSalesOrder order){

        int intOrderStatusColor = order.getOrderStatusColour();
        view.setBackgroundColor(intOrderStatusColor);

    }


    private void scrollToStartOfList(final AbsListView listView){

        //do nothing if list not initialised (crash-fix: NPE)
        if(listView==null){
            return;
        }

        //needed to move this statment to a runnable as this does not work immediately after notifydatasetchanged
        //newsfeedHome_lvListView.setSelection(0);
        listView.post(new Runnable() {

            @Override
            public void run() {
                listView.setSelection(0);
            }
        });


    }


    private void retrieveOrderHistory(){

        orderList_tvNoData.setText(R.string.orderList_tvNoData_pleaseWait);

        updateOrderTrackingList();

        if(ObjSalesOrderManager.getInstance().size()==0){
            orderList_swipeRefreshLayout.setRefreshing(true);
            AppRequestHandler.instance().retrieveOrderData(V3HybridActivity.instance(), AppRequestHandler.ON_RESPONSE_ACTION.SUBMIT_ORDER_TRACKING_REQUEST);
        }

    }


    public boolean orderTrackingScreenCanGoBack(boolean blnBackWasPressed){


        if(lytOrderDetail.getVisibility()==View.VISIBLE){

            if(blnBackWasPressed) {
                orderTrackingScreenGoBack();
            }
            return true;
        }else{

            return false;
        }


    }

    private void orderTrackingScreenGoBack(){

        lytOrderTracking.setVisibility(View.VISIBLE);
        lytOrderDetail.setVisibility(View.GONE);
    }

    private void updateAllOrderTrackingBadges(){

        //update notification badge on My Account tab
        V3HybridActivity.instance().updateOrderTrackingBadge();

        //update order tracking counter on My Account screen
        if(FragmentMyAccount.instance()!=null
                && FragmentMyAccount.instance().isAdded()){
            FragmentMyAccount.instance().updateOrderTrackingBadge();
        }

        //update "new" status icon on order tracking list
        updateOrderTrackingList();

    }

    @Override
    public void onHiddenChanged(boolean hidden){

        if(!hidden){
            setTitleBarText();
        }
    }

    private void setTitleBarText(){

        if(V3HybridActivity.instance()!=null){
            Bundle data = getArguments();

            // Get the extras
            if (data.containsKey(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE)) {

                String strTitle = data.getString(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE);
                V3HybridActivity.instance().setBreadcrumbText(strTitle);

            }

        }


    }

    private void buildOrderInfoTable(){

        //remove all drawables first if any already available ..
        if (orderDetail_orderInfoTable.getChildCount() > 0) {
            AppUtils.unbindDrawables(orderDetail_orderInfoTable);
        }

        addOrderInfoTableCell(getString(R.string.str_order_detail_payment), mActiveOrder.getPaymentStatus());
        addOrderInfoTableCell(getString(R.string.str_order_detail_packing), mActiveOrder.getPackingStatus());
        addOrderInfoTableCell(getString(R.string.str_order_detail_shipping), mActiveOrder.getShippingStatus());
        addOrderInfoTableCell(getString(R.string.str_order_detail_delivery), mActiveOrder.getDeliveryStatus());


    }

    private void addOrderInfoTableCell(String strKey, ObjSalesOrder.StepStatus stepStatus){

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final RelativeLayout orderInfoCell = (RelativeLayout) inflater.inflate(R.layout.v3_order_detail_info_table_cell, lytOrderDetail, false);

        TextView orderInfoCell_tvKey = (TextView) orderInfoCell.findViewById(R.id.orderInfoCell_tvKey);
        orderInfoCell_tvKey.setTypeface(AppTypefaces.instance().fontMainBold);
        orderInfoCell_tvKey.setText(strKey);

        TextView orderInfoCell_tvValue = (TextView) orderInfoCell.findViewById(R.id.orderInfoCell_tvValue);
        orderInfoCell_tvValue.setTypeface(AppTypefaces.instance().fontMainRegular);
        orderInfoCell_tvValue.setText(stepStatus.getStatusValue());

        int intStatusColour = stepStatus.getStatusColor();
        orderInfoCell.setBackgroundColor(intStatusColour);

        orderDetail_orderInfoTable.addView(orderInfoCell);

    }


    private void buildOrderProductList(){

        //remove all drawables first if any already available ..
        if (orderDetail_productList.getChildCount() > 0) {
            AppUtils.unbindDrawables(orderDetail_productList);
        }

        for(int i=0;i<mActiveOrder.getSalesItems().size();i++){
            ObjSalesItem product = mActiveOrder.getSalesItems().get(i);
            addProductCell(product);
        }

    }

    private void addProductCell(final ObjSalesItem product){

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final RelativeLayout productCell = (RelativeLayout) inflater.inflate(R.layout.v3_order_detail_product_cell, lytOrderDetail, false);

        ImageView orderDetail_ivProductImage = (ImageView) productCell.findViewById(R.id.orderDetail_ivProductImage);
        String strProductImageUrl = product.getImagePath("?w=200&amp;h=200&amp;a=0");
        PicassoSSL.with(ApplicationContextProvider.getContext()).load(strProductImageUrl).into(orderDetail_ivProductImage);

        TextView orderDetail_tvProductName = (TextView) productCell.findViewById(R.id.orderDetail_tvProductName);
        orderDetail_tvProductName.setTypeface(AppTypefaces.instance().fontMainBold);
        orderDetail_tvProductName.setText(product.getString(ObjSalesItem.FIELD_PRODUCT_NAME));

        TextView orderDetail_tvProductAdditionalInfo = (TextView) productCell.findViewById(R.id.orderDetail_tvProductAdditionalInfo);
        orderDetail_tvProductAdditionalInfo.setTypeface(AppTypefaces.instance().fontMainRegular);
        String strAdditionalInfo = product.getAdditionalInfo();
        orderDetail_tvProductAdditionalInfo.setText(strAdditionalInfo);

        TextView orderDetail_tvProductPrice = (TextView) productCell.findViewById(R.id.orderDetail_tvProductPrice);
        orderDetail_tvProductPrice.setTypeface(AppTypefaces.instance().fontMainRegular);
        orderDetail_tvProductPrice.setText(product.getString(ObjSalesItem.FIELD_LOCAL_LINE_TOTAL_FORMATTED));

        TextView orderDetail_tvOrderAgain = (TextView) productCell.findViewById(R.id.orderDetail_tvOrderAgain);
        orderDetail_tvProductPrice.setTypeface(AppTypefaces.instance().fontMainRegular);

        orderDetail_tvOrderAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Go to product URL
                V3HybridActivity.instance().loadUrlInWebViewFragment(product.getString(ObjSalesItem.FIELD_PRODUCT_URL));
            }
        });


        orderDetail_productList.addView(productCell);

    }


    private void buildCustomerHelpWidget(){

        //remove all drawables first if any already available ..
        if (orderDetail_lytFAQContainer.getChildCount() > 0) {
            AppUtils.unbindDrawables(orderDetail_lytFAQContainer);
        }

        addHyperlinkWidget(orderDetail_lytFAQContainer, getString(R.string.str_help_faq_order_issues), AppUtils.getWiggleURL(getString(R.string.str_url_help_faq_order_issues)));

        //Different returns URL for UK and international customers
        /*
        String strReturnsUrl = getString(R.string.str_url_returns_information_world);
        if(getString(R.string.str_locale_country_iso_code_uk).equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY))){
            strReturnsUrl = getString(R.string.str_url_returns_information_uk);
        }
        addHyperlinkWidget(orderDetail_lytFAQContainer, getString(R.string.str_help_faq_returns), AppUtils.getWiggleURL(strReturnsUrl));

         */

        addHyperlinkWidget(orderDetail_lytFAQContainer, getString(R.string.str_help_faq_cancel_order), AppUtils.getWiggleURL(getString(R.string.str_url_help_faq_cancel_order)));
        addHyperlinkWidget(orderDetail_lytFAQContainer, getString(R.string.str_help_faq_late_delivery), AppUtils.getWiggleURL(getString(R.string.str_url_help_faq_late_delivery)));

    }

    private void addHyperlinkWidget(LinearLayout targetLayout, String strDisplayText, final String strTargetUrl){

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final RelativeLayout hyperlinkWidget = (RelativeLayout) inflater.inflate(R.layout.v3_order_detail_hyperlink_widget, lytOrderDetail, false);

        TextView hyperlinkWidget_tvLink = (TextView) hyperlinkWidget.findViewById(R.id.hyperlinkWidget_tvLink);
        //underline text
        SpannableString strUnderlinedText = new SpannableString(strDisplayText);
        strUnderlinedText.setSpan(new UnderlineSpan(), 0, strDisplayText.length(), 0);
        hyperlinkWidget_tvLink.setText(strUnderlinedText);
        hyperlinkWidget_tvLink.setTypeface(AppTypefaces.instance().fontMainRegular);


        hyperlinkWidget_tvLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Go to product URL
                V3HybridActivity.instance().loadUrlInWebViewFragment(strTargetUrl);
            }
        });


        targetLayout.addView(hyperlinkWidget);

    }


}


