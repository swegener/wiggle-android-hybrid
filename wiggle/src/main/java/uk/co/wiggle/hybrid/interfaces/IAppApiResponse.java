package uk.co.wiggle.hybrid.interfaces;


import org.json.JSONObject;

import uk.co.wiggle.hybrid.application.datamodel.objects.ObjApiRequest;


/**
 * 
 * @author AndroMedia
 * 
 * All Activities submitting Volley requests should implement this interface so that they can handle Api-network responses appropriately
 *
 */
public interface IAppApiResponse {
	
	public void onApiRequestSuccess(ObjApiRequest apiRequest, JSONObject apiSuccessDetail);

	public void onApiRequestError(ObjApiRequest request, int intHttpResponseCode, JSONObject apiErrorDetail);
		
}
