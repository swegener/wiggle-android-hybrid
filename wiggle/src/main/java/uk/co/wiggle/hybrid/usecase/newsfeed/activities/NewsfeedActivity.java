package uk.co.wiggle.hybrid.usecase.newsfeed.activities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;

import com.android.volley.Request.Method;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.usecase.shop.classicnav.activities.HybridActivity;
import uk.co.wiggle.hybrid.usecase.newsfeed.adapters.NewsfeedCategoryAdapter;
import uk.co.wiggle.hybrid.usecase.newsfeed.adapters.NewsfeedHomeAdapter;
import uk.co.wiggle.hybrid.usecase.newsfeed.adapters.NewsfeedSportsAdapter;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler.ON_RESPONSE_ACTION;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjNewsfeedCategoryManager;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjNewsfeedManager;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjNewsfeedSportsManager;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjApiRequest;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNewsfeed;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNewsfeedCategory;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNewsfeedRSSItem;
import uk.co.wiggle.hybrid.application.animations.AppAnimations;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.application.utils.DateUtils;
import uk.co.wiggle.hybrid.extensions.NestedListView;
import uk.co.wiggle.hybrid.extensions.PicassoSSL;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.interfaces.IAppActivityUI;
import uk.co.wiggle.hybrid.interfaces.IAppApiResponse;
import uk.co.wiggle.hybrid.tagging.AppTagManager;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Browser;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.text.Html;
import android.text.Spanned;
import android.view.*;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/


public class NewsfeedActivity extends Activity implements View.OnClickListener, IAppActivityUI, IAppApiResponse {
	
	private static final String logTag = "NewsfeedActivity";

	public static String getLogTag(){
		return logTag;
	}
	
    //allow other activities to read an instance of MainActivity so that MainActivity methods can be called
	private static NewsfeedActivity myInstance;

	public static NewsfeedActivity instance(){
		return myInstance;
	}
	
	//Widgets
	RelativeLayout lytNewsfeedHome;
	TextView newsfeedHome_tvNews;
	LinearLayout newsfeedHome_lytSettings;
	TextView newsfeedHome_tvSettings;
	TextView newsfeedHome_tvBack;
	TextView newsfeedHome_tvGoogleMaterialSettings;
	TextView newsfeedHome_tvUserGuide;
	AbsListView newsfeedHome_lvListView;
	NewsfeedHomeAdapter newsfeedHomeAdapter;
	private NewsfeedHomeListUpdateTask mNewsfeedHomeListUpdateTask = new NewsfeedHomeListUpdateTask();
	SwipeRefreshLayout newsfeedHome_swipeRefreshLayout;
	TextView newsfeedHome_tvNoData;
	TextView newsPreferencesHint_tvHint;
	LinearLayout newsfeedHome_lytNewsfeedHint;
	
	//Newsfeed settings
	RelativeLayout lytNewsfeedSettings;
	TextView newsfeedSettings_tvNewsPreferences;
	TextView newsfeedSettings_tvUserGuide;
	TextView newsfeedSettings_tvBrands;
	
	NewsfeedCategoryAdapter categoryAdapter;
	NestedListView newsfeedSettings_lvNewsfeedCategories;
	private NewsfeedCategoryTask mNewsfeedCategoryTask = new NewsfeedCategoryTask();
	
	NewsfeedSportsAdapter sportsAdapter;
	NestedListView newsfeedSettings_lvNewsfeedSports;
	private NewsfeedSportsTask mNewsfeedSportsTask = new NewsfeedSportsTask();
	
	//Newsfeed detail
	LinearLayout lytNewsfeedDetail;
	TextView newsfeedDetail_tvNews;
	ImageView newsfeedDetail_ivNewsImage;
	ImageView newsfeedDetail_ivBrandImage;
	TextView newsfeedDetail_tvTitle;
	TextView newsfeedDetail_tvReadMore;
	TextView newsfeedDetail_tvDate;
	WebView newsfeedDetail_webView;
	ScrollView newsfeedDetail_svScrollView;
	LinearLayout newsfeedDetail_lytShare;
	TextView newsfeedDetail_tvShare;
	TextView newsfeedDetail_tvGoogleMaterialShare;
	
	ObjNewsfeedRSSItem mActiveNewsItem; //news item currently active on detail page
	
	//List of outstanding newsfeed requests
	ArrayList<ObjNewsfeed> lstQueriedNewsfeeds = new ArrayList<ObjNewsfeed>();
	ArrayList<ObjNewsfeed> lstCurrentRequestBatch = new ArrayList<ObjNewsfeed>();
	int mNewsfeedQueueCounter = 0;
	
	//Track changes on settings screen
	private boolean mBlnSettingsChanged;
	
	
	
   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        setContentView(R.layout.newsfeed_activity);
        
        myInstance = this;
        
		findViews();
		
		displayLayout(R.id.newsfeedHome);
		       

    }
   
    @Override
    public void onResume(){
    	
    	super.onResume();
    	
    	//no newsfeed configuration downloaded - get config.json from server
    	if(ObjNewsfeedManager.getInstance().size()==0 || ObjNewsfeedCategoryManager.getInstance().size()==0){
    		retrieveNewsfeedConfiguration();
    	}
    	
    	updateNewsfeedHomeList();
    	
        
    }
   

	  
	@Override
	public void onApiRequestSuccess(ObjApiRequest apiRequest,
			JSONObject apiSuccessDetail) {
		
		if(AppRequestHandler.getAuthenticationEndpoint().equals(apiRequest.getURLEndpoint())){
			
			
		}else if(AppRequestHandler.URL_NEWSFEED_CONFIG_FILE.equals(apiRequest.getURLEndpoint())){
			
			AppUtils.showCustomToast("Newsfeed categories: " + ObjNewsfeedCategoryManager.getInstance().size() + 
					"\nNewsfeeds: " + ObjNewsfeedManager.getInstance().size()
					, true);
			
			//get newsfeeds
			retrieveNews();
			
		}else if(AppRequestHandler.URL_NEWSFEED_SPORTS_CONFIG_FILE.equals(apiRequest.getURLEndpoint())){
			
			AppUtils.showCustomToast("Newsfeed sports: " + ObjNewsfeedSportsManager.getInstance().size()
					, true);
			
			
		}else if(AppRequestHandler.ENDPOINT_NEWSFEED_DUMMY.equals(apiRequest.getURLEndpoint())){
			
			handleNewsfeedRequestProcessed(apiRequest);
			
		}
		
	}

	@Override
	public void onApiRequestError(ObjApiRequest apiRequest,
			int intHttpResponseCode, JSONObject apiErrorDetail) {

		if(AppRequestHandler.getAuthenticationEndpoint().equals(apiRequest.getURLEndpoint())){
			
			
		}else if(AppRequestHandler.ENDPOINT_NEWSFEED_DUMMY.equals(apiRequest.getURLEndpoint())){
			
			handleNewsfeedRequestProcessed(apiRequest);
			
		}		
		
	}
	
	private void handleNewsfeedRequestProcessed(ObjApiRequest apiRequest){
		
		//if this was called from onApiRequestSuccess - this newsfeed item now should have RSS channels and RSS Items linked to it
		ObjNewsfeed processedFeed = (ObjNewsfeed) apiRequest.getApiObject();
		lstQueriedNewsfeeds.remove(processedFeed); //remove feed in case of error or success
		
		//remove request from current batch
		lstCurrentRequestBatch.remove(processedFeed);
		
		updateNewsfeedCounter();
		
		//all newsfeed data retrieved - stop swipe refresh animation and show result
		if(lstQueriedNewsfeeds.size()==0){
			updateNewsfeedHomeList();
		}else{
			//no more requests in batch, but still unprocessed newsfeeds? get next batch .. 
			if(lstCurrentRequestBatch.size()==0){
				submitNewsfeedRequesBatch();
			}
		}
		
	}
	
	private void updateNewsfeedCounter(){
		
		String strPleaseWait = String.format(
										getString(R.string.newsfeedHome_tvNoData_pleaseWait),
										mNewsfeedQueueCounter - lstQueriedNewsfeeds.size() + 1,
										mNewsfeedQueueCounter
										);
		newsfeedHome_tvNoData.setText(strPleaseWait);
		
	}

	@Override
	public void setupCustomActionBar() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCustomFontTypes() {
		
		newsfeedHome_tvNews.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		newsfeedHome_tvSettings.setTypeface(AppTypefaces.instance().fontMainRegular);
		newsfeedHome_tvBack.setTypeface(AppTypefaces.instance().fontMainRegular);
		newsfeedHome_tvGoogleMaterialSettings.setTypeface(AppTypefaces.instance().fontGoogleMaterial);
		newsfeedHome_tvUserGuide.setTypeface(AppTypefaces.instance().fontMainRegular);
		newsfeedHome_tvNoData.setTypeface(AppTypefaces.instance().fontMainBold);
		newsPreferencesHint_tvHint.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		
		newsfeedSettings_tvNewsPreferences.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		newsfeedSettings_tvUserGuide.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		newsfeedSettings_tvBrands.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		
		newsfeedDetail_tvNews.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		newsfeedDetail_tvTitle.setTypeface(AppTypefaces.instance().fontMainBold);
		newsfeedDetail_tvReadMore.setTypeface(AppTypefaces.instance().fontMainRegular);
		newsfeedDetail_tvDate.setTypeface(AppTypefaces.instance().fontMainRegular);
		
		newsfeedDetail_tvShare.setTypeface(AppTypefaces.instance().fontMainRegular);
		newsfeedDetail_tvGoogleMaterialShare.setTypeface(AppTypefaces.instance().fontGoogleMaterial);
		
		
		
	}

	@Override
	public void findViews() {
		
		//Newsfeed Home
		lytNewsfeedHome = (RelativeLayout) findViewById(R.id.newsfeedHome);
		newsfeedHome_tvNews = (TextView) findViewById(R.id.newsfeedHome_tvNews);
		newsfeedHome_lytSettings = (LinearLayout) findViewById(R.id.newsfeedHome_lytSettings);
		newsfeedHome_tvGoogleMaterialSettings = (TextView) findViewById(R.id.newsfeedHome_tvGoogleMaterialSettings);
		newsfeedHome_tvSettings = (TextView) findViewById(R.id.newsfeedHome_tvSettings);
		newsfeedHome_tvBack = (TextView) findViewById(R.id.newsfeedHome_tvBack);
		newsfeedHome_tvUserGuide = (TextView) findViewById(R.id.newsfeedHome_tvUserGuide);
		newsfeedHome_swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.newsfeedHome_swipeRefreshLayout);
		setupSwipeToRefreshLayout();
		
		newsfeedHome_lvListView = (AbsListView) findViewById(R.id.newsfeedHome_lvListView);
		
		//we need to make sure that swipe refresh is only possible if user is at the top of the listview. Otherwise swipe refresh is triggered 
		//every time the user scrolls up from anywhere in the listview. 
		//Thank you, http://nlopez.io/swiperefreshlayout-with-listview-done-right/
		newsfeedHome_lvListView.setOnScrollListener(new AbsListView.OnScrollListener() {  
			  @Override
			  public void onScrollStateChanged(AbsListView view, int scrollState) {

			  }

			  @Override
			  public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				  
			    int topRowVerticalPosition = 
			      (newsfeedHome_lvListView == null || newsfeedHome_lvListView.getChildCount() == 0) ? 0 : newsfeedHome_lvListView.getChildAt(0).getTop();
			    newsfeedHome_swipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
			  }
		});
		
		newsfeedHome_tvNoData = (TextView) findViewById(R.id.newsfeedHome_tvNoData);
		newsPreferencesHint_tvHint = (TextView) findViewById(R.id.newsPreferencesHint_tvHint);
		newsfeedHome_lytNewsfeedHint = (LinearLayout) findViewById(R.id.newsfeedHome_lytNewsfeedHint);
		
		//Newsfeed Settings
		lytNewsfeedSettings = (RelativeLayout) findViewById(R.id.newsfeedSettings);
		newsfeedSettings_tvNewsPreferences = (TextView) findViewById(R.id.newsfeedSettings_tvNewsPreferences);
		newsfeedSettings_tvUserGuide = (TextView) findViewById(R.id.newsfeedSettings_tvUserGuide);
		newsfeedSettings_tvBrands = (TextView) findViewById(R.id.newsfeedSettings_tvBrands);
		newsfeedSettings_lvNewsfeedCategories = (NestedListView) findViewById(R.id.newsfeedSettings_lvNewsfeedCategories);
		newsfeedSettings_lvNewsfeedSports = (NestedListView) findViewById(R.id.newsfeedSettings_lvNewsfeedSports);
		
		//Newsfeed Detail
		lytNewsfeedDetail = (LinearLayout) findViewById(R.id.newsfeedDetail);
		newsfeedDetail_tvNews = (TextView) findViewById(R.id.newsfeedDetail_tvNews);
		newsfeedDetail_ivNewsImage = (ImageView) findViewById(R.id.newsfeedDetail_ivNewsImage);
		newsfeedDetail_ivBrandImage = (ImageView) findViewById(R.id.newsfeedDetail_ivBrandImage);
		newsfeedDetail_tvTitle = (TextView) findViewById(R.id.newsfeedDetail_tvTitle);
		newsfeedDetail_tvReadMore = (TextView) findViewById(R.id.newsfeedDetail_tvReadMore);
		newsfeedDetail_tvDate = (TextView) findViewById(R.id.newsfeedDetail_tvDate);
		newsfeedDetail_webView = (WebView) findViewById(R.id.newsfeedDetail_webView);
		setupWebView();
		newsfeedDetail_svScrollView = (ScrollView) findViewById(R.id.newsfeedDetail_svScrollView);
		newsfeedDetail_lytShare = (LinearLayout) findViewById(R.id.newsfeedDetail_lytShare);
		newsfeedDetail_tvShare = (TextView) findViewById(R.id.newsfeedDetail_tvShare);
		newsfeedDetail_tvGoogleMaterialShare = (TextView) findViewById(R.id.newsfeedDetail_tvGoogleMaterialShare);
		
		
		//add changing background to image views
		/*
		setDependantImageViewListener(startup_lytLegal, startup_ivLegal, getResources().getColor(R.color.orangeButton));
		*/
		
		//standard methods to style textviews
		setCustomFontTypes();
		
		setupCategoryGallery();
		setupNewsfeedHomeListview();
	}

	private void setupWebView(){
		
		//Configure web view to display RSS items in an optimum way
		
		/* Note: the following scales the web page content to the width of the screen. With large images embedded the 
		   text becomes too small to read though
		newsfeedDetail_webView.getSettings().setLoadWithOverviewMode(true);
		newsfeedDetail_webView.getSettings().setUseWideViewPort(true);
	    */
		
		//disable scrolling on webview - so we can do this in scrollview
		// disable scroll on touch
		newsfeedDetail_webView.setOnTouchListener(new View.OnTouchListener() {
		    @Override
		    public boolean onTouch(View v, MotionEvent event) {
		      return (event.getAction() == MotionEvent.ACTION_MOVE);
		    }
		  });
		
		newsfeedDetail_webView.getSettings().setJavaScriptEnabled(true); //yes, that is a security issue ... but given that we only load known and no external URLs this is in control
		
		newsfeedDetail_webView.setWebViewClient(new WebViewClient() {
		    @Override
		    public boolean shouldOverrideUrlLoading(WebView view, String url) {
		        if (url.contains("www.wiggle.co.uk/")) {
		            openLinkInWiggleHybridActivity(url);
		            return true;
		        }
		        return false;
		    }
		});
		
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		int id = v.getId();
		
		switch (id) {
		
			/* Widgets starting app internal web view */
			case R.id.newsfeed_lytBack:
				onBackPressed();
				break;
				
			case R.id.newsfeedHome_lytSettings:
				AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_news_preferences), "selectNewsCategories");
				displayLayout(R.id.newsfeedSettings);
				break;
				
			case R.id.newsfeedDetail_lytShare:
				shareNews(mActiveNewsItem);
				break;
			
			case R.id.newsfeedDetail_tvReadMore:
				openLinkInExternalBrowser(mActiveNewsItem);
				break;
				
			case R.id.newsfeedHome_lytNewsfeedHint:
				hidePreferencesPopup();
				break;
				
		default:
			break;
			
		}
		
	}
	
	
	private void openUrlInExternalBrowser(String strTargetURL){
		
		Intent externalBrowserIntent = new Intent(Intent.ACTION_VIEW);
		externalBrowserIntent.setData(Uri.parse(strTargetURL));
		startActivity(externalBrowserIntent);
		
	}
	
	private void displayLayout(int layoutId){
		
		lytNewsfeedHome.setVisibility(View.GONE);
		lytNewsfeedSettings.setVisibility(View.GONE);
		lytNewsfeedDetail.setVisibility(View.GONE);
		
		//set passed view visible
		View layoutView = findViewById(layoutId);
		layoutView.setVisibility(View.VISIBLE);
		
		if(layoutId==R.id.newsfeedHome){
			newsfeedHome_lytSettings.setVisibility(View.VISIBLE);
		}else{
			newsfeedHome_lytSettings.setVisibility(View.GONE);
		}
		
		if(layoutId==R.id.newsfeedDetail){
			newsfeedDetail_lytShare.setVisibility(View.VISIBLE);
			newsfeedHome_lytNewsfeedHint.setVisibility(View.INVISIBLE);
		}else{
			newsfeedDetail_lytShare.setVisibility(View.GONE);
		}
		
		if(layoutId==R.id.newsfeedSettings){
			newsfeedHome_lytNewsfeedHint.setVisibility(View.INVISIBLE);
			updateCategoryList();
		}
		
		
				

	}
	
	@Override
	public void onBackPressed(){
		
		if(lytNewsfeedDetail.getVisibility()==View.VISIBLE){
			
			displayLayout(R.id.newsfeedHome);
			//as we re-use this webview: clear content first:
			newsfeedDetail_webView.loadUrl("about:blank"); //Done when detail screen is closed. This helps to stop Youtube videos etc to continue playing too
			return;
		}
		
		if(lytNewsfeedSettings.getVisibility()==View.VISIBLE
				){
			
			displayLayout(R.id.newsfeedHome);
			
			//if changes were applied to settings screen - update news feed
			if(mBlnSettingsChanged){
				mBlnSettingsChanged = false;
				//retrieveNews();
				retrieveNewsfeedConfiguration(); //refresh everything including news config
			}
			return;
		}
		
		AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_general), "generalBack");
		
		super.onBackPressed();
	}
	

	
	/**
	 * setUp swipeRefreshLayout to refresh current fragment data
	 */
	private void setupSwipeToRefreshLayout(){
		
		newsfeedHome_swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

        	@Override
        	public void onRefresh() {
        		retrieveNewsfeedConfiguration(); //refresh everything including news config
        	}
        });

        
		newsfeedHome_swipeRefreshLayout.setColorSchemeResources(
        		R.color.orangeButton,
        		R.color.backgroundGreyDark,
        		R.color.orangeButton,
        		R.color.backgroundGreyDark
        		);
		
	}
	
	private void retrieveNews(){
		
		newsfeedHome_swipeRefreshLayout.setRefreshing(true);
		updateNewsfeedCounter();
		newsfeedHome_tvNoData.setVisibility(View.VISIBLE);
		newsfeedHome_lvListView.setVisibility(View.GONE);
		
		ArrayList<ObjNewsfeed> lstNewsfeeds = ObjNewsfeedManager.getInstance().getEnabledFeeds();
		
		if(lstNewsfeeds.size()==0){
			updateNewsfeedHomeList();
			return;
		}
		
		//TODO: test only to verify individual feeds - remove this
		/**
		//Wiggle feeds with trailing spaces or characters that lead to parsing errors .. "<?xml" needs to be at very first position
		ObjNewsfeed testFeed1 = ObjNewsfeedManager.getInstance().getFeedByURL("http://guides.wiggle.co.uk/cycle-guides/rss.xml");
		ObjNewsfeed testFeed2 = ObjNewsfeedManager.getInstance().getFeedByURL("http://guides.wiggle.co.uk/swim-guides/rss.xml");
		ObjNewsfeed testFeed3 = ObjNewsfeedManager.getInstance().getFeedByURL("http://guides.wiggle.co.uk/triathlon-guides/rss.xml");
		ObjNewsfeed testFeed4 = ObjNewsfeedManager.getInstance().getFeedByURL("http://guides.wiggle.co.uk/events/rss.xml");
		ObjNewsfeed testFeed5 = ObjNewsfeedManager.getInstance().getFeedByURL("http://guides.wiggle.co.uk/run-guides/rss.xml");
		
		lstNewsfeeds.clear();
		lstNewsfeeds.add(testFeed1);
		lstNewsfeeds.add(testFeed2);
		lstNewsfeeds.add(testFeed3);
		lstNewsfeeds.add(testFeed4);
		lstNewsfeeds.add(testFeed5);
		**/

		
		lstQueriedNewsfeeds.addAll(lstNewsfeeds);
		mNewsfeedQueueCounter = lstQueriedNewsfeeds.size();
		
		/* 
		 * Approach 1: submit all requests at once. 
		 * This led to performance problems as the cache queue gets bombarded and 3-4 of the requests got very slow 
		 * giving the impression the entire thing is slow
		 *
		for(int i=0;i<lstNewsfeeds.size();i++){
			ObjNewsfeed singleFeed = lstNewsfeeds.get(i);
			submitNewsfeedRequest(singleFeed);
		}
		*/
		
		//Approach 2: submit a small number of parallel requests (configurable)
		//wait for response - and submit the next request once response received.
		submitNewsfeedRequesBatch();
		
	}
	
	private void submitNewsfeedRequesBatch(){
		
		int intMaxBatchSize = 10; //we send a maximum of n requests in parallel. The next batch is sent when all responses received
		
		//defend against index out of bounds
		if(intMaxBatchSize>lstQueriedNewsfeeds.size()){
			intMaxBatchSize = lstQueriedNewsfeeds.size();
		}
		
		//built list of feeds that we want to request (dont submit yet to avoid concurrency issues - even if unlikely)
		for(int i=0;i<intMaxBatchSize;i++){
			ObjNewsfeed singleFeed = lstQueriedNewsfeeds.get(i);
			lstCurrentRequestBatch.add(singleFeed);
		}
		
		//submit the requests for newsfeesd in our batch
		for(int i=0;i<lstCurrentRequestBatch.size();i++){
			ObjNewsfeed singleFeed = lstCurrentRequestBatch.get(i);
			submitNewsfeedRequest(singleFeed);
		}
		
		
	}
	
	private void submitNewsfeedRequest(ObjNewsfeed feed){
		
		new ObjApiRequest(this, 
				feed.getString(ObjNewsfeed.FIELD_URL), 
				AppRequestHandler.ENDPOINT_NEWSFEED_DUMMY, 
				Method.GET, 
				feed, 
				null, 
				ON_RESPONSE_ACTION.NO_ACTION)
		.submit();
		//.submitAfterHeadRequest(); //we check for Etags and last modified 
		
	}
	
	public void stopRefreshAnimation(){
		
		if(newsfeedHome_swipeRefreshLayout != null){
			
			if(newsfeedHomeAdapter.getCount()==0){
				newsfeedHome_tvNoData.setText(getString(R.string.newsfeedHome_tvNoData));
				newsfeedHome_tvNoData.setVisibility(View.VISIBLE);
				newsfeedHome_lvListView.setVisibility(View.GONE);
			}else{
				newsfeedHome_tvNoData.setVisibility(View.GONE);
				newsfeedHome_lvListView.setVisibility(View.VISIBLE);

		    	showPreferencesPopup();
			}
			
			// scroll to start if update listview was called through swipeRefresh
			if(newsfeedHome_swipeRefreshLayout.isRefreshing()){
				scrollToStart();
			}
			
			newsfeedHome_swipeRefreshLayout.setRefreshing(false);
			

		}
		
	}
	
	public void scrollToStart(){
		
		//do nothing if list not initialised (crash-fix: NPE)
		if(newsfeedHome_lvListView==null){
			return;
		}
		
		//needed to move this statment to a runnable as this does not work immediately after notifydatasetchanged
		//newsfeedHome_lvListView.setSelection(0);
		newsfeedHome_lvListView.post(new Runnable() {

	        @Override
	        public void run() {
	        	newsfeedHome_lvListView.setSelection(0);
	        }
	    });

		
		
		//newsfeedHome_lvListView.smoothScrollToPosition(0);
		
	}
	

	
	private void setupCategoryGallery(){
		
		categoryAdapter = new NewsfeedCategoryAdapter(this);
		newsfeedSettings_lvNewsfeedCategories.setAdapter(categoryAdapter);
		sportsAdapter = new NewsfeedSportsAdapter(this);
		newsfeedSettings_lvNewsfeedSports.setAdapter(sportsAdapter);
		updateCategoryList();
		
	}
	
	private void setupNewsfeedHomeListview(){
		
		newsfeedHomeAdapter = new NewsfeedHomeAdapter(this);
		newsfeedHome_lvListView.setAdapter(newsfeedHomeAdapter);
	}
	

	
	public void updateCategoryList(){
		
		if(categoryAdapter==null){
			return;
		}
		
		runOnUiThread(mNewsfeedSportsTask);
		runOnUiThread(mNewsfeedCategoryTask);
	}
	
	
	
	public void updateNewsfeedHomeList(){
		
		if(newsfeedHomeAdapter==null){
			return;
		}
		
		runOnUiThread(mNewsfeedHomeListUpdateTask);
	}
	


	private class NewsfeedCategoryTask implements Runnable {
		
		@Override
		public void run() {
			
			
			List<ObjNewsfeedCategory> categoryList = ObjNewsfeedCategoryManager.getInstance().getSortedCategories();
			categoryAdapter.setListContent(categoryList);
			categoryAdapter.notifyDataSetChanged();
			
			
			
		}
		
	}
	
	private class NewsfeedSportsTask implements Runnable {
		
		@Override
		public void run() {
			
			
			List<ObjNewsfeedCategory> sportsList = ObjNewsfeedSportsManager.getInstance().getAll();
			sportsAdapter.setListContent(sportsList);
			sportsAdapter.notifyDataSetChanged();
			
			
			
		}
		
	}
	

	private class NewsfeedHomeListUpdateTask implements Runnable {
		
		@Override
		public void run() {
			
			
			//List<String> userGallery = profileUser.getGallery();
			List<ObjNewsfeedRSSItem> newsfeedItemList = ObjNewsfeedManager.getInstance().getAllNews();
			
			//Sort newsfeed list by date descending - newest items on top
			SortByDate sortByDate = new SortByDate();
			Collections.sort(newsfeedItemList, sortByDate);
			
			newsfeedHomeAdapter.setListContent(newsfeedItemList);
			newsfeedHomeAdapter.notifyDataSetChanged();
			
			stopRefreshAnimation();
			
		}
		
	}
	
	
	class SortByDate implements Comparator<ObjNewsfeedRSSItem> {

		@Override
		public int compare(ObjNewsfeedRSSItem item1, ObjNewsfeedRSSItem item2) {

			//not the nicest solution to treat item creation dates as string - but as we convert to yyyy-mm-dd this will do and work
			Date item1PublishedDate = DateUtils.getDateFromRSSDate(item1.getPublishDate());
			Date item2PublishedDate = DateUtils.getDateFromRSSDate(item2.getPublishDate());

			//this sorts by date descending - newest items are on top of list
			return item2PublishedDate.compareTo(item1PublishedDate);
			
		}

	}

	
	private void retrieveNewsfeedConfiguration(){
		
		//this is the important one - downloading config-brands.json that defines which newsfeeds belong to which brand
		new ObjApiRequest(this, 
				null, 
				AppRequestHandler.URL_NEWSFEED_CONFIG_FILE, 
				Method.GET, 
				null,
				null, 
				ON_RESPONSE_ACTION.NO_ACTION)
		.submitAfterHeadRequest(); //config file: we only get this from network if network file has changed
	
		//this downloads a list of sports featured in all newsfeeds
		new ObjApiRequest(this, 
				null, 
				AppRequestHandler.URL_NEWSFEED_SPORTS_CONFIG_FILE, 
				Method.GET, 
				null,
				null, 
				ON_RESPONSE_ACTION.NO_ACTION)
		.submitAfterHeadRequest();
		
	}
	
	public void showRssDetail(ObjNewsfeedRSSItem newsItem){

		AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_news_list), "listReadMore");



		mActiveNewsItem = newsItem;
		
		
		String strAdditionalImageUrl = newsItem.getAdditionalImageUrl();
		if(strAdditionalImageUrl!=null && !"".equals(strAdditionalImageUrl)
				//skip this for Gore Apparel Atom feed - articles of this feed always contain images and paths dont match easily
				&& !"http://inspiration.goreapparel.com/feed/atom/".equals(newsItem.getParentChannel().getParentNewsfeed().getString(ObjNewsfeed.FIELD_URL))){
			
			//don't duplicate images in header and article.
			boolean blnImageAtRiskOfBeingDuplicated = newsItem.getContent().contains(strAdditionalImageUrl);
			if(blnImageAtRiskOfBeingDuplicated){
				newsfeedDetail_ivNewsImage.setVisibility(View.GONE);
			}else{
				//show image in header
				newsfeedDetail_ivNewsImage.setVisibility(View.VISIBLE);
				PicassoSSL.with(this).load(strAdditionalImageUrl).into(newsfeedDetail_ivNewsImage);
			}
			
			
		}else{
			newsfeedDetail_ivNewsImage.setVisibility(View.GONE);
		}
		
		
		ObjNewsfeed parentFeed = newsItem.getParentChannel().getParentNewsfeed();
		String strBrandImageUrl = parentFeed.getString(ObjNewsfeed.FIELD_IMAGE_URL);
		PicassoSSL.with(this).load(strBrandImageUrl).into(newsfeedDetail_ivBrandImage);
		
		String strTitle = newsItem.getString(ObjNewsfeedRSSItem.FIELD_TITLE);
		Spanned htmlTitle = Html.fromHtml(strTitle);
		newsfeedDetail_tvTitle.setText(htmlTitle);
		
		String strGUIDate = DateUtils.getTimeAgoFromRSSDate(newsItem.getPublishDate());
		newsfeedDetail_tvDate.setText(strGUIDate);
		
		
		String strHtmlContent = newsItem.getContent();
		//base URL needs to be set so that images get displayed properly
		String strBaseUrl = newsItem.getParentChannel().getParentNewsfeed().getBaseUrl();
		//insert style tags into Html: this also makes sure that images are displayed in the correct size and fit the screen. Yey!
		//You can change the font-size here for easier readability - just change the font-size: nnpx tag to whatever you want. 
		int intWebViewFontSizeInWebPx = 15;
		String strGothamBookFontPath = "file:///android_asset/fonts/roboto-ttf/Roboto-Regular.ttf";
		
		String strStyle = "<style type='text/css'>img {max-width: 100%; height: auto;} @font-face {font-family: 'customAppFont'; src: url('" + strGothamBookFontPath + "');} body {font-family: 'customAppFont'; font-size: " + String.valueOf(intWebViewFontSizeInWebPx) + "px; margin: 0; padding: 0;} a {color: #F1951F; text-decoration: none;} iframe {max-width: 100%;}</style>";
	    if(strHtmlContent!=null){
	    	if(strHtmlContent.contains("<body>")){
	    		strHtmlContent.replace("<body>", "\n" + strStyle + "\n<body>");
	    	}else{
	    		strHtmlContent = strStyle + "\n" + strHtmlContent;
	    	}
	    }
		

		  
		//as we re-use this webview: clear content first:
	    //newsfeedDetail_webView.loadUrl("about:blank"); //Done when detail screen is closed. This helps to stop Youtube videos etc to continue playing too
		newsfeedDetail_webView.loadDataWithBaseURL(strBaseUrl, strHtmlContent, "text/html", "utf-8", "");
		newsfeedDetail_svScrollView.scrollTo(0, 0);
		

		displayLayout(R.id.newsfeedDetail);
		
		
		
	}
	
	public void shareNews(ObjNewsfeedRSSItem newsItem){

		//can be used for forwarding via email / skype etc later ... 
       	Intent externalAppIntent = new Intent(Intent.ACTION_SEND);
       	//externalAppIntent.setType("text/html"); // in case of HTML text - not really needed
       	externalAppIntent.setType("text/plain"); //this sends data to any other app that accepts text input (Email, SMS, as long as these apps registered the mime type in their manifest file ... )
       	//externalAppIntent.setType("*/*"); //this brings up any app - should be avoided
       	//De-activated EXTRA_SUBJECT as this sometimes gets concatenated with EXTRA_TEXT (e.g. in standard Android SMS app) which makes the share text look odd. A user will need to type in his own email subject.
       	//externalAppIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.itemDetail_shareAd_EmailSubject) + " " + mItem.getString(ObjResort.FIELD_NAME)));
       	String strShareText = String.format(getString(R.string.newsfeed_tvShare_text),
       						  newsItem.getString(ObjNewsfeedRSSItem.FIELD_LINK)
       						  );
       	externalAppIntent.putExtra(android.content.Intent.EXTRA_TEXT, strShareText);
       	startActivity(Intent.createChooser(externalAppIntent,getString(R.string.newsfeed_tvShare_title)));
       	
       	
	}
	
	

	//Convenience methods
	public void setNewsTopicSubscribedOnOff(ObjNewsfeedCategory category, View foregroundView, ImageView starImageView){
		
		//Check if user subscribed to this topic
		if(ObjAppUser.instance().isSubscribedToNewsTopic(category)){
			
			ObjAppUser.instance().unsubscribeFromTopic(category);
			starImageView.setImageDrawable(getResources().getDrawable(R.drawable.sd_news_topic_activated));
			AppUtils.setSDKDependentBackground(foregroundView, R.drawable.sd_button_clear_to_opaque);	
			//foregroundView.setSelected(false);
			//starImageView.setSelected(false);
		}else{
			
			ObjAppUser.instance().subscribeToTopic(category);
			starImageView.setImageDrawable(getResources().getDrawable(R.drawable.sd_news_topic_deactivated));
			AppUtils.setSDKDependentBackground(foregroundView, R.drawable.sd_button_opaque_to_clear);	
			//foregroundView.setSelected(true);
			//starImageView.setSelected(true);
		}
		
		updateCategoryList();
		
		mBlnSettingsChanged = true;
		
		
	}
	
	private void openLinkInExternalBrowser(ObjNewsfeedRSSItem newsItem){

		AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_news_detail), "detailExternalLink");
		
		Bundle bundle = new Bundle();
		
		/* You can add additional header information as follows - but only do this if it can't be achieved in webview.setSettings()  */
		//bundle.putString(key, value);

		String strTargetUrl = newsItem.getString(ObjNewsfeedRSSItem.FIELD_LINK);
		Intent externalBrowserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(strTargetUrl));
		externalBrowserIntent.putExtra(Browser.EXTRA_HEADERS, bundle);
		startActivity(externalBrowserIntent);
		
		
	}
	
	private void openLinkInWiggleHybridActivity(String strTargetUrl){

		//not really external news .. but don't want to loose that click
		AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_news_detail), "detailExternalLink");

		Intent intent = new Intent(this, HybridActivity.class);
		//intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION); //this makes sure activities do not slide in from right to left when started, but simply appear
		intent.putExtra(HybridActivity.WEB_VIEW_CALLER, newsfeedDetail_webView.getId());
		intent.putExtra(HybridActivity.LOAD_TARGET_URL_FROM_NATIVE_SCREEN, strTargetUrl);
		startActivityForResult(intent, 0);
		
		
		
	}
	
	private void showPreferencesPopup(){
		
		//only show this popup once .. if user already saw it - nothing more to do here.
		boolean blnUserSawPreferencesHint = ObjAppUser.instance().getBoolean(ObjAppUser.FIELD_NEWS_PREFERENCES_HINT_ALREADY_SEEN);
		if(blnUserSawPreferencesHint){
			newsfeedHome_lytNewsfeedHint.setVisibility(View.GONE);
			return;
		}
		
		//wait 1 second, notify progress (onTick) in 100 ms intervals
		new CountDownTimer(1000, 100) {

		     public void onTick(long millisUntilFinished) {
		         
		     }

		     public void onFinish() {
		    	 AppAnimations.getInstance().startFadeInAnimationWithDurationInMsAndStartOpacity(300, 0 ,newsfeedHome_lytNewsfeedHint);
		    	 //log that we have displayed this to the user
		    	 ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_NEWS_PREFERENCES_HINT_ALREADY_SEEN, "1");
		     }
		  }.start();
		  
	}
	
	private void hidePreferencesPopup(){
		
		//fade out - and set view to Gone after animation so it can no longer be pressed.
		AppAnimations.getInstance().startFadeOutAnimationWithDurationInMs(300, newsfeedHome_lytNewsfeedHint, true);
		
	}
	

	
}
