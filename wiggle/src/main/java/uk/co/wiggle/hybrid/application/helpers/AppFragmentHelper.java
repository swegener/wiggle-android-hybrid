package uk.co.wiggle.hybrid.application.helpers;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.view.View;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.activities.TabbedHybridActivity;
import uk.co.wiggle.hybrid.usecase.shop.version3.activities.V3HybridActivity;


/**
 *
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 *
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 *
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/
public class AppFragmentHelper {

    public enum FRAGMENT_TRANSACTION {
        REMOVE_ADD,
        ADD_OR_REPLACE,
        REPLACE,
        REMOVE,
        POP,
        ADD,
        HIDE
    }

    public static void handleFragment(
            Context context
            , FragmentManager fm
            , View vFragmentContainerView
            , Fragment fragment
            , String strUserDefinedFragmentTag //can be NULL - if NULL, then fragment's class name is used as tag. Only important for ADD or REPLACE actions and useful for multiple use of the same Fragment Class
            , FRAGMENT_TRANSACTION transactionType
            , int intAnimation
            , boolean blnCommitAllowingStateLoss) {

        FragmentTransaction transaction = fm.beginTransaction();
        //transaction.setCustomAnimations(intAnimation, 0);
        //setCustomAnimations (int enter, int exit, int popEnter, int popExit)
        //fixed out-animation when popping back-stack.
        //TODO: make this generic to match the above enter/exit animation during normal fragment handling and when popping back stack
        transaction.setCustomAnimations(intAnimation, 0, 0, R.anim.activity_right_slide_out);

        //use user defined tag or default tag for this fragment
        String strFragmentTag = strUserDefinedFragmentTag;
        if(strFragmentTag==null){
            strFragmentTag = fragment.getClass().getName(); //do not use getSimpleName() as this can cause problems after code obfuscation when using Proguard (best always supply a user defined Fragment tag - dont use null!
        }

        if (FRAGMENT_TRANSACTION.ADD.equals(transactionType)) {

            /*
                Right - after lots of experimenting the following works:

                If you want to add fragments to a back stack the user can navigate through nicely in reverse order when pressing the
                back button, then use transaction.add()

                The trick is how to navigate back to a particular fragment that is hidden deep in the back stack. Now
                this can be done by using popBackStackImmediate(fragmentTag, 0) [thanks to http://stackoverflow.com/a/28180369]

                Our dashboard fragment is the home screen. Pressing back on this leaves the app. We thus check in onBackPressed()
                if the fragment currently displayed is the home fragment - if yes, we show an exit dialog.

                So things like this will work:

                1 > 2 > 3 > 4 > 5   Back > 4   Back > 3   Back > 2   Back  > 1

                But also the following

                1 > 3 > 5 > 3 > 5   Back > 3   Back > 1 (! note we dont repeat the duplicate back stack  (5 > 3 > 5 > 3 > 1)
                , but we now have a nice navigation for the user. Yes! :-)

                Oh .. and we have one activity with a shared background. Some fragments are transparent. Adding them on top
                of each other would look odd as you see content of multiple fragments. Hence we hide any fragment that goes
                into the back stack. We show it once it gets added or re-ordered to the front.


             */

            //as some of our fragments have transparent background to work with app background, hide the fragment
            //displayed at the moment before adding the new one.
            //This is the fragment that is displayed before handling the new fragment that just is coming in!
            //Note: disabled for Wiggle app
            Fragment currentFragment = fm.findFragmentById(vFragmentContainerView.getId());
            if (currentFragment != null) {
                //Exception - tabbed navigation: hide fragment that goes into backstack on HybridActivity
                //we are using show / hide there instead of add/replace/remove to keep the state of fragments such as nav browser and webview
                if(context instanceof TabbedHybridActivity
                    || context instanceof V3HybridActivity) {
                    //transaction.hide(currentFragment);
                }
            }

            //add fragment
            transaction.add(vFragmentContainerView.getId(), fragment, strFragmentTag); //3rd parameter tags fragment with a name (here - simple class name)
            transaction.addToBackStack(strFragmentTag);
            //transaction.commit();
            transaction.commitAllowingStateLoss(); //prevent IllegalStateExceptions

            /*
            Fragment testFragment = fm.findFragmentByTag(strFragmentTag);

            if (testFragment != null) {
                //fragment in back stack? Simply return to it ..
                fm.popBackStackImmediate(strFragmentTag, 0); //undo latest transaction on this fragment (disabled for Wiggle app)
                transaction.show(fragment); //if it is in the background we would have hidden it - so show it now
                transaction.commit();

            } else {

                //fragment does not exist - add it to front and to back stack for later use
                transaction.add(vFragmentContainerView.getId(), fragment, strFragmentTag); //3rd parameter tags fragment with a name (here - simple class name)
                transaction.addToBackStack(strFragmentTag);
                //transaction.commit();
                transaction.commitAllowingStateLoss(); //prevent IllegalStateExceptions

            }

             */

            if (vFragmentContainerView.getVisibility() == View.GONE) {
                vFragmentContainerView.setVisibility(View.VISIBLE);
            }

        }else if (FRAGMENT_TRANSACTION.REMOVE.equals(transactionType)) {

            transaction.remove(fragment);
            if (blnCommitAllowingStateLoss) {
                transaction.commitAllowingStateLoss();
            } else {
                transaction.commit();
            }

            //Thank you, http://stackoverflow.com/questions/9033019/removing-a-fragment-from-the-back-stack
            //You add fragments to the back stack using FragmentTransaction only
            //You remove fragments using a transaction followed by fragmentManager.pop methods
            fm.popBackStack();

            if (fm.getBackStackEntryCount() == 0) {
                vFragmentContainerView.setVisibility(View.GONE);
            }

        } else if (FRAGMENT_TRANSACTION.POP.equals(transactionType)) {

            fm.popBackStackImmediate(strFragmentTag, 0);

            if (fm.getBackStackEntryCount() == 0) {
                vFragmentContainerView.setVisibility(View.GONE);
            }

        } else if (FRAGMENT_TRANSACTION.REPLACE.equals(transactionType)) {

            //note: when replacing fragments there is no back navigation to the previous one as it gets removed.

            Fragment testFragment = fm.findFragmentByTag(strFragmentTag);

            if(testFragment!=null){
                //fragment in back stack? Simply return to it ..
                fm.popBackStackImmediate(strFragmentTag, 0);

            }else{

                //fragment not in back stack - add it (use replace, this does remove() followed by add()
                transaction.replace(vFragmentContainerView.getId(), fragment, strFragmentTag); //3rd parameter tags fragment with a name (here - simple class name)
                transaction.addToBackStack(strFragmentTag);
                transaction.commit();

            }


            if (vFragmentContainerView.getVisibility() == View.GONE) {
                vFragmentContainerView.setVisibility(View.VISIBLE);
            }

        }else if (FRAGMENT_TRANSACTION.HIDE.equals(transactionType)) {

            transaction.hide(fragment);
            transaction.commit();

        }

        //mop-up after transactions - helps to resolve inconsistencies (e.g. existing fragments cant be found by tag in the back stack)
        //see https://stackoverflow.com/questions/26988588/findfragmentbytag-always-return-null-android
        fm.executePendingTransactions();

    }

    public static String getFragmentBreadcrumbTitle(Fragment fragment){

        String strBreadcrumb = "";

        //No fragment? No title ...
        if(fragment==null){
            return strBreadcrumb;
        }

        //get title passed to fragment when it was created - default to "empty title" if parameter is missing
        Bundle fragmentArguments = fragment.getArguments();
        if(fragmentArguments!=null){
            strBreadcrumb = fragmentArguments.getString(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE, "");
        }

        return strBreadcrumb;

    }

}
