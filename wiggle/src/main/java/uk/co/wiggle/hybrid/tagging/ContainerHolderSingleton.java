package uk.co.wiggle.hybrid.tagging;

import com.google.android.gms.tagmanager.ContainerHolder;


/**
 *
 * AndroMedia: This class is as provided by Google. Don't adjust or remove as long as Google Tag Manager is used.
 *
 * Singleton to hold the GTM Container (since it should be only created once
 * per run of the app).
 */
public class ContainerHolderSingleton {
    private static ContainerHolder containerHolder;

    /**
     * Utility class; don't instantiate.
     */
    private ContainerHolderSingleton() {
    }

    public static ContainerHolder getContainerHolder() {
        return containerHolder;
    }

    public static void setContainerHolder(ContainerHolder c) {
        containerHolder = c;
    }
}
