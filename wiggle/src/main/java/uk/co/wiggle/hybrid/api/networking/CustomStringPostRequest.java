package uk.co.wiggle.hybrid.api.networking;

import com.android.volley.AuthFailureError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.Map;

import uk.co.wiggle.hybrid.application.logging.Logger;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 * @doc      This request was created to allow submitting a POST request without using Json,
 *           but URL encoded (x-www-form-urlencoded) post parameters. This will result in a request looking like this:
 *
 *           POST http//www.myapi.com/login HTTP/1.1
 *           Authorization: Basic ...
 *
 *           email=android%40google.com&password=12345
 *
 *
 **/

public class CustomStringPostRequest extends StringRequest {

	private String logTag = "CustomStringPostRequest";

	private Map<String, String> headerParams;
	private Listener<String> listener;
	private Map<String, String> params;
	private Priority mPriority = Priority.IMMEDIATE; // all volley requests need
														// to be executed
														// immediately - if you
														// want to change this
														// for a given request
														// simply call
														// DubizzleJsonObjectRequest.setPriority(Priority.xxx);

	public CustomStringPostRequest(int method, String url,
								   Map<String, String> params,
								   Map<String, String> headerParams,
								   Listener<String> listener,
								   ErrorListener errorListener) {
		super(method, url, listener, errorListener);
		// TODO Auto-generated constructor stub
		this.listener = listener;
		this.headerParams = headerParams;
		this.params = params;
		Logger.printMessage(logTag, "Request Parameters: " + prettyPrintParams(params), Logger.INFO);
	}

	@Override
	protected Map<String, String> getParams() throws AuthFailureError {
		Logger.printMessage(logTag, "Request Parameters: " + prettyPrintParams(params), Logger.INFO);
		return params;
	};

	@Override
	public Map<String, String> getHeaders() throws AuthFailureError {
		Logger.printMessage(logTag, "Request Parameters: " + prettyPrintParams(headerParams), Logger.INFO);
		return headerParams;
	}

	@Override
    public String getBodyContentType() {
        return "application/x-www-form-urlencoded";
    }

	@Override
	protected void deliverResponse(String string) {
		listener.onResponse(string);
	}

	@Override
	protected VolleyError parseNetworkError(VolleyError volleyError) {
		// TODO Auto-generated method stub
		if (volleyError.networkResponse != null
				&& volleyError.networkResponse.data != null) {

			VolleyError error = new VolleyError(new String(
					volleyError.networkResponse.data));
			volleyError = error;

		}

		return volleyError;
	}

	@Override
	public Priority getPriority() {
		return mPriority;
	}

	public void setPriority(Priority priority) {
		mPriority = priority;
	}

	private String prettyPrintParams(Map<String, String> paramsMap){

		String strParams = "";
		for (Map.Entry<String, String> entry : paramsMap.entrySet()) {
			strParams = strParams + "|" + entry;
		}

		return strParams;

	}

}
