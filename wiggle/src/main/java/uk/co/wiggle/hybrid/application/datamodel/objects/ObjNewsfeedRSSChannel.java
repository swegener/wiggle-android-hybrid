package uk.co.wiggle.hybrid.application.datamodel.objects;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;


import android.os.Bundle;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class ObjNewsfeedRSSChannel extends CustomObject {

	public static final String OBJECT_NAME = "channel";

	private List<ObjNewsfeedRSSItem> mItems = new ArrayList<ObjNewsfeedRSSItem>();
	private ObjNewsfeed mParentNewsfeed = new ObjNewsfeed();
	
	//fields specific to a newsfeed channel XML structure
	//Part I: for RSS Channels
	public static final String FIELD_TITLE = "title";
	public static final String FIELD_LINK = "link";
	public static final String FIELD_DESCRIPTION = "description";
	public static final String FIELD_LANGUAGE = "language";
	public static final String FIELD_ATOM_LINK = "link"; //not "atom:link" as in XML document, but "link" only is returned from XML parser
	public static final String FIELD_PUBDATE = "pubDate"; 
	public static final String FIELD_PUBDATE_LOWERCASE = "pubdate"; 
	public static final String FIELD_LAST_BUILD_DATE = "lastBuildDate"; 
	
	//Part II - additional fields specific to Atom Feeds
	public static final String FIELD_SUBTITLE = "subtitle";
	public static final String FIELD_UPDATED = "updated";
	public static final String FIELD_ID = "id";
	
	
	private static List<String> sFieldList;
	static {

		sFieldList = Collections.synchronizedList(new ArrayList<String>());
		
		//Part I: for RSS Channels
		sFieldList.add(FIELD_TITLE);
		sFieldList.add(FIELD_LINK);
		sFieldList.add(FIELD_DESCRIPTION);
		sFieldList.add(FIELD_LANGUAGE);
		sFieldList.add(FIELD_ATOM_LINK);
		sFieldList.add(FIELD_PUBDATE);
		sFieldList.add(FIELD_PUBDATE_LOWERCASE);
		sFieldList.add(FIELD_LAST_BUILD_DATE);
		
		//Part II - additional fields specific to Atom Feeds
		sFieldList.add(FIELD_SUBTITLE);
		sFieldList.add(FIELD_UPDATED);
		sFieldList.add(FIELD_ID);
		
		
	}

	private static ConcurrentHashMap<String, Integer> sFieldTypes;
	static {
		sFieldTypes = new ConcurrentHashMap<String, Integer>();
		
		//Part I: for RSS Channels
		sFieldTypes.put(FIELD_TITLE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_LINK, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_DESCRIPTION, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_LANGUAGE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_ATOM_LINK, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PUBDATE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PUBDATE_LOWERCASE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_LAST_BUILD_DATE, FIELD_TYPE_STRING);
		
		//Part II - additional fields specific to Atom Feeds
		sFieldTypes.put(FIELD_SUBTITLE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_UPDATED, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_ID, FIELD_TYPE_STRING);
				
		
	}

	public ObjNewsfeedRSSChannel() {
	}

	public ObjNewsfeedRSSChannel(Bundle bundle) {
		super(bundle);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ObjNewsfeedRSSChannel)) {
			return false;
		}

		ObjNewsfeedRSSChannel other = (ObjNewsfeedRSSChannel) o;
		String otherFeedLink = other.getString(FIELD_LINK);
		String thisFeedLink = this.getString(FIELD_LINK);
		return otherFeedLink.equals(thisFeedLink);
	}

	@Override
	public String getObjectName() {
		return OBJECT_NAME;
	}

	@Override
	public List<String> getFields() {
		return sFieldList;
	}

	@Override
	public Class<?> getSubclass() {
		return ObjNewsfeedRSSChannel.class;
	}

	@Override
	public Integer getFieldType(String fieldName) {
		return sFieldTypes.get(fieldName);
	}

	// handler for children of this object
	public List<ObjNewsfeedRSSItem> getRssItems() {
		return mItems;
	}
	
	public boolean setRssItems(Collection<? extends ObjNewsfeedRSSItem> arg0) {
		mItems.clear();
		return mItems.addAll(arg0);
	}
	
	public void addRssItem(ObjNewsfeedRSSItem rssItem){
		if(!mItems.contains(rssItem)){
			mItems.add(rssItem);
		}
	}
	
	public void setParentNewsfeed(ObjNewsfeed parent){
		mParentNewsfeed = parent;
	}
	
	public ObjNewsfeed getParentNewsfeed(){
		return mParentNewsfeed;
	}
	
	/* Other convenience methods */
	
}