package uk.co.wiggle.hybrid.usecase.ordertracking.adapters;

import java.util.ArrayList;
import java.util.List;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.usecase.shop.classicnav.activities.HybridActivity;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesItem;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesOrder;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.extensions.PicassoSSL;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.activities.TabbedHybridActivity;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.fragments.FragmentOrderTracking;


import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class OrderTrackingListAdapter extends BaseAdapter {

	private String logTag = "OrderTrackingListAdapter";

	private List<ObjSalesItem> mOrderedItems = new ArrayList<ObjSalesItem>(); 

	private HybridActivity mContext;

	public OrderTrackingListAdapter() {

	}

	public OrderTrackingListAdapter(HybridActivity context) {
		mContext = context;
	}

	public void setListContent(List<ObjSalesItem> listItems) {
		mOrderedItems = listItems;
	}

	@Override
	public int getCount() {
		
		if (mOrderedItems == null){
			return 0;
			
		}else{
			return mOrderedItems.size();
		}
		
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	private class ViewHolder {
		
		LinearLayout orderTrackingListRow;
		ImageView orderList_ivProductImage;
		TextView orderList_tvOrderUpdated;
		TextView orderList_tvTitle;
		TextView orderList_tvOrderStatus;
		TextView orderList_tvDeliveryStatusText;
		TextView orderList_tvDeliveryStatusDate;
		
	}		

	@Override
	public View getView(int arg0, View view, ViewGroup arg2) {
		final int position = arg0;

		final ObjSalesItem salesItem = (ObjSalesItem) mOrderedItems.get(arg0);

		Logger.printMessage(logTag, "ITEM: " + salesItem.toXML(), Logger.INFO);

		final ViewHolder holder;
			
		if(view==null){
			
			LayoutInflater inflater = LayoutInflater.from(ApplicationContextProvider.getContext());
			view = inflater.inflate(R.layout.order_tracking_list_row, null);
			
			holder = new ViewHolder();
			
			holder.orderTrackingListRow = (LinearLayout) view.findViewById(R.id.orderTrackingListRow);
			
			holder.orderList_tvTitle = (TextView) view.findViewById(R.id.orderList_tvTitle);
			holder.orderList_tvTitle.setTypeface(AppTypefaces.instance().fontMainBold);
			
			holder.orderList_tvOrderStatus = (TextView) view.findViewById(R.id.orderList_tvOrderStatus);
			holder.orderList_tvOrderStatus.setTypeface(AppTypefaces.instance().fontMainBold);
			
			
			holder.orderList_tvDeliveryStatusDate = (TextView) view.findViewById(R.id.orderList_tvDeliveryStatusDate);
			holder.orderList_tvDeliveryStatusDate.setTypeface(AppTypefaces.instance().fontMainBold);
			
			holder.orderList_tvDeliveryStatusText = (TextView) view.findViewById(R.id.orderList_tvDeliveryStatusText);
			holder.orderList_tvDeliveryStatusText.setTypeface(AppTypefaces.instance().fontMainBold);
			

			holder.orderList_ivProductImage = (ImageView) view.findViewById(R.id.orderList_ivProductImage);
			
			holder.orderList_tvOrderUpdated = (TextView) view.findViewById(R.id.orderList_tvOrderUpdated);
			holder.orderList_tvOrderUpdated.setTypeface(AppTypefaces.instance().fontMainBold);

			view.setTag(holder);
			
		}else{
			holder = (ViewHolder)view.getTag();		
		}

		//TODO: adjust
		final ObjSalesOrder parentOrder = salesItem.getParentRecord();
		
		String strProductName = salesItem.getProductBrandAndName();
		holder.orderList_tvTitle.setText(strProductName);
		
		String strOrderStatus = parentOrder.getDisplayOrderStatus();
		holder.orderList_tvOrderStatus.setText(strOrderStatus);
		
		String strDeliveryStatusText = parentOrder.getDisplayDeliveryStatus();
		holder.orderList_tvDeliveryStatusText.setText(strDeliveryStatusText);
		
		/* show grey background for delivered orders. 
		if(ApplicationContextProvider.getContext().getString(R.string.str_order_status_delivered).equals(strDeliveryStatusText)){
			holder.orderTrackingListRow.setSelected(true);
		}else{
			holder.orderTrackingListRow.setSelected(false);
		}
		*/
		
		String strDeliveryStatusDate = parentOrder.getDisplayDeliveryDate();
		holder.orderList_tvDeliveryStatusDate.setText(strDeliveryStatusDate);
		
		String strProductImageUrl = salesItem.getImagePath("?w=200&amp;h=200&amp;a=0");
		if(strProductImageUrl!=null && !"".equals(strProductImageUrl)){
			holder.orderList_ivProductImage.setVisibility(View.VISIBLE);
			PicassoSSL.with(ApplicationContextProvider.getContext()).load(strProductImageUrl).into(holder.orderList_ivProductImage);
		}else{
			holder.orderList_ivProductImage.setVisibility(View.GONE);
		}
		
		boolean blnOrderUpdated = parentOrder.hasNewInformation();
		if(blnOrderUpdated){
			holder.orderList_tvOrderUpdated.setVisibility(View.VISIBLE);
		}else{
			holder.orderList_tvOrderUpdated.setVisibility(View.INVISIBLE);
		}
		
		
		LinearLayout lytGroupHeader = (LinearLayout) view.findViewById(R.id.orderList_lytGroupHeader);
		TextView orderList_tvGroupHeader = (TextView) view.findViewById(R.id.orderList_tvGroupHeader);
		orderList_tvGroupHeader.setTypeface(AppTypefaces.instance().fontMainRegular);
		
		String strOrderIdGroup = salesItem.getOrderListHeaderString();

		if (strOrderIdGroup != null && !"".equals(strOrderIdGroup)) {
			orderList_tvGroupHeader.setText(strOrderIdGroup);
			if(AppUtils.showListviewHeaders()){
				lytGroupHeader.setVisibility(View.VISIBLE);
			}
		} else {
			lytGroupHeader.setVisibility(View.GONE);
		}
		
		
		view.setOnClickListener(null);
		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {

				if(AppUtils.getWiggleVersion().getVersionCode()>AppUtils.APP_VERSION.VERSION_1_HYBRID.getVersionCode()){
					FragmentOrderTracking.instance().onOrderedItemClick(salesItem);
				}else {
					HybridActivity.instance().onOrderedItemClick(salesItem);
				}
				
			}

		});

		return view;

	}

	@Override
	public Object getItem(int arg0) {
		return mOrderedItems.get(arg0);
	}
	
}
