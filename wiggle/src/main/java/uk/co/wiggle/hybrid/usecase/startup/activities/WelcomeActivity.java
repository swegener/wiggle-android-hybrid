package uk.co.wiggle.hybrid.usecase.startup.activities;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.application.animations.AppAnimations;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjApiRequest;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.session.AppSession;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.interfaces.IAppActivityUI;
import uk.co.wiggle.hybrid.interfaces.IAppApiResponse;
import uk.co.wiggle.hybrid.tagging.AppTagManager;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.activities.Alert;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.activities.TabbedHybridActivity;
import uk.co.wiggle.hybrid.usecase.shop.version3.activities.V3HybridActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.view.*;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;

import org.json.JSONObject;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class WelcomeActivity extends Activity implements View.OnClickListener, IAppActivityUI, IAppApiResponse {
	
	private static final String logTag = "WelcomeActivity";
	
    //allow other activities to read an instance of MainActivity so that MainActivity methods can be called
	private static WelcomeActivity myInstance;

	public static WelcomeActivity instance(){
		return myInstance;
	}
	
	//Widgets
	LinearLayout lytWelcomeActivity;
	TextView welcome_tvWelcome;
	LinearLayout lytWelcomeOptions;
	ScrollView lytCountrySelector;
	ImageView welcome_ivCountry;
	TextView welcome_tvSelectStore;
	TextView welcome_tvChangeLocation;
	TextView welcome_tvContinue;
	TextView welcome_tvCountryUK;
	TextView welcome_tvCountryFrance;
	TextView welcome_tvCountryAustralia;
	TextView welcome_tvCountryNewZealand;
	TextView welcome_tvCountryUS;
	TextView welcome_tvCountryES;
	TextView welcome_tvCountryIT;
	TextView welcome_tvCountryCN;
	TextView welcome_tvCountryJP;
	TextView welcome_tvCountryDE;
	TextView welcome_tvCountrySE;
	TextView welcome_tvCountryNL;
	TextView welcome_tvAppRedirectActive;

	Handler handler = new Handler();
	
	
   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        
        setContentView(R.layout.welcome_activity);
        
        myInstance = this;

	    findViews();
	    //show splash screen longer - hide activity layout first, then show this after a split-second
	    lytWelcomeActivity.setVisibility(View.INVISIBLE);

	    //start handler that will wait for n amount of time so that the branding is fully visible ..
	    loadMainScreenWithDelay();
	    //Moved to V3HybridActivity: checkForRequiredAppUpgrade();


   }
	   
   
    @Override
	public void setupCustomActionBar() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCustomFontTypes() {
		
		welcome_tvWelcome.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		welcome_tvSelectStore.setTypeface(AppTypefaces.instance().fontMainRegular);
		welcome_tvChangeLocation.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		welcome_tvContinue.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		welcome_tvCountryUK.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		welcome_tvCountryFrance.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		welcome_tvCountryAustralia.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		welcome_tvCountryNewZealand.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		welcome_tvCountryUS.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		welcome_tvCountryES.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		welcome_tvCountryIT.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		welcome_tvCountryCN.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		welcome_tvCountryJP.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		welcome_tvCountryDE.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		welcome_tvCountrySE.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		welcome_tvCountryNL.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		
	}

	@Override
	public void findViews() {

		welcome_tvAppRedirectActive = (TextView) findViewById(R.id.welcome_tvAppRedirectActive);
		if(!"".equals(AppUtils.REDIRECT_APP_TO_TEST_SERVER)){
			welcome_tvAppRedirectActive.setText(AppUtils.REDIRECT_APP_TO_TEST_SERVER);
			welcome_tvAppRedirectActive.setTypeface(AppTypefaces.instance().fontMainRegular);
			welcome_tvAppRedirectActive.setVisibility(View.VISIBLE);
		}else{
			welcome_tvAppRedirectActive.setVisibility(View.GONE);
		}

		lytWelcomeActivity = (LinearLayout) findViewById(R.id.welcomeActivity);
		welcome_tvWelcome = (TextView) findViewById(R.id.welcome_tvWelcome);
		lytWelcomeOptions = (LinearLayout) findViewById(R.id.lytWelcomeOptions);
		lytCountrySelector = (ScrollView) findViewById(R.id.lytCountrySelector);
		welcome_ivCountry = (ImageView) findViewById(R.id.welcome_ivCountry);
		welcome_tvSelectStore = (TextView) findViewById(R.id.welcome_tvSelectStore);
		welcome_tvChangeLocation = (TextView) findViewById(R.id.welcome_tvChangeLocation);
		welcome_tvContinue = (TextView) findViewById(R.id.welcome_tvContinue);
		welcome_tvCountryUK = (TextView) findViewById(R.id.welcome_tvCountryUK);
		welcome_tvCountryFrance = (TextView) findViewById(R.id.welcome_tvCountryFrance);
		welcome_tvCountryAustralia = (TextView) findViewById(R.id.welcome_tvCountryAustralia);
		welcome_tvCountryNewZealand = (TextView) findViewById(R.id.welcome_tvCountryNewZealand);
		welcome_tvCountryUS = (TextView) findViewById(R.id.welcome_tvCountryUS);
		welcome_tvCountryES = (TextView) findViewById(R.id.welcome_tvCountryES);
		welcome_tvCountryIT = (TextView) findViewById(R.id.welcome_tvCountryIT);
		welcome_tvCountryCN = (TextView) findViewById(R.id.welcome_tvCountryCN);
		welcome_tvCountryJP = (TextView) findViewById(R.id.welcome_tvCountryJP);
		welcome_tvCountryDE = (TextView) findViewById(R.id.welcome_tvCountryDE);
		welcome_tvCountrySE = (TextView) findViewById(R.id.welcome_tvCountrySE);
		welcome_tvCountryNL = (TextView) findViewById(R.id.welcome_tvCountryNL);
		
		//standard methods to style textviews
		setCustomFontTypes();
		

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		int id = v.getId();
		
		switch (id) {
		
			case R.id.welcome_lytChangeLocation:
				AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_welcome), "changeLocation");
				displayLayout(R.id.lytCountrySelector);
				break;
				
			case R.id.welcome_lytContinue:
				AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_welcome), "welcomeContinue");
				AppSession.instance().setAllowBackNavigationToWelcomeScreen(true);
				if(AppUtils.getWiggleVersion().getVersionCode()>AppUtils.APP_VERSION.VERSION_1_HYBRID.getVersionCode()){
					launchHybridActivity(0);
				}else {
					launchStartupActivity();
				}
				break;
				
			case R.id.welcome_lytCountryUK:
			case R.id.welcome_lytCountryFrance:
			case R.id.welcome_lytCountryUS:
			case R.id.welcome_lytCountryAustralia:
			case R.id.welcome_lytCountryNewZealand:
			case R.id.welcome_lytCountryES:
			case R.id.welcome_lytCountryIT:
			case R.id.welcome_lytCountryCN:
			case R.id.welcome_lytCountryJP:
			case R.id.welcome_lytCountryDE:
			case R.id.welcome_lytCountrySE:
			case R.id.welcome_lytCountryNL:
				
				setSelectedStore(v);
				break;
			
		default:
			break;
			
		}
		
	}
	
	
	private void launchHybridActivity(int intViewId){
		
		if(AppUtils.getWiggleVersion().equals(AppUtils.APP_VERSION.VERSION_2_TABBED_NAV)){

			Intent intent = new Intent(this, TabbedHybridActivity.class);
			//intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION); //this makes sure activities do not slide in from right to left when started, but simply appear
			intent.putExtra(TabbedHybridActivity.WEB_VIEW_CALLER, intViewId);
			startActivityForResult(intent, 0);

		}else if(AppUtils.getWiggleVersion().equals(AppUtils.APP_VERSION.VERSION_3_APP_WIDE_SEARCH)){

			Intent intent = new Intent(this, V3HybridActivity.class);
			//intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION); //this makes sure activities do not slide in from right to left when started, but simply appear
			intent.putExtra(V3HybridActivity.WEB_VIEW_CALLER, intViewId);
			startActivityForResult(intent, 0);

		}

		//finish this activity
		finish();

	}
	
	private void displayLayout(int layoutId) {

		lytWelcomeOptions.setVisibility(View.GONE);
		lytCountrySelector.setVisibility(View.GONE);

		//set passed view visible
		View layoutView = findViewById(layoutId);
		layoutView.setVisibility(View.VISIBLE);

		if(layoutId==R.id.lytCountrySelector){
			welcome_tvWelcome.setText(getString(R.string.str_select_your_location));
		}else{
			welcome_tvWelcome.setText(getString(R.string.welcome_tvWelcome));
		}

	}
	
	@Override
	public void onBackPressed(){
		
		if(lytCountrySelector.getVisibility()==View.VISIBLE){
			
			displayLayout(R.id.lytWelcomeOptions);
			return;
		}
		
		AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_general), "generalBack");
		
		super.onBackPressed();
	}

	public void finishActivity(){
		finish();
	}
	

	
	private void launchStartupActivity(){
		
		Intent intent = new Intent(this, StartupActivity.class);
		//intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION); //this makes sure activities do not slide in from right to left when started, but simply appear
		startActivityForResult(intent, 0);

	}
	
	private void setSelectedStore(View view){
		
		//persist selected wiggle store
		String strSelectedStoreKey = view.getTag().toString();
		
		Map<String, Object> gtmTagsMap = new HashMap<String, Object>();
		gtmTagsMap.put("chosenLocation", strSelectedStoreKey);
		AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_welcome), "changeLocation", gtmTagsMap);
		
		//derive other country settings (language, currency etc and persist with ObjAppUser)
		deriveCountrySettings(strSelectedStoreKey);
		
		//display selection following country choice
		displayLayout(R.id.lytWelcomeOptions);
		welcome_tvSelectStore.setText(String.format(getString(R.string.str_your_selected_location), AppUtils.getCountryName()));
		welcome_ivCountry.setImageDrawable(AppUtils.getCountryFlag());
		
	}
	
	private void initialiseAppUserCountryByDeviceLocation(){
		
		String strAppUserCountry = ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY);
		
		if(strAppUserCountry==null || "".equals(strAppUserCountry)){
		
			TelephonyManager tm = (TelephonyManager)this.getSystemService(this.TELEPHONY_SERVICE);
			
			
			/**
			String strUserConfiguredCountry = getResources().getConfiguration().locale.getCountry();
			String strLocaleCountry = Locale.getDefault().getCountry();
			
			String strNetworkCountryCode = tm.getNetworkCountryIso();
			String strSimCountryCode = tm.getSimCountryIso();
			
			AppUtils.showCustomToast("ResConfig Locale:" + strUserConfiguredCountry 
								+ "\n" + "Default Locale: " + strLocaleCountry
								+ "\n" + "Network country: " + strNetworkCountryCode
								+ "\n" + "Sim country: " + strSimCountryCode
								, true);
								
			**/
			
			//In priority order - more of an art than a science. An IP lookup such as http://ip-api.com/json would be great
			//- Network (where is the phone?)
			//- SIM (what country did user purchase the sim card in?)
			//- User device config (Android country / language config)
			String strDeviceCountryCode = tm.getNetworkCountryIso();  //(gb, us, fr, au, nz, ...)
			
			if(strDeviceCountryCode==null || "".equals(strDeviceCountryCode)){
				strDeviceCountryCode = tm.getSimCountryIso(); //(gb, us, fr, au, nz, ...)
			}
			
			if(strDeviceCountryCode==null || "".equals(strDeviceCountryCode)){
				strDeviceCountryCode = getResources().getConfiguration().locale.getCountry();  //(GB, US, FR, .. AU and NZ only if user installed this)
			}
			
			//turn into upper case
			strDeviceCountryCode = strDeviceCountryCode.toUpperCase();
			
			//verify if this country is supported by the Hybrid app
			String strSupportedCountryCode = AppUtils.getSupportedCountryCode(strDeviceCountryCode);
			
			//derive other country settings (language, currency etc and persist with ObjAppUser)
			deriveCountrySettings(strSupportedCountryCode);
			
			
			AppUtils.showCustomToast("Best guess country: " + strDeviceCountryCode, true);
			
		}else{
			AppUtils.showCustomToast("AppUser country: " + strAppUserCountry, true);
		}
		
		
		welcome_ivCountry.setImageDrawable(AppUtils.getCountryFlag());
		welcome_tvSelectStore.setText(String.format(getString(R.string.welcome_tvSelectStore), AppUtils.getCountryName()));
		
		
	}

	private void initialiseAppUserCountryByDeviceLanguage(){

		/*

			Some exotic locales that we may encounter ...
			en_NZ - New Zealand
			en_AU - Australia
			en_US - USA
			en_GB - United Kingdom -> fallback

			sv_ -> Sweden


		 */

		String strAppUserCountry = ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY);

		if(strAppUserCountry==null || "".equals(strAppUserCountry)){

			//get device language
			String strLocale = Locale.getDefault().toString();

			//Default - get language part of locale (e.g. en_GB -> retrieve "en")
			String strDeviceLanguage = Locale.getDefault().getLanguage();
			//String strDeviceLanguage = getResources().getConfiguration().locale.getLanguage();

			//turn into upper case
			strDeviceLanguage = strDeviceLanguage.toUpperCase();

			//Translate exotic locales into appropriate dummy-languages where applicable
			//So e.g. en_NZ - even if the device is set to English, we want to route the user to the NZ shop of course
			if("en_US".equals(strLocale)){
				strDeviceLanguage = getString(R.string.str_locale_country_iso_code_us);
			}else if("en_NZ".equals(strLocale)){
				strDeviceLanguage = getString(R.string.str_locale_country_iso_code_nz);
			}else if("en_AU".equals(strLocale)){
				strDeviceLanguage = getString(R.string.str_locale_country_iso_code_au);
			}

			//Extra handling for Swedish, Japanese and Chinese
			if("SV".equals(strDeviceLanguage)){
				strDeviceLanguage = getString(R.string.str_locale_country_iso_code_se);
			}else if("JA".equals(strDeviceLanguage)){
				strDeviceLanguage = getString(R.string.str_locale_country_iso_code_jp);
			}else if("ZH".equals(strDeviceLanguage)){
				strDeviceLanguage = getString(R.string.str_locale_country_iso_code_cn);
			}

			//verify if this country is supported by the Hybrid app
			String strSupportedCountryCode = AppUtils.getSupportedCountryCode(strDeviceLanguage);

			//derive other country settings (language, currency etc and persist with ObjAppUser)
			deriveCountrySettings(strSupportedCountryCode);


			String strInfo = "Locale=" + strLocale + "\nLanguage=" + strDeviceLanguage + "\nCountryCdoe=" + strSupportedCountryCode;
			AppUtils.showCustomToast("Language info: " + strInfo, true);

		}else{
			AppUtils.showCustomToast("AppUser country: " + strAppUserCountry, true);
		}


		welcome_ivCountry.setImageDrawable(AppUtils.getCountryFlag());
		welcome_tvSelectStore.setText(String.format(getString(R.string.welcome_tvSelectStore), AppUtils.getCountryName()));


	}

	private void checkStoredUserCountry(){
		
		String strAppUserCountry = ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY);
		//strAppUserCountry = ""; //test only to stop on this screen
		if(strAppUserCountry!=null && !"".equals(strAppUserCountry)){
			if(AppUtils.getWiggleVersion().getVersionCode()>AppUtils.APP_VERSION.VERSION_1_HYBRID.getVersionCode()){
				launchHybridActivity(0);
			}else {
				launchStartupActivity();
			}
		}else{

			AppTagManager.instance().sendVersion2ScreenView("App - Home");

			//show welcome activity
			lytWelcomeActivity.setVisibility(View.VISIBLE);
			//standard layout
			displayLayout(R.id.lytWelcomeOptions);

			//fade main layout in nicely
			AppAnimations.getInstance().startFadeInAnimationWithDurationInMs(300, lytWelcomeActivity);

		}
		
	}
	
	private void deriveCountrySettings(String strCountryCode){
		
		//clear all fields
		ObjAppUser.instance().clearCountrySpecificFields();
		
		//then re-initialise
		ObjAppUser countryUser = new ObjAppUser();
		countryUser.setField(ObjAppUser.FIELD_COUNTRY, strCountryCode);
		ObjAppUser.instance().setField(ObjAppUser.FIELD_COUNTRY, strCountryCode); //updating app user immediately as AppUtils methods below read from this object
		
		String strStoreUrl = AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home));
		Uri uri = Uri.parse(strStoreUrl);
		String strHost = uri.getHost();
		countryUser.setField(ObjAppUser.FIELD_CURRENT_WIGGLE_STORE, strHost);
		ObjAppUser.instance().setField(ObjAppUser.FIELD_CURRENT_WIGGLE_STORE, strHost); //updating app user immediately as AppUtils methods below read from this object
		
		
		String strCurrency = AppUtils.getCurrency(strCountryCode);
		countryUser.setField(ObjAppUser.FIELD_PARAMETER_CURRENCY, strCurrency);
		
		String strLanguage = AppUtils.getLanguageCodeForStore(strHost);
		countryUser.setField(ObjAppUser.FIELD_PARAMETER_LANGUAGE, strLanguage);
		
		String strDestination = AppUtils.getDefaultDestination(strCountryCode);
		countryUser.setField(ObjAppUser.FIELD_PARAMETER_DELIVERY_DESTINATION, strDestination);
		
		//update remaining fields of app user - and persist to shared preferences
		ObjAppUser.instance().updateAppUser(countryUser);
		
	}

	@Override
	protected void onNewIntent(Intent intent)
	{
		super.onNewIntent(intent);

	}

	private void checkForRequiredAppUpgrade(){

		//Check if the user needs to upgrade their app
        new ObjApiRequest(this,
                null,
                AppRequestHandler.ENDPOINT_UPGRADE_CHECK,
                Request.Method.GET,
                null, //temporary login user as opposed to application user
                null,
                AppRequestHandler.ON_RESPONSE_ACTION.NO_ACTION)
                .submit();
	}

	private void showUpgradeRequiredMessage(){

   		Intent intent = new Intent(this, UpdateRequiredActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		//intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION); //this makes sure activities do not slide in from right to left when started, but simply appear
		startActivityForResult(intent, 0);

	}

	private void loadMainScreenWithDelay(){

		handler.postDelayed(loadMainScreenDelayed, 0);
	}

	// Define the code block to be executed
	private Runnable loadMainScreenDelayed = new Runnable() {
		@Override
		public void run() {

			loadMainScreen();

		}
	};


   private void loadMainScreen(){

       //if the user already selected a country - open next activity immediately
	   checkStoredUserCountry();

	   //do this after checkStoredUserCountry
	   initialiseAppUserCountryByDeviceLanguage();
   }


    @Override
    public void onApiRequestSuccess(ObjApiRequest apiRequest, JSONObject apiSuccessDetail) {

        if(AppRequestHandler.ENDPOINT_UPGRADE_CHECK.equals(apiRequest.getURLEndpoint())){

            if(AppRequestHandler.ON_RESPONSE_ACTION.SHOW_UPGRADE_REQUIRED_DIALOG.equals(apiRequest.getOnResponseAction())){
            	showUpgradeRequiredMessage();
			}else{
				loadMainScreenWithDelay();
			}

        }

    }

    @Override
    public void onApiRequestError(ObjApiRequest request, int intHttpResponseCode, JSONObject apiErrorDetail) {

        if(AppRequestHandler.ENDPOINT_UPGRADE_CHECK.equals(request.getURLEndpoint())){

            //do nothing if the upgrade check api is unavailable or errors .. , just ignore continue
			loadMainScreenWithDelay();
        }
    }
}
