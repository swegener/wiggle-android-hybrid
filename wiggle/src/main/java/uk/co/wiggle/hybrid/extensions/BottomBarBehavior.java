package uk.co.wiggle.hybrid.extensions;

import android.content.Context;
import com.google.android.material.appbar.AppBarLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

public class BottomBarBehavior extends CoordinatorLayout.Behavior<LinearLayout> {

    private int defaultDependencyTop = -1;

    public BottomBarBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, LinearLayout child, View dependency) {
        return dependency instanceof AppBarLayout;
    }


    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, LinearLayout child, View dependency) {
        if (defaultDependencyTop == -1) {
            defaultDependencyTop = dependency.getTop();
        }
        //Doing exactly what the top view is doing:
        // child.setTranslationY(-dependency.getTop() + defaultDependencyTop);

        //We however have a difference in height between the top and bottom view - thus we need to adjust both
        //so if the dependency is hiding 30% of its height, the bottom view should hide 30% of its height too
        float movementRatio = (float) dependency.getTop() / dependency.getHeight();
        /*
        Logger.printMessage("return", "movementRatio" + movementRatio, Logger.INFO);
        Logger.printMessage("return", "dependency.getTop()" + dependency.getTop(), Logger.INFO);
        Logger.printMessage("return", "dependency.getHeight()" + dependency.getHeight(), Logger.INFO);
        */
        child.setTranslationY(-(movementRatio * child.getHeight()));
        return true;
    }

}