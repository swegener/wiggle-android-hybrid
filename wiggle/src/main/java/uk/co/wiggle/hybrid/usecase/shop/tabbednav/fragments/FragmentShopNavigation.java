package uk.co.wiggle.hybrid.usecase.shop.tabbednav.fragments;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjNavigationCategoryManager;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjRecentSearchesManager;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjApiRequest;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNavigationCategory;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjTypeaheadResponse;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjUserSearch;
import uk.co.wiggle.hybrid.application.helpers.AppGlobalConstants;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.application.session.AppSession;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentSelfCloseListener;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentUI;
import uk.co.wiggle.hybrid.tagging.AppTagManager;
import uk.co.wiggle.hybrid.usecase.newsfeed.activities.NewsfeedActivity;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.activities.TabbedHybridActivity;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.adapters.TabbedTextSearchListAdapter;


/**
 * @author Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 *
 * @license The code of this Application is licensed to Wiggle UK Ltd.
 * The license and its ownership rights can not be transferred to a Third Party.
 *
 * @android If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 **/


public class FragmentShopNavigation extends Fragment implements IAppFragmentUI {

    private static final String logTag = "FragmentShopNavigation";

    public static String getLogTag(){
        return logTag;
    }

    RelativeLayout lytNativeSearch;
    LinearLayout nativeSearch_lytDummyFocus;
    EditText nativeSearch_etQueryString;
    ImageView nativeSearch_ivTextSearchClear;
    LinearLayout navigation_lytCategoryChildren;
    RelativeLayout nativeSearch_lytResult;
    AbsListView nativeSearch_lvSearchResult;
    LinearLayout nativeSearch_dynamicHeader;
    TextView textSearch_tvGroupHeader;
    ScrollView navigation_svBrowser;
    TextView nativeSearch_tvCancel;

    IAppFragmentSelfCloseListener selfCloseListener;


    String strCaller = "";
    private static FragmentShopNavigation myInstance;

    public static FragmentShopNavigation instance() {
        return myInstance;
    }

    //Native search
    TabbedTextSearchListAdapter mTextSearchListAdapter;
    UpdateTextSearchListviewTask mUpdateTextSearchListviewTask;

    //Categories
    UpdateNavigationBrowserTask mUpdateNavigationBrowserTask = new UpdateNavigationBrowserTask();


    ObjNavigationCategory mCurrentNavigationCategory;

    public ObjNavigationCategory getCurrentNavigationCategory(){
        return mCurrentNavigationCategory;
    }

    public void clearCurrentNavigationCategory(){
        mCurrentNavigationCategory = null;
    }


    //defines the category order in which we want our typeahead search result returned
    ArrayList<String> mTypeadheadSortOrder = new ArrayList<String>(
            Arrays.asList(ObjTypeaheadResponse.CONST_RESULT_TYPE_PRODUCT,
                    ObjTypeaheadResponse.CONST_RESULT_TYPE_BRAND,
                    ObjTypeaheadResponse.CONST_RESULT_TYPE_HELP,
                    ObjTypeaheadResponse.CONST_RESULT_TYPE_CONTENT)
    );

    List<ObjTypeaheadResponse> mLastKnownTypeaheadResponses = new ArrayList<ObjTypeaheadResponse>();

    String mDeviceLanguage;
    int intMinCharsForTextWatcher; //character minimum for text watcher

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myInstance = this;


        /** Getting the arguments to the Bundle object */
        Bundle data = getArguments();

        // Get the extras
        if (!data.containsKey(AppGlobalConstants.CALLING_ACTIVITY)) {
            throw new IllegalArgumentException("The " + AppGlobalConstants.CALLING_ACTIVITY + " must be passed as extra for the intent. "
                    + "The content of the extras is " + data);
        }

        strCaller = data.getString(AppGlobalConstants.CALLING_ACTIVITY);

        //get device language (used to reduce number of minimum characters required on typeahead search for Chinese language)
        mDeviceLanguage = Locale.getDefault().getLanguage();

         //If this device is on Chinese language - reduce number of minimum characters required for a search to 1
        if(Locale.CHINESE.getLanguage().equals(mDeviceLanguage)
                || getString(R.string.str_locale_country_iso_code_cn).equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY))
                ){

            //For Chinese we only require 1 character minimum for our text watcher
            intMinCharsForTextWatcher = 0;

        }else {

            //all other languages: we require 3 characters minimum for our text watcher
            intMinCharsForTextWatcher = 2;

        }


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.search_tab_layout, container, false);


        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        updateFragmentContent(view);

    }


    @Override
    public void updateFragmentContent(View view) {


        findViews(view);

        setCustomFontTypes();

        //load recent searches on search screen
        ObjRecentSearchesManager.getInstance().loadPersistedData();

        //initialise navigation browser on navigation screen
        updateNavigationBrowser();


    }


    public void findViews(View view) {

        lytNativeSearch = (RelativeLayout) view.findViewById(R.id.lytNativeSearch);
        nativeSearch_lytDummyFocus = (LinearLayout) view.findViewById(R.id.nativeSearch_lytDummyFocus);
        nativeSearch_etQueryString = (EditText) view.findViewById(R.id.nativeSearch_etQueryString);
        setupNativeSearchWidgets();

        nativeSearch_ivTextSearchClear = (ImageView) view.findViewById(R.id.nativeSearch_ivTextSearchClear);
        navigation_lytCategoryChildren = (LinearLayout) view.findViewById(R.id.navigation_lytCategoryChildren);
        nativeSearch_lytResult = (RelativeLayout) view.findViewById(R.id.nativeSearch_lytResult);
        nativeSearch_lvSearchResult = (AbsListView) view.findViewById(R.id.nativeSearch_lvSearchResult);
        nativeSearch_dynamicHeader = (LinearLayout) view.findViewById(R.id.nativeSearch_dynamicHeader);
        textSearch_tvGroupHeader = (TextView) view.findViewById(R.id.textSearch_tvGroupHeader);
        navigation_svBrowser = (ScrollView) view.findViewById(R.id.navigation_svBrowser);
        nativeSearch_tvCancel = (TextView) view.findViewById(R.id.nativeSearch_tvCancel);

        updateWidgets();

    }

    public void updateWidgets() {

        displayLayout(navigation_lytCategoryChildren);

        mTextSearchListAdapter = new TabbedTextSearchListAdapter();
        nativeSearch_lvSearchResult.setAdapter(mTextSearchListAdapter);

        setListViewScrollAdapter();

    }


    private void setCustomFontTypes() {

        nativeSearch_etQueryString.setTypeface(AppTypefaces.instance().fontMainRegular);
        textSearch_tvGroupHeader.setTypeface(AppTypefaces.instance().fontMainBold);
        nativeSearch_tvCancel.setTypeface(AppTypefaces.instance().fontMainRegular);

    }


    @Override
    public void onStart() {
        super.onStart();

        setTitleBarText();

    }

    @Override
    public void onResume() {

        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();



    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);


    }


    //Check if this is working - this is the new version of deprecated
    //public void onAttach(Activity activity)
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            selfCloseListener = (IAppFragmentSelfCloseListener) context;
        } catch (ClassCastException e) {
            throw new RuntimeException(getActivity().getClass().getName() + " must implement IAppFragmentSelfCloseListener to use this fragment", e);
        }

    }

    public void onClickEvent(int intClickedViewId) {

        switch (intClickedViewId) {

            case R.id.nativeSearch_ivTextSearchClear:
                onKeywordTextCleared();
                break;

            case R.id.nativeSearch_tvCancel:
                shopFragmentGoBack();
                break;

            default:
                break;

        }

    }

    private void displayLayout(View viewToShow) {

        navigation_lytCategoryChildren.setVisibility(View.GONE);
        nativeSearch_lytResult.setVisibility(View.GONE);

        //Show navigation browser
        if(viewToShow.getId()==navigation_lytCategoryChildren.getId()){
            //remove focus from EditText & close soft keyboard
            nativeSearch_etQueryString.clearFocus();
            nativeSearch_lytDummyFocus.requestFocus();
            nativeSearch_tvCancel.setVisibility(View.GONE);
            TabbedHybridActivity.instance().hideVirtualKeyboard();

        //Show search screen
        }else if(viewToShow.getId()==nativeSearch_lytResult.getId()){

            AppTagManager.instance().sendVersion2ScreenView("App - search");

            //start with clean view every time the search screen is shown after the nav screen
            mLastKnownTypeaheadResponses = null;
            updateQueryResult(null);

            nativeSearch_tvCancel.setVisibility(View.VISIBLE);
            //let user do this as he may wish to browse recent searches only:
            //nativeSearch_etQueryString.clearFocus();
            //nativeSearch_etQueryString.requestFocus();
        }

        viewToShow.setVisibility(View.VISIBLE);

    }


    private void setupNativeSearchWidgets() {

        nativeSearch_etQueryString.addTextChangedListener(mTypeaheadTextWatcher);

        nativeSearch_etQueryString.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {

                    //User opens search view by clicking into the EditText
                    AppTagManager.instance().sendVersion2ScreenView("App - search");
                    AppTagManager.instance().sendV2EventTags("linkclick","App - search","Search Box","Initiate Search");

                    //nativeSearch_etQueryString.setText(""); //leave text as user might still need previous search result - user can press "x" to clear text
                    //whenever EditText gets the focus - display search layout
                    displayLayout(nativeSearch_lytResult);
                    updateQueryResult(null);
                }

            }

        });

        //handle "Go" (Enter) click on keyboard
        nativeSearch_etQueryString.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_GO) {

                    //Event tag for executing a search
                    AppTagManager.instance().sendV2EventTags("formSubmit","App - search","Search Box","Search");

                    //if user presses enter whilst typing in a search string - build query string
                    //http://www.wiggle.co.uk?s=texttheuserentered
                    //and open in webview

                    String strUserKeyword = nativeSearch_etQueryString.getText().toString();

                    //upper case this for API
                    String strQueryKeyword = strUserKeyword.toUpperCase();

                    //execute search in browser
                    excecuteSearchForKeyword(strQueryKeyword);

                    //user executed this search deliberately - so store it for later use
                    saveAsRecentSearch(strUserKeyword);




                    return true;
                }
                return false;
            }


        });


    }


	/* Native search handling */


    private final TextWatcher mTypeaheadTextWatcher = new TextWatcher() {

        public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

            //do nothing if text was deleted
            if (count <= before) { //<= as sometimes counts get equal even though no change perceived for user
                return;
            }


            if (charSequence.length() > 0) {

                onKeywordTextEntered();



                if (charSequence.length() > intMinCharsForTextWatcher) {


                    String strQueryKeyword = charSequence.toString().toUpperCase();

                    submitAjaxRequest(strQueryKeyword);

                }

            }

        }

        public void afterTextChanged(Editable s) {

            if(s.length()==0){
                //clear last search and display empty list
                mLastKnownTypeaheadResponses = null;
                updateQueryResult(null);
            }
        }

    };


    private void onKeywordTextEntered() {

        nativeSearch_ivTextSearchClear.setVisibility(View.GONE); //always hide (APP-200)

    }

    private void onKeywordTextCleared() {

        nativeSearch_ivTextSearchClear.setVisibility(View.GONE);
        nativeSearch_etQueryString.setText("");

        //clear last search and display empty list
        mLastKnownTypeaheadResponses = null;
        updateQueryResult(null);

        showVirtualKeyboard();

    }

    public void updateQueryResult(List<ObjTypeaheadResponse> lstTypeaheadResponse) {

        mUpdateTextSearchListviewTask = new UpdateTextSearchListviewTask(lstTypeaheadResponse);
        getActivity().runOnUiThread(mUpdateTextSearchListviewTask);

    }


    private class UpdateTextSearchListviewTask implements Runnable {

        private List<ObjTypeaheadResponse> resultList;
        private List<ObjTypeaheadResponse> displayList = new ArrayList<ObjTypeaheadResponse>();

        public UpdateTextSearchListviewTask(List<ObjTypeaheadResponse> resultList) {
            this.resultList = resultList;

            if(this.resultList==null){
                this.resultList = mLastKnownTypeaheadResponses;
            }
            //save this .. used in combination with saved searches
            if(resultList!=null) {
                mLastKnownTypeaheadResponses = resultList;
            }
        }


        @Override
        public void run() {


            if (resultList!= null && resultList.size() > 0) {

                //loop through result list and pick items in the desired order
                for (int i = 0; i < mTypeadheadSortOrder.size(); i++) {
                    String strType = mTypeadheadSortOrder.get(i);
                    for (int j = 0; j < resultList.size(); j++) {
                        ObjTypeaheadResponse result = resultList.get(j);
                        if (result.isRecordOfType(strType)) {
                            displayList.add(result);
                        }
                    }
                }

                //fallback - any results that we did not know about - add them to the end of the list
                for (int i = 0; i < resultList.size(); i++) {
                    ObjTypeaheadResponse result = resultList.get(i);
                    if (!displayList.contains(result)) {
                        displayList.add(result);
                    }
                }

            }


            //always add recent searches to list
            displayList.addAll(getRecentSearches());

            //show display list (combination of search result and recent searches) if not empty
            if(displayList.size()>0){

                //Add dummy item that is used to make the list scrollable above the keyboard
                ObjTypeaheadResponse dummyFooter = new ObjTypeaheadResponse();
                dummyFooter.setField(ObjTypeaheadResponse.FIELD_DISPLAY_TYPE, "DummyFooter");
                displayList.add(dummyFooter);

                mTextSearchListAdapter.setListContent(displayList);

                mTextSearchListAdapter.notifyDataSetChanged();
                nativeSearch_lvSearchResult.setSelection(0); //scroll back to top on each new search result

                nativeSearch_lvSearchResult.setVisibility(View.VISIBLE);


            } else {

                nativeSearch_lvSearchResult.setVisibility(View.GONE);
                textSearch_tvGroupHeader.setText("");

            }




        }

    }


    private void setListViewScrollAdapter() {


        nativeSearch_lvSearchResult.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScroll(AbsListView listView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                if (nativeSearch_lvSearchResult != null && nativeSearch_lvSearchResult.getCount() > 0) {

                    int intFirstVisibleItem = nativeSearch_lvSearchResult.getFirstVisiblePosition();

                    ObjTypeaheadResponse response = (ObjTypeaheadResponse) nativeSearch_lvSearchResult.getItemAtPosition(intFirstVisibleItem);

                    if (response.getParentRecord() != null) {
                        response = response.getParentRecord();
                    }

                    String strDisplayType = response.getString(ObjTypeaheadResponse.FIELD_DISPLAY_TYPE);
                    if(ObjTypeaheadResponse.CONST_DISPLAY_TYPE_PARENT.equals(strDisplayType)) {

                        String strGroupHeader = response.getString(ObjTypeaheadResponse.FIELD_NAME);
                        nativeSearch_dynamicHeader.setVisibility(View.VISIBLE);
                        textSearch_tvGroupHeader.setText(strGroupHeader);

                    }


                } else {

                    nativeSearch_dynamicHeader.setVisibility(View.GONE);

                }


            }

            @Override
            public void onScrollStateChanged(AbsListView arg0, int arg1) {
                // TODO Auto-generated method stub

            }

        });

    }

    private void showVirtualKeyboard() {

        nativeSearch_etQueryString.clearFocus();
        nativeSearch_etQueryString.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(nativeSearch_etQueryString, InputMethodManager.SHOW_IMPLICIT);

    }

    public String getSearchTerm() {

        String strSearchTerm = nativeSearch_etQueryString.getText().toString().toUpperCase();
        return strSearchTerm;

    }


    private class UpdateNavigationBrowserTask implements Runnable {

        @Override
        public void run() {

            ObjNavigationCategory combinedNavigationTree = ObjNavigationCategoryManager.getInstance().getCombinedNavigationTree(TabbedHybridActivity.getLogTag());
            //Prevent NPE
            if(combinedNavigationTree==null){
                return;
            }

            List<ObjNavigationCategory> lstMenuItems = combinedNavigationTree.getChildrenForCurrentCountry();

            if(lstMenuItems.size()>0){
                navigation_svBrowser.setVisibility(View.VISIBLE);
                updateCategoryList(lstMenuItems);
                //expand list to go to current navigation category
                //expandTreeToCurrentCategory(currentNavigationCategory);
            } else {
                navigation_svBrowser.setVisibility(View.INVISIBLE);
            }

        }

    }

    public void updateNavigationBrowser(){

        //prevent NPEs
        if(getActivity()==null){
            return;
        }

        if(ObjNavigationCategoryManager.getInstance().getLocalNavigationTree()==null){
            TabbedHybridActivity.instance().loadLocalNavigationTreeIfMissing();
        }

        getActivity().runOnUiThread(mUpdateNavigationBrowserTask);

    }

    private void updateCategoryList(List<ObjNavigationCategory> lstMenuItems){

        //remove all drawables first if any already available ..
        if (navigation_lytCategoryChildren.getChildCount() > 0) {
            unbindDrawables(navigation_lytCategoryChildren);
        }

        for (int i = 0; i < lstMenuItems.size(); i++) {

            ObjNavigationCategory category = lstMenuItems.get(i);
            addLevelOneCategory(category);

        }

    }

    /**
     * This method is supposed to clear the link between the layout object image views and their drawables,
     * allowing them to be garbage collected and memory freed up. App gets terribly slow as painting the
     * resort layout consumes a lot of memory that does not get released.
     */
    private void unbindDrawables(View view) {

        if (view.getBackground() != null) {

            if (view instanceof ImageView) {
                ((ImageView) view).setImageDrawable(null);
            }

            view.getBackground().setCallback(null);
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }

            ((ViewGroup) view).removeAllViews();
        }

    }

    private void addLevelOneCategory(final ObjNavigationCategory category){


        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final LinearLayout levelOneLayout = (LinearLayout) inflater.inflate(R.layout.nav_browser_category, null);
        //store this view against the category so we can access it later to update the progress bar etc
        category.setAdapterView(levelOneLayout);

        TextView categoryText = (TextView) levelOneLayout.findViewById(R.id.navBrowserCategory_tvLevelOneButton);
        TextView childCategoryText = (TextView) levelOneLayout.findViewById(R.id.navBrowserCategory_tvLevelChildButton);
        categoryText.setVisibility(View.VISIBLE);
        childCategoryText.setVisibility(View.GONE);

        categoryText.setTypeface(AppTypefaces.instance().fontMainRegular);
        categoryText.setText(category.getDisplayName());

        final RelativeLayout levelOneButton = (RelativeLayout) levelOneLayout.findViewById(R.id.navBrowserCategory_button);
        levelOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                handleMenuCategoryClick(category, false);
            }
        });

        //Tag the layout for this category (only used for debugging)
        levelOneLayout.setTag(category);

        //initialise open/close state
        //initialiseVtFAQTopicVisibility(faqTopicLayout, topic);

        navigation_lytCategoryChildren.addView(levelOneLayout);

        //LinearLayout lytFAQDetails = (LinearLayout) faqTopicLayout.findViewById(R.id.insuranceFAQCategoryButton_lytAllFAQs);
        //addVtQAPairs(lytFAQDetails, topic, lstQuestionAnswerPairs);

    }



    private void addCategoryChildren(final ObjNavigationCategory category){


        //Get child container from category adapter view
        View parentView = category.getAdapterView();

        final LinearLayout navigationItem_lytChildren = (LinearLayout) parentView.findViewById(R.id.navigationItem_lytChildren);

        //tag this container with the parent category (only used for debugging)
        navigationItem_lytChildren.setTag(category);

        //set layout params to cover content. This is done in code to ensure animations will work first time
        navigationItem_lytChildren.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        //remove all drawables first if any already available ..
        if (navigationItem_lytChildren.getChildCount() > 0) {
            unbindDrawables(navigationItem_lytChildren);
        }

        //Sort children by display name - use sorted list of children for display purposes
        //List<ObjNavigationCategory> lstCategoryChildren = category.getSortedChildren();
        //Wiggle requirement 07/08/2018: show categories as returned by the server - no alphabetic ordering
        List<ObjNavigationCategory> lstCategoryChildren = category.getAllChildren();
        for(int i=0;i<lstCategoryChildren.size();i++){
            ObjNavigationCategory singleCategory = lstCategoryChildren.get(i);
            addSingleChild(singleCategory, navigationItem_lytChildren, false);
        }

        //If this category has a list of children, then add "All of this category" child at the end of this list
        if(lstCategoryChildren.size()>0) {

            //the Parent category is already in this list as the parent level, so we cannot add it again
            //For the "All"-category, we hence create a copy of the parent and add that copy.
            ObjNavigationCategory allCategory = new ObjNavigationCategory();
            //copy fields from parent
            allCategory = category.createCopy(allCategory);

            String strCategoryName = category.getDisplayName();
            String strAllCategoryName = String.format(getString(R.string.str_all_of_this_category), strCategoryName);
            //create "Category (All)" text
            allCategory.setField(ObjNavigationCategory.FIELD_NAME, strAllCategoryName);
            allCategory.setField(ObjNavigationCategory.FIELD_REFINEMENT_NAME, strAllCategoryName);
            //also send a separate GTM tag. This will also help to differentiate this category from the original parent (equals() no longer true)
            String strGTMTag = allCategory.getString(ObjNavigationCategory.FIELD_GOOGLE_TAG_MANAGER_ID);
            if(strGTMTag==null || "".equals(strGTMTag)){
                strGTMTag = strCategoryName; //No GTM Tag? Use category name.
            }
            allCategory.setField(ObjNavigationCategory.FIELD_GOOGLE_TAG_MANAGER_ID, strGTMTag + "All"); //TODO: adjust "All" category tag to Wiggle specification
            //flag this as "All of" category
            allCategory.setIsAllOfCategory();

            String strCategoryType = allCategory.getString(ObjNavigationCategory.FIELD_TYPE);
            if (ObjNavigationCategory.CONST_CATEGORY_TYPE_CATEGORIES.equals(strCategoryType)
                    || ObjNavigationCategory.CONST_CATEGORY_TYPE_WIGGLE_API.equals(strCategoryType)
                    || ObjNavigationCategory.CONST_CATEGORY_TYPE_SUBMENU.equals(strCategoryType)
                    || ObjNavigationCategory.CONST_CATEGORY_TYPE_END_CATEGORY_OPEN_URL.equals(strCategoryType)
                    ) {
                addSingleChild(allCategory, navigationItem_lytChildren, true);
            }
        }

        category.toggleChildVisibility();

    }



    private void addSingleChild(final ObjNavigationCategory category, final LinearLayout childContainer, final boolean blnAllOfThisCategoryItem){

        //check if this category is already displayed in this layout. If yes, return...
        if(childContainer.findViewWithTag(category)!=null){
            return;
        }

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final LinearLayout currentLevelLayout = (LinearLayout) inflater.inflate(R.layout.nav_browser_category, null);
        //store this view agains the category so we can access it later to update the progress bar etc
        category.setAdapterView(currentLevelLayout);

        TextView categoryText = (TextView) currentLevelLayout.findViewById(R.id.navBrowserCategory_tvLevelChildButton);
        categoryText.setText(category.getDisplayName());
        categoryText.setVisibility(View.VISIBLE);
        TextView parentCategoryText = (TextView) currentLevelLayout.findViewById(R.id.navBrowserCategory_tvLevelOneButton);
        parentCategoryText.setVisibility(View.GONE);

        View tab1 = currentLevelLayout.findViewById(R.id.navBrowserCategory_tab1);
        View tab2 = currentLevelLayout.findViewById(R.id.navBrowserCategory_tab2);
        View tab3 = currentLevelLayout.findViewById(R.id.navBrowserCategory_tab3);
        View bottomLine = currentLevelLayout.findViewById(R.id.navigationItem_vBottomLine);
        ImageView chevron = (ImageView) currentLevelLayout.findViewById(R.id.navigationItem_ivMore);
        View navBrowserCategory_vTopMargin = currentLevelLayout.findViewById(R.id.navBrowserCategory_vTopMargin);
        navBrowserCategory_vTopMargin.setVisibility(View.GONE); // no top margin for child categories



        //Get category level and format text accordingly
        int intCategoryLevel = category.getCategoryLevel();

        switch (intCategoryLevel) {

            //Level 3 API Category - bold text, one tab indent
            //Level 1 - Wiggle Home (root), Level 2 - Shop by Department, Level 3 - Parent Category (Cycle, Run, Swim, Tri)
            //Level 4 is the first API level
            case ObjNavigationCategory.CONST_NAV_BROWSER_LEVEL_1:
                categoryText.setTypeface(AppTypefaces.instance().fontMainRegular);
                tab1.setVisibility(View.VISIBLE);
                bottomLine.setVisibility(View.VISIBLE);
                chevron.setVisibility(View.GONE);
                break;

            case ObjNavigationCategory.CONST_NAV_BROWSER_LEVEL_2:
                categoryText.setTypeface(AppTypefaces.instance().fontMainRegular);
                tab1.setVisibility(View.VISIBLE);
                tab2.setVisibility(View.VISIBLE);
                bottomLine.setVisibility(View.VISIBLE);
                chevron.setVisibility(View.VISIBLE);
                break;

            case ObjNavigationCategory.CONST_NAV_BROWSER_LEVEL_3:
                categoryText.setTypeface(AppTypefaces.instance().fontMainRegular);
                tab1.setVisibility(View.VISIBLE);
                tab2.setVisibility(View.VISIBLE);
                tab3.setVisibility(View.VISIBLE);
                bottomLine.setVisibility(View.VISIBLE);
                chevron.setVisibility(View.GONE);
                break;

            default:
                categoryText.setTypeface(AppTypefaces.instance().fontMainBold);
                tab1.setVisibility(View.GONE);
                tab2.setVisibility(View.GONE);
                tab3.setVisibility(View.GONE);
                bottomLine.setVisibility(View.GONE);
                chevron.setVisibility(View.GONE);
                break;

        }


        final RelativeLayout categoryButton = (RelativeLayout) currentLevelLayout.findViewById(R.id.navBrowserCategory_button);
        //children - button has no background
        categoryButton.setBackground(null);



        //so we can use this in the inner class below
        final int intFinalLevel = intCategoryLevel;

        categoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //force open URL when
                //a) this is an "All of this category" item
                //b) we have exceeded 3 taps on the navigation browser (i.e. Tap on Level 5 should open URL and not retrieve further child categories)
                boolean blnForceOpenUrl = blnAllOfThisCategoryItem || intFinalLevel >= 5;

                handleMenuCategoryClick(category, blnForceOpenUrl);
            }
        });



        //initialise open/close state
        //initialiseVtFAQTopicVisibility(faqTopicLayout, topic);
        childContainer.addView(currentLevelLayout);

    }




    public void handleMenuCategoryClick(ObjNavigationCategory category,
                                        boolean blnForceOpenUrl) {

        if(category==null){
            return;
        }

        setCurrentNavigationCategory(category, blnForceOpenUrl);

    }



    public void setCurrentNavigationCategory(ObjNavigationCategory category, boolean blnForceOpenUrl){

        //remove highlight from "current" category if required
        if(mCurrentNavigationCategory!=null){
            mCurrentNavigationCategory.setHighlighted(false);
        }

        //update navigation category
        mCurrentNavigationCategory = category;

        //highlight current category if required
        if(mCurrentNavigationCategory!=null) {
            mCurrentNavigationCategory.setHighlighted(true);
        }


        handleNavigationClick(category, blnForceOpenUrl);


    }

    public void handleNavigationClick(ObjNavigationCategory category, boolean blnForceOpenUrl){


        ImageView navigationItem_ivMore = null;
        ProgressBar navigationItem_pbInProgress = null;

        if(category==null){
            return;
        }

        String strGtmEvent = category.getString(ObjNavigationCategory.FIELD_GOOGLE_TAG_MANAGER_ID);
        //find out category type
        String strCategoryType = category.getString(ObjNavigationCategory.FIELD_TYPE);

        View categoryAdapterView = category.getAdapterView();
        if(categoryAdapterView!=null){
            navigationItem_ivMore = (ImageView) categoryAdapterView.findViewById(R.id.navigationItem_ivMore);
            navigationItem_pbInProgress = (ProgressBar) categoryAdapterView.findViewById(R.id.navigationItem_pbInProgress);
        }

        //force open URL if no more children for this category ...
        if((category.getAllChildren().size()==0	&& category.isChildrenRetrievedFromServer())
                ){

            //flag this category as dead end
            if(categoryAdapterView!=null){
                navigationItem_pbInProgress.setVisibility(View.GONE);
                navigationItem_ivMore.setVisibility(View.VISIBLE); //APP-245
            }

            strGtmEvent = "shopShop"; //make sure this tag is transmitted for UTM Tracking

            blnForceOpenUrl = true;

        }

        //only open URL if we need to (All xxx)-navigation item or category where server didn't return any more children.
        if(blnForceOpenUrl){

            TabbedHybridActivity.instance().openCategoryUrl(category, strGtmEvent);
            return;
        }


        //If we make it to here - check if this category is a parent that has got children that we already know about
        if(categoryAdapterView!=null){
            if(category.isChildrenRetrievedFromServer()) {
                navigationItem_pbInProgress.setVisibility(View.GONE);
            }
            //only show chevron for certain levels on the nav tree (middle layer)
            int intCategoryLevel = category.getCategoryLevel();

            if(intCategoryLevel==ObjNavigationCategory.CONST_NAV_BROWSER_LEVEL_2) {
                navigationItem_ivMore.setVisibility(View.VISIBLE);
            }else{
                navigationItem_ivMore.setVisibility(View.GONE);
            }
        }

        //if we have children available for this category and no URL to force-open yet, display them
        if(category.getAllChildren().size()>0){

            //Reveal categories - send corresponding event
            String strCategoryTag = category.getVersion2GATag();
            String strCategoryTier = category.getVersion2GATagLevel();
            AppTagManager.instance().sendV2EventTags("tabclick","App - Navigation",strCategoryTier,strCategoryTag);

            //stability fix ..
            if(category.countChildViews()==-1) {
                //just reset the nav browser again
                updateNavigationBrowser();
            }else if(category.countChildViews()==0){
                addCategoryChildren(category);
            }else{
                category.toggleChildVisibility();
            }

            /*
            if(category.countChildViews()==0){
                category.displayChildrenOnNavigationBrowser();
            }else{
                category.toggleChildVisibility();
            }
            */

            return;


        }



        if(ObjNavigationCategory.CONST_CATEGORY_TYPE_LINK.equals(strCategoryType)){

            //exception: IF user presses "Home" - clear all history from web view
            //Not doing this here led to users complaining about the "back button not working" as navigating back from the homepage
            //naturally showed all the previous deep-navigation pages again rather than closing the app
            String strGTMId = category.getString(ObjNavigationCategory.FIELD_GOOGLE_TAG_MANAGER_ID);
            //TODO: this should not rely on the GTM Tag for the Home category, but a separate tag or identifier
            if("shopHome".equals(strGTMId)){
                //make sure history is cleared after homepage is finished
                TabbedHybridActivity.instance().setForceClearHistory(true);
            }

            //all other categories - open URL associated with category
            TabbedHybridActivity.instance().openCategoryUrl(category, strGtmEvent);
            return; //no need to refresh navigation pane

        }else if(ObjNavigationCategory.CONST_CATEGORY_TYPE_MENU_ITEM.equals(strCategoryType)){

            if(NewsfeedActivity.getLogTag().equals(category.getString(ObjNavigationCategory.FIELD_MENU_ITEM_TARGET))) {
                TabbedHybridActivity.instance().startNewsfeedActivity(category, strGtmEvent);
            }else if("OrderTracking".equals(category.getString(ObjNavigationCategory.FIELD_MENU_ITEM_TARGET))){
                //close navigation drawer

                //retrieveOrderHistory();
                handleAndroidNativeMenuItemClick(category);
            }
            return; //no need to refresh navigation pane

        }else if(ObjNavigationCategory.CONST_CATEGORY_TYPE_CATEGORIES.equals(strCategoryType)
                || ObjNavigationCategory.CONST_CATEGORY_TYPE_WIGGLE_API.equals(strCategoryType)
                ){

            /*
               Not doing this here - as this method will be re-executed as soon as categories are
               downloaded from the server which would lead to a double-counting of events that are sent earlier in this method
            //Reveal categories - send corresponding event
            String strCategoryTag = category.getVersion2GATag();
            String strCategoryTier = category.getVersion2GATagLevel();
            AppTagManager.instance().sendV2EventTags("tabclick","App - Navigation",strCategoryTier,strCategoryTag);
            */

            //no children? get them from API
            if(category.getAllChildren().size()==0){

                //show progress bar
                if(categoryAdapterView!=null){
                    navigationItem_ivMore.setVisibility(View.GONE);
                    navigationItem_pbInProgress.setVisibility(View.VISIBLE);
                }

                //get additional categories from Wiggle API
                new ObjApiRequest(TabbedHybridActivity.instance(),
                        null,
                        AppRequestHandler.ENDPOINT_CATEGORIES,
                        Request.Method.GET,
                        category,
                        null,
                        AppRequestHandler.ON_RESPONSE_ACTION.NO_ACTION)
                        .submit();
                return; //navigation pane is refreshed or not depending on result of this request //TODO: add loading spinner and handle accordingly

            }

        }


        //if we get here i.e. in any other case ... update browser for given category
        //TODO: only update browser if there are further childs to display


    }


    private void handleAndroidNativeMenuItemClick(ObjNavigationCategory category){

        if("OrderTracking".equals(category.getString(ObjNavigationCategory.FIELD_MENU_ITEM_TARGET))){

            //send GTM Tag - report "linkclick" as another screen is opened (webview or native screen)
            String strCategoryTag = category.getVersion2GATag();
            String strCategoryTier = category.getVersion2GATagLevel();
            AppTagManager.instance().sendV2EventTags("linkclick","App - Navigation",strCategoryTier,strCategoryTag);


            //check if user is logged in
            if(ObjAppUser.instance().isUserLoggedIn()==false){
                AppSession.instance().setAndroidNativeCategory(category);
                TabbedHybridActivity.instance().showLoginScreen();
                return;
            }else{
                TabbedHybridActivity.instance().addOrderTrackingFragment(false);
            }

        }


    }


    public boolean shopFragmentCanGoBack(){

        if(nativeSearch_lytResult.getVisibility()==View.VISIBLE){

            shopFragmentGoBack();
            return true;
        }else{

            return false;
        }

    }

    public void shopFragmentGoBack(){

        displayLayout(navigation_lytCategoryChildren);
        nativeSearch_ivTextSearchClear.setVisibility(View.GONE);
        nativeSearch_etQueryString.setText("");
        nativeSearch_lytResult.setVisibility(View.GONE);


    }

    public List<ObjTypeaheadResponse> getRecentSearches(){

        //a dummy list of typeahead responses ..
        List<ObjTypeaheadResponse> lstRecentSearches = new ArrayList<ObjTypeaheadResponse>();
        List<ObjTypeaheadResponse> displayList = new ArrayList<ObjTypeaheadResponse>();


        //get the list of saved searches
        lstRecentSearches = ObjRecentSearchesManager.getInstance().getRecentSearchesForDisplay();

        if(lstRecentSearches.size()>0) {

            //Title
            ObjTypeaheadResponse dummyRecentSearchHeading = new ObjTypeaheadResponse();
            dummyRecentSearchHeading.setField(ObjTypeaheadResponse.FIELD_NAME, getString(R.string.tab_shop_your_recent_searches));
            dummyRecentSearchHeading.setField(ObjTypeaheadResponse.FIELD_DISPLAY_TYPE, ObjTypeaheadResponse.CONST_DISPLAY_TYPE_PARENT);
            displayList.add(dummyRecentSearchHeading);

            //Recent search items
            displayList.addAll(lstRecentSearches);

            //Clear Recent Searches button
            ObjTypeaheadResponse dummyClearSearchesItem = new ObjTypeaheadResponse();
            dummyClearSearchesItem.setField(ObjTypeaheadResponse.FIELD_NAME, getString(R.string.tab_shop_clear_recent_searches));
            dummyClearSearchesItem.setField(ObjTypeaheadResponse.FIELD_DISPLAY_TYPE, ObjTypeaheadResponse.CONST_DISPLAY_TYPE_BUTTON);
            displayList.add(dummyClearSearchesItem);

        }

        return displayList;

    }


    public void useRecentSearch(String strKeyword){

        //Report keyword entered by user (translation not really an option here)
        AppTagManager.instance().sendV2EventTags("linkclick","App - search","Recent Search History",strKeyword);


        //setting the text automatically will trigger the ajax search to update the search result list
        nativeSearch_etQueryString.setText(""); //clear keyword first
        nativeSearch_etQueryString.setText(strKeyword); //use stored keyword

        //open search in web shop browser
        excecuteSearchForKeyword(strKeyword);

        //store the fact that this search was re-used
        ObjUserSearch search = new ObjUserSearch(strKeyword);
        //persist in manager (addIfMissing adjusts the manager and persists its data)
        ObjRecentSearchesManager.getInstance().addIfMissing(search);

    }


    private void submitAjaxRequest(String strKeyword){

        try{

            String strGETQueryString = "s=" + URLEncoder.encode(strKeyword, "UTF-8");

            new ObjApiRequest(
                    TabbedHybridActivity.instance(),
                    null,
                    AppRequestHandler.ENDPOINT_TYPEAHEAD,
                    Request.Method.GET,
                    null,
                    strGETQueryString,
                    AppRequestHandler.ON_RESPONSE_ACTION.NO_ACTION
            ).submit();

        } catch (UnsupportedEncodingException e) {
            Logger.printMessage(logTag, "" + e.getMessage(), Logger.ERROR);
        }

    }

    private void saveAsRecentSearch(String strSearchTerm){

        //Do not store empty search terms
        if(strSearchTerm==null || "".equals(strSearchTerm)){
            return;
        }

        ObjUserSearch recentSearch = new ObjUserSearch(strSearchTerm);

        ObjRecentSearchesManager.getInstance().addIfMissing(recentSearch);

        updateQueryResult(null);

    }

    private void excecuteSearchForKeyword(String strQueryKeyword){

        if(strQueryKeyword==null || "".equals(strQueryKeyword)){
            TabbedHybridActivity.instance().hideVirtualKeyboard();
            return;
        }

        try{

            String strQuery = URLEncoder.encode(strQueryKeyword, "UTF-8");
            String strTargetURL = AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home)) + "?s=" + strQuery;
            TabbedHybridActivity.instance().loadUrlInWebShopFragment(strTargetURL);

        } catch (UnsupportedEncodingException e) {
            Logger.printMessage(logTag, "" + e.getMessage(), Logger.ERROR);
        }

        TabbedHybridActivity.instance().hideVirtualKeyboard();

    }

    public void clearRecentSearches(){

        ObjRecentSearchesManager.getInstance().clearPersistedData();
        updateQueryResult(null);


    }

    @Override
    public void onHiddenChanged(boolean hidden){

        if(!hidden){
            setTitleBarText();
        }
    }

    private void setTitleBarText(){

        if(TabbedHybridActivity.instance()!=null){
            Bundle data = getArguments();

            // Get the extras
            if (data.containsKey(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE)) {

                String strTitle = data.getString(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE);
                TabbedHybridActivity.instance().setBreadcrumbText(strTitle);

            }

        }


    }

}


