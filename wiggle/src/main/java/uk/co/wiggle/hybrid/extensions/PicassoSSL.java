package uk.co.wiggle.hybrid.extensions;

import android.content.Context;
import com.squareup.okhttp.HttpResponseCache;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
 
import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.GeneralSecurityException;

/**
 * 
 * @author AndroMedia
 * 
 * Picasso only efficiently cashes images if OKHttp is used - otherwise e.g. images are never 
 * received from SD card etc. 
 * 
 * This app uses HTTPUrlConnection directly or through Android's Volley library. This is not 
 * an issue as long as unsecured HTTP is used. 
 * 
 * As soon as secured HTTPS is used, OKHttp and HttpURLConnection clash when it comes to using
 * the SSLSocket simultaneously.
 * 
 * This class is a workaround to resolve this issue by passing our own OKHttpClient to Picasso 
 * which will separate all SSL calls out and avoid clashes with Volley / HttpURLConnection
 * 
 *  Instead of the standard call Picasso.with()... 
 * simply call PicassoSSL.with()... wherever Picasso is used. Any call to Picasso.with().. will 
 * cause uncaught crashes.
 *
 */

public class PicassoSSL {

	//Represent Picasso by a singleton
    private static Picasso picasso = null;
    
    public static Picasso with(Context context) {
        
    	if (picasso == null) {
        
    		OkHttpClient separateOKHttpClient = createClient();
    		
            try {
            	separateOKHttpClient.setResponseCache(createPicassoResponseCache(context));
            } catch (IOException ignored) {
            }
            
            //pass separate client to Picasso
            picasso = new Picasso.Builder(context).downloader(new OkHttpDownloader(separateOKHttpClient)).build();
        }
    	
        return picasso;
    }
    
    private static OkHttpClient createClient() {
    	
        OkHttpClient separateOKHttpClient = new OkHttpClient();
 
        SSLContext sslContext;
        
        try {
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, null, null);
            
        } catch (GeneralSecurityException e) {
            throw new AssertionError(); // No TLS available - nothing we can do here
        }
        
        separateOKHttpClient.setSslSocketFactory(sslContext.getSocketFactory());
        return separateOKHttpClient;
    }
    
    private static File createSeparatePicassoCacheDir(Context context) {
    	
        try {
            final Class<?> clazz = Class.forName("com.squareup.picasso.Utils");
            final Method method = clazz.getDeclaredMethod("createDefaultCacheDir", Context.class);
            method.setAccessible(true);
            return (File)method.invoke(null, context);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e); 
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e); 
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e); 
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e); 
        }
    }
 
    private static long getMaxDiskCacheSize(File dir) {
        try {
            final Class<?> clazz = Class.forName("com.squareup.picasso.Utils");
            final Method method = clazz.getDeclaredMethod("calculateDiskCacheSize", File.class);
            method.setAccessible(true);
            return (Long)method.invoke(null, dir);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e); 
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e); 
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e); 
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e); 
        }
    }
 
    private static HttpResponseCache createPicassoResponseCache(Context context) throws IOException {
    	
        File cacheDir = createSeparatePicassoCacheDir(context);
        long maxSize = getMaxDiskCacheSize(cacheDir);
        return new HttpResponseCache(cacheDir, maxSize);
        
    }	
	
}
