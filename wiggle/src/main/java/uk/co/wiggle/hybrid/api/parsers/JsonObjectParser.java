package uk.co.wiggle.hybrid.api.parsers;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.salesforce.marketingcloud.MarketingCloudSdk;
import com.salesforce.marketingcloud.registration.RegistrationManager;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjMegamenuManager;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjNavigationCategoryManager;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjRecentSearchesManager;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjMegamenuItem;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjUserSearch;
import uk.co.wiggle.hybrid.usecase.shop.classicnav.activities.HybridActivity;
import uk.co.wiggle.hybrid.usecase.newsfeed.activities.NewsfeedActivity;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.activities.TabbedHybridActivity;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.fragments.FragmentShopNavigation;
import uk.co.wiggle.hybrid.usecase.shop.version3.activities.V3HybridActivity;
import uk.co.wiggle.hybrid.usecase.shop.version3.fragments.V3FragmentShopNavigation;
import uk.co.wiggle.hybrid.usecase.startup.activities.StartupActivity;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler.ON_RESPONSE_ACTION;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjNewsfeedCategoryManager;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjNewsfeedManager;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjNewsfeedSportsManager;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjSalesOrderManager;
import uk.co.wiggle.hybrid.application.datamodel.objects.CustomObject;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjApiRequest;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjCustomNameValue;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjCustomerName;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNavigationCategory;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNewsfeed;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNewsfeedCategory;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesItem;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesOrder;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjTypeaheadResponse;
import uk.co.wiggle.hybrid.application.session.AppSession;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.persistence.DataStore;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.usecase.startup.activities.WelcomeActivity;


/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class JsonObjectParser {

	/* Custom parsers for API Endpoints */
	private static JsonObjectParser mInstance = null;
	private static String logTag = "JsonObjectParser";

	// JSON main tags 
	private static final String JSON_TAG_RESULTS = "Results";
	private static final String JSON_TAG_NAVIGATION = "Navigation";
	private static final String JSON_TAG_CATEGORY = "Category";
	private static final String JSON_TYPEAHEAD_TAG_RESULT_PARENT = "result";
	private static final String JSON_TYPEAHEAD_TAG_RESULT_CHILDREN = "results";
	private static final String JSON_TAG_ORDERS = "Orders";
	private static final String JSON_TAG_LINKS = "Links";
	
	private static final String JSON_TAG_RESPONSE_SUCCESS = "Success";
	private static final String JSON_TAG_RESPONSE_ERROR_REASON = "Reason";

	public static JsonObjectParser instance() {

		if (mInstance == null) {
			mInstance = new JsonObjectParser();
		}

		return mInstance;

	}
	

	/**
	 * 
	 * Check if an API-response contains server-handled errors or not.
	 * 
	 * @param apiRequest
	 * @param jsonPayload
	 * @return boolean - true if the Kombi response contained errors. False if
	 *         Kombi response did not contain an error.
	 * @throws JSONException
	 */
	public static int getErrorCode(ObjApiRequest apiRequest, JSONObject jsonPayload, VolleyError volleyError) throws JSONException {
		
		//This message translates server error messages into common HTTP error codes

		/*
			Silent login (refresh token) response in case of failure:
			 {
			  "Success": false,
			  "Reason": "NotFound"
			}
		 */
		if(AppRequestHandler.ENDPOINT_SILENT_LOGIN.equals(apiRequest.getURLEndpoint())){
			String strError = apiRequest.getResponseMessage();
			if(AppRequestHandler.ERROR_TEXT_REFRESH_TOKEN_INCORRECT.equals(strError)) {
				return AppRequestHandler.ERROR_CODE_UNAUTHORIZED;
			}
		}

		if (jsonPayload.has(JSON_TAG_RESPONSE_ERROR_REASON)) {

			// request failed - Log error, display error message if available
			String strError = apiRequest.getResponseMessage();

			if (AppRequestHandler.ERROR_TEXT_LOGIN_INCORRECT.equals(strError)
				|| AppRequestHandler.ERROR_TEXT_LOGIN_MULTIFACTOR_FAILED.equals(strError)
					|| AppRequestHandler.ERROR_TEXT_LOGIN_MFA_REQUIRED.equals(strError)
					|| AppRequestHandler.ERROR_TEXT_LOGIN_MULTIFACTOR_FAILED.equals(strError)

				) {
				return AppRequestHandler.ERROR_CODE_UNAUTHORIZED;
			}
			
			Logger.printMessage(logTag, apiRequest.getURLEndpoint() + "\n" + strError, Logger.ERROR);

			// report error response back to caller
			if (volleyError.networkResponse == null) {
				return -1; // unknown error
			}
			
			int intHTTPResponseCode = volleyError.networkResponse.statusCode;
			return intHTTPResponseCode;

		}

		return -1; // unknown error

	}

	/**
	 * 
	 * Extract API response return codes / control fields (to be used for all requests)
	 * 
	 * @param apiRequest
	 * @param jsonPayload
	 * @return void
	 * @throws JSONException
	 */	
	public void setRequestApiReturnCodes(ObjApiRequest apiRequest, JSONObject jsonPayload) {

		//store api response
		apiRequest.setApiResponse(jsonPayload);

		if (jsonPayload.has(JSON_TAG_RESPONSE_SUCCESS)) {
			
			try {
				boolean blnResponseSuccess = jsonPayload.getBoolean(JSON_TAG_RESPONSE_SUCCESS);
				apiRequest.setResponseError(!blnResponseSuccess);
			} catch (JSONException e) {
				Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - setApiResponseFields failed extracing error status due to: " + e.getMessage(), Logger.WARN);
			}

		}
		
		if (jsonPayload.has(JSON_TAG_RESPONSE_ERROR_REASON)) {
			
			try {
				String strResponseError = jsonPayload.getString(JSON_TAG_RESPONSE_ERROR_REASON);
				apiRequest.setResponseMessage(strResponseError);
			} catch (JSONException e) {
				Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - setApiResponseFields failed extracing error message due to: " + e.getMessage(), Logger.WARN);
			}

		}		

	}

	/**
	 * Parse the Kombi authentication response retrieved from
	 * DubizzleRequestHandler.ENDPOINT_AUTHENTICATE for availability of an
	 * Access token.
	 * 
	 * @param apiRequest
	 * @param jsonPayload
	 * @return String strKombiAccessToken --> the access token that can be used
	 *         for subsequent Kombi-requests
	 * @throws JSONException
	 */
	public String handleAuthenticationResponse(final ObjApiRequest apiRequest, final JSONObject jsonPayload) throws JSONException {
		
		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing start", Logger.INFO);
		
		String strApiAccessToken = jsonPayload.getString(ObjAppUser.FIELD_ACCESS_TOKEN);

		// user response contains one object only - not an array
		// create a new object and populate it with pertinent JSON values
		final ObjAppUser user = (ObjAppUser) createObjectFromJSON(jsonPayload, new ObjAppUser());

		//extract refresh token
		String strRefreshToken = jsonPayload.getString(AppRequestHandler.JSON_FIELD_REFRESH_TOKEN);
		if(strRefreshToken != null && !"".equals(strRefreshToken)){
			AppRequestHandler.instance().setRefreshToken(strRefreshToken); //saves refresh token to encrypted shared preferences
		}

		//extract customer ID
		String strCustomerId = user.getString(ObjAppUser.FIELD_CUSTOMER_ID);

		//if "CustomerId" is available - send it to SalesForce Marketing Cloud
		if(strCustomerId!=null && !"".equals(strCustomerId)){
			MarketingCloudSdk.requestSdk(new MarketingCloudSdk.WhenReadyListener() {
				@Override
				public void ready(MarketingCloudSdk marketingCloudSdk) {
					RegistrationManager.Editor registrationEditor = marketingCloudSdk.getRegistrationManager().edit();
					registrationEditor.setContactKey(strCustomerId); // ContactKey must always be unique in the Marketing Cloud Servers

					registrationEditor.commit();
				}
			});
		}
		
		// add user name information - this is delivered in a nested json object. We flatten this structure for ease of use
		String strCustomerName = jsonPayload.getString(ObjCustomerName.OBJECT_NAME);
		
		//correct handling of nested CustomerName Json object
		JSONObject customerNameJson = new JSONObject(strCustomerName);		
		final ObjCustomerName customerName = (ObjCustomerName) createObjectFromJSON(customerNameJson, new ObjCustomerName());
		user.setCustomerName(customerName);
		
		//in addition: redundant "flattening" of CustomerName elements to user object:
		List<String> fields = user.getFields();
		for(String fieldName : fields){
			if(customerNameJson.has(fieldName)){
				//we assume these are all Strings
				user.setField(fieldName, customerNameJson.getString(fieldName));
			}
		}
		
		ObjAppUser.instance().updateAppUser(user);
		
		apiRequest.setApiObject(user);
		
		//if this is a login to facilitate account actions e.g. retrieving orders, submit new request
		if(ON_RESPONSE_ACTION.RETRIEVE_ORDERS_AFTER_AUTHENTICATION.equals(apiRequest.getOnResponseAction())
				|| ON_RESPONSE_ACTION.RETRIEVE_USER_RELATED_DATA.equals(apiRequest.getOnResponseAction())
				){
			
			
			new ObjApiRequest(apiRequest.getContext(), 
					null, 
					AppRequestHandler.getOrderTrackingEndpoint(), 
					Method.GET, 
					null,
					null, 
					apiRequest.getOnResponseAction()) //forward the on_response_action that came with this request
			.submit();
			
		}
		
		
		if (apiRequest.getContext() instanceof StartupActivity) {

			StartupActivity.instance().runOnUiThread(
					new Thread(new Runnable() {
						public void run() {
							StartupActivity.instance().onApiRequestSuccess(apiRequest, null);
						}
					}));

		//for silent login from web view activity
		}else if (apiRequest.getContext() instanceof HybridActivity) {

			HybridActivity.instance().runOnUiThread(
					new Thread(new Runnable() {
						public void run() {
							HybridActivity.instance().onApiRequestSuccess(apiRequest, null);
						}
					}));

        //login (explict or silent) from FragmentUserLogin hosted by HybridActivity
		}else if(apiRequest.getContext() instanceof TabbedHybridActivity){

            TabbedHybridActivity.instance().runOnUiThread(
                    new Thread(new Runnable() {
                        public void run() {
                            TabbedHybridActivity.instance().onApiRequestSuccess(apiRequest, null);
                        }
                    }));


		}else if(apiRequest.getContext() instanceof V3HybridActivity){

			V3HybridActivity.instance().runOnUiThread(
					new Thread(new Runnable() {
						public void run() {
							V3HybridActivity.instance().onApiRequestSuccess(apiRequest, null);
						}
					}));


		}


		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing end", Logger.INFO);

		return strApiAccessToken;

	}

	/**
	 * Parse the json contained in local navigation file top_level_navigation.json
	 * 	 * 
	 * @param apiRequest
	 * @param jsonPayload
	 * @return void
	 * @throws JSONException
	 */
	public void parseLocalNavigationJson(final ObjApiRequest apiRequest, final JSONObject jsonPayload) throws JSONException {

		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing start", Logger.INFO);
		
		JSONArray navigationArray = jsonPayload.getJSONArray(AppRequestHandler.LOCAL_NAVIGATION_FILE);
		
		//we are creating an artificial root category that will store all child nodes.
		final ObjNavigationCategory rootCategory = new ObjNavigationCategory();
		rootCategory.setField(ObjNavigationCategory.FIELD_TYPE, ObjNavigationCategory.CONST_CATEGORY_TYPE_SUBMENU);
		rootCategory.setField(ObjNavigationCategory.FIELD_TITLE, ApplicationContextProvider.getContext().getString(R.string.app_name));
		
		
		List<ObjNavigationCategory> lstCategories = new ArrayList<ObjNavigationCategory>();
		
		for (int i = 0; i < navigationArray.length(); i++) {
			
			JSONObject level1MenuJson = navigationArray.getJSONObject(i);
			ObjNavigationCategory level1MenuItem = (ObjNavigationCategory) createObjectFromJSON(level1MenuJson, new ObjNavigationCategory());
			//set relational parent reference
			level1MenuItem.setParent(rootCategory);
			//set source of this item to "local navigation tree"
			level1MenuItem.setField(ObjNavigationCategory.FIELD_SOURCE, ObjNavigationCategory.CONST_CATEGORY_SOURCE_LOCAL);


			//get child menu items
			if(ObjNavigationCategory.CONST_CATEGORY_TYPE_SUBMENU.equals(level1MenuItem.getString(ObjNavigationCategory.FIELD_TYPE))){
				
				JSONArray childArray = level1MenuJson.getJSONArray(ObjNavigationCategory.JSON_LIST_CHILDREN);
				if(childArray!=null){

					for (int j = 0; j < childArray.length(); j++) {
						JSONObject level2ChildJson = childArray.getJSONObject(j);
						ObjNavigationCategory level2Child = (ObjNavigationCategory) createObjectFromJSON(level2ChildJson, new ObjNavigationCategory());
						//set relational parent reference
						level2Child.setParent(level1MenuItem);
						//set source of this item to "local navigation tree"
						level2Child.setField(ObjNavigationCategory.FIELD_SOURCE, ObjNavigationCategory.CONST_CATEGORY_SOURCE_LOCAL);

						//set availability for children accordingly
						JSONArray level2ChildAvailabilityArray = level1MenuJson.getJSONArray(ObjNavigationCategory.FIELD_AVAILABILITY);
						if(level2ChildAvailabilityArray!=null){

							for (int k = 0; k < level2ChildAvailabilityArray.length(); k++) {
								String strAvailability = level2ChildAvailabilityArray.getString(k);
								level2Child.addAvailability(strAvailability);
							}

						}

						level1MenuItem.addChild(level2Child);

						//There are some menu 1 items with children (new Gym & Outdoor categories
						//get level 2 child menu items
						if(ObjNavigationCategory.CONST_CATEGORY_TYPE_CATEGORIES.equals(level2Child.getString(ObjNavigationCategory.FIELD_TYPE))){

							if(!level2ChildJson.has(ObjNavigationCategory.JSON_LIST_CHILDREN)){
								continue;
							}

							JSONArray level3ChildArray = level2ChildJson.getJSONArray(ObjNavigationCategory.JSON_LIST_CHILDREN);
							if(level3ChildArray!=null){

								for (int k = 0; k < level3ChildArray.length(); k++) {
									JSONObject level3ChildJson = level3ChildArray.getJSONObject(k);
									ObjNavigationCategory level3Child = (ObjNavigationCategory) createObjectFromJSON(level3ChildJson, new ObjNavigationCategory());
									//set relational parent reference
									level3Child.setParent(level2Child);
									//set source of this item to "local navigation tree"
									level3Child.setField(ObjNavigationCategory.FIELD_SOURCE, ObjNavigationCategory.CONST_CATEGORY_SOURCE_LOCAL);

									//set availability for children accordingly
									JSONArray level3ChildAvailabilityArray = level3ChildJson.getJSONArray(ObjNavigationCategory.FIELD_AVAILABILITY);
									if(level3ChildAvailabilityArray!=null){

										for (int l = 0; l < level3ChildAvailabilityArray.length(); l++) {
											String strAvailability = level3ChildAvailabilityArray.getString(l);
											level3Child.addAvailability(strAvailability);
										}

									}

									level2Child.addChild(level3Child);


									//Level 4: TODO - this is repetitive .. refactor when time
									if(ObjNavigationCategory.CONST_CATEGORY_TYPE_CATEGORIES.equals(level3Child.getString(ObjNavigationCategory.FIELD_TYPE))){

										if(!level3ChildJson.has(ObjNavigationCategory.JSON_LIST_CHILDREN)){
											continue;
										}

										JSONArray level4ChildArray = level3ChildJson.getJSONArray(ObjNavigationCategory.JSON_LIST_CHILDREN);
										if(level4ChildArray!=null){

											for (int m = 0; m < level4ChildArray.length(); m++) {
												JSONObject level4ChildJson = level4ChildArray.getJSONObject(m);
												ObjNavigationCategory level4Child = (ObjNavigationCategory) createObjectFromJSON(level4ChildJson, new ObjNavigationCategory());
												//set relational parent reference
												level4Child.setParent(level3Child);
												//set source of this item to "local navigation tree"
												level4Child.setField(ObjNavigationCategory.FIELD_SOURCE, ObjNavigationCategory.CONST_CATEGORY_SOURCE_LOCAL);

												//set availability for children accordingly
												JSONArray level4ChildAvailabilityArray = level3ChildJson.getJSONArray(ObjNavigationCategory.FIELD_AVAILABILITY);
												if(level4ChildAvailabilityArray!=null){

													for (int n = 0; n < level4ChildAvailabilityArray.length(); n++) {
														String strAvailability = level4ChildAvailabilityArray.getString(n);
														level4Child.addAvailability(strAvailability);
													}

												}

												level3Child.addChild(level4Child);
											}


										}

									}

								}


							}

						}

					}


				}
				
			}
			
			//set availability for level 1 parent accordingly
			JSONArray availabilityArray = level1MenuJson.getJSONArray(ObjNavigationCategory.FIELD_AVAILABILITY);
			if(availabilityArray!=null){
				
				for (int k = 0; k < availabilityArray.length(); k++) {
					String strAvailability = availabilityArray.getString(k);
					level1MenuItem.addAvailability(strAvailability);
				}
				
			}
			
			lstCategories.add(level1MenuItem);
			
		}
		
		rootCategory.setChildren(lstCategories);
		
		ObjNavigationCategoryManager.getInstance().setLocalNavigationTree(rootCategory);
		
		//set navigation tree
		if(StartupActivity.instance()!=null){
			
		
			StartupActivity.instance().runOnUiThread(
					new Thread(new Runnable() {
						public void run() {
							//refresh menu list
							StartupActivity.instance().updateMoreFromMiggleMenu();
						}
					}));

		}
		
		if(ON_RESPONSE_ACTION.REFRESH_NAVIGATION_DRAWER.equals(apiRequest.getOnResponseAction())){

			if(apiRequest.getContext().equals(HybridActivity.instance())) {
				if (HybridActivity.instance() != null) {

					//triggered from Hybrid activity
					HybridActivity.instance().runOnUiThread(
							new Thread(new Runnable() {
								public void run() {
									//refresh menu list
									HybridActivity.instance().setCurrentNavigationCategory(ObjNavigationCategoryManager.getInstance().getCombinedNavigationTree(HybridActivity.getLogTag()), false);
								}
							}));

				}
			}else if(apiRequest.getContext().equals(TabbedHybridActivity.instance())) {
				if (TabbedHybridActivity.instance() != null) {

					//triggered from Tabbed Hybrid activity
					TabbedHybridActivity.instance().runOnUiThread(
							new Thread(new Runnable() {
								public void run() {
									//refresh menu list
									if(FragmentShopNavigation.instance()!=null
											&& FragmentShopNavigation.instance().isAdded()) {
										FragmentShopNavigation.instance().setCurrentNavigationCategory(ObjNavigationCategoryManager.getInstance().getCombinedNavigationTree(TabbedHybridActivity.getLogTag()), false);
									}
								}
							}));

				}
			}else if(apiRequest.getContext().equals(V3HybridActivity.instance())) {
				if (V3HybridActivity.instance() != null) {

					//triggered from V3Hybrid activity
					V3HybridActivity.instance().runOnUiThread(
							new Thread(new Runnable() {
								public void run() {
									//refresh menu list
									if(V3FragmentShopNavigation.instance()!=null
											&& V3FragmentShopNavigation.instance().isAdded()) {
										V3FragmentShopNavigation.instance().setCurrentNavigationCategory(ObjNavigationCategoryManager.getInstance().getCombinedNavigationTree(V3HybridActivity.getLogTag()), false);
									}
								}
							}));

				}
			}
			
		}

		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing end", Logger.INFO);
	
	}

	
	/**
	 * Parse the json file that contains the Wiggle newsfeed configuration (config-brands.json)
	 * 	 * 
	 * @param apiRequest
	 * @param jsonPayload
	 * @return void
	 * @throws JSONException
	 */
	public void parseNewsfeedConfigFile(final ObjApiRequest apiRequest, final JSONObject jsonPayload) throws JSONException {

		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing start", Logger.INFO);
		
		//write response to file so we can use it in case of no network
		DataStore.writeJSONResponseToFile(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY), apiRequest.getURLEndpoint(), jsonPayload);
		
		
		//two arrays contained in this Json - one for Newsfeed categories - and one for individual feeds
		JSONArray newsCategoryArray = jsonPayload.getJSONArray("categories");
		JSONArray newsFeedsArray = jsonPayload.getJSONArray("feeds");
		
		//parse newsfeed categories
		ArrayList<ObjNewsfeedCategory> lstCategories = new ArrayList<ObjNewsfeedCategory>();
		
		for (int i = 0; i < newsCategoryArray.length(); i++) {
			
			JSONObject categoryJson = newsCategoryArray.getJSONObject(i);
			ObjNewsfeedCategory category = (ObjNewsfeedCategory) createObjectFromJSON(categoryJson, new ObjNewsfeedCategory());
			
			lstCategories.add(category);
			
		}
		
		//set object manager
		ObjNewsfeedCategoryManager.getInstance().set(lstCategories);
		
		
		//parse Feeds
		List<ObjNewsfeed> lstFeeds = new ArrayList<ObjNewsfeed>();
		
		for (int i = 0; i < newsFeedsArray.length(); i++) {
			
			JSONObject feedJson = newsFeedsArray.getJSONObject(i);
			ObjNewsfeed feed = (ObjNewsfeed) createObjectFromJSON(feedJson, new ObjNewsfeed());
			
			//get embedded array of categories
			JSONArray feedCategoriesArray = feedJson.getJSONArray("categories");
			
			ArrayList<ObjNewsfeedCategory> lstFeedCategories = new ArrayList<ObjNewsfeedCategory>();
			
			if(feedCategoriesArray!=null){
				
				for(int j=0; j<feedCategoriesArray.length();j++){
					
					//get category Id
					int intCategoryId = feedCategoriesArray.getInt(j);
					
					//get category object from manager
					ObjNewsfeedCategory feedCategory = ObjNewsfeedCategoryManager.getInstance().getRecordByUniqueId(intCategoryId);
					lstFeedCategories.add(feedCategory);
				}
				
				//add feed categories to newsfeed
				feed.setCategories(lstFeedCategories);
				
			}
			

			lstFeeds.add(feed);
			
		}

		//set object manager
		ObjNewsfeedManager.getInstance().set(lstFeeds);
		
		//report parsing end to Newsfeed activity
		if(NewsfeedActivity.instance()!=null){
			
		
			NewsfeedActivity.instance().runOnUiThread(
					new Thread(new Runnable() {
						public void run() {
							//refresh menu list
							NewsfeedActivity.instance().onApiRequestSuccess(apiRequest, null);
						}
					}));

		}
		
		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing end", Logger.INFO);
	
	}
	
	
	/**
	 * Parse the json file that contains the Wiggle newsfeed configuration (config.json)
	 * 	 * 
	 * @param apiRequest
	 * @param jsonPayload
	 * @return void
	 * @throws JSONException
	 */
	public void parseNewsfeedSportsConfigFile(final ObjApiRequest apiRequest, final JSONObject jsonPayload) throws JSONException {

		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing start", Logger.INFO);
		
		//write response to file so we can use it in case of no network
		DataStore.writeJSONResponseToFile(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY), apiRequest.getURLEndpoint(), jsonPayload);
		
		
		//two arrays contained in this Json - one for Newsfeed sports - and one for individual feeds
		JSONArray newsCategoryArray = jsonPayload.getJSONArray("categories");
		
		//parse newsfeed categories
		ArrayList<ObjNewsfeedCategory> lstCategories = new ArrayList<ObjNewsfeedCategory>();
		
		for (int i = 0; i < newsCategoryArray.length(); i++) {
			
			JSONObject categoryJson = newsCategoryArray.getJSONObject(i);
			ObjNewsfeedCategory category = (ObjNewsfeedCategory) createObjectFromJSON(categoryJson, new ObjNewsfeedCategory());
			
			lstCategories.add(category);
			
		}
		
		//set object manager
		ObjNewsfeedSportsManager.getInstance().set(lstCategories);
		
		
		//report parsing end to Newsfeed activity
		if(NewsfeedActivity.instance()!=null){
			
		
			NewsfeedActivity.instance().runOnUiThread(
					new Thread(new Runnable() {
						public void run() {
							//refresh menu list
							NewsfeedActivity.instance().onApiRequestSuccess(apiRequest, null); 
						}
					}));

		}
		
		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing end", Logger.INFO);
	
	}


	/**
	 * Parse the json file that contains the Version upgrade information
	 * 	 *
	 * @param apiRequest
	 * @param jsonPayload
	 * @return void
	 * @throws JSONException
	 */
	public void parseUpgradeCheckResponse(final ObjApiRequest apiRequest, final JSONObject jsonPayload) throws JSONException {

		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing start", Logger.INFO);
		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - payload:" + jsonPayload.toString(), Logger.INFO);

		//Simple json object - example format:
		//{"ios":{"minVer":"2.8.3","forced":"true"},"android":{"minVer":"2.6.7","forced":"true"}}
		JSONObject androidVersionJson = jsonPayload.getJSONObject("android");
		String strRequiredMinimumAppVersion = androidVersionJson.getString("minVer");
		String strCurrentAppVersion = AppUtils.getAppVersionNumber();
		String strForcedVersion = androidVersionJson.getString("forced");
		boolean blnForcedVersion = "true".equals(strForcedVersion);

		//force upgrade necessary - set corresponding onResponseAction
		if(AppUtils.isCurrentAppOutdated(strCurrentAppVersion, strRequiredMinimumAppVersion)) {
			if(blnForcedVersion) {
				Logger.printMessage(logTag, "VERSION-CHECK: App Outdated and requires an upgrade", Logger.INFO);
				apiRequest.setOnResponseAction(ON_RESPONSE_ACTION.SHOW_UPGRADE_REQUIRED_DIALOG);
			}
		}

		//notify completion of this api call irrespective of result
		if(V3HybridActivity.instance()!=null){


			V3HybridActivity.instance().runOnUiThread(
					new Thread(new Runnable() {
						public void run() {
							//refresh menu list
							V3HybridActivity.instance().onApiRequestSuccess(apiRequest, null);
						}
					}));

		}else if(WelcomeActivity.instance()!=null){


			WelcomeActivity.instance().runOnUiThread(
					new Thread(new Runnable() {
						public void run() {
							//refresh menu list
							WelcomeActivity.instance().onApiRequestSuccess(apiRequest, null);
						}
					}));

		}

		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing end", Logger.INFO);

	}



	/**
	 * Parse the json contained in local navigation file top_level_navigation.json
	 * 	 * 
	 * @param apiRequest
	 * @param jsonPayload
	 * @return void
	 * @throws JSONException
	 */
	public void handleCategoryResponse(final ObjApiRequest apiRequest, final JSONObject jsonPayload, boolean isCachedData) throws JSONException {

		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing start", Logger.INFO);
		
		final ObjNavigationCategory parentCategory = (ObjNavigationCategory) apiRequest.getApiObject();
		
		JSONObject resultsJson = jsonPayload.getJSONObject(JSON_TAG_RESULTS);
		JSONObject navigationJson = resultsJson.getJSONObject(JSON_TAG_NAVIGATION);
		JSONObject categoryJson = navigationJson.getJSONObject(JSON_TAG_CATEGORY);
		JSONArray refinementArray = categoryJson.getJSONArray(ObjNavigationCategory.JSON_LIST_REFINEMENTS);
		
		List<ObjNavigationCategory> lstChildCategories = new ArrayList<ObjNavigationCategory>();
		
		for (int i = 0; i < refinementArray.length(); i++) {
			
			JSONObject refinementJson = refinementArray.getJSONObject(i);
			ObjNavigationCategory refinementItem = (ObjNavigationCategory) createObjectFromJSON(refinementJson, new ObjNavigationCategory());
			
			//flag these categories as Wiggle API categories
			refinementItem.setField(ObjNavigationCategory.FIELD_TYPE, ObjNavigationCategory.CONST_CATEGORY_TYPE_WIGGLE_API);
			
			//set relational parent reference
			refinementItem.setParent(parentCategory);
			
			lstChildCategories.add(refinementItem);
			
		}
		
		//add all refinements as a child to current category
		parentCategory.setChildren(lstChildCategories);
		parentCategory.setChildrenRetrievedFromServer(true); //used to indicate that we tried to get categories from the server if list size is 0 (no more children)
		
		if(apiRequest.getContext().equals(HybridActivity.instance())){

			if(HybridActivity.instance()!=null){


				HybridActivity.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								//refresh menu list
								HybridActivity.instance().setCurrentNavigationCategory(parentCategory, false);
							}
						}));

			}

		}else if(apiRequest.getContext().equals(TabbedHybridActivity.instance())) {

			if (TabbedHybridActivity.instance() != null) {


				TabbedHybridActivity.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								//refresh menu list
								if(FragmentShopNavigation.instance()!=null
										&& FragmentShopNavigation.instance().isAdded()) {
									FragmentShopNavigation.instance().setCurrentNavigationCategory(parentCategory, false);
								}
							}
						}));

			}
		}else if(apiRequest.getContext().equals(V3HybridActivity.instance())) {

			if (V3HybridActivity.instance() != null) {


				V3HybridActivity.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								//refresh menu list
								if(V3FragmentShopNavigation.instance()!=null
										&& V3FragmentShopNavigation.instance().isAdded()) {
									V3FragmentShopNavigation.instance().setCurrentNavigationCategory(parentCategory, false);
								}
							}
						}));

			}
		}

		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing end", Logger.INFO);
	
	}

	/**
	 * Parse the json returned by the megamenu endpoint
	 * 	 *
	 * @param apiRequest
	 * @param jsonPayload
	 * @return void
	 * @throws JSONException
	 */
	public void parseMegamenuResponse(final ObjApiRequest apiRequest, final JSONObject jsonPayload) throws JSONException {

		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing start", Logger.INFO);

		final ObjNavigationCategory parentCategory = (ObjNavigationCategory) apiRequest.getApiObject();

		JSONArray resultsJson = jsonPayload.getJSONArray(JSON_TAG_LINKS);

		List<ObjNavigationCategory> lstChildCategories = new ArrayList<ObjNavigationCategory>();

		for (int i = 0; i < resultsJson.length(); i++) {

			JSONObject menuItemJson = resultsJson.getJSONObject(i);
			ObjNavigationCategory megamenuItem = (ObjNavigationCategory) createObjectFromJSON(menuItemJson, new ObjNavigationCategory());

			//flag these categories as Wiggle API categories
			megamenuItem.setField(ObjNavigationCategory.FIELD_TYPE, ObjNavigationCategory.CONST_CATEGORY_TYPE_MEGAMENU_API);

			//set relational parent reference
			megamenuItem.setParent(parentCategory);

			lstChildCategories.add(megamenuItem);

		}

		//add all refinements as a child to current category
		parentCategory.setChildren(lstChildCategories);
		parentCategory.setChildrenRetrievedFromServer(true); //used to indicate that we tried to get categories from the server if list size is 0 (no more children)


		if(apiRequest.getContext().equals(V3HybridActivity.instance())) {

			if (V3HybridActivity.instance() != null) {


				V3HybridActivity.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								//refresh menu list
								if(V3FragmentShopNavigation.instance()!=null
										&& V3FragmentShopNavigation.instance().isAdded()) {
									V3FragmentShopNavigation.instance().setCurrentNavigationCategory(parentCategory, false);
									//AppUtils.showCustomToast("Megamenu contains " + ObjMegamenuManager.getInstance().getAll().size() + " items", false);
								}
							}
						}));

			}
		}

		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing end", Logger.INFO);

	}


	/**
	 * Parse the json returned by the megamenu endpoint
	 * 	 *
	 * @param apiRequest
	 * @param jsonPayload
	 * @return void
	 * @throws JSONException
	 */
	public void parseMegamenuResponseV2(final ObjApiRequest apiRequest, final JSONObject jsonPayload) throws JSONException {

		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing start", Logger.INFO);

		final ObjMegamenuItem callingItem = (ObjMegamenuItem) apiRequest.getApiObject();

		JSONArray resultsJson = jsonPayload.getJSONArray(JSON_TAG_LINKS);

		List<ObjMegamenuItem> lstChildCategories = new ArrayList<ObjMegamenuItem>();

		for (int i = 0; i < resultsJson.length(); i++) {

			JSONObject menuItemJson = resultsJson.getJSONObject(i);
			ObjMegamenuItem megamenuItem = (ObjMegamenuItem) createObjectFromJSON(menuItemJson, new ObjMegamenuItem());

			//set country code field
			megamenuItem.setField(ObjMegamenuItem.FIELD_COUNTRY_CODE, callingItem.getInt(ObjMegamenuItem.FIELD_COUNTRY_CODE));
			ObjMegamenuManager.getInstance().addIfMissing(megamenuItem);

		}

		if(apiRequest.getContext().equals(V3HybridActivity.instance())) {

			if (V3HybridActivity.instance() != null) {


				V3HybridActivity.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								//refresh menu list
								if(V3FragmentShopNavigation.instance()!=null
										&& V3FragmentShopNavigation.instance().isAdded()) {
									//V3FragmentShopNavigation.instance().setCurrentNavigationCategory(parentCategory, false);
									AppUtils.showCustomToast("Megamenu contains " + ObjMegamenuManager.getInstance().getAll().size() + " items", false);
								}
							}
						}));

			}
		}

		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing end", Logger.INFO);

	}
		
	
	/**
	 * Parse response from Typeahead endpoint
	 * 	 * 
	 * @param apiRequest
	 * @param jsonPayload
	 * @return void
	 * @throws JSONException
	 */
	public void parseTypeaheadResponse(final ObjApiRequest apiRequest, final JSONObject jsonPayload) throws JSONException {

		final List<ObjTypeaheadResponse> lstTypeaheadResponse = new ArrayList<ObjTypeaheadResponse>();
		
		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing start", Logger.INFO);
		
		JSONArray resultArray = jsonPayload.getJSONArray(JSON_TYPEAHEAD_TAG_RESULT_PARENT);
		
		//only parse if there is a result - prevent JSON exception
		if(resultArray!=null){
			
			for (int i = 0; i < resultArray.length(); i++) {
				
				JSONObject parentJSON = resultArray.getJSONObject(i);
				ObjTypeaheadResponse parentRecord = (ObjTypeaheadResponse) createObjectFromJSON(parentJSON, new ObjTypeaheadResponse());
				//flag this record as parent
				parentRecord.setField(ObjTypeaheadResponse.FIELD_DISPLAY_TYPE, ObjTypeaheadResponse.CONST_DISPLAY_TYPE_PARENT);
				lstTypeaheadResponse.add(parentRecord);
				
				//get all children for this record
				JSONArray childrenArray = parentJSON.getJSONArray(JSON_TYPEAHEAD_TAG_RESULT_CHILDREN);
				
				for (int j = 0; j < childrenArray.length(); j++) {
					
					JSONObject childJSON = childrenArray.getJSONObject(j);
					ObjTypeaheadResponse childRecord = (ObjTypeaheadResponse) createObjectFromJSON(childJSON, new ObjTypeaheadResponse());
					//flag this record as child
					childRecord.setField(ObjTypeaheadResponse.FIELD_DISPLAY_TYPE, ObjTypeaheadResponse.CONST_DISPLAY_TYPE_CHILD);
					childRecord.setParentRecord(parentRecord); //add parent for reference
					
					lstTypeaheadResponse.add(childRecord);				
					
				}
				
				
			}
			
		}

		if(apiRequest.getContext().equals(HybridActivity.instance())) {

			if (HybridActivity.instance() != null) {

				HybridActivity.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								//refresh menu list
								HybridActivity.instance().updateQueryResult(lstTypeaheadResponse);
							}
						}));

			}

		}else if(apiRequest.getContext().equals(TabbedHybridActivity.instance())) {

			if (TabbedHybridActivity.instance() != null) {

				TabbedHybridActivity.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								//refresh menu list
								TabbedHybridActivity.instance().updateQueryResult(lstTypeaheadResponse);
							}
						}));

			}
		}else if(apiRequest.getContext().equals(V3HybridActivity.instance())) {

			if (V3HybridActivity.instance() != null) {

				V3HybridActivity.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								//refresh menu list
								V3HybridActivity.instance().updateQueryResult(lstTypeaheadResponse);
							}
						}));

			}
		}
		
		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing end", Logger.INFO);
	
	}
	

	
	/**
	 * Parse response from OrderTracking endpoint
	 * 	 * 
	 * @param apiRequest
	 * @param jsonPayload
	 * @return void
	 * @throws JSONException
	 */
	public void parseOrderTrackingResponse(final ObjApiRequest apiRequest, final JSONObject jsonPayload, final boolean isCachedData) throws JSONException {

		Logger.printMessage(logTag, "Orders: " + jsonPayload.toString(), Logger.ALL);
		
		//save received response to data store for later use / comparison with new data
		if(!isCachedData){
			//We are not caching the response as we receive it - we are caching it only when the user actually views the order tracking data
			//DataStore.writeJSONResponseToFile(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY), apiRequest.getURLEndpoint(), jsonPayload);
			AppSession.instance().setOrderTrackingPayload(jsonPayload, apiRequest.getURLEndpoint());
		}
		
		//AppUtils.showCustomToast(jsonPayload.toString(), true);
		
		final List<ObjSalesOrder> lstOrders = new ArrayList<ObjSalesOrder>();
		
		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing start", Logger.INFO);
		
		JSONArray resultArray = jsonPayload.getJSONArray(JSON_TAG_ORDERS);
		
		//only parse if there is a result - prevent JSON exception
		if(resultArray!=null){
			
			for (int i = 0; i < resultArray.length(); i++) {
				
				JSONObject parentJSON = resultArray.getJSONObject(i);
		
				ObjSalesOrder parentRecord = (ObjSalesOrder) createObjectFromJSON(parentJSON, new ObjSalesOrder());
				
				lstOrders.add(parentRecord);
				
				//get all children for this record
				JSONArray childrenArray = parentJSON.getJSONArray(ObjSalesOrder.JSON_LIST_ITEMS);
				
				List<ObjSalesItem> lstSalesItems = new ArrayList<ObjSalesItem>();
				
				for (int j = 0; j < childrenArray.length(); j++) {
					
					JSONObject childJSON = childrenArray.getJSONObject(j);
					ObjSalesItem childRecord = (ObjSalesItem) createObjectFromJSON(childJSON, new ObjSalesItem());
					//flag this record as child
					childRecord.setParentRecord(parentRecord); //add parent for reference
					
					//add array index (used for sorting internally)
					childRecord.setField(ObjSalesItem.FIELD_JSON_ARRAY_INDEX, j);
					
					lstSalesItems.add(childRecord);				
					
				}
				
				parentRecord.setSalesItems(lstSalesItems);
				
				
			}
			
		}
		

		Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing end", Logger.INFO);
		
		if(ON_RESPONSE_ACTION.SUBMIT_ORDER_TRACKING_REQUEST.equals(apiRequest.getOnResponseAction())){
			
			//populate order manager with cached orders
			ObjSalesOrderManager.getInstance().set(lstOrders);
			AppUtils.showCustomToast("Orders from cache: " + ObjSalesOrderManager.getInstance().size(), true);
			
			//Now: get server status of orders.
			//Submit a request to the Order tracking endpoint; change on response action to handle authentication if required
			apiRequest.setOnResponseAction(ON_RESPONSE_ACTION.RETRIEVE_ORDERS_AFTER_AUTHENTICATION);
			apiRequest.submit();
			
			
		}else{
			
			//compare new orders with order manager and flag updated orders where required
			ObjSalesOrderManager.getInstance().updateOrderManager(lstOrders);
			AppUtils.showCustomToast("Orders from API: " + ObjSalesOrderManager.getInstance().size(), true);
			
			
			if (apiRequest.getContext() instanceof StartupActivity) {

				if(StartupActivity.instance()!=null){
					
					StartupActivity.instance().runOnUiThread(
							new Thread(new Runnable() {
								public void run() {
									StartupActivity.instance().onApiRequestSuccess(apiRequest, null);
								}
							}));
					
					
				}


			//for silent login from web view activity
			}else if (apiRequest.getContext() instanceof HybridActivity) {

				if(HybridActivity.instance()!=null){
					
					HybridActivity.instance().runOnUiThread(
							new Thread(new Runnable() {
								public void run() {
									HybridActivity.instance().onApiRequestSuccess(apiRequest, null);
								}
							}));
		
				}
			}else if (apiRequest.getContext() instanceof TabbedHybridActivity) {

				if(TabbedHybridActivity.instance()!=null){

					TabbedHybridActivity.instance().runOnUiThread(
							new Thread(new Runnable() {
								public void run() {
									TabbedHybridActivity.instance().onApiRequestSuccess(apiRequest, null);
								}
							}));

				}
			}else if (apiRequest.getContext() instanceof V3HybridActivity) {

				if(V3HybridActivity.instance()!=null){

					V3HybridActivity.instance().runOnUiThread(
							new Thread(new Runnable() {
								public void run() {
									V3HybridActivity.instance().onApiRequestSuccess(apiRequest, null);
								}
							}));

				}
			}

					
		}

	
	}



	
	public ObjAppUser createUserFromJSON(String json) throws JSONException{
		
		ObjAppUser user = null;
		
		JSONArray dataArray = new JSONArray(json);
		
		for (int i = 0; i < dataArray.length(); i++) {
			
			JSONObject userJSON = dataArray.getJSONObject(i);
			
			user = (ObjAppUser)createObjectFromJSON(userJSON, new ObjAppUser());
		
		}
		
		//set API access token if needed
		if("".equals(AppRequestHandler.instance().getApiAccessToken())){
			//AppRequestHandler.instance().setApiAccessToken(ObjAppUser.instance().getString(ObjAppUser.FIELD_ACCESS_TOKEN));
		}
		
		return user;
		
	}




	public void parseRecentSearchesJsonArray(String strJsonArray) throws JSONException{

		ArrayList<ObjUserSearch> persistedSearches = new ArrayList<ObjUserSearch>();

		//Data comes in a simple Json array as follows
		// [
		//		{"search_string":"Wiggle bike","selected_date":"2016-11-06 11:08:28"}
		//     ,{"search_string":"Swimming goggles","selected_date":"2016-11-06 11:07:42"}
		// ]
		//so we need to extract the "mMap" information and create an ObjUserSearch with this
		JSONArray dataArray = new JSONArray(strJsonArray);

		for (int i = 0; i < dataArray.length(); i++) {

			JSONObject recentSearchJson = dataArray.getJSONObject(i);

			ObjUserSearch recentSearch = (ObjUserSearch)createObjectFromJSON(recentSearchJson, new ObjUserSearch());
			persistedSearches.add(recentSearch);

		}

		ObjRecentSearchesManager.getInstance().set(persistedSearches);

		if(AppUtils.getWiggleVersion().equals(AppUtils.APP_VERSION.VERSION_2_TABBED_NAV)) {
			TabbedHybridActivity.instance().updateQueryResult(new ArrayList<ObjTypeaheadResponse>());
		}else if(AppUtils.getWiggleVersion().equals(AppUtils.APP_VERSION.VERSION_3_APP_WIDE_SEARCH)) {
			V3HybridActivity.instance().updateQueryResult(new ArrayList<ObjTypeaheadResponse>());
		}

	}



	
	/**
	 * 
	 * This parser reads the App configuration response
	 * 
	 * @param apiRequest
	 * @param jsonPayload
	 * @throws JSONException
	 * 
	 */
	public void handleAppConfigurationResponse(final ObjApiRequest apiRequest,
			JSONObject jsonPayload, boolean isCachedData) throws JSONException {

		
		/**
			
			if(!isCachedData){
				// data was received - store response data in cache file (Note: LeadR app only uses cached data when there is no network connection)
				DataStore.writeJSONResponseToFile("", 
						apiRequest.getURLEndpoint(),
						jsonPayload);
			}
			
			Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing start", Logger.INFO);

			String strData = jsonPayload.getString(JSON_TAG_LEVEL_1_DATA);
			// user response contains one object only - not an array
			JSONObject appConfigJSON = new JSONObject(strData);			


 
			// create a new object and populate it with pertinent JSON values
			final ObjLeadrAppConfig appConfig = (ObjLeadrAppConfig) createObjectFromJSON(appConfigJSON, new ObjLeadrAppConfig());
			
			
			// add cards to app configuration to user
			if(appConfigJSON.has(ObjLeadrDataCaptureCard.OBJECT_NAME)){
			
				JSONArray cards = appConfigJSON.getJSONArray(ObjLeadrDataCaptureCard.OBJECT_NAME);
				
				for(int i=0;i<cards.length();i++){
					
					JSONObject cardJson = cards.getJSONObject(i);
					ObjLeadrDataCaptureCard card = (ObjLeadrDataCaptureCard) createObjectFromJSON(cardJson, new ObjLeadrDataCaptureCard());
					appConfig.addDataCaptureCard(card);
					
				}
				
			}
			
			ObjLeadrApp app = (ObjLeadrApp) apiRequest.getApiObject();
			app.setConfiguration(appConfig);
			
			
			if (apiRequest.getContext() instanceof MainActivity) {

				MainActivity.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								MainActivity.instance().onApiRequestSuccess(apiRequest, null);
							}
						}));

			}

			Logger.printMessage(logTag, apiRequest.getURLEndpoint() + " - parsing end", Logger.INFO);
			
		
**/			



	}	

	/**
	 * Parse the Kombi response retrieved from
	 * DubizzleRequestHandler.ENDPOINT_USER_LOGOUT
	 * 
	 * @param apiRequest
	 * @param jsonPayload
	 * @return void
	 * @throws JSONException
	 */

	public void handleUserLogoutResponse(ObjApiRequest apiRequest,
			JSONObject jsonPayload) throws JSONException {

		// TODO: ...
		if (apiRequest.getContext() instanceof HybridActivity) {

			HybridActivity.instance().runOnUiThread(new Thread(new Runnable() {
				public void run() {
					//MainActivity.instance().onLogoutConfirmed();
				}
			}));

		}

	}

	/**
	 * Parse the Kombi response retrieved from
	 * DubizzleRequestHandler.ENDPOINT_FORGOT_PASSWORD
	 * 
	 * @param apiRequest
	 * @param jsonPayload
	 * @return void
	 * @throws JSONException
	 */

	public void handleForgotPasswordResponse(
			final ObjApiRequest apiRequest, final JSONObject jsonPayload)
			throws JSONException {

		// TODO: ...
		/*
		 * 05-29 20:38:30.628: I/DubizzleRequestHandler(5636):
		 * {"data":{"data":"Email sent with reset password link"
		 * ,"meta":{},"paging":{},"facets":{}}}
		 */
		
		/**
		if (apiRequest.getContext() instanceof ResetPassword) {

			ResetPassword.instance().runOnUiThread(new Thread(new Runnable() {
				public void run() {
					ResetPassword.instance().onApiRequestSuccess(apiRequest, jsonPayload);
				}
			}));

		}
		**/

	}

	
	public void handleRegistrationResponse(ObjApiRequest apiRequest,
			JSONObject jsonPayload, boolean isCachedData) {
		// TODO Auto-generated method stub
		
	}
	
	
	/**  enables using createObjectFromJSON with Json strings as opposed to Json object **/
	public CustomObject createObjectFromJsonString(String strJsonString, CustomObject targetObject){
		
		try {
			JSONObject jsonObject = new JSONObject(strJsonString);
			return createObjectFromJSON(jsonObject, targetObject);
		} catch (JSONException e) {
			Logger.printMessage(logTag,"createObjectFromJsonString " + e.getMessage(), Logger.ERROR);
		}	
		
		return null;
		
	}
	

	/**
	 * 
	 * Kombi objects have flat attributes - use this method to extract all flat
	 * attributes from JSON and assign them to the target Kombi object
	 * 
	 * @param incomingJSONObject
	 * @param targetObject
	 * @return DubizzleKombiObject targetObject --> the Java object created from
	 *         the JSON object
	 * @throws JSONException
	 * 
	 */
	private CustomObject createObjectFromJSON(
			JSONObject incomingJSONObject, CustomObject targetObject)
			throws JSONException {

		List<String> fields = targetObject.getFields();

		for (String strFieldName : fields) {

			try {
				if (incomingJSONObject.has(strFieldName)) {

					// get value depending on data type
					int intValueFieldType = targetObject
							.getFieldType(strFieldName);

					switch (intValueFieldType) {
					case CustomObject.FIELD_TYPE_OBJECT:
						break;
					case CustomObject.FIELD_TYPE_DATETIME:
						break;
					case CustomObject.FIELD_TYPE_STRING:
						
						targetObject.setField(strFieldName,
								incomingJSONObject.getString(strFieldName));
						
						break;
					case CustomObject.FIELD_TYPE_INTEGER:
						
						try {
							// try proper integer first
							targetObject.setField(strFieldName,
									incomingJSONObject.getInt(strFieldName));
						} catch (JSONException e) {
							
							// if not working try string
							String value = incomingJSONObject
									.getString(strFieldName);
							// cast the value to string, since the data type in model is string
							targetObject.setField(strFieldName,Integer.parseInt(value));
									
						}
						
						break;
					case CustomObject.FIELD_TYPE_LONG:
						
						try {
							// try proper long first
							targetObject.setField(strFieldName,
									incomingJSONObject.getLong(strFieldName));
						} catch (JSONException e) {
							
							// if not working try string
							String value = incomingJSONObject
									.getString(strFieldName);
							// cast the value to string, since the data type in model is string
							targetObject.setField(strFieldName,Long.parseLong(value));
									
						}
						
						break;
					case CustomObject.FIELD_TYPE_BOOLEAN:
						// The dubizzle API sometimes returns booleans as
						// Integers and at other times at boolean or strings
						// (see e.g. display_me on listings detail (integer) and
						// listing detail (boolean)
						// Hence try (and fail) different approaches to extract
						// the boolean value from JSON
						try {
							// try proper boolean first
							targetObject
									.setField(strFieldName, incomingJSONObject
											.getBoolean(strFieldName));
						} catch (JSONException e) {

							// if not working, try integer
							try {
								targetObject
										.setField(strFieldName,
												incomingJSONObject
														.getInt(strFieldName));
							} catch (JSONException f) {

								// if not working try String - no try/catch as
								// in this case Exception should be reported
								// back and handled by caller
								// (DubizzleRequestHandler)
								targetObject.setField(strFieldName,
										incomingJSONObject
												.getString(strFieldName));
							}

						}

						break;
					case CustomObject.FIELD_TYPE_DOUBLE:
						targetObject.setField(strFieldName,
								incomingJSONObject.getDouble(strFieldName));
						break;
					default:
						Logger.printMessage(logTag,
								"createNameValueList: unsupported field type "
										+ intValueFieldType, Logger.ERROR);
						break;
					}

				}
			} catch (Exception e) {
				Logger.printMessage(
						targetObject.getObjectName(),
						"Error setting field " + strFieldName + " due to "
								+ e.getMessage(), Logger.ERROR);
			}

		}

		return targetObject;

	}

	/**
	 * 
	 * Kombi objects have flat attributes and can have name-value lists
	 * associated to them (e.g. [language name, display text] or [x, longitude]
	 * etc) Use this method to build a name-value list of the appropriate data
	 * type from a given JSON array of name-value information.
	 * 
	 * @param incomingJSONObject
	 *            --> the JSON object containing the Kombi object
	 * @param strJSONArrayTag
	 *            --> the JSON tag of the name-value array. This is used to
	 *            extract the desired name-value array from the Json object
	 * @param intValueFieldType
	 *            --> the "name" is always assumed to be a String. The "value"
	 *            can have different data-types - specify the supported datatype
	 *            as available in DubizzleKombiObject
	 * @return List<ObjKombiNameValue> nameValueList - a list of
	 *         ObjKombiNameValue that can be added to the Kombi object
	 * @throws JSONException
	 * 
	 */
	private List<ObjCustomNameValue> createNameValueList(
			JSONObject incomingJSONObject, String strJSONArrayTag,
			int intValueFieldType) throws JSONException {

		List<ObjCustomNameValue> nameValueList = new ArrayList<ObjCustomNameValue>();

		JSONObject jsonNameValueList = incomingJSONObject
				.getJSONObject(strJSONArrayTag);

		// this is an name-value array - loop through it and add items as they
		// appear
		Iterator iter = jsonNameValueList.keys();
		while (iter.hasNext()) {

			ObjCustomNameValue nameValuePair = new ObjCustomNameValue();

			// set name information
			String key = (String) iter.next();
			nameValuePair.setField(ObjCustomNameValue.FIELD_NAME, key);

			// get value depending on data type
			switch (intValueFieldType) {
			case CustomObject.FIELD_TYPE_OBJECT:
				break;
			case CustomObject.FIELD_TYPE_DATETIME:
				break;
			case CustomObject.FIELD_TYPE_STRING:
				String strValue = jsonNameValueList.getString(key);
				nameValuePair.setField(ObjCustomNameValue.FIELD_VALUE_STRING,
						strValue);
				break;
			case CustomObject.FIELD_TYPE_INTEGER:
				int intValue = jsonNameValueList.getInt(key);
				nameValuePair.setField(ObjCustomNameValue.FIELD_VALUE_INT,
						intValue);
				break;
	
			case CustomObject.FIELD_TYPE_BOOLEAN:
				// The dubizzle API sometimes returns booleans as Integers and
				// at other times at boolean or strings
				// (see e.g. display_me on listings detail (integer) and listing
				// detail (boolean)
				// Hence try (and fail) different approaches to extract the
				// boolean value from JSON
				try {
					// try proper boolean first
					boolean blnValue = jsonNameValueList.getBoolean(key);
					nameValuePair.setField(
							ObjCustomNameValue.FIELD_VALUE_BOOLEAN, blnValue);
				} catch (JSONException e) {

					// if not working, try integer
					try {
						int intBlnValue = jsonNameValueList.getInt(key);
						nameValuePair.setField(
								ObjCustomNameValue.FIELD_VALUE_BOOLEAN,
								intBlnValue);
					} catch (JSONException f) {

						// if not working try String - no try/catch as in this
						// case Exception should be reported back and handled by
						// caller (DubizzleRequestHandler)
						String strBlnValue = jsonNameValueList.getString(key);
						nameValuePair.setField(
								ObjCustomNameValue.FIELD_VALUE_BOOLEAN,
								strBlnValue);

					}

				}

				break;
			case CustomObject.FIELD_TYPE_DOUBLE:
				double dblValue = jsonNameValueList.getDouble(key);
				nameValuePair.setField(ObjCustomNameValue.FIELD_VALUE_DOUBLE,
						dblValue);
				break;
			default:
				Logger.printMessage(logTag,
						"createNameValueList: unsupported field type "
								+ intValueFieldType, Logger.ERROR);
				break;
			}

			nameValueList.add(nameValuePair);

		}

		return nameValueList;

	}

	/**
	 * 
	 * The Kombi API returns some nested entities as a key - entrySet map e.g.
	 * {"coordinates"
	 * :{"y":25.2813877882,"x":55.3452134148},"breadcrumb":{"ar":["UAE"
	 * ,"Dubai","Hor Al Anz"],"en":["UAE","Dubai","Hor Al Anz"]}}
	 * 
	 * Level 1: "breadcrumb" (the name of the map) Level 2: "ar" (the key for
	 * Arabic language entries) Level 3: xxxxx (an Arabic String value) Level 3:
	 * yyyyy (an Arabic String value) Level 2: "en" (the key for English
	 * language entries) Level 3: xxxxx (an English String value) Level 3: yyyyy
	 * (an English String value)
	 * 
	 * This method uses this information and turns it into a HashMap<String,
	 * List<String>>() that can be stored against any DubizzleKombi object
	 * Please note that we only support String entries for the entry set.
	 * 
	 * @param incomingJSONObject
	 * @param strJSONArrayTag
	 * @return the created Map<String, List<String>> keyEntrySetMap
	 * @throws JSONException
	 * 
	 */
	private Map<String, List<String>> createKeyStringListMap(
			JSONObject incomingJSONObject, String strJSONArrayTag)
			throws JSONException {

		Map<String, List<String>> keyEntrySetMap = new HashMap<String, List<String>>();

		JSONObject jsonKeyEntrySet = incomingJSONObject
				.getJSONObject(strJSONArrayTag);

		// loop through all keys of this object (currently only "ar" and "en" -
		// but make this future proof for future languages)
		Iterator<?> keys = jsonKeyEntrySet.keys();

		while (keys.hasNext()) {

			String key = (String) keys.next();

			if (jsonKeyEntrySet.get(key) instanceof JSONArray) {

				String strEntrySet = jsonKeyEntrySet.get(key).toString();
				JSONArray entrySetArray = new JSONArray(strEntrySet);

				List<String> entrySetList = new ArrayList<String>(); // used to
																		// store
																		// the
																		// values
																		// available
																		// in
																		// the
																		// JSON
																		// Array

				// iterate through json array, extract json objects if available
				// and add to corresponding data object manager
				for (int i = 0; i < entrySetArray.length(); i++) {
					String strValue = entrySetArray.getString(i);
					entrySetList.add(strValue);
				}

				// after all done - add the hash map entry for this String,
				// List<String> combination
				keyEntrySetMap.put(key, entrySetList);

			}
		}

		return keyEntrySetMap;

	}

	/**
	 * 
	 * The Kombi API returns some nested entities as a key - entrySet map with
	 * nested name-value pairs e.g. "details": {"important": {"ar": [["Year",
	 * "2011"], ["Kilometers", "14500"]], "en": [["Year", "2011"],
	 * ["Kilometers", "14500"]]},
	 * 
	 * Level 1: "details" (the name of the map) Level 2: "ar" (the key for
	 * Arabic language entries) Level 3: [xkey, xvalue] (an Arabic Name-value
	 * pair) Level 3: [ykey, yvalue] (an Arabic Name-value pair) Level 2: "en"
	 * (the key for English language entries) Level 3: [xkey, xvalue] (an
	 * English Name-value pair) Level 3: [ykey, yvalue] (an English Name-value
	 * pair)
	 * 
	 * This method uses this information and turns it into a HashMap<String,
	 * List<ObjKombiNameValue>>() that can be stored against any DubizzleKombi
	 * object Please note that we only support ObjKombiNameValue entries for the
	 * entry set.
	 * 
	 * @param incomingJSONObject
	 * @param strJSONArrayTag
	 * @return the created Map<String, List<ObjKombiNameValue>> keyEntrySetMap
	 * @throws JSONException
	 * 
	 */
	/*
	 * commenting this out for now because we are getting a different structure
	 * from KOMBI
	 * 
	 * private Map<String, List<ObjKombiNameValue>>
	 * createKeyNameValueListMap(JSONObject incomingJSONObject, String
	 * strJSONArrayTag) throws JSONException{
	 * 
	 * Map<String, List<ObjKombiNameValue>> keyEntrySetMap = new HashMap<String,
	 * List<ObjKombiNameValue>>();
	 * 
	 * JSONObject jsonKeyEntrySet =
	 * incomingJSONObject.getJSONObject(strJSONArrayTag);
	 * 
	 * //loop through all keys of this object (currently only "ar" and "en" -
	 * but make this future proof for future languages) Iterator<?> keys =
	 * jsonKeyEntrySet.keys(); while( keys.hasNext() ){
	 * 
	 * String key = (String)keys.next(); String val;
	 * 
	 * 
	 * //////////////////////////////////////////////////////////////
	 * 
	 * String strEntrySet = jsonKeyEntrySet.get(key).toString(); JSONArray
	 * entrySetArray = jsonKeyEntrySet.getJSONArray(key);// new
	 * JSONArray(strEntrySet);
	 * 
	 * List<ObjKombiNameValue> entrySetList = new
	 * ArrayList<ObjKombiNameValue>(); //used to store the name-value pairs
	 * available in the JSON Array
	 * 
	 * //iterate through json array, extract json objects if available and add
	 * to corresponding data object manager for (int i = 0; i <
	 * entrySetArray.length(); i++) {
	 * 
	 * //extract the name-value pair (an Array of 2 entries) JSONArray
	 * nameValueArray = entrySetArray.getJSONArray(i);
	 * 
	 * //extract name and value from the JSON Array [name, value]
	 * ObjKombiNameValue nameValuePair = new
	 * ObjKombiNameValue(nameValueArray.getString(0),
	 * nameValueArray.getString(1)); entrySetList.add(nameValuePair); }
	 * 
	 * //after all done - add the hash map entry for this String, List<String>
	 * combination keyEntrySetMap.put(key, entrySetList);
	 * 
	 * //} }
	 * 
	 * return keyEntrySetMap;
	 * 
	 * }
	 */

	/**
	 * method for parsing supporting languages out of country configurations's
	 * JSON. Not the best implementation but it works for now. We need to make
	 * it fully dynamic.
	 * 
	 */
	private Map<String, List<ObjCustomNameValue>> createKeyNameValueListMap(
			JSONObject incomingJSONObject, String strJSONArrayTag)
			throws JSONException {
		Map<String, List<ObjCustomNameValue>> keyEntrySetMap = new HashMap<String, List<ObjCustomNameValue>>();

		JSONObject jsonKeyEntrySet = incomingJSONObject
				.getJSONObject(strJSONArrayTag);

		JSONObject enObj;
		JSONObject arObj;
		JSONObject frObj;

		if (jsonKeyEntrySet.has("en")) {
			enObj = (JSONObject) jsonKeyEntrySet.get("en");
			List<ObjCustomNameValue> enList = getInnerList(enObj);

			if (!enList.isEmpty()) {
				keyEntrySetMap.put("en", enList);
				// ;//Logger.e("teeeesting ",enList.get(0).getString(ObjKombiNameValue.FIELD_VALUE_STRING)+"");
			}

		}

		if (jsonKeyEntrySet.has("ar")) {
			arObj = (JSONObject) jsonKeyEntrySet.get("ar");
			List<ObjCustomNameValue> arList = getInnerList(arObj);

			if (!arList.isEmpty())
				keyEntrySetMap.put("ar", arList);
		}

		if (jsonKeyEntrySet.has("fr")) {
			frObj = (JSONObject) jsonKeyEntrySet.get("fr");
			List<ObjCustomNameValue> frList = getInnerList(frObj);

			if (!frList.isEmpty())
				keyEntrySetMap.put("fr", frList);
		}

		// ;//Logger.e("language map", keyEntrySetMap.toString());

		return keyEntrySetMap;
	}

	// get labels for 1st level tags
	public List<ObjCustomNameValue> getInnerList(JSONObject obj)
			throws JSONException {

		List<ObjCustomNameValue> entrySetList = new ArrayList<ObjCustomNameValue>();
		ObjCustomNameValue nameValuePair;
		if (obj.has("en")) {
			String eng = obj.getString("en");
			if (eng != null) {
				nameValuePair = new ObjCustomNameValue("en", eng);
				entrySetList.add(nameValuePair);
			}

		}

		if (obj.has("ar")) {
			String ar = obj.getString("ar");
			if (ar != null) {
				nameValuePair = new ObjCustomNameValue("ar", ar);
				entrySetList.add(nameValuePair);
			}
		}

		if (obj.has("fr")) {
			String fr = obj.getString("fr");
			if (fr != null) {
				nameValuePair = new ObjCustomNameValue("fr", fr);
				entrySetList.add(nameValuePair);
			}
		}

		return entrySetList;
	}

	/**
	 * 
	 * For saved searches it is beneficial to store name-value pairs into an
	 * object that can directly be URL encoded - we are using Apache's
	 * NameValuePair for this.
	 * 
	 * @param incomingJSONObject
	 *            --> the JSON object containing the Kombi object
	 * @param strJSONArrayTag
	 *            --> the JSON tag of the name-value array. This is used to
	 *            extract the desired name-value array from the Json object
	 * @return List<NameValuePair> nameValueList - a list of NameValuePair that
	 *         can be added to the Kombi object
	 * @throws JSONException
	 * 
	 */
	private List<NameValuePair> createNameValuePairList(
			JSONObject incomingJSONObject, String strJSONArrayTag)
			throws JSONException {

		List<NameValuePair> nameValuePairList = new ArrayList<NameValuePair>();

		JSONObject jsonNameValueList = incomingJSONObject
				.getJSONObject(strJSONArrayTag);

		// this is an name-value array - loop through it and add items as they
		// appear
		Iterator iter = jsonNameValueList.keys();
		while (iter.hasNext()) {

			// set name information
			String strKey = (String) iter.next();
			String strValue = jsonNameValueList.getString(strKey);
			nameValuePairList.add(new BasicNameValuePair(strKey, strValue));

		}

		return nameValuePairList;

	}
	
	
}

