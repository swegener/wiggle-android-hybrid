package uk.co.wiggle.hybrid.application.datamodel.objects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;


import android.os.Bundle;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class ObjNewsfeedRSSItem extends CustomObject {

	public static final String OBJECT_NAME = "item";
	
	private ObjNewsfeedRSSChannel mParentChannel = new ObjNewsfeedRSSChannel();

	//fields specific to a newsfeed channel XML structure
	//Part I: for RSS Channels
	public static final String FIELD_TITLE = "title";
	public static final String FIELD_LINK = "link";
	public static final String FIELD_DESCRIPTION = "description";
	public static final String FIELD_AUTHOR = "author"; 
	public static final String FIELD_ENCLOSURE = "enclosure"; 
	public static final String FIELD_GUID = "guid"; 
	public static final String FIELD_PUBDATE = "pubDate"; //RSS standard - used by Wiggle, Garmin
	public static final String FIELD_PUBDATE_LOWERCASE = "pubdate"; //used by Adidas RSS
	public static final String FIELD_SOURCE = "source"; 
	public static final String FIELD_LAST_BUILD_DATE = "lastBuildDate"; 
	public static final String FIELD_CONTENT_ENCODED = "encoded"; //not "content:encoded" as in XML document but xpp.getName returns "encoded" only; the description, but with HTML tags - used by Garmin RSS
	public static final String FIELD_CATEGORY = "category"; //used by Garmin
	public static final String FIELD_CREATOR = "creator"; ////not "dc:creator" as in XML document but xpp.getName returns "creator" only; used by Garmin
	public static final String FIELD_MEDIA_CONTENT = "content"; //not "media:content" as in XML document but xpp.getName returns "content" only; //used by Wiggle High 5 - additional image
	
	//Part II - additional fields specific to Atom Feeds
	public static final String FIELD_UPDATED = "updated";
	public static final String FIELD_PUBLISHED = "published";
	public static final String FIELD_SUMMARY = "summary";
	public static final String FIELD_CONTENT = "content";
	public static final String FIELD_IMAGE_SRC = "src";
	
	
	private static List<String> sFieldList;
	static {

		sFieldList = Collections.synchronizedList(new ArrayList<String>());
		
		//Part I: for RSS Channels
		sFieldList.add(FIELD_TITLE);
		sFieldList.add(FIELD_LINK);
		sFieldList.add(FIELD_DESCRIPTION);
		sFieldList.add(FIELD_AUTHOR);
		sFieldList.add(FIELD_ENCLOSURE);
		sFieldList.add(FIELD_GUID);
		sFieldList.add(FIELD_PUBDATE);
		sFieldList.add(FIELD_PUBDATE_LOWERCASE);
		sFieldList.add(FIELD_PUBLISHED);
		sFieldList.add(FIELD_LAST_BUILD_DATE);
		sFieldList.add(FIELD_CONTENT_ENCODED);
		sFieldList.add(FIELD_CATEGORY);
		sFieldList.add(FIELD_CREATOR);
		sFieldList.add(FIELD_MEDIA_CONTENT);
		
		//Part II - additional fields specific to Atom Feeds
		sFieldList.add(FIELD_UPDATED);
		sFieldList.add(FIELD_PUBLISHED);
		sFieldList.add(FIELD_SUMMARY);
		sFieldList.add(FIELD_CONTENT);
		sFieldList.add(FIELD_IMAGE_SRC);
		
	}

	private static ConcurrentHashMap<String, Integer> sFieldTypes;
	static {
		sFieldTypes = new ConcurrentHashMap<String, Integer>();
		
		//Part I: for RSS Channels
		sFieldTypes.put(FIELD_TITLE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_LINK, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_DESCRIPTION, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AUTHOR, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_ENCLOSURE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_GUID, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PUBDATE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PUBDATE_LOWERCASE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PUBLISHED, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_LAST_BUILD_DATE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_CONTENT_ENCODED, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_CATEGORY, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_CREATOR, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_MEDIA_CONTENT, FIELD_TYPE_STRING);
		
		//Part II - additional fields specific to Atom Feeds
		sFieldTypes.put(FIELD_UPDATED, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PUBLISHED, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_SUMMARY, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_CONTENT, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_IMAGE_SRC, FIELD_TYPE_STRING);
		
	}

	public ObjNewsfeedRSSItem() {
	}

	public ObjNewsfeedRSSItem(Bundle bundle) {
		super(bundle);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ObjNewsfeedRSSItem)) {
			return false;
		}

		ObjNewsfeedRSSItem other = (ObjNewsfeedRSSItem) o;
		String otherFeedLink = other.getString(FIELD_LINK);
		String thisFeedLink = this.getString(FIELD_LINK);
		return otherFeedLink.equals(thisFeedLink);
	}

	@Override
	public String getObjectName() {
		return OBJECT_NAME;
	}

	@Override
	public List<String> getFields() {
		return sFieldList;
	}

	@Override
	public Class<?> getSubclass() {
		return ObjNewsfeedRSSItem.class;
	}

	@Override
	public Integer getFieldType(String fieldName) {
		return sFieldTypes.get(fieldName);
	}

	// handler for children of this object
	public void setParentChannel(ObjNewsfeedRSSChannel channel){
		mParentChannel = channel;
	}
	
	public ObjNewsfeedRSSChannel getParentChannel(){
		return mParentChannel;
	}
	
	
	/* Other convenience methods */
	public String getPublishDate(){
		
		/* As different RSS feeds have different date formats and different date tags, use this to return the date */
		String strDate = getString(FIELD_PUBDATE);
		
		if(strDate==null || "".equals(strDate)){
			strDate = getString(FIELD_PUBDATE_LOWERCASE);
		}
		
		if(strDate==null || "".equals(strDate)){
			strDate = getString(FIELD_PUBLISHED);
		}
		
		return strDate;
		
	}
	
	public String getContent(){
		
		//Garmin uses content:encoded field to supply HTML content - use this if populated
		String strContent = getString(FIELD_CONTENT_ENCODED);
		
		if(strContent==null || "".equals(strContent)){
			strContent = getString(FIELD_DESCRIPTION);
		}
		
		//Atom feeds
		if(strContent==null || "".equals(strContent)){
			strContent = getString(FIELD_CONTENT);
		}
		
		return strContent;
		
	}
	
	
	public String getAdditionalImageUrl(){
		
		//get Atom image first if exists .. as FIELD_MEDIA_CONTENT used further down has a different meaning in Atom Feeds
		String strImageUrl = getString(FIELD_IMAGE_SRC);
				
		if(strImageUrl!=null && !"".equals(strImageUrl)){
			return getAtomImageUrl(strImageUrl);
		}
		
		//deal with RSS images
		strImageUrl = getString(FIELD_MEDIA_CONTENT);
		
		if(strImageUrl==null || "".equals(strImageUrl)){
			strImageUrl = getString(FIELD_ENCLOSURE);
		}
			
		if(strImageUrl==null || "".equals(strImageUrl)){
			//additional fields ... 
		}
			
		
		return strImageUrl;
	}
	
	private String getAtomImageUrl(String strImageUrl){
		
		//check if this string starts with a domain .. if not, add it artificially from parent feed.
		//This is highly specific, so may not work for other newsfeeds apart from current http://inspiration.goreapparel.com/feed/atom/
		if(!strImageUrl.startsWith("http")){
			
			String strServerBaseUrl = getParentChannel().getParentNewsfeed().getBaseUrl();

			if(!strImageUrl.startsWith("/")){
				strServerBaseUrl = strServerBaseUrl + "/";
			}
			
			strImageUrl = strServerBaseUrl + strImageUrl;
		}
		
		return strImageUrl;
			
	}
	
}