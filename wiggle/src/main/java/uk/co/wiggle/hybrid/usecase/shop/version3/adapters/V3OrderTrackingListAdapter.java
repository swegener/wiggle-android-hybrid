package uk.co.wiggle.hybrid.usecase.shop.version3.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesItem;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesOrder;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.extensions.PicassoSSL;
import uk.co.wiggle.hybrid.usecase.shop.version3.fragments.V3FragmentOrderTracking;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class V3OrderTrackingListAdapter extends BaseAdapter {

	private String logTag = "V3OrderTrackingListAdapter";

	private List<ObjSalesOrder> mOrders = new ArrayList<ObjSalesOrder>();

	public V3OrderTrackingListAdapter() {

	}


	public void setListContent(List<ObjSalesOrder> orders) {
		this.mOrders = orders;
	}

	@Override
	public int getCount() {
		
		if (mOrders == null){
			return 0;
			
		}else{
			return mOrders.size();
		}
		
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	private class ViewHolder {
		
		LinearLayout orderTrackingListRow;
		LinearLayout rowContent;
		LinearLayout orderListRowHeader_lytNew;
		ImageView orderListRowHeader_ivNew;
		TextView orderListRowHeader_tvNew;
		TextView orderListRowHeader_tvOrderStatus;
		TextView orderListRowHeader_tvOrderSubStatus;
		ImageView orderList_ivProductImage1;
		ImageView orderList_ivProductImage2;
		ImageView orderList_ivProductImage3;
		View orderList_vImageMask;
		TextView orderList_tvMoreProducts3;
		TextView orderListRow_tvOrderNbr;
		TextView orderListRow_tvOrderDate;

	}		

	@Override
	public View getView(int arg0, View view, ViewGroup arg2) {
		final int position = arg0;

		final ObjSalesOrder order = (ObjSalesOrder) mOrders.get(arg0);

		Logger.printMessage(logTag, "ORDER: " + order.toXML(), Logger.INFO);

		final ViewHolder holder;
			
		if(view==null){
			
			LayoutInflater inflater = LayoutInflater.from(ApplicationContextProvider.getContext());
			view = inflater.inflate(R.layout.v3_order_tracking_list_row, null);
			
			holder = new ViewHolder();
			
			holder.orderTrackingListRow = (LinearLayout) view.findViewById(R.id.orderTrackingListRow);
			holder.rowContent = (LinearLayout) view.findViewById(R.id.rowContent);

			holder.orderListRowHeader_lytNew = (LinearLayout) view.findViewById(R.id.orderListRowHeader_lytNew);
			holder.orderListRowHeader_ivNew = (ImageView) view.findViewById(R.id.orderListRowHeader_ivNew);
			holder.orderListRowHeader_tvNew = (TextView) view.findViewById(R.id.orderListRowHeader_tvNew);

			holder.orderListRowHeader_tvOrderStatus = (TextView) view.findViewById(R.id.orderListRowHeader_tvOrderStatus);
			holder.orderListRowHeader_tvOrderStatus.setTypeface(AppTypefaces.instance().fontMainBold);

			holder.orderListRowHeader_tvOrderSubStatus = (TextView) view.findViewById(R.id.orderListRowHeader_tvOrderSubStatus);
			holder.orderListRowHeader_tvOrderSubStatus.setTypeface(AppTypefaces.instance().fontMainRegular);

			holder.orderList_ivProductImage1 = (ImageView) view.findViewById(R.id.orderList_ivProductImage1);
			holder.orderList_ivProductImage2 = (ImageView) view.findViewById(R.id.orderList_ivProductImage2);
			holder.orderList_ivProductImage3 = (ImageView) view.findViewById(R.id.orderList_ivProductImage3);
			holder.orderList_vImageMask = view.findViewById(R.id.orderList_vImageMask);

			holder.orderList_tvMoreProducts3 = (TextView) view.findViewById(R.id.orderList_tvMoreProducts3);
			holder.orderList_tvMoreProducts3.setTypeface(AppTypefaces.instance().fontMainRegular);

			holder.orderListRow_tvOrderNbr = (TextView) view.findViewById(R.id.orderListRow_tvOrderNbr);
			holder.orderListRow_tvOrderNbr.setTypeface(AppTypefaces.instance().fontMainRegular);

			holder.orderListRow_tvOrderDate = (TextView) view.findViewById(R.id.orderListRow_tvOrderDate);
			holder.orderListRow_tvOrderDate.setTypeface(AppTypefaces.instance().fontMainRegular);


			view.setTag(holder);
			
		}else{
			holder = (ViewHolder)view.getTag();		
		}


		//TODO: adjust
		String strOrderStatus = order.getDisplayOrderStatus();
		holder.orderListRowHeader_tvOrderStatus.setText(strOrderStatus);

		String strDeliveryStatusHeadline = order.getString(ObjSalesOrder.FIELD_STATUS_HEADLINE);
		holder.orderListRowHeader_tvOrderSubStatus.setText(strDeliveryStatusHeadline);


		boolean blnOrderUpdated = order.hasNewInformation();
		if(blnOrderUpdated){
			holder.orderListRowHeader_lytNew.setVisibility(View.VISIBLE);

			String strOrderLastUpdated = ApplicationContextProvider.getContext().getString(R.string.str_order_status_updated); //TODO: what to display here? (1 min ago)
			holder.orderListRowHeader_tvNew.setText(strOrderLastUpdated);
		}else{
			holder.orderListRowHeader_lytNew.setVisibility(View.GONE);
		}

		String strOrderNumber = order.getString(ObjSalesOrder.FIELD_ORDER_ID);
		strOrderNumber = String.format(ApplicationContextProvider.getContext().getString(R.string.str_order_id), strOrderNumber);
		holder.orderListRow_tvOrderNbr.setText(strOrderNumber);

		String strOrderDate = order.getDisplayOrderDate();
		strOrderDate = ApplicationContextProvider.getContext().getString(R.string.str_ordered_on) + " " + strOrderDate;
		holder.orderListRow_tvOrderDate.setText(strOrderDate);

		setProductImages(holder, order);

		setOrderStatusColour(holder, order);
		
		view.setOnClickListener(null);
		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {

				V3FragmentOrderTracking.instance().onOrderedItemClick(order);

			}

		});

		return view;

	}

	private void setOrderStatusColour(ViewHolder holder, ObjSalesOrder order){

		int intOrderStatusColor = order.getOrderStatusColour();
		holder.rowContent.setBackgroundColor(intOrderStatusColor);

	}

	private void setProductImages(ViewHolder holder, ObjSalesOrder order){

		holder.orderList_ivProductImage1.setImageDrawable(null);
		holder.orderList_ivProductImage2.setImageDrawable(null);
		holder.orderList_ivProductImage3.setImageDrawable(null);

		List<ObjSalesItem> lstItems = order.getSalesItems();

		//Testing only
		/*
		ObjSalesItem addItem = lstItems.get(0);
		lstItems.clear();
		lstItems.add(addItem);
		 */


		//set max 3 images
		for(int i=0;i<Math.min(lstItems.size(), 3);i++){

			ObjSalesItem myItem = lstItems.get(i);

			String strProductImageUrl = myItem.getImagePath("?w=200&amp;h=200&amp;a=0");
			if(i==0){
				PicassoSSL.with(ApplicationContextProvider.getContext()).load(strProductImageUrl).into(holder.orderList_ivProductImage1);
			}else if(i==1){
				PicassoSSL.with(ApplicationContextProvider.getContext()).load(strProductImageUrl).into(holder.orderList_ivProductImage2);
			}else if(i==2){
				PicassoSSL.with(ApplicationContextProvider.getContext()).load(strProductImageUrl).into(holder.orderList_ivProductImage3);
			}

		}


		//more than 3 items - show 'more' textview
		if(lstItems.size()>3){
			holder.orderList_tvMoreProducts3.setText("+" + (lstItems.size()-3));
			holder.orderList_vImageMask.setVisibility(View.VISIBLE);
		}else{
			holder.orderList_tvMoreProducts3.setText("");
			holder.orderList_vImageMask.setVisibility(View.GONE);
		}


	}

	@Override
	public Object getItem(int arg0) {
		return mOrders.get(arg0);
	}
	
}
