package uk.co.wiggle.hybrid.application.datamodel.objects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class ObjApiResponse extends CustomObject {
	
	public static final String OBJECT_NAME = "apiResponse";
	
	public static final String FIELD_ERROR = "error";
	public static final String FIELD_JSON_LIST_RESULTS = "results";
	public static final String FIELD_MESSAGE = "message";
		
			
	private static List<String> sFieldList;
	static {
		
		sFieldList = Collections.synchronizedList(new ArrayList<String>());
		sFieldList.add(FIELD_ERROR);
		sFieldList.add(FIELD_MESSAGE);
				
	}

	private static ConcurrentHashMap<String, Integer> sFieldTypes;
	static {
		
		sFieldTypes = new ConcurrentHashMap<String, Integer>();
		sFieldTypes.put(FIELD_ERROR, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_MESSAGE, FIELD_TYPE_STRING);
		
	}
	
	public ObjApiResponse() {		
		
	}
	
	public ObjApiResponse(Bundle bundle) {
		super(bundle);
	}
	

	@Override
	public String getObjectName() {
		return OBJECT_NAME;
	}
	
	@Override
	public List<String> getFields() {
		return sFieldList;
	}
	
	@Override
	public Class<?> getSubclass() {
		return ObjApiResponse.class;
	}
	
	@Override
	public Integer getFieldType(String fieldName) {
		return sFieldTypes.get(fieldName);
	}
	

	
	public static String createJSONfromResponse(ObjApiResponse object , CustomObject targetObject) throws JSONException{
		
		JSONArray mainArray = new JSONArray();
		List<String> fields = targetObject.getFields();
			
		JSONObject jsonObject = new JSONObject();
			
			// first do the flat fields
			for (String strFieldName : fields) {
				
				if(object.get(strFieldName) != null){
					
					int intValueFieldType = targetObject
							.getFieldType(strFieldName);
					
					switch (intValueFieldType) {
					
					case CustomObject.FIELD_TYPE_OBJECT:
						break;
					
					case CustomObject.FIELD_TYPE_DATETIME:
						break;
					
					default:
						jsonObject.put(strFieldName,
								object.get(strFieldName));
						break;
					}
					
					
				}
				
			}
			
			mainArray.put(jsonObject);
			
		
		return mainArray.toString();
		
	}
	
	
}

