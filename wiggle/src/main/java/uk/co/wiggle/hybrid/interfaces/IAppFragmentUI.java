package uk.co.wiggle.hybrid.interfaces;


import android.view.View;

/**
 * 
 * @author AndroMedia
 * 
 * All Fragments should implement this interface
 *
 */

public interface IAppFragmentUI {
	
	public void updateFragmentContent(View view);

}
