package uk.co.wiggle.hybrid.application.datamodel.objects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;


import android.os.Bundle;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class ObjNewsfeedCategory extends CustomObject {

	public static final String OBJECT_NAME = "ObjNewsfeedCategory";

	//fields used for categories taken from top_level_navigation.json
	public static final String FIELD_TITLE = "title";
	public static final String FIELD_IMAGE_URL = "image_url";
	
	private static List<String> sFieldList;
	static {

		sFieldList = Collections.synchronizedList(new ArrayList<String>());
		
		//fields used for categories taken from top_level_navigation.json
		sFieldList.add(FIELD_ID);
		sFieldList.add(FIELD_TITLE);
		sFieldList.add(FIELD_IMAGE_URL);
		
	}

	private static ConcurrentHashMap<String, Integer> sFieldTypes;
	static {
		sFieldTypes = new ConcurrentHashMap<String, Integer>();
		
		//fields used for categories taken from top_level_navigation.json
		sFieldTypes.put(FIELD_ID, FIELD_TYPE_INTEGER);
		sFieldTypes.put(FIELD_TITLE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_IMAGE_URL, FIELD_TYPE_STRING);
	}

	public ObjNewsfeedCategory() {
	}

	public ObjNewsfeedCategory(Bundle bundle) {
		super(bundle);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ObjNewsfeedCategory)) {
			return false;
		}

		ObjNewsfeedCategory other = (ObjNewsfeedCategory) o;
		int otherId = other.getUniqueId();
		int thisId = this.getUniqueId();
		return otherId==thisId;
	}

	@Override
	public String getObjectName() {
		return OBJECT_NAME;
	}

	@Override
	public List<String> getFields() {
		return sFieldList;
	}

	@Override
	public Class<?> getSubclass() {
		return ObjNewsfeedCategory.class;
	}

	@Override
	public Integer getFieldType(String fieldName) {
		return sFieldTypes.get(fieldName);
	}

}