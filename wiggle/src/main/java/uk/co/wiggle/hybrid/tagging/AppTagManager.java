package uk.co.wiggle.hybrid.tagging;

import java.util.Map;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.logging.Logger;

import android.os.Bundle;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class AppTagManager {

	private static final String logTag = "AppTagManager";
	
	private static AppTagManager instance = null;

	private AppTagManager() {

	}

	public static AppTagManager instance() {

		if (instance == null) {
			instance = new AppTagManager();
		}

		return instance;
	}
	
	
	//for simple screen name / event tags (auto-sends user ID if logged in)
	public void sendEventTags(String strUIScreenName, String strUIEvent) {

		//do not send these tags if app version > 1 is enabled (version 2 tag methods are used for Tabbed Navigation)
		if(AppUtils.getWiggleVersion().getVersionCode()>AppUtils.APP_VERSION.VERSION_1_HYBRID.getVersionCode()){
			//return; //send these tags as well
		}
		
		//Postpone Search event sending until iOS finished (https://basecamp.com/2987293/projects/10994234/todos/221990748)
		if(ApplicationContextProvider.getContext().getString(R.string.str_gtm_page_name_search).equals(strUIScreenName)){
			return;
		}

		Bundle bundle = new Bundle();
		bundle.putString("event", strUIEvent);
		bundle.putString("screenName", strUIScreenName);

		//add user ID to map if user is logged in
		if(ObjAppUser.instance().isUserLoggedIn()){
			String strUserId = ObjAppUser.instance().getString(ObjAppUser.FIELD_CUSTOMER_ID);
			bundle.putString("userId", strUserId);
		}

		ApplicationContextProvider.instance().getAppFirebaseAnalytics().logEvent(strUIEvent, bundle);
		//add further calls to other tag services if needed .. 


	}


	
	//for tags with additional information apart from screen name / event (auto-sends user ID if logged in)
	public void sendEventTags(String strUIScreenName, String strUIEvent, Map<String, Object> tagsMap) {

		//do not send these tags if app version > 1 is enabled (version 2 tag methods are used for Tabbed Navigation)
		if(AppUtils.getWiggleVersion().getVersionCode()>AppUtils.APP_VERSION.VERSION_1_HYBRID.getVersionCode()){
			//return;  //send these tags as well
		}

		//Postpone Search event sending until iOS finished (https://basecamp.com/2987293/projects/10994234/todos/221990748)
		if(ApplicationContextProvider.getContext().getString(R.string.str_gtm_page_name_search).equals(strUIScreenName)){
			return;
		}

		Bundle bundle = new Bundle();

		//Translate old tags Map into Bundle
		for (Map.Entry<String, Object> entry : tagsMap.entrySet()) {
			Logger.printMessage(logTag, "TagMap Entry: " + entry.getKey() + "/" + entry.getValue(), Logger.ALL);
			bundle.putString(entry.getKey(), entry.getValue().toString());
		}

		bundle.putString("event", strUIEvent);
		bundle.putString("screenName", strUIScreenName);

		//add user ID to map if user is logged in
		if(ObjAppUser.instance().isUserLoggedIn()){
			String strUserId = ObjAppUser.instance().getString(ObjAppUser.FIELD_CUSTOMER_ID);
			bundle.putString("userId", strUserId);
		}

		ApplicationContextProvider.instance().getAppFirebaseAnalytics().logEvent(strUIEvent, bundle);

		//add further calls to other tag services if needed .. 


	}

	/*
		Tagging in version 2.0
		Default tagging now sends:
		- event
		- ScreenName
		- actionElement
		- eventLabel
		- UserId (auto-sent if user is logged in)

	 */

	//for simple screen name / event tags (auto-sends user ID if logged in)
	public void sendV2EventTags(String strUIEvent, String strUIScreenName, String strActionElement, String strEventLabel) {

		Bundle bundle = new Bundle();
		bundle.putString("event", strUIEvent);
		bundle.putString("screenName", strUIScreenName);
		bundle.putString("actionElement", strActionElement);
		bundle.putString("labelElement", strEventLabel);

		//add user ID to map if user is logged in
		if(ObjAppUser.instance().isUserLoggedIn()){
			String strUserId = ObjAppUser.instance().getString(ObjAppUser.FIELD_CUSTOMER_ID);
			bundle.putString("userId", strUserId);
		}

		ApplicationContextProvider.instance().getAppFirebaseAnalytics().logEvent(strUIEvent, bundle);
		//add further calls to other tag services if needed ..


	}

	public void sendVersion2ScreenView(String strUIScreenName) {

		String strScreenview = "openScreen";

		//Postpone Search event sending until iOS finished (for Wiggle v1 only) (https://basecamp.com/2987293/projects/10994234/todos/221990748)
		if(AppUtils.getWiggleVersion().getVersionCode()==AppUtils.APP_VERSION.VERSION_1_HYBRID.getVersionCode()){
			if (ApplicationContextProvider.getContext().getString(R.string.str_gtm_page_name_search).equals(strUIScreenName)) {
				return;
			}
		}

		Bundle bundle = new Bundle();
		bundle.putString("event", strScreenview);
		bundle.putString("screenName", strUIScreenName);

		//add user ID to map if user is logged in
		if(ObjAppUser.instance().isUserLoggedIn()){
			String strUserId = ObjAppUser.instance().getString(ObjAppUser.FIELD_CUSTOMER_ID);
			bundle.putString("userId", strUserId);
		}

		ApplicationContextProvider.instance().getAppFirebaseAnalytics().logEvent(strScreenview, bundle);

		//add further calls to other tag services if needed ..


	}


}
