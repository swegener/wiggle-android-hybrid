package uk.co.wiggle.hybrid.usecase.shop.tabbednav.activities;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Browser;
import android.provider.Settings;
import com.google.android.material.appbar.AppBarLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.ValueCallback;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request.Method;

import org.json.JSONObject;

import java.util.Date;
import java.util.List;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler.ON_RESPONSE_ACTION;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.animations.AppAnimations;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjNavigationCategoryManager;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjSalesOrderManager;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjApiRequest;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNavigationCategory;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesOrder;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjTypeaheadResponse;
import uk.co.wiggle.hybrid.application.fragments.Dialog;
import uk.co.wiggle.hybrid.application.fragments.Dialog.DialogActionListener;
import uk.co.wiggle.hybrid.application.helpers.AppFragmentHelper;
import uk.co.wiggle.hybrid.application.helpers.AppGlobalConstants;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.application.persistence.DataStore;
import uk.co.wiggle.hybrid.application.session.AppSession;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.application.utils.DateUtils;
import uk.co.wiggle.hybrid.interfaces.IAppActivityUI;
import uk.co.wiggle.hybrid.interfaces.IAppApiResponse;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentSelfCloseListener;
import uk.co.wiggle.hybrid.tagging.AppTagManager;
import uk.co.wiggle.hybrid.usecase.newsfeed.activities.NewsfeedActivity;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.fragments.FragmentMyAccount;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.fragments.FragmentOrderTracking;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.fragments.FragmentShopNavigation;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.fragments.FragmentUserLogin;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.fragments.FragmentWebShop;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.helpers.TabbedNavTabHistoryHelper;
import uk.co.wiggle.hybrid.usecase.startup.activities.WelcomeActivity;

import static android.os.Build.VERSION_CODES.LOLLIPOP;


/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/


@SuppressLint("NewApi") public class TabbedHybridActivity extends AppCompatActivity
        implements IAppActivityUI, IAppApiResponse, OnClickListener, DialogActionListener, IAppFragmentSelfCloseListener {



    //allow other activities to read an instance of MainActivity so that MainActivity methods can be called
	private static TabbedHybridActivity myInstance;

	public static TabbedHybridActivity instance(){
		return myInstance;
	}


	private static final String logTag = "HybridActivity";

	public static String getLogTag(){
		return logTag;
	}

	/* Constants used to pass information from other screens */
	public static String WEB_VIEW_CALLER = "WEB_VIEW_CALLER";
	public static String LOAD_TARGET_URL_FROM_NATIVE_SCREEN = "LOAD_TARGET_URL_FROM_NATIVE_SCREEN";
	public static String BREADCRUMB_TEXT = "BREADCRUMB_TEXT";



	//Widgets
	AppBarLayout main_lytTopElements;
	TextView main_tvBreadcrumb;
	TextView main_tvInfo;
	RelativeLayout lytPleaseWaitDark;
	ImageView pleaseWaitDark_ivPleaseWait;
	TextView pleaseWaitDark_tvPleaseWait;
	ProgressBar main_pbLoadProgress;
	View main_headerDivider;

	//Used for order tracking caching
	boolean blnAcceptResponseIntoCacheOnSwipeRefresh = false;

	public void setAcceptOrderTrackingResponseOnSwipeRefresh(boolean blnValue){
		blnAcceptResponseIntoCacheOnSwipeRefresh = blnValue;
	}

	//Used in case of country changes in MyAccount - if this boolean is set and the fragment
	//manager back stack is cleared, a fresh "home" fragment will be added
	boolean blnAddHomeAfterBackstackCleared = false;

	public void setAddHomeFragmentOnBackStackClearedListener(){
		blnAddHomeAfterBackstackCleared = true;
		showPleaseWait(getString(R.string.main_tvPleaseWait));
	}


	//Tabbed navigation bar
	LinearLayout hybridAcitivtyNavigationTab;
	RelativeLayout navigation_Home;
	ImageView navigation_ivHome;
	RelativeLayout navigation_Search;
	ImageView navigation_ivSearch;
	RelativeLayout navigation_Basket;
	ImageView navigation_ivBasket;
	RelativeLayout navigation_Wishlist;
	ImageView navigation_ivWishlist;
	RelativeLayout navigation_Account;
	ImageView navigation_ivAccount;
	TextView navigation_tvBasketCounter;
	TextView navigation_tvAccountBadge;

	//Fragment container and manager
	FrameLayout main_fragmentContainer;
	FragmentManager fm;



	//Last URL user tried before login was required
	String strLastUserRequestedURL;

	//Reference to search menu item
	MenuItem actionBarSearchMenuItem;

	//Used to store the actual FragmentWebShop instance that called the login screen
	Fragment mLoginScreenCaller = null;

	public void setLoginScreenCaller(Fragment callingFragment){
		mLoginScreenCaller = callingFragment;
	}

    //URL used to navigate to unsupported stores in external browser
    String strUnsupportedStoreHost = "";

    //used to force clearing history when user returns to "Home" after deep-navigation
    boolean blnForceClearHistory = false;

	public void setForceClearHistory(boolean blnValue){
		blnForceClearHistory = blnValue;
	}


	boolean blnIsDisplayedToUser = false;

	//For deep links
	String strDeepLinkTargetFragmentName = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //No title window
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        overridePendingTransition(AppUtils.getActivityOpenAnimation(), android.R.anim.fade_out);

        setContentView(R.layout.hybrid_activity_tabbednav);

        myInstance = this;

        //perform any upgrade relevant code
        executeCodeForVersionUpgrade();

        setupCustomActionBar();

        initialiseSupportFragmentManager();

        findViews();

        //retriever order data if user is logged in
        if (ObjAppUser.instance().isUserLoggedIn()) {
            AppRequestHandler.instance().retrieveOrderData(this, ON_RESPONSE_ACTION.SUBMIT_ORDER_TRACKING_REQUEST);
        }

        //Activity was opened by user or any other mechanism
        //Add Home webview & initialise tab
        setNavigationTabSelected(navigation_Home);

		//Check for deep or app linking
		boolean blnIsDeepOrAppLink = scanForDeepLinking() || scanForAppLinking();

		if(!blnIsDeepOrAppLink) {
			//no deep or app link (i.e. standard start of app via homescreen) - show home page
			addWebViewFragment(AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home_https)), true);
		}

        AppTagManager.instance().sendVersion2ScreenView("App - Shop");



    }






    public void updateShoppingCartCounter(String strShoppingCartCounter){

    	//prevent NPE
    	if(navigation_tvBasketCounter==null){
    		return;
    	}

     	if(strShoppingCartCounter!=null && !"".equals(strShoppingCartCounter) && Integer.parseInt(strShoppingCartCounter)>0){
    		navigation_tvBasketCounter.setText(strShoppingCartCounter);
			navigation_tvBasketCounter.setVisibility(View.VISIBLE);
    	}else{
    		navigation_tvBasketCounter.setText(strShoppingCartCounter);
			navigation_tvBasketCounter.setVisibility(View.GONE);
    	}

    }




	public void handleNetworkStatusChange(boolean isConnected) {

		// TODO Implement as required

	}

	public void restartApp(){

		/**
		 *  This kills the app process - not very fond of this solution.
		 *  Handler delays restarting the app as we may have to wait for shared preferences to be saved (async)
		 *
		 **/
		appRestartHandler.postDelayed(appRestartRunnable, 800);
	}

	private Handler appRestartHandler = new Handler();

	private Runnable appRestartRunnable = new Runnable() {
		   public void run() {
				killAndRestartApp();
		   }
	};

	private void killAndRestartApp(){

		/*
			Some native activities use HybridActivity to show content e.g.
				Legal & Insurance (insurance quote)
				Events - event details
				News - some news links

			Just killing and restarting the app with a default intent leads to old back
			stack being shown. We hence restart the app as a completely new task and clear
			the back stack.

		 */
		Intent restartIntent = new Intent(this, WelcomeActivity.class);
		restartIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);


		//With API level 16, we can finish all activities in the current task (even those above the current activity) by just calling ..
		finishAffinity();


		AlarmManager alm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
		alm.set(AlarmManager.RTC, System.currentTimeMillis() + 500, PendingIntent.getActivity(this, 0, restartIntent, 0));
		android.os.Process.killProcess(android.os.Process.myPid());

	}



	@Override
	public void onSaveInstanceState(Bundle savedState) {

	    super.onSaveInstanceState(savedState);

	}


	@Override
	public void onRestoreInstanceState(Bundle savedState) {

	    super.onRestoreInstanceState(savedState);


	}




	public void showUserAsLoggedInOrOut(){

		//If my Account fragment is shown - update it
		if(FragmentMyAccount.instance()!=null
				&& FragmentMyAccount.instance().isAdded()
				&& FragmentMyAccount.instance().getView()!=null //make sure onViewCreated was executed
				){

			FragmentMyAccount.instance().showUserAsLoggedInOrOut();

		}

		updateOrderTrackingBadge();

	}

	public void handleInAppLogin(){

		showUserAsLoggedInOrOut();
		AppSession.instance().setInAppLogin(false);
		if(mLoginScreenCaller!=null) {
			if(mLoginScreenCaller instanceof FragmentWebShop) {

				FragmentWebShop webShopOrView = ((FragmentWebShop) mLoginScreenCaller);

				if(webShopOrView.isWebViewFragment()){

					FragmentWebShop webViewFragment = (FragmentWebShop) fm.findFragmentByTag(FragmentWebShop.CONST_WEB_VIEW_FRAGMENT);
					if(webViewFragment!=null
							&& webViewFragment.isAdded()
							){
						webViewFragment.navigateToReturnUrl();
					}

				}else if(webShopOrView.isWebShopFragment()){

					FragmentWebShop webShopFragment = (FragmentWebShop) fm.findFragmentByTag(FragmentWebShop.CONST_WEB_SHOP_FRAGMENT);
					if(webShopFragment!=null
							&& webShopFragment.isAdded()
							){
						webShopFragment.navigateToReturnUrl();
					}

				}

			}
			mLoginScreenCaller = null;
		}

	}




	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub

		int id = view.getId();

		switch (id) {

			case R.id.nativeSearch_ivResortTextSearchClear:
				FragmentShopNavigation.instance().onClickEvent(id);
				break;

			case R.id.orderDetail_btnMoreDetails:
				FragmentOrderTracking.instance().onClickEvent(id);
				break;

			case R.id.navigation_Home:
			case R.id.navigation_Search:
			case R.id.navigation_Basket:
			case R.id.navigation_Wishlist:
			case R.id.navigation_Account:
				onNavigationTabPressed(id, true);
				break;

			//handle click events from MyAccount fragment
			case R.id.myAccount_btnLogin:
			case R.id.myAccount_btnRegister:
			case R.id.myAccount_lytTrackMyOrder:
			case R.id.myAccount_tvDetails:
			case R.id.myAccount_tvCurrency:
			case R.id.myAccount_tvDeliveryDestination:
			case R.id.myAccount_tvHelp:
			case R.id.myAccount_tvContactUs:
			case R.id.myAccount_tvAppFeedback:
            case R.id.myAccount_tvChangeCountry:
			case R.id.myAccount_btnLogout:
				FragmentMyAccount.instance().onClickEvent(id);
				break;

			//handle click events from UserLogin fragment
			case R.id.userLogin_btnLogin:
			case R.id.userLogin_btnRegister:
			case R.id.userLogin_tvForgotPassword:
			case R.id.userLogin_swRememberMyDetails:
			case R.id.userLogin_ivClearEmail:
			case R.id.userLogin_ivClearPassword:
				FragmentUserLogin.instance().onClickEvent(id);
				break;

			//click events from navigation Fragment
			case R.id.nativeSearch_tvCancel:
				FragmentShopNavigation.instance().onClickEvent(id);
				break;


			case R.id.main_tvBreadcrumb:
				showAppReviewPromptActivity(true);
				//getSessionConfirmationNumber();
				break;

		default:
			break;
		}

	}

	@Override
	public void onApiRequestSuccess(ObjApiRequest apiRequest,
			JSONObject apiSuccessDetail) {

		if(AppRequestHandler.getAuthenticationEndpoint().equals(apiRequest.getURLEndpoint())){

			//Close FragmentUserLogin if displayed (only the case on explicit login, not silent log in)
			if(FragmentUserLogin.instance()!=null
					&& FragmentUserLogin.instance().isAdded()){
				hideVirtualKeyboard();
				//close FragmentUserLogin
				AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, FragmentUserLogin.instance(), FragmentUserLogin.getLogTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.REMOVE,  R.anim.bottom_view_slide_out, false);
				//set title to visible fragment after removing login fragment
				setTitleForVisibleFragment();
			}

			handleInAppLogin();


		}else if(AppRequestHandler.getOrderTrackingEndpoint().equals(apiRequest.getURLEndpoint())){

			//update order counter if any
			updateOrderTrackingBadge();

			if(ON_RESPONSE_ACTION.RETRIEVE_USER_RELATED_DATA.equals(apiRequest.getOnResponseAction())
					){

				if(FragmentMyAccount.instance()!=null
						&& FragmentMyAccount.instance().isAdded()){
					FragmentMyAccount.instance().updateOrderTrackingBadge();
				}

			}else{

				//update entire list
				updateOrderTrackingList();

			}

			if(blnAcceptResponseIntoCacheOnSwipeRefresh){
				blnAcceptResponseIntoCacheOnSwipeRefresh = false;
				AppSession.instance().acceptOrderTrackingPayloadIntoCache();
			}

		}

	}

	private void updateOrderTrackingList(){

		if(FragmentOrderTracking.instance()!=null
				&& FragmentOrderTracking.instance().isAdded()
				){
			FragmentOrderTracking.instance().initialiseNoDataText();
			FragmentOrderTracking.instance().updateOrderTrackingList();

		}

	}

	@Override
	public void onApiRequestError(ObjApiRequest request,
			int intHttpResponseCode, JSONObject apiErrorDetail) {

		if(AppRequestHandler.getAuthenticationEndpoint().equals(request.getURLEndpoint())){

			//Notify FragmentUserLogin if displayed (explicit login)
            if(FragmentUserLogin.instance()!=null
                    && FragmentUserLogin.instance().isAdded()){
                FragmentUserLogin.instance().onLoginResponseReceived();
            }else {

                //in case of in-app silent login:
                //any problems with login - show login screen
                AppUtils.showCustomToast("Please log in manually..", true);
                showLoginScreen();

            }

		}

	}

	@Override
	public void setupCustomActionBar() {
		// TODO Auto-generated method stub


	}

	@Override
	public void setCustomFontTypes() {
		// TODO Auto-generated method stub

		main_tvBreadcrumb.setTypeface(AppTypefaces.instance().fontMainBold);
		main_tvInfo.setTypeface(AppTypefaces.instance().fontMainBold);
		pleaseWaitDark_tvPleaseWait.setTypeface(AppTypefaces.instance().fontMainRegular);



		navigation_tvBasketCounter.setTypeface(AppTypefaces.instance().fontMainRegular);
		navigation_tvAccountBadge.setTypeface(AppTypefaces.instance().fontMainRegular);


	}

	@Override
	public void findViews() {

		main_lytTopElements = (AppBarLayout) findViewById(R.id.main_lytTopElements);
		main_tvBreadcrumb = (TextView) findViewById(R.id.main_tvBreadcrumb);
		main_tvInfo = (TextView) findViewById(R.id.main_tvInfo);
		lytPleaseWaitDark = (RelativeLayout) findViewById(R.id.lytPleaseWaitDark);
		pleaseWaitDark_ivPleaseWait = (ImageView) findViewById(R.id.pleaseWaitDark_ivPleaseWait);
		pleaseWaitDark_tvPleaseWait = (TextView) findViewById(R.id.pleaseWaitDark_tvPleaseWait);
		main_pbLoadProgress = (ProgressBar) findViewById(R.id.main_pbLoadProgress);
		main_headerDivider = findViewById(R.id.main_headerDivider);


		 //Tabbed navigation bar
		 hybridAcitivtyNavigationTab = (LinearLayout) findViewById(R.id.hybridAcitivtyNavigationTab);
		 navigation_Home = (RelativeLayout) findViewById(R.id.navigation_Home);
		 navigation_ivHome = (ImageView) findViewById(R.id.navigation_ivHome);
		 navigation_Search = (RelativeLayout) findViewById(R.id.navigation_Search);
		 navigation_ivSearch = (ImageView) findViewById(R.id.navigation_ivSearch);
		 navigation_Basket = (RelativeLayout) findViewById(R.id.navigation_Basket);
		 navigation_ivBasket = (ImageView) findViewById(R.id.navigation_ivBasket);
		 navigation_Wishlist = (RelativeLayout) findViewById(R.id.navigation_Wishlist);
		 navigation_ivWishlist = (ImageView) findViewById(R.id.navigation_ivWishlist);
		 navigation_Account = (RelativeLayout) findViewById(R.id.navigation_Account);
		 navigation_ivAccount = (ImageView) findViewById(R.id.navigation_ivAccount);
		 navigation_tvBasketCounter = (TextView) findViewById(R.id.navigation_tvBasketCounter);
		 navigation_tvAccountBadge = (TextView) findViewById(R.id.navigation_tvAccountBadge);

		 //fragment container
		 main_fragmentContainer = (FrameLayout) findViewById(R.id.main_fragmentContainer);

		 updateWidgets();

	}

	private void updateWidgets(){

        showUserAsLoggedInOrOut();

        setCustomFontTypes();

		showAdditionalInfo();

	}



		@Override
		public void onResume() {
			
		    super.onResume();

			//load local navigation tree
			loadLocalNavigationTreeIfMissing();

			myInstance = this; //repeated after onCreate - prevent NPE after app in background

		}

		@Override
		protected void onPause() {
			super.onPause();
			blnIsDisplayedToUser = false;
		}
		
		@Override
		public void onWindowFocusChanged(boolean hasFocus) {
		    super.onWindowFocusChanged(hasFocus);

			if(hasFocus){


			}
		    
		}		
		
		
		@Override
		public void onBackPressed(){


			AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_general), "generalBack");

			//special handling of back-press depending on fragment shown
			if(handleFragmentBackNavigation()){
				return;
			}

			//If we get to here - close app
				
			/**
			alertUser(
					0,
					"",
					getString(R.string.str_place_an_ad_dialog_title),
					getString(R.string.str_place_an_ad_dialog_message),
					Alert.OK_GO_BACK);
			**/
			AppUtils.showCustomToast("Nothing more to go back to...", true);


			//clear order manager
			ObjSalesOrderManager.getInstance().clear();

			//clear any session parameters
			AppSession.instance().setAndroidNativeCategory(null);
			AppSession.instance().setInAppLogin(false);

			finish();


		}




	public void loadUrlInExternalBrowser(String strTargetUrl){
		
		Bundle bundle = new Bundle();
		
		/* You can add additional header information as follows - but only do this if it can't be achieved in webview.setSettings()  */
		//bundle.putString(key, value);

		Intent externalBrowserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(strTargetUrl));
		externalBrowserIntent.putExtra(Browser.EXTRA_HEADERS, bundle);
		startActivity(externalBrowserIntent);
		
	}

	public void loadUrlInWebViewFragment(String strTargetUrl){


		/* Use this method if a URL needs to be opened in the WebView fragment */

		addWebViewFragment(strTargetUrl, true);

	}

	public void loadUrlForViewInWebViewFragment(int intCallingViewId){

		/* Use this method if a URL linked to a specific calling view
			needs to be opened in the WebView fragment */

		addWebViewFragment(intCallingViewId, false);

	}

	public void loadUrlInWebShopFragment(String strTargetUrl){

		/* Use this method if a URL needs to be opened in the WebShop fragment */

		addWebShopFragment(false, strTargetUrl);

	}



	public void loadLocalNavigationTreeIfMissing(){

		if(ObjNavigationCategoryManager.getInstance().getLocalNavigationTree()==null){ //indicates app was in background and paused - or just starting now


			//get navigation tree from local store and make sure navigation drawer is updated afterwards
			ObjApiRequest localFileReadRequest =
					new ObjApiRequest(this,
							null,
							AppRequestHandler.LOCAL_NAVIGATION_FILE,
							Method.GET,
							null,
							null,
							ON_RESPONSE_ACTION.REFRESH_NAVIGATION_DRAWER);
			AppRequestHandler.instance().getJsonObjectDataFromRawResource(localFileReadRequest);


		}

	}

	public void loadSearchResultInWebView(String strTargetUrl){
		
		hideVirtualKeyboard();
		loadUrlInWebShopFragment(strTargetUrl);
		
	}
	
	public void openCategoryUrl(ObjNavigationCategory category, String strGTMEvent){

		//send GTM Tag - report "linkclick" as another screen is opened (webview or native screen)
		String strCategoryTag = category.getVersion2GATag();
		String strCategoryTier = category.getVersion2GATagLevel();
		AppTagManager.instance().sendV2EventTags("linkclick","App - Navigation",strCategoryTier,strCategoryTag);


		//get URL depending on category source / type
		String strCategoryUrl = category.getTargetUrl();

		//build Wiggle shop URL
		String strTargetUrl = AppUtils.getWiggleURL(strCategoryUrl);
		
		//additional UMT tagging for all categories with a Google Tag Manager event .. amend UTM parameters to URL
		if(strGTMEvent!=null && !"".equals(strGTMEvent)){
			strTargetUrl = AppUtils.appendUtmTag(strTargetUrl, strGTMEvent);
		}
		
		
		if(category.getBoolean(ObjNavigationCategory.FIELD_OPEN_IN_EXTERNAL_BROWSER)){
			loadUrlInExternalBrowser(strTargetUrl);
		}else{
			//load URL in web shop (not web view)
			loadUrlInWebShopFragment(strTargetUrl);
		}
		
	}
	
	public void startNewsfeedActivity(ObjNavigationCategory category, String strGTMEvent){

		//send GTM Tag - report "linkclick" as another screen is opened (webview or native screen)
		if(category!=null) {
			String strCategoryTag = category.getVersion2GATag();
			String strCategoryTier = category.getVersion2GATagLevel();
			AppTagManager.instance().sendV2EventTags("linkclick", "App - Navigation", strCategoryTier, strCategoryTag);
		}

		Intent intent = new Intent(this, NewsfeedActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		//intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION); //this makes sure activities do not slide in from right to left when started, but simply appear
		startActivityForResult(intent, 0);
		
		
	}

	public void performLogout(){

        AppSession.instance().clearUserSpecificData();
        showUserAsLoggedInOrOut();
        clearLoginCookieOnWebview(); //clear login cookie
		//APP-238: requirement now is to keep the Basket cookie (we need to keep all cookies in this case) after logout
		//So, disable the following. We would prefer to leave this in place and have the Wiggle server handle the basket cookie correctly after logout / relogin
		//clearAllCookiesAfterLogout(); //APP-190 - clear Shopping basket and other cookies after logout
		navigateToHomeTab();

    }

    public void navigateToHomeTab(){
		navigation_Home.performClick();
	}

    private void clearLoginCookieOnWebview(){

		if(FragmentWebShop.instance()!=null
				&& FragmentWebShop.instance().isAdded()
				){
			FragmentWebShop.instance().addAdditionalHttpHeaders();
		}
	}


	@SuppressWarnings("deprecation")
	public static void clearAllCookiesAfterLogout()
	{

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			CookieManager.getInstance().removeAllCookies(null);
			CookieManager.getInstance().flush();
		} else
		{
			CookieSyncManager cookieSyncMngr=CookieSyncManager.createInstance(ApplicationContextProvider.getContext());
			cookieSyncMngr.startSync();
			CookieManager cookieManager=CookieManager.getInstance();
			cookieManager.removeAllCookie();
			cookieManager.removeSessionCookie();
			cookieSyncMngr.stopSync();
			cookieSyncMngr.sync();
		}
	}


	public void showLoginScreen(){

		AppSession.instance().setInAppLogin(true);

        addUserLoginFragment(false);

		
	}

	


	private void addNavigationFragment(boolean blnSupressAnimation){

		AppTagManager.instance().sendVersion2ScreenView("App - Navigation");

		//If the shop fragment is added - bring it to front instead of the navigation fragment
		Fragment webShopFragment = fm.findFragmentByTag(FragmentWebShop.CONST_WEB_SHOP_FRAGMENT);
		if(webShopFragment!=null
				&& webShopFragment.isAdded()){
			addWebShopFragment(false, null);
			return;
		}

		/*
		if(FragmentWebShop.getWebShopInstance()!=null
				&& FragmentWebShop.getWebShopInstance().isAdded()){
			addWebShopFragment(false, null);
		}
		*/

		//Fragment already in back stack? Just show it again and hide all the others
		if(bringFragmentToTop(FragmentShopNavigation.getLogTag())){
			return;
		}

		int intAppearAnimation = R.anim.activity_right_slide_in;

		//if this is the initial fragment: no animation as otherwise activity starts with an ugly delay
		if(blnSupressAnimation){
			intAppearAnimation = 0;
		}

		FragmentShopNavigation navigationFragment = new FragmentShopNavigation();
		setFragmentArguments(navigationFragment, getString(R.string.shop_navigationFragment_breadcrumb_text));
		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, navigationFragment, FragmentShopNavigation.getLogTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.ADD,  intAppearAnimation, false);


	}

	public void hideVirtualKeyboard(){
		
		InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		im.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		
	}


	public void updateQueryResult(List<ObjTypeaheadResponse> lstTypeaheadResponse){

		//moved to FragmentShopNavigation
		if(FragmentShopNavigation.instance()!=null
				&& FragmentShopNavigation.instance().isAdded()) {
			FragmentShopNavigation.instance().updateQueryResult(lstTypeaheadResponse);
		}

	}
	

	
	public void showUnsupportedStoreDialog(String strTargetHost){
		
		strUnsupportedStoreHost = strTargetHost;
		
		DialogFragment clearFormDialog = new Dialog(this,
				getString(R.string.str_unsupported_store_dialog_title),
				String.format(
						getString(R.string.str_unsupported_store_dialog_message),
						AppUtils.getCountryNameFromHost(strTargetHost)
						),
				String.format(
						getString(R.string.str_unsupported_store_dialog_action_confirm),
						strTargetHost
						),
				getString(R.string.str_unsupported_store_dialog_action_cancel)
			 );
		
		
		clearFormDialog.setCancelable(false); //make sure dialog can't be cancelled via back button or tapping outside it
		
		clearFormDialog.show(fm, Dialog.DIALOG_GO_TO_UNSUPPORTED_STORE);
		
	}
	

	@Override
	public void onDialogPositiveClick(DialogFragment dialog) {
		
		String strDialogTag = dialog.getTag();
		
		if(Dialog.DIALOG_GO_TO_UNSUPPORTED_STORE.equals(strDialogTag)){
			
			loadUrlInExternalBrowser("http://" + strUnsupportedStoreHost);
			strUnsupportedStoreHost = "";
			finish(); //close Wiggle app (if not doing this, do mWebView.clearHistory(); to avoid issues with back-navigation)
			
		}else if(Dialog.DIALOG_LEAVE_APP_REVIEW_AFTER_ORDER.equals(strDialogTag)){

			String strStoreUrl = AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home));

			Uri uri = Uri.parse(strStoreUrl);
			String strHost = uri.getHost();

			//Example for ES ..
			//https://www.trustpilot.com/review/www.wiggle.es
			//For UK:
			//https://www.trustpilot.com/review/www.wiggle.co.uk
			loadUrlInExternalBrowser("https://www.trustpilot.com/review/" + strHost);

			ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_POST_ORDER_TRUSTPILOTREVIEW_ACCEPTED, String.valueOf(AppUtils.getAppVersionInt()));

		/* Dialogs from FragmentEvents */
		}else if(Dialog.DIALOG_EVENT_LIST_SWIPE_TO_REVEAL_HINT.equals(strDialogTag)){
			ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_EVENTS_SWIPE_HINT_ALREADY_SEEN, "1");
		}else if(Dialog.DIALOG_EVENT_SCREEN_LOCATION_REQUIRED.equals(strDialogTag)){
			// Show location settings screen ..
			Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);
		}else if(Dialog.DIALOG_EVENT_SCREEN_UPDATE_GOOGLE_PLAY_SERVICES.equals(strDialogTag)){

			//Go to Google Play and suggest Google Play Services
			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setData(Uri.parse("market://details?id=com.google.android.gms"));
			startActivity(intent);

		}
		
	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		
		String strDialogTag = dialog.getTag();
		
		if(Dialog.DIALOG_GO_TO_UNSUPPORTED_STORE.equals(strDialogTag)){
			
			strUnsupportedStoreHost = "";
			clearWebViewHistory(); //avoid issues with back-navigation
			
		}else if(Dialog.DIALOG_LEAVE_APP_REVIEW_AFTER_ORDER.equals(strDialogTag)) {

			//make sure user is not asked for review multiple times in the same session
			ObjSalesOrderManager.getInstance().resetJustDeliveredOrders();

		}

	}

	private void clearWebViewHistory(){

		if(FragmentWebShop.instance()!=null
				&& FragmentWebShop.instance().isAdded()
				){
			FragmentWebShop.instance().setForceClearHistory();
		}
	}
	

	
	public String getSearchTerm(){
		
		//moved to FragmentShopNavigation
		if(FragmentShopNavigation.instance()!=null
				&& FragmentShopNavigation.instance().isAdded()){
			return FragmentShopNavigation.instance().getSearchTerm();
		}else{
			return "";
		}
		
	}

	
	@Override
	public void onFragmentSelfClose(String tag) {

		//remove fragment by tag where applicable
		Fragment fragmentToRemove = fm.findFragmentByTag(tag);

		// remove fragment by tag ...
//		Fragment fragmentToRemove = fm.findFragmentByTag(tag);
//		FragmentTransaction transaction = fm.beginTransaction();
//		transaction.remove(fragmentToRemove);
//		transaction.commitAllowingStateLoss();

		//or just pop the entire back stack if dealing with multiple fragments
		//The idea is to display a "virgin" parent activity
		fm.popBackStack();
		
		
	}
	
	private void executeCodeForVersionUpgrade(){
		
		final String VERSION_UPGRADE_LOG = "VERSION_UPGRADE_LOG";
		final String LAST_SUCCESSFUL_UPGRADE_TARGET_VERSION = "LAST_SUCCESSFUL_UPGRADE_TARGET_VERSION";
		
		String strLastSucessfulUpgradeTargetVersion = DataStore.getStringFromSharedPreferences(VERSION_UPGRADE_LOG, LAST_SUCCESSFUL_UPGRADE_TARGET_VERSION);
		
		//Version spefic code executed after version upgrades if required
		//First introduced with version 1.02 - any version before will return "" from shared preferences
		if(strLastSucessfulUpgradeTargetVersion.equals("")){
			
			//Version 1.02 upgrade - we need to remove all Cookies as previous versions incorrectly duplicated cookie WiggleCustomer2
			//Without clearing all cookies, the app will show a "too_many_redirects" error again during checkout
			clearAllCookies();
			
			//Job done? All upgrade relevant code executed .. log target version number in shared preferences
			String strAppVersion = AppUtils.getAppVersionNumber();
			DataStore.saveStringToSharedPreferences(VERSION_UPGRADE_LOG, LAST_SUCCESSFUL_UPGRADE_TARGET_VERSION, strAppVersion);
			
		}else if(strLastSucessfulUpgradeTargetVersion.equals("1.03")){
			
			//do some upgrade work if required .. TODO: add version numbers and handling as required
			
		}
		
		
	}
	
	private void clearAllCookies(){

		CookieManager cookieManager = CookieManager.getInstance();

		if (Build.VERSION.SDK_INT >= LOLLIPOP) {
		       cookieManager.removeAllCookies(new ValueCallback<Boolean>() {
		         // a callback which is executed when the cookies have been removed
		         @Override
		         public void onReceiveValue(Boolean aBoolean) {
		               Logger.printMessage(logTag, "clearAllCookies - removedAllCookies: " + aBoolean, Logger.INFO);
		         }
		       });
		}else{
			cookieManager.removeAllCookie();
		}

	}


	
	public void showReviewOrderDialog(ObjSalesOrder salesOrder){
		
		//get current app version number:
		int intCurrentAppVersion = AppUtils.getAppVersionInt();
		
		//get version number the user last agreed to leave feedback on an order
		int intLastFeedbackInVersion = ObjAppUser.instance().getInt(ObjAppUser.FIELD_POST_ORDER_TRUSTPILOTREVIEW_ACCEPTED);
		
		//let at least one version pass before we ask user to submit review again. E.g. if user submitted review
		//in version 16, we wont ask again before version 18. If user never submitted review, intCurrentAppVersion would be 
		//zero and we ask him in any case. 
		/** Enable this if you would like to ask users for further feedback in the future
		if(intLastFeedbackInVersion == 0 
				|| intCurrentAppVersion - intLastFeedbackInVersion <=1){ // 1 = skip 1 version for feedback
			return;
		}
		**/
		
		//if user already accepted the trustpilot review dialog - dont ask him again.
		if(intLastFeedbackInVersion>0){
			//Already asked for Feedback? Return .. 
			return;
		}
		
		
		DialogFragment reviewOrderDialog = new Dialog(this,
				"", //getString(R.string.str_leave_order_review_dialog_title),
//				String.format(
//						getString(R.string.str_leave_order_review_dialog_message),
//						salesOrder.getString(ObjSalesOrder.FIELD_ORDER_ID)
//						),
				getString(R.string.str_leave_order_review_dialog_message_new),
				getString(R.string.str_leave_order_review_dialog_action_confirm),
				getString(R.string.str_leave_order_review_dialog_action_cancel)
			 );
		
		
		reviewOrderDialog.show(fm, Dialog.DIALOG_LEAVE_APP_REVIEW_AFTER_ORDER);
		
	}


	public void updateOrderTrackingBadge(){
		
		int intUpdatedOrders = ObjSalesOrderManager.getInstance().getOrdersWithNewInformation().size();

		if(intUpdatedOrders>0){
			//Wiggle requirement - on "My Account" navigation tab, only show badge, but no text
			navigation_tvAccountBadge.setText("");
			//navigation_tvAccountBadge.setText(String.valueOf(intUpdatedOrders));
			navigation_tvAccountBadge.setVisibility(View.VISIBLE);
		}else{
			navigation_tvAccountBadge.setVisibility(View.GONE);
		}



	}







	private boolean scanForDeepLinking(){

		/*

		   Deep links can point to this app currently with the following configuration:
		   1) wiggle://native  ->   opens native app screen
		      Current supported deep links are:
		      		   •	wiggle://native/news -> NewsfeedActivity
		      		   •	wiggle://native/events -> EventsActivity
		      		   •	wiggle://native/delivery -> Order tracking screen in HybridActivity
		   2) wiggle://web -> opens Hybrid activity and opens URL slug after /web
		      For instance:
		      			•	wiggle://web/swim  -->  opens www.wiggle.<topleveldomain>/swim

		 */
		Intent intent = getIntent();
		String action = intent.getAction();
		Uri deepLinkUri = intent.getData();

		if(deepLinkUri==null){
			//nothing to do
			return false;
		}else{
			return onDeepLinkDetected(deepLinkUri);
		}

	}

	private boolean onDeepLinkDetected(Uri deepLinkUri){

		/* 1. Handle dedicated wiggle://native deep links
		*
		*     They can look just like "wiggle://native/news"
		*     or have parameters appended to it. e.g in app message deep links may
		*    look like "wiggle://native/news" in the console, but
		*     arrive here as wiggle://native/delivery?ampExternalOpen=true&ampAction=click
		*     Thus using startsWith (could use Uri.getHost etc ... but doesnt matter here)
		* */
		if(deepLinkUri.toString().startsWith("wiggle://native/news")){

			AppUtils.showCustomToast("Open Newsfeed Activity for " + deepLinkUri.toString(), true);
			closeNativeScreensOnDeepLink();
			startNewsfeedActivity(null, "deepLinkNews"); //TODO: adjust GTM tag
			//Deep link identified
			return true;

		} else if (deepLinkUri.toString().startsWith("wiggle://native/delivery")){

			AppUtils.showCustomToast("Open Order tracking screen for " + deepLinkUri.toString(), true);
			//Close any activities that would appear on top of our Hybrid Activity
			closeNativeScreensOnDeepLink();
			if(blnIsDisplayedToUser){
				addOrderTrackingFragment(true);
			}else{
				//Activity is not shown or started by deep link? Wait for onStart to open deep link
				strDeepLinkTargetFragmentName = FragmentOrderTracking.getLogTag();
			}
			//Deep link identified
			return true;

		}

		/* 2. Handle dedicated wiggle://web deep links */
		if("wiggle".equals(deepLinkUri.getScheme()) && "web".equals(deepLinkUri.getHost())){

			String strTargetUrl = AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home)) + deepLinkUri.getPath().toString();
			AppUtils.showCustomToast("Open hybrid activity for " + deepLinkUri.toString(), true);
			//Close any activities that would appear on top of our Hybrid Activity
			closeNativeScreensOnDeepLink(); //false - do not close HybridActivity as we open URL here
			openDeepLinkInWebView(strTargetUrl);
			//Deep link identified
			return true;

		}

		/* 3. Handle other URL patterns */
		/* Not needed for now
		if(!blnDeepLinkIdentified
				&& ("wiggle".equals(deepLinkUri.getScheme()) && "sportives".equals(deepLinkUri.getHost()))
				||
				("wiggle".equals(deepLinkUri.getScheme()) && "events".equals(deepLinkUri.getHost()))
				){

			if(AppUtils.isNativeEventScreenEnabled()) {
				AppUtils.showCustomToast("Open Events Activity for " + deepLinkUri.toString(), true);
				blnDeepLinkIdentified = true;
				closeNativeScreensOnDeepLink();
				startEventsActivity(null, "deepLinkEvents");
				finish(); //finish Hybrid Activity when detail screen is opened to avoid multiple instances of HybridActiviy in back stack
				return;
			}

		}
		*/

		/* 4. Check if this in general is a wiggle-hosted URL - just open it in Hybrid Activity */
		if(AppUtils.isTrustedWiggleHost(deepLinkUri.getHost().toString()) //removed in version 2.1.1 - we rely on App-Linking to auto-verify trusted hosts
				){
			//Close any activities that would appear on top of our Hybrid Activity
			closeNativeScreensOnDeepLink(); //false - do not close HybridActivity as we open URL here
			openDeepLinkInWebView(deepLinkUri.toString());
			//Deep link identified
			return true;
		}


		//no handled deep link found? Do nothing
		return false;

	}

	private void openDeepLinkInWebView(String strUrl){

	    //Check if we have a webshop fragment already showing - just load Deep link URL in it
		Fragment currentFragment = fm.findFragmentById(R.id.main_fragmentContainer);
		if (currentFragment instanceof FragmentWebShop){

			FragmentWebShop.instance().loadUrlInWebView(strUrl);

		}else{
			// no current web shop fragment - add it and show deep link
			loadUrlInWebViewFragment(strUrl);
		}

    }

	private void closeNativeScreensOnDeepLink(){


		/*
		   Closing any native screens that currently exist in the activity stack
		    makes sure that deep-linking does not introduce duplicate activities to the stack
		   and all deep-link intents are correctly processed (e.g. when HybridActivity is open
		   a deep-link needs to take you to the correct target page)
		 */

		if(NewsfeedActivity.instance()!=null){
			NewsfeedActivity.instance().finish();
		}

		//TODO: add further activities here when implemented

	}



	@Override
	public void onNewIntent(Intent intent){

		//check new intents as well for deep links
		//If StartupActivity has already been started and deep links are pressed elsewhere they may not work otherwise
		super.onNewIntent(intent);
		setIntent(intent);
		scanForDeepLinking();

	}




	@Override
	public void onStart(){

		super.onStart();

		blnIsDisplayedToUser = true;

		goToDeepLinkFragment();


	}




	public void showHttpErrorDialog(String strUrl){

		DialogFragment dialog = new Dialog(this,
				getString(R.string.hybridActivity_webview_load_error_title),
				getString(R.string.hybridActivity_webview_load_error_message),
				getString(R.string.hybridActivity_webview_load_error_ok_button).toUpperCase(),
				"" // no cancel button
		);

		dialog.show(fm, Dialog.DIALOG_WEBVIEW_LOAD_ERROR);


	}


	private void onNavigationTabPressed(int intTabId, boolean blnExplicitUserAction){

		//home tab pressed? clear all tab-navigation history
		if(intTabId==R.id.navigation_Home){
			TabbedNavTabHistoryHelper.instance().clearHistory();
		}

		//tab is pressed: Wiggle require a navigation history for tabs
		maintainTabHistory(blnExplicitUserAction);

		//Whenever tab is pressed - check if FragmentWebView is shown and remove if required
		Fragment webViewFragment = fm.findFragmentByTag(FragmentWebShop.CONST_WEB_VIEW_FRAGMENT);
		if(webViewFragment!=null
				&& webViewFragment.isAdded()){
			AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, webViewFragment, webViewFragment.getTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.REMOVE, 0, false);
		}

		//properly remove account fragments if required
		removeMyAccountFragments();

		//for Shop tab: if active tab is pressed again - go back to tab root
		resetTabIfRequired(intTabId);


		View tabView = findViewById(intTabId);

		setNavigationTabSelected(tabView);

		resetCoordinatorLayout();

		performTabAction(tabView, intTabId);

	}

	private void setNavigationTabSelected(View tabView){

		//highlight current tab
		deselectAllNavigationTabs();

		ImageView tabImage = (ImageView) tabView.findViewWithTag("tabImage");
		tabView.setSelected(true);
		tabImage.setColorFilter(getResources().getColor(R.color.orangeButton));

		setBreadcrumbText(tabView.getTag().toString());

		AppAnimations.getInstance().pulseAnimation(tabImage, R.anim.pulse_navtab);

	}

	private void performTabAction(View tabView, int intTabId){

		//legacy tracking - same for all tabs
		AppTagManager.instance().sendEventTags("App - Thumb Nav","ThumbNav");

		//when clicking a tab - always clear any fragments that might be displayed
		//removeAllFragmentsToIndex(0); //this REMOVES all fragments, so last user interaction is lost when fragment next added

		//String strTabTag = tabView.getTag().toString();
		switch (intTabId) {

			case R.id.navigation_Home:
                AppTagManager.instance().sendV2EventTags("linkclick","App - Thumb Nav","Main","Home Page");
                addWebViewFragment(R.id.navigation_Home, false);
				break;

			case R.id.navigation_Search:
                AppTagManager.instance().sendV2EventTags("linkclick","App - Thumb Nav","Main","Navigation");
				addNavigationFragment(false);
				break;

			case R.id.navigation_Basket:
                AppTagManager.instance().sendV2EventTags("linkclick","App - Thumb Nav","Main","Basket");
                String strBasketUrl = AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_shopping_basket), "shopBasket");
				addWebViewFragment(strBasketUrl, false);
				break;

			case R.id.navigation_Wishlist:
                AppTagManager.instance().sendV2EventTags("linkclick","App - Thumb Nav","Main","Wishlist");
                //loadTargetURL(R.id.navigation_Wishlist);
                addWebViewFragment(R.id.navigation_Wishlist, false);
				break;

			case R.id.navigation_Account:
                AppTagManager.instance().sendV2EventTags("linkclick","App - Thumb Nav","Main","My Account");
                //loadTargetURL(R.id.navigation_Account);
				addMyAccountFragment(false);
				break;

			default:
				break;

		}

	}

	private void deselectAllNavigationTabs(){

		navigation_Home.setSelected(false);
		navigation_ivHome.setColorFilter(null);
		navigation_Search.setSelected(false);
		navigation_ivSearch.setColorFilter(null);
		navigation_Basket.setSelected(false);
		navigation_ivBasket.setColorFilter(null);
		navigation_Wishlist.setSelected(false);
		navigation_ivWishlist.setColorFilter(null);
		navigation_Account.setSelected(false);
		navigation_ivAccount.setColorFilter(null);

	}


	private void initialiseSupportFragmentManager() {

		fm = getSupportFragmentManager();
		//add onBackStackListener so we can update the breadcrumb etc depending on the fragment currently shown
		fm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {

			@Override
			public void onBackStackChanged() {

				Fragment activeFragment = fm.findFragmentById(R.id.main_fragmentContainer);
				updateUIWidgetsForFragment(activeFragment);

				if (fm.getBackStackEntryCount() == 0) {
					//main_fragmentContainer.setVisibility(View.GONE);
					//no more fragments - add "Home" fragment automatically (this is used when user changes country in account options)
					if(blnAddHomeAfterBackstackCleared) {
						blnAddHomeAfterBackstackCleared = false;
						//Recreate home fragment - but wait a while as fragment manager needs to finish handling outstanding transactions
						recreateHomeFragmentWithDelay();
					}
				}

			}
		});

	}

	public void updateUIWidgetsForFragment(Fragment fragment){


		String strFragmentBreadcrumbTitle = AppFragmentHelper.getFragmentBreadcrumbTitle(fragment);
		if(strFragmentBreadcrumbTitle!=null && !"".equals(strFragmentBreadcrumbTitle)) {


			//Due to the use of hidden fragments this does not always work
			//Hence let navTabs and fragments do the breadcrumb setting
			//setBreadcrumbText(strFragmentBreadcrumbTitle);
			setTitleForVisibleFragment(); //show title for visible fragment instead

			main_tvBreadcrumb.setVisibility(View.VISIBLE);
		}else{
			main_tvBreadcrumb.setVisibility(View.GONE);
		}

	}



	private void addMyAccountFragment(boolean blnSupressAnimation){

		AppTagManager.instance().sendVersion2ScreenView("App - My Account");


		//Fragment already in back stack? Just show it again and hide all the others
		if(bringFragmentToTop(FragmentMyAccount.getLogTag())){
			return;
		}

		int intAppearAnimation = 0; //removed (workaround for Shop-tab screens showing when fragments are changed on top of Shop-tab)

		//if this is the initial fragment: no animation as otherwise activity starts with an ugly delay
		if(blnSupressAnimation){
			intAppearAnimation = 0;
		}

		FragmentMyAccount myAccountFragment = new FragmentMyAccount();
		setFragmentArguments(myAccountFragment, getString(R.string.shop_myAccount_breadcrumb_text));
		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, myAccountFragment, FragmentMyAccount.getLogTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.ADD,  intAppearAnimation, false);


	}


	private void addWebShopFragment(boolean blnSupressAnimation, String strOptionalURL){

		//Web SHOP Fragment already in back stack? Just show it again and hide all the others
		Fragment webShopFragment = fm.findFragmentByTag(FragmentWebShop.CONST_WEB_SHOP_FRAGMENT);
		if(webShopFragment!=null
			&& webShopFragment.isAdded()
			){
			if (bringFragmentToTop(FragmentWebShop.CONST_WEB_SHOP_FRAGMENT)) {
				return;
			}
		}

		/*
		if (bringFragmentToTop(FragmentWebShop.CONST_WEB_SHOP_FRAGMENT)) {
			return;
		}
		*/


		int intAppearAnimation = R.anim.activity_right_slide_in;

		//if this is the initial fragment: no animation as otherwise activity starts with an ugly delay
		if(blnSupressAnimation){
			intAppearAnimation = 0;
		}

		FragmentWebShop newWebShopFragment = new FragmentWebShop();
		setWebShopFragmentArguments(newWebShopFragment, "Web Shop Fragment", FragmentWebShop.CONST_WEB_SHOP_FRAGMENT, strOptionalURL);
		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, newWebShopFragment, FragmentWebShop.CONST_WEB_SHOP_FRAGMENT, AppFragmentHelper.FRAGMENT_TRANSACTION.ADD,  intAppearAnimation, false);

	}

	//only called in exceptions if the user changes the Wiggle shop region in the settings screen
	public void removeWebShopFragment(){

		//Web SHOP Fragment already in back stack? Just show it again and hide all the others
		Fragment webShopFragment = fm.findFragmentByTag(FragmentWebShop.CONST_WEB_SHOP_FRAGMENT);
		if(webShopFragment!=null
				&& webShopFragment.isAdded()
				){
			removeSingleFragment(webShopFragment);
		}

	}

	public void addUserLoginFragment(boolean blnSupressAnimation){

		AppTagManager.instance().sendVersion2ScreenView("App - signin");


		if(bringFragmentToTop(FragmentUserLogin.getLogTag())){
			return;
		}

		int intAppearAnimation = R.anim.activity_right_slide_in;

		//if this is the initial fragment: no animation as otherwise activity starts with an ugly delay
		if(blnSupressAnimation){
			intAppearAnimation = 0;
		}

		FragmentUserLogin userLoginFragment = new FragmentUserLogin();
		setFragmentArguments(userLoginFragment, getString(R.string.shop_userLogin_breadcrumb_text));
		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, userLoginFragment, FragmentUserLogin.getLogTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.ADD,  intAppearAnimation, false);


	}


    private void addWebViewFragment(int intCallingView,  boolean blnSupressAnimation){


		//This fragment always goes to the top of the stack
        //And always will get removed when navigated away from it
        //It's purpose is to load Wiggle shop URLs from various callers
        //whereas the WebShopFragment is used for browing on the "Shop" tab and always remains in the
        //background retaining its last state

        int intAppearAnimation = 0; //removed (workaround for Shop-tab screens showing when fragments are changed on top of Shop-tab);

        //if this is the initial fragment: no animation as otherwise activity starts with an ugly delay
        if(blnSupressAnimation){
            intAppearAnimation = 0;
        }

        FragmentWebShop webViewFragment = new FragmentWebShop();
        setWebShopFragmentArguments(webViewFragment, "Web View Fragment", FragmentWebShop.CONST_WEB_VIEW_FRAGMENT, intCallingView);
		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, webViewFragment, FragmentWebShop.CONST_WEB_VIEW_FRAGMENT, AppFragmentHelper.FRAGMENT_TRANSACTION.ADD,  intAppearAnimation, false);


    }

	private void addWebViewFragment(String strTargetUrl,  boolean blnSupressAnimation){


		//This fragment always goes to the top of the stack
		//And always will get removed when navigated away from it
		//It's purpose is to load Wiggle shop URLs from various callers
		//whereas the WebShopFragment is used for browing on the "Shop" tab and always remains in the
		//background retaining its last state

		int intAppearAnimation = 0; //removed (workaround for Shop-tab screens showing when fragments are changed on top of Shop-tab);

		//if this is the initial fragment: no animation as otherwise activity starts with an ugly delay
		if(blnSupressAnimation){
			intAppearAnimation = 0;
		}

		FragmentWebShop webViewFragment = new FragmentWebShop();
		setWebShopFragmentArguments(webViewFragment, getString(R.string.shop_home_breadcrumb_text), FragmentWebShop.CONST_WEB_VIEW_FRAGMENT, strTargetUrl);
		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, webViewFragment, FragmentWebShop.CONST_WEB_VIEW_FRAGMENT, AppFragmentHelper.FRAGMENT_TRANSACTION.ADD,  intAppearAnimation, false);


	}


	public void addOrderTrackingFragment(boolean blnSupressAnimation){

		AppTagManager.instance().sendVersion2ScreenView("App - Track My Order - My Orders");


		//Fragment already in back stack? Just show it again and hide all the others
		if(bringFragmentToTop(FragmentOrderTracking.getLogTag())){
			return;
		}

		int intAppearAnimation = R.anim.activity_right_slide_in;

		//if this is the initial fragment: no animation as otherwise activity starts with an ugly delay
		if(blnSupressAnimation){
			intAppearAnimation = 0;
		}

		FragmentOrderTracking orderTrackingFragment = new FragmentOrderTracking();
		setFragmentArguments(orderTrackingFragment, getString(R.string.myAccount_orderTracking_breadcrumb_text));
		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, orderTrackingFragment, FragmentOrderTracking.getLogTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.ADD,  intAppearAnimation, false);


	}


	private void setFragmentArguments(Fragment anyFragmentOnThisActivity, String strBreadcrumbTitle){

        /* common method used to populate
            CALLING_ACTIVITY
            FRAGMENT_BREADCRUMB_TITLE
            for any fragment hosted by this Activity
         */
		Bundle arguments = new Bundle();
		arguments.putString(AppGlobalConstants.CALLING_ACTIVITY, TabbedHybridActivity.getLogTag());
		arguments.putString(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE, strBreadcrumbTitle);
		anyFragmentOnThisActivity.setArguments(arguments);

	}

	//specific implementation to setFragmentArguments for WebShopFragment
	private void setWebShopFragmentArguments(
			FragmentWebShop webShopFragment
			, String strBreadcrumbTitle
			, String strFragmentUsage
			, int intWebViewCaller
			){


		Bundle arguments = new Bundle();
		arguments.putString(AppGlobalConstants.CALLING_ACTIVITY, TabbedHybridActivity.getLogTag());
		arguments.putString(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE, strBreadcrumbTitle);
		arguments.putString(FragmentWebShop.CONST_FRAGMENT_USAGE, strFragmentUsage);
		arguments.putInt(FragmentWebShop.WEB_VIEW_CALLER, intWebViewCaller);
		webShopFragment.setArguments(arguments);

	}

	private void setWebShopFragmentArguments(
			FragmentWebShop webShopFragment
			, String strBreadcrumbTitle
			, String strFragmentUsage
			, String strTargerUrl
	){


		Bundle arguments = new Bundle();
		arguments.putString(AppGlobalConstants.CALLING_ACTIVITY, TabbedHybridActivity.getLogTag());
		arguments.putString(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE, strBreadcrumbTitle);
		arguments.putString(FragmentWebShop.CONST_FRAGMENT_USAGE, strFragmentUsage);
		arguments.putString(FragmentWebShop.LOAD_TARGET_URL, strTargerUrl);
		webShopFragment.setArguments(arguments);

	}

	private boolean handleFragmentBackNavigation(){


		if(fm.getBackStackEntryCount()==0){
			return false;
		}

		//Check if FragmentWebView (i.e. FragmentWebShop with webView usage)
		// is the current fragment - if so, then remove it
        Fragment currentFragment = fm.findFragmentById(R.id.main_fragmentContainer);
        if (currentFragment instanceof FragmentWebShop
				&& ((FragmentWebShop) currentFragment).isWebViewFragment()) {

			//TODO:
			if (((FragmentWebShop) currentFragment).instance().webViewCanGoBack()) {
				//go back
				return true;
			} else {
				//on home Tab? Close app
				if(getActiveTab()==R.id.navigation_Home){
					return false;
				}else {
					if(isRootFragmentForTab(currentFragment)) {
						/*
						//Fragment is already there - so simply load new URL
						//but clear history so we can't navigate back to previous URL
						((FragmentWebShop) currentFragment).instance().setForceClearHistory();
						((FragmentWebShop) currentFragment).instance().loadUrlInWebView(AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home_https)));
						//set Home tab selected without executing its action
						setNavigationTabSelected(navigation_Home);
						*/
						navigateTabHistory();
					}else{
						AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, currentFragment, currentFragment.getTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.REMOVE, 0, false);
					}
					return true;
				}
			}

        }

		Fragment visibleFragment = getTopVisibleFragment();

		//No fragmnents visible? Nothing to handle
		if(visibleFragment==null){
			return false;
		}


		//special handling for some fragments if required .. (some fragments have a "canGoBack" option)
		if(visibleFragment instanceof FragmentWebShop) {

			if (FragmentWebShop.instance().webViewCanGoBack()) {
				return true;
			} else {
				//We can't go back on the web shop fragment? Remove it as no history to keep now
				AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, visibleFragment, visibleFragment.getTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.REMOVE, 0, false);
				//.. and go back to navigation browser
				addNavigationFragment(false);
				return true;
			}

		}else if(visibleFragment instanceof FragmentShopNavigation){

			//on this root-fragment, there is a "goBack" option
			if(FragmentShopNavigation.instance().shopFragmentCanGoBack()) {
				return true;
			}
			//important:  we need to continue with isRootFragmentForTab if not true


		}else if(visibleFragment instanceof FragmentUserLogin){

			AppSession.instance().setInAppLogin(false);
		}

		//for all other fragments - check if this is their root
		//Business rule: if we hit the root fragment for a tab - the back button goes back to
		//the last page you visited in the home tab. The user can tab home again to load the actual home page
		if(isRootFragmentForTab(visibleFragment)){
			//navigateToHomeTab();
			navigateTabHistory();
			return true;
		}

		if(visibleFragment instanceof  FragmentOrderTracking){

			//allow back navigation on order tracking screen
			if(FragmentOrderTracking.instance().orderTrackingScreenCanGoBack()){
				return true;
			}

		}else if(visibleFragment instanceof FragmentShopNavigation){

			//We want to keep the state of the navigation fragment - thus just hiding and not removing
			hideSingleFragment(visibleFragment);
			return true;

		}



		//default handling - remove fragment on back pressed
		if(visibleFragment!=null) {
			AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, visibleFragment, visibleFragment.getTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.REMOVE, 0, false);
		}

		return true;

	}


	private boolean isRootFragmentForTab(Fragment fragment){

		/* returns true if the entry-level fragment of a tab is displayed */

		int intActiveTab = getActiveTab();

		switch (intActiveTab) {

			case R.id.navigation_Home:
				//no root fragment - back button should close the app
				break;
			case R.id.navigation_Search:
				if(fragment instanceof FragmentShopNavigation){
					return true;
				}
				break;
			case R.id.navigation_Basket:
				if(fragment instanceof FragmentWebShop
						&& ((FragmentWebShop) fragment).isWebViewFragment()
						){
					return true;
				}
				break;
			case R.id.navigation_Wishlist:
				if(fragment instanceof FragmentWebShop
						&& ((FragmentWebShop) fragment).isWebViewFragment()
						){
					return true;
				}
				break;
			case R.id.navigation_Account:
				if(fragment instanceof FragmentMyAccount){
					return true;
				}
				break;

			default:
				break;

		}

		return false;

	}

	public int getActiveTab(){

		int lstNavTabs[] = {
			R.id.navigation_Home,
			R.id.navigation_Search,
			R.id.navigation_Basket,
			R.id.navigation_Wishlist,
			R.id.navigation_Account
		};


		for(int idView : lstNavTabs){
			View tabView = findViewById(idView);
			if(tabView.isSelected()){
				return idView;
			}
		}

		return -1;

	}

	public void removeAllFragmentsToIndex(int entryIndex) {

		// This removes all fragments at once leading to an empty fragment stack
		// Use with care as this of course looses any interaction the user has last done on the fragment

		if (fm == null) {
			return;
		}

		if (fm.getBackStackEntryCount() <= entryIndex) {
			return;
		}

		FragmentManager.BackStackEntry entry = fm.getBackStackEntryAt(entryIndex);

		if (entry != null) {
			fm.popBackStackImmediate(entry.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}


	}


	private Fragment getTopVisibleFragment(){


		//gets the "topmost" fragment in the back stack that is visible - this one is displayed to the user

		if(fm==null || fm.getFragments()==null){
			return null;
		}

		Fragment topVisibleFragment  = null;

		//On this Activity, we show / hide fragments in order to keep their state rather than replacing them.
		//Show/Hide operations do not change the backstack - and the "active" fragment on top of the stack may not be the one
		//that you are seeing on the screen. So - in order to find the fragment that is currently displayed, we
		//loop through all fragments and identify the one that is currently visible
		for (Fragment myFragment : fm.getFragments()) {
			if (myFragment!=null && myFragment.isVisible()) {
				topVisibleFragment = myFragment;
			}
		}

		//no fragments or all fragments hidden?
		return topVisibleFragment;
	}


	public boolean bringFragmentToTop(String tag) {

		Fragment fragment = fm.findFragmentByTag(tag);

		if (fragment != null) {

			FragmentTransaction fragmentTransaction = fm.beginTransaction();

			fragmentTransaction.setCustomAnimations(R.anim.activity_right_slide_in, R.anim.activity_left_slide_out, 0, 0);


			for (Fragment f : fm.getFragments()) {

				if(f==null){
					continue;
				}

				if (f.equals(fragment)) {
					fragmentTransaction.show(f);
					//update UI Widgets for this fragment
					updateUIWidgetsForFragment(fragment);
				}else {
					fragmentTransaction.hide(f);
				}
			}

			fragmentTransaction.commit();
			return true;
		}

		return false;
	}

	private void hideAllFragments(){

		if(fm==null || fm.getFragments()==null){
			return;
		}

		FragmentTransaction fragmentTransaction = fm.beginTransaction();

		for (Fragment f : fm.getFragments()) {
			if(f==null){
				continue;
			}

			if(f.isVisible()) {
				fragmentTransaction.hide(f);
			}
		}

		fragmentTransaction.commit();

	}




	public void setBreadcrumbText(String strUrlOrText){

		//Hide progress bar if displayed
		main_pbLoadProgress.setVisibility(View.GONE);
		hideHeaderDividerForShopFragment(strUrlOrText);
		resetCoordinatorLayout();

		main_tvBreadcrumb.setText(strUrlOrText);

		if(strUrlOrText==null || "".equals(strUrlOrText)){
			main_lytTopElements.setVisibility(View.GONE);
		}else {
			main_lytTopElements.setVisibility(View.VISIBLE);
		}


	}

	public void hideSingleFragment(Fragment fragment){

		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, fragment, fragment.getTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.HIDE,  0, false);

	}

	public void removeSingleFragment(Fragment fragment){

		AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, fragment, fragment.getTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.REMOVE,  0, false);

	}



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);



	}

	public void setTitleForVisibleFragment(){

		//Exception - do nothing when we are on "Home" tab and user is navigating deep into the website
		//TODO: this is very hacky .. please adjust
		if(R.id.navigation_Home==TabbedHybridActivity.instance().getActiveTab()){
			if("    ".equals(main_tvBreadcrumb.getText())){
				return;
			}
		}


		Fragment visibleFragment = getTopVisibleFragment();

		if(visibleFragment!=null){

			//We shop fragment does not have arguments for use on back pressed - set screen title to tab title
			if(visibleFragment instanceof FragmentWebShop){

				String strBreadcrumb = "";

				//check if there was a caller for this fragment
				if(((FragmentWebShop) visibleFragment).getWebViewCaller()>0) {
					strBreadcrumb = ((FragmentWebShop) visibleFragment).getBreadcrumbText();
				}

				//Breadcrumb for caller available? Use it ..
				if(!"".equals(strBreadcrumb)){
					setBreadcrumbText(strBreadcrumb);
				}else{
					//default: show tab title as breadcrumb for FragmentWebShop
					int intActiveTab = getActiveTab();
					View activeTab = findViewById(intActiveTab);
					setBreadcrumbText(activeTab.getTag().toString());
				}

				return;

			}

			//other fragments: set screen title to fragment breadcrumb extra
			Bundle data = visibleFragment.getArguments();

			// Get the extras
			if (data.containsKey(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE)) {

				String strTitle = data.getString(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE);
				setBreadcrumbText(strTitle);

			}
		}

	}

	private void recreateHomeFragmentWithDelay(){


		new Handler().postDelayed(new Runnable() {
			public void run() {
				loadUrlForViewInWebViewFragment(R.id.navigation_Home);
				hidePleaseWait();
			}
		}, 500);

	}

	public void showPleaseWait(String strMessage){

		lytPleaseWaitDark.setVisibility(View.VISIBLE);
		AppAnimations.getInstance().pulseAnimation(pleaseWaitDark_ivPleaseWait, R.anim.pulse);
		pleaseWaitDark_tvPleaseWait.setText(strMessage);

	}

	public void hidePleaseWait(){

		lytPleaseWaitDark.setVisibility(View.INVISIBLE);
	}

	private void goToDeepLinkFragment(){

		/*
		    We need to wait for onStart for deep links to be opened - deep link fires way earlier than onStart
		    and at that point the system is still to add the WebShopFragment for the home screen
		    This leads to deep links not being displayed in the foreground.
		 */

		if(strDeepLinkTargetFragmentName==null){
			return;
		}

		if(FragmentOrderTracking.getLogTag().equals(strDeepLinkTargetFragmentName)){
			addOrderTrackingFragment(false);
		}

		strDeepLinkTargetFragmentName = null;

	}

	public void setProgressBar(int intProgress){

		Fragment visibleFragment = getTopVisibleFragment();

		//only show progress bar when WebShop fragment is the top visible fragment
		if(visibleFragment instanceof FragmentWebShop) {
			main_pbLoadProgress.setVisibility(View.VISIBLE);
		}else{
			main_pbLoadProgress.setVisibility(View.GONE);
		}

		main_pbLoadProgress.setProgress(intProgress);

		if(intProgress == 100){
			//Page loaded .. hide progress bar
			main_pbLoadProgress.setVisibility(View.GONE);
		}

	}

	private void hideHeaderDividerForShopFragment(String strBreadcrumbText){

		if(getString(R.string.shop_navigationFragment_breadcrumb_text).equals(strBreadcrumbText)
				&& getTopVisibleFragment() instanceof FragmentShopNavigation){
			main_headerDivider.setVisibility(View.GONE);
		}else{
			main_headerDivider.setVisibility(View.VISIBLE);
		}

	}



	public void resetCoordinatorLayout(){

		Fragment topVisibleFragment = getTopVisibleFragment();

		//reset coordinator layout on all views but webShop / webView
		if(!(topVisibleFragment instanceof FragmentWebShop)) {
			main_lytTopElements.setExpanded(true);
		}

	}

	private void removeMyAccountFragments(){

		//only do the following if we currently are on the MyAccount tab before switching tabs
		if(getActiveTab()!=R.id.navigation_Account){
			return;
		}

		if(FragmentOrderTracking.instance()!=null
				&& FragmentOrderTracking.instance().isAdded()){
			removeSingleFragment(FragmentOrderTracking.instance());
		}

		if(FragmentUserLogin.instance()!=null
			&& FragmentUserLogin.instance().isAdded()){
			removeSingleFragment(FragmentUserLogin.instance());
		}

		if(FragmentMyAccount.instance()!=null
				&& FragmentMyAccount.instance().isAdded()){
			removeSingleFragment(FragmentMyAccount.instance());
		}

	}



	private void showAdditionalInfo(){

		String strAdditionalInfo = "";

		//Check if app is redirected to a test server
		strAdditionalInfo = AppUtils.REDIRECT_APP_TO_TEST_SERVER;


		main_tvInfo.setText(strAdditionalInfo);

		if(!"".equals(strAdditionalInfo)){
			main_tvInfo.setVisibility(View.VISIBLE);
		}else{
			main_tvInfo.setVisibility(View.GONE);
		}

	}

	private void resetTabIfRequired(int intPressedTabId){

		//check if this tab is already active
		int intActiveTab = getActiveTab();

		if(intPressedTabId==intActiveTab){

			//special handling for some tabs
			if(intActiveTab==R.id.navigation_Search){

				Fragment visibleFragment = getTopVisibleFragment();

				if(visibleFragment!=null){

					if(visibleFragment instanceof FragmentWebShop) {

						//Whatever the history - close Shop fragment
						AppFragmentHelper.handleFragment(this, fm, main_fragmentContainer, visibleFragment, visibleFragment.getTag(), AppFragmentHelper.FRAGMENT_TRANSACTION.REMOVE, 0, false);
						//.. and go back to navigation browser, display the category browser
						FragmentShopNavigation.instance().shopFragmentCanGoBack();
						bringFragmentToTop(FragmentShopNavigation.getLogTag());

					}else if (visibleFragment instanceof FragmentShopNavigation){

						//Navigation fragment shown? Display the category browser
						FragmentShopNavigation.instance().shopFragmentCanGoBack();

					}

				}




			}

		}

	}

	private void maintainTabHistory(boolean blnExplicitUserAction){

		int intCurrentTab = getActiveTab();
		String strTabName = getResources().getResourceEntryName(intCurrentTab);

		if(blnExplicitUserAction) {
			TabbedNavTabHistoryHelper.instance().addHistoryEntry(intCurrentTab, strTabName, "");
		}

	}

	private void navigateTabHistory(){

		//check if current tab is in history - if so, remove it so we dont navigate back to it
		int intActiveTab = getActiveTab();
		TabbedNavTabHistoryHelper.instance().removeTabFromHistory(intActiveTab);

		//get most recent tab from history and navigate to it
		TabbedNavTabHistoryHelper.HistoryEntry entry = TabbedNavTabHistoryHelper.instance().getBackNavigationEntry();
		if(entry!=null){
			int intNextTabId = entry.getTabId();
			onNavigationTabPressed(intNextTabId, false);
		}else{
			finish(); //finish app
		}

	}

	private boolean scanForAppLinking(){

		//This method is to handle auto-verified deep links (http links mapped via App-Linking)

		Intent appLinkIntent = getIntent();
		String appLinkAction = appLinkIntent.getAction();
		Uri appLinkUri = appLinkIntent.getData();

		if(appLinkUri==null){
			//nothing to do
			return false;
		}else{
			return onDeepLinkDetected(appLinkUri);
		}


	}

	public void showAppReviewPromptActivity(boolean blnForceShow){

		//AppUtils.showCustomToast(ObjAppUser.instance().toXML(), false);

		//check if the user already was presented with this dialog - and if so, what their response was
		String strPreviousAppReviewResponse = ObjAppUser.instance().getString(ObjAppUser.FIELD_APP_REVIEW_PROMPT_RESPONSE);

		//if user has already seen this dialog and responded to it ... check if there is a point showing it again:
		if(blnForceShow==false) {
			if (ObjAppUser.CONST_RATE_US_PROMPT_ACCEPTED.equals(strPreviousAppReviewResponse)
					|| ObjAppUser.CONST_RATE_US_PROMPT_DECLINED.equals(strPreviousAppReviewResponse)
					|| ObjAppUser.CONST_THUMBS_DOWN.equals(strPreviousAppReviewResponse)
					) {
				//Review (likely) given or declined to review the app - or did not like the app previously
				return;
			}
		}


		Intent intent = new Intent(this, Alert.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		//intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION); //this makes sure activities do not slide in from right to left when started, but simply appear
		startActivityForResult(intent, 0);


		//store the date of this ..
		String strSystemDate = DateUtils.dateToInternalString(new Date());
		ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_APP_REVIEW_PROMPT_DISPLAY_DATE, strSystemDate);


	}

}
