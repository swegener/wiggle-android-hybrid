package uk.co.wiggle.hybrid.application.persistence;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Date;

import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.logging.Logger;

/**
 * 
 * @author AndroMedia
 * 
 * This class contains handler for writing / reading data to a data store (File, DB, etc...)
 *
 */

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class DataStore {
	
	private static String logTag = "DataStore";
	
	public static final String CACHE_FILE_FAVOURITES = "CACHE_FILE_FAVOURITES";
	public static final String SHARED_PREF_ETAG_STORE = "SHARED_PREF_ETAG_STORE";


	public static boolean isLocalDataOutdated(String strCountryCode, String strURLEndpoint, long lngServerLastModifiedDate, String strServerETag){
		
		String filename = buildFilename(strCountryCode, strURLEndpoint);
		
		File targetFile = new File(ApplicationContextProvider.getContext().getFilesDir() + "/" + filename);
		
		//Evaluate ETag first
		if (strServerETag != null) {
        	String strLocalETag = getStringFromSharedPreferences(SHARED_PREF_ETAG_STORE, strCountryCode + "_" + strURLEndpoint);
        	if(!strServerETag.equals(strLocalETag)){
        		//AppUtils.showCustomToast("Cache for " + strCountryCode + "_" + strURLEndpoint + " data out of date (ETag changed / missing locally)", true);
        		deleteCacheFile(strCountryCode, strURLEndpoint); //remove cache file if it is outdated (ETag changed on server)
        		//server Etag is not saved here as request may still go wrong at this point. ETags are saved on successful receipt of a response. (DubizzleRequestHandler.saveETagIfAvailable()) 
        		return true;
        	}
        }   
		
		//If ETag not available - check last modified date
		long timeDifferenceInMillis;
		Date dtFileLastModified;
		
		if (targetFile.exists()) {
			dtFileLastModified = new Date(targetFile.lastModified());
		}else{
			//AppUtils.showCustomToast("Cache for " + strCountryCode + "_" + strURLEndpoint + " data out of date (no cache file yet)", true);
			return true; //get new data if this cache file does not exist
		}
		
		long lngFileLastModified = dtFileLastModified.getTime();
		//For simplicity we are not taking timezone into consideration for now - we can live with being a few hours out of date for now.
		//TODO: add server timezone or store last modified date with cached data and use the latter for comparison
		timeDifferenceInMillis = lngServerLastModifiedDate - lngFileLastModified; 
		
		if(timeDifferenceInMillis>0){
			deleteCacheFile(targetFile); //remove cache file if it is outdated (last modified date on server more recent than local copy)
			//AppUtils.showCustomToast("Cache for " + strCountryCode + "_" + strURLEndpoint + " data out of date (Server Last modified date more recent)", true);
			return true;
		}
		
		//AppUtils.showCustomToast("Cache for " + strCountryCode + "_" + strURLEndpoint + " still up to date. Using cache file.", true);
		return false;
		
	}
	
	public static void writeJSONResponseToFile(String strCountryCode, String strURLEndpoint, JSONObject jsonPayload) {
		
		final String filename = buildFilename(strCountryCode, strURLEndpoint);
		
		final String incomingJSON = jsonPayload.toString();
		
		//save the file async so that we do not prevent other processes from running
		Runnable r = new Runnable()
		{
		    @Override
		    public void run()
		    {
		    	
		    	FileOutputStream outputStream;
				
				try {
					  outputStream = ApplicationContextProvider.getContext().openFileOutput(filename, Context.MODE_PRIVATE);
					  outputStream.write(incomingJSON.getBytes());
					  outputStream.close();
				} catch (Exception e) {
					  e.printStackTrace();
				}
			    
		    }
		};
		
		Thread saveFileThread = new Thread(r);
		saveFileThread.start();

	}
	
	public static void writeStringResponseToFile(String strCountryCode, String strURLEndpoint, final String strResponseData) {
		
		final String filename = buildFilename(strCountryCode, strURLEndpoint);
		
		final String strEncoding = AppRequestHandler.instance().getEndpointEncoding(strURLEndpoint);
		
		//save the file async so that we do not prevent other processes from running
		Runnable r = new Runnable()
		{;
		    @Override
		    public void run()
		    {
		    	
		    	FileOutputStream outputStream;
				
				try {
					  outputStream = ApplicationContextProvider.getContext().openFileOutput(filename, Context.MODE_PRIVATE);
					  //outputStream.write(strResponseData.getBytes(strEncoding));
					  outputStream.write(strResponseData.getBytes()); //write file as it comes from the web - if encoding is needed, do this when response is read (same as for response from web)
					  outputStream.close();
				} catch (Exception e) {
					  e.printStackTrace();
				}
			    
		    }
		};
		
		Thread saveFileThread = new Thread(r);
		saveFileThread.start();

	}
	
	public static void writeByteResponseToFile(String strCountryCode, String strURLEndpoint, final byte[] jsonPayload) {
		
		final String filename = buildFilename(strCountryCode, strURLEndpoint);
		
		//save the file async so that we do not prevent other processes from running
		Runnable r = new Runnable()
		{
		    @Override
		    public void run()
		    {
		    	
		    	FileOutputStream outputStream;
				
				try {
					  outputStream = ApplicationContextProvider.getContext().openFileOutput(filename, Context.MODE_PRIVATE);
					  outputStream.write(jsonPayload);
					  outputStream.close();
				} catch (Exception e) {
					  e.printStackTrace();
				}
			    
		    }
		};
		
		Thread saveFileThread = new Thread(r);
		saveFileThread.start();

	}	
	
	
	public static String readFileAsString(String strCountryCode, String strURLEndpoint, String strEncoding){
		
		String strFileContent = "";
		String filename = buildFilename(strCountryCode, strURLEndpoint);
		
		File targetFile = new File(ApplicationContextProvider.getContext().getFilesDir() + "/" + filename);
		
		//no cache file? Return ... 
		if(!targetFile.exists()){
			return strFileContent;
		}
		
		// access token does not expire unless logout endpoint is called
		//Only used for cached access token: if the data file is out of date - tell the caller that it needs refreshing
		/*if(DubizzleRequestHandler.ENDPOINT_AUTHENTICATE.equals(strURLEndpoint) && isAccessTokenOutdated(targetFile)){
			return strFileContent;
		}*/
		
		try {
			
	        InputStream inputStream = ApplicationContextProvider.getContext().openFileInput(filename);

	        if ( inputStream != null ) {
	        	
	            //InputStreamReader inputStreamReader = new InputStreamReader(inputStream, strEncoding);
	        	InputStreamReader inputStreamReader = new InputStreamReader(inputStream); //no encoding here - convert to appropriate encoding on display
	            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	            String buffer = "";
	            StringBuilder stringBuilder = new StringBuilder();

	            while ( (buffer = bufferedReader.readLine()) != null ) {
	                stringBuilder.append(buffer);
	            }

	            inputStream.close();
	            strFileContent = stringBuilder.toString();
	        }
	    }
	    catch (FileNotFoundException e) {
	    	Logger.printMessage(logTag, "File " + filename + " not found", Logger.WARN);
	    	return strFileContent;
	    } catch (IOException e) {
	    	Logger.printMessage(logTag, "Can not read file " + filename + " due to " + e.toString(), Logger.WARN);
	    	return strFileContent;
	    }
		
		return strFileContent;
		
	}
	
	
	public static byte[] readFileAsStream(String strCountryCode, String strURLEndpoint){
		
		byte[] fileContent = null;
		String filename = buildFilename(strCountryCode, strURLEndpoint);
				
		File targetFile = new File(ApplicationContextProvider.getContext().getFilesDir() + "/" + filename);
		
		//no cache file? Return ... 
		if(!targetFile.exists()){
			return fileContent;
		}
		
		try {
			
	        InputStream inputStream = ApplicationContextProvider.getContext().openFileInput(filename);
	        fileContent = getBytes(inputStream);
	        
	    }
	    catch (FileNotFoundException e) {
	    	Logger.printMessage(logTag, "File " + filename + " not found", Logger.WARN);
	    	return fileContent;
	    } catch (IOException e) {
	    	Logger.printMessage(logTag, "Can not read file " + filename + " due to " + e.toString(), Logger.WARN);
	    	return fileContent;
	    }
		
		return fileContent;
		
	}
	

	public static byte[] getBytes(InputStream is) throws IOException {

	    int len;
	    int size = 1024;
	    byte[] buf;
	
	    if(is instanceof ByteArrayInputStream){
	    	
	      size = is.available();
	      buf = new byte[size];
	      len = is.read(buf, 0, size);
	      
	    }else{
	    	
	      ByteArrayOutputStream bos = new ByteArrayOutputStream();
	      buf = new byte[size];
	      
	      while((len = is.read(buf, 0, size)) != -1){
	        bos.write(buf, 0, len);
	      }
	      buf = bos.toByteArray();
	    }
	    
	    return buf;
    
	}
	
	
	private static boolean deleteCacheFile(File targetFile){
		
		boolean deleted = false;
		
		if(targetFile.exists()){
			deleted = targetFile.delete();
		}
		
		return deleted;
		
	}
	
	public static boolean deleteCacheFile(String strCountryCode, String strURLEndpoint){
		
		String filename = buildFilename(strCountryCode, strURLEndpoint);
		
		//Clear potential ETag associated with this cache file
		saveStringToSharedPreferences(SHARED_PREF_ETAG_STORE, strCountryCode + "_" + strURLEndpoint, "");
		File targetFile = new File(ApplicationContextProvider.getContext().getFilesDir() + "/" + filename);
		return deleteCacheFile(targetFile);
		
	}
	
	
	public static void writeStringToFile(final String filename, final String incomingJSON) {
		
		//save the file async so that we do not prevent other processes from running
		Runnable r = new Runnable()
		{
		    @Override
		    public void run()
		    {
		    	
		    	FileOutputStream outputStream;
				
				try {
					  outputStream = ApplicationContextProvider.getContext().openFileOutput(filename, Context.MODE_PRIVATE);
					  outputStream.write(incomingJSON.getBytes());
					  outputStream.close();
				} catch (Exception e) {
					  e.printStackTrace();
				}
			    
		    }
		};
		
		Thread saveFileThread = new Thread(r);
		saveFileThread.start();

	}	
	
	
	/* Method can be used to store simple strings to (private) shared preferences on application level */
	public static void saveStringToSharedPreferences(String strPreferenceFile, String strKey, String strValue){
		
		SharedPreferences sharedPreferences = ApplicationContextProvider.getContext().getSharedPreferences(strPreferenceFile, ApplicationContextProvider.MODE_PRIVATE);
	    Editor editor = sharedPreferences.edit();
	    editor.putString(strKey, strValue);
	    editor.apply(); //asynchronous
		
	}
	
	/* Method can be used to retrieve a String from (private) shared preferences - "" is returned as default value */
	public static String getStringFromSharedPreferences(String strPreferenceFile, String strKey){
		
		SharedPreferences sharedPreferences = ApplicationContextProvider.getContext().getSharedPreferences(strPreferenceFile, ApplicationContextProvider.MODE_PRIVATE);
		String strValue = sharedPreferences.getString(strKey, "");
		return strValue;
		
	}
	
	
	private static String buildFilename(String strCountryCode, String strURLEndpoint){
		
		String filename = strURLEndpoint;
		
		//for RSS feeds remove other stuff as well (http:// / https://)
		if(strURLEndpoint.contains("http")){
			try {
				URI uri = new URI(strURLEndpoint);
				filename = uri.getHost() + uri.getPath();
			} catch (URISyntaxException e) {
				Logger.printMessage(logTag, "buildFilename: URI Parsing failed", Logger.WARN);
			}
		}
		
		//default for any endpoint
		filename = filename.replaceAll("/", "");
		
		//additional stuff for RSS cache files
		filename = filename.replaceAll(".xml", "");
		filename = filename.replaceAll("-", "");
		filename = filename.replaceAll("\\.", ""); //needs escaping
		
		//creates a file name where the country code is used as file directory - the URL Endpoint with removed slashes is the file name
		if(strCountryCode!=null && !"".equals(strCountryCode)){
			filename = strCountryCode + "_" + filename;
		}
		
		return filename;
		
	}
	
	
	/*
	 * We still store to the kombi access token in a local file for a number of hourse to avoid
	 * repeated calls to Kombi for authentication. 
	 * 
	 * The access token gets invalidated after a period - so we need to tell the caller to remove the cached token 
	 * and ask for a new one where needed
	 * 
	 */
	private static boolean isAccessTokenOutdated(File file){
		
		int intStoredDataExpiresInHours = 1; //cache token for 1 hour (just for convenience and to avoid repeated calls within the same session)
		
		long timeDifferenceInMillis;
		Date dtNow = new Date(System.currentTimeMillis());
		Date dtFileLastModified = dtNow;
		
		long lngNow = dtNow.getTime();
		
		if (file.exists()) {
			dtFileLastModified = new Date(file.lastModified());	
		}else{
			return false;
		}
		
		long lngFileLastModified = dtFileLastModified.getTime();
		timeDifferenceInMillis = lngNow - lngFileLastModified;
		
		if(timeDifferenceInMillis>1000 * 60 * 60 * intStoredDataExpiresInHours){
			return true;
		}else{
			return false;
		}
		
		
	}	
	
	
	public static String[] readRawValueList(int intRawFileID){
		
		Logger.printMessage("readRawValueList", "Start" , Logger.INFO);
		
		Context context = ApplicationContextProvider.getContext();

		//read raw-file
		try {
			
			Logger.printMessage("readRawValueList", "Read raw file" , Logger.INFO);
			
			  InputStream inputStream = context.getResources().openRawResource(intRawFileID);
	
		        if ( inputStream != null ) {
		        	
		            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		            String buffer = "";
		            StringBuilder stringBuilder = new StringBuilder();
	
		            while ( (buffer = bufferedReader.readLine()) != null ) {
		            	
		                stringBuilder.append(buffer + "\n"); //appending  \n as buffered reader eliminates end of line characters and we need to preserve them here
		            }
	
		            inputStream.close();
		            String strRawFileContent = stringBuilder.toString();
		            String lines[] = strRawFileContent.split("\n"); //new line separated values
		            return lines;
		            
		        }			
			
			Logger.printMessage("readRawValueList", "Read raw file - end" , Logger.INFO);
	        
	    }
	    catch (FileNotFoundException e) {
	    	Logger.printMessage(logTag, "Raw file " + intRawFileID + " not found in raw folder", Logger.WARN);
	    	return null;
	    } catch (IOException e) {
	    	Logger.printMessage(logTag, "Can not read raw file " + intRawFileID + " due to " + e.toString(), Logger.WARN);
	    	return null;
	    }
		
		Logger.printMessage("readRawValueList", "End" , Logger.INFO);
		return null;
		
	}	
	
	
}
