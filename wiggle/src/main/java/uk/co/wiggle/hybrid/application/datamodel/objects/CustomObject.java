package uk.co.wiggle.hybrid.application.datamodel.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import uk.co.wiggle.hybrid.application.logging.Logger;


import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public abstract class CustomObject{
	
	private static final String TAG = "CustomObject";
	
	public static final String FIELD_ID = "id";
	
	public int getUniqueId(){
		return getInt(FIELD_ID);
	}
	
	public static final String FIELD_URI = "uri"; //unique URI ID for this object
	
	public String getURI(){
		return getString(FIELD_URI);
	}
		
	
	public static final int FIELD_TYPE_OBJECT = 0;
	public static final int FIELD_TYPE_INTEGER = 1;
	public static final int FIELD_TYPE_STRING = 2;
	public static final int FIELD_TYPE_FLOAT = 3;
	public static final int FIELD_TYPE_DOUBLE = 4;
	public static final int FIELD_TYPE_DATETIME = 5;
	public static final int FIELD_TYPE_BOOLEAN = 6;
	public static final int FIELD_TYPE_LONG = 7;

	
	/** 
	 * Storage is delegated to an inner bundle which can be used for serialization between Activities.
	 */
	private Bundle mBundle = new Bundle();
	
	public CustomObject() {
		
	}
	
	public void setObject(String key, CustomObject value) {
		mBundle.putBundle(key, value.getBundle());
	}
	
	public void setBundle(Bundle value){
		mBundle = value;
	}
	
	
	public CustomObject getObject(String strEntityKey) {
		
		if(strEntityKey.equals(ObjNewsfeed.OBJECT_NAME)){
			return new ObjNewsfeed(mBundle.getBundle(strEntityKey));
		}else if(strEntityKey.equals(ObjNewsfeedCategory.OBJECT_NAME)){
			return new ObjNewsfeedCategory(mBundle.getBundle(strEntityKey));
		}else if(strEntityKey.equals(ObjNewsfeedRSSChannel.OBJECT_NAME)){
			return new ObjNewsfeedRSSChannel(mBundle.getBundle(strEntityKey));		
		}else if(strEntityKey.equals(ObjNewsfeedRSSItem.OBJECT_NAME)){
			return new ObjNewsfeedRSSItem(mBundle.getBundle(strEntityKey));	
		}else{
			//fallback
			return null;
		}

		
	}
	
	
	public CustomObject(Bundle bundle) {
		mBundle = bundle;
	}

	/**
	 * @return the mBundle
	 */
	public Bundle getBundle() {
		return mBundle;
	}

	/** Must be implemented to return the desired name of this object. */
	abstract public String getObjectName();
	
	/** Must be implemented to return the desired fields of this object. */
	abstract public List<String> getFields();
	
	/** Must be implemented to return the desired fields of this object. */
	abstract public Class<?> getSubclass();
	
	/** Must be implemented to return the appropriate type of field */
	abstract public Integer getFieldType(String fieldName);

	/** Generic setter for all fields */
	public void setField(String name, String value) {
		
		//workaround for empty strings returned by API - do not set fields if API returns "null", "(null)"
		//and even "False" for non-boolean values
		if(value!=null && 
				(value.equals("null") || value.equals("(null)") || value.equals("False"))){
			return;
		}			
		
		Integer type = getFieldType(name);
		if (type == null || value == null) {
			Logger.printMessage(TAG, "Unknown field: " + name + " not setting!", Logger.WARN);			
		} else {
			switch(type) {
			case FIELD_TYPE_STRING:
				putString(name, value);
				break;

			case FIELD_TYPE_BOOLEAN:
				putBoolean(name, ObjectTypeHelper.parseBoolean(value));
				break;
				
			case FIELD_TYPE_INTEGER:
				if(value==null || value.equals("")){
					break;
				}
				putInt(name, Integer.valueOf(value));
				break;				
			case FIELD_TYPE_FLOAT:
				putFloat(name, Float.parseFloat(value));
				break;
				
			case FIELD_TYPE_DOUBLE:
				putDouble(name, Double.parseDouble(value));
				break;

			case FIELD_TYPE_DATETIME:
				Logger.printMessage(TAG, "DATETIME NOT IMPLEMENTED", Logger.WARN);
				break;
				
			case FIELD_TYPE_OBJECT:
				//dummy type - no warning to be raised
				break;
				
			case FIELD_TYPE_LONG:
				if(value==null || value.equals("")){
					break;
				}
				putLong(name, Long.valueOf(value));
				break;	

			default:
				Logger.printMessage(TAG, "TYPE #" + type + " NOT IMPLEMENTED", Logger.WARN);
			}
		}
	}
	
	/** Specific setters for non-string fields if needed*/
	public void setField(String name, Boolean value) {
		Integer type = getFieldType(name);
		if (type == null) {
			Logger.printMessage(TAG, "Unknown field (name/Boolean): " + name + " not setting!", Logger.WARN);
		} else {
			switch(type) {
			case FIELD_TYPE_BOOLEAN:
				putBoolean(name, value);
				break;

			default:
				Logger.printMessage(TAG, "TYPE (name/Boolean) #" + type + " NOT IMPLEMENTED", Logger.WARN);
			}
		}
	}	
	

	public void setField(String name, Integer value) {
		Integer type = getFieldType(name);
		if (type == null) {
			Logger.printMessage(TAG, "Unknown field (name/Integer): " + name + " not setting!", Logger.WARN);
		} else {
			switch(type) {
			case FIELD_TYPE_BOOLEAN:
				putInt(name, value);
				break;

			default:
				Logger.printMessage(TAG, "TYPE (name/Integer) #" + type + " NOT IMPLEMENTED", Logger.WARN);
			}
		}
	}
	
	public void setField(String name, int value) {
		Integer type = getFieldType(name);
		if (type == null) {
			Logger.printMessage(TAG, "Unknown field (name/int): " + name + " not setting!", Logger.WARN);
		} else {
			switch(type) {
				
			case FIELD_TYPE_INTEGER:
				putInt(name, value);
				break;
				
			case FIELD_TYPE_BOOLEAN:
				putBoolean(name, ObjectTypeHelper.parseBoolean(value));
				break;
				
			case FIELD_TYPE_LONG:
				putLong(name, value);
				break;				
				
			default:
				Logger.printMessage(TAG, "TYPE (name/int) #" + type + " NOT IMPLEMENTED", Logger.WARN);
			}
		}
	}	
	
	public void setField(String name, long value) {
		Integer type = getFieldType(name);
		if (type == null) {
			Logger.printMessage(TAG, "Unknown field (name/int): " + name + " not setting!", Logger.WARN);
		} else {
			switch(type) {
				
			case FIELD_TYPE_LONG:
				putLong(name, value);
				break;
				
			default:
				Logger.printMessage(TAG, "TYPE (name/int) #" + type + " NOT IMPLEMENTED", Logger.WARN);
			}
		}
	}	
	
	public void setField(String name, float value) {
		Integer type = getFieldType(name);
		if (type == null) {
			Logger.printMessage(TAG, "Unknown field (name/float): " + name + " not setting!", Logger.WARN);
		} else {
			switch(type) {
				
			case FIELD_TYPE_FLOAT:
				putFloat(name, value);
				break;
				
			default:
				Logger.printMessage(TAG, "TYPE (name/float) #" + type + " NOT IMPLEMENTED", Logger.WARN);
			}
		}
	}
	
	public void setField(String name, double value) {
		Integer type = getFieldType(name);
		if (type == null) {
			Logger.printMessage(TAG, "Unknown field (name/double): " + name + " not setting!", Logger.WARN);
		} else {
			switch(type) {
				
			case FIELD_TYPE_DOUBLE:
				putDouble(name, value);
				break;

			default:
				Logger.printMessage(TAG, "TYPE (name/double) #" + type + " NOT IMPLEMENTED", Logger.WARN);
			}
		}
	}	
	

	
	
	/**
	 * 
	 * @see android.os.Bundle#clear()
	 */
	public void clear() {
		mBundle.clear();
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#containsKey(java.lang.String)
	 */
	public boolean containsKey(String key) {
		return mBundle.containsKey(key);
	}

	/**
	 * @return
	 * @see android.os.Bundle#describeContents()
	 */
	public int describeContents() {
		return mBundle.describeContents();
	}

	/**
	 * @param o
	 * @return
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		return mBundle.equals(o);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#get(java.lang.String)
	 */
	public Object get(String key) {
		return mBundle.get(key);
	}

	/**
	 * @param key
	 * @param defaultValue
	 * @return
	 * @see android.os.Bundle#getBoolean(java.lang.String, boolean)
	 */
	public boolean getBoolean(String key, boolean defaultValue) {
		return mBundle.getBoolean(key, defaultValue);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getBoolean(java.lang.String)
	 */
	public boolean getBoolean(String key) {
		return mBundle.getBoolean(key);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getBooleanArray(java.lang.String)
	 */
	public boolean[] getBooleanArray(String key) {
		return mBundle.getBooleanArray(key);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getBundle(java.lang.String)
	 */
	public Bundle getBundle(String key) {
		return mBundle.getBundle(key);
	}

	/**
	 * @param key
	 * @param defaultValue
	 * @return
	 * @see android.os.Bundle#getByte(java.lang.String, byte)
	 */
	public Byte getByte(String key, byte defaultValue) {
		return mBundle.getByte(key, defaultValue);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getByte(java.lang.String)
	 */
	public byte getByte(String key) {
		return mBundle.getByte(key);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getByteArray(java.lang.String)
	 */
	public byte[] getByteArray(String key) {
		return mBundle.getByteArray(key);
	}

	/**
	 * @param key
	 * @param defaultValue
	 * @return
	 * @see android.os.Bundle#getChar(java.lang.String, char)
	 */
	public char getChar(String key, char defaultValue) {
		return mBundle.getChar(key, defaultValue);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getChar(java.lang.String)
	 */
	public char getChar(String key) {
		return mBundle.getChar(key);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getCharArray(java.lang.String)
	 */
	public char[] getCharArray(String key) {
		return mBundle.getCharArray(key);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getCharSequence(java.lang.String)
	 */
	public CharSequence getCharSequence(String key) {
		return mBundle.getCharSequence(key);
	}

	/**
	 * @param key
	 * @param defaultValue
	 * @return
	 * @see android.os.Bundle#getDouble(java.lang.String, double)
	 */
	public double getDouble(String key, double defaultValue) {
		return mBundle.getDouble(key, defaultValue);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getDouble(java.lang.String)
	 */
	public double getDouble(String key) {
		return mBundle.getDouble(key);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getDoubleArray(java.lang.String)
	 */
	public double[] getDoubleArray(String key) {
		return mBundle.getDoubleArray(key);
	}

	/**
	 * @param key
	 * @param defaultValue
	 * @return
	 * @see android.os.Bundle#getFloat(java.lang.String, float)
	 */
	public float getFloat(String key, float defaultValue) {
		return mBundle.getFloat(key, defaultValue);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getFloat(java.lang.String)
	 */
	public float getFloat(String key) {
		return mBundle.getFloat(key);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getFloatArray(java.lang.String)
	 */
	public float[] getFloatArray(String key) {
		return mBundle.getFloatArray(key);
	}

	/**
	 * @param key
	 * @param defaultValue
	 * @return
	 * @see android.os.Bundle#getInt(java.lang.String, int)
	 */
	public int getInt(String key, int defaultValue) {
		return mBundle.getInt(key, defaultValue);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getInt(java.lang.String)
	 */
	public int getInt(String key) {
		return mBundle.getInt(key);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getIntArray(java.lang.String)
	 */
	public int[] getIntArray(String key) {
		return mBundle.getIntArray(key);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getIntegerArrayList(java.lang.String)
	 */
	public ArrayList<Integer> getIntegerArrayList(String key) {
		return mBundle.getIntegerArrayList(key);
	}

	/**
	 * @param key
	 * @param defaultValue
	 * @return
	 * @see android.os.Bundle#getLong(java.lang.String, long)
	 */
	public long getLong(String key, long defaultValue) {
		return mBundle.getLong(key, defaultValue);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getLong(java.lang.String)
	 */
	public long getLong(String key) {
		return mBundle.getLong(key);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getLongArray(java.lang.String)
	 */
	public long[] getLongArray(String key) {
		return mBundle.getLongArray(key);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getParcelable(java.lang.String)
	 */
	public <T extends Parcelable> T getParcelable(String key) {
		return mBundle.getParcelable(key);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getParcelableArray(java.lang.String)
	 */
	public Parcelable[] getParcelableArray(String key) {
		return mBundle.getParcelableArray(key);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getParcelableArrayList(java.lang.String)
	 */
	public <T extends Parcelable> ArrayList<T> getParcelableArrayList(String key) {
		return mBundle.getParcelableArrayList(key);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getSerializable(java.lang.String)
	 */
	public Serializable getSerializable(String key) {
		return mBundle.getSerializable(key);
	}

	/**
	 * @param key
	 * @param defaultValue
	 * @return
	 * @see android.os.Bundle#getShort(java.lang.String, short)
	 */
	public short getShort(String key, short defaultValue) {
		return mBundle.getShort(key, defaultValue);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getShort(java.lang.String)
	 */
	public short getShort(String key) {
		return mBundle.getShort(key);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getShortArray(java.lang.String)
	 */
	public short[] getShortArray(String key) {
		return mBundle.getShortArray(key);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getSparseParcelableArray(java.lang.String)
	 */
	public <T extends Parcelable> SparseArray<T> getSparseParcelableArray(
			String key) {
		return mBundle.getSparseParcelableArray(key);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getString(java.lang.String)
	 */
	public String getString(String key) {
		//AndroMedia - return empty string. This makes it easier to handle NULL display values across the app
		if(mBundle.getString(key)==null){
			return "";
		}else{
			return mBundle.getString(key);
		}
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getStringArray(java.lang.String)
	 */
	public String[] getStringArray(String key) {
		return mBundle.getStringArray(key);
	}

	/**
	 * @param key
	 * @return
	 * @see android.os.Bundle#getStringArrayList(java.lang.String)
	 */
	public ArrayList<String> getStringArrayList(String key) {
		return mBundle.getStringArrayList(key);
	}

	/**
	 * @return
	 * @see android.os.Bundle#hasFileDescriptors()
	 */
	public boolean hasFileDescriptors() {
		return mBundle.hasFileDescriptors();
	}

	/**
	 * @return
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		return mBundle.hashCode();
	}

	/**
	 * @return
	 * @see android.os.Bundle#isEmpty()
	 */
	public boolean isEmpty() {
		return mBundle.isEmpty();
	}

	/**
	 * @return
	 * @see android.os.Bundle#keySet()
	 */
	public Set<String> keySet() {
		return mBundle.keySet();
	}

	/**
	 * @param map
	 * @see android.os.Bundle#putAll(android.os.Bundle)
	 */
	public void putAll(Bundle map) {
		mBundle.putAll(map);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putBoolean(java.lang.String, boolean)
	 */
	public void putBoolean(String key, boolean value) {
		mBundle.putBoolean(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putBooleanArray(java.lang.String, boolean[])
	 */
	public void putBooleanArray(String key, boolean[] value) {
		mBundle.putBooleanArray(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putBundle(java.lang.String, android.os.Bundle)
	 */
	public void putBundle(String key, Bundle value) {
		mBundle.putBundle(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putByte(java.lang.String, byte)
	 */
	public void putByte(String key, byte value) {
		mBundle.putByte(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putByteArray(java.lang.String, byte[])
	 */
	public void putByteArray(String key, byte[] value) {
		mBundle.putByteArray(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putChar(java.lang.String, char)
	 */
	public void putChar(String key, char value) {
		mBundle.putChar(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putCharArray(java.lang.String, char[])
	 */
	public void putCharArray(String key, char[] value) {
		mBundle.putCharArray(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putCharSequence(java.lang.String, java.lang.CharSequence)
	 */
	public void putCharSequence(String key, CharSequence value) {
		mBundle.putCharSequence(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putDouble(java.lang.String, double)
	 */
	public void putDouble(String key, double value) {
		mBundle.putDouble(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putDoubleArray(java.lang.String, double[])
	 */
	public void putDoubleArray(String key, double[] value) {
		mBundle.putDoubleArray(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putFloat(java.lang.String, float)
	 */
	public void putFloat(String key, float value) {
		mBundle.putFloat(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putFloatArray(java.lang.String, float[])
	 */
	public void putFloatArray(String key, float[] value) {
		mBundle.putFloatArray(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putInt(java.lang.String, int)
	 */
	public void putInt(String key, int value) {
		mBundle.putInt(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putIntArray(java.lang.String, int[])
	 */
	public void putIntArray(String key, int[] value) {
		mBundle.putIntArray(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putIntegerArrayList(java.lang.String, java.util.ArrayList)
	 */
	public void putIntegerArrayList(String key, ArrayList<Integer> value) {
		mBundle.putIntegerArrayList(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putLong(java.lang.String, long)
	 */
	public void putLong(String key, long value) {
		mBundle.putLong(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putLongArray(java.lang.String, long[])
	 */
	public void putLongArray(String key, long[] value) {
		mBundle.putLongArray(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putParcelable(java.lang.String, android.os.Parcelable)
	 */
	public void putParcelable(String key, Parcelable value) {
		mBundle.putParcelable(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putParcelableArray(java.lang.String, android.os.Parcelable[])
	 */
	public void putParcelableArray(String key, Parcelable[] value) {
		mBundle.putParcelableArray(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putParcelableArrayList(java.lang.String, java.util.ArrayList)
	 */
	public void putParcelableArrayList(String key,
			ArrayList<? extends Parcelable> value) {
		mBundle.putParcelableArrayList(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putSerializable(java.lang.String, java.io.Serializable)
	 */
	public void putSerializable(String key, Serializable value) {
		mBundle.putSerializable(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putShort(java.lang.String, short)
	 */
	public void putShort(String key, short value) {
		mBundle.putShort(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putShortArray(java.lang.String, short[])
	 */
	public void putShortArray(String key, short[] value) {
		mBundle.putShortArray(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putSparseParcelableArray(java.lang.String, android.util.SparseArray)
	 */
	public void putSparseParcelableArray(String key,
			SparseArray<? extends Parcelable> value) {
		mBundle.putSparseParcelableArray(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putString(java.lang.String, java.lang.String)
	 */
	public void putString(String key, String value) {
		mBundle.putString(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putStringArray(java.lang.String, java.lang.String[])
	 */
	public void putStringArray(String key, String[] value) {
		mBundle.putStringArray(key, value);
	}

	/**
	 * @param key
	 * @param value
	 * @see android.os.Bundle#putStringArrayList(java.lang.String, java.util.ArrayList)
	 */
	public void putStringArrayList(String key, ArrayList<String> value) {
		mBundle.putStringArrayList(key, value);
	}

	/**
	 * @param parcel
	 * @see android.os.Bundle#readFromParcel(android.os.Parcel)
	 */
	public void readFromParcel(Parcel parcel) {
		mBundle.readFromParcel(parcel);
	}

	/**
	 * @param key
	 * @see android.os.Bundle#remove(java.lang.String)
	 */
	public void remove(String key) {
		mBundle.remove(key);
	}

	/**
	 * @param loader
	 * @see android.os.Bundle#setClassLoader(java.lang.ClassLoader)
	 */
	public void setClassLoader(ClassLoader loader) {
		mBundle.setClassLoader(loader);
	}

	/**
	 * @return
	 * @see android.os.Bundle#size()
	 */
	public int size() {
		return mBundle.size();
	}

	/**
	 * @param parcel
	 * @param flags
	 * @see android.os.Bundle#writeToParcel(android.os.Parcel, int)
	 */
	public void writeToParcel(Parcel parcel, int flags) {
		mBundle.writeToParcel(parcel, flags);
	}
	
	/**
	 * Creates an XML representation of this object.
	 * @return xml
	 */
	public String toXML() {
		Set<String> keys = mBundle.keySet();
		String name = getObjectName();
		StringBuilder builder = new StringBuilder();
		
		builder.append("<");
		builder.append(name);
		
		for (String key : keys) {
			builder.append(" ");
			Object value = mBundle.get(key);
			builder.append(key);
			builder.append("=\"");
			builder.append(String.valueOf(value));
			builder.append("\"");
		}
		
		builder.append("/>");
		return builder.toString();
	}
	
	@Override
	public String toString() {
		return toXML();
	}




	public static JSONObject createJSONfromObject(CustomObject object , CustomObject targetObject) throws JSONException {

		List<String> fields = targetObject.getFields();

		JSONObject jsonObject = new JSONObject();

		// first do the flat fields
		for (String strFieldName : fields) {

			if(object.get(strFieldName) != null){

				int intValueFieldType = targetObject.getFieldType(strFieldName);

				switch (intValueFieldType) {

					case CustomObject.FIELD_TYPE_OBJECT:
						break;

					case CustomObject.FIELD_TYPE_DATETIME:
						break;

					default:
						jsonObject.put(strFieldName, object.get(strFieldName));
						break;
				}


			}

		}

		return jsonObject;

	}



}