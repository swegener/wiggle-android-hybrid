package uk.co.wiggle.hybrid.tagging;

/**
 *
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 *
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 *
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class TaggingGlobals {


    /*
        The following constants are used for SmartLink analysis (deep linking - for details see AndroidManifest.xml)
     */
    public static final String REFERRER_NAME = "android.intent.extra.REFERRER_NAME";

    public static final String KEY_REFERRER_MEDIUM = "referrerMedium";
    public static final String KEY_REFERRER_TYPE= "referrerType";
    public static final String KEY_REFERRER_DETAIL= "referrerDetail";

    //Referrer Medium (None, Organic, Referral)
    public static final String REFERRER_MEDIUM_NONE = "none";
    public static final String REFERRER_MEDIUM_DIRECT = "direct";
    public static final String REFERRER_MEDIUM_ORGANIC = "organic";
    public static final String REFERRER_MEDIUM_REFERRAL = "referral";

    //Referrer type
    public static final String REFERRER_DIRECT_OPEN = "Direct open";
    public static final String REFERRER_ORGANIC_BROWSER_GOOGLE = "Browser (google.com)";
    public static final String REFERRER_OTHER_WEBSITE = "Browser (other website)";
    public static final String REFERRER_GOOGLE_SEARCH_ANDROID_APP = "Google Search App";
    public static final String REFERRER_OTHER_ANDROID_APP = "Other Android App";


    //Medium param for UTM Tracking
    public static final String UTM_MEDIUM_PARAM = "utm_medium";
    public static final String UTM_REFERRER_PARAM = "utm_source";


    public static final String QUICK_SEARCH_BOX = "com.google.android.googlequicksearchbox";
    public static final String APP_CRAWLER = "com.google.appcrawler";


}
