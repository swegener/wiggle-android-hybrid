package uk.co.wiggle.hybrid.application.datamodel.objectmanagers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesItem;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesOrder;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;


public class ObjSalesOrderManager {
	
	List<ObjSalesOrder> mSalesOrders = Collections.synchronizedList(new ArrayList<ObjSalesOrder>());
	
	private static ObjSalesOrderManager sInstance = null;

	public static ObjSalesOrderManager getInstance() {
		if (sInstance == null) {
			sInstance = new ObjSalesOrderManager();
		}
		return sInstance;
	}

	protected ObjSalesOrderManager() {
		mSalesOrders = Collections.synchronizedList(new ArrayList<ObjSalesOrder>());
	}
	
	
	/**
	 * @param location
	 * @param object
	 * @see java.util.List#add(int, java.lang.Object)
	 */
	public void add(int location, ObjSalesOrder object) {
		mSalesOrders.add(location, object);
	}
	
	
	/**
	 * @param object
	 * @return
	 * @see java.util.List#add(java.lang.Object)
	 */
	public boolean add(ObjSalesOrder object) {
		return mSalesOrders.add(object);
	}

	/**
	 * @param arg0
	 * @return
	 * @see java.util.List#addAll(java.util.Collection)
	 */
	public boolean addAll(Collection<? extends ObjSalesOrder> arg0) {
		return mSalesOrders.addAll(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 * @see java.util.List#addAll(int, java.util.Collection)
	 */
	public boolean addAll(int arg0, Collection<? extends ObjSalesOrder> arg1) {
		return mSalesOrders.addAll(arg0, arg1);
	}

	/**
	 * 
	 * @see java.util.List#clear()
	 */
	public void clear() {
		mSalesOrders.clear();
	}

	/**
	 * @param object
	 * @return
	 * @see java.util.List#contains(java.lang.Object)
	 */
	public boolean contains(Object object) {
		return mSalesOrders.contains(object);
	}

	/**
	 * @param object
	 * @return
	 * @see java.util.List#equals(java.lang.Object)
	 */
	public boolean equals(Object object) {
		return mSalesOrders.equals(object);
	}

	/**
	 * @param location
	 * @return
	 * @see java.util.List#get(int)
	 */
	public ObjSalesOrder get(int location) {
		return mSalesOrders.get(location);
	}
	
	public void set(Collection<? extends ObjSalesOrder> arg0) {
		mSalesOrders.clear();
		mSalesOrders.addAll(arg0);
	}
	
	/**
	 * @param object
	 * @return
	 * @see java.util.List#indexOf(java.lang.Object)
	 */
	public int indexOf(Object object) {
		return mSalesOrders.indexOf(object);
	}

	/**
	 * @param object
	 * @return
	 * @see java.util.List#remove(java.lang.Object)
	 */
	public boolean remove(Object object) {
		return mSalesOrders.remove(object);
	}
	
	public ObjSalesOrder remove(int index) {
		return mSalesOrders.remove(index);
	}

	/**
	 * @param arg0
	 * @return
	 * @see java.util.List#removeAll(java.util.Collection)
	 */
	public void removeAll() {
		mSalesOrders.clear();
	}

	/**
	 * @return
	 * @see java.util.List#size()
	 */
	public int size() {
		return mSalesOrders.size();
	}

	public List<ObjSalesOrder> getAll() {
		return mSalesOrders;
	}

	public boolean isEmpty() {
		return mSalesOrders.isEmpty();
	}
	
	public String toString(){
		return mSalesOrders.toString();
	}
	
	
	/** 
	 * 
	 * Convenience methods for easy access to data in this manager
	 * 
	 */
	public ObjSalesOrder getRecordByOrderId(String strOrderId){
		
		for(int i=0;i<mSalesOrders.size();i++){
			
			ObjSalesOrder order = mSalesOrders.get(i);
			
			if(strOrderId.equals(order.getString(ObjSalesOrder.FIELD_ORDER_ID))){
				return order;
			}
			
		}

		return null;		
		
	}
	
	public boolean addIfMissing(ObjSalesOrder item) {
		if(!mSalesOrders.contains(item)){
			mSalesOrders.add(item);
			return true;
		}else{
			return false;
		}
	}
	
	public List<ObjSalesItem> getAllOrderedItems(){
		
		
		List<ObjSalesItem> lstItems = new ArrayList<ObjSalesItem>();
		
		//loop through all orders
		List<ObjSalesOrder> lstOrders = getAll();
		for(int i=0;i<lstOrders.size();i++){
			
			//get all ordered items and add to list
			ObjSalesOrder order = lstOrders.get(i);
			
			List<ObjSalesItem> orderedItems = order.getSalesItems();
			lstItems.addAll(orderedItems);
			
		}
		
		//Sort ordered items list by order date
		SortItemsByOrderDate sortItemsByOrderDate = new SortItemsByOrderDate();
		Collections.sort(lstItems, sortItemsByOrderDate);
		

		//add grouping to list
		//populate extra field for use in Order list
		for(int j=0;j<lstItems.size();j++){
			
			ObjSalesItem thisItem = lstItems.get(j);
			String strThisOrderId = thisItem.getParentRecord().getString(ObjSalesOrder.FIELD_ORDER_ID);
			
			if(j==0){
				thisItem.setField(ObjSalesItem.FIELD_ORDER_ID_GROUP, strThisOrderId);
			}else{
				
				ObjSalesItem previousRecord = lstItems.get(j-1);
				String strPreviousOrderId = previousRecord.getParentRecord().getString(ObjSalesOrder.FIELD_ORDER_ID);
				
				if(!strPreviousOrderId.equals(strThisOrderId)){
					thisItem.setField(ObjSalesItem.FIELD_ORDER_ID_GROUP, strThisOrderId);
				}
				
			}
			
		}		
		
		
		return lstItems;
		
	}
	

	
	//can be used to sort individual orders by date
	//Use
	//SortOrdersByDate sortByDate = new SortOrdersByDate();
	//Collections.sort(orderList, sortByDate);
	class SortOrdersByDate implements Comparator<ObjSalesOrder> {

		@Override
		public int compare(ObjSalesOrder item1, ObjSalesOrder item2) {

			Date item1OrderDate = item1.getDateFromJson(ObjSalesOrder.FIELD_ORDER_DATE); 
			Date item2OrderDate = item2.getDateFromJson(ObjSalesOrder.FIELD_ORDER_DATE); 
			
			
			if(item1OrderDate.equals(item2OrderDate)){
				
				int item1OrderIndex = item1.getInt(ObjSalesItem.FIELD_JSON_ARRAY_INDEX);
				int item2OrderIndex = item1.getInt(ObjSalesItem.FIELD_JSON_ARRAY_INDEX);
				
				return item1OrderIndex - item2OrderIndex;
			}

			//this sorts by date descending - newest orders are on top of list
			return item2OrderDate.compareTo(item1OrderDate);
			
		}

	}

	class SortItemsByOrderDate implements Comparator<ObjSalesItem> {

		@Override
		public int compare(ObjSalesItem item1, ObjSalesItem item2) {

			Date item1OrderDate = item1.getParentRecord().getDateFromJson(ObjSalesOrder.FIELD_ORDER_DATE); 
			Date item2OrderDate = item2.getParentRecord().getDateFromJson(ObjSalesOrder.FIELD_ORDER_DATE); 

			//this sorts by date descending - newest orders are on top of list
			return item2OrderDate.compareTo(item1OrderDate);
			
		}

	}

	public void updateOrderManager(List<ObjSalesOrder> lstOrdersFromApi) {
		
		String strStatusDelivered = ApplicationContextProvider.getContext().getString(R.string.str_order_status_delivered);
		
		
		//Updates the order manager containing orders previously retrieved with the latest information from the server
		//During this process, we are trying to identify which order has changed (e.g. delivery status etc)
		for(int i=0;i<lstOrdersFromApi.size();i++){
			
			ObjSalesOrder apiOrder = lstOrdersFromApi.get(i);
			ObjSalesOrder cachedOrder = getRecordByOrderId(apiOrder.getString(ObjSalesOrder.FIELD_ORDER_ID));
			
			//order not in list? Flag as updated information
			if(cachedOrder==null){
				apiOrder.setField(ObjSalesOrder.FIELD_ORDER_NEW_INFORMATION, true);
			}else{
				
				//check if this order was just delivered and flag as such
				//This is used in HybridActivity to show a Trustpilot review dialog
				String strCachedOrderStatus = cachedOrder.getDisplayOrderStatus();
				String strApiOrderStatus = apiOrder.getDisplayOrderStatus();
				if(!strStatusDelivered.equals(strCachedOrderStatus)
						&& strStatusDelivered.equals(strApiOrderStatus)
						){
					apiOrder.setField(ObjSalesOrder.FIELD_ORDER_JUST_DELIVERED, true);
				}
				
				//existing order?
				//check if key information on our order has changed and flag accordingly
				boolean blnOrderKeyInfoChanged = cachedOrder.compareOrderWith(apiOrder);
				apiOrder.setField(ObjSalesOrder.FIELD_ORDER_NEW_INFORMATION, blnOrderKeyInfoChanged);
				
			}
			
		}
		
		//key step: update cached orders with new orders from api
		set(lstOrdersFromApi);
		
	}

	public List<ObjSalesOrder> getOrdersWithNewInformation() {
		
		List<ObjSalesOrder> lstOrdersWithNewInformation = new ArrayList<ObjSalesOrder>();
		for(int i=0;i<mSalesOrders.size();i++){
			ObjSalesOrder order = mSalesOrders.get(i);
			if(order.getBoolean(ObjSalesOrder.FIELD_ORDER_NEW_INFORMATION)){
				lstOrdersWithNewInformation.add(order);
			}
		}
		
		return lstOrdersWithNewInformation;
		
	}

	public List<ObjSalesOrder> getJustDeliveredOrderList() {

		List<ObjSalesOrder> lstOrdersJustDelivered = new ArrayList<ObjSalesOrder>();
		
		for(int i=0;i<mSalesOrders.size();i++){
			ObjSalesOrder order = mSalesOrders.get(i);
			if(order.getBoolean(ObjSalesOrder.FIELD_ORDER_JUST_DELIVERED)){
				lstOrdersJustDelivered.add(order);
			}
		}
		
		return lstOrdersJustDelivered;
		
	}
	
	public void resetJustDeliveredOrders(){
		
		for(int i=0;i<mSalesOrders.size();i++){
			ObjSalesOrder order = mSalesOrders.get(i);
			order.setField(ObjSalesOrder.FIELD_ORDER_JUST_DELIVERED, false);
		}
		
	}

}
