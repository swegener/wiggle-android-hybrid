package uk.co.wiggle.hybrid.application.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.helpers.AppGlobalConstants;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.application.persistence.DataStore;
import uk.co.wiggle.hybrid.extensions.VersionComparator;
import uk.co.wiggle.hybrid.usecase.shop.classicnav.activities.HybridActivity;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.activities.TabbedHybridActivity;
import uk.co.wiggle.hybrid.usecase.shop.version3.activities.V3HybridActivity;
import uk.co.wiggle.hybrid.usecase.startup.activities.StartupActivity;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.usecase.startup.activities.WelcomeActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Location;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.widget.SwitchCompat;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class AppUtils {
	
	private static String logTag = "AppUtils";

	public static String CONST_APP_DIRECTORY = "." + ApplicationContextProvider.getContext().getString(R.string.app_name);
    public static String CONST_SUB_DIRECTORY_HIDDEN_FILES = ".Temp"; // used for temporary thumbnail files etc ..

	public static String SPORTSUFFIXPLACEHOLDER = "[SPORTPLACEHOLDER]";

	public enum APP_VERSION {

		VERSION_1_HYBRID(1),
		VERSION_2_TABBED_NAV(2),
		VERSION_3_APP_WIDE_SEARCH(3);

		private final int versionCode;

		private APP_VERSION(final int versionCode) {
			this.versionCode = versionCode;
		}

		public int getVersionCode() { return versionCode; }


	}

	public static APP_VERSION getWiggleVersion(){

		//Easter-egg:
		//you can return to classic navigation by using the login "classicnavigation"
		//you can return to tabbed navigation by using the loggin "tabbednavigation"
		String strEmail = ObjAppUser.instance().getString(ObjAppUser.FIELD_EMAIL);
		if("classicnavigation".equals(strEmail)){
			return APP_VERSION.VERSION_1_HYBRID;
		}else if("tabbednavigation".equals(strEmail)){
			return APP_VERSION.VERSION_2_TABBED_NAV;
		}else if("version3navigation".equals(strEmail)){
			return APP_VERSION.VERSION_3_APP_WIDE_SEARCH;
		}

		//Default app-version:
		return APP_VERSION.VERSION_3_APP_WIDE_SEARCH;

	}
	
	private static boolean blnPicassoDebugEnabled = false;

	public static boolean isPicassoDebugEnabled() {
		return blnPicassoDebugEnabled;
	}
	
	public static boolean isProductionVersionOfApp() {
		// TODO Auto-generated method stub
		return true;
	}


	//Enable this: App works in standard mode
	public static String REDIRECT_APP_TO_TEST_SERVER = checkForTestServerConfiguration();
	//Enable this: all App URLs get re-pointed to a given test server.
	//You need to supply <protocol>://<host> e.g. "http://cupdev.wiggle.co.uk"
	//public static String REDIRECT_APP_TO_TEST_SERVER = "http://cupdev.wiggle.co.uk";
	//public static String REDIRECT_APP_TO_TEST_SERVER = "http://beta.wiggle.co.uk";
	//use this to test the redirect using a server we can access
	//public static String REDIRECT_APP_TO_TEST_SERVER = "https://www.wiggle.co.nz";
	//public static String REDIRECT_APP_TO_TEST_SERVER = "https://tst9.wiggle.co.uk";
	public static String checkForTestServerConfiguration (){
		String strTestServerUrl = ObjAppUser.instance().getString(ObjAppUser.FIELD_TEST_SERVER_URL);
		if(strTestServerUrl==null){
			return "";
		}else{
			return strTestServerUrl;
		}
	}

	public static boolean showFragmentDebugList(){
		return false; // set to true to show fragment list for debugging in version 3
	}

	public final static int HTTP_CONNECTION_TIMEOUT = 15000; // timeout to establish connection in ms
	public final static int HTTP_WAIT_RESPONSE_TIMEOUT = 30000; // timeout to wait for response in ms (30 secs)


	
	public static boolean isEmailValid(String email) {
		
		//Very basic validation as we currently only use it on a login form
		
		if(email.contains("@") && email.contains(".")){
			return true;
		}else{
			return false;
		}
		
		/* The following validation too restrictive as some of the newer domains are not passing - adjust this to a more
		  current solution e.g. it disallows numbers in the domain which are valid!
		 
		final String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
				+ "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
				+ "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
				+ "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
				+ "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
				+ "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

		CharSequence inputStr = email;

		Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(inputStr);

		if (matcher.matches())
			return true;
		else
			return false;
		
		*/
	}
	

	public static String validateEmail(EditText emailView){
		
		String strEmailAddress = emailView.getText().toString().trim();
		
		Context context = ApplicationContextProvider.getContext();
		
		if("".equals(strEmailAddress)){
			
			
			switch (emailView.getId()) {
			
				case R.id.login_etEmail:

					return context.getString(R.string.login_etEmail_err_missing);

				case R.id.claimPersonalDetails_etEmail:

					return String.format(context.getString(R.string.str_err_value_required), context.getString(R.string.claimPersonalDetails_tvEmail));
					
			default:
				return "";
				
			}
			
			
		}
		
		if(!isEmailValid(strEmailAddress)){
			
			return context.getString(R.string.str_error_invalid_email);
			
		}
		
		//no error - do not return a message
		return null;
	}
	
	
	public static String validatePassword(EditText passwordView){
		
		String strPassword = passwordView.getText().toString().trim();
		
		Context context = ApplicationContextProvider.getContext();
		
		if("".equals(strPassword)){
			
			switch (passwordView.getId()) {
			
				case R.id.login_etPassword:
				case R.id.userLogin_etPassword:
					return context.getString(R.string.login_etPassword_err_missing);
					
			default:
				return null;
				
			}
			
		}
		
		//no error - do not return a message
		return null;
	}	

	
	
	// this method generates a unique ID for android devices - not to be
	// confused with udid!
	public static String getUniqueID() {
		
		Context context = ApplicationContextProvider.getContext();
		final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

		final String tmDevice, tmSerial, androidId;
		tmDevice = "" + tm.getDeviceId();
		tmSerial = "" + tm.getSimSerialNumber();
		androidId = ""
				+ android.provider.Settings.Secure.getString(
						context.getContentResolver(),
						android.provider.Settings.Secure.ANDROID_ID);

		UUID deviceUuid = new UUID(androidId.hashCode(),
				((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
		String deviceId = deviceUuid.toString();
		return deviceId;
	}


	public static void showCustomToast(final String strToastMessage,
			boolean blnIsDebugMessage) {

		// Do not show debug messages if this is a production version of the app
		if (blnIsDebugMessage) {
			if (AppUtils.isProductionVersionOfApp()) {
				return;
			}
		}

		//showing toasts async as sometimes this method is called from a background thread
		if(StartupActivity.instance()!=null) {

			StartupActivity.instance().runOnUiThread(new Runnable() {
				public void run() {
					showCustomToast(strToastMessage);
				}
			});

		}else if(WelcomeActivity.instance()!=null){

			WelcomeActivity.instance().runOnUiThread(new Runnable() {
				public void run() {
					showCustomToast(strToastMessage);
				}
			});

		}else if(HybridActivity.instance()!=null){
			
			HybridActivity.instance().runOnUiThread(new Runnable() {
				public void run() {
					showCustomToast(strToastMessage);
				}
			});

		}else if(TabbedHybridActivity.instance()!=null){

			TabbedHybridActivity.instance().runOnUiThread(new Runnable() {
				public void run() {
					showCustomToast(strToastMessage);
				}
			});

		}else if(V3HybridActivity.instance()!=null){

			V3HybridActivity.instance().runOnUiThread(new Runnable() {
				public void run() {
					showCustomToast(strToastMessage);
				}
			});

		}else{
			showCustomToast(strToastMessage);
		}


		

	}
	
	private static void showCustomToast(String strToastMessage){

		try {
			Toast.makeText(ApplicationContextProvider.getContext(),
					strToastMessage, Toast.LENGTH_SHORT).show();
		}catch(Exception e){
			return; //do nothing if this fails
		}
		
		/**
		 * Toast toast = Toast.makeText(context, strToastMessage,
		 * Toast.LENGTH_SHORT); View view = toast.getView();
		 * view.setBackgroundResource(R.drawable.wiy_toast_bg); TextView text =
		 * (TextView) view.findViewById(android.R.id.message);
		 * text.setTextColor(getResources().getColor(R.color.text_blue));
		 * toast.show();
		 **/
		
	}

	public static int dpToPx(Resources res, int dp) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
				res.getDisplayMetrics());
	}

	public static int dpToPx(int dp) {
		return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
	}

	public static int pxToDp(int px) {
		return (int) (px / Resources.getSystem().getDisplayMetrics().density);
	}

	public static String concatenateString(String strBaseString,
			String strToAdd, String strDelimiter) {

		if (strToAdd == null || "".equals(strToAdd)) {
			return strBaseString; // do nothing if there is nothing to add
		}

		if (strDelimiter != null) {
			if (strBaseString != null && !"".equals(strBaseString)) {
				strBaseString = strBaseString + strDelimiter;
			}
		}

		strBaseString = strBaseString + strToAdd;

		return strBaseString;

	}

	public static ArrayList<String> getSplitText(Paint paintedText,
			String strOriginalText, int intCanvasHeight, int intCanvasWidth) {

		ArrayList<String> splitText = new ArrayList<String>();

		// remove HTML tags from original text
		strOriginalText = Html.fromHtml(strOriginalText).toString();

		String strLine1Buffer = "";
		String strLine1Accepted = "";

		for (int i = 0; i < strOriginalText.length(); i++) {

			float flTextWidth = paintedText.measureText(strLine1Buffer);

			if (flTextWidth < intCanvasWidth) {
				strLine1Buffer = strOriginalText.substring(0, i);
				strLine1Accepted = strLine1Buffer;
			} else {
				break;
			}

		}

		if (!"".equals(strLine1Accepted)) {
			splitText.add(strLine1Accepted);
		}

		String strLine2Buffer = "";
		String strLine2Accepted = "";

		String strRemainingText = strOriginalText.substring(strLine1Accepted
				.length());

		for (int j = 0; j <= strRemainingText.length(); j++) {

			float flTextWidth = paintedText.measureText(strLine2Buffer);

			if (flTextWidth < intCanvasWidth) {
				strLine2Buffer = strRemainingText.substring(0, j);
				strLine2Accepted = strLine2Buffer;
			} else {
				break;
			}

		}

		if (!"".equals(strLine2Accepted)) {
			splitText.add(strLine2Accepted);
		}

		return splitText;

	}

	@SuppressLint("NewApi") 
	public static void setSDKDependentBackground(View view, int intDrawableResourceId) {

		Context context = ApplicationContextProvider.getContext();

		// Method is used to make use of the improved setBackground() method
		// from API level 16 (Build.VERSION_CODES.JELLY_BEAN)
		// For lower API versions, setBackgroundDrawable() is used
		if (Build.VERSION.SDK_INT < 16) {
			view.setBackgroundDrawable(context.getResources().getDrawable(intDrawableResourceId));
		} else {
			view.setBackground(context.getResources().getDrawable(intDrawableResourceId));
		}

	}
	
	@SuppressLint("NewApi") 
	public static void setSDKDependentBackground(View view, Drawable drawable) {

		Context context = ApplicationContextProvider.getContext();

		// Method is used to make use of the improved setBackground() method
		// from API level 16 (Build.VERSION_CODES.JELLY_BEAN)
		// For lower API versions, setBackgroundDrawable() is used
		if (Build.VERSION.SDK_INT < 16) {
			view.setBackgroundDrawable(drawable);
		} else {
			view.setBackground(drawable);
		}

	}	

	
	//method returns correct animation to be used when opening an activity (default: slide in from right. Arabic: slide in from left)
	public static int getActivityOpenAnimation(){
		
		return R.anim.activity_right_slide_in;
		
	}
	
	//method returns correct animation to be used when opening an activity (default: slide out to right. Arabic: slide out to left)
	public static int getActivityCloseAnimation(){
		
		return R.anim.activity_right_slide_out;
		
	}
	
	
	public static boolean isNetworkConnected(){
		
		Context context = ApplicationContextProvider.getContext();
		
		ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
	    
	    if(networkInfo==null){
	    	  return false;
	     }
	      
	     boolean isConnected = networkInfo.isConnected();
	     
	     return isConnected;
	      
	}

	public static void flagWidgetDataStatus(View widget, boolean blnDataValid, String strErrorMessage) {
		
		Context context = widget.getContext();

		setViewBackground(widget, blnDataValid);

		if(widget instanceof EditText){
			if(strErrorMessage==null){
				((EditText) widget).setError(null);				
			}else{
				((EditText) widget).setError(strErrorMessage);
			}
		}
		
	}
	

	private static void setViewBackground(View widget, boolean blnDataValid){
		
		//a few special cases to handle for backgrounds
		
		//On data capture form - set error or ok background
		if(widget.getContext() instanceof StartupActivity) {

			//a view special views ..
			if (widget.getTag() != null) {

				String strViewTag = widget.getTag().toString();

				if ("group_top".equals(strViewTag)) {

					if (blnDataValid) {
						setSDKDependentBackground(widget, widget.getContext().getResources().getDrawable(R.drawable.d_welcome_edit_text_top));
					} else {
						setSDKDependentBackground(widget, widget.getContext().getResources().getDrawable(R.drawable.d_welcome_edit_text_top_error));
					}
				} else if ("group_middle".equals(strViewTag)) {

					if (blnDataValid) {
						setSDKDependentBackground(widget, widget.getContext().getResources().getDrawable(R.drawable.d_welcome_edit_text_centre));
					} else {
						setSDKDependentBackground(widget, widget.getContext().getResources().getDrawable(R.drawable.d_welcome_edit_text_centre_error));
					}

				} else if ("group_bottom".equals(strViewTag)) {

					if (blnDataValid) {
						setSDKDependentBackground(widget, widget.getContext().getResources().getDrawable(R.drawable.d_welcome_edit_text_bottom));
					} else {
						setSDKDependentBackground(widget, widget.getContext().getResources().getDrawable(R.drawable.d_welcome_edit_text_bottom_error));
					}

				}

			} else {

				//an individual control (no tag) - set normal background
				if (blnDataValid) {
					setSDKDependentBackground(widget, widget.getContext().getResources().getDrawable(R.drawable.d_welcome_edit_text));
				} else {
					setSDKDependentBackground(widget, widget.getContext().getResources().getDrawable(R.drawable.d_welcome_edit_text_error));
				}

			}

		}else if(widget.getContext() instanceof TabbedHybridActivity){


            if (blnDataValid) {
                setSDKDependentBackground(widget, widget.getContext().getResources().getDrawable(R.drawable.d_edit_text_account_login));
            } else {
                setSDKDependentBackground(widget, widget.getContext().getResources().getDrawable(R.drawable.d_edit_text_account_login_error));
            }

		}else if(widget.getContext() instanceof V3HybridActivity){


			if (blnDataValid) {
				setSDKDependentBackground(widget, widget.getContext().getResources().getDrawable(R.drawable.d_edit_text_account_login));
			} else {
				setSDKDependentBackground(widget, widget.getContext().getResources().getDrawable(R.drawable.d_edit_text_account_login_error));
			}


		}else{
			
			//default - set black or white background colour
			if(blnDataValid){
				widget.setBackgroundColor(widget.getContext().getResources().getColor(R.color.white));
			}else{
				widget.setBackgroundColor(widget.getContext().getResources().getColor(R.color.error_red));
			}
			
		}
		
		
		
	}


	public static String getParameterFromSwitch(Switch switchView){
		
		if(switchView.isChecked()){
			return "1";
		}else{
			return "0";
		}
		
	}

	public static String getParameterFromSwitch(SwitchCompat switchView){

		if(switchView.isChecked()){
			return "1";
		}else{
			return "0";
		}

	}

	public static String getParameterFromWiggleCheckbox(Button wiggleCheckbox){

		if(wiggleCheckbox.isSelected()){
			return "1";
		}else{
			return "0";
		}

	}


	public static void saveLastAuthenticationTimestamp() {
		
		//Save refresh date
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		SimpleDateFormat apiDateFormat = new SimpleDateFormat(ApplicationContextProvider.getContext().getString(R.string.str_gui_date_time_format));
		String strFormattedDate = apiDateFormat.format(cal.getTime());
		DataStore.saveStringToSharedPreferences("LAST_APP_REFRESH", "LAST_REFRESH_TIME", strFormattedDate);
		
	}	
	
	public static void startFloatingLogoAnimation(Context context,
			final View view) {

		final Animation floatingLogo = AnimationUtils.loadAnimation(context,
				R.anim.floating);

		floatingLogo.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				view.startAnimation(floatingLogo);
			}

		});

		view.startAnimation(floatingLogo);

	}

	public static void stopFloatingLogoAnimation(Context context, final View view) {

		view.clearAnimation();

	}
	
	public static String getWiggleURL(String strFormatPattern){

		Context context = ApplicationContextProvider.getContext();

		//APP-221: Chinese / Japanese URLs are encoded
		//e.g. "%e8%b7%91%e6%ad%a5%e7%b3%bb%e5%88%97" for "run"
		//Running a String.format("cn", "www.wiggle.%1$s/api/list/%e8%b7%91%e6%ad%a5%e7%b3%bb%e5%88%97") fails
		//due to the percentage signs in the api url. Hence we do it backwards now:
		//Rather than replacing the "--TLD--" in the raw json files with ".%1$s" and doing String.format we now do:
		//Replace any ".%1$s" with ".--TLD--" (note the point!) and run a String.replace("--TLD--","cn")
		strFormatPattern = strFormatPattern.replace(".%1$s", ".--TLD--"); //replace placeholder from strings.xml to something without % sign (otherwise String.format() will fail for CH / JA / RU URL encoded URLs
		
		//special handling for Australia insurance page
		if(context.getString(R.string.str_locale_country_iso_code_au)
				.equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY))
			&& 	strFormatPattern.equals(context.getString(R.string.str_url_cycle_insurance))){			
			return context.getString(R.string.str_url_cycle_insurance_australia);
		}
		
		//Buyers guides - whilst it exists for NZ it does not have any content. 
		//.com buyers guide is the UK one .. so redirect accordingly
		if(strFormatPattern.equals(context.getString(R.string.str_url_buyers_guides_generic))){
			
			String strCountry = ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY);
			
			if(context.getString(R.string.str_locale_country_iso_code_au).equals(strCountry)){
				return context.getString(R.string.str_url_buyers_guides_australia);
			}else if(context.getString(R.string.str_locale_country_iso_code_fr).equals(strCountry)){
				return context.getString(R.string.str_url_buyers_guides_france);
			}else if(context.getString(R.string.str_locale_country_iso_code_es).equals(strCountry)){
				return context.getString(R.string.str_url_buyers_guides_spain);
			}else if(context.getString(R.string.str_locale_country_iso_code_jp).equals(strCountry)){
				return context.getString(R.string.str_url_buyers_guides_japan);
			}else if(context.getString(R.string.str_locale_country_iso_code_se).equals(strCountry)){
				return context.getString(R.string.str_url_buyers_guides_sweden);	
			}else if(context.getString(R.string.str_locale_country_iso_code_de).equals(strCountry)){
				return context.getString(R.string.str_url_buyers_guides_germany);	
			}else{
				return context.getString(R.string.str_url_buyers_guides_uk);
			}
			
		}
		
		//don't do anything with non-Wiggle domains
		if(!strFormatPattern.contains("wiggle")){
			return strFormatPattern;
		}
		
		//Check if this is .co.uk, .it, .fr ...
		String strTopLevelDomain = getCountryTopLevelDomain(null);
		
		
		//String strResult = String.format(strFormatPattern, strTopLevelDomain);
		String strResult = strFormatPattern.replace("--TLD--", strTopLevelDomain);
		
		//Some countries use www.wigglesport. instead of www.wiggle.
		if(strResult.contains(SPORTSUFFIXPLACEHOLDER)){
			strResult = getWiggleHostForCountry(strResult);
		}
				
		
		if(context.getString(R.string.str_url_typeahead).equals(strFormatPattern)){
			//TODO: handle domain change in typeahead URL (fails Url.parse-ing as host invalid (axax.wiggle.)
			return strResult;
		}
		
		/**
		//Some countries use www.wigglesport. instead of www.wiggle.
		if(context.getString(R.string.str_locale_country_iso_code_it).equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY))){
			strResult = strResult.replace("www.wiggle.", "www.wigglesport.");
		}
		**/
		
		strResult = handleWiggleDomainChange(strResult);

		//if test server enabled - handle it
		if(!"".equals(REDIRECT_APP_TO_TEST_SERVER)){
			strResult = getTestServerUrl(strResult);
		}
		
		return strResult;
		
	}
	
	public static String getWiggleUrlWithUTMTracking(String strFormatPattern, String strGTMEvent){
		
		String strWiggleURL = getWiggleURL(strFormatPattern);
				
		strWiggleURL = appendUtmTag(strWiggleURL, strGTMEvent);
		
		return strWiggleURL; //default to wiggle URL if encoding goes wrong - otherwise append UTM parameters
		
	}
	
	public static String appendUtmTag(String strOriginalUrl, String strGTMEvent){
		
		Context context = ApplicationContextProvider.getContext();
		
		String strAppendedUrl = strOriginalUrl;
		

		/** UTM Tracking pattern 
	    utm_source=Android&amp;utm_medium=Wiggle MobileApp&amp;utm_content=%1$s&amp;utm_campaign=%2$s
	    **/

		//no URL encoding done here as shouldnt be needed

		/* Version Code 19: replaced the below
		String strUTMTrackingParameters = "utm_source=Android" 
										   + "&" + 
										  "utm_medium=Wiggle%20MobileApp" 
										   + "&" + 
										  "utm_content=" + getAppVersionNumber() 
										   + "&" + 
										  "utm_campaign=" + strGTMEvent;
          with the following ... */
		String strUTMTrackingParameters = "app=app"
										  + "&" +
										  "appversion=" + getAppVersionNumber()
				                          + "&" +
				                          "appplatform=Android"
										  + "&" +
				                          "appcid=" + ApplicationContextProvider.instance().getAnalyticsTrackerCID();
				                          ;
		
		//Removed the "/" appendix - this causes issues with appended paths (e.g. intitial www.wiggle.co.uk/basket turns into www.wiggle.co.uk/basket/basket
		//if(!strAppendedUrl.endsWith("/")){
		//	strAppendedUrl = strAppendedUrl + "/";
		//}

		//append with "?" by default
		String strQueryStringPrefix = "?";
		if(strOriginalUrl.contains("?")){
			//...unless Url already contains a question mark. In this case append with "&"
			strQueryStringPrefix = "&";
		}
			
		strAppendedUrl = strAppendedUrl + strQueryStringPrefix + strUTMTrackingParameters;

		//AppUtils.showCustomToast(strAppendedUrl, false);
		return strAppendedUrl;
				
	}
	
	private static String handleWiggleDomainChange(String strOriginalUrl){
		
		String strAdjustedUrl = strOriginalUrl;
		
		//Only if we have saved a previous Wiggle top level domain for this user:
		//Turn this into a URI .. and check if there is a user selected store we need to re-route this to
		//We just replace the hard coded hosts from string.xml with the host stored in ObjAppUser.FIELD_IN_APP_SELECTED_WIGGLE_STORE
		String strUserSelectedStore = ObjAppUser.instance().getString(ObjAppUser.FIELD_CURRENT_WIGGLE_STORE);
		
		if(strUserSelectedStore!=null && !"".equals(strUserSelectedStore)){
			
			Uri uri = Uri.parse(strOriginalUrl);
			String strHost = uri.getHost();
			
			//a hack for header domains ... they come in without schema (http/s) and can't be Uri.parse-d. 
			//They might still need replacing as follows though
			if(strHost==null 
					|| !strHost.startsWith("www.") //make sure external links (legal / insurance etc) are not replaced. TODO: improve
					){
				//strHost = strOriginalUrl;			
				return strOriginalUrl;
			}
			
			strAdjustedUrl = strOriginalUrl.replace(strHost, strUserSelectedStore);
			
		}
		
		return strAdjustedUrl;
		
	}
	
	public static String getAppVersionNumber() {

		String strVersionLabel = "";
		Context context = ApplicationContextProvider.getContext();

		PackageInfo pinfo;
		try {
			pinfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			strVersionLabel = pinfo.versionName;

		} catch (NameNotFoundException e) {
			Logger.printMessage(logTag, e.toString(), Logger.ERROR);
		}

		return strVersionLabel;
		
	}

	public static String getAppUserInfo() {

		//Get useful information: user country, delivery destination, currency, domain
		String strLanguage = ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_LANGUAGE);
		String strCurrency = ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_CURRENCY);
		String strDeliveryDestination = ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_DELIVERY_DESTINATION);
		String strStore = ObjAppUser.instance().getString(ObjAppUser.FIELD_CURRENT_WIGGLE_STORE);

		String strUserInfo = "";
		if (strStore != null && !"".equals(strStore)) {
			strUserInfo = strUserInfo + strStore;
		}

		if (strUserInfo != null && !"".equals(strUserInfo)){
			strUserInfo = strUserInfo + ";";
		}

		if (strLanguage != null && !"".equals(strLanguage)) {
			strUserInfo = strUserInfo + strLanguage;
		}

		if (strUserInfo != null && !"".equals(strUserInfo)){
			strUserInfo = strUserInfo + ";";
		}

		if (strDeliveryDestination != null && !"".equals(strDeliveryDestination)) {
			strUserInfo = strUserInfo + strDeliveryDestination;
		}

		if (strUserInfo != null && !"".equals(strUserInfo)){
			strUserInfo = strUserInfo + ";";
		}

		if (strCurrency != null && !"".equals(strCurrency)) {
			strUserInfo = strUserInfo + strCurrency;
		}

		return "(" + strUserInfo + ")";

	}
	
	public static int getAppVersionInt(){
		
		Context context = ApplicationContextProvider.getContext();
		int intAppVersionInt = 0;
		
		PackageInfo pinfo;
		try {
			pinfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			intAppVersionInt = pinfo.versionCode;
			
		} catch (NameNotFoundException e) {
			Logger.printMessage(logTag, e.toString(), Logger.ERROR);
		}		
		
		return intAppVersionInt;
		
	}
	
	public static Drawable getCountryFlag(){
		
		String strUserCountry = ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY);
		Context context = ApplicationContextProvider.getContext();

		if (context.getString(R.string.str_locale_country_iso_code_uk).equals(strUserCountry)){
			return context.getResources().getDrawable(R.drawable.flag_gb);
		}else if(context.getString(R.string.str_locale_country_iso_code_us).equals(strUserCountry)){
			return context.getResources().getDrawable(R.drawable.flag_us);
		}else if(context.getString(R.string.str_locale_country_iso_code_fr).equals(strUserCountry)){
			return context.getResources().getDrawable(R.drawable.flag_fr);
		}else if(context.getString(R.string.str_locale_country_iso_code_au).equals(strUserCountry)){
			return context.getResources().getDrawable(R.drawable.flag_au);
		}else if(context.getString(R.string.str_locale_country_iso_code_nz).equals(strUserCountry)){
			return context.getResources().getDrawable(R.drawable.flag_nz);
		}else if(context.getString(R.string.str_locale_country_iso_code_es).equals(strUserCountry)){
			return context.getResources().getDrawable(R.drawable.flag_es);
		}else if(context.getString(R.string.str_locale_country_iso_code_it).equals(strUserCountry)){
			return context.getResources().getDrawable(R.drawable.flag_it);
		}else if(context.getString(R.string.str_locale_country_iso_code_cn).equals(strUserCountry)){
			return context.getResources().getDrawable(R.drawable.flag_cn);	
		}else if(context.getString(R.string.str_locale_country_iso_code_jp).equals(strUserCountry)){
			return context.getResources().getDrawable(R.drawable.flag_jp);		
		}else if(context.getString(R.string.str_locale_country_iso_code_de).equals(strUserCountry)){
			return context.getResources().getDrawable(R.drawable.flag_de);		
		}else if(context.getString(R.string.str_locale_country_iso_code_se).equals(strUserCountry)){
			return context.getResources().getDrawable(R.drawable.flag_se);
		}else if(context.getString(R.string.str_locale_country_iso_code_nl).equals(strUserCountry)){
			return context.getResources().getDrawable(R.drawable.flag_nl);				
		}else{
			//default: UK store
			return context.getResources().getDrawable(R.drawable.flag_gb);
		}
		
	}
	
	public static String getCountryName(){
		
		String strUserCountry = ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY);
		Context context = ApplicationContextProvider.getContext();
		
		if(context.getString(R.string.str_locale_country_iso_code_uk).equals(strUserCountry)){
			return context.getString(R.string.str_country_name_UK);
		}else if(context.getString(R.string.str_locale_country_iso_code_us).equals(strUserCountry)){
			return context.getString(R.string.str_country_name_US);
		}else if(context.getString(R.string.str_locale_country_iso_code_fr).equals(strUserCountry)){
			return context.getString(R.string.str_country_name_France);
		}else if(context.getString(R.string.str_locale_country_iso_code_au).equals(strUserCountry)){
			return context.getString(R.string.str_country_name_Australia);
		}else if(context.getString(R.string.str_locale_country_iso_code_nz).equals(strUserCountry)){
			return context.getString(R.string.str_country_name_NewZealand);
		}else if(context.getString(R.string.str_locale_country_iso_code_es).equals(strUserCountry)){
			return context.getString(R.string.str_country_name_ES);
		}else if(context.getString(R.string.str_locale_country_iso_code_it).equals(strUserCountry)){
			return context.getString(R.string.str_country_name_IT);			
		}else if(context.getString(R.string.str_locale_country_iso_code_cn).equals(strUserCountry)){
			return context.getString(R.string.str_country_name_CN);	
		}else if(context.getString(R.string.str_locale_country_iso_code_jp).equals(strUserCountry)){
			return context.getString(R.string.str_country_name_JP);	
		}else if(context.getString(R.string.str_locale_country_iso_code_de).equals(strUserCountry)){
			return context.getString(R.string.str_country_name_DE);		
		}else if(context.getString(R.string.str_locale_country_iso_code_se).equals(strUserCountry)){
			return context.getString(R.string.str_country_name_SE);		
		}else if(context.getString(R.string.str_locale_country_iso_code_nl).equals(strUserCountry)){
			return context.getString(R.string.str_country_name_NL);				
		}else{
			//default: UK store
			return context.getString(R.string.str_country_name_UK);
		}
		
	}
	
	
	public static String getSupportedCountryCode(String strDetectedCountry){
		
		Context context = ApplicationContextProvider.getContext();
		
		//make sure that only supported country codes are accepted - any other detected country defaults to UK
		if(context.getString(R.string.str_locale_country_iso_code_uk).equals(strDetectedCountry)
			|| context.getString(R.string.str_locale_country_iso_code_fr).equals(strDetectedCountry)	
			|| context.getString(R.string.str_locale_country_iso_code_au).equals(strDetectedCountry)	
			|| context.getString(R.string.str_locale_country_iso_code_nz).equals(strDetectedCountry)	
			|| context.getString(R.string.str_locale_country_iso_code_us).equals(strDetectedCountry)
			|| context.getString(R.string.str_locale_country_iso_code_es).equals(strDetectedCountry)	
			|| context.getString(R.string.str_locale_country_iso_code_it).equals(strDetectedCountry)	
			|| context.getString(R.string.str_locale_country_iso_code_cn).equals(strDetectedCountry)	
			|| context.getString(R.string.str_locale_country_iso_code_jp).equals(strDetectedCountry)	
			|| context.getString(R.string.str_locale_country_iso_code_de).equals(strDetectedCountry)	
			|| context.getString(R.string.str_locale_country_iso_code_se).equals(strDetectedCountry)	
			|| context.getString(R.string.str_locale_country_iso_code_nl).equals(strDetectedCountry)			
				){
			return strDetectedCountry;
		}else{
			//default: UK store
			return context.getString(R.string.str_locale_country_iso_code_uk);
		}
		
	}
	
	public static String getCountryTopLevelDomain(String strUserCountry){
		
		Context context = ApplicationContextProvider.getContext();
		
		if(strUserCountry==null){
			strUserCountry = ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY);
		}
		
		if(context.getString(R.string.str_locale_country_iso_code_uk).equals(strUserCountry)){
			return context.getString(R.string.str_url_domain_uk);
		}else if(context.getString(R.string.str_locale_country_iso_code_us).equals(strUserCountry)){
			return context.getString(R.string.str_url_domain_us);
		}else if(context.getString(R.string.str_locale_country_iso_code_fr).equals(strUserCountry)){
			return context.getString(R.string.str_url_domain_fr);
		}else if(context.getString(R.string.str_locale_country_iso_code_au).equals(strUserCountry)){
			return context.getString(R.string.str_url_domain_au);
		}else if(context.getString(R.string.str_locale_country_iso_code_nz).equals(strUserCountry)){
			return context.getString(R.string.str_url_domain_nz);
		}else if(context.getString(R.string.str_locale_country_iso_code_es).equals(strUserCountry)){
			return context.getString(R.string.str_url_domain_es);	
		}else if(context.getString(R.string.str_locale_country_iso_code_it).equals(strUserCountry)){
			return context.getString(R.string.str_url_domain_it);	
		}else if(context.getString(R.string.str_locale_country_iso_code_cn).equals(strUserCountry)){
			return context.getString(R.string.str_url_domain_cn);
		}else if(context.getString(R.string.str_locale_country_iso_code_jp).equals(strUserCountry)){
			return context.getString(R.string.str_url_domain_jp);		
		}else if(context.getString(R.string.str_locale_country_iso_code_de).equals(strUserCountry)){
			return context.getString(R.string.str_url_domain_de);	
		}else if(context.getString(R.string.str_locale_country_iso_code_se).equals(strUserCountry)){
			return context.getString(R.string.str_url_domain_se);
		}else if(context.getString(R.string.str_locale_country_iso_code_nl).equals(strUserCountry)){
			return context.getString(R.string.str_url_domain_nl);				
		}else{
			//default: UK store
			return context.getString(R.string.str_url_domain_uk);
		}
		
	}
	
	public static String getWiggleHostForCountry(String strWiggleURL){
		
		Context context = ApplicationContextProvider.getContext();
		
		String strAdjustedURL = "";
		
		String strUserCountry = ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY);
		
		//Italy and Germany use "www.wigglesport."
		if(context.getString(R.string.str_locale_country_iso_code_it).equals(strUserCountry)
				|| context.getString(R.string.str_locale_country_iso_code_de).equals(strUserCountry)
				){
			strAdjustedURL = strWiggleURL.replace(SPORTSUFFIXPLACEHOLDER, "sport");			
		}else{
			//all other countries use "www.wiggle."
			strAdjustedURL = strWiggleURL.replace(SPORTSUFFIXPLACEHOLDER, "");	
		}
		
		return strAdjustedURL;
		
	}
	
	public static String getCountryByTopLevelDomain(String strTopLevelDomainOrHost){

		//TODO: make this replace of www.wiggle. / www.wigglesport. generic!
		String strTopLevelDomain = strTopLevelDomainOrHost.replace("www.wiggle.", "");
		strTopLevelDomain = strTopLevelDomain.replace("www.wigglesport.", "");


		Context context = ApplicationContextProvider.getContext();
		
		if(strTopLevelDomain.equals(context.getString(R.string.str_url_domain_uk))){
			return context.getString(R.string.str_locale_country_iso_code_uk);
		}else if(strTopLevelDomain.equals(context.getString(R.string.str_url_domain_us))){
			return context.getString(R.string.str_locale_country_iso_code_us);
		}else if(strTopLevelDomain.equals(context.getString(R.string.str_url_domain_fr))){
			return context.getString(R.string.str_locale_country_iso_code_fr);
		}else if(strTopLevelDomain.equals(context.getString(R.string.str_url_domain_au))){
			return context.getString(R.string.str_locale_country_iso_code_au);
		}else if(strTopLevelDomain.equals(context.getString(R.string.str_url_domain_nz))){
			return context.getString(R.string.str_locale_country_iso_code_nz);
		}else if(strTopLevelDomain.equals(context.getString(R.string.str_url_domain_es))){
			return context.getString(R.string.str_locale_country_iso_code_es);
		}else if(strTopLevelDomain.equals(context.getString(R.string.str_url_domain_it))){
			return context.getString(R.string.str_locale_country_iso_code_it);	
		}else if(strTopLevelDomain.equals(context.getString(R.string.str_url_domain_cn))){
			return context.getString(R.string.str_locale_country_iso_code_cn);
		}else if(strTopLevelDomain.equals(context.getString(R.string.str_url_domain_jp))){
			return context.getString(R.string.str_locale_country_iso_code_jp);	
		}else if(strTopLevelDomain.equals(context.getString(R.string.str_url_domain_de))){
			return context.getString(R.string.str_locale_country_iso_code_de);	
		}else if(strTopLevelDomain.equals(context.getString(R.string.str_url_domain_se))){
			return context.getString(R.string.str_locale_country_iso_code_se);	
		}else if(strTopLevelDomain.equals(context.getString(R.string.str_url_domain_nl))){
			return context.getString(R.string.str_locale_country_iso_code_nl);			
		}else{
			//default: UK store
			return context.getString(R.string.str_locale_country_iso_code_uk);
		}
		
	}	
	
	public static String getKeyValueFromNVPString(String strNVPString, String strKeyName, String strNVPSeparator, String strAssignmentCharacter){
		
		/**
		 *  AndroMedia - gets a named value from a name-value-pair string like  
		 *  name1=value1&name2=value2&name3=value3
		 *  
		 *  strNVPString = the entire string above
		 *  strKeyName = the Key you are interested in (e.g. name2)
		 *  strNVPSeparator = the separator between the individual name-value-key pairs (the & above)
		 *  strAssignmentCharacter = the name-key assignment character (the "=" between name=value) 
		 *  
		 */
		
		if(strNVPString==null){
			return "";
		}
		
		String[] allNameValuePairs = strNVPString.split(strNVPSeparator);
		String strReturnValue = "";
		
        for (int i=0;i<allNameValuePairs.length;i++){
        	
            String[] nvp = allNameValuePairs[i].split(strAssignmentCharacter);
            if(nvp.length == 2){
            	
                String key = nvp[0];
                String value = nvp[1];
                if(key.equals(strKeyName)){
                	strReturnValue = value;
                    return strReturnValue;
                }
                
            }
        }
        
        return strReturnValue;
		
	}
	
	public static String getLanguageCodeForStore(String strStoreHost){
		
		String strLanguageCode = "";
		
		if("www.wiggle.co.uk".equals(strStoreHost)){
			strLanguageCode = "en";
		}else if("www.wiggle.com".equals(strStoreHost)){
			strLanguageCode = "us";
		}else if("www.wiggle.com.au".equals(strStoreHost)){
			strLanguageCode = "au";
		}else if("www.wiggle.co.nz".equals(strStoreHost)){
			strLanguageCode = "nz";
		}else if("www.wiggle.cn".equals(strStoreHost)){
			strLanguageCode = "zh";
		}else if("www.wiggle.es".equals(strStoreHost)){
			strLanguageCode = "es";
		}else if("www.wiggle.jp".equals(strStoreHost)){
			strLanguageCode = "ja";
		}else if("www.wiggle.fr".equals(strStoreHost)){
			strLanguageCode = "fr";
		}else if("www.wiggle.ru".equals(strStoreHost)){
			strLanguageCode = "ru";
		}else if("www.wiggle.nl".equals(strStoreHost)){
			strLanguageCode = "nl";
		}else if("www.wigglesport.de".equals(strStoreHost)){
			strLanguageCode = "de";
		}else if("www.wigglesport.it".equals(strStoreHost)){
			strLanguageCode = "it";
		}else if("www.wiggle.se".equals(strStoreHost)){
			strLanguageCode = "sv"; //mismatch to iso code se!
		}else{
			strLanguageCode = "en"; //default
		}
		
		return strLanguageCode;
		
		
	}


	public static boolean isTrustedWiggleHost(String strHost){

		if("www.wiggle.co.uk".equals(strHost)
				|| "www.wiggle.com".equals(strHost)
				|| "www.wiggle.com.au".equals(strHost)
				|| "www.wiggle.co.nz".equals(strHost)
				|| "www.wiggle.cn".equals(strHost)
				|| "www.wiggle.es".equals(strHost)
				|| "www.wiggle.jp".equals(strHost)
				|| "www.wiggle.fr".equals(strHost)
				|| "www.wiggle.ru".equals(strHost)
				|| "www.wiggle.nl".equals(strHost)
				|| "www.wigglesport.de".equals(strHost)
				|| "www.wigglesport.it".equals(strHost)
				|| "www.wiggle.se".equals(strHost)
				){
			return true; //yes, this is definitely a Wiggle store host
		}else{
			return false; //no, don't trust this host
		}

	}
	
	public static String getCurrency(String strUserCountry){
		
		Context context = ApplicationContextProvider.getContext();

		if(strUserCountry==null){
			strUserCountry = ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY);
		}
		
		if(context.getString(R.string.str_locale_country_iso_code_uk).equals(strUserCountry)){
			return "GBP";
		}else if(context.getString(R.string.str_locale_country_iso_code_us).equals(strUserCountry)){
			return "USD";
		}else if(context.getString(R.string.str_locale_country_iso_code_fr).equals(strUserCountry)){
			return "EUR";
		}else if(context.getString(R.string.str_locale_country_iso_code_au).equals(strUserCountry)){
			return "AUD";
		}else if(context.getString(R.string.str_locale_country_iso_code_nz).equals(strUserCountry)){
			return "NZD";
		}else if(context.getString(R.string.str_locale_country_iso_code_es).equals(strUserCountry)){
			return "EUR";
		}else if(context.getString(R.string.str_locale_country_iso_code_it).equals(strUserCountry)){
			return "EUR";		
		}else if(context.getString(R.string.str_locale_country_iso_code_cn).equals(strUserCountry)){
			return "CNY";
		}else if(context.getString(R.string.str_locale_country_iso_code_jp).equals(strUserCountry)){
			return "JPY";	
		}else if(context.getString(R.string.str_locale_country_iso_code_de).equals(strUserCountry)){
			return "EUR";	
		}else if(context.getString(R.string.str_locale_country_iso_code_se).equals(strUserCountry)){
			return "SEK";			
		}else if(context.getString(R.string.str_locale_country_iso_code_nl).equals(strUserCountry)){
			return "EUR";			
		}else{
			//default: GBP
			return "GBP";
		}
		
	}
	
	public static String getDefaultDestination(String strUserCountry){
		
		Context context = ApplicationContextProvider.getContext();

		if(strUserCountry==null){
			strUserCountry = ObjAppUser.instance().getString(ObjAppUser.FIELD_COUNTRY);
		}
		
		/* Take this manually from delivery_destinations.txt saved in raw-folder */
		if(context.getString(R.string.str_locale_country_iso_code_uk).equals(strUserCountry)){
			return "1";
		}else if(context.getString(R.string.str_locale_country_iso_code_us).equals(strUserCountry)){
			return "18";
		}else if(context.getString(R.string.str_locale_country_iso_code_fr).equals(strUserCountry)){
			return "11";
		}else if(context.getString(R.string.str_locale_country_iso_code_au).equals(strUserCountry)){
			return "27";
		}else if(context.getString(R.string.str_locale_country_iso_code_nz).equals(strUserCountry)){
			return "35";
		}else if(context.getString(R.string.str_locale_country_iso_code_es).equals(strUserCountry)){
			return "13";
		}else if(context.getString(R.string.str_locale_country_iso_code_it).equals(strUserCountry)){
			return "10";	
		}else if(context.getString(R.string.str_locale_country_iso_code_cn).equals(strUserCountry)){
			return "31";			
		}else if(context.getString(R.string.str_locale_country_iso_code_jp).equals(strUserCountry)){
			return "33";
		}else if(context.getString(R.string.str_locale_country_iso_code_de).equals(strUserCountry)){
			return "9";	
		}else if(context.getString(R.string.str_locale_country_iso_code_se).equals(strUserCountry)){
			return "14";		
		}else if(context.getString(R.string.str_locale_country_iso_code_nl).equals(strUserCountry)){
			return "5";				
		}else{
			//default: UK
			return "1";
		}
		
	}
	
	public static boolean isAppSupportingWiggleStore(String strStoreHost){

		//remove www-prefix if any
		if(strStoreHost!=null) {
			strStoreHost = strStoreHost.replaceFirst("www.", "");
		}
		
		if("wiggle.co.uk".equals(strStoreHost) ||
			"wiggle.com".equals(strStoreHost) ||
			"wiggle.com.au".equals(strStoreHost) ||
			"wiggle.co.nz".equals(strStoreHost) ||
			"wiggle.fr".equals(strStoreHost) ||
			"wiggle.es".equals(strStoreHost) ||
			"wigglesport.it".equals(strStoreHost) ||
			"wiggle.cn".equals(strStoreHost) ||
			"wiggle.jp".equals(strStoreHost) ||
			"wigglesport.de".equals(strStoreHost) ||
			"wiggle.se".equals(strStoreHost) ||
			"wiggle.nl".equals(strStoreHost)
			)
		{
			return true;
			
		}else{
			return false;
		}
		
		
	}
	
	public static String getCountryNameFromHost(String strStoreHost){
		
		Context context = ApplicationContextProvider.getContext();
		
		String strCountryName = "";
		
		if("www.wiggle.co.uk".equals(strStoreHost)){
			strCountryName = context.getString(R.string.str_wiggle_store_uk);
		}else if("www.wiggle.com".equals(strStoreHost)){
			strCountryName = context.getString(R.string.str_wiggle_store_us);
		}else if("www.wiggle.com.au".equals(strStoreHost)){
			strCountryName = context.getString(R.string.str_wiggle_store_au);
		}else if("www.wiggle.co.nz".equals(strStoreHost)){
			strCountryName = context.getString(R.string.str_wiggle_store_nz);
		}else if("www.wiggle.cn".equals(strStoreHost)){
			strCountryName = context.getString(R.string.str_wiggle_store_cn);
		}else if("www.wiggle.es".equals(strStoreHost)){
			strCountryName = context.getString(R.string.str_wiggle_store_es);
		}else if("www.wiggle.jp".equals(strStoreHost)){
			strCountryName = context.getString(R.string.str_wiggle_store_jp);
		}else if("www.wiggle.fr".equals(strStoreHost)){
			strCountryName = context.getString(R.string.str_wiggle_store_fr);
		}else if("www.wiggle.ru".equals(strStoreHost)){
			strCountryName = context.getString(R.string.str_wiggle_store_ru);
		}else if("www.wiggle.nl".equals(strStoreHost)){
			strCountryName = context.getString(R.string.str_wiggle_store_nl);
		}else if("www.wigglesport.de".equals(strStoreHost)){
			strCountryName = context.getString(R.string.str_wiggle_store_de);
		}else if("www.wigglesport.it".equals(strStoreHost)){
			strCountryName = context.getString(R.string.str_wiggle_store_it);
		}else if("www.wiggle.se".equals(strStoreHost)){
			strCountryName = context.getString(R.string.str_wiggle_store_se);
		}else{
			strCountryName = context.getString(R.string.str_wiggle_store_uk); //default
		}
		
		return strCountryName;
		
		
	}
	
	public static int getDrawableByName(String strDrawableResourceFileName){
		
		if(strDrawableResourceFileName==null){
			return R.mipmap.ic_launcher; //any default would do ..
		}
		
		//make sure the file name corresponds to Android standards (lower case)
		String strAndroidResourceFileName = strDrawableResourceFileName.toLowerCase();
		
		Context context = ApplicationContextProvider.getContext();
		
		//remove extension from file name if any (.png etc)
		int dotIndex=strAndroidResourceFileName.lastIndexOf('.');
		if(dotIndex>=0) { // to prevent exception if there is no dot
			strAndroidResourceFileName=strAndroidResourceFileName.substring(0,dotIndex);
		}
		
		
		Resources resources = context.getResources();
		final int resourceId = resources.getIdentifier(strAndroidResourceFileName, "drawable",  context.getPackageName());
		
		return resourceId;
		
	}
	
	public static boolean showListviewHeaders(){

		//set to true to show order number headers in the Order tracking list view
		return false;
		
	}

	public static int getMetresFromMiles(int intMiles) {

		//TODO: not absolutely correct - but works for our purposes
		return (int) Math.round(intMiles * 1609.344);

	}

	public static int getKilometresFromMiles(int intMiles) {

		return (int) getMetresFromMiles(intMiles) / 1000;

	}


	public static int getMilesFromMetres(float flMetres) {

		//TODO: not absolutely correct - but works for our purposes
		return (int) Math.round(flMetres / 1609.344);

	}

	public static Location latLngToLocation(LatLng anyLatLng){

		Location location = new Location("");
		location.setLatitude(anyLatLng.latitude);
		location.setLongitude(anyLatLng.longitude);
		return location;

	}

	public static LatLng locationToLatLng(Location anyLocation){

		return new LatLng(anyLocation.getLatitude(), anyLocation.getLongitude());

	}

	public static String formatPriceWithCurrency(double dblPrice){

		//TODO: Wiggle events are only active for the UK and quoted in GBP
		//TODO: Adjust this method in case other market events / currencies get added
		Locale locale = new Locale("en", "GB"); //TODO: adjust ...
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(locale);
		String strCurrencyPrice = currencyFormatter.format(dblPrice);
		return strCurrencyPrice;

	}

	public static String buildAddressFromGeocoderResponse(Address address){

		// build a typical UK address (TODO: need to internationalise when required)

		String strAddress = "";

		//read address lines
		for(int i=0;i<=address.getMaxAddressLineIndex();i++){
			if(!"".equals(strAddress)){
				strAddress = strAddress + ", ";
			}
			strAddress = strAddress + address.getAddressLine(i);
		}

		/*
		not required as also in address line list
		strAddress =strAddress
				+ ", " + address.getLocality()
				+ ", " + address.getPostalCode();
		*/

		return strAddress;

	}

	private static String getTestServerUrl(String strAnyUrl){

		try{

			/* Step 1: identify which part of the incoming URL needs to be changed */
			//create URL from incoming url string
			URL originalUrl = new URL(strAnyUrl);
			//identify host
			String strHost = originalUrl.getHost();
			//identify protocol
			String strProtocol = originalUrl.getProtocol();
			//the combination of which is the String we are looking to replace
			String strReplaceThis = strProtocol + "://" + strHost;

			/* Step 2: keep the protocol of the incoming URL the same. Some features only work with https - so dont change it for http */
			String strWithThis = REDIRECT_APP_TO_TEST_SERVER;
			URL redirectUrl = new URL(strWithThis);
			String strRedirectProtocol = redirectUrl.getProtocol();
			if(!strRedirectProtocol.equals(strProtocol)){
				strWithThis = strWithThis.replace(strRedirectProtocol, strProtocol);
			}

			//replace the current protocol / host with the test server one
			String strNewUrl = strAnyUrl.replace(strReplaceThis, strWithThis);

			return strNewUrl;


		}catch(Exception e){

		}

		//Any issues - return original URL
		return strAnyUrl;

	}

	public static void setBinaryChoice(View parentView, String strUserChoice){

		Context context = ApplicationContextProvider.getContext();

		String POSITIVE = context.getString(R.string.str_choice_option_positive);
		String NEGATIVE = context.getString(R.string.str_choice_option_negative);

		//get "positive" view
		View positiveView = parentView.findViewWithTag(POSITIVE);
		View negativeView = parentView.findViewWithTag(NEGATIVE);

		//set choice option accordingly ..
		if(POSITIVE.equals(strUserChoice)){
			positiveView.setSelected(true);
			negativeView.setSelected(false);
		}else if(NEGATIVE.equals(strUserChoice)){
			positiveView.setSelected(false);
			negativeView.setSelected(true);
		}else{
			positiveView.setSelected(false);
			negativeView.setSelected(false);
		}

	}

	public static String getBinaryChoice(View parentView){

		Context context = ApplicationContextProvider.getContext();

		String NONE = context.getString(R.string.str_choice_option_none);
		String POSITIVE = context.getString(R.string.str_choice_option_positive);
		String NEGATIVE = context.getString(R.string.str_choice_option_negative);

		//get "positive" view
		View positiveView = parentView.findViewWithTag(POSITIVE);
		View negativeView = parentView.findViewWithTag(NEGATIVE);

		if(positiveView.isSelected()){
			return POSITIVE;
		}else if(negativeView.isSelected()){
			return NEGATIVE;
		}else{
			return NONE;
		}


	}

	public static void toggleBinaryChoice(View pressedView){

		//get parent view containing the pressed view
		View parentView = (View) ((ViewGroup) pressedView.getParent());

		//prevent NPEs
		if(parentView==null){
			return;
		}

		if(pressedView.getTag()==null){
			return;
		}

		String strUserChoice = pressedView.getTag().toString();

		setBinaryChoice(parentView, strUserChoice);

	}

	/**
	 * Convert the image URI to the direct file system path of the image file
	 *
	 * @param contentUri
	 * @return String
	 *
	 * Note AndroMedia: see http://stackoverflow.com/questions/12714701/deprecated-managedquery-issue
	 */

	public static String getRealPathFromURI(Uri contentUri) {
		String res = null;
		String[] proj = { MediaStore.Images.Media.DATA };
		Cursor cursor = ApplicationContextProvider.getContext().getContentResolver().query(contentUri, proj, null, null, null);
		if(cursor.moveToFirst()){;
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			res = cursor.getString(column_index);
		}
		cursor.close();
		return res;
	}

	public static void CopyStream(InputStream is, OutputStream os)
	{
		final int buffer_size=1024;
		try
		{
			byte[] bytes=new byte[buffer_size];
			for(;;)
			{
				int count=is.read(bytes, 0, buffer_size);
				if(count==-1)
					break;
				os.write(bytes, 0, count);
			}
		}
		catch(Exception ex){}
	}


	public static int deriveRequiredImageRotation(String strImagePath, boolean blnCameraImage){

		//if this is a picture taken with the camera -don't rotate it as the user would like to see it as taken
		if(blnCameraImage){
			return 0;
		}

		try {
			int rotate = 0; // no rotate
			ExifInterface exif = new ExifInterface(strImagePath);

			String orientation = exif
					.getAttribute(ExifInterface.TAG_ORIENTATION);
			Logger.printMessage(logTag, "Picture:" + strImagePath, Logger.INFO);

			BitmapFactory.Options options = new BitmapFactory.Options();

			if (orientation.equals(String
					.valueOf(ExifInterface.ORIENTATION_ROTATE_90))) {
				rotate = 90;
			} else if (orientation.equals(String
					.valueOf(ExifInterface.ORIENTATION_ROTATE_270))) {
				rotate = 270;
			} else if (orientation.equals(String
					.valueOf(ExifInterface.ORIENTATION_ROTATE_180))) {
				rotate = 180;
			}

			return rotate;

		} catch (IOException e) {
			Logger.printMessage(logTag, e.getMessage(), Logger.ERROR);
		}

		return 0;

	}


	//decodes image and scales it to reduce memory consumption
	public static Bitmap decodeAndScaleFile(File f, int intSampleSizeOneToN){

		try {
			//Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f),null,o);

			//Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize=intSampleSizeOneToN; //e.g. intSampleSizeOneToN = 4 results in 1/4 of original image size
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);

		} catch (FileNotFoundException e) {}

		return null;

	}


	public static File setupAppFileDirectory(String strSubFolder){

		File appDir;
		Context context = ApplicationContextProvider.getContext();

		//Find the dir to save cached images
		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)){
			appDir=new File(android.os.Environment.getExternalStorageDirectory(), CONST_APP_DIRECTORY + "/" + strSubFolder);
		}else{
			appDir=context.getCacheDir();
		}

		if(!appDir.exists()){
			appDir.mkdirs();
		}

		return appDir;

	}

	public static boolean appHasRequiredPermissions(Context context, String... permissions) {
		if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
			for (String permission : permissions) {
				if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
					return false;
				}
			}
		}
		return true;
	}

	public static void showMissingPermissionToast(String[] PERMISSIONS){

		AppUtils.showCustomToast("Please grant this app the following permissions to use this feature: " + Arrays.toString(PERMISSIONS), false);

	}


	public static void requestChildFocus(View targetView){

		//method is useful in scrollviews. The scrollview scrolls to reveal the widget passed to this method
		targetView.getParent().requestChildFocus(targetView,targetView);

	}

	public static boolean isUrlPdfDocument(String strUrl){


		//return "false" if we try to direct a pdf-url to google docs viewer (we need to be able to load this in HybridActivity)
		if(strUrl.startsWith(AppGlobalConstants.GOOGLE_DOCS_VIEWER)){
			return false;
		}

		//TODO: this is highly bespoke and works for certain URL patterns only. Adjust this if required
		// Wiggle use URL documents as follows
		//http://cycleinsurance2.wiggle.co.uk/sites/default/files/Wiggle%20Policy%20Wording.pdf?_ga=1.125222284.745621523.1485812398
		//https://cycleinsurance2.wiggle.co.uk/sites/default/files/Terms%20and%20Conditions.pdf?_ga=1.239048838.745621523.1485812398

		//so identifying the file type is tricky. Thus we do:
		if(strUrl.endsWith(".pdf")
			|| strUrl.contains(".pdf?")){
			return true;
		}else {
			return false;
		}

	}

	public static String reverseString(String strIn){

		String strOut = new StringBuilder(strIn).reverse().toString();
		return strOut;

	}

	/** Returns a user friendly device name */

	public static String getDeviceName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			return capitalize(model);
		}
		return capitalize(manufacturer) + " " + model;
	}


	private static String capitalize(String str) {
		if (TextUtils.isEmpty(str)) {
			return str;
		}
		char[] arr = str.toCharArray();
		boolean capitalizeNext = true;

		StringBuilder phrase = new StringBuilder();
		for (char c : arr) {
			if (capitalizeNext && Character.isLetter(c)) {
				phrase.append(Character.toUpperCase(c));
				capitalizeNext = false;
				continue;
			} else if (Character.isWhitespace(c)) {
				capitalizeNext = true;
			}
			phrase.append(c);
		}

		return phrase.toString();
	}

	public static void unbindDrawables(View view) {

		if (view.getBackground() != null) {

			if (view instanceof ImageView) {
				((ImageView) view).setImageDrawable(null);
			}

			view.getBackground().setCallback(null);
		}

		if (view instanceof ViewGroup) {
			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
				unbindDrawables(((ViewGroup) view).getChildAt(i));
			}

			((ViewGroup) view).removeAllViews();
		}

	}

	public static boolean isCurrentAppOutdated(String currentVersion, String requiredMinimumVersion){

		VersionComparator c = new VersionComparator();
		int intResult = c.compare(currentVersion, requiredMinimumVersion);



		if(intResult> 0) {
			Logger.printMessage(logTag, "VERSION-CHECK: cur_Version is greater than required_Version", Logger.INFO);
			return false;
		}
		else if(intResult< 0) {
			Logger.printMessage(logTag, "VERSION-CHECK: cur_Version is less than required_Version", Logger.INFO);
			return true;
		}
		else {
			Logger.printMessage(logTag, "VERSION-CHECK: cur_Version is equal to required_Version", Logger.INFO);
			return false;
		}

	}

}
