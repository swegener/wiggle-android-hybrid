package uk.co.wiggle.hybrid.application.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class Dialog extends DialogFragment {
	
	Context mContext;
	String  mDialogTitle;
	String  mDialogMessage;
	String  mPositiveButtonText;
	String  mNegativeButtonText;
	
	DialogActionListener mListener;
	
	//Dialog tags used across the app
	public static final String DIALOG_GO_TO_UNSUPPORTED_STORE = "DIALOG_GO_TO_UNSUPPORTED_STORE";
	public static final String DIALOG_LEAVE_APP_REVIEW_AFTER_ORDER = "DIALOG_LEAVE_APP_REVIEW_AFTER_ORDER";
    public static final String DIALOG_EVENT_LIST_SWIPE_TO_REVEAL_HINT = "DIALOG_EVENT_LIST_SWIPE_TO_REVEAL_HINT";
    public static final String DIALOG_EVENT_SCREEN_LOCATION_REQUIRED = "DIALOG_EVENT_SCREEN_LOCATION_REQUIRED";
    public static final String DIALOG_EVENT_SCREEN_UPDATE_GOOGLE_PLAY_SERVICES = "DIALOG_EVENT_SCREEN_UPDATE_GOOGLE_PLAY_SERVICES";
    public static final String DIALOG_EVENT_SCREEN_GOOGLE_API_ERROR = "DIALOG_EVENT_SCREEN_GOOGLE_API_ERROR";
    public static final String DIALOG_EDIT_EXISTING_CLAIM = "DIALOG_EDIT_EXISTING_CLAIM";
    public static final String DIALOG_DISMISS_EXISTING_CLAIM = "DIALOG_DISMISS_EXISTING_CLAIM";
    public static final String DIALOG_START_NEW_CLAIM = "DIALOG_START_NEW_CLAIM";
    public static final String DIALOG_CLAIM_ADD_EVIDENCE_PHOTO = "DIALOG_CLAIM_ADD_EVIDENCE_PHOTO";
    public static final String DIALOG_CLAIM_REMOVE_EVIDENCE_PHOTO = "DIALOG_CLAIM_REMOVE_EVIDENCE_PHOTO";
    public static final String DIALOG_CLEAR_CLAIM_DATA = "DIALOG_CLEAR_CLAIM_DATA";
    public static final String DIALOG_CALL_WIGGLE_LEGAL_AND_INSURANCE = "DIALOG_CALL_WIGGLE_LEGAL_AND_INSURANCE";
    public static final String DIALOG_CALL_EMERGENCY_SERVICES = "DIALOG_CALL_EMERGENCY_SERVICES";
    public static final String DIALOG_LEGAL_ABOUT_US = "DIALOG_LEGAL_ABOUT_US";
    public static final String DIALOG_WEBVIEW_LOAD_ERROR = "DIALOG_WEBVIEW_LOAD_ERROR";
    public static final String DIALOG_CLAIM_NOT_COMPLETE = "DIALOG_CLAIM_NOT_COMPLETE";
    public static final String DIALOG_APP_VERSION = "DIALOG_APP_VERSION";

	public Dialog(Context context, String strDialogTitle, String strDialogMessage, String strPositiveButtonText, String strNegativeButtonText){
		
		mContext = context;
		mDialogTitle = strDialogTitle;
		mDialogMessage = strDialogMessage;
		mPositiveButtonText = strPositiveButtonText;
		mNegativeButtonText = strNegativeButtonText;
		
	}
	
    @Override
    public AlertDialog onCreateDialog(Bundle savedInstanceState) {
    	
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        
        builder.setMessage(mDialogMessage)
        	   .setTitle(mDialogTitle)
        	   .setPositiveButton(mPositiveButtonText, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   // Send the positive button event back to the host activity
                	   mListener.onDialogPositiveClick(Dialog.this);
                   }
               });

        if(!"".equals(mNegativeButtonText)){
            builder.setNegativeButton(mNegativeButtonText, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // Send the negative button event back to the host activity
                    mListener.onDialogNegativeClick(Dialog.this);
                }
            });
        }

        // Create the AlertDialog object and return it
        return builder.create();
    }
    
    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface DialogActionListener {
        public void onDialogPositiveClick(DialogFragment dialog);
        public void onDialogNegativeClick(DialogFragment dialog);
    }
    
    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        //play notification sound 
//		final MediaPlayer mp = MediaPlayer.create(activity, R.raw.notification_in_app);
//		mp.start();
        
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DialogActionListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

}
