package uk.co.wiggle.hybrid.application.datamodel.objects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class ObjCustomerName extends CustomObject {
	
	public static final String OBJECT_NAME = "CustomerName";
	
	public static final String FIELD_TITLE = "Title";
	public static final String FIELD_FORENAME = "FirstName";
	public static final String FIELD_SURNAME = "Surname";
	
	private static List<String> sFieldList;
	static {
		sFieldList = Collections.synchronizedList(new ArrayList<String>());
		sFieldList.add(FIELD_TITLE);
		sFieldList.add(FIELD_FORENAME);
		sFieldList.add(FIELD_SURNAME);
		
	}

	private static ConcurrentHashMap<String, Integer> sFieldTypes;
	static {
		sFieldTypes = new ConcurrentHashMap<String, Integer>();
		
		sFieldTypes.put(FIELD_TITLE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_FORENAME, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_SURNAME, FIELD_TYPE_STRING);		

	}
	
	public ObjCustomerName() {		
		
	}
	
	public ObjCustomerName(Bundle bundle) {
		super(bundle);
	}
	
	@Override
	public String getObjectName() {
		return OBJECT_NAME;
	}
	
	@Override
	public List<String> getFields() {
		return sFieldList;
	}
	
	@Override
	public Class<?> getSubclass() {
		return ObjCustomerName.class;
	}
	
	@Override
	public Integer getFieldType(String fieldName) {
		return sFieldTypes.get(fieldName);
	}
	
	
	public static String createJSONfromName(ObjCustomerName object , CustomObject targetObject) throws JSONException{
		
		JSONArray mainArray = new JSONArray();
		List<String> fields = targetObject.getFields();
			
		JSONObject jsonObject = new JSONObject();
			
			// first do the flat fields
			for (String strFieldName : fields) {
				
				if(object.get(strFieldName) != null){
					
					int intValueFieldType = targetObject.getFieldType(strFieldName);
					
					switch (intValueFieldType) {
					
					case CustomObject.FIELD_TYPE_OBJECT:
						break;
					
					case CustomObject.FIELD_TYPE_DATETIME:
						break;
					
					default:
						jsonObject.put(strFieldName, object.get(strFieldName));
						break;
					}
					
					
				}
				
			}
			
			mainArray.put(jsonObject);
			
		
		return mainArray.toString();
		
	}
	
	
	
	public String getUserName(){
		return getString(ObjCustomerName.FIELD_FORENAME) + " " + getString(ObjCustomerName.FIELD_SURNAME);
	}
		
}

