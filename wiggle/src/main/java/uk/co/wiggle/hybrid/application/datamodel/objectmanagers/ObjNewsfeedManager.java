package uk.co.wiggle.hybrid.application.datamodel.objectmanagers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNewsfeed;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNewsfeedCategory;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNewsfeedRSSChannel;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNewsfeedRSSItem;


public class ObjNewsfeedManager {
	
	List<ObjNewsfeed> mNewsfeeds = Collections.synchronizedList(new ArrayList<ObjNewsfeed>());
	
	private static ObjNewsfeedManager sInstance = null;

	public static ObjNewsfeedManager getInstance() {
		if (sInstance == null) {
			sInstance = new ObjNewsfeedManager();
		}
		return sInstance;
	}

	protected ObjNewsfeedManager() {
		mNewsfeeds = Collections.synchronizedList(new ArrayList<ObjNewsfeed>());
	}
	
	
	/**
	 * @param location
	 * @param object
	 * @see java.util.List#add(int, java.lang.Object)
	 */
	public void add(int location, ObjNewsfeed object) {
		mNewsfeeds.add(location, object);
	}
	
	
	/**
	 * @param object
	 * @return
	 * @see java.util.List#add(java.lang.Object)
	 */
	public boolean add(ObjNewsfeed object) {
		return mNewsfeeds.add(object);
	}

	/**
	 * @param arg0
	 * @return
	 * @see java.util.List#addAll(java.util.Collection)
	 */
	public boolean addAll(Collection<? extends ObjNewsfeed> arg0) {
		return mNewsfeeds.addAll(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 * @see java.util.List#addAll(int, java.util.Collection)
	 */
	public boolean addAll(int arg0, Collection<? extends ObjNewsfeed> arg1) {
		return mNewsfeeds.addAll(arg0, arg1);
	}

	/**
	 * 
	 * @see java.util.List#clear()
	 */
	public void clear() {
		mNewsfeeds.clear();
	}

	/**
	 * @param object
	 * @return
	 * @see java.util.List#contains(java.lang.Object)
	 */
	public boolean contains(Object object) {
		return mNewsfeeds.contains(object);
	}

	/**
	 * @param object
	 * @return
	 * @see java.util.List#equals(java.lang.Object)
	 */
	public boolean equals(Object object) {
		return mNewsfeeds.equals(object);
	}

	/**
	 * @param location
	 * @return
	 * @see java.util.List#get(int)
	 */
	public ObjNewsfeed get(int location) {
		return mNewsfeeds.get(location);
	}
	
	public void set(Collection<? extends ObjNewsfeed> arg0) {
		mNewsfeeds.clear();
		mNewsfeeds.addAll(arg0);
	}
	
	/**
	 * @param object
	 * @return
	 * @see java.util.List#indexOf(java.lang.Object)
	 */
	public int indexOf(Object object) {
		return mNewsfeeds.indexOf(object);
	}

	/**
	 * @param object
	 * @return
	 * @see java.util.List#remove(java.lang.Object)
	 */
	public boolean remove(Object object) {
		return mNewsfeeds.remove(object);
	}
	
	public ObjNewsfeed remove(int index) {
		return mNewsfeeds.remove(index);
	}

	/**
	 * @param arg0
	 * @return
	 * @see java.util.List#removeAll(java.util.Collection)
	 */
	public void removeAll() {
		mNewsfeeds.clear();
	}

	/**
	 * @return
	 * @see java.util.List#size()
	 */
	public int size() {
		return mNewsfeeds.size();
	}

	public List<ObjNewsfeed> getAll() {
		return mNewsfeeds;
	}

	public boolean isEmpty() {
		return mNewsfeeds.isEmpty();
	}
	
	public String toString(){
		return mNewsfeeds.toString();
	}
	
	
	/** 
	 * 
	 * Convenience methods for retrieving the next children of a category, top level parents etc.
	 * 
	 */
	public ObjNewsfeed getRecordByUniqueId(int intUniqueId){
		
		for(int i=0;i<mNewsfeeds.size();i++){
			
			ObjNewsfeed feed = mNewsfeeds.get(i);
			
			if(feed.getUniqueId()==intUniqueId){
				return feed;
			}
			
		}

		return null;		
		
	}
	
	public ObjNewsfeed getFeedByURL(String strUrl){
		
		for(int i=0;i<mNewsfeeds.size();i++){
			
			ObjNewsfeed feed = mNewsfeeds.get(i);
			
			if(strUrl.equals(feed.getString(ObjNewsfeed.FIELD_URL))){
				return feed;
			}
			
		}

		return null;		
		
	}
	


	public boolean addIfMissing(ObjNewsfeed item) {
		if(!mNewsfeeds.contains(item)){
			mNewsfeeds.add(item);
			return true;
		}else{
			return false;
		}
	}
	
	public ArrayList<ObjNewsfeed> getEnabledFeeds(){
		
		ArrayList<ObjNewsfeed> lstCategoryFeeds = new ArrayList<ObjNewsfeed>();
		
		//check which categories this user has subscribed to
		ArrayList<ObjNewsfeedCategory> unsubscribedTopics = ObjAppUser.instance().getUnsubscribedNewsTopics();
				
		
		for(int i=0;i<mNewsfeeds.size();i++){
			
			ObjNewsfeed thisFeed = mNewsfeeds.get(i);
			List<ObjNewsfeedCategory> thisFeedCategories = thisFeed.getCategories();
			
			//we assume the user has blocked all topics. If we however find at least one topic that is not on the user's unsubscribe liset 
			//then we are good to go and can present this RSS feed to the user
			boolean blnUserUnsubscribedFromTopic = true;
			for(int j=0;j<thisFeedCategories.size();j++){
				ObjNewsfeedCategory feedTopic = thisFeedCategories.get(j);
				if(!unsubscribedTopics.contains(feedTopic)){
					blnUserUnsubscribedFromTopic = false;
					break; //user unsubscribed from at least one related topic - break loop
				}
			}
			
			//no topic of interest found on this feed (i.e. this feed is not linked to any of the topics the user wants to see) - continue with next feed
			if(blnUserUnsubscribedFromTopic){
				continue;
			}
			
			//user subscribed to feed - get all RSS channels and RSS items
			if(thisFeed.getBoolean(ObjNewsfeed.FIELD_DEFAULT_ON)){
				lstCategoryFeeds.add(thisFeed);
			}
			
		}
		
		return lstCategoryFeeds;
		
	}
	
	public ArrayList<ObjNewsfeedRSSItem> getAllNews(){
		
		ArrayList<ObjNewsfeedRSSItem> lstAllNews = new ArrayList<ObjNewsfeedRSSItem>();
		
		//check which categories this user has subscribed to
		ArrayList<ObjNewsfeedCategory> unsubscribedTopics = ObjAppUser.instance().getUnsubscribedNewsTopics();
		
		for(int i=0;i<mNewsfeeds.size();i++){
			
			ObjNewsfeed myFeed = mNewsfeeds.get(i);
			
			//get newsfeed categories
			List<ObjNewsfeedCategory> feedCategories = myFeed.getCategories();
			
			//we assume the user has blocked all topics. If we however find at least one topic that is not on the user's unsubscribe liset 
			//then we are good to go and can present this RSS feed to the user
			boolean blnUserUnsubscribedFromTopic = true;
			for(int j=0;j<feedCategories.size();j++){
				ObjNewsfeedCategory feedTopic = feedCategories.get(j);
				if(!unsubscribedTopics.contains(feedTopic)){
					blnUserUnsubscribedFromTopic = false;
					break; //user unsubscribed from at least one related topic - break loop
				}
			}
			
			//no topic of interest found on this feed (i.e. this feed is not linked to any of the topics the user wants to see) - continue with next feed
			if(blnUserUnsubscribedFromTopic){
				continue;
			}
			
			//user subscribed to feed - get all RSS channels and RSS items
			List<ObjNewsfeedRSSChannel> allChannels = myFeed.getRSSChannels();
			
			for(int j=0;j<allChannels.size();j++){
				ObjNewsfeedRSSChannel myChannel = allChannels.get(j);
				List<ObjNewsfeedRSSItem> allItems = myChannel.getRssItems();
				lstAllNews.addAll(allItems);
			}
			
			
		}
		
		return lstAllNews;
		
	}

}
