package uk.co.wiggle.hybrid.application.datamodel.objectmanagers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesItem;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjMegamenuItem;


public class ObjMegamenuManager {
	
	List<ObjMegamenuItem> mMegamenuItems = Collections.synchronizedList(new ArrayList<ObjMegamenuItem>());
	
	private static ObjMegamenuManager sInstance = null;

	public static ObjMegamenuManager getInstance() {
		if (sInstance == null) {
			sInstance = new ObjMegamenuManager();
		}
		return sInstance;
	}

	protected ObjMegamenuManager() {
		mMegamenuItems = Collections.synchronizedList(new ArrayList<ObjMegamenuItem>());
	}
	
	
	/**
	 * @param location
	 * @param object
	 * @see List#add(int, Object)
	 */
	public void add(int location, ObjMegamenuItem object) {
		mMegamenuItems.add(location, object);
	}
	
	
	/**
	 * @param object
	 * @return
	 * @see List#add(Object)
	 */
	public boolean add(ObjMegamenuItem object) {
		return mMegamenuItems.add(object);
	}

	/**
	 * @param arg0
	 * @return
	 * @see List#addAll(Collection)
	 */
	public boolean addAll(Collection<? extends ObjMegamenuItem> arg0) {
		return mMegamenuItems.addAll(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 * @see List#addAll(int, Collection)
	 */
	public boolean addAll(int arg0, Collection<? extends ObjMegamenuItem> arg1) {
		return mMegamenuItems.addAll(arg0, arg1);
	}

	/**
	 * 
	 * @see List#clear()
	 */
	public void clear() {
		mMegamenuItems.clear();
	}

	/**
	 * @param object
	 * @return
	 * @see List#contains(Object)
	 */
	public boolean contains(Object object) {
		return mMegamenuItems.contains(object);
	}

	/**
	 * @param object
	 * @return
	 * @see List#equals(Object)
	 */
	public boolean equals(Object object) {
		return mMegamenuItems.equals(object);
	}

	/**
	 * @param location
	 * @return
	 * @see List#get(int)
	 */
	public ObjMegamenuItem get(int location) {
		return mMegamenuItems.get(location);
	}
	
	public void set(Collection<? extends ObjMegamenuItem> arg0) {
		mMegamenuItems.clear();
		mMegamenuItems.addAll(arg0);
	}
	
	/**
	 * @param object
	 * @return
	 * @see List#indexOf(Object)
	 */
	public int indexOf(Object object) {
		return mMegamenuItems.indexOf(object);
	}

	/**
	 * @param object
	 * @return
	 * @see List#remove(Object)
	 */
	public boolean remove(Object object) {
		return mMegamenuItems.remove(object);
	}
	
	public ObjMegamenuItem remove(int index) {
		return mMegamenuItems.remove(index);
	}

	/**
	 * @param
	 * @return
	 * @see List#removeAll(Collection)
	 */
	public void removeAll() {
		mMegamenuItems.clear();
	}

	/**
	 * @return
	 * @see List#size()
	 */
	public int size() {
		return mMegamenuItems.size();
	}

	public List<ObjMegamenuItem> getAll() {
		return mMegamenuItems;
	}

	public boolean isEmpty() {
		return mMegamenuItems.isEmpty();
	}
	
	public String toString(){
		return mMegamenuItems.toString();
	}
	
	
	/** 
	 * 
	 * Convenience methods for easy access to data in this manager
	 * 
	 */
	public ObjMegamenuItem getRecordByDataMapId(String strCountryCode, String strDataMapId){
		
		for(int i=0;i<mMegamenuItems.size();i++){
			
			ObjMegamenuItem item = mMegamenuItems.get(i);
			
			if(strCountryCode.equals(item.getString(ObjMegamenuItem.FIELD_COUNTRY_CODE))){
				if(strDataMapId.equals(item.getString(ObjMegamenuItem.FIELD_DATA_MAP))) {
					return item;
				}
			}
			
		}

		return null;		
		
	}
	
	public boolean addIfMissing(ObjMegamenuItem item) {
		if(!mMegamenuItems.contains(item)){
			mMegamenuItems.add(item);
			return true;
		}else{
			return false;
		}
	}


}
