package uk.co.wiggle.hybrid.application.datamodel.objectmanagers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNavigationCategory ;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.application.utils.DateUtils;
import uk.co.wiggle.hybrid.usecase.shop.classicnav.activities.HybridActivity;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.activities.TabbedHybridActivity;
import uk.co.wiggle.hybrid.usecase.shop.tabbednav.fragments.FragmentShopNavigation;
import uk.co.wiggle.hybrid.usecase.shop.version3.activities.V3HybridActivity;
import uk.co.wiggle.hybrid.usecase.shop.version3.fragments.V3FragmentShopNavigation;
import uk.co.wiggle.hybrid.usecase.startup.activities.StartupActivity;


public class ObjNavigationCategoryManager {

	//category list storing the local navigation tree shipped with this app (raw folder json)
	//the local navigation tree for this application
	ObjNavigationCategory localNavigationTree;

	public ObjNavigationCategory getLocalNavigationTree(){
		return localNavigationTree;
	}

	public void setLocalNavigationTree(ObjNavigationCategory categories){
		localNavigationTree = categories;
	}


	//list storing the menu items that are configured remotely by Wiggle via the Contentful API
	List<ObjNavigationCategory > mContentfulMenuItems = Collections.synchronizedList(new ArrayList<ObjNavigationCategory >());

	private static ObjNavigationCategoryManager sInstance = null;

	public static ObjNavigationCategoryManager getInstance() {
		if (sInstance == null) {
			sInstance = new ObjNavigationCategoryManager();
		}
		return sInstance;
	}

	protected ObjNavigationCategoryManager() {
		mContentfulMenuItems = Collections.synchronizedList(new ArrayList<ObjNavigationCategory >());
	}
	
	
	/**
	 * @param location
	 * @param object
	 * @see List#add(int, Object)
	 */
	public void add(int location, ObjNavigationCategory  object) {
		mContentfulMenuItems.add(location, object);
	}
	
	
	/**
	 * @param object
	 * @return
	 * @see List#add(Object)
	 */
	public boolean add(ObjNavigationCategory  object) {
		return mContentfulMenuItems.add(object);
	}

	/**
	 * @param arg0
	 * @return
	 * @see List#addAll(Collection)
	 */
	public boolean addAll(Collection<? extends ObjNavigationCategory > arg0) {
		return mContentfulMenuItems.addAll(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 * @return
	 * @see List#addAll(int, Collection)
	 */
	public boolean addAll(int arg0, Collection<? extends ObjNavigationCategory > arg1) {
		return mContentfulMenuItems.addAll(arg0, arg1);
	}

	/**
	 * 
	 * @see List#clear()
	 */
	public void clear() {
		mContentfulMenuItems.clear();
	}

	/**
	 * @param object
	 * @return
	 * @see List#contains(Object)
	 */
	public boolean contains(Object object) {
		return mContentfulMenuItems.contains(object);
	}

	/**
	 * @param object
	 * @return
	 * @see List#equals(Object)
	 */
	public boolean equals(Object object) {
		return mContentfulMenuItems.equals(object);
	}

	/**
	 * @param location
	 * @return
	 * @see List#get(int)
	 */
	public ObjNavigationCategory  get(int location) {
		return mContentfulMenuItems.get(location);
	}
	
	public void set(Collection<? extends ObjNavigationCategory > arg0) {
		mContentfulMenuItems.clear();
		mContentfulMenuItems.addAll(arg0);
	}
	
	/**
	 * @param object
	 * @return
	 * @see List#indexOf(Object)
	 */
	public int indexOf(Object object) {
		return mContentfulMenuItems.indexOf(object);
	}

	/**
	 * @param object
	 * @return
	 * @see List#remove(Object)
	 */
	public boolean remove(Object object) {
		return mContentfulMenuItems.remove(object);
	}
	
	public ObjNavigationCategory  remove(int index) {
		return mContentfulMenuItems.remove(index);
	}

	/**
	 * @param
	 * @return
	 * @see List#removeAll(Collection)
	 */
	public void removeAll() {
		mContentfulMenuItems.clear();
	}

	/**
	 * @return
	 * @see List#size()
	 */
	public int size() {
		return mContentfulMenuItems.size();
	}

	public List<ObjNavigationCategory > getAll() {
		return mContentfulMenuItems;
	}

	public boolean isEmpty() {
		return mContentfulMenuItems.isEmpty();
	}
	
	public String toString(){
		return mContentfulMenuItems.toString();
	}
	
	
	/** 
	 * 
	 * Convenience methods for easy access to data in this manager
	 * 
	 */
	
	
	public boolean addIfMissing(ObjNavigationCategory  item) {
		if(!mContentfulMenuItems.contains(item)){
			mContentfulMenuItems.add(item);
			return true;
		}else{
			return false;
		}
	}
	
	public List<ObjNavigationCategory > getAllOrderedCategories(){
		
		
		List<ObjNavigationCategory > lstNavCategories = getAll();

		//sort NavCategories
		SortNavCategories sortNavCategories = new SortNavCategories();
		Collections.sort(lstNavCategories, sortNavCategories);
		

		return lstNavCategories;
		
	}
	


	class SortNavCategories implements Comparator<ObjNavigationCategory > {

		@Override
		public int compare(ObjNavigationCategory  navCategory1, ObjNavigationCategory  navCategory2) {


			double item1OrderIndex = navCategory1.getDouble(ObjNavigationCategory.FIELD_ORDER_ID);
			double item2OrderIndex = navCategory2.getDouble(ObjNavigationCategory.FIELD_ORDER_ID);

			return Double.compare(item1OrderIndex, item2OrderIndex);

			
		}

	}

	/*
	public ArrayList<ObjNavigationCategory > getFeaturedNavCategories(){

		ArrayList<ObjNavigationCategory > lstFeaturedNavCategories = new ArrayList<ObjNavigationCategory >();

		for(int i=0;i<mContentfulMenuItems.size();i++){
			ObjNavigationCategory  NavCategory = mContentfulMenuItems.get(i);
			if(NavCategory.isFeatured()){
				lstFeaturedNavCategories.add(NavCategory);
			}
		}

		return lstFeaturedNavCategories;

	}

	*/

	public ObjNavigationCategory getCombinedNavigationTree(String strMenuRequestor){

		//this method combines local and contentful remote navigation items

		//default: get entire navigation tree
		ObjNavigationCategory wiggleRoot = getLocalNavigationTree();


		//if we dont have a local navigation tree yet (parsing not finished) - return null
		if(wiggleRoot==null){
			return null;
		}

		//for app-versions above 1: get the tabbed navigation tree (i.e. level 1 shop categories only)
		if(AppUtils.getWiggleVersion().getVersionCode()>AppUtils.APP_VERSION.VERSION_1_HYBRID.getVersionCode()){
			wiggleRoot = getTabbedNavigationTree();
		}



		//get all contentful menu items we need to show in combination with the local navigation tree
		//we are creating an artificial root category that will store all contentful child nodes.
		ObjNavigationCategory contentfulRootCategory = new ObjNavigationCategory();
		contentfulRootCategory.setField(ObjNavigationCategory.FIELD_TYPE, ObjNavigationCategory.CONST_CATEGORY_TYPE_SUBMENU);
		contentfulRootCategory.setField(ObjNavigationCategory.FIELD_TITLE, "Contentful");
		//now add all the contentful items from this manager
		contentfulRootCategory.setChildren(getContentfulDisplayMenuItems(strMenuRequestor));
		//get the list of all children for the current country
		List<ObjNavigationCategory> contentfulMenuList = contentfulRootCategory.getChildrenForCurrentCountry();

		//create a separate list so we dont mess with our original local / contentful item lists
		ObjNavigationCategory combinedRoot = new ObjNavigationCategory();
		combinedRoot.setField(ObjNavigationCategory.FIELD_TYPE, ObjNavigationCategory.CONST_CATEGORY_TYPE_SUBMENU);
		combinedRoot.setField(ObjNavigationCategory.FIELD_TITLE, ApplicationContextProvider.getContext().getString(R.string.app_name));
		//add local navigation tree to combinedRoot
		combinedRoot.setChildren(wiggleRoot.getChildrenForCurrentCountry());
		//Add contentful items to beginning of the list
		combinedRoot.addChildrenAtPosition(contentfulMenuList, 0);

		return combinedRoot;

	}

	public void clearNavigationTrees(){
		setLocalNavigationTree(null);
		mContentfulMenuItems.clear();
	}

	private void updateMenuLists(final AppRequestHandler.ON_RESPONSE_ACTION responseAction){


		//set navigation tree
		if (StartupActivity.instance() != null) {


			StartupActivity.instance().runOnUiThread(
					new Thread(new Runnable() {
						public void run() {
							//refresh menu list
							StartupActivity.instance().updateMoreFromMiggleMenu();
						}
					}));

		}

		if (AppRequestHandler.ON_RESPONSE_ACTION.REFRESH_NAVIGATION_DRAWER.equals(responseAction)) {

			if (HybridActivity.instance() != null) {

				//triggered from Hybrid activity
				HybridActivity.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								//refresh menu list
								HybridActivity.instance().setCurrentNavigationCategory(getCombinedNavigationTree(HybridActivity.getLogTag()), false);
							}
						}));

			}else if (TabbedHybridActivity.instance() != null) {

				//triggered from Hybrid activity
				TabbedHybridActivity.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								//refresh menu list
								if(FragmentShopNavigation.instance()!=null
										&& FragmentShopNavigation.instance().isAdded()) {
									FragmentShopNavigation.instance().setCurrentNavigationCategory(getCombinedNavigationTree(TabbedHybridActivity.getLogTag()), false);
								}
							}
						}));

			}else if (V3HybridActivity.instance() != null) {

				//triggered from Hybrid activity
				V3HybridActivity.instance().runOnUiThread(
						new Thread(new Runnable() {
							public void run() {
								//refresh menu list
								if(V3FragmentShopNavigation.instance()!=null
										&& V3FragmentShopNavigation.instance().isAdded()) {
									V3FragmentShopNavigation.instance().setCurrentNavigationCategory(getCombinedNavigationTree(V3HybridActivity.getLogTag()), false);
								}
							}
						}));

			}

		}

	}

	public List<ObjNavigationCategory> getContentfulDisplayMenuItems(String strMenuRequestor){

		List<ObjNavigationCategory> orderedContentfulItems = getAllOrderedCategories();
		ArrayList<ObjNavigationCategory> displayList = new ArrayList<ObjNavigationCategory>();

		//filter out anything we don't need
		for(int i=0;i<orderedContentfulItems.size();i++){

			ObjNavigationCategory menuItem = orderedContentfulItems.get(i);

			//make sure the item is active
			if(!"".equals(menuItem.getString(ObjNavigationCategory.FIELD_START_DATE))
					|| !"".equals(menuItem.getString(ObjNavigationCategory.FIELD_END_DATE))
					){

				Date startDate = DateUtils.getDateFromContentfulString(menuItem, ObjNavigationCategory.FIELD_START_DATE);
				Date endDate = DateUtils.getDateFromContentfulString(menuItem, ObjNavigationCategory.FIELD_END_DATE);
				//Item not active? Do not display it
				if (!menuItem.between(startDate, endDate)) {
					continue;
				}

			}

			//check if this contentful item needs returning for HybridActivity
			if(HybridActivity.getLogTag().equals(strMenuRequestor)){
				//check if item should be displayed on navigation drawer in HybridActivity
				if(!menuItem.getBoolean(ObjNavigationCategory.FIELD_SHOW_ON_NAV_DRAWER)){
					//display suppressed? don't add item to list
					continue;
				}
			}

			//check if this contentful item needs returning for TabbedHybridActivity
			if(TabbedHybridActivity.getLogTag().equals(strMenuRequestor)){
				//check if item should be displayed on navigation drawer in HybridActivity
				if(!menuItem.getBoolean(ObjNavigationCategory.FIELD_SHOW_ON_NAV_DRAWER)){
					//display suppressed? don't add item to list
					continue;
				}
			}

			//check if this contentful item needs returning for TabbedHybridActivity
			if(V3HybridActivity.getLogTag().equals(strMenuRequestor)){
				//check if item should be displayed on navigation drawer in HybridActivity
				if(!menuItem.getBoolean(ObjNavigationCategory.FIELD_SHOW_ON_NAV_DRAWER)){
					//display suppressed? don't add item to list
					continue;
				}
			}


			//check if this contentful item needs returning for StartupActivity
			if(StartupActivity.getLogTag().equals(strMenuRequestor)){
				//check if item should be displayed on navigation drawer in HybridActivity
				if(!menuItem.getBoolean(ObjNavigationCategory.FIELD_SHOW_ON_STARTUP)){
					//display suppressed? don't add item to list
					continue;
				}
			}


			//if we make it to here none of the above exclusions applied .. add item to display list
			displayList.add(menuItem);


		}

		return displayList;

	}

	public ArrayList<ObjNavigationCategory> getStartupMoreFromWiggleItems(){

		ArrayList<ObjNavigationCategory> lstMenuItems = new ArrayList<ObjNavigationCategory>();

		//get combined menu tree
		ObjNavigationCategory combinedRootCategory = getCombinedNavigationTree(StartupActivity.getLogTag());

		//if navigation tree has not been built / retrieved yet - return
		if(combinedRootCategory==null){
			return lstMenuItems;
		}

		//loop through tree (1st level children) and add relevant items to list
		List<ObjNavigationCategory> level1Items = combinedRootCategory.getChildrenForCurrentCountry();
		for(int i=0;i<level1Items.size();i++){
			ObjNavigationCategory menuItem = level1Items.get(i);
			//check if this item is meant to be displayed on StartupActivity (from local nav tree as well as from remote contentful nav tree)
			if(menuItem.getBoolean(ObjNavigationCategory.FIELD_SHOW_ON_STARTUP)) {
				lstMenuItems.add(menuItem);
			}
		}

		return lstMenuItems;

	}

	public ObjNavigationCategory getCategoryByMenuItemTarget(String strTarget){

		List<ObjNavigationCategory> lstLocalCategories = localNavigationTree.getAllChildren();

		for(int i=0;i<lstLocalCategories.size();i++){

			ObjNavigationCategory category = lstLocalCategories.get(i);
			if(strTarget.equals(category.getString(ObjNavigationCategory.FIELD_MENU_ITEM_TARGET))){
				return category;
			}

		}

		return null;
	}

	public ObjNavigationCategory getCategoryByGTMID(String strGTMID){

		List<ObjNavigationCategory> lstLocalCategories = localNavigationTree.getAllChildren();

		for(int i=0;i<lstLocalCategories.size();i++){

			ObjNavigationCategory category = lstLocalCategories.get(i);
			if(strGTMID.equals(category.getString(ObjNavigationCategory.FIELD_GOOGLE_TAG_MANAGER_ID))){
				return category;
			}

		}

		return null;
	}


	public ObjNavigationCategory getCategoryByType(String strType){

		List<ObjNavigationCategory> lstLocalCategories = localNavigationTree.getAllChildren();

		for(int i=0;i<lstLocalCategories.size();i++){

			ObjNavigationCategory category = lstLocalCategories.get(i);
			if(strType.equals(category.getString(ObjNavigationCategory.FIELD_TYPE))){
				return category;
			}

		}

		return null;
	}

	private ObjNavigationCategory getTabbedNavigationTree(){

		//get the navigation category "Shop by department" and return this
		//This is the only "submenu" type in the navigation tree - so use this identifier
		ObjNavigationCategory tabbedHomeCategory = getCategoryByType(ObjNavigationCategory.CONST_CATEGORY_TYPE_SUBMENU);

		//add children to this category:
		//- Events
		//News not to be shown anymore
		ObjNavigationCategory eventCategory = getCategoryByGTMID("shopEvents");
		appendThisToHomeCategory(tabbedHomeCategory, eventCategory);

		/* Sale category needs showing (Removed in version 3.3.2 on Wiggle's request
		ObjNavigationCategory saleCategory = getCategoryByGTMID("shopSale");
		appendThisToHomeCategory(tabbedHomeCategory, saleCategory);
		*/

		//Legal & Insurance
		ObjNavigationCategory insuranceCategory = getCategoryByGTMID("shopCycleInsurance");
		appendThisToHomeCategory(tabbedHomeCategory, insuranceCategory);

		//Buyers guides need showing
		ObjNavigationCategory buyersGuide = getCategoryByGTMID("shopBuyerGuides"); //TODO: make this more generic by giving categories in json raw files a unique ID
		appendThisToHomeCategory(tabbedHomeCategory, buyersGuide);

		//New category no longer needs showing
		//ObjNavigationCategory newCategory = getCategoryByGTMID("shopNew");
		//appendThisToHomeCategory(tabbedHomeCategory, newCategory);

		return tabbedHomeCategory;

	}

	private void appendThisToHomeCategory(ObjNavigationCategory tabbedHomeCategory, ObjNavigationCategory categoryToAdd){

		if(categoryToAdd!=null) {
			if (!tabbedHomeCategory.getAllChildren().contains(categoryToAdd)) {
				tabbedHomeCategory.addChild(categoryToAdd);
			}
		}

	}


}
