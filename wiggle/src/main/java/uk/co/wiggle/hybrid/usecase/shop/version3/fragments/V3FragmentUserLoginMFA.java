package uk.co.wiggle.hybrid.usecase.shop.version3.fragments;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.yakivmospan.scytale.Crypto;
import com.yakivmospan.scytale.Options;
import com.yakivmospan.scytale.Store;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import javax.crypto.SecretKey;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.animations.AppAnimations;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjApiRequest;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjGeneric;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjHoehle;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjTFA;
import uk.co.wiggle.hybrid.application.helpers.AppGlobalConstants;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.application.session.AppSession;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.application.utils.DateUtils;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentSelfCloseListener;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentUI;
import uk.co.wiggle.hybrid.tagging.AppTagManager;
import uk.co.wiggle.hybrid.usecase.shop.version3.activities.V3HybridActivity;

/**
 * @author Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 *
 * @license The code of this Application is licensed to Wiggle UK Ltd.
 * The license and its ownership rights can not be transferred to a Third Party.
 *
 * @android If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 **/


public class V3FragmentUserLoginMFA extends Fragment implements IAppFragmentUI {

    private static final String logTag = "V3FragmentUserLoginMFA";

    public static String LOGIN_SCREEN_CALLER = "LOGIN_SCREEN_CALLER";

    public static String getLogTag(){
        return logTag;
    }

    //Scrollview
    ScrollView svUserLoginMFA;

    //Home
    RelativeLayout userLoginMFA_home;
    TextView userLoginMFA_tvTitle;
    TextView userLoginMFA_tvError;
    TextView userLoginMFA_tvEmail;
    EditText userLoginMFA_etEmail;
    TextView userLoginMFA_tvPassword;
    EditText userLoginMFA_etPassword;
    TextView userLoginMFA_tvRememberMyDetails;
    LinearLayout userLoginMFA_lytError;
    TextView userLoginMFA_tvForgotPassword;
    TextView userLoginMFA_tvRecoverMyPassword;
    ProgressBar userLoginMFA_pbLoginInProgress;
    TextView userLoginMFA_tvLogin;
    TextView userLoginMFA_tvOr;
    TextView userLoginMFA_tvRegister;
    ImageView userLoginMFA_ivClearPassword;
    ImageView userLoginMFA_ivClearEmail;
    Button userLoginMFA_cbRememberMyDetails;
    RelativeLayout userLoginMFA_btnLogin;


    //TSV
    RelativeLayout userLoginMFA_tsv;
    TextView userLoginMFA_tsv_tvTitle;
    TextView userLoginMFA_tsv_tvInfo;
    TextView userLoginMFA_tsv_tvError;
    TextView userLoginMFA_tsv_tvOTP;
    TextView userLoginMFA_tsv_tvCountdown;
    EditText userLoginMFA_tsv_etOTP;
    ImageView userLoginMFA_tsv_ivClearOTP;
    ProgressBar userLoginMFA_tsv_pbLoginInProgress;
    TextView userLoginMFA_tsv_tvLogin;
    TextView userLoginMFA_tsv_tvEnterRevoveryCode;
    TextView userLoginMFA_tsv_tvLoginHelp;
    RelativeLayout userLoginMFA_tsv_btnLogin;

    /*
        The countdown fires as soon as the "mfa" endpoint responded to use that MFA is required.
        The user now has 5 minutes to either submit his OTP code or Recovery code for the current session
     */
    CountDownTimer mfaCountdownTimer;


    //Recover
    RelativeLayout userLoginMFA_recover;
    TextView userLoginMFA_recover_tvTitle;
    TextView userLoginMFA_recover_tvInfo;
    TextView userLoginMFA_recover_tvError;
    TextView userLoginMFA_recover_tvCountdown;
    TextView userLoginMFA_recover_tvOTP;
    EditText userLoginMFA_recover_etRecoveryCode;
    ImageView userLoginMFA_recover_ivClearRecoveryCode;
    ProgressBar userLoginMFA_recover_pbSubmissionInProgress;
    TextView userLoginMFA_recover_tvSubmit;
    TextView userLoginMFA_recover_tvLoginHelp;
    RelativeLayout userLoginMFA_recover_btnSubmit;
    CountDownTimer accountRecoveryCountdownTimer;

    //Account recovered
    RelativeLayout userLoginMFA_recovered;
    TextView userLoginMFA_recovered_tvTitle;
    TextView userLoginMFA_recovered_tvInfo;
    TextView userLoginMFA_recovered_tvRecoveryCode;
    TextView userLoginMFA_recovered_tvUserConfirmation;
    RelativeLayout userLoginMFA_recovered_btnContinue;
    TextView userLoginMFA_recovered_tvContinue;
    LinearLayout userLoginMFA_recovered_btnCopy;
    ImageView userLoginMFA_recovered_ivCheck;
    ImageView userLoginMFA_recovered_ivCopy;
    Button userLoginMFA_recovered_cbUserConfirmation;

    //Account locked
    RelativeLayout userLoginMFA_locked;
    TextView userLoginMFA_locked_tvTitle;
    TextView userLoginMFA_locked_tvInfo;
    TextView userLoginMFA_locked_tvHome;

    IAppFragmentSelfCloseListener selfCloseListener;


    String strCaller = "";
    int mLoginScreenCaller = 0;

    public int getLoginScreenCaller(){
        return mLoginScreenCaller;
    }

    private String mLoginVerificationId = null;

    public void setLoginVerificationId(String strLoginVerificationId){
        this.mLoginVerificationId = strLoginVerificationId;
    }

    public String getLoginVerificationId(){
        return this.mLoginVerificationId;
    }

    private String mNewRecoveryCode = null;

    public void setNewRecoveryCode(String strNewRecoveryCode){
        this.mNewRecoveryCode = strNewRecoveryCode;
    }

    public String getNewRecoveryCode(){
        return this.mNewRecoveryCode;
    }

    private static V3FragmentUserLoginMFA myInstance;

    public static V3FragmentUserLoginMFA instance(){
        return myInstance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myInstance = this;


        /** Getting the arguments to the Bundle object */
        Bundle data = getArguments();

        // Get the extras
        if (!data.containsKey(AppGlobalConstants.CALLING_ACTIVITY)) {
            throw new IllegalArgumentException("The " + AppGlobalConstants.CALLING_ACTIVITY + " must be passed as extra for the intent. "
                    + "The content of the extras is " + data);
        }

        strCaller = data.getString(AppGlobalConstants.CALLING_ACTIVITY);

        mLoginScreenCaller = data.getInt(LOGIN_SCREEN_CALLER, 0);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.my_account_login_mfa, container, false);


        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

       updateFragmentContent(view);

    }


    @Override
    public void updateFragmentContent(View view) {


        findViews(view);

        setCustomFontTypes();

    }



    public void findViews(View view) {

        svUserLoginMFA = (ScrollView) view.findViewById(R.id.svUserLoginMFA);

        //Home
        userLoginMFA_home = (RelativeLayout) view.findViewById(R.id.userLoginMFA_home);
        userLoginMFA_tvTitle = (TextView) view.findViewById(R.id.userLoginMFA_tvTitle);
        userLoginMFA_tvError = (TextView) view.findViewById(R.id.userLoginMFA_tvError);
        userLoginMFA_tvEmail = (TextView) view.findViewById(R.id.userLoginMFA_tvEmail);
        userLoginMFA_etEmail = (EditText) view.findViewById(R.id.userLoginMFA_etEmail);
        userLoginMFA_tvPassword = (TextView) view.findViewById(R.id.userLoginMFA_tvPassword);
        userLoginMFA_etPassword = (EditText) view.findViewById(R.id.userLoginMFA_etPassword);

        userLoginMFA_etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                userLoginMFA_btnLogin.setEnabled(isLoginFieldsPopulated());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        userLoginMFA_etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                userLoginMFA_btnLogin.setEnabled(isLoginFieldsPopulated());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        userLoginMFA_etEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    userLoginMFA_btnLogin.setEnabled(isLoginFieldsPopulated());
                }
            }
        });

        userLoginMFA_etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    userLoginMFA_btnLogin.setEnabled(isLoginFieldsPopulated());
                }
            }
        });

        userLoginMFA_tvRememberMyDetails = (TextView) view.findViewById(R.id.userLoginMFA_tvRememberMyDetails);

        userLoginMFA_lytError = (LinearLayout) view.findViewById(R.id.userLoginMFA_lytError);
        userLoginMFA_tvRecoverMyPassword = (TextView) view.findViewById(R.id.userLoginMFA_tvRecoverMyPassword);
        userLoginMFA_tvForgotPassword = (TextView) view.findViewById(R.id.userLoginMFA_tvForgotPassword);
        userLoginMFA_pbLoginInProgress = (ProgressBar) view.findViewById(R.id.userLoginMFA_pbLoginInProgress);
        userLoginMFA_tvLogin = (TextView) view.findViewById(R.id.userLoginMFA_tvLogin);
        userLoginMFA_tvOr = (TextView) view.findViewById(R.id.userLoginMFA_tvOr);
        userLoginMFA_tvRegister = (TextView) view.findViewById(R.id.userLoginMFA_tvRegister);
        userLoginMFA_ivClearPassword = (ImageView) view.findViewById(R.id.userLoginMFA_ivClearPassword);
        userLoginMFA_ivClearEmail = (ImageView) view.findViewById(R.id.userLoginMFA_ivClearEmail);
        userLoginMFA_cbRememberMyDetails = (Button) view.findViewById(R.id.userLoginMFA_cbRememberMyDetails);
        userLoginMFA_btnLogin = (RelativeLayout) view.findViewById(R.id.userLoginMFA_btnLogin);

        //TSV
        userLoginMFA_tsv = (RelativeLayout) view.findViewById(R.id.userLoginMFA_tsv);
        userLoginMFA_tsv_tvTitle = (TextView) view.findViewById(R.id.userLoginMFA_tsv_tvTitle);
        userLoginMFA_tsv_tvInfo = (TextView) view.findViewById(R.id.userLoginMFA_tsv_tvInfo);
        userLoginMFA_tsv_tvError = (TextView) view.findViewById(R.id.userLoginMFA_tsv_tvError);
        userLoginMFA_tsv_tvOTP = (TextView) view.findViewById(R.id.userLoginMFA_tsv_tvOTP);
        userLoginMFA_tsv_tvCountdown = (TextView) view.findViewById(R.id.userLoginMFA_tsv_tvCountdown);
        userLoginMFA_tsv_etOTP = (EditText) view.findViewById(R.id.userLoginMFA_tsv_etOTP);
        userLoginMFA_tsv_ivClearOTP = (ImageView) view.findViewById(R.id.userLoginMFA_tsv_ivClearOTP);
        userLoginMFA_tsv_pbLoginInProgress = (ProgressBar) view.findViewById(R.id.userLoginMFA_tsv_pbLoginInProgress);
        userLoginMFA_tsv_tvLogin = (TextView) view.findViewById(R.id.userLoginMFA_tsv_tvLogin);
        userLoginMFA_tsv_tvEnterRevoveryCode = (TextView) view.findViewById(R.id.userLoginMFA_tsv_tvEnterRevoveryCode);
        userLoginMFA_tsv_tvLoginHelp = (TextView) view.findViewById(R.id.userLoginMFA_tsv_tvLoginHelp);
        userLoginMFA_tsv_btnLogin = (RelativeLayout) view.findViewById(R.id.userLoginMFA_tsv_btnLogin);

        userLoginMFA_tsv_etOTP.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    userLoginMFA_tsv_btnLogin.setEnabled(isOTPFieldPopulated());
                }
            }
        });

        userLoginMFA_tsv_etOTP.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                userLoginMFA_tsv_btnLogin.setEnabled(s.length() > 0);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        //Recover
        userLoginMFA_recover = (RelativeLayout) view.findViewById(R.id.userLoginMFA_recover);
        userLoginMFA_recover_tvTitle = (TextView) view.findViewById(R.id.userLoginMFA_recover_tvTitle);
        userLoginMFA_recover_tvInfo = (TextView) view.findViewById(R.id.userLoginMFA_recover_tvInfo);
        userLoginMFA_recover_tvError = (TextView) view.findViewById(R.id.userLoginMFA_recover_tvError);
        userLoginMFA_recover_tvCountdown = (TextView) view.findViewById(R.id.userLoginMFA_recover_tvCountdown);
        userLoginMFA_recover_tvOTP = (TextView) view.findViewById(R.id.userLoginMFA_recover_tvOTP);
        userLoginMFA_recover_etRecoveryCode = (EditText) view.findViewById(R.id.userLoginMFA_recover_etRecoveryCode);
        userLoginMFA_recover_ivClearRecoveryCode = (ImageView) view.findViewById(R.id.userLoginMFA_recover_ivClearRecoveryCode);
        userLoginMFA_recover_pbSubmissionInProgress = (ProgressBar) view.findViewById(R.id.userLoginMFA_recover_pbSubmissionInProgress);
        userLoginMFA_recover_tvSubmit = (TextView) view.findViewById(R.id.userLoginMFA_recover_tvSubmit);
        userLoginMFA_recover_tvLoginHelp = (TextView) view.findViewById(R.id.userLoginMFA_recover_tvLoginHelp);
        userLoginMFA_recover_btnSubmit = (RelativeLayout) view.findViewById(R.id.userLoginMFA_recover_btnSubmit);

        userLoginMFA_recover_etRecoveryCode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    userLoginMFA_recover_btnSubmit.setEnabled(isRecoveryCodeFieldPopulated());
                }
            }
        });

        userLoginMFA_recover_etRecoveryCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                userLoginMFA_recover_btnSubmit.setEnabled(s.length() > 0);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //Account recovered
        userLoginMFA_recovered = (RelativeLayout) view.findViewById(R.id.userLoginMFA_recovered);
        userLoginMFA_recovered_tvTitle = (TextView) view.findViewById(R.id.userLoginMFA_recovered_tvTitle);
        userLoginMFA_recovered_tvInfo = (TextView) view.findViewById(R.id.userLoginMFA_recovered_tvInfo);
        userLoginMFA_recovered_tvRecoveryCode = (TextView) view.findViewById(R.id.userLoginMFA_recovered_tvRecoveryCode);
        userLoginMFA_recovered_tvUserConfirmation = (TextView) view.findViewById(R.id.userLoginMFA_recovered_tvUserConfirmation);
        userLoginMFA_recovered_btnContinue = (RelativeLayout) view.findViewById(R.id.userLoginMFA_recovered_btnContinue);
        userLoginMFA_recovered_tvContinue = (TextView) view.findViewById(R.id.userLoginMFA_recovered_tvContinue);
        userLoginMFA_recovered_ivCopy = (ImageView) view.findViewById(R.id.userLoginMFA_recovered_ivCopy);
        userLoginMFA_recovered_ivCheck = (ImageView) view.findViewById(R.id.userLoginMFA_recovered_ivCheck);
        userLoginMFA_recovered_btnCopy = (LinearLayout) view.findViewById(R.id.userLoginMFA_recovered_btnCopy);
        userLoginMFA_recovered_cbUserConfirmation = (Button) view.findViewById(R.id.userLoginMFA_recovered_cbUserConfirmation);

        userLoginMFA_locked = (RelativeLayout) view.findViewById(R.id.userLoginMFA_locked);
        userLoginMFA_locked_tvTitle = (TextView) view.findViewById(R.id.userLoginMFA_locked_tvTitle);
        userLoginMFA_locked_tvInfo = (TextView) view.findViewById(R.id.userLoginMFA_locked_tvInfo);
        userLoginMFA_locked_tvHome = (TextView) view.findViewById(R.id.userLoginMFA_locked_tvHome);

        updateWidgets();

        //show initial layout
        displayLayout(userLoginMFA_home);

    }

    public void updateWidgets(){

        userLoginMFA_etEmail.setText(ObjAppUser.instance().getString(ObjAppUser.FIELD_EMAIL));
        //this will read the user's encrypted password (stored in FIELD_PASSWORD)
        // and populate the EditText with the original password (to avoid re-encryption of already encrypted string)
        userLoginMFA_etPassword.setText(ObjAppUser.instance().getPassword());
        // auto-check remember my details button if required
        userLoginMFA_cbRememberMyDetails.setSelected(ObjAppUser.instance().getBoolean(ObjAppUser.FIELD_REMEMBER_CREDENTIALS));
        //in case we had an auto-population of username password ..
        userLoginMFA_btnLogin.setEnabled(isLoginFieldsPopulated());

    }


    private void setCustomFontTypes(){

        userLoginMFA_tvTitle.setTypeface(AppTypefaces.instance().fontMainBold);
        userLoginMFA_tvError.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_tvEmail.setTypeface(AppTypefaces.instance().fontMainBold);
        userLoginMFA_etEmail.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_tvPassword.setTypeface(AppTypefaces.instance().fontMainBold);
        userLoginMFA_etPassword.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_tvRememberMyDetails.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_tvForgotPassword.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_tvRecoverMyPassword.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_tvLogin.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_tvOr.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_tvRegister.setTypeface(AppTypefaces.instance().fontMainRegular);

        userLoginMFA_tsv_tvTitle.setTypeface(AppTypefaces.instance().fontMainBold);
        userLoginMFA_tsv_tvInfo.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_tsv_tvError.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_tsv_tvOTP.setTypeface(AppTypefaces.instance().fontMainBold);
        userLoginMFA_tsv_tvCountdown.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_tsv_etOTP.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_tsv_tvLogin.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_tsv_tvEnterRevoveryCode.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_tsv_tvLoginHelp.setTypeface(AppTypefaces.instance().fontMainRegular);

        userLoginMFA_recover_tvTitle.setTypeface(AppTypefaces.instance().fontMainBold);
        userLoginMFA_recover_tvInfo.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_recover_tvError.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_recover_tvOTP.setTypeface(AppTypefaces.instance().fontMainBold);
        userLoginMFA_recover_tvCountdown.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_recover_etRecoveryCode.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_recover_tvSubmit.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_recover_tvLoginHelp.setTypeface(AppTypefaces.instance().fontMainRegular);

        userLoginMFA_recovered_tvTitle.setTypeface(AppTypefaces.instance().fontMainBold);
        userLoginMFA_recovered_tvInfo.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_recovered_tvRecoveryCode.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_recovered_tvUserConfirmation.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_recovered_tvContinue.setTypeface(AppTypefaces.instance().fontMainRegular);

        userLoginMFA_locked_tvTitle.setTypeface(AppTypefaces.instance().fontMainBold);
        userLoginMFA_locked_tvInfo.setTypeface(AppTypefaces.instance().fontMainRegular);
        userLoginMFA_locked_tvHome.setTypeface(AppTypefaces.instance().fontMainRegular);


    }



    @Override
    public void onStart() {
        super.onStart();

        setTitleBarText();

    }

    @Override
    public void onResume() {

        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);


    }



    //Check if this is working - this is the new version of deprecated
    //public void onAttach(Activity activity)
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            selfCloseListener = (IAppFragmentSelfCloseListener) context;
        } catch (ClassCastException e) {
            throw new RuntimeException(getActivity().getClass().getName() + " must implement IAppFragmentSelfCloseListener to use this fragment", e);
        }

    }


    public void onClickEvent(int intClickedViewId){

       switch (intClickedViewId) {

            case R.id.userLoginMFA_cbRememberMyDetails:
                toggleRememberDetailsSwitch();
               break;
            case R.id.userLoginMFA_ivClearEmail:
                userLoginMFA_etEmail.setText("");
                break;

           case R.id.userLoginMFA_ivClearPassword:
               userLoginMFA_etPassword.setText("");
               break;

            case R.id.userLoginMFA_btnLogin:
                AppTagManager.instance().sendV2EventTags("linkclick","App - signin","Main","Sign In");
                onEmailLoginTapped();
                break;

            //go to target web forms for non-native user cases
            case R.id.userLoginMFA_btnRegister:
                //Register - this is not a login usecase so reset session variable ..
                AppSession.instance().setInAppLogin(false);
                //open Registration URL
                AppTagManager.instance().sendV2EventTags("linkclick","App - signin","Main","Register");
                goToWebForm(intClickedViewId);
                break;
           case R.id.userLoginMFA_tvForgotPassword:
           case R.id.userLoginMFA_tvRecoverMyPassword:
                //Register - this is not a login usecase so reset session variable ..
                AppSession.instance().setInAppLogin(false);

                //Open forgot password URL
                AppTagManager.instance().sendV2EventTags("linkclick","App - signin","Main","Forgot Password");
                goToWebForm(intClickedViewId);
                break;

                //TSV
           case R.id.userLoginMFA_tsv_btnLogin:
               onMFALoginTapped();
               break;

           case R.id.userLoginMFA_tsv_tvEnterRevoveryCode:
               displayLayout(userLoginMFA_recover);
               break;

           case R.id.userLoginMFA_tsv_tvLoginHelp:
           case R.id.userLoginMFA_recover_tvLoginHelp:
               goToWebForm(intClickedViewId);
               break;

           case R.id.userLoginMFA_recover_btnSubmit:
               onRecoverMyAccountTapped();
               break;

           case R.id.userLoginMFA_recovered_btnCopy:
               copyRecoveryCodeToClipboard();
               break;

           case R.id.userLoginMFA_recovered_cbUserConfirmation:
               toggleRecoveryCodeCopiedUserConfirmationSwitch();
               break;

           case R.id.userLoginMFA_recovered_btnContinue:
               onPostAccountRecoveryContinueTapped();
               break;

           case R.id.userLoginMFA_locked_btnHome:
               if(V3HybridActivity.instance() != null){
                   V3HybridActivity.instance().navigateToHomepage();
                   displayLayout(userLoginMFA_home); //so we show initial screen again when user opens login screen again
               }
               break;
            default:
                break;

        }

    }




    public void onLoginSuccessResponse(ObjApiRequest apiRequest,
                                        JSONObject apiSuccessDetail){

        Logger.printMessage(getLogTag(), "Login successful", Logger.INFO);

        //cancel countdown timer if active
        cancelCountdownTimer();

        resetButtonsAndProgressBars();
    }

    private void resetButtonsAndProgressBars(){

        userLoginMFA_pbLoginInProgress.setVisibility(View.GONE);
        userLoginMFA_tvLogin.setText(getString(R.string.login_tvLogin));
        userLoginMFA_tsv_pbLoginInProgress.setVisibility(View.GONE);
        userLoginMFA_tsv_tvLogin.setText(getString(R.string.userLoginMFA_tsv_tvLogin));
        userLoginMFA_recover_pbSubmissionInProgress.setVisibility(View.GONE);
        userLoginMFA_recover_tvSubmit.setText(getString(R.string.userLoginMFA_recover_tvSubmit));

    }

    public void onLoginErrorResponse(ObjApiRequest request,
                                     int intHttpResponseCode, JSONObject apiErrorDetail){


        resetButtonsAndProgressBars();
        handleLoginError(request);

        Logger.printMessage(getLogTag(), "Response Message: " + request.getResponseMessage(), Logger.INFO);

    }

    private void handleLoginError(ObjApiRequest request){

        String strResponseMessage = request.getResponseMessage(); //request object contains error message from api

        /*test only - imitates an AccountLockedMFA error response
        if(strResponseMessage.equals(AppRequestHandler.ERROR_TEXT_LOGIN_MULTIFACTOR_FAILED)){
            strResponseMessage = AppRequestHandler.ERROR_TEXT_LOGIN_ACCOUNT_LOCKED_MFA;
        }
        */

        if(AppRequestHandler.ERROR_TEXT_LOGIN_INCORRECT.equals(strResponseMessage)) {
            //show error only
            showLoginError(getString(R.string.str_api_error_message_unauthorized_user_feedback));
            return;
        }else if(AppRequestHandler.ERROR_TEXT_LOGIN_MFA_REQUIRED.equals(strResponseMessage)){

            //Extract LoginVerificationId from api response
            JSONObject apiResponse = request.getApiResponse();
            if(apiResponse.has(ObjTFA.FIELD_LOGIN_VERIFICATION_ID)){
                try {
                    String strLoginVerificationId = apiResponse.getString(ObjTFA.FIELD_LOGIN_VERIFICATION_ID);
                    setLoginVerificationId(strLoginVerificationId);
                } catch (JSONException e) {
                    Logger.printMessage(getLogTag(), "Parsing of LoginVerificationId failed", Logger.ERROR);
                    showLoginError(getString(R.string.str_err_login_fallback));
                    return;
                }
            }
            //user needs to enter one time passcode
            displayLayout(userLoginMFA_tsv);
            int intSecondsTillExpiry = 5 * 60; //MFA responses are valid for 5 minutes
            intSecondsTillExpiry = intSecondsTillExpiry  - 1; //taking a second off so we definitely stay within the expiry time
            int intShowWarningWithRemainingSeconds = 2 * 60; //show expiry warning after 2 minutes
            startMFALoginCountdownTimer(Long.valueOf(intSecondsTillExpiry * 1000), Long.valueOf(intShowWarningWithRemainingSeconds * 1000));
            return;

        }else if(AppRequestHandler.ERROR_TEXT_LOGIN_ACCOUNT_LOCKED_GENERAL.equals(strResponseMessage)){
            //cancel countdown timer if active
            cancelCountdownTimer();
            //Account locked (general) for username / password login
            displayLayout(userLoginMFA_home);
            showLoginError(getString(R.string.str_err_account_locked));
            userLoginMFA_tvRecoverMyPassword.setVisibility(View.VISIBLE);
            forceLogOut();
            return;
        }else if(AppRequestHandler.ERROR_TEXT_LOGIN_ACCOUNT_LOCKED_MFA.equals(strResponseMessage)){
            //cancel countdown timer if active
            cancelCountdownTimer();
            //Account locked after too many incorrect MFA attempts
            displayLayout(userLoginMFA_locked);
            forceLogOut();
            return;
        }else if(AppRequestHandler.ERROR_TEXT_LOGIN_MULTIFACTOR_FAILED.equals(strResponseMessage)){
            //this can be the recovery code or the TSV

            //If this is an issue with account recovery ..
            if(request.getInfoObject() != null){
                ObjTFA tfaObject = (ObjTFA) request.getInfoObject();
                if(tfaObject.isAccountRecovery()){
                    displayLayout(userLoginMFA_recover);
                    userLoginMFA_recover_etRecoveryCode.setText(""); //clear recovery code
                    showLoginError(getString(R.string.str_err_recovery_code_failed));
                    return;
                }
            }

            displayLayout(userLoginMFA_tsv);
            userLoginMFA_tsv_etOTP.setText(""); //clear TSV field
            showLoginError(getString(R.string.str_err_multifactor_failed));
            return;
        }else{
            //cancel countdown timer if active
            cancelCountdownTimer();
            //Fallback
            showLoginError(getString(R.string.str_err_login_fallback));
            return;
        }

    }

    private void showLoginError(String strError){

        //hide all errors to start with
        userLoginMFA_tvError.setText("");
        userLoginMFA_lytError.setVisibility(View.GONE);
        userLoginMFA_tsv_tvError.setText("");
        userLoginMFA_tsv_tvError.setVisibility(View.GONE);
        userLoginMFA_recover_tvError.setText("");
        userLoginMFA_recover_tvError.setVisibility(View.GONE);

        if(strError == null || "".equals(strError)){
            return;
        }

        if(userLoginMFA_home.isShown()){
            userLoginMFA_lytError.setVisibility(View.VISIBLE);
            userLoginMFA_tvError.setText(strError);
            userLoginMFA_pbLoginInProgress.setVisibility(View.GONE);
            userLoginMFA_tvLogin.setText(getString(R.string.login_tvLogin));
        }else if(userLoginMFA_tsv.isShown()){
            userLoginMFA_tsv_tvError.setVisibility(View.VISIBLE);
            userLoginMFA_tsv_tvError.setText(strError);
            userLoginMFA_tsv_pbLoginInProgress.setVisibility(View.GONE);
            userLoginMFA_tsv_tvLogin.setText(getString(R.string.userLoginMFA_tsv_tvLogin));
        }else if(userLoginMFA_recover.isShown()){
            userLoginMFA_recover_tvError.setVisibility(View.VISIBLE);
            userLoginMFA_recover_tvError.setText(strError);
            userLoginMFA_recover_pbSubmissionInProgress.setVisibility(View.GONE);
            userLoginMFA_recover_tvSubmit.setText(getString(R.string.userLoginMFA_recover_tvSubmit));
        }

    }


    private void onEmailLoginTapped(){

        //Check if a "reserved" login was used to access hidden app features
        if(isReservedLogin()){
            return;
        }

        ObjAppUser loginUser = isLoginValid();
        if(loginUser==null){
            return;
        }

        //store user immediately
        ObjAppUser.instance().updateAppUser(loginUser);

        submitEmailLogin(loginUser);

    }


    private ObjAppUser isLoginValid(){

        View invalidView = null;
        ObjAppUser loginUser = new ObjAppUser(); //temporary user to pass login information to request

        //save setting of "Remember my credentials" switch
        String strRememberCredentials = AppUtils.getParameterFromWiggleCheckbox(userLoginMFA_cbRememberMyDetails);
        ObjAppUser.instance().setField(ObjAppUser.FIELD_REMEMBER_CREDENTIALS, strRememberCredentials);

        //Password validation
        String strPasswordValidationResult = AppUtils.validatePassword(userLoginMFA_etPassword);
        if(strPasswordValidationResult!=null){
            invalidView = userLoginMFA_etPassword;
            AppUtils.flagWidgetDataStatus(userLoginMFA_etPassword, false, strPasswordValidationResult);
        }else{
            AppUtils.flagWidgetDataStatus(userLoginMFA_etPassword, true, strPasswordValidationResult);

            //We are using a temporary user object for request (loginUser)
            //We are encrypting the password the user typed in
            Crypto crypto = new Crypto(Options.TRANSFORMATION_SYMMETRIC);
            Store store = new Store(ApplicationContextProvider.getContext());
            SecretKey key = store.getSymmetricKey("pwkey", null);
            String strEncryptedPassword = crypto.encrypt(userLoginMFA_etPassword.getText().toString(), key);
            loginUser.setField(ObjAppUser.FIELD_PASSWORD, strEncryptedPassword);

            //only persist password if user wanted to save his credentials (we are saving the encrypted password only)
            if(ObjAppUser.instance().getBoolean(ObjAppUser.FIELD_REMEMBER_CREDENTIALS)){
                ObjAppUser.instance().setField(ObjAppUser.FIELD_PASSWORD, strEncryptedPassword);
            }

        }

        //Email Mandatory
        String strEmailValidationResult = AppUtils.validateEmail(userLoginMFA_etEmail);
        if(strEmailValidationResult!=null){
            invalidView = userLoginMFA_etEmail;
            AppUtils.flagWidgetDataStatus(userLoginMFA_etEmail, false, strEmailValidationResult);
        }else{
            AppUtils.flagWidgetDataStatus(userLoginMFA_etEmail, true, strEmailValidationResult);
            //temporary user object for requst
            loginUser.setField(ObjAppUser.FIELD_EMAIL, userLoginMFA_etEmail.getText().toString());
            //only persist email if user wanted to save his credentials
            if(ObjAppUser.instance().getBoolean(ObjAppUser.FIELD_REMEMBER_CREDENTIALS)) {
                ObjAppUser.instance().setField(ObjAppUser.FIELD_EMAIL, userLoginMFA_etEmail.getText().toString());
            }
        }

        if(invalidView==null){
            return loginUser;
        }else{
            invalidView.requestFocus();
            return null;
        }


    }


    private void submitEmailLogin(ObjAppUser loginUser){

        hideKeyboard();

        //clear error messages
        showLoginError(null);

        //clear any lingering LoginVerificationId from Two-Factor authentication
        setLoginVerificationId(null);

        userLoginMFA_pbLoginInProgress.setVisibility(View.VISIBLE);
        userLoginMFA_tvLogin.setText(getString(R.string.login_btnLogin_inProgress));

        new ObjApiRequest(getActivity(),
                null,
                AppRequestHandler.getAuthenticationEndpoint(),
                Request.Method.POST,
                loginUser, //temporary login user as opposed to application user
                null,
                AppRequestHandler.ON_RESPONSE_ACTION.RETRIEVE_USER_RELATED_DATA
        ).submit();

    }



    private void goToWebForm(int intPressedViewId){

        V3HybridActivity.instance().loadUrlForViewInWebViewFragment(intPressedViewId);

    }

    @Override
    public void onHiddenChanged(boolean hidden){

        if(!hidden){
            setTitleBarText();
        }
    }

    private void setTitleBarText(){

        if(V3HybridActivity.instance()!=null){
            Bundle data = getArguments();

            // Get the extras
            if (data.containsKey(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE)) {

                String strTitle = data.getString(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE);
                V3HybridActivity.instance().setBreadcrumbText(strTitle);

            }

        }


    }

    private boolean isReservedLogin(){

        String strEmail = userLoginMFA_etEmail.getText().toString();

        ObjHoehle h = new ObjHoehle();
        String strLoginToTestServer = h.getValue(ObjHoehle.FIELD_AM);
        String strClearTestServer = h.getValue(ObjHoehle.FIELD_AN);

        if("contentfulpreview".equals(userLoginMFA_etEmail.getText().toString())
                ||
                "classicnavigation".equals(userLoginMFA_etEmail.getText().toString())
                ||
                "tabbednavigation".equals(userLoginMFA_etEmail.getText().toString())
                ||
                "version3navigation".equals(userLoginMFA_etEmail.getText().toString())
                ){

            ObjAppUser.instance().setField(ObjAppUser.FIELD_FORENAME, strEmail);
            ObjAppUser.instance().setField(ObjAppUser.FIELD_EMAIL, strEmail);
            ObjAppUser.instance().setField(ObjAppUser.FIELD_ACCESS_TOKEN, strEmail); //to make user appeared "logged in"
            ObjAppUser.instance().saveToSharedPreferences();

            V3HybridActivity.instance().restartApp();

            return true;

        }else if(strLoginToTestServer.toLowerCase().equals(userLoginMFA_etEmail.getText().toString().toLowerCase())){

            //start dialog to define test server
            showTestServerConfigDialog();
            return true;

        }else if(strClearTestServer.toLowerCase().equals(userLoginMFA_etEmail.getText().toString().toLowerCase())){

            //disconnect from test server - revert to live server
            String strTestServer = ObjAppUser.instance().getString(ObjAppUser.FIELD_TEST_SERVER_URL);
            AppUtils.showCustomToast("Restarting to disconnect from " + strTestServer, false);
            ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_TEST_SERVER_URL, "");
            V3HybridActivity.instance().restartApp();
            return true;

        }

        return false;
    }

    private void showTestServerConfigDialog(){

        ObjHoehle h = new ObjHoehle();
        String strDialogTitle = h.getValue(ObjHoehle.FIELD_AP);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(strDialogTitle);

        // Set up the input
        final EditText input = new EditText(getActivity());
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        String strTestServerExample = h.getValue(ObjHoehle.FIELD_AO);
        input.setText(strTestServerExample);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton(getString(R.string.str_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String strTestServer = input.getText().toString();
                AppUtils.showCustomToast("Restarting to connect to " + strTestServer, false);
                ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_TEST_SERVER_URL, strTestServer);
                V3HybridActivity.instance().restartApp();
            }
        });
        builder.setNegativeButton(getString(R.string.str_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }


    private void displayLayout(View viewToShow) {

        //Default: hide everything
        showLoginError(null);

        //clear focus before switching screens
        userLoginMFA_etEmail.clearFocus();
        userLoginMFA_etPassword.clearFocus();
        userLoginMFA_tsv_etOTP.clearFocus();
        userLoginMFA_recover_etRecoveryCode.clearFocus();

        userLoginMFA_home.setVisibility(View.GONE);
        userLoginMFA_tsv.setVisibility(View.GONE);
        userLoginMFA_recover.setVisibility(View.GONE);
        userLoginMFA_recovered.setVisibility(View.GONE);
        userLoginMFA_locked.setVisibility(View.GONE);

        //scroll to top of scrollview container
        scrollToTop();

        //special handling for some screens
        if(viewToShow == userLoginMFA_home){
            userLoginMFA_btnLogin.setEnabled(isLoginFieldsPopulated());
            userLoginMFA_tvRecoverMyPassword.setVisibility(View.GONE);
            //cancel countdown timer if active
            cancelCountdownTimer();
        }else if(viewToShow == userLoginMFA_tsv){
            userLoginMFA_tsv_etOTP.setText("");
            userLoginMFA_tsv_etOTP.requestFocus();
            userLoginMFA_tsv_btnLogin.setEnabled(isOTPFieldPopulated());
        }else if(viewToShow == userLoginMFA_recover){
            userLoginMFA_recover_etRecoveryCode.setText("");
            userLoginMFA_recover_etRecoveryCode.requestFocus();
            userLoginMFA_recover_btnSubmit.setEnabled(isRecoveryCodeFieldPopulated());
        }else if(viewToShow == userLoginMFA_recovered){
            userLoginMFA_recovered_tvUserConfirmation.setTextColor(getResources().getColor(R.color.text_type_checkbox_label, null));
            userLoginMFA_recovered_tvUserConfirmation.setBackgroundColor(getResources().getColor(R.color.transparent, null));
            userLoginMFA_recovered_tvUserConfirmation.getBackground().setAlpha(255); //fully opaque
            userLoginMFA_recovered_tvRecoveryCode.setText(getNewRecoveryCode());
            userLoginMFA_recovered_btnContinue.setEnabled(false); //to start with (await user confirmation that recovery code was stored)
            displayCopyRecoveryCodeButton();
        }else{
            if(V3HybridActivity.instance() != null) {
                V3HybridActivity.instance().hideVirtualKeyboard();
            }
        }

        viewToShow.setVisibility(View.VISIBLE);

    }

    private void scrollToTop(){

        //scroll to top whenever layouts are changed from logged in to logged out
        svUserLoginMFA.post(new Runnable()
        {
            public void run()
            {
                svUserLoginMFA.fullScroll(ScrollView.FOCUS_UP);
            }
        });
    }

    public boolean canGoBack(boolean blnBackWasPressed){

        if(userLoginMFA_tsv.getVisibility()==View.VISIBLE){
            if(blnBackWasPressed) {
                displayLayout(userLoginMFA_home);
                return true;
            }
        }else if(userLoginMFA_recover.getVisibility()==View.VISIBLE){
            if(blnBackWasPressed) {
                displayLayout(userLoginMFA_tsv);
                return true;
            }
        }else if(userLoginMFA_recovered.getVisibility()==View.VISIBLE){
            if(blnBackWasPressed) {
                //special handling here: disallow back navigation until user checks the
                //"I have stored this code somewhere safe" box
                if(userLoginMFA_recovered_cbUserConfirmation.isSelected() == false){
                    userLoginMFA_recovered_tvUserConfirmation.setTextColor(getResources().getColor(R.color.error_red_solid, null));
                    userLoginMFA_recovered_tvUserConfirmation.setBackgroundColor(getResources().getColor(R.color.error_red, null));
                    userLoginMFA_recovered_tvUserConfirmation.getBackground().setAlpha(33); //13% opacity
                    return true; //doesnt mean we can go back, but stop execution here and handle logic further on this fragment
                }
            }
        }else if(userLoginMFA_locked.getVisibility()==View.VISIBLE){
            if(blnBackWasPressed) {
                //we allow back navigation (no return statement)
                //but before navigating back, we change from the "locked" screen to the "login home" screen
                displayLayout(userLoginMFA_home);
            }
        }

        //cancel countdown timer if active
        cancelCountdownTimer();

        //TODO: on account recovery confirmation page, warn user to copy the code?
        return false;

    }

    private void cancelCountdownTimer(){

        if(mfaCountdownTimer != null){
            mfaCountdownTimer.cancel();
            userLoginMFA_tsv_tvCountdown.setText("");
            userLoginMFA_recover_tvCountdown.setText("");
        }

    }

    /* MFA Login */
    private void onMFALoginTapped(){

        //TSV field mandatory
        String strTSVCode = userLoginMFA_tsv_etOTP.getText().toString().trim();
        if(strTSVCode==null || "".equals(strTSVCode)){
            userLoginMFA_tsv_etOTP.requestFocus();
            AppUtils.flagWidgetDataStatus(userLoginMFA_tsv_etOTP, false, getString(R.string.str_err_please_enter_your_tsv));
            return;
        }else{
            AppUtils.flagWidgetDataStatus(userLoginMFA_tsv_etOTP, true, null);
        }

        submitLoginVerification(strTSVCode);

    }


    private void submitLoginVerification(String strTSVCode){

        hideKeyboard();

        //clear error messages
        showLoginError(null);

        userLoginMFA_tsv_pbLoginInProgress.setVisibility(View.VISIBLE);
        userLoginMFA_tsv_tvLogin.setText(getString(R.string.login_btnLogin_inProgress));

        ObjTFA tfaObject = new ObjTFA();
        tfaObject.setField(ObjTFA.FIELD_LOGIN_VERIFICATION_ID, getLoginVerificationId());
        tfaObject.setField(ObjTFA.FIELD_LOGIN_TOTP, strTSVCode);

        new ObjApiRequest(getActivity(),
                null,
                AppRequestHandler.ENDPOINT_VERIFY_MFA,
                Request.Method.POST,
                tfaObject, //request parameters
                null,
                AppRequestHandler.ON_RESPONSE_ACTION.RETRIEVE_USER_RELATED_DATA
        ).submit();

    }

    private void onRecoverMyAccountTapped(){

        //TSV field mandatory
        String strRecoveryCode = userLoginMFA_recover_etRecoveryCode.getText().toString().trim();
        if(strRecoveryCode==null || "".equals(strRecoveryCode)){
            userLoginMFA_recover_etRecoveryCode.requestFocus();
            AppUtils.flagWidgetDataStatus(userLoginMFA_recover_etRecoveryCode, false, getString(R.string.str_err_please_enter_your_tsv));
            return;
        }else{
            AppUtils.flagWidgetDataStatus(userLoginMFA_recover_etRecoveryCode, true, null);
        }

        submitAccountRecovery(strRecoveryCode);

    }

    private void submitAccountRecovery(String strRecoveryCode){

        hideKeyboard();

        //clear error messages
        showLoginError(null);

        userLoginMFA_recover_pbSubmissionInProgress.setVisibility(View.VISIBLE);
        userLoginMFA_recover_tvSubmit.setText(getString(R.string.login_btnLogin_inProgress));

        ObjTFA tfaObject = new ObjTFA();
        tfaObject.setField(ObjTFA.FIELD_LOGIN_VERIFICATION_ID, getLoginVerificationId());
        tfaObject.setField(ObjTFA.FIELD_LOGIN_RECOVERY_CODE, strRecoveryCode);

        new ObjApiRequest(getActivity(),
                null,
                AppRequestHandler.ENDPOINT_VERIFY_MFA,
                Request.Method.POST,
                tfaObject, //request parameters
                null,
                AppRequestHandler.ON_RESPONSE_ACTION.RETRIEVE_USER_RELATED_DATA
        ).submit();

    }

    public void handleAccountRecovery(ObjApiRequest request){

        //Extract new account recovery code from api response so we can display it
        JSONObject apiResponse = request.getApiResponse();
        if(apiResponse.has(ObjTFA.FIELD_NEW_RECOVERY_CODE)){
            try {
                String strNewRecoveryCode = apiResponse.getString(ObjTFA.FIELD_NEW_RECOVERY_CODE);
                Logger.printMessage(getLogTag(), "NewRecoveryCode: " + strNewRecoveryCode, Logger.INFO);
                setNewRecoveryCode(strNewRecoveryCode);
            } catch (JSONException e) {
                Logger.printMessage(getLogTag(), "Parsing of NewRecoveryCode failed", Logger.ERROR);
                showLoginError(getString(R.string.str_err_login_fallback));
                return;
            }
        }


        displayLayout(userLoginMFA_recovered);

    }

    private void copyRecoveryCodeToClipboard(){

        ClipboardManager clipboard = (ClipboardManager) ApplicationContextProvider.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(getString(R.string.str_your_account_recovery_code), userLoginMFA_recovered_tvRecoveryCode.getText());
        clipboard.setPrimaryClip(clip);

        userLoginMFA_recovered_ivCopy.setVisibility(View.GONE);
        userLoginMFA_recovered_ivCheck.setVisibility(View.VISIBLE);
        userLoginMFA_recovered_btnCopy.setEnabled(false);

        //Show check-button for 2 seconds
        new Handler().postDelayed(new Runnable() {
            public void run() {

                displayCopyRecoveryCodeButton();

            }
        }, 2000);

        AppUtils.showCustomToast(getString(R.string.str_copied_to_clipboard), true);

    }

    private void displayCopyRecoveryCodeButton(){
        userLoginMFA_recovered_btnCopy.setEnabled(true);
        userLoginMFA_recovered_ivCopy.setVisibility(View.VISIBLE);
        userLoginMFA_recovered_ivCheck.setVisibility(View.GONE);
    }

    private void onPostAccountRecoveryContinueTapped(){

        if(V3HybridActivity.instance() != null) {
            V3HybridActivity.instance().executePostMFALoginSuccessActions();
        }

    }

    private void forceLogOut(){

        AppSession.instance().clearUserSpecificData();
        if(V3HybridActivity.instance() != null) {
            V3HybridActivity.instance().showUserAsLoggedInOrOut();
            V3HybridActivity.instance().clearLoginCookieOnWebview(); //clear login cookie
        }

    }

    private void toggleRememberDetailsSwitch(){

        userLoginMFA_cbRememberMyDetails.setSelected(!userLoginMFA_cbRememberMyDetails.isSelected());

        AppTagManager.instance().sendV2EventTags("swipeclick","App - signin","Main","Remember my details");

    }


    private void toggleRecoveryCodeCopiedUserConfirmationSwitch(){

        userLoginMFA_recovered_cbUserConfirmation.setSelected(!userLoginMFA_recovered_cbUserConfirmation.isSelected());

        //enable / disable continue button
        userLoginMFA_recovered_btnContinue.setEnabled(userLoginMFA_recovered_cbUserConfirmation.isSelected());

        //reset error highlight if any
        if(userLoginMFA_recovered_cbUserConfirmation.isSelected()) {
            userLoginMFA_recovered_tvUserConfirmation.setTextColor(getResources().getColor(R.color.text_type_checkbox_label, null));
            userLoginMFA_recovered_tvUserConfirmation.setBackgroundColor(getResources().getColor(R.color.transparent, null));
            userLoginMFA_recovered_tvUserConfirmation.getBackground().setAlpha(255); //fully opaque
        }

    }

    private boolean isLoginFieldsPopulated(){
        return userLoginMFA_etEmail.getText().length() > 0 && userLoginMFA_etPassword.getText().length() > 0;
    }

    private boolean isOTPFieldPopulated(){
        return userLoginMFA_tsv_etOTP.getText().length() > 0;
    }

    private boolean isRecoveryCodeFieldPopulated(){
        return userLoginMFA_recover_etRecoveryCode.getText().length() > 0;
    }

    private void hideKeyboard(){
        if (V3HybridActivity.instance() != null){
            V3HybridActivity.instance().hideVirtualKeyboard();
        }
    }

    private void startMFALoginCountdownTimer(long lngTotalTime, long lngDisplayWarningAfterMs){

        //cancel any existing timers
        cancelCountdownTimer();

        //A manged branding screen
        mfaCountdownTimer = new CountDownTimer(lngTotalTime, 1000) {

            public void onTick(long millisUntilFinished) {

                if(millisUntilFinished <= lngDisplayWarningAfterMs + 1000){ //countdown timers arent perfectly accurat. Add 1sec so we start at the expected time

                    String strMinutesAndSeconds = DateUtils.getMinutesAndSecondsFromTimestamp(millisUntilFinished);
                    String strCountdownLabel = String.format(getString(R.string.strSession_timeout_in),strMinutesAndSeconds);
                    if(userLoginMFA_tsv_tvCountdown != null) {
                        userLoginMFA_tsv_tvCountdown.setText(strCountdownLabel);
                    }

                    if(userLoginMFA_recover_tvCountdown!=null) {
                        userLoginMFA_recover_tvCountdown.setText(strCountdownLabel);
                    }
                }

            }

            public void onFinish() {
                if(userLoginMFA_tsv_tvCountdown != null) {
                    userLoginMFA_tsv_tvCountdown.setText("");
                }

                if(userLoginMFA_recover_tvCountdown != null) {
                    userLoginMFA_recover_tvCountdown.setText("");
                }

                if(userLoginMFA_home != null) {
                    //cancel current MFA auth action (OTP or recovery code) - go back to home screen
                    displayLayout(userLoginMFA_home);
                }
            }

        }.start();

    }

}


