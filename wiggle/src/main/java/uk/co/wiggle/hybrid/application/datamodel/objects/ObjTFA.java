package uk.co.wiggle.hybrid.application.datamodel.objects;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Chain Reaction Cycles / Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Chain Reaction Cycles / Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class ObjTFA extends CustomObject {

	public static final String OBJECT_NAME = "object_tfa";

	public final static String FIELD_LOGIN_VERIFICATION_ID = "LoginVerificationId";
	public final static String FIELD_LOGIN_RECOVERY_CODE = "RecoveryCode";
	public final static String FIELD_LOGIN_TOTP = "TOTP";
	public final static String FIELD_NEW_RECOVERY_CODE = "NewRecoveryCode";

	public boolean isAccountRecovery(){
		String recoveryCode = getString(FIELD_LOGIN_RECOVERY_CODE);
		return (recoveryCode != null && !"".equals(recoveryCode));
	}

	private static List<String> sFieldList;
	static {

		sFieldList = Collections.synchronizedList(new ArrayList<String>());

		sFieldList.add(FIELD_LOGIN_VERIFICATION_ID);
		sFieldList.add(FIELD_LOGIN_RECOVERY_CODE);
		sFieldList.add(FIELD_LOGIN_TOTP);
		sFieldList.add(FIELD_NEW_RECOVERY_CODE);

	}

	private static ConcurrentHashMap<String, Integer> sFieldTypes;
	static {
		sFieldTypes = new ConcurrentHashMap<String, Integer>();

		sFieldTypes.put(FIELD_LOGIN_VERIFICATION_ID, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_LOGIN_RECOVERY_CODE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_LOGIN_TOTP, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_NEW_RECOVERY_CODE, FIELD_TYPE_STRING);

	}

	public ObjTFA() {
	}

	public ObjTFA(Bundle bundle) {
		super(bundle);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ObjTFA)) {
			return false;
		}

		ObjTFA other = (ObjTFA) o;
		String otherURI = other.getString(FIELD_LOGIN_VERIFICATION_ID);
		String thisURI = this.getString(FIELD_LOGIN_VERIFICATION_ID);
		return otherURI.equals(thisURI);
	}

	@Override
	public String getObjectName() {
		return OBJECT_NAME;
	}

	@Override
	public List<String> getFields() {
		return sFieldList;
	}

	@Override
	public Class<?> getSubclass() {
		return ObjTFA.class;
	}

	@Override
	public Integer getFieldType(String fieldName) {
		return sFieldTypes.get(fieldName);
	}


}