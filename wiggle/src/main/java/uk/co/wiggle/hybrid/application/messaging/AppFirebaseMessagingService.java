package uk.co.wiggle.hybrid.application.messaging;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.app.NotificationCompat;
import android.text.TextUtils;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.salesforce.marketingcloud.MarketingCloudSdk;
import com.salesforce.marketingcloud.messages.push.PushMessageManager;

import java.util.Map;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.usecase.startup.activities.StartupActivity;
import uk.co.wiggle.hybrid.usecase.startup.activities.WelcomeActivity;

/**
 *
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 *
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 *
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 * @content  Used for Firebase messaging through SalesForce Marketing Cloud
 **/
public class AppFirebaseMessagingService extends FirebaseMessagingService
{

    private static String logTag = "AppFirebaseMessagingService";

    @Override
    public void onNewToken(String s) {

        Logger.printMessage(logTag, "onNewToken: " + s, Logger.INFO);
        String refreshedToken = s;

        //Regsiter this token with SalesForce marketing cloud
        MarketingCloudSdk.requestSdk(new MarketingCloudSdk.WhenReadyListener() {
            @Override
            public void ready(MarketingCloudSdk marketingCloudSdk) {
                marketingCloudSdk.getPushMessageManager().setPushToken(refreshedToken);
                Logger.printMessage(logTag, "Set push token " + refreshedToken, Logger.INFO);
            }
        });

    }

    /**
     * PushMessageManager.isMarketingCloudPush(data) will return true if the message is found to be
     * originating from SalesForce Marketing Cloud. If this returns false, you should handle the message yourself.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> data = remoteMessage.getData();

        Logger.printMessage(logTag, "Push received" + data.toString(), Logger.INFO);

        //Check if this is a push message that originated from SalesForce Marketing Cloud
        if(PushMessageManager.isMarketingCloudPush(data)) {
            //... if yes, let SalesForce SDK handle it
            MarketingCloudSdk.requestSdk(new MarketingCloudSdk.WhenReadyListener() {
                @Override
                public void ready(MarketingCloudSdk marketingCloudSdk) {
                    Logger.printMessage(logTag, "SF to handle message", Logger.INFO);

                    marketingCloudSdk.getPushMessageManager().handleMessage(data);
                }
            });
        }else {
            //... if no, manually handle the push message
            showNotification(data.get("message"));
        }

    }

    private void showNotification(String message) {
        if (!TextUtils.isEmpty(message)) {
            Intent intent = new Intent(this, WelcomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "SOME_CHANNEL_ID")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("FCM Message")
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0, notificationBuilder.build());
        }
    }

}