package uk.co.wiggle.hybrid.application.datamodel.objects;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.utils.DateUtils;


import android.content.Context;
import android.os.Bundle;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class ObjSalesOrder extends CustomObject {

	public static final String OBJECT_NAME = "ObjSalesOrder";
	
	private List<ObjSalesItem> mSalesItems = new ArrayList<ObjSalesItem>();

	//fields used for categories taken from top_level_navigation.json
	public static final String FIELD_ORDER_ID = "OrderId";
	public static final String FIELD_ORDER_DATE = "OrderDate";
	public static final String FIELD_ORDER_DATE_FORMATTED = "OrderDateFormatted";
	public static final String FIELD_DISPATCH_DATE = "DispatchDate";
	public static final String FIELD_DISPATCH_DATE_FORMATED = "DispatchDateFormatted";
	public static final String FIELD_PAYMENT_METHOD = "PaymentMethod";
	public static final String FIELD_STATUS_HEADLINE = "StatusHeadline";
	public static final String FIELD_STATUS_DETAILS = "StatusDetails";
	public static final String FIELD_STATUS_CODE = "StatusCode";
	public static final String FIELD_ESTIMATED_DELIVERY = "EstimatedDelivery";
	public static final String FIELD_DELIVERY_ADDRESS = "DeliveryAddress";
	public static final String FIELD_CANCEL_ORDER_URL = "CancelOrderUrl";
	public static final String FIELD_DETAILS_URL = "DetailsUrl";
	public static final String FIELD_TRACKING_URL = "TrackingUrl";
	public static final String FIELD_TOTAL_FORMATTED = "TotalFormatted";
	public static final String FIELD_IS_AWAITING_PROCESSING = "IsAwaitingProcessing";
	public static final String FIELD_CONTAINS_BIKE = "ContainsBike";
	public static final String FIELD_DELIVERY_METHOD = "DeliveryMethod";
	public static final String FIELD_IS_TRACKED = "IsTracked";
	public static final String FIELD_TRACKING_CODE = "TrackingCode";
	public static final String FIELD_DELIVERY_DATE = "DeliveryDate";
	public static final String JSON_LIST_ITEMS = "Items";
	public static final String FIELD_ORDER_NEW_INFORMATION = "order_new_information"; //internal field to track when we get new info on an order
	public static final String FIELD_ORDER_JUST_DELIVERED = "just_delivered"; //internal field to track if an order changed its status to "StatusDeliveredText"
	
	private static List<String> sFieldList;
	static {

		sFieldList = Collections.synchronizedList(new ArrayList<String>());
		
		sFieldList.add(FIELD_ID);
		sFieldList.add(FIELD_ORDER_ID);
		sFieldList.add(FIELD_ORDER_DATE);
		sFieldList.add(FIELD_ORDER_DATE_FORMATTED);
		sFieldList.add(FIELD_DISPATCH_DATE);
		sFieldList.add(FIELD_DISPATCH_DATE_FORMATED);
		sFieldList.add(FIELD_PAYMENT_METHOD);
		sFieldList.add(FIELD_STATUS_HEADLINE);
		sFieldList.add(FIELD_STATUS_DETAILS);
		sFieldList.add(FIELD_STATUS_CODE);
		sFieldList.add(FIELD_ESTIMATED_DELIVERY);
		sFieldList.add(FIELD_DELIVERY_ADDRESS);
		sFieldList.add(FIELD_CANCEL_ORDER_URL);
		sFieldList.add(FIELD_DETAILS_URL);
		sFieldList.add(FIELD_TRACKING_URL);
		sFieldList.add(FIELD_TOTAL_FORMATTED);
		sFieldList.add(FIELD_IS_AWAITING_PROCESSING);
		sFieldList.add(FIELD_CONTAINS_BIKE);
		sFieldList.add(FIELD_DELIVERY_METHOD);
		sFieldList.add(FIELD_IS_TRACKED);
		sFieldList.add(FIELD_TRACKING_CODE);
		sFieldList.add(FIELD_DELIVERY_DATE);
		sFieldList.add(FIELD_ORDER_NEW_INFORMATION);
		sFieldList.add(FIELD_ORDER_JUST_DELIVERED);
		
		
	}

	private static ConcurrentHashMap<String, Integer> sFieldTypes;
	static {
		sFieldTypes = new ConcurrentHashMap<String, Integer>();
		
		sFieldTypes.put(FIELD_ID, FIELD_TYPE_INTEGER);
		sFieldTypes.put(FIELD_ORDER_ID, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_ORDER_DATE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_ORDER_DATE_FORMATTED, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_DISPATCH_DATE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_DISPATCH_DATE_FORMATED, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_PAYMENT_METHOD, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_STATUS_HEADLINE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_STATUS_DETAILS, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_STATUS_CODE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_ESTIMATED_DELIVERY, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_DELIVERY_ADDRESS, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_CANCEL_ORDER_URL, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_DETAILS_URL, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_TRACKING_URL, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_TOTAL_FORMATTED, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_IS_AWAITING_PROCESSING, FIELD_TYPE_BOOLEAN);
		sFieldTypes.put(FIELD_CONTAINS_BIKE, FIELD_TYPE_BOOLEAN);
		sFieldTypes.put(FIELD_DELIVERY_METHOD, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_IS_TRACKED, FIELD_TYPE_BOOLEAN);
		sFieldTypes.put(FIELD_TRACKING_CODE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_DELIVERY_DATE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_ORDER_NEW_INFORMATION, FIELD_TYPE_BOOLEAN);
		sFieldTypes.put(FIELD_ORDER_JUST_DELIVERED, FIELD_TYPE_BOOLEAN);
		
	}

	public ObjSalesOrder() {
	}

	public ObjSalesOrder(Bundle bundle) {
		super(bundle);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ObjSalesOrder)) {
			return false;
		}

		ObjSalesOrder other = (ObjSalesOrder) o;
		String otherId = other.getString(ObjSalesOrder.FIELD_ORDER_ID);
		String thisId = this.getString(ObjSalesOrder.FIELD_ORDER_ID);
		return otherId.equals(thisId);
	}

	@Override
	public String getObjectName() {
		return OBJECT_NAME;
	}

	@Override
	public List<String> getFields() {
		return sFieldList;
	}

	@Override
	public Class<?> getSubclass() {
		return ObjSalesOrder.class;
	}

	@Override
	public Integer getFieldType(String fieldName) {
		return sFieldTypes.get(fieldName);
	}

	
	// handler for children of this object
	public List<ObjSalesItem> getSalesItems() {
		return mSalesItems;
	}
	
	public boolean setSalesItems(Collection<? extends ObjSalesItem> arg0) {
		mSalesItems.clear();
		return mSalesItems.addAll(arg0);
	}
	
	
	public Date getDateFromJson(String strFieldName){
		
		
		String strJsonDate = getString(strFieldName);
		
		//Json date format from Wiggle API: /Date(1468839462913)/
		strJsonDate = strJsonDate.replace("/Date(", "");
		strJsonDate = strJsonDate.replace(")/", "");
		
		//extract Long value
		Long lngMillisecs = Long.valueOf(strJsonDate);
		Date dateFromLong = new Date(lngMillisecs);
		
		return dateFromLong;
		
	}
	
	public String getDisplayOrderStatus(){
		
		Context context = ApplicationContextProvider.getContext();
		String strApiOrderStatusCode = getString(FIELD_STATUS_CODE);
		
		ArrayList<String> statusOrderPlaced = new ArrayList<String>(
				Arrays.asList(
				 "StatusAwaitingGoodsText",
                 "WaitingForFinancePaperwork",
                 "WaitingForFinanceApproval",
                 "WaitingForPayment",
                 "AwaitingPaymentResult",
                 "StatusDeclinedText"
                 ));
		
		if(statusOrderPlaced.contains(strApiOrderStatusCode)){
			return context.getString(R.string.str_order_status_ordered);
		}
		
		if("StatusNotProcessedText".equals(strApiOrderStatusCode)){
			return context.getString(R.string.str_order_status_processing);
		}else if("StatusProcessedText".equals(strApiOrderStatusCode)){
			return context.getString(R.string.str_order_status_processed);
		}if("StatusDispatchedText".equals(strApiOrderStatusCode)){
			return context.getString(R.string.str_order_status_dispatched);
		}if("StatusDeliveredText".equals(strApiOrderStatusCode)){
			return context.getString(R.string.str_order_status_delivered);
		}if("StatusCancelledText".equals(strApiOrderStatusCode)){
			return context.getString(R.string.str_order_status_cancelled);
		}else{
			return context.getString(R.string.str_order_status_unknown);
		}
		
	}
	
	public String getDisplayDeliveryStatus(){
		
		Context context = ApplicationContextProvider.getContext();
		Date now = new Date();
		
		String strDeliveryDate = getString(FIELD_DELIVERY_DATE);
		String strDispatchDate = getString(FIELD_DISPATCH_DATE);
		
		if(!"".equals(strDeliveryDate) && strDeliveryDate!=null){

			Date deliveryDate = getDateFromJson(FIELD_DELIVERY_DATE);
			if(deliveryDate.before(now)){
				return context.getString(R.string.str_delivered_on);
			}else{
				return context.getString(R.string.str_estimated_delivery_on);
			}
			
		}else if(!"".equals(strDispatchDate) && strDispatchDate!=null){
			
			Date dispatchDate = getDateFromJson(FIELD_DISPATCH_DATE);
			if(dispatchDate.before(now)){
				return context.getString(R.string.str_dispatched_on);
			}else{
				return context.getString(R.string.str_estimated_dispatch_on);
			}
			
			
		}else{
			
			return context.getString(R.string.str_ordered_on);
		
		}
				
	}
	
	public String getDisplayDeliveryDate(){
		
		Date deliveryDate = new Date();		
		
		String strDeliveryDate = getString(FIELD_DELIVERY_DATE);
		String strDispatchDate = getString(FIELD_DISPATCH_DATE);
		
		if(!"".equals(strDeliveryDate) && strDeliveryDate!=null){
		
			deliveryDate = getDateFromJson(FIELD_DELIVERY_DATE);
			
			
		}else if(!"".equals(strDispatchDate) && strDispatchDate!=null){
			
			deliveryDate = getDateFromJson(FIELD_DISPATCH_DATE);
			
		}else{
			
			deliveryDate = getDateFromJson(FIELD_ORDER_DATE);
		
		}
		
		String strDisplayDate = DateUtils.getOrderTrackingDate(deliveryDate);
		return strDisplayDate;
		
	}


	public String getDisplayOrderDate(){

		String strOrderDate = getString(FIELD_ORDER_DATE);
		Date orderDate = getDateFromJson(FIELD_ORDER_DATE);

		String strDisplayDate = DateUtils.getOrderTrackingDate(orderDate);
		return strDisplayDate;

	}

	public String getFormattedAddress() {
		
		//TODO: get address formatted ... 
		String strFormattedAddress = getString(FIELD_DELIVERY_ADDRESS);
		return strFormattedAddress;
		
	}

	public boolean hasNewInformation() {
		
		return getBoolean(FIELD_ORDER_NEW_INFORMATION);
		
	}
	
	public boolean compareOrderWith(ObjSalesOrder otherOrder){
		
		ObjSalesOrder thisOrder = this;
		
		if(!thisOrder.getString(FIELD_DISPATCH_DATE)
				.equals(otherOrder.getString(FIELD_DISPATCH_DATE))){
			return true;
		}
		
		if(!thisOrder.getString(FIELD_STATUS_CODE)
				.equals(otherOrder.getString(FIELD_STATUS_CODE))){
			return true;
		}
		
		if(!thisOrder.getString(FIELD_ESTIMATED_DELIVERY)
				.equals(otherOrder.getString(FIELD_ESTIMATED_DELIVERY))){
			return true;
		}
		
		if(!thisOrder.getString(FIELD_DELIVERY_DATE)
				.equals(otherOrder.getString(FIELD_DELIVERY_DATE))){
			return true;
		}	
			
		//no changes? return false
		return false;
		
	}

	public int getOrderStatusColour(){

		Context context = ApplicationContextProvider.getContext();
		String strOrderStatus = getDisplayOrderStatus();
		//default colour
		int intColor = context.getResources().getColor(R.color.order_status_colour_default);

		if(context.getString(R.string.str_order_status_ordered).equals(strOrderStatus)){
			intColor = context.getResources().getColor(R.color.order_status_colour_in_progress);
		}else if(context.getString(R.string.str_order_status_processing).equals(strOrderStatus)){
			intColor = context.getResources().getColor(R.color.order_status_colour_in_progress);
		}else if(context.getString(R.string.str_order_status_processed).equals(strOrderStatus)){
			intColor = context.getResources().getColor(R.color.order_status_colour_in_progress);
		}else if(context.getString(R.string.str_order_status_dispatched).equals(strOrderStatus)){
			intColor = context.getResources().getColor(R.color.order_status_colour_in_progress);
		}else if(context.getString(R.string.str_order_status_delivered).equals(strOrderStatus)){
			intColor = context.getResources().getColor(R.color.order_status_colour_done);
		}else if(context.getString(R.string.str_order_status_cancelled).equals(strOrderStatus)){
			intColor = context.getResources().getColor(R.color.order_status_colour_cancelled);
		}else if(context.getString(R.string.str_order_status_unknown).equals(strOrderStatus)){
			intColor = context.getResources().getColor(R.color.order_status_colour_default);
		}

		return intColor;

	}

	public class StepStatus {

		String mStatus;
		int mStatusColor;

		public StepStatus(String status, int intStatusColor){
			this.mStatus = status;
			this.mStatusColor = intStatusColor;
		}

		public String getStatusValue(){
			return mStatus;
		}

		public void setStatusValue(String status){
			this.mStatus = status;
		}

		public int getStatusColor(){
			return this.mStatusColor;
		}

		public void setStatusColor(int intStatusColor){
			this.mStatusColor = intStatusColor;
		}

	}

	public StepStatus getPackingStatus(){

		Context context = ApplicationContextProvider.getContext();
		String strOrderStatus = getDisplayOrderStatus();

		if(context.getString(R.string.str_order_status_ordered).equals(strOrderStatus)){
			return new StepStatus(
					context.getString(R.string.str_packing_status_processing),
					context.getResources().getColor(R.color.order_status_colour_in_progress)
					);
		}else if(context.getString(R.string.str_order_status_processing).equals(strOrderStatus)){
			return new StepStatus(
					context.getString(R.string.str_packing_status_processing),
					context.getResources().getColor(R.color.order_status_colour_in_progress)
			);
		}else if(context.getString(R.string.str_order_status_processed).equals(strOrderStatus)){
			return new StepStatus(
					context.getString(R.string.str_packing_status_processed),
					context.getResources().getColor(R.color.order_status_colour_done)
			);
		}else if(context.getString(R.string.str_order_status_dispatched).equals(strOrderStatus)){
			return new StepStatus(
					context.getString(R.string.str_packing_status_processed),
					context.getResources().getColor(R.color.order_status_colour_done)
			);
		}else if(context.getString(R.string.str_order_status_delivered).equals(strOrderStatus)){
			return new StepStatus(
					context.getString(R.string.str_packing_status_processed),
					context.getResources().getColor(R.color.order_status_colour_done)
			);
		}else if(context.getString(R.string.str_order_status_cancelled).equals(strOrderStatus)){
			return new StepStatus(
					context.getString(R.string.str_packing_status_cancelled),
					context.getResources().getColor(R.color.order_status_colour_cancelled)
			);
		}

		//fallback
		return new StepStatus(
				context.getString(R.string.str_packing_status_unknown),
				context.getResources().getColor(R.color.order_status_colour_open)
		);


	}

	public StepStatus getPaymentStatus(){

		Context context = ApplicationContextProvider.getContext();

		String strPaymentMethod = getString(FIELD_PAYMENT_METHOD);
		if(strPaymentMethod!=null && !"".equals(strPaymentMethod)){
			return new StepStatus(
					strPaymentMethod,
					context.getResources().getColor(R.color.order_status_colour_done)
			);
		}

		//fallback
		return new StepStatus(
				context.getString(R.string.str_payment_status_unknown),
				context.getResources().getColor(R.color.order_status_colour_open)
		);


	}

	public StepStatus getShippingStatus(){

		Context context = ApplicationContextProvider.getContext();
		String strOrderStatus = getDisplayOrderStatus();

		//default
		StepStatus stepStatus = new StepStatus(
				context.getString(R.string.str_shipping_status_unknown),
				context.getResources().getColor(R.color.order_status_colour_open)
		);

		if(context.getString(R.string.str_order_status_ordered).equals(strOrderStatus)){
			stepStatus = new StepStatus(
					context.getString(R.string.str_shipping_status_open),
					context.getResources().getColor(R.color.order_status_colour_open)
			);
		}else if(context.getString(R.string.str_order_status_processing).equals(strOrderStatus)){
			stepStatus = new StepStatus(
					context.getString(R.string.str_shipping_status_open),
					context.getResources().getColor(R.color.order_status_colour_open)
			);
		}else if(context.getString(R.string.str_order_status_processed).equals(strOrderStatus)){
			stepStatus = new StepStatus(
					context.getString(R.string.str_shipping_status_processing),
					context.getResources().getColor(R.color.order_status_colour_in_progress)
			);
		}else if(context.getString(R.string.str_order_status_dispatched).equals(strOrderStatus)){
			stepStatus = new StepStatus(
					context.getString(R.string.str_shipping_status_processed),
					context.getResources().getColor(R.color.order_status_colour_done)
			);
		}else if(context.getString(R.string.str_order_status_delivered).equals(strOrderStatus)){
			stepStatus = new StepStatus(
					context.getString(R.string.str_shipping_status_processed),
					context.getResources().getColor(R.color.order_status_colour_done)
			);
		}else if(context.getString(R.string.str_order_status_cancelled).equals(strOrderStatus)){
			stepStatus = new StepStatus(
					context.getString(R.string.str_shipping_status_cancelled),
					context.getResources().getColor(R.color.order_status_colour_cancelled)
			);
		}

		// if we have a dispatch date - set status text accordingly - whilst leaving status colour unchanged
		String strDipatchDate = getString(FIELD_DISPATCH_DATE);
		if(!"".equals(strDipatchDate) && strDipatchDate!=null) {

			Date dispatchDate = getDateFromJson(FIELD_DISPATCH_DATE);
			Date now = new Date();

			if(dispatchDate.before(now)){
				stepStatus.setStatusValue(context.getString(R.string.str_dispatched_on) + " " + getDisplayDeliveryDate());
			}else{
				stepStatus.setStatusValue(context.getString(R.string.str_estimated_dispatch_on) + " " + getDisplayDeliveryDate());
			}
		}


		return stepStatus;

	}


	public StepStatus getDeliveryStatus(){

		Context context = ApplicationContextProvider.getContext();
		String strOrderStatus = getDisplayOrderStatus();

		//default
		StepStatus stepStatus = new StepStatus(
				context.getString(R.string.str_delivery_status_unknown),
				context.getResources().getColor(R.color.order_status_colour_open)
		);

		if(context.getString(R.string.str_order_status_ordered).equals(strOrderStatus)){
			stepStatus = new StepStatus(
					context.getString(R.string.str_delivery_status_open),
					context.getResources().getColor(R.color.order_status_colour_open)
			);
		}else if(context.getString(R.string.str_order_status_processing).equals(strOrderStatus)){
			stepStatus = new StepStatus(
					context.getString(R.string.str_delivery_status_open),
					context.getResources().getColor(R.color.order_status_colour_open)
			);
		}else if(context.getString(R.string.str_order_status_processed).equals(strOrderStatus)){
			stepStatus = new StepStatus(
					context.getString(R.string.str_delivery_status_open),
					context.getResources().getColor(R.color.order_status_colour_open)
			);
		}else if(context.getString(R.string.str_order_status_dispatched).equals(strOrderStatus)){
			stepStatus = new StepStatus(
					context.getString(R.string.str_delivery_status_processing),
					context.getResources().getColor(R.color.order_status_colour_in_progress)
			);
		}else if(context.getString(R.string.str_order_status_delivered).equals(strOrderStatus)){
			stepStatus = new StepStatus(
					context.getString(R.string.str_delivery_status_processed),
					context.getResources().getColor(R.color.order_status_colour_done)
			);
		}else if(context.getString(R.string.str_order_status_cancelled).equals(strOrderStatus)){
			stepStatus = new StepStatus(
					context.getString(R.string.str_delivery_status_cancelled),
					context.getResources().getColor(R.color.order_status_colour_cancelled)
			);
		}

		// if we have a delivery date - set status text accordingly - whilst leaving status colour unchanged
		String strDeliveryDate = getString(FIELD_DELIVERY_DATE);
		if(!"".equals(strDeliveryDate) && strDeliveryDate!=null) {

			Date deliveryDate = getDateFromJson(FIELD_DELIVERY_DATE);
			Date now = new Date();

			if(deliveryDate.before(now)){
				stepStatus.setStatusValue(context.getString(R.string.str_delivered_on) + " " + getDisplayDeliveryDate());
			}else{
				stepStatus.setStatusValue(context.getString(R.string.str_estimated_delivery_on) + " " + getDisplayDeliveryDate());
			}
		}

		return stepStatus;

	}



}