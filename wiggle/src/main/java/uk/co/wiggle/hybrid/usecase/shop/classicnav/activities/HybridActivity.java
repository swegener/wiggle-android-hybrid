package uk.co.wiggle.hybrid.usecase.shop.classicnav.activities;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.android.volley.Request.Method;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjNavigationCategoryManager;
import uk.co.wiggle.hybrid.application.helpers.AppGlobalConstants;
import uk.co.wiggle.hybrid.usecase.startup.activities.StartupActivity;
import uk.co.wiggle.hybrid.usecase.startup.activities.WelcomeActivity;
import uk.co.wiggle.hybrid.usecase.ordertracking.adapters.OrderTrackingListAdapter;
import uk.co.wiggle.hybrid.usecase.shop.classicnav.adapters.TextSearchListAdapter;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler.ON_RESPONSE_ACTION;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjSalesOrderManager;
import uk.co.wiggle.hybrid.application.datamodel.objects.CustomObject;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjApiRequest;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNavigationCategory;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesItem;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesOrder;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjTypeaheadResponse;
import uk.co.wiggle.hybrid.application.session.AppSession;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.application.persistence.DataStore;
import uk.co.wiggle.hybrid.application.utils.DateUtils;
import uk.co.wiggle.hybrid.application.logging.Logger;
import uk.co.wiggle.hybrid.usecase.ordertracking.helpers.OrderTrackingUtils;
import uk.co.wiggle.hybrid.extensions.PicassoSSL;
import uk.co.wiggle.hybrid.extensions.TypefaceSpan;
import uk.co.wiggle.hybrid.application.fragments.Dialog;
import uk.co.wiggle.hybrid.application.fragments.Dialog.DialogActionListener;
import uk.co.wiggle.hybrid.usecase.shop.classicnav.fragments.NavigationDrawerFragment;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.interfaces.IAppActivityUI;
import uk.co.wiggle.hybrid.interfaces.IAppApiResponse;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentSelfCloseListener;
import uk.co.wiggle.hybrid.usecase.newsfeed.activities.NewsfeedActivity;
import uk.co.wiggle.hybrid.tagging.AppTagManager;
import android.annotation.SuppressLint;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Browser;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/


@SuppressLint("NewApi") public class HybridActivity extends FragmentActivity
        implements IAppActivityUI, IAppApiResponse, View.OnClickListener, DialogActionListener, IAppFragmentSelfCloseListener {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    
    //allow other activities to read an instance of MainActivity so that MainActivity methods can be called
	private static HybridActivity myInstance;

	public static HybridActivity instance(){
		return myInstance;
	}
	
	
	private static final String logTag = "HybridActivity";

	public static String getLogTag(){
		return logTag;
	}
	
	/* Web View Elements */
	private WebView mWebView;	
	private TextView tvErrorMessage;
	private RelativeLayout lytPleaseWait;
	private ImageView ivPleaseWait;
	public static String WEB_VIEW_CALLER = "WEB_VIEW_CALLER";
	public static String LOAD_TARGET_URL_FROM_NATIVE_SCREEN = "LOAD_TARGET_URL_FROM_NATIVE_SCREEN";
	public static String LOAD_TARGET_URL_FROM_STARTUP_MENU = "LOAD_TARGET_URL_FROM_STARTUP_MENU";
	public static String BREADCRUMB_TEXT = "BREADCRUMB_TEXT";
	
	ObjNavigationCategory currentNavigationCategory;
	
	//Widgets
	TextView navigation_tvHello;
	ProgressBar navigation_pbLoginInProgress;
	TextView navigation_tvLogin;
	TextView navigation_tvRegister;
	LinearLayout navigation_lytAccountActions;
	ImageView navigation_ivCountry;
	TextView actionBar_tvCartCounter;
	RelativeLayout lytWebView;
	ProgressBar main_pbLoadProgress;
	TextView main_tvBreadcrumb;
	LinearLayout lytNativeSearch;
	EditText nativeSearch_etQueryString;
	ImageView nativeSearch_ivResortTextSearchClear;
	ListView nativeSearch_lvSearchResult;
	TextView textSearch_tvGroupHeader;
	LinearLayout nativeSearch_dynamicHeader;
	TextView hybridActivity_tvCurrentLink;
	
	//Order tracking layouts and objects
	RelativeLayout lytOrderTracking;
	RelativeLayout orderTracking_orderList;
	SwipeRefreshLayout orderList_swipeRefreshLayout;
	AbsListView orderList_lvOrderHistory;
	TextView orderList_tvNoData;
	LinearLayout orderList_lytGroupHeader;
	TextView orderList_tvGroupHeader;
	ScrollView lytOrderDetail;
	TextView orderList_tvTitle;
	OrderTrackingListAdapter orderTrackingAdapter;
	private OrderTrackingTask mOrderTrackingTask = new OrderTrackingTask();
	int mWebViewCaller4AndroidNativeScreen = 0;
	
	//Order Detail
	TextView orderDetail_tvTitle;
	TextView orderDetail_tvDeliveryStatusText;
	TextView orderDetail_tvDeliveryStatusDate;
	ImageView orderDetail_ivProductImage;
	TextView orderDetail_tvProductBrandAndName;
	TextView orderDetail_tvProductVariety;
	TextView orderDetail_tvProductPrice;
	TextView orderDetail_lblDeliveryAddress;
	TextView orderDetail_tvDeliveryAddress;
	TextView orderDetail_lblOrderNumber;
	TextView orderDetail_tvOrderNumber;
	TextView orderDetail_tvMoreDetails;
	LinearLayout lytOrderTrackingGraph;

	//Notification alert elements in action bar menu
	MenuItem notificationAlertItem;
	RelativeLayout menu_notificationAlert;
	TextView menu_notificationAlert_tvAlert;
	TextView menu_notificationAlert_tvAlertCounter;
	PopupWindow alertPopup;
	View popupLayout;
	TextView menuAlertPopupHint_tvHint;
	private String mPopupNotificationText = "";
	private static final String SUPPRESS_POPUP = "SUPPRESS_POPUP";


	//used for "Native Android Screens" - if this object is populated it means a "native android screen" is displayed or part of the 
	//navigation process. This affects e.g. back navigation - or properties of this item can be used for further features (calling detail URLs etc)
	CustomObject mCurrentNativeItem;
	
	
	
	//Last URL user tried before login was required
	String strLastUserRequestedURL;
	
	//Reference to search menu item
	MenuItem actionBarSearchMenuItem;
	
	//Native search
    TextSearchListAdapter mTextSearchListAdapter;
    UpdateTextSearchListviewTask mUpdateTextSearchListviewTask;
    
    //URL used to navigate to unsupported stores in external browser
    String strUnsupportedStoreHost = "";
    
    //used to force clearing history when user returns to "Home" after deep-navigation
    boolean blnForceClearHistory = false;
    
    //Order screen: when user pulls to refresh, the response needs accepting into the cache as soon as data arrives as user
    //views the result immediately.
    boolean blnAcceptResponseIntoCacheOnSwipeRefresh = false;
    boolean blnSuppressScrollToStart = false;

	boolean blnIsDisplayedToUser = false;

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        
        overridePendingTransition(AppUtils.getActivityOpenAnimation(), android.R.anim.fade_out);
		
        setContentView(R.layout.hybrid_activity);
        
        myInstance = this;

        //perform any upgrade relevant code
        executeCodeForVersionUpgrade();
        
        setupCustomActionBar();
        
        findViews();


        
        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
				R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));

		//Handle deep linking
		scanForDeepLinking();
        
    }

  

    public void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        
        /** de-activated on request with version 1.01
        //set page title to category name initially (unless this is a link that is opened in an external browser)
        //if(!currentNavigationCategory.getBoolean(ObjNavigationCategory.FIELD_OPEN_IN_EXTERNAL_BROWSER)){
        if(currentNavigationCategory!=null){ //prevent NPE
    		mTitle = currentNavigationCategory.getDisplayName(); 
        }
    	//}
    	
        if(mTitle!=null){
        	setActionBarTitle(mTitle.toString());
        }
        **/
        
        updateShoppingCartCounter();

		//check if we need to show notification icon and popup
		showActionBarNotificationIcon();
    }
    
    private void setActionBarTitle(String strActionBarTitle){
    	
    	ActionBar actionBar = getActionBar();
    	
        //Spannable used for Action bar title custom font type
        SpannableString spannable = new SpannableString(strActionBarTitle);
        spannable.setSpan(new TypefaceSpan(this, AppTypefaces.TYPEFACE_MAIN_REGULAR), 0, spannable.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        
    	actionBar.setTitle(spannable);
    	
    }
    
    private void updateShoppingCartCounter(){
    	
    	//prevent NPE
    	if(actionBar_tvCartCounter==null){
    		return;
    	}
    	
    	String strShoppingCartCookie = getCookieValueByName(AppUtils.getWiggleURL(getString(R.string.str_http_header_domain)), getString(R.string.str_http_cookie_wiggle_shopping_basket));
    	
    	//Structure of basket cookie 
    	//BasketId2=..&Count=..&AddtionalInfo=...
    	String strShoppingCartCounter = AppUtils.getKeyValueFromNVPString(strShoppingCartCookie, "Count", "&", "=");
    	
    	if(strShoppingCartCounter!=null && !"".equals(strShoppingCartCounter) && Integer.parseInt(strShoppingCartCounter)>0){
    		actionBar_tvCartCounter.setText(strShoppingCartCounter);
    		actionBar_tvCartCounter.setVisibility(View.VISIBLE);
    	}else{
    		actionBar_tvCartCounter.setVisibility(View.GONE);
    	}
    	
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        
    	super.onPrepareOptionsMenu(menu);
    	restoreActionBar(); //done to keep last selected wiggle category
    	return true;
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            
            //custom action layout needs to be clickable and handled as standard menu item
            final Menu m = menu; // used in onClick below
            final MenuItem shoppingBasketItem = menu.findItem(R.id.action_viewShoppingBasket);
            shoppingBasketItem.getActionView().setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {   
                	m.performIdentifierAction(shoppingBasketItem.getItemId(), 0);
                }
            });
            
            //store reference to shopping cart counter
            RelativeLayout menu_shoppingBasket = (RelativeLayout) shoppingBasketItem.getActionView();
            actionBar_tvCartCounter = (TextView) menu_shoppingBasket.findViewById(R.id.actionBar_tvCartCounter);
			actionBar_tvCartCounter.setTypeface(AppTypefaces.instance().fontMainBold);

            actionBarSearchMenuItem = menu.findItem(R.id.action_search);

			//custom action layout "Notification alert" needs to be clickable and handled as standard menu item
			notificationAlertItem = menu.findItem(R.id.action_notificationAlert);
			notificationAlertItem.getActionView().setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					m.performIdentifierAction(notificationAlertItem.getItemId(), 0);
				}
			});

			//Keep Reference to action bar notification widget
			menu_notificationAlert = (RelativeLayout) notificationAlertItem.getActionView();
			menu_notificationAlert_tvAlert = (TextView) menu_notificationAlert.findViewById(R.id.menu_notificationAlert_tvAlert);
			menu_notificationAlert_tvAlert.setTypeface(AppTypefaces.instance().fontGoogleMaterial);
			menu_notificationAlert_tvAlertCounter = (TextView) menu_notificationAlert.findViewById(R.id.menu_notificationAlert_tvAlertCounter);
			menu_notificationAlert_tvAlertCounter.setTypeface(AppTypefaces.instance().fontMainRegularItalic);

			//check when this view is finished drawing .. a popup can be shown as soon as it is drawn on screen
			addOnGlobalLayoutListener(notificationAlertItem.getActionView());


            restoreActionBar();

            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        
        if (id == R.id.action_search) {
        	mCurrentNativeItem=null; //clear native screen item if exists
        	showHideNativeSearch();
            return true;
        }
        
        if (id == R.id.action_viewShoppingBasket) {
        	mCurrentNativeItem=null; //clear native screen item if exists
        	displayLayout(R.id.lytWebView);
        	AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_shop), "shopBasket");
            loadUrlInWebView(AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_shopping_basket), "shopBasket"));
            return true;
        }

        if (id == R.id.action_webviewHome) {
        	takeUserHome();
            return true;
        }        
        
        
        if (id == R.id.action_changeCountry) {
        	AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_shop), "shopCountry");
        	ObjAppUser.instance().clearCountrySpecificFields();
        	if(ObjAppUser.instance().isUserLoggedIn()){
    			AppSession.instance().clearUserSpecificData();
        	}
        	restartApp();
            return true;
        }
        
        
        if (id == R.id.action_showVersion) {
        	displayAppVersionNumber();
            return true;
        }        

		if(id == R.id.action_notificationAlert){
			if(menu_notificationAlert!=null) {

				if(!mNavigationDrawerFragment.isDrawerOpen()){
					mNavigationDrawerFragment.openNavigationDrawer();
				}
				//hide alert
				dismissAlertPopup();

			}
		}
        
        return super.onOptionsItemSelected(item);
    }

 

	public void handleNetworkStatusChange(boolean isConnected) {
		
		// TODO Implement as required
		
	}
	
	private void restartApp(){
		
		/** 
		 *  This kills the app process - not very fond of this solution. 
		 *  Handler delays restarting the app as we may have to wait for shared preferences to be saved (async)
		 *  
		 **/
		appRestartHandler.postDelayed(appRestartRunnable, 800);
	}
	
	private Handler appRestartHandler = new Handler();
	
	private Runnable appRestartRunnable = new Runnable() {
		   public void run() {
				killAndRestartApp();
		   }
	};
	
	private void killAndRestartApp(){

		/*
			Some native activities use HybridActivity to show content e.g.
				Legal & Insurance (insurance quote)
				Events - event details
				News - some news links

			Just killing and restarting the app with a default intent leads to old back
			stack being shown. We hence restart the app as a completely new task and clear
			the back stack.

		 */
		Intent restartIntent = new Intent(this, WelcomeActivity.class);
		restartIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

		//We also make sure we finish any other native activities first before restarting
		/*
		if(LegalActivity.instance()!=null){
			LegalActivity.instance().finish();
		}

		if(NewsfeedActivity.instance()!=null){
			NewsfeedActivity.instance().finish();
		}

		if(EventsActivity.instance()!=null){
			EventsActivity.instance().finish();
		}

		if(StartupActivity.instance()!=null){
			StartupActivity.instance().finish();
		}

		if(WelcomeActivity.instance()!=null){
			WelcomeActivity.instance().finish();
		}

		//Finish Hybrid activity
		finish();
		*/

		//With API level 16, we can replace the above and finish all activities in the current task by just calling ..
		finishAffinity();


		AlarmManager alm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
		alm.set(AlarmManager.RTC, System.currentTimeMillis() + 500, PendingIntent.getActivity(this, 0, restartIntent, 0));
		android.os.Process.killProcess(android.os.Process.myPid());
		
	}
		
	

	@Override
	public void onSaveInstanceState(Bundle savedState) {

	    super.onSaveInstanceState(savedState);
	    
	}


	@Override
	public void onRestoreInstanceState(Bundle savedState) {

	    super.onRestoreInstanceState(savedState);


	}
	
    

	
	public void showUserAsLoggedInOrOut(){
		
		
		if(ObjAppUser.instance().isUserLoggedIn()){
			
			navigation_tvLogin.setText(getString(R.string.navigation_tvLogin_loggedIn));
			navigation_tvRegister.setText(getString(R.string.str_sign_out));
			navigation_tvHello.setText(String.format(getString(R.string.navigation_tvHello_loggedIn), ObjAppUser.instance().getString(ObjAppUser.FIELD_FORENAME)));
			
		}else{
			
			navigation_tvLogin.setText(getString(R.string.navigation_tvLogin));
			navigation_tvRegister.setText(getString(R.string.navigation_tvRegister));
			navigation_tvHello.setText(getString(R.string.navigation_tvHello));
			
			updateOrderTrackingBadge();
			
		}
		
	}	
	
	public void handleInAppLogin(){
		
		HybridActivity.instance().showUserAsLoggedInOrOut();
		AppSession.instance().setInAppLogin(false);
		navigateToReturnUrl();
		
	}
	
	private void navigateToReturnUrl(){
		
		if(strLastUserRequestedURL!=null){
			loadUrlInWebView(strLastUserRequestedURL);
			strLastUserRequestedURL = null;
		}
		
	}
	
	
	public void submitMetaDataRequests(){
		
        //For first time users: make sure that app does not download categories from server first time, but use the category data that is saved locally
		//DataStore.useLocalMetaData(getCurrentCountry(), AppRequestHandler.RESOURCE_CATEGORIES);
		
    	
	
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		
		int id = view.getId();
		
		switch (id) {
		
			case R.id.navigation_btnLoginAccount:
				handleLoginAccountClick();
				break;
				
			case R.id.navigation_btnLogoutRegister:
				handleLogoutRegisterClick();
				break;
				
			case R.id.nativeSearch_ivResortTextSearchClear:
				onKeywordTextCleared();
				break;

			case R.id.orderDetail_btnMoreDetails:
				AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_my_orders_item_status), "itemstatusMoreDetails");
				openOrderDetailUrl();
				break;
				
		default:
			break;
		}
		
	}

	@Override
	public void onApiRequestSuccess(ObjApiRequest apiRequest,
			JSONObject apiSuccessDetail) {

		if(AppRequestHandler.getAuthenticationEndpoint().equals(apiRequest.getURLEndpoint())){
			hidePleaseWait();
			
			ObjNavigationCategory androidNativeCategory = AppSession.instance().getAndroidNativeCategory();
			if(androidNativeCategory!=null){
				//native android screens: if in app login was required - navigate to target screen
				handleNavigationClick(androidNativeCategory, false);
				AppSession.instance().setAndroidNativeCategory(null); //clear parameter
			}else{
				//default in app login behaviour - navigate to webview return url
				navigateToReturnUrl();
			}
			
		}else if(AppRequestHandler.getOrderTrackingEndpoint().equals(apiRequest.getURLEndpoint())){
			
			orderList_tvNoData.setText(R.string.orderList_tvNoData);
			
			if(ON_RESPONSE_ACTION.RETRIEVE_USER_RELATED_DATA.equals(apiRequest.getOnResponseAction())){
				
				//update order counter if any
				updateOrderTrackingBadge();
				
			}else{
				
				//update entire list
				updateOrderTrackingList();
				
			}
			
			if(blnAcceptResponseIntoCacheOnSwipeRefresh){
				blnAcceptResponseIntoCacheOnSwipeRefresh = false;
				AppSession.instance().acceptOrderTrackingPayloadIntoCache();
			}
			
		}
		
	}

	@Override
	public void onApiRequestError(ObjApiRequest request,
			int intHttpResponseCode, JSONObject apiErrorDetail) {
		
		if(AppRequestHandler.getAuthenticationEndpoint().equals(request.getURLEndpoint())){
			//any problems with login - show login screen
			AppUtils.showCustomToast("Please log in manually..", true);
			hidePleaseWait();
			showLoginScreen();
		}
		
	}

	@Override
	public void setupCustomActionBar() {
		// TODO Auto-generated method stub
		
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.backgroundGreyDark))); 
		
	}

	@Override
	public void setCustomFontTypes() {
		// TODO Auto-generated method stub

		hybridActivity_tvCurrentLink.setTypeface(AppTypefaces.instance().fontMainRegular);
		main_tvBreadcrumb.setTypeface(AppTypefaces.instance().fontMainBold);
		navigation_tvHello.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		navigation_tvLogin.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		navigation_tvRegister.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		nativeSearch_etQueryString.setTypeface(AppTypefaces.instance().fontMainRegular);
		textSearch_tvGroupHeader.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		orderList_tvGroupHeader.setTypeface(AppTypefaces.instance().fontMainRegular);
		orderList_tvNoData.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		orderList_tvTitle.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		
		//Order Detail
		orderDetail_tvTitle.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
		orderDetail_tvDeliveryStatusText.setTypeface(AppTypefaces.instance().fontMainRegular);
		orderDetail_tvDeliveryStatusDate.setTypeface(AppTypefaces.instance().fontMainBold);
		orderDetail_tvProductBrandAndName.setTypeface(AppTypefaces.instance().fontMainBold);
		orderDetail_tvProductVariety.setTypeface(AppTypefaces.instance().fontMainRegular);
		orderDetail_tvProductPrice.setTypeface(AppTypefaces.instance().fontMainBold);
		orderDetail_lblDeliveryAddress.setTypeface(AppTypefaces.instance().fontMainBold);
		orderDetail_tvDeliveryAddress.setTypeface(AppTypefaces.instance().fontMainRegular);
		orderDetail_lblOrderNumber.setTypeface(AppTypefaces.instance().fontMainBold);
		orderDetail_tvOrderNumber.setTypeface(AppTypefaces.instance().fontMainRegular);
		orderDetail_tvMoreDetails.setTypeface(AppTypefaces.instance().fontMainRegular);
			
	}

	@Override
	public void findViews() {
		
		navigation_tvHello = (TextView) findViewById(R.id.navigation_tvHello);
		navigation_pbLoginInProgress = (ProgressBar) findViewById(R.id.navigation_pbLoginInProgress);
		navigation_tvLogin = (TextView) findViewById(R.id.navigation_tvLogin);
		navigation_tvRegister = (TextView) findViewById(R.id.navigation_tvRegister);
		navigation_lytAccountActions = (LinearLayout) findViewById(R.id.navigation_lytAccountActions);
		navigation_ivCountry = (ImageView) findViewById(R.id.navigation_ivCountry);
		navigation_ivCountry.setImageDrawable(AppUtils.getCountryFlag());

		hybridActivity_tvCurrentLink = (TextView) findViewById(R.id.hybridActivity_tvCurrentLink);
		hybridActivity_tvCurrentLink.setVisibility(View.GONE);

		if(!"".equals(AppUtils.REDIRECT_APP_TO_TEST_SERVER) || !AppUtils.isProductionVersionOfApp()){
			hybridActivity_tvCurrentLink.setVisibility(View.VISIBLE);
		}
		
		
		 mWebView = (WebView) findViewById(R.id.main_webView);
		 main_pbLoadProgress = (ProgressBar) findViewById(R.id.main_pbLoadProgress);
		 main_tvBreadcrumb = (TextView) findViewById(R.id.main_tvBreadcrumb);
         tvErrorMessage = (TextView) findViewById(R.id.main_tvErrorMessage);
         lytPleaseWait = (RelativeLayout) findViewById(R.id.main_lytPleaseWait);
         ivPleaseWait = (ImageView) findViewById(R.id.main_ivPleaseWait);
         
     	 lytWebView = (RelativeLayout) findViewById(R.id.lytWebView);
    	 lytNativeSearch = (LinearLayout) findViewById(R.id.lytNativeSearch);
    	 
    	 nativeSearch_etQueryString = (EditText) findViewById(R.id.nativeSearch_etQueryString);
    	 setupNativeSearchWidgets();
         
    	 nativeSearch_ivResortTextSearchClear = (ImageView) findViewById(R.id.nativeSearch_ivResortTextSearchClear);
    	 nativeSearch_lvSearchResult = (ListView) findViewById(R.id.nativeSearch_lvSearchResult);
    	 
    	 textSearch_tvGroupHeader = (TextView) findViewById(R.id.textSearch_tvGroupHeader);
    	 nativeSearch_dynamicHeader = (LinearLayout) findViewById(R.id.nativeSearch_dynamicHeader);
    	 
    	 //Order tracking
    	 lytOrderTracking = (RelativeLayout) findViewById(R.id.lytOrderTracking);
		 //bottom padding only required for tabbed navigation
		if(AppUtils.getWiggleVersion().getVersionCode()>AppUtils.APP_VERSION.VERSION_1_HYBRID.getVersionCode()){
		 	lytOrderTracking.setPadding(0, 0, 0, 0);
		 }

    	 orderTracking_orderList = (RelativeLayout) findViewById(R.id.orderTracking_orderList);
    	 orderList_swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.orderList_swipeRefreshLayout);
    	 setupOrderSwipeToRefreshLayout();
    	 


    	 orderList_lvOrderHistory = (AbsListView) findViewById(R.id.orderList_lvOrderHistory);
    	 orderList_lytGroupHeader = (LinearLayout) findViewById(R.id.orderList_lytGroupHeader);
    	 orderList_tvGroupHeader = (TextView) findViewById(R.id.orderList_tvGroupHeader);
    	 
 		 //we need to make sure that swipe refresh is only possible if user is at the top of the listview. Otherwise swipe refresh is triggered 
 		 //every time the user scrolls up from anywhere in the listview. 
 		 //Thank you, http://nlopez.io/swiperefreshlayout-with-listview-done-right/
    	 orderList_lvOrderHistory.setOnScrollListener(new AbsListView.OnScrollListener() {  
 			   @Override
 			   public void onScrollStateChanged(AbsListView view, int scrollState) {

 			   }

 			   @Override
 			   public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
 				  
 				   //this makes sure we dont swipe to refresh when scrolling up
	 			     int topRowVerticalPosition = 
	 			       (orderList_lvOrderHistory == null || orderList_lvOrderHistory.getChildCount() == 0) ? 0 : orderList_lvOrderHistory.getChildAt(0).getTop();
	 			    orderList_swipeRefreshLayout.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
	 			   
	 			   //this handles the Order group header
	 			   if(orderList_lvOrderHistory!=null &&
		    				ObjSalesOrderManager.getInstance().getAllOrderedItems().size()>0
		    				){
		    			
		    			int intFirstVisibleItem = orderList_lvOrderHistory.getFirstVisiblePosition();
		    			
		    			ObjSalesItem currentItem = (ObjSalesItem) orderList_lvOrderHistory.getItemAtPosition(intFirstVisibleItem);
		    			if(currentItem!=null){ //prevent NPE
			    			String strOrderGroupListHeader = String.format(
			    							getString(R.string.str_order_list_header_group),
			    							currentItem.getParentRecord().getString(ObjSalesOrder.FIELD_ORDER_ID),
			    							DateUtils.getOrderTrackingDate(currentItem.getParentRecord().getDateFromJson(ObjSalesOrder.FIELD_ORDER_DATE))
			    							);
			    			orderList_tvGroupHeader.setText(strOrderGroupListHeader);
			    			if(AppUtils.showListviewHeaders()){
			    				orderList_lytGroupHeader.setVisibility(View.VISIBLE);	
			    			}
		    			}
	
		    			
		    		}else{
		    			
		    			orderList_lytGroupHeader.setVisibility(View.GONE);		    		
		    			
		    		}
 			   
 			   }
 			  
 			   
 		 });
    	 
    	 
    	 orderList_tvNoData = (TextView) findViewById(R.id.orderList_tvNoData);
    	 lytOrderDetail = (ScrollView) findViewById(R.id.lytOrderDetail);
    	 orderList_tvTitle = (TextView) findViewById(R.id.orderList_tvTitle);
    	 setupOrderTrackingList();
    	
    	 //Order Details
    	 orderDetail_tvTitle = (TextView) findViewById(R.id.orderDetail_tvTitle);
    	 orderDetail_tvDeliveryStatusText = (TextView) findViewById(R.id.orderDetail_tvDeliveryStatusText);
    	 orderDetail_tvDeliveryStatusDate = (TextView) findViewById(R.id.orderDetail_tvDeliveryStatusDate);
    	 orderDetail_ivProductImage = (ImageView) findViewById(R.id.orderDetail_ivProductImage);
    	 orderDetail_tvProductBrandAndName = (TextView) findViewById(R.id.orderDetail_tvProductBrandAndName);
    	 orderDetail_tvProductVariety = (TextView) findViewById(R.id.orderDetail_tvProductVariety);
    	 orderDetail_tvProductPrice = (TextView) findViewById(R.id.orderDetail_tvProductPrice);
    	 orderDetail_lblDeliveryAddress = (TextView) findViewById(R.id.orderDetail_lblDeliveryAddress);
    	 orderDetail_tvDeliveryAddress = (TextView) findViewById(R.id.orderDetail_tvDeliveryAddress);
    	 orderDetail_lblOrderNumber = (TextView) findViewById(R.id.orderDetail_lblOrderNumber);
    	 orderDetail_tvOrderNumber = (TextView) findViewById(R.id.orderDetail_tvOrderNumber);
    	 orderDetail_tvMoreDetails = (TextView) findViewById(R.id.orderDetail_tvMoreDetails);
    	 lytOrderTrackingGraph = (LinearLayout) findViewById(R.id.lytOrderTrackingGraph);


		 updateWidgets();
		
	}	
	
	private void updateWidgets(){
		
        setupWebClients();
        showUserAsLoggedInOrOut();
        
        setCustomFontTypes();
        
		mTextSearchListAdapter = new TextSearchListAdapter();
		nativeSearch_lvSearchResult.setAdapter(mTextSearchListAdapter);   	
		
   	 	setListViewScrollAdapter();
		
        
	}
	
	private void setupNativeSearchWidgets(){
		
   	 	nativeSearch_etQueryString.addTextChangedListener(mTypeaheadTextWatcher);
	 
   		nativeSearch_etQueryString.setOnFocusChangeListener(new OnFocusChangeListener() {
   			
   			@Override
   			public void onFocusChange(View v, boolean hasFocus) {
   				
   				if(hasFocus){
   					//nativeSearch_etQueryString.setText(""); //leave text as user might still need previous search result - user can press "x" to clear text
   				}
   				
   			}
   			
   		});
   		
   		//handle "Go" (Enter) click on keyboard
   		nativeSearch_etQueryString.setOnEditorActionListener(new OnEditorActionListener() {
   			
   			@Override
   		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
   		    	
   		        if (actionId == EditorInfo.IME_ACTION_GO) {
   		            
   		        	if(!"".equals(nativeSearch_etQueryString.getText().toString())){
	   		        	//if user presses enter whilst typing in a search string - build query string
	   		        	//http://www.wiggle.co.uk?s=texttheuserentered
	   		        	//and open in webview
   		        		try{
   		        			String strQueryKeyword = nativeSearch_etQueryString.getText().toString().toUpperCase();
   		        			
   		        			//send GTM tag
   		        			Map<String, Object> gtmTagsMap = new HashMap<String, Object>();
   		        			gtmTagsMap.put("keyword", strQueryKeyword);
   		        			AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_search), "searchSearch", gtmTagsMap);
   		        			//also send this tag
   		        			AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_search), "searchViewAll", gtmTagsMap);
   		        			
   		        			//forward search in browser
	   		        		String strQuery = URLEncoder.encode(strQueryKeyword, "UTF-8");
		   		        	String strTargetURL = AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home)) + "?s=" + strQuery;
		   		        	loadUrlInWebView(strTargetURL);
   	    				} catch (UnsupportedEncodingException e) {
   	    					Logger.printMessage(logTag, "" + e.getMessage(), Logger.ERROR);
   	    				}
   		        		
   		        	}
   		        	
   		        	hideVirtualKeyboard();
   		        	displayLayout(R.id.lytWebView);
   		        	
   		        	return true;
   		        }
   		        return false;
   		    }


   		});
   		
   		
	}
	

	private void setupWebClients(){

		mWebView.setWebViewClient(genericWebClient);
		//mWebView.setWebChromeClient(new CustomWebChromeClient()); //chrome client handles file choosers etc ...
		//Version 1.3 - Wiggle requires a progress bar to be shown .. so set web chrome client as well
		mWebView.setWebChromeClient(new WebChromeClient() {
			public void onProgressChanged(WebView view, int progress) {

				main_pbLoadProgress.setVisibility(View.VISIBLE);
				main_pbLoadProgress.setProgress(progress);

				if(progress == 100){
					//Page loaded .. hide progress bar
					main_pbLoadProgress.setVisibility(View.VISIBLE);
				}
			}
		});


		mWebView.getSettings().setJavaScriptEnabled(true); //yes, that is a security issue ... but given that we only load known and no external URLs this is in control
		
		String strUserAgentString = mWebView.getSettings().getUserAgentString();
		
		//enable debugging through Chrome browser
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			mWebView.setWebContentsDebuggingEnabled(true);
		}

		//Back navigation on some Wiggle pages (e.g. assist insurance product quote use case)
		//result in ERR_CACHE_MISS, so add this workaround.
		//Android Lollipop Chrome WebView is the likely cause for this behaviour
		//See https://stackoverflow.com/questions/25664146/android-4-4-giving-err-cache-miss-error-in-onreceivederror-for-webview-back
		if (Build.VERSION.SDK_INT == 19) {

			//CAUTION: this can lead to outdated cache being used, so watch this one for
			//any odd user experience reports.
			//mWebView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK); // uses cached resources even if they're expired. This is no good!

			//Important!! Disabled the above LOAD_CACHE_ELSE_NETWORK as this lead to the following bug:
			//A user (not logged in) adds an item to the basket and presses "Checkout".
			//A request is fired to the /checkout endpoint. Wiggle recognise the user as logged out
			//and suggest he visits the "Login" URL which presents the native login screen.
			//The native login screen is shown and the user submits his login.
			//The secure/checkout request is fired, BUT THE RESPONSE is taken from the cache.
			//This of course shows the user as still "logged out" and results in a request to login again.
			//This then happens indefinitely.
			//So instead, we are working around ERR_CACHE_MISS by not using our cache at all.
			//This is far from ideal, so check for any user reports reporting odd behaviour.
			mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE); // Dont use cache at all

		}
		
		String strCustomUserAgent = strUserAgentString + String.format(getString(R.string.str_browser_header_user_agent_value), AppUtils.getAppVersionNumber());
		//AppUtils.showCustomToast(strCustomUserAgent, true); //test only
		mWebView.getSettings().setUserAgentString(strCustomUserAgent);
		
		//speed up the webview 
		tweakWebviewPerformance();
		
		
		//Javascript interface - TODO: enable and adjust implementation if required
		//mWebView.addJavascriptInterface(new CustomJavaScriptInterface(), "HTMLOUT");
		//mWebView.addJavascriptInterface(new IJavascriptHandler(), "cpjs");
		
		//clear cache to clear any old access tokens that might float around
		clearCache(this, 0);
		
		loadTargetURL(0);
		
	}
	
	private void loadTargetURL(int intWebViewCaller){
		
		//no caller Id passed - get it from intent
		if(intWebViewCaller==0){
			intWebViewCaller = getIntent().getIntExtra(WEB_VIEW_CALLER, 0);
		}
		
		//persist webview caller for later use
		mWebViewCaller4AndroidNativeScreen = intWebViewCaller;
		
		String strTargetUrl = AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home)); //default
		
		//check if this was called from native news or events screen - and load passed URL if required
		if(getIntent().getStringExtra(LOAD_TARGET_URL_FROM_NATIVE_SCREEN)!=null){
			strTargetUrl = getIntent().getStringExtra(LOAD_TARGET_URL_FROM_NATIVE_SCREEN);
		}

		//check if this was called from configurable items on the StartupScreen - and load passed URL if required
		if(getIntent().getStringExtra(LOAD_TARGET_URL_FROM_STARTUP_MENU)!=null){
			strTargetUrl = getIntent().getStringExtra(LOAD_TARGET_URL_FROM_STARTUP_MENU);
		}


		switch (intWebViewCaller) {
		
			case R.id.startup_btnShop:
				AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_home), "homeShop");
				strTargetUrl = AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_wiggle_home), "homeShop");
				break;
				
			case R.id.login_tvForgotPassword:
				AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_signin), "signinFgtPwd");
				strTargetUrl = AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_forgot_password), "signinFgtPwd");
				break;
				
			case R.id.login_btnRegister:
				AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_signin), "signinRegister");
				strTargetUrl = AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_registration), "signinRegister");
				break;
				
			case R.id.startup_lytMyAccount:
				AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_home), "homeMyAccount");
				strTargetUrl = AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_my_account), "homeMyAccount");
				break;
				
			case R.id.startup_lytTrackMyOrder:
				AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_home), "homeTrackOrder");
				//Replaced by a native screen in version 1.06
				//strTargetUrl = AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_my_orders), "homeTrackOrder");
				retrieveOrderHistory();
				break;
				
			case R.id.startup_lytWishlist:
				AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_home), "homeWishlist");
				strTargetUrl = AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_my_wishlist), "homeWishlist");
				break;
				

			case R.id.orderDetail_btnMoreDetails:
				//TODO: send event tag
				strTargetUrl = ((ObjSalesItem) mCurrentNativeItem).getParentRecord().getString(ObjSalesOrder.FIELD_DETAILS_URL);
				break;
			
		default:
			break;
		
		}
		
		
		//no website? do nothing .. 
		if(strTargetUrl==null || "".equals(strTargetUrl)){
			finish();
			return;
		}else{
			if(!strTargetUrl.startsWith("https://") && !strTargetUrl.startsWith("http://")){
				strTargetUrl = "http://" + strTargetUrl;
			}
		}
		
		
		//TODO: this may not be needed for Wiggle as there is no interception of html buttons ... only enable first leg when problems become apparent
		if(1==2 && Build.VERSION.SDK_INT == 19) {
			
			loadUrlInExternalBrowser(strTargetUrl);
			
			
		}else{
		
			//all other Android versions - load URL in webview
			loadUrlInWebView(strTargetUrl);
		
		}

				
	}
	
	
	//user defined WebViewClient class in order to let Android OS handle clicks on certain links 
	WebViewClient genericWebClient = new WebViewClient(){

			String pendingUrl = null;
			


			/* Please note - shouldOverrideUrlLoading is not reliable called on http redirects ... for this purpose onPageStarted was added below */
		    @Override
		    public boolean shouldOverrideUrlLoading(WebView  view, String  url){
		    	
		    	boolean blnValue = isOverrideUrlLoading(view, url);
		    	return blnValue;
		    	//view.loadUrl(url, addAdditionalHttpHeaders());
		    	
		        
		    }
		    
	        //in case we want an external app to be opened ... 	   
	    	private void openURLInExternalApp(String url){
	    		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
	    		startActivity(intent);    		
	    	}
	    	
	    	private boolean isOverrideUrlLoading(WebView  view, String  url){

				//check if we are connected to the internet - if not, show error and abort page load
				if(!AppUtils.isNetworkConnected()){
					showHttpErrorDialog(url);
					view.stopLoading();
					return true;
				}
	    		
		    	//can't make webview visible when Javascript is called, so using a trick with a view that hides the webview from the user
		    	showPleaseWait(false);
		    	
		    	//URLs that are allowed to open in external apps
				if(url.contains("callto:") 
						|| url.contains("tel:") 
						|| url.contains("geo:") 
						|| url.contains("mailto:")){
				
		    		openURLInExternalApp(url);
		    		return true;				
					
				}

				// We want to intercept any attempt to show the web login screen with our own
				//Lower casing this as Wiggle introduced different cased URL patterns in the past ...
                if(url.toLowerCase().contains("myaccount/logon")
							|| url.toLowerCase().contains("checkout/logon")
							){


					//the URL contains a parameter "returnUrl"
					/** we don't need this as we store the URL originally requested in strLastUserRequestedURL 
					Uri uri = Uri.parse(url);
					String strReturnURLFragment = uri.getQueryParameter("returnUrl");
					String strReturnURL = uri.getScheme() + "://" + uri.getHost() + "/" +  strReturnURLFragment;
					strLastUserRequestedURL = strReturnURL;
					**/
					
					handleLoginRequired(url);
					return true; //don't open URL
				}


				//Legal & Insurance - check if we need to display pdf documents (such as Terms & Conditions)
				if(AppUtils.isUrlPdfDocument(url)){

					//open PDF externally
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setDataAndType(Uri.parse(url), "application/pdf");
					try{
						view.getContext().startActivity(intent);
					} catch (ActivityNotFoundException e) {
						//user does not have a pdf viewer installed - open pdf with google docs
						mWebView.loadUrl(AppGlobalConstants.GOOGLE_DOCS_VIEWER + url);
					}

					return true; //dont open URL in webview
				}

		    	//in all other cases, open URL in this webview
				return false;
				
	    	}
	    	
		    
		    //here some defined URLs can be intercepted 
		    @Override
		    public void onLoadResource(WebView  view, String  url){
		    	

				//in all standard cases, simply load URL
				super.onLoadResource(view, url);
		    }
		    
		    
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {

			  //For debugging or on test server
			  hybridActivity_tvCurrentLink.setText(url);

			  //Set breadcrumb text (currently only for Insurance quote Velosure pages)
			  setBreadcrumbText(url);

			  if (pendingUrl == null) {
			    pendingUrl = url;
			  }

	    	  //** on redirects and when not clicking on hyperlinks, shouldOverrideUrlLoading would have been missed - so handling it here.
	    	  isOverrideUrlLoading(view, url);
	    	  
			}		    
		    
		    //HTTP 500 server problem - needed to read html content of web-view before it is displayed to the user
		    @Override
		    public void onPageFinished(WebView webView, String url) {
		    	
		    	if (!url.equals(pendingUrl)) {
		    	    
		    		Logger.printMessage(logTag, "Detected HTTP redirect " + pendingUrl + "->" + url, Logger.INFO);
		    		//Add code reacting to page redirects here if needed ... 
		    	    pendingUrl = null;
		    	  
		    	}

		    	/** .. example use of javascript manipulation .. only enable if required 
		    	 * 
		    	 * If the following is enabled, then you also need to enable mWebView.addJavascriptInterface(new CustomJavaScriptInterface(), "HTMLOUT"); above
		    	 *

		    	String strJavascript = "" 
		    			+ "javascript:(function onSelectFieldChange(sel) {"
		    			+ "var value = sel.value;"
		    			+ "javascript:window.cpjs.sendToAndroid(value);"
		    			+ "}"
		    			+ "javascript:document.getElementById('langId').onchange = 'onSelectFieldChange(this)';"
		    			+ "javascript:window.HTMLOUT.showHTML" +
                        "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');"
		    			;
		    	
		    	
		    	
		    	mWebView.loadUrl(strJavascript, addAdditionalHttpHeaders());
		    	
		    			    	 **/

		    	
		    	//set Activity title to title of current web-page (useful for the user to know where he is when browsing deeper into Wiggle)
		    	//Usually works like this: HybridActiviy.this.setTitle(view.getTitle()); - but we do not have a title here, so set action bar title accordingly
		    	//extractPageTitle(webView.getTitle(), url); //de-activated on request in version 1.01
		    	
		    	updateShoppingCartCounter();
		    	
		    	//Version 1.02
		    	//check if the WiggleCustomer2 cookie indicates that a user has logged in and the delivery destination / currency / language of his account
		    	//does not match the information we have locally
		    	//TODO: should be merged with checkForUserInternationalSettingChange as both methods do the same thing
		    	listenForCustomerCookie(AppUtils.getWiggleURL(getString(R.string.str_http_header_domain)), url);
		    	
	    	    //Wiggle users can change their location within the web-browser (Currency & Delivery options), so we need to listen for that.
		        //Note: as this method contains mWebView.clearHistory() it needs to remain in onPageFinished - as otherwise not all of the preceding history is removed (Gotta love Android :-) )
		    	checkForUserInternationalSettingChange(url);
				
		    	if(blnForceClearHistory){
		    		mWebView.clearHistory();
		    		blnForceClearHistory = false;
		    	}
		    	
		    	hidePleaseWait();
		    }
		    
		    
		    @Override
		    public WebResourceResponse shouldInterceptRequest(WebView view, String url)
		    {
		    	//use this method to intercept AJAX calls
		    	return null;
		    }


			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				super.onReceivedError(view, errorCode, description, failingUrl);

				view.loadUrl("about:blank");
			}


			@TargetApi(android.os.Build.VERSION_CODES.M)
			@Override
			public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
				//Removed due to Google policy: Redirect to deprecated method, so you can use it in all SDK versions
				//onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
				if(req!=null && req.getUrl()!=null) {
					//showHttpErrorDialog(req.getUrl().toString());
				}
			}


			//With Android M we can finally react to Http errors and display a nice user message
			@Override
			public void onReceivedHttpError (WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
				//Toast.makeText(view.getContext(), "HTTP error "+errorResponse.getStatusCode(), Toast.LENGTH_LONG).show();

				/* From google docs:

					Notify the host application that an HTTP error has been received from the server while loading
					a resource. HTTP errors have status codes >= 400. This callback will be called for any resource
					 (iframe, image, etc), not just for the main page. Thus, it is recommended to perform minimum
					 required work in this callback. Note that the content of the server response may not be provided
					 within the errorResponse parameter.

					 So without further logic, even a website that loads, but individual requests have errors would
					 display a multitude of error messages... TODO: filter out relevant errors and showHttpErrorDialog() accordingly

				 */

				if(request!=null && request.getUrl()!=null) {
					//showHttpErrorDialog(request.getUrl().toString());
				}
			}
		    
		    
		};	
		
		private void showPleaseWait(boolean blnConfirmShowPleaseWait){
			
			if(!blnConfirmShowPleaseWait){
				return;
			}
			
			lytPleaseWait.setVisibility(View.VISIBLE);
	    	AppUtils.startFloatingLogoAnimation(getBaseContext(), ivPleaseWait);
	    	
		}
		

		
		private void hidePleaseWait(){
			
			AppUtils.stopFloatingLogoAnimation(getBaseContext(), ivPleaseWait);
			lytPleaseWait.setVisibility(View.GONE);
	    	
		}

		//HTTP 500 server problem - needed to read html content of web-view before it is displayed to the user
		class CustomJavaScriptInterface {
			
			//Annotation @JavascriptInterface necessary for any method you want to make available to web page code
			//Without this annotation, the method will not be executed. NB: the method MUST also be public
			@JavascriptInterface 
			public void showHTML(String html) {
				String htmlString = html;
				
				if(htmlString==null){
					//return and wait for valid html download to complete
					return;
				}else{
					AppUtils.showCustomToast(htmlString, false);
				}
				
				if(htmlString.contains("500 - Internal Server Error")
						|| htmlString.contains("Internal Server Error")
						){
					//reload URL
					showPleaseWait(false);
					
					loadTargetURL(0);
					Logger.printMessage(logTag, "500 error - reloading resource", Logger.ERROR);
				}else{
					//can't make webview visible at this state, so using a trick with a view that hides the webview from the user
					Logger.printMessage(logTag, "Webpage ok - display to user", Logger.INFO);
					hidePleaseWait();
				}
			}
			
		}	
		
		//methods allowing to clear cache of paa webview
		static int clearCacheFolder(final File dir, final int numDays) {

		    int deletedFiles = 0;
		    if (dir!= null && dir.isDirectory()) {
		        try {
		            for (File child:dir.listFiles()) {

		                //first delete subdirectories recursively
		                if (child.isDirectory()) {
		                    deletedFiles += clearCacheFolder(child, numDays);
		                }

		                //then delete the files and subdirectories in this dir
		                //only empty directories can be deleted, so subdirs have been done first
		                
		                if (child.lastModified() < new Date().getTime() - numDays * android.text.format.DateUtils.DAY_IN_MILLIS) {
		                    if (child.delete()) {
		                        deletedFiles++;
		                    }
		                }
		            }
		        }
		        catch(Exception e) {
		        	Logger.printMessage(logTag, "Failed to clean the cache " + e.getMessage(), Logger.ERROR);
		            
		        }
		    }
		    return deletedFiles;
		}

		/*
		 * Delete the files older than numDays days from the application cache
		 * 0 means all files.
		 */
		public static void clearCache(final Context context, final int numDays) {
			int numDeletedFiles = clearCacheFolder(context.getCacheDir(), numDays);
			Logger.printMessage(logTag, "Deleted " + numDeletedFiles + " cache files", Logger.INFO);
		}

		@Override
		public void onResume() {
			
		    super.onResume();

			myInstance = this; //repeated after onCreate - prevent NPE after app in background
		    
	        //load local navigation tree
			loadLocalNavigationTreeIfMissing();

			//check if we need a notification in the options menu
			//We are doing this in onResume as actionbar might not be ready yet when
			//navigation drawer badges are already displayed
			invalidateOptionsMenu();

		}

		@Override
		protected void onPause() {
			super.onPause();
			blnIsDisplayedToUser = false;
		}
		
		@Override
		public void onWindowFocusChanged(boolean hasFocus) {
		    super.onWindowFocusChanged(hasFocus);

			if(hasFocus){


			}
		    
		}		
		
		
		@Override
		public void onBackPressed(){
			
			//in case please wait screen is shown - always hide it
			hidePleaseWait();
			
			if(mNavigationDrawerFragment.isDrawerOpen()){
				mNavigationDrawerFragment.closeDrawer();
				return;
			}
			
			if(lytNativeSearch.getVisibility()==View.VISIBLE){
				displayLayout(R.id.lytWebView);
				return;
			}
			
			//Check if we need to handle any Android Native screens that will over time request some of the Webview / mobile webpage features of this app
			if(onBackFromAndroidNativeScreens()){
				return;
			}
			
			if(mWebView.canGoBack()){
				mWebView.goBack();
			}else{
				
				AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_general), "generalBack");
				/**
				alertUser(
		        		0,
		    			"",
		    			getString(R.string.str_place_an_ad_dialog_title),
		    			getString(R.string.str_place_an_ad_dialog_message),
		    			Alert.OK_GO_BACK);
		    	**/
				AppUtils.showCustomToast("Nothing more to go back to...", true);
				
				
				//clear order manager
				ObjSalesOrderManager.getInstance().clear();
				
				//clear any session parameters
				AppSession.instance().setAndroidNativeCategory(null);
				AppSession.instance().setInAppLogin(false);
				
				finish();
				
			}
			
		}


	private Map<String, String> addAdditionalHttpHeaders(){
		
		Map<String, String> headers = new HashMap<String, String>();
		
		/* Any additional Http headers can be added here. Before doing this check if this can be done on the webview.setSettings() though */
		//map.put(key, value);
		
		
		/* Add Wiggle specific request cookies - this is done using CookieManager.setCookie(domain, cookieNameAndValue) */
		
		//The Customer cookie
		String strCustomerCookieName = getString(R.string.str_http_cookie_name_customer);
		String strCustomerCookieValue = String.format(getString(R.string.str_http_cookie_name_customer_pattern)
										, ObjAppUser.instance().getString(ObjAppUser.FIELD_CUSTOMER_ID)
										, ObjAppUser.instance().getString(ObjAppUser.FIELD_DISCOUNT)
										, ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_CURRENCY)
										, ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_DELIVERY_DESTINATION)
										, ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_LANGUAGE)
										);
		
		//Domain can be one the user selected within the mobile site - or one chosen in the native app
		String strDomain = ObjAppUser.instance().getString(ObjAppUser.FIELD_CURRENT_WIGGLE_STORE);
		if(strDomain==null || "".equals(strDomain)){
			strDomain = AppUtils.getWiggleURL(getString(R.string.str_http_header_domain));
		}
		
		
		
		
		/*
		Date dt = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(dt); 
		c.add(Calendar.DATE, 1);
	    String strExpires = org.apache.http.impl.cookie.DateUtils.formatDate(c.getTime()); //formats date correctly to RFC2616 standard
	    */
		
		//The session cookie
		String strSessionCookieName = getString(R.string.str_http_cookie_name_session);
		String strSessionCookieValue = ObjAppUser.instance().getString(ObjAppUser.FIELD_ACCESS_TOKEN);
		
		
		String strCustomerCookieHeaderString = strCustomerCookieName + "=" + strCustomerCookieValue;
		String strSessionCookieHeaderString = strSessionCookieName + "=" +  strSessionCookieValue;
		
		//set/overwrite existing browser cookie
		//Note - we are only adding very basic cookie information here (name=value). If required set a fully qualified cookie e.g. "cookieName=cookieValue;domain=domain.com;path=/;Expires=Thu, 2 Aug 2021 20:47:11 UTC;"
		CookieManager cookieManager = CookieManager.getInstance();
		
		/*
		 * Version 1.02 - we do no longer add a self-made WiggleCustomer2 cookie as this later leads to THIS cookie and the actual server cookie being returned 
		 * This then led to two WiggleCustomer2 settings - which becomes a problem if the settings between them differ (e.g. one says delivery destination = UK, other one says delivery destination is another country)
		 * So - commmented this out:
 
		cookieManager.setCookie(strDomain, strCustomerCookieHeaderString);
		
		*/
		cookieManager.setCookie(strDomain, strSessionCookieHeaderString);

		return headers;
		
	}

	private void tweakWebviewPerformance(){
		
		/**
		 *  Android's webview can perform poorly at times .. the below uses various strategies to optimise it. 
		 *  Most times, setting android:hardwareAccelerated="true" in Manifest does the trick. 
		 *  At other times this needs disabling (enable LAYER_TYPE_SOFTWARE below).
		 *  If this manifest setting does not help use a combination of the strategies below.
		 *  
		 */
		
		//mWebView.getSettings().setRenderPriority(RenderPriority.HIGH); //deprecated in version 18 and no longer recommended
		//mWebView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE); //only use when cache definitely not beneficial
		
		/**
		if (Build.VERSION.SDK_INT >= 11){
			mWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		}
		**/
		
	}
	
	
	public String getCookieValueByName(String strDomain,String strCookieName){     
		
		/**
		 *  The cookie is a string with semi-colon separated name value pairs e.g. 
		 *  Cookie: nomobilewarn=true; WiggleCustomer2=xyz; SecureLogin=abc
		 *  
		 *  We simply read this string for a given domain and split it after each semi-colon to get an individual cookie e.g. 
		 *  
		 *     WiggleCustomer2=xyz
		 *  
		 *  We check if that string contains our cookie name (WiggleCustomer2) - if yes, then our Cookie value then is the value after the equals to sign.
		 *  
		 */
	    CookieManager cookieManager = CookieManager.getInstance();
	    String strCookieValue = null;
	    String strAllDomainCookies = cookieManager.getCookie(strDomain);
	    
	    if(strAllDomainCookies==null){
	    	return "";
	    }
	    
	    String[] temp=strAllDomainCookies.split(";");
	    for (String strTemp : temp ){
	    	if(strTemp.contains(strCookieName)){
	    		strCookieValue = strTemp.replace(strCookieName + "=", ""); //Wiggle speciality - remove "WiggleCustomer2=" (cookiename=) from strTemp
		        return strCookieValue;
	        }
	    }  
	    
	    return strCookieValue; 
	}
	
	/*
	 *  In parameter: 
	 *  1 - the domain that is being searched for the WiggleCustomer2 cookie
	 *  2 - the URL we are intercepting as we need to reload it with adjusted international settings
	 */
	
	public void listenForCustomerCookie(String strDomain, String strURL){     
		
		class WiggleCustomer2Cookie{
			
			private String mCurrency;
			private String mDestination;
			private String mLanguage;
			private String mSiteDomainName;
			
			public WiggleCustomer2Cookie(String strCurrency, String strDestination, String strLanguage, String strSiteDomainName){
				mCurrency = strCurrency;
				mDestination = strDestination;
				mLanguage = strLanguage;
				mSiteDomainName = strSiteDomainName;
			}
			
			public String getCurrency(){
				return mCurrency;
			}
			
			public String getDestination(){
				return mDestination;
			}
			
			public String getLanguage(){
				return mLanguage;
			}
			
			public String getSiteDomain(){
				return mSiteDomainName;				
			}
			
			public String toString(){
				String strCookie = "Currency: " + getCurrency() + "; Language: " + getLanguage() + "; Destination: " + getDestination() + "; Domain: " + getSiteDomain();
				return strCookie;
			}
			
		}
		
		
		
		/**
		 *  The cookie is a string with semi-colon separated name value pairs e.g. 
		 *  Cookie: nomobilewarn=true; WiggleCustomer2=xyz; SecureLogin=abc
		 *  
		 *  We simply read this string for a given domain and split it after each semi-colon to get an individual cookie e.g. 
		 *  
		 *     WiggleCustomer2=xyz
		 *  
		 *  After certain url calls, a customer cookie is returned (opening shopping basket for instance) It can be used to track changes
		 *  of the user delivery destination, currency and language. Example (relevant information shown only):
		 *  
		 *  Cookie 2: WiggleCustomer2=CID=6204751425& .... &Cur=GBP&Dest=10&Language=en&SiteDomainName=wiggle.co.uk&ListLayout=Grid&ActualDiscountType=;  
		 *  
		 *  So we will wait for this Cookie to make an appearance and compare the values for 
		 *    Cur, Dest, Language, SiteDomainName
		 *  with our local user and adjust the local user settings where appropriate. 
		 *  
		 *  
		 */
	    CookieManager cookieManager = CookieManager.getInstance();
	    String strAllDomainCookies = cookieManager.getCookie(strDomain);
	    
	    if(strAllDomainCookies==null){
	    	return; //no cookies for that domain found? Do nothing
	    }
	    
	    
	    WiggleCustomer2Cookie customerCookie = null;
	    
	    String[] temp=strAllDomainCookies.split(";");
	    for (String strTemp : temp ){
	    	
	    	//create a WiggleCustomer2Cookie object for each time we find the WiggleCustomer2 cookie
	    	if(strTemp.contains("WiggleCustomer2")){
	    		
	    		String strParameterCurrency = "";
	    		String strParameterDestination = "";
	    		String strParameterLanguage = "";
	    		String strParameterSiteDomainName = "";
	    		
	    		String[] parameterNameValuePairs = strTemp.split("&");
	    		for(String strParameterNameValuePair : parameterNameValuePairs){
	    			
	    			//get currency
	    			if(strParameterNameValuePair.startsWith("Cur=")){
	    				strParameterCurrency = strParameterNameValuePair.replace("Cur=", "");
	    			}
	    			//get Destination
	    			if(strParameterNameValuePair.startsWith("Dest=")){
	    				strParameterDestination = strParameterNameValuePair.replace("Dest=", "");
	    			}
	    			//get Language
	    			if(strParameterNameValuePair.startsWith("Language=")){
	    				strParameterLanguage = strParameterNameValuePair.replace("Language=", "");
	    			}
	    			//get SiteDomainName
	    			if(strParameterNameValuePair.startsWith("SiteDomainName=")){
	    				strParameterSiteDomainName = strParameterNameValuePair.replace("SiteDomainName=", "");
	    			}

	    		}

    			
    			//create WiggleCustomer2 representation object so we can work with it
    			customerCookie = new WiggleCustomer2Cookie(strParameterCurrency, strParameterDestination, strParameterLanguage, strParameterSiteDomainName);
    			
	        }
	    	
	    }  
	    
	    if(customerCookie!=null){
	    	
	    	boolean blnRefreshPage = false;
	    	
	    	String strCurrency = customerCookie.getCurrency();
	    	if(strCurrency!=null){
	    		if(!strCurrency.equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_CURRENCY))){
					ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_PARAMETER_CURRENCY, strCurrency);
					blnRefreshPage = true;
				}
	    	}
	    	

	    	String strDestination = customerCookie.getDestination();
	    	if(strDestination!=null){
	    		if(!strDestination.equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_DELIVERY_DESTINATION))){
					ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_PARAMETER_DELIVERY_DESTINATION, strDestination);
					blnRefreshPage = true;
				}	    		
	    	}
	    	
	    	String strLanguage = customerCookie.getLanguage();
	    	if(strLanguage!=null){
	    		if(!strLanguage.equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_LANGUAGE))){
					ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_PARAMETER_LANGUAGE, strLanguage);
					blnRefreshPage = true;
				}	    		
	    	}
	    	
	    	String strSiteDomain = customerCookie.getSiteDomain();
	    	if(strSiteDomain!=null){
	    		//TODO: we currently trap domain changes with other method .. check if this cookie parameter can make things easier for us by tracking it
	    	}
	    	
	    
			if(blnRefreshPage){
				AppUtils.showCustomToast("Customer Cookie found! " + customerCookie.toString(), true);
				showPleaseWait(false);
				//blnForceClearHistory = true; //TODO: check for side effects. Remove if this causes issues.
				loadUrlInWebView(strURL);
			}
	    	
	    }
	    
	    

	}
	
	public void setCurrentNavigationCategory(ObjNavigationCategory category, boolean blnForceOpenUrl){
		
		this.currentNavigationCategory = category;
		
		// dont update the list if we are on a leaf node
		/**
		if(ObjNavigationCategory.CONST_CATEGORY_TYPE_SUBMENU.equals(category.getString(ObjNavigationCategory.FIELD_TYPE)) 
				&& (category.getChildren()==null || category.getChildren().size()==0)){
			return;
		}
		**/
		
		handleNavigationClick(category, blnForceOpenUrl);
		
		
	}
	
	private void handleNavigationClick(ObjNavigationCategory category, boolean blnForceOpenUrl){
		
		ImageView navigationItem_ivMore = null;
		ProgressBar navigationItem_pbInProgress = null;
		
		if(lytNativeSearch.getVisibility()==View.VISIBLE){
			displayLayout(R.id.lytWebView);
		}
		
		//hide Android native screens. But dont do this if the native screen was called from our startup activity
		if(mWebViewCaller4AndroidNativeScreen==0){
			hideAndroidNativeScreens();
		}else{
			//reset webview caller
			mWebViewCaller4AndroidNativeScreen = 0;
		}
		
		if(category==null){
			NavigationDrawerFragment.instance().updateNavigationBrowser();
			return;
		}
		
		//if this category has got a GTMID event tag - send this
		String strGtmEvent = category.getString(ObjNavigationCategory.FIELD_GOOGLE_TAG_MANAGER_ID);
		if(strGtmEvent!=null && !"".equals(strGtmEvent)){
			AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_shop), strGtmEvent);
		}
		
		View categoryAdapterView = category.getAdapterView();
		if(categoryAdapterView!=null){
			navigationItem_ivMore = (ImageView) categoryAdapterView.findViewById(R.id.navigationItem_ivMore);
			navigationItem_pbInProgress = (ProgressBar) categoryAdapterView.findViewById(R.id.navigationItem_pbInProgress);
		}
		
		//force open URL if no more children for this category ... 
		if(category.getAllChildren().size()==0	&& category.isChildrenRetrievedFromServer()){
			
			//flag this category as dead end
			if(categoryAdapterView!=null){
				navigationItem_pbInProgress.setVisibility(View.GONE);
				navigationItem_ivMore.setVisibility(View.GONE);
			}
			
			//send GTM Tag
			Map<String, Object> gtmTagsMap = new HashMap<String, Object>();
			String strCategoryName = category.getString(ObjNavigationCategory.FIELD_NAME);
			if(strCategoryName==null || "".equals(strCategoryName)){
				strCategoryName = category.getString(ObjNavigationCategory.FIELD_REFINEMENT_NAME);
			}
					
			gtmTagsMap.put("shopDept", strCategoryName);
			
			AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_shop), "shopShop", gtmTagsMap);
			strGtmEvent = "shopShop"; //make sure this tag is transmitted for UTM Tracking
			
			blnForceOpenUrl = true;
		}
		
		//only open URL if we need to (All xxx)-navigation item or category where server didn't return any more children. 
		if(blnForceOpenUrl){
			openCategoryUrl(category, strGtmEvent);
			return;
		}

		//find out category type
		String strCategoryType = category.getString(ObjNavigationCategory.FIELD_TYPE);

		if(ObjNavigationCategory.CONST_CATEGORY_TYPE_LINK.equals(strCategoryType)){

			//exception: IF user presses "Home" - clear all history from web view
			//Not doing this here led to users complaining about the "back button not working" as navigating back from the homepage
			//naturally showed all the previous deep-navigation pages again rather than closing the app
			String strGTMId = category.getString(ObjNavigationCategory.FIELD_GOOGLE_TAG_MANAGER_ID);
			//TODO: this should not rely on the GTM Tag for the Home category, but a separate tag or identifier
			if("shopHome".equals(strGTMId)){
				//make sure history is cleared after homepage is finished
				blnForceClearHistory = true;
				mCurrentNativeItem = null; //clear native screen item if exists
			}

			//all other categories - open URL associated with category
			openCategoryUrl(category, strGtmEvent);
			return; //no need to refresh navigation pane
			
		}else if(ObjNavigationCategory.CONST_CATEGORY_TYPE_MENU_ITEM.equals(strCategoryType)){
			
			if(NewsfeedActivity.getLogTag().equals(category.getString(ObjNavigationCategory.FIELD_MENU_ITEM_TARGET))) {
				startNewsfeedActivity(category, strGtmEvent);
			}else if("OrderTracking".equals(category.getString(ObjNavigationCategory.FIELD_MENU_ITEM_TARGET))){
				//close navigation drawer
				NavigationDrawerFragment.instance().onItemSelected(category);	
				//retrieveOrderHistory();
				handleAndroidNativeMenuItemClick(category);
			}
			return; //no need to refresh navigation pane
			
		}else if(ObjNavigationCategory.CONST_CATEGORY_TYPE_CATEGORIES.equals(strCategoryType)
				|| ObjNavigationCategory.CONST_CATEGORY_TYPE_WIGGLE_API.equals(strCategoryType)
				){
			
			//no children? get them from API
			if(category.getAllChildren().size()==0){
				
				//send GTM Tag
				/** only send GTM tag if a dead-end category was clicked (doesnt make sense to track the entire navigation tree) - search for tag "shopShop" in this file to see where it is sent
				Map<String, Object> gtmTagsMap = new HashMap<String, Object>();
				String strCategoryName = category.getString(ObjNavigationCategory.FIELD_NAME);
				if(strCategoryName==null || "".equals(strCategoryName)){
					strCategoryName = category.getString(ObjNavigationCategory.FIELD_REFINEMENT_NAME);
				}
				gtmTagsMap.put("shopDept", strCategoryName);
				AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_shop), "shopShop");
				**/
				
				//show progress bar
				if(categoryAdapterView!=null){
					navigationItem_ivMore.setVisibility(View.GONE);
					navigationItem_pbInProgress.setVisibility(View.VISIBLE);
				}
			
				//get additional categories from Wiggle API
				new ObjApiRequest(this, 
						null, 
						AppRequestHandler.ENDPOINT_CATEGORIES, 
						Method.GET, 
						category,
						null, 
						ON_RESPONSE_ACTION.NO_ACTION)
				.submit();
				return; //navigation pane is refreshed or not depending on result of this request //TODO: add loading spinner and handle accordingly
			
			}
			
		}
		
		
		//if we get here i.e. in any other case ... update browser for given category
		//TODO: only update browser if there are further childs to display
		NavigationDrawerFragment.instance().updateNavigationBrowser();
		
		
		

		
	}
	
	public ObjNavigationCategory getCurrentNavigationCategory(){
		return this.currentNavigationCategory;
	}
	
	
	public void loadUrlInExternalBrowser(String strTargetUrl){
		
		Bundle bundle = new Bundle();
		
		/* You can add additional header information as follows - but only do this if it can't be achieved in webview.setSettings()  */
		//bundle.putString(key, value);

		Intent externalBrowserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(strTargetUrl));
		externalBrowserIntent.putExtra(Browser.EXTRA_HEADERS, bundle);
		startActivity(externalBrowserIntent);
		
	}
	
	public void loadUrlInWebView(String strTargetUrl){

		//just in case an endpoint comes back with "login required" - save last user visited URL
		strLastUserRequestedURL = strTargetUrl;
		mWebView.loadUrl(strTargetUrl, addAdditionalHttpHeaders());
		
	}
	
	public void loadSearchResultInWebView(String strTargetUrl){
		
		hideVirtualKeyboard();
		displayLayout(R.id.lytWebView);
		loadUrlInWebView(strTargetUrl);
		
	}
	
	private void openCategoryUrl(ObjNavigationCategory category, String strGTMEvent){
		
		//get URL depending on category source / type
		String strCategoryUrl = category.getTargetUrl();

		//close navigation drawer
		NavigationDrawerFragment.instance().onItemSelected(category);	
		
		//build Wiggle shop URL
		String strTargetUrl = AppUtils.getWiggleURL(strCategoryUrl);
		
		//additional UMT tagging for all categories with a Google Tag Manager event .. amend UTM parameters to URL
		if(strGTMEvent!=null && !"".equals(strGTMEvent)){
			strTargetUrl = AppUtils.appendUtmTag(strTargetUrl, strGTMEvent);
		}
		
		
		if(category.getBoolean(ObjNavigationCategory.FIELD_OPEN_IN_EXTERNAL_BROWSER)){
			loadUrlInExternalBrowser(strTargetUrl);
		}else{
			loadUrlInWebView(strTargetUrl);
		}
		
	}
	
	private void startNewsfeedActivity(ObjNavigationCategory category, String strGTMEvent){
		
		//close navigation drawer
		if(category!=null) {
			NavigationDrawerFragment.instance().onItemSelected(category);
		}

		Intent intent = new Intent(this, NewsfeedActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		//intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION); //this makes sure activities do not slide in from right to left when started, but simply appear
		startActivityForResult(intent, 0);
		
		
	}


	private void handleLoginAccountClick(){
		
		if(ObjAppUser.instance().isUserLoggedIn()){
			AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_shop), "shopMyAccount");
			loadUrlInWebView(AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_my_account), "shopMyAccount"));
		}else{
			AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_shop), "shopSignin");
			showLoginScreen();
		}
		
		mNavigationDrawerFragment.closeDrawer();
		
	}
	
	private void handleLogoutRegisterClick(){
		
		if(ObjAppUser.instance().isUserLoggedIn()){

			boolean blnReservedLogin = "classicnavigation".equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_EMAIL)); //check before ObjAppUser is cleared

			AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_shop), "shopLogOut");
			AppSession.instance().clearUserSpecificData();
			showUserAsLoggedInOrOut();
			addAdditionalHttpHeaders(); //clear login cookie
			takeUserHome();
			//reset activity variables specific to logged in user
			mPopupNotificationText = "";

			//restart app if required
			if(blnReservedLogin){
				restartApp();
			}

		}else{
			AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_shop), "shopRegister");
			loadUrlInWebView(AppUtils.getWiggleUrlWithUTMTracking(getString(R.string.str_url_registration), "shopRegister"));
			mNavigationDrawerFragment.closeDrawer();
		}
		
	}
	
	private void showLoginScreen(){

		AppSession.instance().setInAppLogin(true);
		
		Intent intent = new Intent(this, StartupActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		intent.putExtra(StartupActivity.SHOW_LOGIN_SCREEN_ONLY, true);
		startActivityForResult(intent, 0);
		
	}
	
	private void handleLoginRequired(String strURL){
		
		strLastUserRequestedURL = getReturnURL(strURL);
		
		if(ObjAppUser.instance().getBoolean(ObjAppUser.FIELD_REMEMBER_CREDENTIALS)){
			submitSilentLogin();
		}else{

			//Samsung fix - if in-app login already in progress, return
			if(AppSession.instance().isInAppLogin()){
				return;
			}

			hidePleaseWait();
			showLoginScreen();
		}
		
	}
	
	private String getReturnURL(String strURL){
		
		//if login screen is displayed Wiggle return the original request URL in parameter "returnUrl"
		//e.g. https://www.wiggle.co.uk/secure/checkout/logon?returnUrl=%2Fsecure%2Fcheckout%2...
		//Use that return URL to redirect the user after login.
		Uri uri = Uri.parse(strURL);
		String strReturnURLParameter = uri.getQueryParameter("returnUrl");
		
		if(strReturnURLParameter==null){
			strReturnURLParameter = uri.getQueryParameter("returnurl"); //some endpoints return the parameter name in lower case ...
		}
		
		String strReturnURL = AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home_https)) + strReturnURLParameter;
		return strReturnURL;
	}
	
	private void submitSilentLogin(){
		
		AppUtils.showCustomToast("Submitting silent login ..", true);
		
		showPleaseWait(false);
		
		new ObjApiRequest(this, 
				null, 
				AppRequestHandler.getAuthenticationEndpoint(),
				Method.POST, 
				ObjAppUser.instance(), //use saved app use details
				null, 
				ON_RESPONSE_ACTION.NO_ACTION)
		.submit();		
		
	}
	
	private void extractPageTitle(String strPageTitle, String strUrl){
		
		//don't do it for URLs that we navigated to from category browsing etc. We only want page titles for deep-browsing via website links.
		if(strUrl.equals(strLastUserRequestedURL)){
			return;
		}
		
		//don't do it for the top level domain URL
		if(strUrl.equals(AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home)) + "/")){
			setActionBarTitle(getString(R.string.app_name));
			return;
		}
		
		//no page title - return
		if(strPageTitle==null || "".equals(strPageTitle)){
			return;
		}

			
				
		
		if(strPageTitle.contains("|")){
			
			//Page title is something like Wiggle | Shoes | Womens shoes ..  so get last token
			strPageTitle = strPageTitle.substring(strPageTitle.lastIndexOf("|"));
	    	if(strPageTitle!=null){
	    		strPageTitle = strPageTitle.replace("|", "");
	    		strPageTitle = strPageTitle.trim();
	    	}
    	
		}
    	
    	//no luck with page title - just use last title from navigation drawer etc.
    	if("".equals(strPageTitle)){
    		strPageTitle = mTitle.toString();
    	}
    	
    	setActionBarTitle(strPageTitle);
    	
	}
	
	private void checkForUserInternationalSettingChange(String strURL){

		//Skip this method if we are looking at a dedicated test server
		if(!"".equals(AppUtils.REDIRECT_APP_TO_TEST_SERVER)){
			return;
		}
		
		Uri uri = Uri.parse(strURL);
		String strNewHost = uri.getHost();

		String strCurrentStoreHost = ObjAppUser.instance().getString(ObjAppUser.FIELD_CURRENT_WIGGLE_STORE);
		
		//check if country domain changed
		if(!strNewHost.equals(strCurrentStoreHost)){
			
			//no need to show this dialog if an image was tapped to show a higher resolution image
			//TODO: remove the hard-coding and replace with generic mechanism .. 
			//Note: www.wigglestatic.com is the image host - tapping on a product image to enlarge it would be classed as country domain change
			//      There also are embedded youtube videos - so make that domains not containing "wiggle" are not classed as country domain change
			if(!strNewHost.contains("wiggle") || strNewHost.startsWith("www.wigglestatic.")){
				return; 
			}
			
			if(!AppUtils.isAppSupportingWiggleStore(strNewHost)){
				showUnsupportedStoreDialog(strNewHost);
				loadUrlInWebView("http://" + strCurrentStoreHost); //revert to current store
				return;
			}

			adjustAppUserToNewDomain(uri);
			
			//adjust menu panel ... (will lead to issues for non-supported countries)
			//reset local navigation tree as this needs re-building
			currentNavigationCategory = null;
			ObjNavigationCategoryManager.getInstance().setLocalNavigationTree(null);
			//reload navigation tree for selected country
			loadLocalNavigationTreeIfMissing();

			
			//get new country domain
			String strNewUserCountry = AppUtils.getCountryByTopLevelDomain(strNewHost);
			ObjAppUser.instance().setField(ObjAppUser.FIELD_COUNTRY, strNewUserCountry); //update current object
			ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_COUNTRY, strNewUserCountry); //persist change
			navigation_ivCountry.setImageDrawable(AppUtils.getCountryFlag());
			mWebView.clearHistory(); //do not allow user to navigate back after country change as this would reset country when user revisits preceding pages from previous country
			
		}else{
			
			boolean blnRefreshPage = false;
			
			//check if only currency or delivery destination changed
			String strCurrency = uri.getQueryParameter(ObjAppUser.FIELD_PARAMETER_CURRENCY);
			if(strCurrency!=null && !"".equals(strCurrency)){
				if(!strCurrency.equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_CURRENCY))){
					ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_PARAMETER_CURRENCY, strCurrency);
					blnRefreshPage = true;
				}
			}
			
			String strDeliveryDestination = uri.getQueryParameter(ObjAppUser.FIELD_PARAMETER_DELIVERY_DESTINATION);
			if(strDeliveryDestination!=null && !"".equals(strDeliveryDestination)){
				if(!strDeliveryDestination.equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_DELIVERY_DESTINATION))){
					ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_PARAMETER_DELIVERY_DESTINATION, strDeliveryDestination);
					blnRefreshPage = true;
				}
			}
			
			String strLanguage = uri.getQueryParameter(ObjAppUser.FIELD_PARAMETER_LANGUAGE);
			if(strLanguage!=null && !"".equals(strLanguage)){
				if(!strLanguage.equals(ObjAppUser.instance().getString(ObjAppUser.FIELD_PARAMETER_LANGUAGE))){
					ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_PARAMETER_LANGUAGE, strLanguage);
					blnRefreshPage = true;
				}
			}
			
			if(blnRefreshPage){
				showPleaseWait(false);
				loadUrlInWebView(strURL);
			}
			
		}
		
	}
	

	private void displayLayout(int layoutId){
		
		lytNativeSearch.setVisibility(View.GONE);
		hideAndroidNativeScreens();
		
		//highlight search item if native search displayed
		if(actionBarSearchMenuItem!=null){ //prevent NPE
			
			Drawable searchIcon = actionBarSearchMenuItem.getIcon();
			Mode mMode = Mode.SRC_ATOP; //required for drawables
			
			if(layoutId==R.id.lytNativeSearch){
				searchIcon.setColorFilter(getResources().getColor(R.color.white), mMode);
			}else{
				searchIcon.setColorFilter(getResources().getColor(R.color.orangeButton), mMode);
			}
		}
		
		//set focus into textfield on native search
		if(layoutId==R.id.lytNativeSearch){
			showVirtualKeyboard();
		}else{
			hideVirtualKeyboard();
		}
		
		//set passed view visible
		View layoutView = findViewById(layoutId);
		layoutView.setVisibility(View.VISIBLE);
		
	}
	
	private void showHideNativeSearch(){
		
		if(lytNativeSearch.getVisibility()==View.VISIBLE){
			displayLayout(R.id.lytWebView);
		}else{
			AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_shop), "shopSearch");
			displayLayout(R.id.lytNativeSearch);
		}
		
	}
	

	/* Native search handling */

	

	private final TextWatcher mTypeaheadTextWatcher = new TextWatcher() {
		
        public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
        	
        }

        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        	
        	//do nothing if text was deleted
        	if(count<=before){ //<= as sometimes counts get equal even though no change perceived for user
        		return;
        	}
        	

        	if(charSequence.length()>0){
        		
        		onKeywordTextEntered();
        		
            	if (charSequence.length() > 3){
    				
    				try {
    					
    					String strQueryKeyword = charSequence.toString().toUpperCase();
    					
    					//send GTM tag
		        		Map<String, Object> gtmTagsMap = new HashMap<String, Object>();
		        		gtmTagsMap.put("keyword", strQueryKeyword);
		        		AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_search), "searchSearch", gtmTagsMap);
		        			
    					String strGETQueryString = "s=" + URLEncoder.encode(strQueryKeyword, "UTF-8");
    					
    					new ObjApiRequest(
    							HybridActivity.instance(), 
    							null, 
    							AppRequestHandler.ENDPOINT_TYPEAHEAD, 
    							Method.GET, 
    							null, 
    							strGETQueryString, 
    							ON_RESPONSE_ACTION.NO_ACTION
    							).submit();
    					
    				} catch (UnsupportedEncodingException e) {
    					Logger.printMessage(logTag, "" + e.getMessage(), Logger.ERROR);
    				}
    				
    			}
            	
        	}else{
        		onKeywordTextCleared();
        	}      
        	
        }

        public void afterTextChanged(Editable s) {
        }
        
	};
	

	
	private void onKeywordTextEntered(){
		
		nativeSearch_ivResortTextSearchClear.setVisibility(View.VISIBLE);
		
	}
	
	private void onKeywordTextCleared(){

		nativeSearch_ivResortTextSearchClear.setVisibility(View.GONE);
		nativeSearch_etQueryString.setText("");
		
		nativeSearch_lvSearchResult.setVisibility(View.GONE);
		nativeSearch_dynamicHeader.setVisibility(View.GONE);
		
		showVirtualKeyboard();
		
	}
	
	public void updateQueryResult(List<ObjTypeaheadResponse> lstTypeaheadResponse){
		
		mUpdateTextSearchListviewTask = new UpdateTextSearchListviewTask(lstTypeaheadResponse);
		runOnUiThread(mUpdateTextSearchListviewTask);
		
	}
	

	private class UpdateTextSearchListviewTask implements Runnable {
		
		private List<ObjTypeaheadResponse> resultList;
		
		public UpdateTextSearchListviewTask(List<ObjTypeaheadResponse> resultList) {
		   this.resultList = resultList;
		}

		  
		@Override
		public void run() {
			
			
			if (resultList.size() > 0) {
				
				mTextSearchListAdapter.setListContent(resultList);

				mTextSearchListAdapter.notifyDataSetChanged();
				nativeSearch_lvSearchResult.setSelection(0); //scroll back to top on each new search result
				
				nativeSearch_lvSearchResult.setVisibility(View.VISIBLE);
				
				
			}else{
				
				nativeSearch_lvSearchResult.setVisibility(View.GONE);
				
			}
			
			
		}
		
	}	
	
	private void hideVirtualKeyboard(){
		
		InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		im.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		
	}
	
	private void setListViewScrollAdapter(){
		

		nativeSearch_lvSearchResult.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScroll(AbsListView listView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

				if (nativeSearch_lvSearchResult != null && nativeSearch_lvSearchResult.getCount() > 0) {

					int intFirstVisibleItem = nativeSearch_lvSearchResult.getFirstVisiblePosition();

					ObjTypeaheadResponse response = (ObjTypeaheadResponse) nativeSearch_lvSearchResult.getItemAtPosition(intFirstVisibleItem);

					if (response.getParentRecord() != null) {
						response = response.getParentRecord();
					}

					String strGroupHeader = response.getString(ObjTypeaheadResponse.FIELD_NAME);
					nativeSearch_dynamicHeader.setVisibility(View.VISIBLE);
					textSearch_tvGroupHeader.setText(strGroupHeader);


				} else {

					nativeSearch_dynamicHeader.setVisibility(View.GONE);

				}


			}

			@Override
			public void onScrollStateChanged(AbsListView arg0, int arg1) {
				// TODO Auto-generated method stub

			}

		});
		
	}	
	
	private void showVirtualKeyboard(){
		
		nativeSearch_etQueryString.requestFocus();
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(nativeSearch_etQueryString, InputMethodManager.SHOW_IMPLICIT);
		
	}
	
	private void displayAppVersionNumber(){
		
		PackageInfo pinfo;
		try {
			pinfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
			String strVersionLabel=pinfo.versionName;
						
			//AppUtils.showCustomToast(strVersionLabel, false);
			showVersionNumberDialog(strVersionLabel);
			
		} catch (NameNotFoundException e) {
			Logger.printMessage(logTag, e.toString(), Logger.ERROR);
		}		
		
	}

	private void showVersionNumberDialog(String strVersionLabel){

		DialogFragment reviewOrderDialog = new Dialog(this,
				getString(R.string.str_app_version_dialog_title),
				strVersionLabel,
				getString(R.string.str_ok),
				"" //OK button only
		);


		reviewOrderDialog.show(getSupportFragmentManager(), Dialog.DIALOG_APP_VERSION);

	}
	
	private void adjustAppUserToNewDomain(Uri uri){
		
		//Format of location change within the app: 
		//https://www.wiggle.es/?curr=EUR&dest=1
		//ObjAppUser.instance().setField(ObjAppUser.FIELD_IN_APP_SELECTED_WIGGLE_STORE, strHost);
		ObjAppUser newCountryUser = new ObjAppUser();
		
		String strHost = uri.getHost();
		newCountryUser.setField(ObjAppUser.FIELD_CURRENT_WIGGLE_STORE, strHost);

		String strCountry = AppUtils.getCountryByTopLevelDomain(strHost);
		newCountryUser.setField(ObjAppUser.FIELD_COUNTRY, strCountry);

		String strCurrency = uri.getQueryParameter(ObjAppUser.FIELD_PARAMETER_CURRENCY);
		newCountryUser.setField(ObjAppUser.FIELD_PARAMETER_CURRENCY, strCurrency);
		
		String strDeliveryDestination = uri.getQueryParameter(ObjAppUser.FIELD_PARAMETER_DELIVERY_DESTINATION);
		newCountryUser.setField(ObjAppUser.FIELD_PARAMETER_DELIVERY_DESTINATION, strDeliveryDestination);
		
		//unfortunately this query parameter is missing .. String strLanguage = uri.getQueryParameter(ObjAppUser.FIELD_PARAMETER_LANGUAGE);
		//so using the target domain to manually set the language. TODO: improve
		String strLanguage = AppUtils.getLanguageCodeForStore(strHost);
		newCountryUser.setField(ObjAppUser.FIELD_PARAMETER_LANGUAGE, strLanguage);
		
		//update app user and persist changes
		ObjAppUser.instance().updateAppUser(newCountryUser);
		
	}
	
	private void showUnsupportedStoreDialog(String strTargetHost){
		
		strUnsupportedStoreHost = strTargetHost;
		
		DialogFragment clearFormDialog = new Dialog(this,
				getString(R.string.str_unsupported_store_dialog_title),
				String.format(
						getString(R.string.str_unsupported_store_dialog_message),
						AppUtils.getCountryNameFromHost(strTargetHost)
						),
				String.format(
						getString(R.string.str_unsupported_store_dialog_action_confirm),
						strTargetHost
						),
				getString(R.string.str_unsupported_store_dialog_action_cancel)
			 );
		
		
		clearFormDialog.setCancelable(false); //make sure dialog can't be cancelled via back button or tapping outside it
		
		clearFormDialog.show(getSupportFragmentManager(), Dialog.DIALOG_GO_TO_UNSUPPORTED_STORE);
		
	}
	
	final class IJavascriptHandler {
		
		   IJavascriptHandler() {
		   }

		   // This annotation is required in Jelly Bean and later:
		   @JavascriptInterface
		   public void sendToAndroid(String text) {
		      // this is called from JS with passed value
		      AppUtils.showCustomToast(text, true);
		   }
		}
	
	@Override
	public void onDialogPositiveClick(DialogFragment dialog) {
		
		String strDialogTag = dialog.getTag();
		
		if(Dialog.DIALOG_GO_TO_UNSUPPORTED_STORE.equals(strDialogTag)){
			
			loadUrlInExternalBrowser("http://" + strUnsupportedStoreHost);
			strUnsupportedStoreHost = "";
			finish(); //close Wiggle app (if not doing this, do mWebView.clearHistory(); to avoid issues with back-navigation)
			
		}else if(Dialog.DIALOG_LEAVE_APP_REVIEW_AFTER_ORDER.equals(strDialogTag)){

			String strStoreUrl = AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home));

			Uri uri = Uri.parse(strStoreUrl);
			String strHost = uri.getHost();

			//Example for ES ..
			//https://www.trustpilot.com/review/www.wiggle.es
			//For UK:
			//https://www.trustpilot.com/review/www.wiggle.co.uk
			loadUrlInExternalBrowser("https://www.trustpilot.com/review/" + strHost);

			ObjAppUser.instance().updateAppUserThroughTransientUser(ObjAppUser.FIELD_POST_ORDER_TRUSTPILOTREVIEW_ACCEPTED, String.valueOf(AppUtils.getAppVersionInt()));


		}
		
	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		
		String strDialogTag = dialog.getTag();
		
		if(Dialog.DIALOG_GO_TO_UNSUPPORTED_STORE.equals(strDialogTag)){
			
			strUnsupportedStoreHost = "";
			mWebView.clearHistory(); //avoid issues with back-navigation
			
		}else if(Dialog.DIALOG_LEAVE_APP_REVIEW_AFTER_ORDER.equals(strDialogTag)){
			
			//make sure user is not asked for review multiple times in the same session
			ObjSalesOrderManager.getInstance().resetJustDeliveredOrders();
			
		}
		
		
	}
	
	private void takeUserHome(){
		
		AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_shop), "shopHome");
		
		//make sure history is cleared after homepage is finished
		blnForceClearHistory = true;
		
		mCurrentNativeItem = null; //clear native screen item if exists

		
		//imitate click on "Home" menu item - this will load appropriate URl
		setCurrentNavigationCategory(ObjNavigationCategoryManager.getInstance().getCombinedNavigationTree(HybridActivity.getLogTag()).getAllChildren().get(0), false);
		
		//set highest root category to make menu show all items again
		setCurrentNavigationCategory(ObjNavigationCategoryManager.getInstance().getCombinedNavigationTree(HybridActivity.getLogTag()), false);
		NavigationDrawerFragment.instance().updateNavigationBrowser();
		
	}
	
	public String getSearchTerm(){
		
		String strSearchTerm = nativeSearch_etQueryString.getText().toString().toUpperCase();
		return strSearchTerm;
		
	}
	
	private void loadLocalNavigationTreeIfMissing(){
		
		if(currentNavigationCategory==null){ //indicates app was in background and paused - or just starting now
			
			if(ObjNavigationCategoryManager.getInstance().getCombinedNavigationTree(HybridActivity.getLogTag())==null){
				
		        //get navigation tree from local store and make sure navigation drawer is updated afterwards
		        ObjApiRequest localFileReadRequest = 
		        new ObjApiRequest(this, 
						null, 
						AppRequestHandler.LOCAL_NAVIGATION_FILE, 
						Method.GET, 
						null,
						null, 
						ON_RESPONSE_ACTION.REFRESH_NAVIGATION_DRAWER);	        
		        AppRequestHandler.instance().getJsonObjectDataFromRawResource(localFileReadRequest);  
		        
	    	}else{
	    		
	    		//load local navigation tree
	    		setCurrentNavigationCategory(ObjNavigationCategoryManager.getInstance().getCombinedNavigationTree(HybridActivity.getLogTag()), false);
		        
	    	}
			
		}
		
	}
	
	@Override
	public void onFragmentSelfClose(String tag) {
		
		getFragmentManager().popBackStack();
		
		
	}
	
	private void executeCodeForVersionUpgrade(){
		
		final String VERSION_UPGRADE_LOG = "VERSION_UPGRADE_LOG";
		final String LAST_SUCCESSFUL_UPGRADE_TARGET_VERSION = "LAST_SUCCESSFUL_UPGRADE_TARGET_VERSION";
		
		String strLastSucessfulUpgradeTargetVersion = DataStore.getStringFromSharedPreferences(VERSION_UPGRADE_LOG, LAST_SUCCESSFUL_UPGRADE_TARGET_VERSION);
		
		//Version spefic code executed after version upgrades if required
		//First introduced with version 1.02 - any version before will return "" from shared preferences
		if(strLastSucessfulUpgradeTargetVersion.equals("")){
			
			//Version 1.02 upgrade - we need to remove all Cookies as previous versions incorrectly duplicated cookie WiggleCustomer2
			//Without clearing all cookies, the app will show a "too_many_redirects" error again during checkout
			clearAllCookies();
			
			//Job done? All upgrade relevant code executed .. log target version number in shared preferences
			String strAppVersion = AppUtils.getAppVersionNumber();
			DataStore.saveStringToSharedPreferences(VERSION_UPGRADE_LOG, LAST_SUCCESSFUL_UPGRADE_TARGET_VERSION, strAppVersion);
			
		}else if(strLastSucessfulUpgradeTargetVersion.equals("1.03")){
			
			//do some upgrade work if required .. TODO: add version numbers and handling as required
			
		}
		
		
	}
	
	private void clearAllCookies(){
		
		CookieManager cookieManager = CookieManager.getInstance();
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
		       cookieManager.removeAllCookies(new ValueCallback<Boolean>() {
		         // a callback which is executed when the cookies have been removed
		         @Override
		         public void onReceiveValue(Boolean aBoolean) {
		               Logger.printMessage(logTag, "clearAllCookies - removedAllCookies: " + aBoolean, Logger.INFO);
		         }
		       });
		}else{
			cookieManager.removeAllCookie();
		}
		
	}
	
	private void retrieveOrderHistory(){
		
		orderList_tvNoData.setText(R.string.orderList_tvNoData_pleaseWait);
		
		mWebViewCaller4AndroidNativeScreen = R.id.lytOrderTracking; //dont remove this -makes sure order tracking screen is shown
		updateOrderTrackingList();
		
		if(ObjSalesOrderManager.getInstance().size()==0){
			orderList_swipeRefreshLayout.setRefreshing(true);
			AppRequestHandler.instance().retrieveOrderData(this, ON_RESPONSE_ACTION.SUBMIT_ORDER_TRACKING_REQUEST);
		}
		
	}
	
	private void setupOrderTrackingList(){
		
		orderTrackingAdapter = new OrderTrackingListAdapter(this);
		orderList_lvOrderHistory.setAdapter(orderTrackingAdapter);
		
	}
	
	public void updateOrderTrackingList(){
		
		if(lytOrderTracking.getVisibility()==View.GONE){
			displayLayout(R.id.lytOrderTracking);
		}
		
		if(orderTrackingAdapter==null){
			return;
		}
		
		runOnUiThread(mOrderTrackingTask);
	}
	

	private class OrderTrackingTask implements Runnable {
		
		@Override
		public void run() {
			
			
			List<ObjSalesItem> orderedItemsList = ObjSalesOrderManager.getInstance().getAllOrderedItems();
			
			if(orderedItemsList.size()>0){
			
				orderTrackingAdapter.setListContent(orderedItemsList);
				orderTrackingAdapter.notifyDataSetChanged();
			
				orderList_lvOrderHistory.setVisibility(View.VISIBLE);
				orderList_tvNoData.setVisibility(View.GONE);
				
				if(AppUtils.showListviewHeaders()){
					orderList_lytGroupHeader.setVisibility(View.VISIBLE);
				}
				
				checkForNewDeliveredOrder();
				
				//check if we need to show a badge in the navigation browser
				updateOrderTrackingBadge();
				
				if(!blnSuppressScrollToStart){
					scrollToStartOfList(orderList_lvOrderHistory);
				}else{
					blnSuppressScrollToStart = false; //reset
				}
				
			}else{
				
				orderList_lvOrderHistory.setVisibility(View.GONE);
				orderList_tvNoData.setVisibility(View.VISIBLE);
				
				orderList_lytGroupHeader.setVisibility(View.GONE);
				
			}
			
			orderList_swipeRefreshLayout.setRefreshing(false);
			
		}
		
	}
	
	
	private void checkForNewDeliveredOrder(){
		
		//If there as at least one order that changed to status "Delivered" since last refresh - show Trustpilot review prompt
		if(ObjSalesOrderManager.getInstance().getJustDeliveredOrderList().size()>0){
			ObjSalesOrder salesOrder = ObjSalesOrderManager.getInstance().getJustDeliveredOrderList().get(0);
			showReviewOrderDialog(salesOrder);
		}
		
	}
	

	/**
	 * setUp swipeRefreshLayout to refresh current fragment data
	 */
	private void setupOrderSwipeToRefreshLayout(){
		
		orderList_swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

        	@Override
        	public void onRefresh() {
        		//clear order manager as we want to update it
        		ObjSalesOrderManager.getInstance().clear();
        		retrieveOrderHistory(); //refresh everything including news config
        		blnAcceptResponseIntoCacheOnSwipeRefresh = true;
        	}
        });

        
		orderList_swipeRefreshLayout.setColorSchemeResources(
				R.color.orangeButton,
				R.color.backgroundGreyDark,
				R.color.orangeButton,
				R.color.backgroundGreyDark
		);
		
	}
	
	public void onOrderedItemClick(ObjSalesItem salesItem){

		AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_my_orders), "myordersItemStatus");
		
		ObjSalesOrder parentOrder = salesItem.getParentRecord();
		//AppUtils.showCustomToast("Clicked on order Id: " + parentOrder.getString(ObjSalesOrder.FIELD_ORDER_ID), true);
		
		showOrderDetail(parentOrder, salesItem);
		
	}
	
	private void showOrderDetail(ObjSalesOrder order, ObjSalesItem salesItem){
		
		//flag this order as "viewed" (resets "updated" label)
		order.setField(ObjSalesOrder.FIELD_ORDER_NEW_INFORMATION, false);
		updateOrderTrackingBadge(); //update order counter badge
		
		mCurrentNativeItem = salesItem;
		
		String strProductImageUrl = salesItem.getImagePath("?w=600&amp;h=600&amp;a=0");
		if(strProductImageUrl!=null && !"".equals(strProductImageUrl)){
			orderDetail_ivProductImage.setVisibility(View.VISIBLE);
			PicassoSSL.with(this).load(strProductImageUrl).into(orderDetail_ivProductImage);
		}else{
			orderDetail_ivProductImage.setVisibility(View.GONE);
		}
		
		String strDispatchStatus = order.getDisplayDeliveryStatus();
		orderDetail_tvDeliveryStatusText.setText(strDispatchStatus);
		
		String strDispatchDate = order.getDisplayDeliveryDate();
		orderDetail_tvDeliveryStatusDate.setText(strDispatchDate);
		
		String strProductName = salesItem.getProductBrandAndName();
		orderDetail_tvProductBrandAndName.setText(strProductName);
		
		String strProductVariety = salesItem.getProductVarietyString();
		orderDetail_tvProductVariety.setText(strProductVariety);
		
		String strLocalProductPrice = salesItem.getString(ObjSalesItem.FIELD_LOCAL_LINE_TOTAL_FORMATTED);
		orderDetail_tvProductPrice.setText(strLocalProductPrice);
		
		String strFormattedAddress = order.getFormattedAddress();
		orderDetail_tvDeliveryAddress.setText(strFormattedAddress);
		
		String strOrderNumber = salesItem.getParentRecord().getString(ObjSalesOrder.FIELD_ORDER_ID);
		orderDetail_tvOrderNumber.setText(strOrderNumber);
		
		lytOrderDetail.setVisibility(View.VISIBLE);
		lytOrderDetail.scrollTo(0, 0); //scroll to start of list (as might have been used earlier)
		
		OrderTrackingUtils.showOrderStatus(lytOrderTrackingGraph, order);
		
		
	}
	
	private void openOrderDetailUrl(){
		
		showPleaseWait(true);
		displayLayout(R.id.lytWebView); 
		loadTargetURL(R.id.orderDetail_btnMoreDetails);
		
	}
	
	public void showReviewOrderDialog(ObjSalesOrder salesOrder){
		
		//get current app version number:
		int intCurrentAppVersion = AppUtils.getAppVersionInt();
		
		//get version number the user last agreed to leave feedback on an order
		int intLastFeedbackInVersion = ObjAppUser.instance().getInt(ObjAppUser.FIELD_POST_ORDER_TRUSTPILOTREVIEW_ACCEPTED);
		
		//let at least one version pass before we ask user to submit review again. E.g. if user submitted review
		//in version 16, we wont ask again before version 18. If user never submitted review, intCurrentAppVersion would be 
		//zero and we ask him in any case. 
		/** Enable this if you would like to ask users for further feedback in the future
		if(intLastFeedbackInVersion == 0 
				|| intCurrentAppVersion - intLastFeedbackInVersion <=1){ // 1 = skip 1 version for feedback
			return;
		}
		**/
		
		//if user already accepted the trustpilot review dialog - dont ask him again.
		if(intLastFeedbackInVersion>0){
			//Already asked for Feedback? Return .. 
			return;
		}
		
		
		DialogFragment reviewOrderDialog = new Dialog(this,
				"", //getString(R.string.str_leave_order_review_dialog_title),
//				String.format(
//						getString(R.string.str_leave_order_review_dialog_message),
//						salesOrder.getString(ObjSalesOrder.FIELD_ORDER_ID)
//						),
				getString(R.string.str_leave_order_review_dialog_message_new),
				getString(R.string.str_leave_order_review_dialog_action_confirm),
				getString(R.string.str_leave_order_review_dialog_action_cancel)
			 );
		
		
		reviewOrderDialog.show(getSupportFragmentManager(), Dialog.DIALOG_LEAVE_APP_REVIEW_AFTER_ORDER);
		
	}
	
	private boolean onBackFromAndroidNativeScreens(){
		
		/** CASE 1: Webview screen was called from Android Native screen **/
		//The base web view can be navigated to from the Android native screen (e.g. details on an order can be calles from the Order tracking screen)
		//If the web view is shown and we have indicated the use of an Android Native screen by having mCurrentNativeItem populated: 
		//- onBackPressed: 
		//  a) show native screen again
		//  b) go back to previous web view page
		if(lytOrderDetail.getVisibility()==View.GONE && mCurrentNativeItem!=null){
			
			//Android Native screen: order tracking
			if(mCurrentNativeItem instanceof ObjSalesItem){
				displayLayout(R.id.lytOrderTracking);
				lytOrderDetail.setVisibility(View.VISIBLE);
				lytOrderDetail.scrollTo(0, 0); //scroll to start of list (as might have been used earlier)
			}
			
			//For CASE 1: Add additional Android Native screens here following the above example
			
			//navigate to previous site
			if(mWebView.canGoBack()){
				mWebView.goBack();
			}
			return true;
			
		}
		
		
		/** CASE 2: Android Native screen shows details starting from a list view **/
		//Android Native screen: Order tracking Detail
		if(lytOrderDetail.getVisibility()==View.VISIBLE){
			lytOrderDetail.setVisibility(View.GONE);
			blnSuppressScrollToStart = true;
			updateOrderTrackingList(); //update list to make sure "updated" label is reset after viewing order detail
			mCurrentNativeItem=null; //clear current item
			return true;
		}//For CASE 2: Add additional Android Native screens here following the above example
			
		
		
		/** CASE 3: Android Native screen shows an overview list **/
		//Android Native screen: Order tracking Overview (List view)
		if(lytOrderTracking.getVisibility()==View.VISIBLE){
			
			//special handling for Order tracking screen: if user decides to view this screen, save json payload to cache
			//save latest API json if order tracking screen is displayed to the user (i.e. the user chose to look at the order tracking screen)
			AppSession.instance().acceptOrderTrackingPayloadIntoCache();
			
			displayLayout(R.id.lytWebView);
			mCurrentNativeItem=null; //clear current item
			return true;
		}//For CASE 3: Add additional Android Native screens here following the above example
		
		

		
		return false; //no Android Native screen handling required

		
	}
	
	private void hideAndroidNativeScreens(){
		
		
		if(lytOrderTracking.getVisibility()==View.VISIBLE){
			lytOrderTracking.setVisibility(View.GONE);
			lytOrderDetail.setVisibility(View.GONE);
			
			//special handling for Order tracking screen: if user decides to view this screen, save json payload to cache
			//save latest API json if order tracking screen is displayed to the user (i.e. the user chose to look at the order tracking screen)
			AppSession.instance().acceptOrderTrackingPayloadIntoCache();
			
		}
		
		//add any other Android native screens
		
	}

	private void handleAndroidNativeMenuItemClick(ObjNavigationCategory category){
		
		if("OrderTracking".equals(category.getString(ObjNavigationCategory.FIELD_MENU_ITEM_TARGET))){
			
			//check if user is logged in 
			if(ObjAppUser.instance().isUserLoggedIn()==false){
				AppSession.instance().setAndroidNativeCategory(category);
				showLoginScreen();
				return;
			}else{
				retrieveOrderHistory();
			}
			
		}
	
		
	}
	
	public void updateOrderTrackingBadge(){
		
		int intUpdatedOrders = ObjSalesOrderManager.getInstance().getOrdersWithNewInformation().size();
		
		NavigationDrawerFragment.instance().updateBadgeCounter("OrderTracking", intUpdatedOrders);

		//update menu to show a notification icon in action bar
		//We need to invalidate the option menu for that
		invalidateOptionsMenu();

	}
	
	private void scrollToStartOfList(final AbsListView listView){
		
		//do nothing if list not initialised (crash-fix: NPE)
		if(listView==null){
			return;
		}
		
		//needed to move this statment to a runnable as this does not work immediately after notifydatasetchanged
		//newsfeedHome_lvListView.setSelection(0);
		listView.post(new Runnable() {

			@Override
			public void run() {
				listView.setSelection(0);
			}
		});

		
	}



	public void handleMenuCategoryClick(ObjNavigationCategory category,
			boolean blnForceOpenUrl) {
		
		//user actively pressed a menu category - reset any webview caller variables
		mWebViewCaller4AndroidNativeScreen = 0;
		
		setCurrentNavigationCategory(category, blnForceOpenUrl);
		
	}

	public boolean isInternalActivityCall(){

		//reports if HybridActivity was called from News or Events activity. In this case
		//we do not want to show the navigation drawer to the user first time he opens HybridActivity
		//from these screens as it disturbs the user experience

		if(getIntent().getStringExtra(LOAD_TARGET_URL_FROM_NATIVE_SCREEN)!=null){
			return true;
		}

		return false;

	}

	private void scanForDeepLinking(){

		/*

		   Deep links can point to this app currently with the following configuration:
		   1) wiggle://native  ->   opens native app screen
		      Current supported deep links are:
		      		   •	wiggle://native/news -> NewsfeedActivity
		      		   •	wiggle://native/events -> EventsActivity
		      		   •	wiggle://native/delivery -> Order tracking screen in HybridActivity
		   2) wiggle://web -> opens Hybrid activity and opens URL slug after /web
		      For instance:
		      			•	wiggle://web/swim  -->  opens www.wiggle.<topleveldomain>/swim

		 */
		Intent intent = getIntent();
		String action = intent.getAction();
		Uri deepLinkUri = intent.getData();

		if(deepLinkUri==null){
			//nothing to do
			return;
		}else{
			onDeepLinkDetected(deepLinkUri);
		}

	}

	private void onDeepLinkDetected(Uri deepLinkUri){

		boolean blnDeepLinkIdentified = false;

		/* 1. Handle dedicated wiggle://native deep links
		*
		*     They can look just like "wiggle://native/news"
		*     or have parameters appended to it. e.g in app message deep links may
		*    look like "wiggle://native/news" in the console, but
		*     arrive here as wiggle://native/delivery?ampExternalOpen=true&ampAction=click
		*     Thus using startsWith (could use Uri.getHost etc ... but doesnt matter here)
		* */
		if(deepLinkUri.toString().startsWith("wiggle://native/news")){

			AppUtils.showCustomToast("Open Newsfeed Activity for " + deepLinkUri.toString(), true);
			blnDeepLinkIdentified = true;
			closeNativeScreensOnDeepLink(true);
			startNewsfeedActivity(null, "deepLinkNews"); //TODO: adjust GTM tag
			return;

		} else if (deepLinkUri.toString().startsWith("wiggle://native/delivery")){

			AppUtils.showCustomToast("Open Order tracking screen for " + deepLinkUri.toString(), true);
			blnDeepLinkIdentified = true;
			//Close any activities that would appear on top of our Hybrid Activity
			closeNativeScreensOnDeepLink(false); //false - do not close HybridActivity as we show order tracking here
			//create a dummy OrderTracking menu item - oops .. order tracking is not a menuItem on startup screen, but a simple button
			ObjNavigationCategory orderTrackingMenuItem = new ObjNavigationCategory();
			orderTrackingMenuItem.setField(ObjNavigationCategory.FIELD_TYPE, ObjNavigationCategory.CONST_CATEGORY_TYPE_MENU_ITEM);
			orderTrackingMenuItem.setField(ObjNavigationCategory.FIELD_MENU_ITEM_TARGET, "OrderTracking");
			orderTrackingMenuItem.setField(ObjNavigationCategory.FIELD_GOOGLE_TAG_MANAGER_ID, "shopTrackOrder"); //TODO: adjust to what is required
			handleAndroidNativeMenuItemClick(orderTrackingMenuItem);

		}

		/* 2. Handle dedicated wiggle://web deep links */
		if(!blnDeepLinkIdentified
				&& "wiggle".equals(deepLinkUri.getScheme()) && "web".equals(deepLinkUri.getHost())){

			blnDeepLinkIdentified = true;
			String strTargetUrl = AppUtils.getWiggleURL(getString(R.string.str_url_wiggle_home)) + deepLinkUri.getPath().toString();
			AppUtils.showCustomToast("Open hybrid activity for " + deepLinkUri.toString(), true);
			//Close any activities that would appear on top of our Hybrid Activity
			closeNativeScreensOnDeepLink(false); //false - do not close HybridActivity as we open URL here
			loadUrlInWebView(strTargetUrl);

		}

		/* 3. Handle other URL patterns */
		/* Not needed for now
		if(!blnDeepLinkIdentified
				&& ("wiggle".equals(deepLinkUri.getScheme()) && "sportives".equals(deepLinkUri.getHost()))
				||
				("wiggle".equals(deepLinkUri.getScheme()) && "events".equals(deepLinkUri.getHost()))
				){

			if(AppUtils.isNativeEventScreenEnabled()) {
				AppUtils.showCustomToast("Open Events Activity for " + deepLinkUri.toString(), true);
				blnDeepLinkIdentified = true;
				closeNativeScreensOnDeepLink(true);
				startEventsActivity(null, "deepLinkEvents");
				finish(); //finish Hybrid Activity when detail screen is opened to avoid multiple instances of HybridActiviy in back stack
				return;
			}

		}
		*/

		/* 4. Check if this in general is a wiggle-hosted URL - just open it in Hybrid Activity */
		if(!blnDeepLinkIdentified
				&& AppUtils.isTrustedWiggleHost(deepLinkUri.getHost().toString())){
			blnDeepLinkIdentified = true;
			//Close any activities that would appear on top of our Hybrid Activity
			closeNativeScreensOnDeepLink(false); //false - do not close HybridActivity as we open URL here
			loadUrlInWebView(deepLinkUri.toString());
		}


		//no handled deep link found? Do nothing (just added here in case post-processing is required at some point)
		if(blnDeepLinkIdentified==false){
			return;
		}

	}

	private void closeNativeScreensOnDeepLink(boolean blnCloseHybridActivity){

		//finish Hybrid Activity when detail screen is opened to avoid multiple instances of HybridActiviy in back stack
		//Only do this if user isn't currently viewing Hybrid activity or is not somewhere deep in the shop
		if(blnCloseHybridActivity) {
			closeHybridActivityOnDeepLink();
		}

		/*
		   Closing any native screens that currently exist in the activity stack
		    makes sure that deep-linking does not introduce duplicate activities to the stack
		   and all deep-link intents are correctly processed (e.g. when HybridActivity is open
		   a deep-link needs to take you to the correct target page)
		 */

		if(NewsfeedActivity.instance()!=null){
			NewsfeedActivity.instance().finish();
		}

		//TODO: add further activities here when implemented

	}

	private void closeHybridActivityOnDeepLink(){

		//do nothing if the webview has got history
		if(mWebView!=null && mWebView.canGoBack()){
			return;
		}

		//do nothing if HybridActivity is currently displayed to the user
		if(blnIsDisplayedToUser){
			return;
		}

		//we made it to here? close Hybrid activity to avoid duplicates in back stack on deep link
		finish();

	}

	@Override
	public void onNewIntent(Intent intent){

		//check new intents as well for deep links
		//If StartupActivity has already been started and deep links are pressed elsewhere they may not work otherwise
		super.onNewIntent(intent);
		setIntent(intent);
		scanForDeepLinking();

	}


	private void initialiseAlertPopup(){

		/*
		   Just as an example - this is how to show the entire "menu.xml" menu layout upon
		   pressing an action bar item in a separate popup. Note that click events need
		   separate handling as this is a popup-window now and NOT the original menu

			PopupMenu popupMenu = new PopupMenu(this, menu_notificationAlert);
			popupMenu.inflate(R.menu.main);
			popupMenu.show();
		 */

		alertPopup = new PopupWindow(this);
		popupLayout = getLayoutInflater().inflate(R.layout.menu_alert_popup_hint, null);
		alertPopup.setContentView(popupLayout);

		// Set content width and height
		// This is important on older devices / versions (e.g. Samsung S4) as otherwise popup is not shown at
		// all due to 0 size
		alertPopup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
		alertPopup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);

		// Closes the popup window when touch outside of it - when looses focus
		alertPopup.setOutsideTouchable(true);
		alertPopup.setFocusable(true); //no touch event on the popup
		//set transparent background, otherwise an ugly grey background is shown
		alertPopup.setBackgroundDrawable(new BitmapDrawable());

		menuAlertPopupHint_tvHint = (TextView) popupLayout.findViewById(R.id.menuAlertPopupHint_tvHint);
		menuAlertPopupHint_tvHint.setTypeface(AppTypefaces.instance().fontMainRegularItalic);


	}



	private void showActionBarNotificationIcon(){

		if(notificationAlertItem==null){
			return;
		}


		//initialise menu alert item
		notificationAlertItem.setVisible(false);

		int intUpdatedOrders = ObjSalesOrderManager.getInstance().getOrdersWithNewInformation().size();

		if(intUpdatedOrders>0) {

			menu_notificationAlert_tvAlertCounter.setText(String.valueOf(intUpdatedOrders));
			notificationAlertItem.setVisible(true);
			//Showing alert popup here is a problem as we want to anchor it to a menu item
			//which at this point is not even displayed yet. As a result the coordinates are
			//always 0,0
			// showAlertPopup(getString(R.string.str_alert_notification_order_status));
			//Alternative: set a variable that is checked "onResume" - if set, the popup is shown
			if(!SUPPRESS_POPUP.equals(mPopupNotificationText)) {
				mPopupNotificationText = getString(R.string.str_alert_notification_order_status);
			}
		}

	}

	private void showAlertPopupIfRequired(){

		if(!"".equals(mPopupNotificationText)
				&&!SUPPRESS_POPUP.equals(mPopupNotificationText)){
			showAlertPopup(mPopupNotificationText);
		}


	}


	private void showAlertPopup(String strAlertText){

		if(alertPopup==null) {
			initialiseAlertPopup();
		}

		//Do not show popup multiple times or when navigation drawer fragment is open
		if(alertPopup.isShowing() || mNavigationDrawerFragment.isDrawerOpen()){
			return;
		}

		//Set alert text
		menuAlertPopupHint_tvHint.setText(strAlertText);

		//Getting the position of an action bar item is not yet supported, so doing the following ..
		int[] location = new int[2];
		menu_notificationAlert.getLocationOnScreen(location);
		int locationX = location[0];
		int locationY = location[1];

		//get the size of our popup
		popupLayout.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
				View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
		int intPopupWidth = popupLayout.getMeasuredWidth();
		int intPopupHeight = popupLayout.getMeasuredHeight();

		//show the popup against the bottom of our alert menu item
		//A popup window is drawn starting from the bottom left corner, so we need to position this right
		//alertPopup.showAtLocation(menu_notificationAlert, Gravity.NO_GRAVITY, locationX - intPopupWidth, locationY);
		//This worked .. once
		//alertPopup.showAtLocation(menu_notificationAlert, Gravity.NO_GRAVITY, locationX - intPopupWidth + menu_notificationAlert.getWidth(), intPopupHeight + locationY);
		alertPopup.showAtLocation(menu_notificationAlert, Gravity.NO_GRAVITY, locationX - intPopupWidth + menu_notificationAlert.getWidth(), AppUtils.dpToPx(65));
		//alertPopup.showAsDropDown(menu_notificationAlert);

		//Popup shown? Make sure this is only shown once in this session
		mPopupNotificationText = SUPPRESS_POPUP;

		//start a countdown timer to auto-dismiss this popup after 8 seconds
		//wait 8 seconds, notify progress (onTick) in 100 ms intervals
		new CountDownTimer(8000, 100) {

			public void onTick(long millisUntilFinished) {

			}

			public void onFinish() {
				dismissAlertPopup();
			}
		}.start();

	}

	private void dismissAlertPopup(){

		if(alertPopup==null || alertPopup.isShowing()==false){
			return;
		}

		alertPopup.dismiss();
		alertPopup = null;

	}

	@Override
	public void onStart(){

		super.onStart();

		blnIsDisplayedToUser = true;


	}

	private void addOnGlobalLayoutListener(final View view){

		view.getViewTreeObserver().addOnGlobalLayoutListener(
				new ViewTreeObserver.OnGlobalLayoutListener() {

					@Override
					public void onGlobalLayout() {

						// only want to do this once
						if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
							view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
						} else {
							view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
						}

						//when layout is drawn .. check if we need to show an alert popup
						//This is the best place to do this as only here it is guaranteed that
						//the action bar menu item that serves as an anchor for our popup is actually drawn
						showAlertPopupIfRequired();

					}
				});

	}

	private void setBreadcrumbText(String strUrl){

		main_tvBreadcrumb.setVisibility(View.GONE);

	}

	private void showHttpErrorDialog(String strUrl){

		DialogFragment dialog = new Dialog(this,
				getString(R.string.hybridActivity_webview_load_error_title),
				getString(R.string.hybridActivity_webview_load_error_message),
				getString(R.string.hybridActivity_webview_load_error_ok_button).toUpperCase(),
				"" // no cancel button
		);

		dialog.show(getSupportFragmentManager(), Dialog.DIALOG_WEBVIEW_LOAD_ERROR);


	}
}
