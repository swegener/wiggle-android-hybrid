package uk.co.wiggle.hybrid.application.datamodel.objects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import android.os.Bundle;

/*
 * 
 * Note: we could have used Map<String, String>; Map<String, int> etc instead of this custom class... 
 * However this is a convenient way to store multiple data types as name-value pair in one common accessible pattern
 * and this proved useful in some areas. 
 * 
 */

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class ObjCustomNameValue extends CustomObject {
	
	public static final String OBJECT_NAME = "name_value_pair";
	
	//field names
	public static final String FIELD_NAME = "name";
	public static final String FIELD_VALUE_STRING = "value_string";
	public static final String FIELD_VALUE_INT = "value_int";
	public static final String FIELD_VALUE_DOUBLE = "value_double";
	public static final String FIELD_VALUE_BOOLEAN = "value_boolean";
	public static final String FIELD_VALUE_OBJECT = "value_object";
	
	
	private static List<String> sFieldList;
	static {
		sFieldList = Collections.synchronizedList(new ArrayList<String>());
		sFieldList.add(FIELD_NAME);
		sFieldList.add(FIELD_VALUE_STRING);		
		sFieldList.add(FIELD_VALUE_INT);
		sFieldList.add(FIELD_VALUE_BOOLEAN);
		sFieldList.add(FIELD_VALUE_OBJECT);
		sFieldList.add(FIELD_VALUE_DOUBLE);
	}

	private static ConcurrentHashMap<String, Integer> sFieldTypes;
	static {
		
		sFieldTypes = new ConcurrentHashMap<String, Integer>();
		sFieldTypes.put(FIELD_NAME, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_VALUE_STRING, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_VALUE_INT, FIELD_TYPE_INTEGER);
		sFieldTypes.put(FIELD_VALUE_BOOLEAN, FIELD_TYPE_BOOLEAN);
		sFieldTypes.put(FIELD_VALUE_OBJECT, FIELD_TYPE_OBJECT);
		sFieldTypes.put(FIELD_VALUE_DOUBLE, FIELD_TYPE_DOUBLE);

	}
	
	public ObjCustomNameValue() {
	}
	
	public ObjCustomNameValue(Bundle bundle) {
		super(bundle);
	}
	
	//constructs an object with a String-key and String-value
	public ObjCustomNameValue(String strName, String strStringValue) {
		
		setField(FIELD_NAME, strName);
		setField(FIELD_VALUE_STRING, strStringValue);
		
	}
	
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ObjCustomNameValue)) {
			return false;
		}

		ObjCustomNameValue other = (ObjCustomNameValue) o;
		String otherURI = other.getString(FIELD_NAME);
		String thisURI = this.getString(FIELD_NAME);
		return otherURI.equals(thisURI);
	}		

	@Override
	public String getObjectName() {
		return OBJECT_NAME;
	}
	
	@Override
	public List<String> getFields() {
		return sFieldList;
	}
	
	@Override
	public Class<?> getSubclass() {
		return ObjCustomNameValue.class;
	}
	
	@Override
	public Integer getFieldType(String fieldName) {
		return sFieldTypes.get(fieldName);
	}
}
