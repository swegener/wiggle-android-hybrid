package uk.co.wiggle.hybrid.extensions;

import android.app.DatePickerDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.DatePicker;
import android.widget.ScrollView;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;

/**
 *
 * @author AndroMedia
 *
    This class is used as a replacement for the standard DatePickerDialog.

    We want to show a custom title on that dialog. Using .setTitle() does not work as
    this method is used by DatePickerDialog internally to update the title to the selected date.
    We thus overwrite this method so it retains a custom title.
 */

public class CustomDatePickerDialog extends DatePickerDialog{

    public CustomDatePickerDialog(Context context, OnDateSetListener callBack, int year, int monthOfYear, int dayOfMonth) {
        super(context, callBack, year, monthOfYear, dayOfMonth);
    }

    public void setTitle(CharSequence title) {
        super.setTitle(ApplicationContextProvider.getContext().getString(R.string.claimIncidentDetails_tvIncidentDate));
    }
	
}
