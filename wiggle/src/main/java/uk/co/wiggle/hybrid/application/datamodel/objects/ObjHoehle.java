package uk.co.wiggle.hybrid.application.datamodel.objects;

import android.os.Bundle;

import net.hockeyapp.android.utils.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import io.michaelrocks.paranoid.Obfuscate;
import uk.co.wiggle.hybrid.application.utils.AppUtils;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

@Obfuscate
public class ObjHoehle extends CustomObject {

	public static final String OBJECT_NAME = "ObjHoehle";

	public static final String FIELD_AA = "ButtonColor";
	public static final String FIELD_AB = "ButtonWidth";
	public static final String FIELD_AC = "ButtonHeight";
	public static final String FIELD_AD = "TextViewColor";
	public static final String FIELD_AE = "TextViewWidth";
	public static final String FIELD_AF = "TextViewHeight";
	public static final String FIELD_AG = "EditTextColor";
	public static final String FIELD_AH = "EditTextWidth";
	public static final String FIELD_AI = "EditTextHeight";
	public static final String FIELD_AJ = "CheckboxWidth";
	public static final String FIELD_AK = "CheckboxHeight";
	public static final String FIELD_AL = "CheckboxStatus"; //holds a configurable test server URL
	public static final String FIELD_AM = "DropdownWidth"; //stores the shortcut to define a test server URL
	public static final String FIELD_AN = "DropdownHeight"; //stores the shortcut to log out from a test server
	public static final String FIELD_AO = "DropdownStyle"; //an example test server
	public static final String FIELD_AP = "ImageViewBG"; //label for the test server configuration dialog

	//App feedback email
	public static final String FIELD_AQ = "PixelHeight"; //Header string "Authorization"
	public static final String FIELD_AR = "PixelDensity"; //Header string "Basic"
	public static final String FIELD_AS = "Color5"; //App feedback email - sender
	public static final String FIELD_AT = "Color6"; //App feedback email - recipient
	public static final String FIELD_AU = "Color7"; //App feedback email - Mailgun key
	public static final String FIELD_AV = "Color8"; //App feedback email - Mailgun endpoint

	//API Calls
	public static final String FIELD_AW = "Color9"; //API requests - user agent string
	public static final String FIELD_AX = "ThemeDaylight"; //API requests - user agent header key
	public static final String FIELD_AY = "ThemeNightTime"; //API requests - auth header key
	public static final String FIELD_AZ = "ThemeDefault"; //API requests - auth header value
	public static final String FIELD_BA = "HoloTheme"; //API requests - login endpoint
	public static final String FIELD_BB = "minorVersion"; //SalesForce Marketing Cloud - MC_APP_ID
	public static final String FIELD_BC = "majorVersion"; //SalesForce Marketing Cloud - MC_ACCESS_TOKEN
	public static final String FIELD_BD = "interimVersion"; //SalesForce Marketing Cloud - MC_APP_SERVER_URL
	public static final String FIELD_BE = "cutoffVersion"; //SalesForce Marketing Cloud - Google Cloud Messaging Sender Id


	private static List<String> sFieldList;
	static {
		sFieldList = Collections.synchronizedList(new ArrayList<String>());
		sFieldList.add(FIELD_AA);
		sFieldList.add(FIELD_AB);
		sFieldList.add(FIELD_AC);
		sFieldList.add(FIELD_AD);
		sFieldList.add(FIELD_AE);
		sFieldList.add(FIELD_AF);
		sFieldList.add(FIELD_AG);
		sFieldList.add(FIELD_AH);
		sFieldList.add(FIELD_AI);
		sFieldList.add(FIELD_AJ);
		sFieldList.add(FIELD_AK);
		sFieldList.add(FIELD_AL);
		sFieldList.add(FIELD_AM);
		sFieldList.add(FIELD_AN);
		sFieldList.add(FIELD_AO);
		sFieldList.add(FIELD_AP);
		sFieldList.add(FIELD_AQ);
		sFieldList.add(FIELD_AR);
		sFieldList.add(FIELD_AS);
		sFieldList.add(FIELD_AT);
		sFieldList.add(FIELD_AU);
		sFieldList.add(FIELD_AV);
		sFieldList.add(FIELD_AW);
		sFieldList.add(FIELD_AX);
		sFieldList.add(FIELD_AY);
		sFieldList.add(FIELD_AZ);
		sFieldList.add(FIELD_BA);
		sFieldList.add(FIELD_BB);
		sFieldList.add(FIELD_BC);
		sFieldList.add(FIELD_BD);
		sFieldList.add(FIELD_BE);
	}

	private static ConcurrentHashMap<String, Integer> sFieldTypes;
	static {
		sFieldTypes = new ConcurrentHashMap<String, Integer>();

		sFieldTypes.put(FIELD_AA, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AB, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AC, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AD, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AF, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AG, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AH, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AI, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AJ, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AK, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AL, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AM, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AN, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AO, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AP, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AQ, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AR, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AS, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AT, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AU, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AV, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AW, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AX, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AY, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_AZ, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_BA, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_BB, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_BC, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_BD, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_BE, FIELD_TYPE_STRING);
	}

	public ObjHoehle() {

		setField(FIELD_AA, "M2VhNmJmNjViMzNlNGY0OGJhZDUzYWEzOWQyM2EwN2U");
		setField(FIELD_AB, "ZHJvd3NzYXAucHRtcy5saWFt");
		setField(FIELD_AC, "Z3JvLm51Z2xpYW0ucHRtcw==");
		setField(FIELD_AD, "dHNvaC5wdG1zLmxpYW0=");
		setField(FIELD_AE, "cmVzdS5wdG1zLmxpYW0=");
		setField(FIELD_AF, "dHJvcC5wdG1zLmxpYW0=");
		setField(FIELD_AG, "Nzg1");
		setField(FIELD_AH, "ZWxiYW5lLnNsdHRyYXRzLnB0bXMubGlhbQ==");
		setField(FIELD_AI, "aHR1YS5wdG1zLmxpYW0=");
		setField(FIELD_AJ, "Pm1vYy5zcHBhZWxnZ2l3QHJldHNhbXRzb3A8IHBwQSBkaW9yZG5BIGVsZ2dpVw==");
		setField(FIELD_AK, "bW9jLnNwcGFlbGdnaXdAcmV0c2FtdHNvcA==");
		setField(FIELD_AL, "UkVWUkVTX1RTRVRfT1RfUFBBX1RDRVJJREVS"); //REDIRECT_APP_TO_TEST_SERVER
		setField(FIELD_AM, "cmV2cmVTdHNlVG9UdGNlbm5vYw=="); //connectToTestServer
		setField(FIELD_AN, "cmV2cmVTdHNlVHJhZWxj"); //clearTestServer
		//setField(FIELD_AO, "bW9jLmVsZ2dpdy5nbml0c2V0ZWxpYm9tLXF3aWMvLzpwdHRo"); //http://ciwq-mobiletesting.wiggle.com
		setField(FIELD_AO, "a3Uub2MuZWxnZ2l3LjV0c3QvLzpzcHR0aA=="); //https://tst5.wiggle.co.uk
		setField(FIELD_AP, "TFJVIHJldnJlUyB0c2VUIGVuaWZlRA=="); //Define Test Server URL
		setField(FIELD_AQ, "bm9pdGF6aXJvaHR1QQ=="); //Authorization
		setField(FIELD_AR, "Y2lzYUI="); //Basic
		setField(FIELD_AS, "Pm1vYy5zcHBhZWxnZ2l3QHlscGVyb248IHBwQSBkaW9yZG5BIGVsZ2dpVw=="); //Wiggle Android App <noreply@wiggleapps.com>
		//setField(FIELD_AT, "emliLmFpZGVtb3JkbmFAbmFocGV0cw=="); //stephan@andromedia.biz (for DEV)
		//setField(FIELD_AT, "a3Uub2Muc2tyb3dkbmltQGVsZ2dpdw=="); //wiggle@mindworks.co.uk (for UAT @ mindworks)
		setField(FIELD_AT, "bW9jLmVsZ2dpd0BrY2FiZGVlZnBwYQ=="); //appfeedback@wiggle.com (production / Wiggle testing)
		setField(FIELD_AU, "NGJlMzcyOTk5MmU3ZmZiNTA1ZjYyYzhlOTJhNTQ2MzkteWVrOmlwYQ=="); //api:key-93645a29e8c26f505bff7e2999273eb4
		setField(FIELD_AV, "c2VnYXNzZW0vbW9jLnNwcGFlbGdnaXcvM3YvdGVuLm51Z2xpYW0uaXBhLy86c3B0dGg="); //https://api.mailgun.net/v3/wiggleapps.com/messages
		setField(FIELD_AW, "L2Rpb3JkbkEvcHBBZWxpYm9NIGVsZ2dpVw=="); // Wiggle MobileApp/Android/
		setField(FIELD_AX, "dG5lZ0EtcmVzVQ=="); // User-Agent
		setField(FIELD_AY, "bm9pdGF6aXJvaHR1QQ=="); // Authorization
		setField(FIELD_AZ, "YmVhNWU3ZTFkOWQ1ZTlhYjdlMDQ4ZTRhN2FlZWNkNjYgbmVrb1Q="); // Token 66dceea7a4e840e7ba9e5d9d1e7e5aeb
		setField(FIELD_BA, "ZWxpYm9tZXRhY2l0bmVodHVhL25vaXNzZXMv"); // /session/authenticatemobile

		setField(FIELD_BB, "NWQwMWU2N2I5OWY0LWRjNWEtMTBkNC1hZjUxLWMyMjI2YWIy"); //SalesForce Marketing Cloud - MC_APP_ID: 2ba6222c-15fa-4d01-a5cd-4f99b76e10d5
		setField(FIELD_BC, "eEZ6NVJMaEhNMUZ3WjgxTXVyT000TU9B"); //SalesForce Marketing Cloud - MC_ACCESS_TOKEN: AOM4MOruM18ZwF1MHhLR5zFx
		setField(FIELD_BD, "L21vYy5zaXBhZHVvbGNnbml0ZWtyYW0uZWNpdmVkLjBtN3A3Njk1cjF6N2x6N3F0MDM2bS0wNnF0Y20vLzpzcHR0aA=="); //SalesForce Marketing Cloud - MC_APP_SERVER_URL: https://mctq60-m630tq7zl7z1r5967p7m0.device.marketingcloudapis.com/
		setField(FIELD_BE, "NzM5MTQyOTk2NzI3"); //SalesForce Marketing Cloud - FCM_SENDER_ID_FOR_MC_APP: 727699241937

	}

	public ObjHoehle(Bundle bundle) {
		super(bundle);
	}
	
	@Override
	public String getObjectName() {
		return OBJECT_NAME;
	}
	
	@Override
	public List<String> getFields() {
		return sFieldList;
	}
	
	@Override
	public Class<?> getSubclass() {
		return ObjHoehle.class;
	}
	
	@Override
	public Integer getFieldType(String fieldName) {
		return sFieldTypes.get(fieldName);
	}
	
	
	public static String createJSONfromName(ObjHoehle object , CustomObject targetObject) throws JSONException{
		
		JSONArray mainArray = new JSONArray();
		List<String> fields = targetObject.getFields();
			
		JSONObject jsonObject = new JSONObject();
			
			// first do the flat fields
			for (String strFieldName : fields) {
				
				if(object.get(strFieldName) != null){
					
					int intValueFieldType = targetObject.getFieldType(strFieldName);
					
					switch (intValueFieldType) {
					
					case CustomObject.FIELD_TYPE_OBJECT:
						break;
					
					case CustomObject.FIELD_TYPE_DATETIME:
						break;
					
					default:
						jsonObject.put(strFieldName, object.get(strFieldName));
						break;
					}
					
					
				}
				
			}
			
			mainArray.put(jsonObject);
			
		
		return mainArray.toString();
		
	}
	
	public String getValue(String strFieldName){

		byte[] dataIn = Base64.decode(getString(strFieldName), Base64.DEFAULT);
		String strTmp = "";
		String strOut = "";
		try {
			strTmp = new String(dataIn, "UTF-8");
			strOut = AppUtils.reverseString(strTmp);
		}catch (Exception e){
			return "";
		}

		return strOut;

	}

}

