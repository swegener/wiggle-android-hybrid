package uk.co.wiggle.hybrid.usecase.ordertracking.helpers;


import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjSalesOrder;
import uk.co.wiggle.hybrid.application.animations.AppAnimations;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;


/**
 * 
 * @author AndroMedia
 * 
 * This class handles Date conversions
 *
 */

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class OrderTrackingUtils {

    private static String logTag = "OrderTrackingUtils";
    
    static ImageView orderTrackingGraph_ivStatusOrderedProcessing;
    static View orderTrackingGraph_vOrderedProcessingBar;
    static TextView orderTrackingGraph_tvOrderedPoint;
    static TextView orderDetail_tvStatusOrderedProcessing;
    
	static ImageView orderTrackingGraph_ivStatusProcessed;
	static View orderTrackingGraph_vProcessedBar;
	static TextView orderTrackingGraph_tvProcessingPoint;
	static TextView orderDetail_tvStatusProcessed;
	
	static ImageView orderTrackingGraph_ivStatusDispatched;
	static View orderTrackingGraph_vDispatchedBar;
	static TextView orderTrackingGraph_tvProcessedPoint;
	static TextView orderDetail_tvStatusDispatched;
	
	static ImageView orderTrackingGraph_ivStatusDelivered;
	static View orderTrackingGraph_vDeliveredBar;
	static TextView orderTrackingGraph_tvDispatchedPoint;
	static TextView orderDetail_tvStatusDelivered;
	
	static RelativeLayout orderTrackingGraph_lytOrderedProcessing;
	static RelativeLayout orderTrackingGraph_lytProcessed;
	static RelativeLayout orderTrackingGraph_lytDispatched;
	static RelativeLayout orderTrackingGraph_lytDelivered;
	
	static ObjSalesOrder mOrder;
	
	static AnimatorSet animatorSet;

    public static void showOrderStatus(View orderStatusWidget, ObjSalesOrder order){
    	
    	mOrder = order;
    	
    	//Order status widgets
    	//Ordered / Processing
    	orderTrackingGraph_ivStatusOrderedProcessing = (ImageView) orderStatusWidget.findViewById(R.id.orderTrackingGraph_ivStatusOrderedProcessing);
    	orderTrackingGraph_vOrderedProcessingBar = orderStatusWidget.findViewById(R.id.orderTrackingGraph_vOrderedProcessingBar);
    	orderTrackingGraph_tvOrderedPoint = (TextView) orderStatusWidget.findViewById(R.id.orderTrackingGraph_tvOrderedPoint);
    	orderDetail_tvStatusOrderedProcessing = (TextView) orderStatusWidget.findViewById(R.id.orderDetail_tvStatusOrderedProcessing);
    	
    	//Processed
    	orderTrackingGraph_ivStatusProcessed = (ImageView) orderStatusWidget.findViewById(R.id.orderTrackingGraph_ivStatusProcessed);
    	orderTrackingGraph_vProcessedBar =  orderStatusWidget.findViewById(R.id.orderTrackingGraph_vProcessedBar);
    	orderTrackingGraph_tvProcessingPoint = (TextView) orderStatusWidget.findViewById(R.id.orderTrackingGraph_tvProcessingPoint);
    	orderDetail_tvStatusProcessed = (TextView) orderStatusWidget.findViewById(R.id.orderDetail_tvStatusProcessed);
    	
    	//Dispatched
    	orderTrackingGraph_ivStatusDispatched = (ImageView) orderStatusWidget.findViewById(R.id.orderTrackingGraph_ivStatusDispatched);
    	orderTrackingGraph_vDispatchedBar =  orderStatusWidget.findViewById(R.id.orderTrackingGraph_vDispatchedBar);
    	orderTrackingGraph_tvProcessedPoint = (TextView) orderStatusWidget.findViewById(R.id.orderTrackingGraph_tvProcessedPoint);
    	orderDetail_tvStatusDispatched = (TextView) orderStatusWidget.findViewById(R.id.orderDetail_tvStatusDispatched);
    	
    	//Delivered
    	orderTrackingGraph_ivStatusDelivered = (ImageView) orderStatusWidget.findViewById(R.id.orderTrackingGraph_ivStatusDelivered);
    	orderTrackingGraph_vDeliveredBar =  orderStatusWidget.findViewById(R.id.orderTrackingGraph_vDeliveredBar);
    	orderTrackingGraph_tvDispatchedPoint = (TextView) orderStatusWidget.findViewById(R.id.orderTrackingGraph_tvDispatchedPoint);
    	orderDetail_tvStatusDelivered = (TextView) orderStatusWidget.findViewById(R.id.orderDetail_tvStatusDelivered);
    	
    	//animated layouts
    	orderTrackingGraph_lytOrderedProcessing = (RelativeLayout) orderStatusWidget.findViewById(R.id.orderTrackingGraph_lytOrderedProcessing);
    	orderTrackingGraph_lytProcessed = (RelativeLayout) orderStatusWidget.findViewById(R.id.orderTrackingGraph_lytProcessed);
    	orderTrackingGraph_lytDispatched = (RelativeLayout) orderStatusWidget.findViewById(R.id.orderTrackingGraph_lytDispatched);
    	orderTrackingGraph_lytDelivered = (RelativeLayout) orderStatusWidget.findViewById(R.id.orderTrackingGraph_lytDelivered);
    	
    	//stop outstanding animations
    	stopPendingAnimations();
    	
    	//setup widgets according to order status
    	setOrderStatusDelivered();
    	
    	//animate the widgets
    	startWidgetAnimations();
    	
    }
    
    private static void setTextColour(TextView textView, boolean blnActive){
    	
    	Context context = ApplicationContextProvider.getContext();
    	
    	if(blnActive){
    		textView.setTextColor(context.getResources().getColor(R.color.orangeButton));
    		textView.setTypeface(AppTypefaces.instance().fontMainBold);
    	}else{
    		textView.setTextColor(context.getResources().getColor(R.color.greySeparator));
    		textView.setTypeface(AppTypefaces.instance().fontMainRegular);
    	}
    	
    }
    
    

    
    private static void setImageColourFilter(ImageView imageView, boolean blnActive){
    	
    	Context context = ApplicationContextProvider.getContext();

		Drawable drawable = imageView.getDrawable();
		Mode mMode = Mode.SRC_ATOP; //required for drawables
		
		if(blnActive){
			drawable.setColorFilter(context.getResources().getColor(R.color.orangeButton), mMode);
		}else{
			drawable.setColorFilter(context.getResources().getColor(R.color.greyButton), mMode);
		}

    }
    
    private static void setBackgroundColor(View view, boolean blnActive){
    	
    	Context context = ApplicationContextProvider.getContext();
    	
    	if(blnActive){
			view.setBackgroundColor(context.getResources().getColor(R.color.orangeButton));
		}else{
			view.setBackgroundColor(context.getResources().getColor(R.color.greyButton));
		}
    	
    }
    
    private static void setPointBackgroundDrawable(View view, boolean blnActive){
    	
    	Context context = ApplicationContextProvider.getContext();
    	
    	if(blnActive){
			AppUtils.setSDKDependentBackground(view, context.getResources().getDrawable(R.drawable.sd_cart_counter_background));
		}else{
			AppUtils.setSDKDependentBackground(view, context.getResources().getDrawable(R.drawable.sd_circle_grey_background));
			
		}
    	
    }
    
    private static void setOrderStatusOrdered(boolean blnProcessed){
    	
    	String strLabelString = ApplicationContextProvider.getContext().getString(R.string.str_order_status_ordered);
    	
    	boolean blnProcessing = ApplicationContextProvider.getContext().getString(R.string.str_order_status_processing).equals(mOrder.getDisplayOrderStatus());
        			
    	
    	//Order processed? Set String to "Ordered" and activate all controls
    	if(blnProcessed || blnProcessing){
    		
        	setImageColourFilter(orderTrackingGraph_ivStatusOrderedProcessing, true);
        	setBackgroundColor(orderTrackingGraph_vOrderedProcessingBar, true);
        	setPointBackgroundDrawable(orderTrackingGraph_tvOrderedPoint, true);
        	setTextColour(orderDetail_tvStatusOrderedProcessing, true);
        	setPointBackgroundDrawable(orderTrackingGraph_tvProcessingPoint, true);
        	
        	//Processing? set label string and processing point
        	if(blnProcessing){
            	strLabelString = ApplicationContextProvider.getContext().getString(R.string.str_order_status_processing);
        	}
        	
        	
    	}else{
    		
    		//special handling for status "Ordered"
    		//set text to "Ordered" and only activate starting point and label
    		if(ApplicationContextProvider.getContext().getString(R.string.str_order_status_ordered).equals(mOrder.getDisplayOrderStatus())){

            	setImageColourFilter(orderTrackingGraph_ivStatusOrderedProcessing, false);
            	setBackgroundColor(orderTrackingGraph_vOrderedProcessingBar, false);
            	setPointBackgroundDrawable(orderTrackingGraph_tvOrderedPoint, true);
            	setPointBackgroundDrawable(orderTrackingGraph_tvProcessingPoint, false);
            	setTextColour(orderDetail_tvStatusOrderedProcessing, true);
        	}else{
        		
        		//any other or unknown status - deactivate all controls 
        		setImageColourFilter(orderTrackingGraph_ivStatusOrderedProcessing, false);
            	setBackgroundColor(orderTrackingGraph_vOrderedProcessingBar, false);
            	setPointBackgroundDrawable(orderTrackingGraph_tvOrderedPoint, false);
            	setPointBackgroundDrawable(orderTrackingGraph_tvProcessingPoint, false);
            	setTextColour(orderDetail_tvStatusOrderedProcessing, false);
            	
        	}
        	
    	}

    	orderDetail_tvStatusOrderedProcessing.setText(strLabelString);
    	
    }
    
    private static void setOrderStatusProcessed(boolean blnDispatched){
    	
    	boolean blnActive = false;
    	
    	if(blnDispatched){
    		blnActive = blnDispatched;
    	}else{
    		
    		if(ApplicationContextProvider.getContext().getString(R.string.str_order_status_processed).equals(mOrder.getDisplayOrderStatus())){
        		blnActive = true;
        	}
        	
    	}
    	
    	
    	setImageColourFilter(orderTrackingGraph_ivStatusProcessed, blnActive);
    	setBackgroundColor(orderTrackingGraph_vProcessedBar, blnActive);
    	setPointBackgroundDrawable(orderTrackingGraph_tvProcessedPoint, blnActive);
    	setTextColour(orderDetail_tvStatusProcessed, blnActive);
    	
    	setOrderStatusOrdered(blnActive);
    	
    }
    
    private static void setOrderStatusDispatched(boolean blnDelivered){
    	
    	boolean blnActive = false;
    	
    	if(blnDelivered){
    		blnActive = blnDelivered;
    	}else{
    		
    		if(ApplicationContextProvider.getContext().getString(R.string.str_order_status_dispatched).equals(mOrder.getDisplayOrderStatus())){
        		blnActive = true;
        	}
        	
    	}
    	
    	setImageColourFilter(orderTrackingGraph_ivStatusDispatched, blnActive);
    	setBackgroundColor(orderTrackingGraph_vDispatchedBar, blnActive);
    	setPointBackgroundDrawable(orderTrackingGraph_tvDispatchedPoint, blnActive);
    	setTextColour(orderDetail_tvStatusDispatched, blnActive);
    	
    	setOrderStatusProcessed(blnActive);
    	
    }

    private static void setOrderStatusDelivered(){
    	
    	boolean blnActive = false;
    	
    	if(ApplicationContextProvider.getContext().getString(R.string.str_order_status_delivered).equals(mOrder.getDisplayOrderStatus())){
    		blnActive = true;
    	}
    	
    	setImageColourFilter(orderTrackingGraph_ivStatusDelivered, blnActive);
    	setBackgroundColor(orderTrackingGraph_vDeliveredBar, blnActive);
    	setPointBackgroundDrawable(orderTrackingGraph_tvDispatchedPoint, blnActive);
    	setTextColour(orderDetail_tvStatusDelivered, blnActive);
    	
    	setOrderStatusDispatched(blnActive);
    	
    }

    private static void startWidgetAnimations(){
    	
    	final Context context = ApplicationContextProvider.getContext();
    	
    	long lngFadeInDuration = 500;
    	
    	ObjectAnimator fadeIn1 = ObjectAnimator.ofFloat(orderTrackingGraph_lytOrderedProcessing, View.ALPHA, 0.1f, 1f);
    	fadeIn1.setDuration(lngFadeInDuration);
    	
    	ObjectAnimator fadeIn2 = ObjectAnimator.ofFloat(orderTrackingGraph_lytProcessed, View.ALPHA, 0.1f, 1f);
    	fadeIn2.setDuration(lngFadeInDuration);
    	
    	ObjectAnimator fadeIn3 = ObjectAnimator.ofFloat(orderTrackingGraph_lytDispatched, View.ALPHA, 0.1f, 1f);
    	fadeIn3.setDuration(lngFadeInDuration);
    	
    	ObjectAnimator fadeIn4 = ObjectAnimator.ofFloat(orderTrackingGraph_lytDelivered, View.ALPHA, 0.1f, 1f);
    	fadeIn4.setDuration(lngFadeInDuration);
    	
    	
    	
    	animatorSet = new AnimatorSet();
    	animatorSet.playSequentially(fadeIn1, fadeIn2, fadeIn3, fadeIn4 );
    	animatorSet.addListener(new AnimatorListener() {
			
			@Override
			public void onAnimationStart(Animator arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animator arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animator arg0) {
				// TODO Auto-generated method stub
				
				View viewToAnimate = null;
				
				//get final view to animate as a pulse 
				if(context.getString(R.string.str_order_status_delivered).equals(mOrder.getDisplayOrderStatus())){
					viewToAnimate = orderTrackingGraph_ivStatusDelivered;
				}else if(context.getString(R.string.str_order_status_dispatched).equals(mOrder.getDisplayOrderStatus())){
					viewToAnimate = orderTrackingGraph_ivStatusDispatched;
				}else if(context.getString(R.string.str_order_status_processed).equals(mOrder.getDisplayOrderStatus())){
					viewToAnimate = orderTrackingGraph_ivStatusProcessed;
				}else if(context.getString(R.string.str_order_status_processing).equals(mOrder.getDisplayOrderStatus())){
					viewToAnimate = orderTrackingGraph_ivStatusOrderedProcessing;
				}else{
					//any other status - do nothing
					return;
				}
				
				AppAnimations.pulseAnimation(viewToAnimate, R.anim.pulse);
				
			}
			
			@Override
			public void onAnimationCancel(Animator arg0) {
				// TODO Auto-generated method stub
				
			}
		});
    	
    	animatorSet.start();
    	
    }
    
    private static void stopPendingAnimations(){
    	
    	if(animatorSet!=null){
    		animatorSet.cancel();
    	}
    	
    	//clear animations of status images
    	orderTrackingGraph_ivStatusDelivered.clearAnimation();
    	orderTrackingGraph_ivStatusDispatched.clearAnimation();
    	orderTrackingGraph_ivStatusProcessed.clearAnimation();
    	orderTrackingGraph_ivStatusOrderedProcessing.clearAnimation();
    	
    	orderTrackingGraph_lytOrderedProcessing.clearAnimation();
    	orderTrackingGraph_lytProcessed.clearAnimation();
    	orderTrackingGraph_lytDispatched.clearAnimation();
    	orderTrackingGraph_lytDelivered.clearAnimation();
    	
    	//set Alpha to start value
    	orderTrackingGraph_lytOrderedProcessing.setAlpha(0f);
    	orderTrackingGraph_lytProcessed.setAlpha(0f);
    	orderTrackingGraph_lytDispatched.setAlpha(0f);
    	orderTrackingGraph_lytDelivered.setAlpha(0f);
    	
    	
    	
    }
    
    
}
