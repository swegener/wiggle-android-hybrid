package uk.co.wiggle.hybrid.application.datamodel.objects;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;


import android.net.Uri;
import android.os.Bundle;

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class ObjNewsfeed extends CustomObject {

	public static final String OBJECT_NAME = "ObjNewsfeed";

	private List<ObjNewsfeedCategory> mCategories = new ArrayList<ObjNewsfeedCategory>();
	private List<ObjNewsfeedRSSChannel> mChannels = new ArrayList<ObjNewsfeedRSSChannel>();
	
	//fields used for categories taken from top_level_navigation.json
	public static final String FIELD_URL = "url"; // link, submenu, refinement
	public static final String FIELD_IMAGE_URL = "image_url";
	public static final String FIELD_TITLE = "title";
	public static final String FIELD_DEFAULT_ON = "default_on"; //if this feed is switched on or off as default
	
	private static List<String> sFieldList;
	static {

		sFieldList = Collections.synchronizedList(new ArrayList<String>());
		
		//fields used for categories taken from top_level_navigation.json
		sFieldList.add(FIELD_URL);
		sFieldList.add(FIELD_IMAGE_URL);
		sFieldList.add(FIELD_TITLE);
		sFieldList.add(FIELD_DEFAULT_ON);
		
		
	}

	private static ConcurrentHashMap<String, Integer> sFieldTypes;
	static {
		sFieldTypes = new ConcurrentHashMap<String, Integer>();
		
		//fields used for categories taken from top_level_navigation.json
		sFieldTypes.put(FIELD_URL, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_IMAGE_URL, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_TITLE, FIELD_TYPE_STRING);
		sFieldTypes.put(FIELD_DEFAULT_ON, FIELD_TYPE_BOOLEAN);
		
	}

	public ObjNewsfeed() {
	}

	public ObjNewsfeed(Bundle bundle) {
		super(bundle);
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ObjNewsfeed)) {
			return false;
		}

		ObjNewsfeed other = (ObjNewsfeed) o;
		String otherFeedURL = other.getString(FIELD_URL);
		String thisFeedURL = this.getString(FIELD_URL);
		return otherFeedURL.equals(thisFeedURL);
	}

	@Override
	public String getObjectName() {
		return OBJECT_NAME;
	}

	@Override
	public List<String> getFields() {
		return sFieldList;
	}

	@Override
	public Class<?> getSubclass() {
		return ObjNewsfeed.class;
	}

	@Override
	public Integer getFieldType(String fieldName) {
		return sFieldTypes.get(fieldName);
	}

	// handler for children of this object
	public List<ObjNewsfeedCategory> getCategories() {
		return mCategories;
	}
	
	public boolean setCategories(Collection<? extends ObjNewsfeedCategory> arg0) {
		mCategories.clear();
		return mCategories.addAll(arg0);
	}
	
	public List<ObjNewsfeedRSSChannel> getRSSChannels() {
		return mChannels;
	}
	
	public boolean setChannels(Collection<? extends ObjNewsfeedRSSChannel> arg0) {
		mChannels.clear();
		return mChannels.addAll(arg0);
	}
	
	public boolean setAtomFeed(ObjNewsfeedRSSChannel arg0) {
		mChannels.clear();
		return mChannels.add(arg0);
	}
	
	
	/* Other convenience methods */
	public String getBaseUrl(){
		
		String strNewsfeedUrl = getString(ObjNewsfeed.FIELD_URL);
		Uri uri = Uri.parse(strNewsfeedUrl);
		String strServerBaseUrl = uri.getScheme() + "://" + uri.getHost();
		
		return strServerBaseUrl;
		
	}
	
}