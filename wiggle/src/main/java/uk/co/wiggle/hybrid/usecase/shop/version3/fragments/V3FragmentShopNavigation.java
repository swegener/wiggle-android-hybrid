package uk.co.wiggle.hybrid.usecase.shop.version3.fragments;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;

import java.util.List;
import java.util.Random;

import androidx.fragment.app.Fragment;
import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.api.networking.AppRequestHandler;
import uk.co.wiggle.hybrid.application.animations.AppAnimations;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjMegamenuManager;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjNavigationCategoryManager;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjSalesOrderManager;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjApiRequest;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjAppUser;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjMegamenuItem;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNavigationCategory;
import uk.co.wiggle.hybrid.application.helpers.AppGlobalConstants;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.application.session.AppSession;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentSelfCloseListener;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentUI;
import uk.co.wiggle.hybrid.tagging.AppTagManager;
import uk.co.wiggle.hybrid.usecase.newsfeed.activities.NewsfeedActivity;
import uk.co.wiggle.hybrid.usecase.shop.version3.activities.V3HybridActivity;


/**
 * @author Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 *
 * @license The code of this Application is licensed to Wiggle UK Ltd.
 * The license and its ownership rights can not be transferred to a Third Party.
 *
 * @android If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 **/


public class V3FragmentShopNavigation extends Fragment implements IAppFragmentUI {

    private static final String logTag = "V3FragmentShopNavigation";

    public static String getLogTag(){
        return logTag;
    }

    LinearLayout navigationTree;
    LinearLayout navigation_lytCategoryChildren;
    LinearLayout navigation_lytBreadcrumb;
    ScrollView navigation_svBrowser;
    TextView tvBreadcrumb;

    //Account widgets
    LinearLayout navigation_lytAnonymousUser;
    Button navigation_btnLogout;
    TextView navigation_lblAppVersion;

    //Support navigation
    LinearLayout navigation_lytAccountActions;
    TextView navigation_tvSupport;
    TextView navigation_tvClose;
    ImageView navigationItem_ivAllCategories;
    RelativeLayout navigation_itemAllCategories;
    TextView navigation_tvAllCategories;
    ImageView navigationItem_ivManageAcccount;
    RelativeLayout navigation_itemL1Account;
    TextView navigation_tvAccount;
    TextView navigation_tvMyAccountOrderTrackingBadge;
    LinearLayout navigation_lytL2AccountActions;
    RelativeLayout navigation_itemL2ManageAccount;
    TextView navigation_tvManageAccount;
    RelativeLayout navigation_itemL2MyOrders;
    TextView navigation_tvMyOrders;
    TextView navigation_tvMyOrdersOrderTrackingBadge;
    RelativeLayout navigation_itemL2Wishlist;
    TextView navigation_tvWishlist;
    RelativeLayout navigation_itemL2Help;
    TextView navigation_tvHelp;
    RelativeLayout navigation_itemL2Currency;
    TextView navigation_tvCurrency;
    RelativeLayout navigation_itemL2DeliveryDestination;
    TextView navigation_tvDeliveryDestination;

    //Department navigation
    RelativeLayout navigation_lytDepartments;
    TextView navigation_tvDeparments;
    TextView navigation_lblWiggle;


    IAppFragmentSelfCloseListener selfCloseListener;


    String strCaller = "";
    private static V3FragmentShopNavigation myInstance;

    public static V3FragmentShopNavigation instance() {
        return myInstance;
    }

    //Categories
    UpdateNavigationBrowserTask mUpdateNavigationBrowserTask = new UpdateNavigationBrowserTask();
    UpdateBreadcrumbTask mUpdateBreadcrumbTask = new UpdateBreadcrumbTask();


    ObjNavigationCategory mCurrentNavigationCategory;

    public ObjNavigationCategory getCurrentNavigationCategory(){
        return mCurrentNavigationCategory;
    }

    public void clearCurrentNavigationCategory(){
        mCurrentNavigationCategory = null;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myInstance = this;


        /** Getting the arguments to the Bundle object */
        Bundle data = getArguments();

        // Get the extras
        if (!data.containsKey(AppGlobalConstants.CALLING_ACTIVITY)) {
            throw new IllegalArgumentException("The " + AppGlobalConstants.CALLING_ACTIVITY + " must be passed as extra for the intent. "
                    + "The content of the extras is " + data);
        }

        strCaller = data.getString(AppGlobalConstants.CALLING_ACTIVITY);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.v3_search_tab_layout, container, false);


        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        updateFragmentContent(view);

    }


    @Override
    public void updateFragmentContent(View view) {


        findViews(view);

        setCustomFontTypes();

        //initialise navigation browser on navigation screen
        updateNavigationBrowser();

        //Show / hide widgets for logged in users
        setAccountWidgetVisibility();


    }


    public void findViews(View view) {

        navigationTree = (LinearLayout) view.findViewById(R.id.navigationTree);
        navigation_lytCategoryChildren = (LinearLayout) view.findViewById(R.id.navigation_lytCategoryChildren);
        navigation_lytBreadcrumb = (LinearLayout) view.findViewById(R.id.navigation_lytBreadcrumb);
        navigation_svBrowser = (ScrollView) view.findViewById(R.id.navigation_svBrowser);
        tvBreadcrumb = (TextView) view.findViewById(R.id.tvBreadcrumb);

        //Account widgets
        navigation_lytAnonymousUser = (LinearLayout) view.findViewById(R.id.navigation_lytAnonymousUser);
        navigation_btnLogout = (Button) view.findViewById(R.id.navigation_btnLogout);
        navigation_lblAppVersion = (TextView) view.findViewById(R.id.navigation_lblAppVersion);
        setAppVersionLabel();

        //Support navigation
        navigation_lytAccountActions = (LinearLayout) view.findViewById(R.id.navigation_lytAccountActions);
        navigation_tvSupport = (TextView) view.findViewById(R.id.navigation_tvSupport);
        navigation_tvClose = (TextView) view.findViewById(R.id.navigation_tvClose);
        navigationItem_ivAllCategories = (ImageView) view.findViewById(R.id.navigationItem_ivAllCategories);
        navigation_itemAllCategories = (RelativeLayout) view.findViewById(R.id.navigation_itemAllCategories);
        navigation_tvAllCategories = (TextView) view.findViewById(R.id.navigation_tvAllCategories);
        navigationItem_ivManageAcccount = (ImageView) view.findViewById(R.id.navigationItem_ivManageAcccount);
        navigation_itemL1Account = (RelativeLayout) view.findViewById(R.id.navigation_itemL1Account);
        navigation_tvAccount = (TextView) view.findViewById(R.id.navigation_tvAccount);
        navigation_tvMyAccountOrderTrackingBadge = (TextView) view.findViewById(R.id.navigation_tvMyAccountOrderTrackingBadge);
        navigation_lytL2AccountActions = (LinearLayout) view.findViewById(R.id.navigation_lytL2AccountActions);
        navigation_itemL2ManageAccount = (RelativeLayout) view.findViewById(R.id.navigation_itemL2ManageAccount);
        navigation_tvManageAccount = (TextView) view.findViewById(R.id.navigation_tvManageAccount);
        navigation_itemL2MyOrders = (RelativeLayout) view.findViewById(R.id.navigation_itemL2MyOrders);
        navigation_tvMyOrders = (TextView) view.findViewById(R.id.navigation_tvMyOrders);
        navigation_tvMyOrdersOrderTrackingBadge = (TextView) view.findViewById(R.id.navigation_tvMyOrdersOrderTrackingBadge);
        navigation_itemL2Wishlist = (RelativeLayout) view.findViewById(R.id.navigation_itemL2Wishlist);
        navigation_tvWishlist = (TextView) view.findViewById(R.id.navigation_tvWishlist);
        navigation_itemL2Help = (RelativeLayout) view.findViewById(R.id.navigation_itemL2Help);
        navigation_tvHelp = (TextView) view.findViewById(R.id.navigation_tvHelp);
        navigation_itemL2Currency = (RelativeLayout) view.findViewById(R.id.navigation_itemL2Currency);
        navigation_tvCurrency = (TextView) view.findViewById(R.id.navigation_tvCurrency);
        navigation_itemL2DeliveryDestination = (RelativeLayout) view.findViewById(R.id.navigation_itemL2DeliveryDestination);
        navigation_tvDeliveryDestination = (TextView) view.findViewById(R.id.navigation_tvDeliveryDestination);

        //Department navigation
        navigation_lytDepartments = (RelativeLayout) view.findViewById(R.id.navigation_lytDepartments);
        navigation_tvDeparments = (TextView) view.findViewById(R.id.navigation_tvDeparments);
        navigation_lblWiggle = (TextView) view.findViewById(R.id.navigation_lblWiggle);

        updateWidgets();

    }

    public void updateWidgets() {

        updateOrderTrackingBadge();

    }


    private void setCustomFontTypes() {

        //Support navigation
        navigation_tvSupport.setTypeface(AppTypefaces.instance().fontMainBold);
        navigation_tvClose.setTypeface(AppTypefaces.instance().fontGoogleMaterial);
        navigation_tvAllCategories.setTypeface(AppTypefaces.instance().fontMainRegular);
        navigation_tvAccount.setTypeface(AppTypefaces.instance().fontMainRegular);
        navigation_tvMyAccountOrderTrackingBadge.setTypeface(AppTypefaces.instance().fontMainRegular);
        navigation_tvManageAccount.setTypeface(AppTypefaces.instance().fontMainRegular);
        navigation_tvMyOrders.setTypeface(AppTypefaces.instance().fontMainRegular);
        navigation_tvMyOrdersOrderTrackingBadge.setTypeface(AppTypefaces.instance().fontMainRegular);
        navigation_tvWishlist.setTypeface(AppTypefaces.instance().fontMainRegular);
        navigation_tvHelp.setTypeface(AppTypefaces.instance().fontMainRegular);
        navigation_tvCurrency.setTypeface(AppTypefaces.instance().fontMainRegular);
        navigation_tvDeliveryDestination.setTypeface(AppTypefaces.instance().fontMainRegular);
        navigation_lblAppVersion.setTypeface(AppTypefaces.instance().fontMainRegular);

        //Department navigation
        navigation_tvDeparments.setTypeface(AppTypefaces.instance().fontMainBold);
        navigation_lblWiggle.setTypeface(AppTypefaces.instance().fontMainRegular);

    }


    @Override
    public void onStart() {
        super.onStart();

        setTitleBarText();

    }

    @Override
    public void onResume() {

        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();



    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);


    }


    //Check if this is working - this is the new version of deprecated
    //public void onAttach(Activity activity)
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            selfCloseListener = (IAppFragmentSelfCloseListener) context;
        } catch (ClassCastException e) {
            throw new RuntimeException(getActivity().getClass().getName() + " must implement IAppFragmentSelfCloseListener to use this fragment", e);
        }

    }

    public void onClickEvent(int intClickedViewId) {

        switch (intClickedViewId) {

            case R.id.navigation_tvClose:
                shopFragmentCanGoBack(true);
                break;

            case R.id.navigation_itemAllCategories:
                toggleAccountActions();
                break;
            case R.id.navigation_itemL1Account:
                toggleAccountActions();
                break;

            case R.id.navigation_btnLogin:
                V3HybridActivity.instance().hideNavigationScreenIfDisplayed();
                showLoginScreen();
                break;
            case R.id.navigation_btnLogout:
                V3HybridActivity.instance().performLogout();
                scrollToTop();
                break;
            case R.id.navigation_btnRegister:
                V3HybridActivity.instance().hideNavigationScreenIfDisplayed();
                showRegistrationScreen(intClickedViewId);
                break;

            case R.id.navigation_itemL2ManageAccount:
            case R.id.navigation_itemL2MyOrders:
            case R.id.navigation_itemL2Wishlist:
                actionForLoggedInUser(intClickedViewId);
                break;

            case R.id.navigation_itemL2Currency:
            case R.id.navigation_itemL2DeliveryDestination:
            case R.id.navigation_itemL2Help:
                actionForAllUsers(intClickedViewId);
                break;

            default:
                break;

        }

    }


    private class UpdateNavigationBrowserTask implements Runnable {

        @Override
        public void run() {

            // doing this together with nav browser update as this shows the current delivery
            // destination which is relevant for the megamenu api
            setAppVersionLabel();

            ObjNavigationCategory combinedNavigationTree = ObjNavigationCategoryManager.getInstance().getCombinedNavigationTree(V3HybridActivity.getLogTag());
            //Prevent NPE
            if(combinedNavigationTree==null){
                return;
            }

            List<ObjNavigationCategory> lstMenuItems = combinedNavigationTree.getChildrenForCurrentCountry();

            if(lstMenuItems.size()>0){
                navigation_svBrowser.setVisibility(View.VISIBLE);
                updateCategoryList(lstMenuItems);
                //expand list to go to current navigation category
                //expandTreeToCurrentCategory(currentNavigationCategory);

            } else {
                navigation_svBrowser.setVisibility(View.INVISIBLE);
            }

        }

    }

    public void updateNavigationBrowser(){

        //prevent NPEs
        if(getActivity()==null){
            return;
        }

        if(ObjNavigationCategoryManager.getInstance().getLocalNavigationTree()==null){
            V3HybridActivity.instance().loadLocalNavigationTreeIfMissing();
        }

        //if we are on the 'Shop by department' level - get 00 megamenu
        if(getCurrentNavigationCategory()==null || getCurrentNavigationCategory().getCategoryLevel()==ObjNavigationCategory.CONST_NAV_BROWSER_HIDDEN_LEVELS -1) {
            ObjNavigationCategory rootCategory = ObjNavigationCategoryManager.getInstance().getCategoryByGTMID("shopShop");
            rootCategory.setField(ObjNavigationCategory.FIELD_DATA_MAP, "00");
            getMegamenu(rootCategory);
        }

        getActivity().runOnUiThread(mUpdateNavigationBrowserTask);

    }

    private void getMegamenu(ObjNavigationCategory callingCategory){

        //get random menu item if megamenu available
        ObjNavigationCategory myRequestingCategory = new ObjNavigationCategory();
        if(callingCategory!=null){
            myRequestingCategory = callingCategory;
        }else {
            myRequestingCategory.setField(ObjMegamenuItem.FIELD_DATA_MAP, "00"); //default
        }

        /*
        Random rnd = new Random();
        if(ObjMegamenuManager.getInstance().getAll().size()>0) {
            myRequestingItem = ObjMegamenuManager.getInstance().get(rnd.nextInt(ObjMegamenuManager.getInstance().getAll().size()));
        }
        */


        new ObjApiRequest(
                V3HybridActivity.instance(),
                null,
                AppRequestHandler.ENDPOINT_MEGAMENU,
                Request.Method.GET,
                myRequestingCategory,
                null,
                AppRequestHandler.ON_RESPONSE_ACTION.NO_ACTION
        ).submit();


    }

    private class UpdateBreadcrumbTask implements Runnable {

        @Override
        public void run() {

            updateBreadcrumbList();

        }

    }

    private void updateBreadcrumbList(){

        //remove all drawables first if any already available ..
        if (navigation_lytBreadcrumb.getChildCount() > 0) {
            AppUtils.unbindDrawables(navigation_lytBreadcrumb);
        }


        //Add current breadcrumb list
        if(getCurrentNavigationCategory()!=null && getCurrentNavigationCategory().getCategoryLevel()>ObjNavigationCategory.CONST_NAV_BROWSER_HIDDEN_LEVELS){

            //Add an "All Categories" item
            ObjNavigationCategory allCategory = new ObjNavigationCategory();
            allCategory.setField(ObjNavigationCategory.FIELD_TYPE, ObjNavigationCategory.CONST_CATEGORY_TYPE_SUBMENU);
            allCategory.setField(ObjNavigationCategory.FIELD_TITLE, getString(R.string.str_navigation_all_categories));
            //allCategory.setParent(getCurrentNavigationCategory().getBreadcrumb().get(0));
            addSingleChild(allCategory, navigation_lytBreadcrumb, false, true);
            allCategory.setHighlighted(true);

            List<ObjNavigationCategory> breadcrumbList = getCurrentNavigationCategory().getBreadcrumb();
            //Dismiss first two items (Wiggle / Shop by department)
            for(int i=breadcrumbList.size()-1-ObjNavigationCategory.CONST_NAV_BROWSER_HIDDEN_LEVELS;i>=0;i--){
                ObjNavigationCategory breadcrumbCategory = breadcrumbList.get(i);
                addSingleChild(breadcrumbCategory, navigation_lytBreadcrumb, false, true);
                breadcrumbCategory.setHighlighted(true);
            }
        }


    }

    private void updateCategoryList(List<ObjNavigationCategory> lstMenuItems){

        //remove all drawables first if any already available ..
        if (navigation_lytCategoryChildren.getChildCount() > 0) {
            AppUtils.unbindDrawables(navigation_lytCategoryChildren);
        }


        //Show level 1 only when we are on the 'Shop by department' category
        if(getCurrentNavigationCategory()==null || getCurrentNavigationCategory().getCategoryLevel()==ObjNavigationCategory.CONST_NAV_BROWSER_HIDDEN_LEVELS) {

            if(getCurrentNavigationCategory()!=null) {
                //Show child categories of root
                List<ObjNavigationCategory> lstTopLevelCategories = getCurrentNavigationCategory().getAllChildren();
                for (int i = 0; i < lstTopLevelCategories.size(); i++) {
                    ObjNavigationCategory category = lstTopLevelCategories.get(i);
                    addLevelOneCategory(category);
                }
            }


            //Show contentful / raw menu items
            /* Part of the above, no longer needed ..
            for (int i = 0; i < lstMenuItems.size(); i++) {

                ObjNavigationCategory category = lstMenuItems.get(i);
                addLevelOneCategory(category);

            }
            */

            showSupportMenu(); //on level 1 - also show support menu

        }else{
            //If we click one of the 'deeper' categories - hide support menu
            hideSupportMenu();
        }

        AppAnimations.getInstance().expand(navigation_lytCategoryChildren);

    }


    private void addLevelOneCategory(final ObjNavigationCategory category){


        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final LinearLayout levelOneLayout = (LinearLayout) inflater.inflate(R.layout.v3_nav_browser_category, null);
        //store this view against the category so we can access it later to update the progress bar etc
        category.setAdapterView(levelOneLayout);

        TextView categoryText = (TextView) levelOneLayout.findViewById(R.id.navBrowserCategory_tvLevelOneButton);
        categoryText.setVisibility(View.VISIBLE);

        categoryText.setTypeface(AppTypefaces.instance().fontMainRegular);
        categoryText.setText(category.getDisplayName());

        View tab1 = levelOneLayout.findViewById(R.id.navBrowserCategory_tab1);
        tab1.setVisibility(View.VISIBLE);

        ImageView chevron = (ImageView) levelOneLayout.findViewById(R.id.navigationItem_ivMore);
        //hide chevron for link categories etc
        category.showMoreChildrenIndicator(chevron);

        final LinearLayout levelOneButton = (LinearLayout) levelOneLayout.findViewById(R.id.navBrowserCategory_button);
        levelOneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                handleMenuCategoryClick(category, false, false);
            }
        });

        //Tag the layout for this category (only used for debugging)
        levelOneLayout.setTag(category);

        //initialise open/close state
        //initialiseVtFAQTopicVisibility(faqTopicLayout, topic);

        navigation_lytCategoryChildren.addView(levelOneLayout);

        //LinearLayout lytFAQDetails = (LinearLayout) faqTopicLayout.findViewById(R.id.insuranceFAQCategoryButton_lytAllFAQs);
        //addVtQAPairs(lytFAQDetails, topic, lstQuestionAnswerPairs);

    }



    private void addCategoryChildren(final ObjNavigationCategory category){


        //Get child container from category adapter view
        View parentView = category.getAdapterView();

        final LinearLayout navigationItem_lytChildren = (LinearLayout) parentView.findViewById(R.id.navigationItem_lytChildren);

        //tag this container with the parent category (only used for debugging)
        navigationItem_lytChildren.setTag(category);

        //set layout params to cover content. This is done in code to ensure animations will work first time
        navigationItem_lytChildren.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        //remove all drawables first if any already available ..
        if (navigationItem_lytChildren.getChildCount() > 0) {
            AppUtils.unbindDrawables(navigationItem_lytChildren);
        }

        //Sort children by display name - use sorted list of children for display purposes
        //List<ObjNavigationCategory> lstCategoryChildren = category.getSortedChildren();
        //Wiggle requirement 07/08/2018: show categories as returned by the server - no alphabetic ordering
        List<ObjNavigationCategory> lstCategoryChildren = category.getAllChildren();

        //If this category has a list of children, then add "All of this category" child at the top of this list
        if(lstCategoryChildren.size()>0) {

            //the Parent category is already in this list as the parent level, so we cannot add it again
            //For the "All"-category, we hence create a copy of the parent and add that copy.
            ObjNavigationCategory allCategory = new ObjNavigationCategory();
            //copy fields from parent
            allCategory = category.createCopy(allCategory);

            //We adjust the category level so it will work correctly with our breadcrumb
            //We do this by setting its parent to itself
            allCategory.setParent(category);

            String strCategoryName = category.getDisplayName();
            //String strAllCategoryName = String.format(getString(R.string.str_all_of_this_category), strCategoryName);
            String strAllCategoryName = getString(R.string.str_all_of_this_category_view_all);
            //create "Category (All)" text
            allCategory.setField(ObjNavigationCategory.FIELD_NAME, strAllCategoryName);
            allCategory.setField(ObjNavigationCategory.FIELD_REFINEMENT_NAME, strAllCategoryName);
            //also send a separate GTM tag. This will also help to differentiate this category from the original parent (equals() no longer true)
            String strGTMTag = allCategory.getString(ObjNavigationCategory.FIELD_GOOGLE_TAG_MANAGER_ID);
            if(strGTMTag==null || "".equals(strGTMTag)){
                strGTMTag = strCategoryName; //No GTM Tag? Use category name.
            }
            allCategory.setField(ObjNavigationCategory.FIELD_GOOGLE_TAG_MANAGER_ID, strGTMTag + "All"); //TODO: adjust "All" category tag to Wiggle specification
            //flag this as "All of" category
            allCategory.setIsAllOfCategory();

            String strCategoryType = allCategory.getString(ObjNavigationCategory.FIELD_TYPE);
            if (ObjNavigationCategory.CONST_CATEGORY_TYPE_CATEGORIES.equals(strCategoryType)
                    || ObjNavigationCategory.CONST_CATEGORY_TYPE_WIGGLE_API.equals(strCategoryType)
                    || ObjNavigationCategory.CONST_CATEGORY_TYPE_MEGAMENU_API.equals(strCategoryType)
                    || ObjNavigationCategory.CONST_CATEGORY_TYPE_SUBMENU.equals(strCategoryType)
                    || ObjNavigationCategory.CONST_CATEGORY_TYPE_END_CATEGORY_OPEN_URL.equals(strCategoryType)
            ) {
                addSingleChild(allCategory, navigationItem_lytChildren, true, false);
            }
        }

        //add the individual children
        for(int i=0;i<lstCategoryChildren.size();i++){
            ObjNavigationCategory singleCategory = lstCategoryChildren.get(i);
            addSingleChild(singleCategory, navigationItem_lytChildren, false, false);
        }



        category.toggleChildVisibility();

    }



    private void addSingleChild(final ObjNavigationCategory category, final LinearLayout childContainer, final boolean blnAllOfThisCategoryItem, final boolean blnIsBreadcrumb){

        //check if this category is already displayed in this layout. If yes, return...
        if(childContainer.findViewWithTag(category)!=null){
            return;
        }

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final LinearLayout currentLevelLayout = (LinearLayout) inflater.inflate(R.layout.v3_nav_browser_category, null);
        //store this view agains the category so we can access it later to update the progress bar etc
        category.setAdapterView(currentLevelLayout);

        TextView categoryText = (TextView) currentLevelLayout.findViewById(R.id.navBrowserCategory_tvLevelOneButton);
        categoryText.setText(category.getDisplayName());
        //Exception: "All-of-this category" to be named "View All"
        if(blnAllOfThisCategoryItem){
            categoryText.setText(getString(R.string.str_all_of_this_category_view_all));
        }
        categoryText.setVisibility(View.VISIBLE);

        View tab1 = currentLevelLayout.findViewById(R.id.navBrowserCategory_tab1);
        View tab2 = currentLevelLayout.findViewById(R.id.navBrowserCategory_tab2);
        View tab3 = currentLevelLayout.findViewById(R.id.navBrowserCategory_tab3);
        ImageView chevron = (ImageView) currentLevelLayout.findViewById(R.id.navigationItem_ivMore);
        //hide chevron for link categories etc
        category.showMoreChildrenIndicator(chevron);


        //Get category level and format text accordingly
        int intCategoryLevel = category.getCategoryLevel();

        //Adjust indent dynamically
        tab1.setVisibility(View.VISIBLE);
        int intIndentPerLevel = 10;
        int indentWidth = AppUtils.dpToPx(getResources(), intIndentPerLevel) + (intCategoryLevel - ObjNavigationCategory.CONST_NAV_BROWSER_HIDDEN_LEVELS) * AppUtils.dpToPx(getResources(), intIndentPerLevel);
        //Exception - "All" category needs to be shifted to be in line with the other categories on the same level
        if(blnAllOfThisCategoryItem){
            indentWidth = indentWidth - AppUtils.dpToPx(getResources(), intIndentPerLevel);
        }

        //Handle "All categories"
        if(ObjNavigationCategory.CONST_CATEGORY_TYPE_SUBMENU.equals(category.getString(ObjNavigationCategory.FIELD_TYPE))
                && getString(R.string.str_navigation_all_categories).equals(category.getString(ObjNavigationCategory.FIELD_TITLE))
        ) {
            indentWidth = AppUtils.dpToPx(getResources(), intIndentPerLevel);
        }

        tab1.getLayoutParams().width = indentWidth;
        tab1.requestLayout();

        switch (intCategoryLevel) {

            //Level 3 API Category - bold text, one tab indent
            //Level 1 - Wiggle Home (root), Level 2 - Shop by Department, Level 3 - Parent Category (Cycle, Run, Swim, Tri)
            //Level 4 is the first API level
            /*
            case ObjNavigationCategory.CONST_NAV_BROWSER_LEVEL_1:
                categoryText.setTypeface(AppTypefaces.instance().fontMainRegular);
                chevron.setVisibility(View.VISIBLE);
                break;

            case ObjNavigationCategory.CONST_NAV_BROWSER_LEVEL_2:
                categoryText.setTypeface(AppTypefaces.instance().fontMainRegular);
                chevron.setVisibility(View.VISIBLE);
                break;

            case ObjNavigationCategory.CONST_NAV_BROWSER_LEVEL_3:
                categoryText.setTypeface(AppTypefaces.instance().fontMainRegular);
                chevron.setVisibility(View.GONE);
                break;

             */
            default:
                categoryText.setTypeface(AppTypefaces.instance().fontMainRegular);
                //Hide chevron for "All categories"
                if(ObjNavigationCategory.CONST_CATEGORY_TYPE_SUBMENU.equals(category.getString(ObjNavigationCategory.FIELD_TYPE))
                        && getString(R.string.str_navigation_all_categories).equals(category.getString(ObjNavigationCategory.FIELD_TITLE))
                ) {
                    //chevron.setVisibility(View.GONE);
                    //Wiggle: show Chevron for 'All Categories' too
                    chevron.setVisibility(View.VISIBLE);
                }else {
                    //No chevron for 'category (All)' item
                    if(blnAllOfThisCategoryItem){
                        chevron.setVisibility(View.GONE);
                        //bold text for this item
                        categoryText.setTypeface(AppTypefaces.instance().fontMainBold);
                    }else {
                        //chevron.setVisibility(View.VISIBLE);
                        category.showMoreChildrenIndicator(chevron);
                    }
                }
                break;

        }


        final LinearLayout categoryButton = (LinearLayout) currentLevelLayout.findViewById(R.id.navBrowserCategory_button);
        //children - button has no background
        //categoryButton.setBackground(null);



        //so we can use this in the inner class below
        final int intFinalLevel = intCategoryLevel;

        categoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //force open URL when
                //a) this is an "All of this category" item
                //b) we have exceeded 3 taps on the navigation browser (i.e. Tap on Level 5 should open URL and not retrieve further child categories)
                //boolean blnForceOpenUrl = blnAllOfThisCategoryItem || intFinalLevel >= 5;
                //handleMenuCategoryClick(category, blnForceOpenUrl);
                //2019.2 - navigate the entire tree
                handleMenuCategoryClick(category, blnAllOfThisCategoryItem, blnIsBreadcrumb);
            }
        });



        //initialise open/close state
        //initialiseVtFAQTopicVisibility(faqTopicLayout, topic);
        childContainer.addView(currentLevelLayout);

    }




    public void handleMenuCategoryClick(ObjNavigationCategory category,
                                        boolean blnForceOpenUrl,
                                        boolean blnIsBreadcrumClick) {

        if(category==null){
            return;
        }

        onNavigationCategoryClicked(category, blnForceOpenUrl, blnIsBreadcrumClick);

    }

    //For manual clicks
    private void onNavigationCategoryClicked(ObjNavigationCategory category, boolean blnForceOpenUrl, boolean blnIsBreadcrumClick){


        //Is this a breadcrumb click? Handle back navigation through the category tree
        if(getCurrentNavigationCategory()!=null && blnIsBreadcrumClick) {

            /*
                 Only do the following for "isBreadcrumb()" categories
                 a) Navigation deeper - new category level is deeper than current breadcrumb
                    --> Open that chosen category (setCurrentNavigationCategory)
                 b) Navigation back
                    b1) new category level is the same as current breadcrumb
                        --> navigate back by opening its parent (!)
                    b2) new category level is above current breadcrumb
                        --> navigate back by opening the chosen breadcrumb category (setCurrentNavigationCategory)
             */


            /* Navigate to parent level if
               a) we are on level 1
               b) we are on deepest possible level for a branch
               .. otherwise navigate to clicked level
             */
            int intNewCategoryLevel = category.getCategoryLevel();
            int intCurrentCategoryLevel = getCurrentNavigationCategory().getBreadcrumb().get(0).getCategoryLevel();
            if((intCurrentCategoryLevel - intNewCategoryLevel == 0
                    || ((intCurrentCategoryLevel - intNewCategoryLevel ==1) && getCurrentNavigationCategory().isConfirmedDeadEnd())) //category tree leaf
                    || ((intCurrentCategoryLevel - intNewCategoryLevel ==2) && getCurrentNavigationCategory().isAllOfCategory())
                ){


                    ObjNavigationCategory parentCategory = category.getParent();
                    setCurrentNavigationCategory(parentCategory, false);
                    return;


            }
        }

        setCurrentNavigationCategory(category, blnForceOpenUrl);

    }


    //For automated clicks / callbacks
    public void setCurrentNavigationCategory(ObjNavigationCategory category, boolean blnForceOpenUrl){


        /*
        //remove highlight from "current" category if required
        if(mCurrentNavigationCategory!=null){
            mCurrentNavigationCategory.setHighlighted(false);
        }

         */


        //highlight current category if required
        /*
        if(mCurrentNavigationCategory!=null) {
            mCurrentNavigationCategory.setHighlighted(true);
        }
        */


        //do nothing if this category is a menu item or URL only ..
        if(category!=null && !category.isStaticCategory()){

            //.. otherwise - save category and adjust breadcrumb and navigation tree
            mCurrentNavigationCategory = category;
            if(category.getCategoryLevel()==ObjNavigationCategory.CONST_NAV_BROWSER_HIDDEN_LEVELS
                ||
                category.isChildrenRetrievedFromServer() && !category.isConfirmedDeadEnd()) {
                updateBreadcrumbList();
                updateNavigationBrowser();
            }

        }


        handleNavigationClick(category, blnForceOpenUrl);



    }

    public void handleNavigationClick(ObjNavigationCategory category, boolean blnForceOpenUrl){

        ImageView navigationItem_ivMore = null;
        ProgressBar navigationItem_pbInProgress = null;

        if(category==null){
            return;
        }

        String strBreadcrumb = category.getBreadcrumbString();
        tvBreadcrumb.setText(strBreadcrumb);

        String strGtmEvent = category.getString(ObjNavigationCategory.FIELD_GOOGLE_TAG_MANAGER_ID);
        //find out category type
        String strCategoryType = category.getString(ObjNavigationCategory.FIELD_TYPE);


        View categoryAdapterView = category.getAdapterView();
        if(categoryAdapterView!=null){
            navigationItem_ivMore = (ImageView) categoryAdapterView.findViewById(R.id.navigationItem_ivMore);
            navigationItem_pbInProgress = (ProgressBar) categoryAdapterView.findViewById(R.id.navigationItem_pbInProgress);
        }


        //Handle "All categories"
        if(ObjNavigationCategory.CONST_CATEGORY_TYPE_SUBMENU.equals(category.getString(ObjNavigationCategory.FIELD_TYPE))
            && getString(R.string.str_navigation_all_categories).equals(category.getString(ObjNavigationCategory.FIELD_TITLE))
            ) {
            mCurrentNavigationCategory = null;
            updateBreadcrumbList();
            updateNavigationBrowser();
            return;
        }

        //force open URL if no more children for this category ...
        if(category.isConfirmedDeadEnd()){

            //flag this category as dead end
            if(categoryAdapterView!=null){
                if(navigationItem_pbInProgress!=null) {
                    navigationItem_pbInProgress.setVisibility(View.GONE);
                }
                if(navigationItem_ivMore!=null) {
                    navigationItem_ivMore.setVisibility(View.GONE); //APP-245
                }
            }

            strGtmEvent = "shopShop"; //make sure this tag is transmitted for UTM Tracking

            blnForceOpenUrl = true;

        }

        //only open URL if we need to (All xxx)-navigation item or category where server didn't return any more children.
        if(blnForceOpenUrl){

            V3HybridActivity.instance().openCategoryUrl(category, strGtmEvent);
            return;
        }


        //If we make it to here - check if this category is a parent that has got children that we already know about
        if(categoryAdapterView!=null){
            if(category.isChildrenRetrievedFromServer()) {
                navigationItem_pbInProgress.setVisibility(View.GONE);
            }
            //only show chevron for certain levels on the nav tree (middle layer)
            /*
            int intCategoryLevel = category.getCategoryLevel();

            if(intCategoryLevel==ObjNavigationCategory.CONST_NAV_BROWSER_LEVEL_2) {
                navigationItem_ivMore.setVisibility(View.VISIBLE);
            }else{
                navigationItem_ivMore.setVisibility(View.GONE);
            }
             */
        }

        //if we have children available for this category and no URL to force-open yet, display them
        if(category.getAllChildren().size()>0){

            //Reveal categories - send corresponding event
            String strCategoryTag = category.getVersion2GATag();
            String strCategoryTier = category.getVersion2GATagLevel();
            AppTagManager.instance().sendV2EventTags("tabclick","App - Navigation",strCategoryTier,strCategoryTag);

            //stability fix ..
            if(category.countChildViews()==-1) {
                //just reset the nav browser again
                updateNavigationBrowser();
            }else if(category.countChildViews()==0){
                addCategoryChildren(category);
            }else{
                category.toggleChildVisibility();
            }


            /*
            if(category.countChildViews()==0){
                category.displayChildrenOnNavigationBrowser();
            }else{
                category.toggleChildVisibility();
            }
            */

            return;


        }



        if(ObjNavigationCategory.CONST_CATEGORY_TYPE_LINK.equals(strCategoryType)){

            //exception: IF user presses "Home" - clear all history from web view
            //Not doing this here led to users complaining about the "back button not working" as navigating back from the homepage
            //naturally showed all the previous deep-navigation pages again rather than closing the app
            String strGTMId = category.getString(ObjNavigationCategory.FIELD_GOOGLE_TAG_MANAGER_ID);
            //TODO: this should not rely on the GTM Tag for the Home category, but a separate tag or identifier
            if("shopHome".equals(strGTMId)){
                //make sure history is cleared after homepage is finished
                V3HybridActivity.instance().setForceClearHistory(true);
            }

            //all other categories - open URL associated with category
            V3HybridActivity.instance().openCategoryUrl(category, strGtmEvent);
            return; //no need to refresh navigation pane

        }else if(ObjNavigationCategory.CONST_CATEGORY_TYPE_MENU_ITEM.equals(strCategoryType)){

            if(NewsfeedActivity.getLogTag().equals(category.getString(ObjNavigationCategory.FIELD_MENU_ITEM_TARGET))) {
                V3HybridActivity.instance().startNewsfeedActivity(category, strGtmEvent);
            }else if("OrderTracking".equals(category.getString(ObjNavigationCategory.FIELD_MENU_ITEM_TARGET))){
                //close navigation drawer

                //retrieveOrderHistory();
                handleAndroidNativeMenuItemClick(category);
            }
            return; //no need to refresh navigation pane

        }else if(ObjNavigationCategory.CONST_CATEGORY_TYPE_CATEGORIES.equals(strCategoryType)
                || ObjNavigationCategory.CONST_CATEGORY_TYPE_WIGGLE_API.equals(strCategoryType)
                || ObjNavigationCategory.CONST_CATEGORY_TYPE_MEGAMENU_API.equals(strCategoryType)
        ){

            /*
               Not doing this here - as this method will be re-executed as soon as categories are
               downloaded from the server which would lead to a double-counting of events that are sent earlier in this method
            //Reveal categories - send corresponding event
            String strCategoryTag = category.getVersion2GATag();
            String strCategoryTier = category.getVersion2GATagLevel();
            AppTagManager.instance().sendV2EventTags("tabclick","App - Navigation",strCategoryTier,strCategoryTag);
            */

            //no children? get them from API
            if(category.getAllChildren().size()==0){

                //show progress bar
                if(categoryAdapterView!=null){
                    if(navigationItem_ivMore!=null) {
                        navigationItem_ivMore.setVisibility(View.GONE);
                    }
                    if(navigationItem_pbInProgress!=null) {
                        navigationItem_pbInProgress.setVisibility(View.VISIBLE);
                    }
                }

                //get additional categories from Wiggle API
                /*
                new ObjApiRequest(V3HybridActivity.instance(),
                        null,
                        AppRequestHandler.ENDPOINT_CATEGORIES,
                        Request.Method.GET,
                        category,
                        null,
                        AppRequestHandler.ON_RESPONSE_ACTION.NO_ACTION)
                        .submit();

                 */
                getMegamenu(category);
                return; //navigation pane is refreshed or not depending on result of this request //TODO: add loading spinner and handle accordingly

            }

        }


        //if we get here i.e. in any other case ... update browser for given category
        //TODO: only update browser if there are further children to display

        //Version 2019.2 - each navigation click leads to update of the navigation tree
        updateNavigationBrowser();

    }


    private void handleAndroidNativeMenuItemClick(ObjNavigationCategory category){

        if("OrderTracking".equals(category.getString(ObjNavigationCategory.FIELD_MENU_ITEM_TARGET))){

            //send GTM Tag - report "linkclick" as another screen is opened (webview or native screen)
            String strCategoryTag = category.getVersion2GATag();
            String strCategoryTier = category.getVersion2GATagLevel();
            AppTagManager.instance().sendV2EventTags("linkclick","App - Navigation",strCategoryTier,strCategoryTag);


            //check if user is logged in
            if(ObjAppUser.instance().isUserLoggedIn()==false){
                AppSession.instance().setAndroidNativeCategory(category);
                V3HybridActivity.instance().showLoginScreen();
                return;
            }else{
                V3HybridActivity.instance().addOrderTrackingFragment(false);
            }

        }


    }


    public boolean shopFragmentCanGoBack(boolean blnBackWasPressed){


        V3HybridActivity.instance().onBackPressed();
        return false;

        //TODO: use back-button to navigate back-up in the navigation tree?
        /* Disabled the following as this confused users ...
        if(navigation_lytL2AccountActions.getVisibility()==View.VISIBLE){
            if(blnBackWasPressed){
                toggleAccountActions();
                return true;
            }
        }else{
            V3HybridActivity.instance().onBackPressed();
        }
        return false;
        */

    }

    public void shopFragmentGoBack(){

        //TODO: use back-button to navigate back-up in the navigation tree?

    }


    @Override
    public void onHiddenChanged(boolean hidden){

        if(!hidden){
            setTitleBarText();
        }
    }

    private void setTitleBarText(){

        if(V3HybridActivity.instance()!=null){
            Bundle data = getArguments();

            // Get the extras
            if (data.containsKey(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE)) {

                String strTitle = data.getString(AppGlobalConstants.FRAGMENT_BREADCRUMB_TITLE);
                V3HybridActivity.instance().setBreadcrumbText(strTitle);

            }

        }


    }

    private void toggleAccountActions(){

        if(navigation_lytL2AccountActions.getVisibility()==View.GONE){
            navigation_itemAllCategories.setVisibility(View.VISIBLE);
            navigation_itemAllCategories.setSelected(true);
            navigation_lytL2AccountActions.setVisibility(View.VISIBLE);
            navigation_itemL1Account.setSelected(true);
            ObjectAnimator.ofFloat(navigationItem_ivManageAcccount, "rotation", 0, -180).start();
            //AppAnimations.getInstance().doCircleReveal(navigation_lytL2AccountActions, AppAnimations.CENTER, AppAnimations.MIN, 2);
            AppAnimations.getInstance().expand(navigation_lytL2AccountActions);
            navigationTree.setVisibility(View.GONE);
        }else{
            navigation_itemAllCategories.setVisibility(View.GONE);
            navigation_itemL1Account.setSelected(false);
            ObjectAnimator.ofFloat(navigationItem_ivManageAcccount, "rotation", -180, 0).start();
            //AppAnimations.getInstance().doCircleCollapse(navigation_lytL2AccountActions, AppAnimations.CENTER, AppAnimations.MIN, 2);
            AppAnimations.getInstance().collapse(navigation_lytL2AccountActions);
            navigationTree.setVisibility(View.VISIBLE);
        }

    }


    private void showSupportMenu(){

        if(navigation_lytAccountActions.getVisibility()==View.GONE) {
            AppAnimations.getInstance().expand(navigation_lytAccountActions);
        }

    }

    private void hideSupportMenu(){

        if(navigation_lytAccountActions.getVisibility()==View.VISIBLE) {
            AppAnimations.getInstance().collapse(navigation_lytAccountActions);
        }

    }


    public void setAccountWidgetVisibility(){

        boolean isUserLoggedIn = ObjAppUser.instance().isUserLoggedIn();

        if(isUserLoggedIn){
            navigation_lytAnonymousUser.setVisibility(View.GONE);
            navigation_btnLogout.setVisibility(View.VISIBLE);
        }else{
            navigation_lytAnonymousUser.setVisibility(View.VISIBLE);
            navigation_btnLogout.setVisibility(View.GONE);
        }

    }


    private void showLoginScreen(){
        V3HybridActivity.instance().showLoginScreen();
    }

    private void showRegistrationScreen(int intClickedViewId){

        V3HybridActivity.instance().loadUrlForViewInWebViewFragment(intClickedViewId);

    }

    private void scrollToTop(){


        navigation_svBrowser.post(new Runnable() {
            @Override
            public void run() {
                navigation_svBrowser.smoothScrollTo(0, 0);
            }
        });

    }

    private void actionForLoggedInUser(int intClickedViewId){


        if(ObjAppUser.instance().isUserLoggedIn()){
            V3HybridActivity.instance().performAccountAction(intClickedViewId);
        }else{
            V3HybridActivity.instance().addUserLoginFragmentMFA(false, intClickedViewId);
        }

    }

    private void actionForAllUsers(int intClickedViewId){
        V3HybridActivity.instance().performAccountAction(intClickedViewId);
    }


    public void updateOrderTrackingBadge(){

        int intUpdatedOrders = ObjSalesOrderManager.getInstance().getOrdersWithNewInformation().size();

        if(intUpdatedOrders>0){
            //On "Account" - show 'new info' badge
            navigation_tvMyAccountOrderTrackingBadge.setText(""); //show empty dot
            navigation_tvMyAccountOrderTrackingBadge.setVisibility(View.VISIBLE);
            //On "My Orders" - show dedicated order tracking counter
            navigation_tvMyOrdersOrderTrackingBadge.setText(String.valueOf(intUpdatedOrders));
            navigation_tvMyOrdersOrderTrackingBadge.setVisibility(View.VISIBLE);
        }else{
            navigation_tvMyAccountOrderTrackingBadge.setVisibility(View.GONE);
            navigation_tvMyOrdersOrderTrackingBadge.setVisibility(View.GONE);
        }

    }

    private void setAppVersionLabel(){

        String strAppVersion = String.format(getString(R.string.myAccountHome_tvAppVersion)
                ,AppUtils.getAppVersionNumber()
        );
        String strUserInfo = AppUtils.getAppUserInfo();
        navigation_lblAppVersion.setText(strAppVersion + "\n" + strUserInfo);

    }

}