package uk.co.wiggle.hybrid.usecase.startup.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNavigationCategory;
import uk.co.wiggle.hybrid.application.helpers.AppTypefaces;
import uk.co.wiggle.hybrid.application.utils.AppUtils;
import uk.co.wiggle.hybrid.extensions.PicassoSSL;
import uk.co.wiggle.hybrid.tagging.AppTagManager;
import uk.co.wiggle.hybrid.usecase.startup.activities.StartupActivity;

/**
 *
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 *
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 *
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/
public class StartupMenuListAdapter extends BaseAdapter {

    private String logTag = "StartupMenuListAdapter";

    private List<ObjNavigationCategory> mMenuItemList = new ArrayList<ObjNavigationCategory>();
    private StartupActivity mContext;
    private int mSelectedGridItem = 0;
    private int NUMBER_OF_ITEMS_IN_ROW = 3;

    public StartupMenuListAdapter(StartupActivity context) {
        mContext = context;
    }

    public void setListContent(List<ObjNavigationCategory> listItems) {
        mMenuItemList = listItems;
    }

    @Override
    public int getCount() {
        //return mMenuItemList.size();
        //the size of the list view does no longer equal to the number of items in the list, but 
        //to the number of rows displayed. Each row displays NUMBER_OF_ITEMS_IN_ROW markers --> (items / NUMBER_OF_ITEMS_IN_ROW items displayed in each row)
        //this is used to e.g. calculate the scrollbar etc
        if ( (mMenuItemList.size() % NUMBER_OF_ITEMS_IN_ROW) == 0){
            return (int) (mMenuItemList.size() / NUMBER_OF_ITEMS_IN_ROW);
        }else{
            return (int) 1 + (mMenuItemList.size() / NUMBER_OF_ITEMS_IN_ROW);
        }
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    private class ViewHolder {

        LinearLayout startup_lytMenuItem1;
        LinearLayout startup_lytMenuItem2;
        LinearLayout startup_lytMenuItem3;

    }

    @Override
    public View getView(int arg0, View view, ViewGroup arg2)
    {
        final int listRowNumber = arg0;

        //get NUMBER_OF_ITEMS_IN_ROW items per list row
        ObjNavigationCategory menuItem1 = null;
        ObjNavigationCategory menuItem2 = null;
        ObjNavigationCategory menuItem3 = null;

        if(getItemCount()>arg0*NUMBER_OF_ITEMS_IN_ROW){
            menuItem1 = (ObjNavigationCategory) getItem(arg0 * NUMBER_OF_ITEMS_IN_ROW);
        }

        if(getItemCount()>arg0*NUMBER_OF_ITEMS_IN_ROW+1){
            menuItem2 = (ObjNavigationCategory) getItem(arg0 * NUMBER_OF_ITEMS_IN_ROW+1);
        }

        if(getItemCount()>arg0*NUMBER_OF_ITEMS_IN_ROW+2){
            menuItem3= (ObjNavigationCategory) getItem(arg0 * NUMBER_OF_ITEMS_IN_ROW+2);
        }

        ViewHolder holder;

        if (view == null){

            LayoutInflater inflater = LayoutInflater.from(mContext);
            view = inflater.inflate(R.layout.startup_menu_list_row_layout, null);

            holder = new ViewHolder();
            holder.startup_lytMenuItem1 = (LinearLayout) view.findViewById(R.id.startup_lytMenuItem1);
            holder.startup_lytMenuItem2 = (LinearLayout) view.findViewById(R.id.startup_lytMenuItem2);
            holder.startup_lytMenuItem3 = (LinearLayout) view.findViewById(R.id.startup_lytMenuItem3);
            view.setTag(holder);

        }else{
            holder = (ViewHolder)view.getTag();
        }

        final ImageView startup_ivMenuItem1 = (ImageView) holder.startup_lytMenuItem1.findViewById(R.id.startup_ivMenuItem1);
        final ImageView startup_ivMenuItem2 = (ImageView) holder.startup_lytMenuItem2.findViewById(R.id.startup_ivMenuItem2);
        final ImageView startup_ivMenuItem3 = (ImageView) holder.startup_lytMenuItem3.findViewById(R.id.startup_ivMenuItem3);

        final TextView startup_tvMenuItem1 = (TextView) holder.startup_lytMenuItem1.findViewById(R.id.startup_tvMenuItem1);
        final TextView startup_tvMenuItem2 = (TextView) holder.startup_lytMenuItem2.findViewById(R.id.startup_tvMenuItem2);
        final TextView startup_tvMenuItem3 = (TextView) holder.startup_lytMenuItem3.findViewById(R.id.startup_tvMenuItem3);

        startup_tvMenuItem1.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
        startup_tvMenuItem2.setTypeface(AppTypefaces.instance().fontMainRegularItalic);
        startup_tvMenuItem3.setTypeface(AppTypefaces.instance().fontMainRegularItalic);


        //handle items individually
        if(menuItem1!=null){

            //Set menu item title
            String strTitle= menuItem1.getString(ObjNavigationCategory.FIELD_TITLE);
            startup_tvMenuItem1.setText(strTitle);

            //Get menu item image
            String strMenuItemSource = menuItem1.getString(ObjNavigationCategory.FIELD_SOURCE);
            String strMenuImage = menuItem1.getImagePath(ObjNavigationCategory.FIELD_MENU_ITEM_IMAGE);

            if(ObjNavigationCategory.CONST_CATEGORY_SOURCE_CONTENTFUL.equals(strMenuItemSource)){

                if(strMenuImage!=null && !"".equals(strMenuImage)) {
                    //Contentful menu item - get image from Web
                    PicassoSSL.with(mContext).load(strMenuImage).into(startup_ivMenuItem1);
                }

            }else if(ObjNavigationCategory.CONST_CATEGORY_SOURCE_LOCAL.equals(strMenuItemSource)){

                if(strMenuImage!=null && !"".equals(strMenuImage)) {
                    //Local menu item - get image from drawables;
                    PicassoSSL.with(mContext).load(AppUtils.getDrawableByName(strMenuImage)).into(startup_ivMenuItem1);
                }
            }

            holder.startup_lytMenuItem1.setVisibility(View.VISIBLE);

        }else{
            holder.startup_lytMenuItem1.setVisibility(View.GONE);
        }


        //make this object final for inner class handling
        final ObjNavigationCategory finalMenuItem1 = menuItem1;
        holder.startup_lytMenuItem1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                handleMenuItemClick(finalMenuItem1);


            }

        });

        //highlight imageview on touch
        mContext.setDependantImageViewListener(holder.startup_lytMenuItem1, startup_ivMenuItem1, mContext.getResources().getColor(R.color.orangeButton));


        if(menuItem2!=null){

            //Set menu item title
            String strTitle= menuItem2.getString(ObjNavigationCategory.FIELD_TITLE);
            startup_tvMenuItem2.setText(strTitle);

            //Get menu item image
            String strMenuItemSource = menuItem2.getString(ObjNavigationCategory.FIELD_SOURCE);
            String strMenuImage = menuItem2.getImagePath(ObjNavigationCategory.FIELD_MENU_ITEM_IMAGE);

            if(ObjNavigationCategory.CONST_CATEGORY_SOURCE_CONTENTFUL.equals(strMenuItemSource)){

                if(strMenuImage!=null && !"".equals(strMenuImage)) {
                    //Contentful menu item - get image from Web
                    PicassoSSL.with(mContext).load(strMenuImage).into(startup_ivMenuItem2);
                }

            }else if(ObjNavigationCategory.CONST_CATEGORY_SOURCE_LOCAL.equals(strMenuItemSource)){

                if(strMenuImage!=null && !"".equals(strMenuImage)) {
                    //Local menu item - get image from drawables;
                    PicassoSSL.with(mContext).load(AppUtils.getDrawableByName(strMenuImage)).into(startup_ivMenuItem2);
                }
            }

            holder.startup_lytMenuItem2.setVisibility(View.VISIBLE);

        }else{
            holder.startup_lytMenuItem2.setVisibility(View.GONE);
        }


        //make this object final for inner class handling
        final ObjNavigationCategory finalMenuItem2 = menuItem2;
        holder.startup_lytMenuItem2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                handleMenuItemClick(finalMenuItem2);


            }

        });

        //highlight imageview on touch
        mContext.setDependantImageViewListener(holder.startup_lytMenuItem2, startup_ivMenuItem2, mContext.getResources().getColor(R.color.orangeButton));

        if(menuItem3!=null){

            //Set menu item title
            String strTitle= menuItem3.getString(ObjNavigationCategory.FIELD_TITLE);
            startup_tvMenuItem3.setText(strTitle);

            //Get menu item image
            String strMenuItemSource = menuItem3.getString(ObjNavigationCategory.FIELD_SOURCE);
            String strMenuImage = menuItem3.getImagePath(ObjNavigationCategory.FIELD_MENU_ITEM_IMAGE);

            if(ObjNavigationCategory.CONST_CATEGORY_SOURCE_CONTENTFUL.equals(strMenuItemSource)){

                if(strMenuImage!=null && !"".equals(strMenuImage)) {
                    //Contentful menu item - get image from Web
                    PicassoSSL.with(mContext).load(strMenuImage).into(startup_ivMenuItem3);
                }

            }else if(ObjNavigationCategory.CONST_CATEGORY_SOURCE_LOCAL.equals(strMenuItemSource)){

                if(strMenuImage!=null && !"".equals(strMenuImage)) {
                    //Local menu item - get image from drawables;
                    PicassoSSL.with(mContext).load(AppUtils.getDrawableByName(strMenuImage)).into(startup_ivMenuItem3);
                }
            }

            holder.startup_lytMenuItem3.setVisibility(View.VISIBLE);

        }else{
            holder.startup_lytMenuItem3.setVisibility(View.GONE);
        }


        //make this object final for inner class handling
        final ObjNavigationCategory finalMenuItem3 = menuItem3;
        holder.startup_lytMenuItem3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                handleMenuItemClick(finalMenuItem3);


            }

        });

        //highlight imageview on touch
        mContext.setDependantImageViewListener(holder.startup_lytMenuItem3, startup_ivMenuItem3, mContext.getResources().getColor(R.color.orangeButton));


        return view;
    }



    @Override
    public Object getItem(int arg0) {
        return mMenuItemList.get(arg0);
    }

    private int getItemCount(){
        return mMenuItemList.size();
    }

    private void handleMenuItemClick(ObjNavigationCategory menuItem){

        String strStartupScreenGTMID = menuItem.getString(ObjNavigationCategory.FIELD_STARTUP_GOOGLE_TAG_MANAGER_ID);
        String strTargetUrlPattern = menuItem.getString(ObjNavigationCategory.FIELD_LINK);

        //Send onClick event tag
        AppTagManager.instance().sendEventTags(mContext.getString(R.string.str_gtm_page_name_home), strStartupScreenGTMID);

        //Actions on click depend on configuration
        String strItemType = menuItem.getString(ObjNavigationCategory.FIELD_TYPE);

        //link items open a URL either external or internally
        if(ObjNavigationCategory.CONST_CATEGORY_TYPE_LINK.equals(strItemType)){

            //configured to open in external browser?
            if(menuItem.getBoolean(ObjNavigationCategory.FIELD_OPEN_IN_EXTERNAL_BROWSER)) {
                AppUtils.showCustomToast("Open in External Browser :" + strTargetUrlPattern, true);
                mContext.openUrlInExternalBrowser(AppUtils.getWiggleUrlWithUTMTracking(strTargetUrlPattern, strStartupScreenGTMID));
            }else{
                //open in HybridActivity
                AppUtils.showCustomToast("Open link in Hybrid Activity: " + strTargetUrlPattern, true);
                boolean blnCloseStartupActivity = true;
                //TODO: confirm with Wiggle - I assume that for "contentful" items the StartupActivity should perhaps not be closed?
                if(ObjNavigationCategory.CONST_CATEGORY_SOURCE_CONTENTFUL.equals(menuItem.getString(ObjNavigationCategory.FIELD_SOURCE))){
                    //menu item comes from contentful - do not close Startup screen
                    blnCloseStartupActivity = false;
                }
                mContext.launchHybridActivity(AppUtils.getWiggleUrlWithUTMTracking(strTargetUrlPattern, strStartupScreenGTMID), blnCloseStartupActivity);

            }


        }else if(ObjNavigationCategory.CONST_CATEGORY_TYPE_MENU_ITEM.equals(strItemType)){

            //in all other cases i.e. native event screen enabled - or any other category: start native screen
            AppUtils.showCustomToast("Start native screen " + menuItem.getString(ObjNavigationCategory.FIELD_MENU_ITEM_TARGET), true);
            mContext.startNativeScreen(menuItem);

        }

    }

}
