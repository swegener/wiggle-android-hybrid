package uk.co.wiggle.hybrid.application.animations;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.ApplicationContextProvider;
import uk.co.wiggle.hybrid.application.utils.AppUtils;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;

import static android.R.attr.duration;

public class AppAnimations {
	

	private static AppAnimations instance = null;
	HideOnEndListener hideOnEndListener = null;
	public static int LONG_ANIMATION = 2000;
	public static int MEDIUM_ANIMATION = 800;
	public static int SHORT_ANIMATION = 300;

	private static int COLLAPSE_EXPAND_ANIMATION_SPEED_MS_PER_DP = 1;

    public static final String COLLAPSE = "COLLAPSE";
    public static final String EXPAND = "EXPAND";

	//constants for circle reveal animation
	public static final String CENTER = "CENTER";
	public static final String MIN = "MIN";
	public static final String MAX = "MAX";
	
	private AppAnimations() {
		hideOnEndListener = new HideOnEndListener();
	}

	public static AppAnimations getInstance() {

		if (instance == null) {
			instance = new AppAnimations();
		}

		return instance;
	}
	
	final static Context mContext = ApplicationContextProvider.getContext();
	
	public static void pulseAnimation(View viewToAnimate, int intAnimation){
		
		//mLVDiscover.smoothScrollToPositionFromTop(intListPosition, 0, 300);
		Animation imageViewAnimation = AnimationUtils.loadAnimation(mContext, intAnimation);
		imageViewAnimation.setAnimationListener(
                new AnimationListener() {

                    @Override
                    public void onAnimationStart(Animation animation) {
                    	
                    }
                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    	
                    }
                    @Override
                    public void onAnimationEnd(Animation animation) {
                    	
                    	
                    }
                }
            );
		
		viewToAnimate.startAnimation(imageViewAnimation);
		
	}
	

	public void startFadeInAnimationWithDurationInMsAndStartOpacity(long duration, float startOpacity, View v) {
		// make the view visible and start the animation
		v.setAlpha(startOpacity);
		v.setVisibility(View.VISIBLE);
		v.animate().alpha(1).setDuration(duration);
	}
	
	
	public View startFadeInAnimationWithDurationInMs(long duration, View v) {
		
		// make the view visible and start the animation
		v.setAlpha(0.0f);
		v.setVisibility(View.VISIBLE);
		v.animate().alpha(1).setDuration(duration);
		return v;
	}
	
	public View startFadeOutAnimationWithDurationInMs(long duration, View v, boolean blnViewGoneAfterAnimation) {

        //An Android bug prevents us from waiting until "onAnimationEnd" when a view is set to gone
        //The "GONE" view consumes click events and often does not clear the occupied space.
        //Thus, just setting the view to GONE immediately.
        //More details, see http://stackoverflow.com/questions/30367587/visibility-of-gone-takes-still-space-of-particular-control-in-android?noredirect=1#comment48826130_30367587
        //TODO: this works fine though if a normal animation is used with Animation.setAnimationListener - convert this method accordingly
        if(blnViewGoneAfterAnimation){
            v.setVisibility(View.GONE);
            return v;
        }

        // make the view visible and start the animation
		v.setAlpha(1.0f);
		hideOnEndListener.setView(v);
        v.animate().alpha(0).setDuration(duration);
        return v;
		
	}
	
	
	

	public void startScrollInFromLeftAnimationWithDurationInMs(long duration, View v, Context ctx) {
		// make the view visible and start the animation
		v.setAlpha(1.0f);
		hideOnEndListener.setView(v);
		v.animate().translationX(v.getWidth()).setDuration(duration).alpha(0.0f);
	}

	public void startConfiguredAnimation(View viewToAnimate, int intAnimationXML) {
		// make the view visible and start the animation
		viewToAnimate.setVisibility(View.VISIBLE);
		Animation imageViewAnimation = AnimationUtils.loadAnimation(mContext, intAnimationXML);
		viewToAnimate.startAnimation(imageViewAnimation);

	}
	
	public class HideOnEndListener implements AnimatorListener {
		
		View v;
	    public void setView(View v) {
	        this.v = v;
	    }
		
		@Override
		public void onAnimationStart(Animator animation) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onAnimationEnd(Animator animation) {
			// TODO Auto-generated method stub
            //An Android bug causes a view whose visibility is set to GONE in "onAnimationEnd" to
            //still consume its layout space and consume click events. It becomes "INVISIBLE" rather than gone.
            //We have found, that setting the view to "INVISIBLE" first, then to "GONE"
            //causes the correct behaviour i.e. the view no longer consumes layout space and is no longer consuming click events.
            //More details on the underlying issue (not for solution), see http://stackoverflow.com/questions/30367587/visibility-of-gone-takes-still-space-of-particular-control-in-android?noredirect=1#comment48826130_30367587
            v.setVisibility(View.INVISIBLE); //seems superfluous, but helps to correctly execute the GONE call below
			v.setVisibility(View.GONE);
		}

		@Override
		public void onAnimationCancel(Animator animation) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onAnimationRepeat(Animator animation) {
			// TODO Auto-generated method stub
			
		}
		
	}

	public void expandOrCollapse(final View v, String strExpandOrCollapse) {

		TranslateAnimation anim = null;

		if(strExpandOrCollapse.equals(EXPAND)){
			anim = new TranslateAnimation(0.0f, 0.0f, -v.getHeight(), 0.0f);
			v.setVisibility(View.VISIBLE);
		}else{

			anim = new TranslateAnimation(0.0f, 0.0f, 0.0f, -v.getHeight());
			AnimationListener collapselistener= new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
				}

				@Override
				public void onAnimationEnd(Animation animation) {
					v.setVisibility(View.GONE);
				}
			};

			anim.setAnimationListener(collapselistener);
		}

		anim.setDuration(300);
		anim.setInterpolator(new AccelerateInterpolator(0.5f));
		v.startAnimation(anim);
	}

    public void doCircleReveal(View myView, String strCenterX, String strCenterY, int intRadiusDenominator) {

        //requires API level 21. Fallback: just set view to GONE
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            myView.setVisibility(View.VISIBLE);
            return;
        }

        // make the view visible and start the animation
        myView.setVisibility(View.VISIBLE);

		// get the center for the clipping circle
		int cx = 0; //initialise with MIN
		if(CENTER.equals(strCenterX)) {
			cx = myView.getMeasuredWidth() / 2;
		}else if(MAX.equals(strCenterX)){
			cx = myView.getMeasuredWidth();
		}
		//int cy = myView.getMeasuredHeight() / 2;

		int cy = 0; //initalise with MIN
		if(CENTER.equals(strCenterY)){
			cx = myView.getMeasuredHeight() / 2;
		}else if(MAX.equals(strCenterY)){
			cx = myView.getMeasuredHeight();
		}


        // get the final radius for the clipping circle
        int finalRadius = Math.max(myView.getWidth(), myView.getHeight()) / intRadiusDenominator;

        // create the animator for this view (the start radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);


        anim.start();
    }

    public void doCircleCollapse(final View myView, String strCenterX, String strCenterY, int intRadiusDenominator) {

        //requires API level 21. Fallback: just set view to GONE
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            myView.setVisibility(View.GONE);
            return;
        }

        // get the center for the clipping circle
        int cx = 0; //initialise with MIN
		if(CENTER.equals(strCenterX)) {
			cx = myView.getMeasuredWidth() / 2;
		}else if(MAX.equals(strCenterX)){
			cx = myView.getMeasuredWidth();
		}
		//int cy = myView.getMeasuredHeight() / 2;

		int cy = 0; //initalise with MIN
		if(CENTER.equals(strCenterY)){
			cx = myView.getMeasuredHeight() / 2;
		}else if(MAX.equals(strCenterY)){
			cx = myView.getMeasuredHeight();
		}

        // get the initial radius for the clipping circle
        int initialRadius = myView.getWidth() / intRadiusDenominator;

        // create the animation (the final radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(myView, cx, cy, initialRadius, 0);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                myView.setVisibility(View.INVISIBLE);
                myView.setVisibility(View.GONE);
            }
        });

        // start the animation
        anim.start();
    }

	public void expand(final View v) {
		int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
		int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
		v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
		final int targetHeight = v.getMeasuredHeight();

		// Older versions of android (pre API 21) cancel animations for views with a height of 0.
		v.getLayoutParams().height = 1;
		v.setVisibility(View.VISIBLE);
		Animation a = new Animation()
		{
			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {
				v.getLayoutParams().height = interpolatedTime == 1
						? LinearLayout.LayoutParams.WRAP_CONTENT
						: (int)(targetHeight * interpolatedTime);
				v.requestLayout();
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		// Expansion speed of COLLAPSE_EXPAND_ANIMATION_SPEED_MS_PER_DP * 1dp/ms
		a.setDuration(((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density)) * COLLAPSE_EXPAND_ANIMATION_SPEED_MS_PER_DP);
		v.startAnimation(a);
	}

	public void collapse(final View v) {
		final int initialHeight = v.getMeasuredHeight();

		Animation a = new Animation()
		{
			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {
				if(interpolatedTime == 1){
					v.setVisibility(View.GONE);
				}else{
					v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
					v.requestLayout();
				}
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		// Collapse speed of COLLAPSE_EXPAND_ANIMATION_SPEED_MS_PER_DP * 1dp/ms
		a.setDuration(((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density) * COLLAPSE_EXPAND_ANIMATION_SPEED_MS_PER_DP));
		v.startAnimation(a);
	}

}
