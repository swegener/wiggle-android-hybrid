package uk.co.wiggle.hybrid.usecase.shop.classicnav.fragments;


import java.util.ArrayList;
import java.util.List;

import uk.co.wiggle.hybrid.R;
import uk.co.wiggle.hybrid.application.datamodel.objectmanagers.ObjNavigationCategoryManager;
import uk.co.wiggle.hybrid.usecase.shop.classicnav.activities.HybridActivity;
import uk.co.wiggle.hybrid.usecase.shop.classicnav.adapters.NavigationCategoryAdapter;
import uk.co.wiggle.hybrid.usecase.shop.classicnav.adapters.NavigationHeaderAdapter;
import uk.co.wiggle.hybrid.application.datamodel.objects.ObjNavigationCategory;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentSelfCloseListener;
import uk.co.wiggle.hybrid.interfaces.IAppFragmentUI;
import uk.co.wiggle.hybrid.tagging.AppTagManager;
import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import androidx.legacy.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */

/**
 * 
 * @author   Created by AndroMedia IT Ltd on behalf of Wiggle UK Ltd
 * 
 * @license  The code of this Application is licensed to Wiggle UK Ltd.
 *           The license and its ownership rights can not be transferred to a Third Party.
 * 
 * @android  If you need help with your Android project feel free to contact us on enquiries@andromedia.biz :-)
 *
 **/

public class NavigationDrawerFragment extends Fragment implements IAppFragmentUI {

    /**
     * Remember the position of the selected item.
     */
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    /**
     * Per the design guidelines, you should show the drawer on launch until the user manually
     * expands it. This shared preference tracks this.
     */
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";


    /**
     * Helper component that ties the action bar to the navigation drawer.
     */
    private ActionBarDrawerToggle mDrawerToggle;

    private DrawerLayout mDrawerLayout;
    //private ListView mDrawerListView;
    private View mFragmentContainerView;

    //private int mCurrentSelectedPosition = 0;
    private boolean mFromSavedInstanceState;
    private boolean mUserLearnedDrawer;
    
    //Navigation category and sub-category list
  	NavigationHeaderAdapter	 				mSearchHeaderAdapter;
  	NavigationCategoryAdapter	 			mSearchCategoryChildAdapter;
  	NavigationCategoryAdapter	 			mSearchAllCategoriesAdapter;
  	UpdateNavigationBrowserTask 			mUpdateNavigationBrowserTask = new UpdateNavigationBrowserTask();	
    ListView lvBreadcrumb;
    ListView lvCategories;
    
    IAppFragmentSelfCloseListener selfCloseListener;
    
    private static NavigationDrawerFragment myInstance;
    
    public static NavigationDrawerFragment instance(){
    	return myInstance;
    }

    public NavigationDrawerFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        myInstance = this;

        // Read in the flag indicating whether or not the user has demonstrated awareness of the
        // drawer. See PREF_USER_LEARNED_DRAWER for details.
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

        if (savedInstanceState != null) {
            //mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
        }
        
        mSearchHeaderAdapter = new NavigationHeaderAdapter(this);
        mSearchCategoryChildAdapter = new NavigationCategoryAdapter(this);
        mSearchAllCategoriesAdapter = new NavigationCategoryAdapter(this);

        // Select either the default item (0) or the last selected item.
        //selectItem(mCurrentSelectedPosition);
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Indicate that this fragment would like to influence the set of actions in the action bar.
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	
    	View v = inflater.inflate(R.layout.navigation_browser_categories, container, false);
        
        lvBreadcrumb = (ListView) v.findViewById(R.id.navigation_lvBreadcrumb);
        lvCategories = (ListView) v.findViewById(R.id.navigation_lvCategories);
        
    	return v;
        
    }

    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    /**
     * Users of this fragment must call this method to set up the navigation drawer interactions.
     *
     * @param fragmentId   The android:id of this fragment in its activity's layout.
     * @param drawerLayout The DrawerLayout containing this fragment's UI.
     */
    public void setUp(int fragmentId, DrawerLayout drawerLayout) {
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the navigation drawer and the action bar app icon.
        mDrawerToggle = new ActionBarDrawerToggle(
                getActivity(),                    /* host Activity */
                mDrawerLayout,                    /* DrawerLayout object */
                R.drawable.ic_drawer,             /* nav drawer image to replace 'Up' caret */
                R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
                R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) {
                    return;
                }

                getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                
                if (!isAdded()) {
                    return;
                }
                
                AppTagManager.instance().sendEventTags(getString(R.string.str_gtm_page_name_shop), "shopOpenLHS");

                if (!mUserLearnedDrawer && !HybridActivity.instance().isInternalActivityCall()) {
                    // The user manually opened the drawer; store this flag to prevent auto-showing
                    // the navigation drawer automatically in the future.
                    mUserLearnedDrawer = true;
                    SharedPreferences sp = PreferenceManager
                            .getDefaultSharedPreferences(getActivity());
                    sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).apply();
                }

                getActivity().invalidateOptionsMenu(); // calls onPrepareOptionsMenu()
                
                
            }
        };

        // If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
        // per the navigation drawer design guidelines.
        if (!mUserLearnedDrawer && !mFromSavedInstanceState && !HybridActivity.instance().isInternalActivityCall()) {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }

        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    public void closeDrawer(){
    	if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
    }

    
    public void onItemSelected(ObjNavigationCategory category){
    	
    	if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
    	
    	HybridActivity.instance().restoreActionBar();
    	
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Forward the new configuration the drawer toggle component.
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    	/**
        // If the drawer is open, show the global app actions in the action bar. See also
        // showGlobalContextActionBar, which controls the top-left area of the action bar.
        if (mDrawerLayout != null && isDrawerOpen()) {
            inflater.inflate(R.menu.global, menu);
            showGlobalContextActionBar();
        }
        **/
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    /**
     * Per the navigation drawer design guidelines, updates the action bar to show the global app
     * 'context', rather than just what's in the current screen.
     */
    private void showGlobalContextActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setTitle(R.string.app_name);
    }

    private ActionBar getActionBar() {
        return getActivity().getActionBar();
    }

    /**
     * Callbacks interface that all activities using this fragment must implement.
     */
    public static interface NavigationDrawerCallbacks {
        /**
         * Called when an item in the navigation drawer is selected.
         */
        void onNavigationDrawerItemSelected(int position);
    }

	@Override
	public void updateFragmentContent(View view) {
		// TODO Auto-generated method stub
		
	}
	
	private class UpdateNavigationBrowserTask implements Runnable {
		
		@Override
		public void run() {
			
			List<ObjNavigationCategory> lstMenuItems = null;
            List<ObjNavigationCategory> lstBreadcrumb = null;
			
			ObjNavigationCategory currentNavigationCategory = HybridActivity.instance().getCurrentNavigationCategory();
			
			//prevent NPE
			if(currentNavigationCategory==null){
				return;
			}
			
			
			//get menu items to display
			if(ObjNavigationCategory.CONST_CATEGORY_TYPE_WIGGLE_API.equals(currentNavigationCategory.getString(ObjNavigationCategory.FIELD_TYPE))
				|| ObjNavigationCategory.CONST_CATEGORY_TYPE_CATEGORIES.equals(currentNavigationCategory.getString(ObjNavigationCategory.FIELD_TYPE))
				){
				//for leave node of top_level_navigation.json and Wiggle API nodes - get all children (i.e. always display all categories returned by Wiggle API)
				lstMenuItems = currentNavigationCategory.getAllChildren();
			}else{

                //for the very top level category (Wiggle) - just get the combined navigation tree
                if(currentNavigationCategory.getParent()==null){
                    lstMenuItems = ObjNavigationCategoryManager.getInstance().getCombinedNavigationTree(HybridActivity.getLogTag()).getChildrenForCurrentCountry();
                }else {
                    //This is not the very top level item (i.e. not the artificial top level "Wiggle", but any top level category below that)
                    //for category parents taken from top_level_navigation.json - only display children applicable to the current country
                    lstMenuItems = currentNavigationCategory.getChildrenForCurrentCountry();
                }
			}

			//build header (breadcrumb and "Back to" string)
			lstBreadcrumb = currentNavigationCategory.getBreadcrumb();
			
			// From the breadcrumb - only use the last 2 items
			//  Item 1 is the "< Back to" item
			//  Item 2 is the "Header" of the current category "(All) xxx"
			//
			List<ObjNavigationCategory> lstHeader = new ArrayList<ObjNavigationCategory>();
			if(lstBreadcrumb.size()>1){ 
				
				if(lstBreadcrumb.size()>1){
					lstHeader.add(lstBreadcrumb.get(1)); //add the "Back to" entry
				}
				
				if(!ObjNavigationCategory.CONST_CATEGORY_TYPE_SUBMENU.equals(currentNavigationCategory.getString(ObjNavigationCategory.FIELD_TYPE))){
					lstHeader.add(lstBreadcrumb.get(0)); //add the "(All) xxx" entry. We are not doing it for the first level "shop by department" entry
				}
				
				mSearchHeaderAdapter.setListContent(lstHeader, lstBreadcrumb);
				lvBreadcrumb.setAdapter(mSearchHeaderAdapter);
				mSearchHeaderAdapter.notifyDataSetChanged();
				
				lvBreadcrumb.setVisibility(View.VISIBLE);
			}else{
				//do not show listview if this is the root element
				lvBreadcrumb.setVisibility(View.GONE);
			}				


			
			
			//tvBreadcrumb.setText(mObjKombiCategoryManager.getBreadcrumbString(currentSearchCategory, searchDefinition.getSearchParameter(ObjSearchDefinition.FIELD_LANGUAGE)));
			
			//get children for current category

			
			if(lstMenuItems.size()>0){
				mSearchCategoryChildAdapter.setListContent(lstMenuItems, lstBreadcrumb);
				lvCategories.setAdapter(mSearchCategoryChildAdapter);
				mSearchCategoryChildAdapter.notifyDataSetChanged();
				lvCategories.setVisibility(View.VISIBLE);
			} else {
				lvCategories.setVisibility(View.INVISIBLE);
			}
			
		}

	}
	
	public void updateNavigationBrowser(){
		
		//prevent NPEs
		if(getActivity()==null){
			return;
		}
		
		getActivity().runOnUiThread(mUpdateNavigationBrowserTask);
	}
	
	@Override
	public void onResume(){
		super.onResume();
		

		if(getActivity()==null){
			selfCloseListener.onFragmentSelfClose(getTag());
			return;
		}
		
		//workaround if app was stuck in the background for a while - try to update navigation browser if invisible
		if(lvCategories!=null && lvCategories.getVisibility()==View.INVISIBLE){
			updateNavigationBrowser();
		}
		
	}
	
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
           selfCloseListener = (IAppFragmentSelfCloseListener) activity;
        } catch (ClassCastException e) {
           throw new RuntimeException(getActivity().getClass().getName() + " must implement IAppFragmentSelfCloseListener to use this fragment", e);
        }
    }

	public void updateBadgeCounter(String strMenuItemWithBadge, int intCounterValue) {
		
		
		int intMenuItems = lvCategories.getChildCount();
		
		for(int i=0;i<intMenuItems;i++){
			View view = lvCategories.getChildAt(i);
			if(view.getTag()==null){
				continue;
			}
			
			String strTag = view.getTag().toString();
			if(strTag!=null && strMenuItemWithBadge.equals(strTag)){
				
				TextView tvBadge = (TextView) view.findViewById(R.id.navigationItem_tvCounterBadge);
				
				if(tvBadge!=null){
					
					if(intCounterValue>0){
						tvBadge.setText(String.valueOf(intCounterValue));
						tvBadge.setVisibility(View.VISIBLE);
					}else{
						tvBadge.setVisibility(View.GONE);
					}
				}
				
			}
		}
		
		
	}
	
	public void openNavigationDrawer(){
        if(mDrawerLayout!=null && mFragmentContainerView!=null) {
            mDrawerLayout.openDrawer(mFragmentContainerView);
        }
    }
}

